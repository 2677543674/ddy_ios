//
//  LYAgreementActionView.h
//  DingDingYang
//
//  Created by zhaoyang on 2019/11/21.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBActionView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LYAgreementActionView : FBActionView

@end

NS_ASSUME_NONNULL_END
