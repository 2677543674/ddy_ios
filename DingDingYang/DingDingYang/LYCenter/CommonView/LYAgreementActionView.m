//
//  LYAgreementActionView.m
//  DingDingYang
//
//  Created by zhaoyang on 2019/11/21.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "LYAgreementActionView.h"
#import "WCAttributedTextMaker.h"

@interface LYAgreementActionView ()

@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UIButton *continueBtn;

@property (nonatomic, strong) UIButton *confirmBtn;

@end

@implementation LYAgreementActionView

- (instancetype)init {
    if ([super init]) {
        self.contentView = ({
            UIView *aView = [[UIView alloc] init];
            aView.backgroundColor = [UIColor whiteColor];
            aView.layer.cornerRadius = 15;
            aView.layer.masksToBounds = true;
            
            aView;
        });
        
        [self addSubview:self.contentView];
        
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(0);
            make.width.mas_equalTo(300);
        }];
        
        self.titleLabel = ({
            UILabel *aLabel = [[UILabel alloc] init];
            aLabel.text = @"温馨提示";
            aLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
            aLabel.textColor = [UIColor blackColor];
            
            aLabel;
        });
        
        [self.contentView addSubview:self.titleLabel];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(20);
            make.centerX.mas_equalTo(0);
        }];
        
        self.contentLabel = ({
            UILabel *aLabel = [[UILabel alloc] init];
            aLabel.textAlignment = NSTextAlignmentCenter;
            aLabel.numberOfLines = 0;
            
            NSMutableAttributedString *tmpAtt = [WCAttributedTextMaker makeAttributedTextUsingBlock:^(id<WCAttributedTextAppend>  _Nonnull maker) {
                [maker appendText:@"感谢您信任并使用叮叮羊微集！\n我们深知个人信息对您的重要性，我们将按法\n律法规要求，采取相应安全保护措施，尽力保护\n您的个人信息安全可控。\n\n" color:[UIColor colorWithHexString:@"#474747"] font:[UIFont systemFontOfSize:12]];
                [maker appendText:@"鉴此，广州叮叮羊微集网络科技有限公司（或简\n称“我们”）制定本隐私权政策并提醒您\n\n" color:[UIColor colorWithHexString:@"#DD4747"] font:[UIFont systemFontOfSize:12]];
                [maker appendText:@"1您在使用叮叮羊微集各项产品或服务时，将会\n提供与具体功能相关的个人信息（可能涉及账\n号、位置、交易等信息）。\n2您可以对上述信息进行访问、更正、删除以\n及撤回同意等。\n3未经您的再次同意，我们不会将上述信息用\n于您未授权的其他用途或目的。\n4未经监护人同意，我们不会收集使用14周岁\n以下（含）未成年人个人信息，且不会利用其\n信息推送新闻、时政信息、广告等定向推送活\n动。\n\n" color:[UIColor colorWithHexString:@"#989898"] font:[UIFont systemFontOfSize:12]];
                [maker appendText:@"点击同意即表示已阅读《叮叮羊微集用户注册\n服务协议》和《叮叮羊微集隐私政策》" color:[UIColor colorWithHexString:@"#DD4747"] font:[UIFont systemFontOfSize:12]];
            }].mutableCopy;
            
            NSMutableParagraphStyle *mutPara = [[NSMutableParagraphStyle alloc] init];
            mutPara.minimumLineHeight = (16);
            mutPara.maximumLineHeight = (16);

            [tmpAtt addAttribute:NSParagraphStyleAttributeName value:[mutPara copy] range:NSMakeRange(0, tmpAtt.length)];
            
            aLabel.attributedText = [tmpAtt copy];
            
            aLabel;
        });
        
        [self.contentView addSubview:self.contentLabel];
        
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
            make.centerX.mas_equalTo(0);
        }];
        
        self.continueBtn = ({
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setBackgroundColor:[UIColor colorWithHexString:@"#F1F1F1"]];
            [btn setTitle:@"继续阅读" forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:16];
            [btn setTitleColor:[UIColor colorWithHexString:@"#989898"] forState:UIControlStateNormal];
            
            btn.layer.cornerRadius = 20;
            btn.layer.masksToBounds = true;
            
            btn;
        });
        
        [self.contentView addSubview:self.continueBtn];
        
        [self.continueBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentLabel.mas_left);
            make.top.equalTo(self.contentLabel.mas_bottom).offset(10);
            make.width.mas_equalTo(120);
            make.height.mas_equalTo(40);
            make.bottom.mas_equalTo(-20);
        }];
        
        self.confirmBtn = ({
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setBackgroundColor:[UIColor colorWithHexString:@"#F23427"]];
            [btn setTitle:@"我已确认" forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:16];
            [btn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
            
            [btn addTarget:self action:@selector(confirmBtnClick) forControlEvents:UIControlEventTouchUpInside];
            
            btn.layer.cornerRadius = 20;
            btn.layer.masksToBounds = true;
            
            btn;
        });
        
        [self.contentView addSubview:self.confirmBtn];
        
        [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentLabel);
            make.top.equalTo(self.continueBtn);
            make.width.mas_equalTo(120);
            make.height.mas_equalTo(40);
        }];
    }
    
    return self;
}

- (void)confirmBtnClick {
    [self removeFromSuperview];
}

@end
