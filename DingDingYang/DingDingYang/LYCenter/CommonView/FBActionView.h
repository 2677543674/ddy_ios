//
//  FBActionView.h
//  LzyActionViewController
//
//  Created by zhaoyang on 2019/6/18.
//  Copyright © 2019 lzy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBActionView : UIView

/**
 需要重写的方法
 */
- (void)show;

- (void)showInView:(UIView *)view;

- (void)hide;

/**
 点击了黑色背景
 */
@property (copy, nonatomic) dispatch_block_t closeBlock;


/**
 默认为 黑色 0.2透明度，没有高斯模糊
 */
- (void)configureBgColor:(UIColor *)color blur:(BOOL)isBlur;

- (void)removeAllContentSubView;


@end

NS_ASSUME_NONNULL_END
