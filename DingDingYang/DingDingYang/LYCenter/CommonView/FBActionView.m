//
//  FBActionView.m
//  LzyActionViewController
//
//  Created by zhaoyang on 2019/6/18.
//  Copyright © 2019 lzy. All rights reserved.
//

#import "FBActionView.h"

@interface FBActionView ()

@property (strong, nonatomic) UIControl *bgControl;

@end

@implementation FBActionView

- (instancetype)init{
    if ([super init]) {
        //baseViewController的zPosition为1，要盖过它
        self.layer.zPosition = 2;
        
        self.backgroundColor = [UIColor clearColor];
        
        self.bgControl = ({
            UIControl *control = [[UIControl alloc] init];
            [control addTarget:self action:@selector(bgControlClick) forControlEvents:UIControlEventTouchUpInside];
            [control setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.2]];
            
            control;
        });
        
        [self addSubview:self.bgControl];
        
        [self.bgControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(0);
        }];
    }
    
    return self;
}

- (void)configureBgColor:(UIColor *)color blur:(BOOL)isBlur{
    if (isBlur) {
        self.bgControl.backgroundColor = [UIColor clearColor];
        [self showEffectViewWithColor:color];
    }else{
        self.bgControl.backgroundColor = color;
    }
}

- (void)showEffectViewWithColor:(UIColor *)bgColor{
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
    
    effectView.userInteractionEnabled = false;
    self.bgControl.backgroundColor = [UIColor clearColor];
    
    [effectView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isMemberOfClass:NSClassFromString(@"_UIVisualEffectSubview")]) {
            obj.backgroundColor = bgColor;
        }
        
        if ([obj isMemberOfClass:NSClassFromString(@"_UIVisualEffectBackdropView")]) {
            obj.alpha = 0.8;
        }
        
        NSLog(@"%@",obj);
    }];
    
    [self addSubview:effectView];
    
    [effectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

- (void)show{
    [[UIApplication sharedApplication].delegate.window addSubview:self];
    
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    [self layoutIfNeeded];
}

- (void)showInView:(UIView *)view{
    [view addSubview:self];
    
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    [self layoutIfNeeded];
}

- (void)hide{
    
}


- (void)bgControlClick{
    if (self.closeBlock) {
        self.closeBlock();
    }
}

- (void)removeAllContentSubView{
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj isEqual:self.bgControl]) {
            [obj removeFromSuperview];
        }
    }];
    
    NSArray<CALayer *> *layerArr = [self.layer.sublayers copy];
    
    [layerArr enumerateObjectsUsingBlock:^(__kindof CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[CAShapeLayer class]]) {
            [obj removeFromSuperlayer];
        }
    }];
    
    self.layer.mask = nil;
}


@end


