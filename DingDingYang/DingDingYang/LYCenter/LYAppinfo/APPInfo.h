//
//  APPInfo.h
//  DingDingYang
//
//  Created by zhaoyang on 2019/11/18.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

static inline NSDictionary *infoDictionary() {
    static NSDictionary *dic;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dic = [[NSBundle mainBundle] infoDictionary];
    });
    
    return dic;
}

static inline NSString *APPCurrentVersion() {
    return [infoDictionary() objectForKey:@"CFBundleShortVersionString"];
}

static inline NSString *APPName() {
    return [infoDictionary() objectForKey:@"CFBundleDisplayName"];
}

static inline BOOL APPIsFirstAppear () {
    static BOOL isFirstOpenAPP = false;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:APPCurrentVersion()];
        if (str.length == 0) {
            isFirstOpenAPP = true;

            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:APPCurrentVersion()];
        }
    });
    
    return isFirstOpenAPP;
}


@interface APPInfo : NSObject

@end

NS_ASSUME_NONNULL_END
