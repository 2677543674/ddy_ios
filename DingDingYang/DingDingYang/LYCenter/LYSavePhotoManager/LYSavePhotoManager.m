//
//  LYSavePhotoManager.m
//  DingDingYang
//
//  Created by zhaoyang on 2019/11/13.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "LYSavePhotoManager.h"
#import <SDWebImage.h>

@interface LYSavePhotoManager ()

@property (nonatomic, copy) void (^imageCompleteBlock)(NSString *);

@property (nonatomic, copy) void (^videoCompleteBlock)(NSString *);

@end

@implementation LYSavePhotoManager

- (void)saveImageToAlbumWithUrl:(NSString *)url complete:(void (^)(NSString * _Nonnull))completeBlock{
    self.imageCompleteBlock = completeBlock;
    
    NSURL *imageUrl = [NSURL URLWithString:url];
    NSData *data = [NSData dataWithContentsOfURL:imageUrl];
    
    UIImage *image = [UIImage imageWithData:data];
    
    if (image) {
        UIImageWriteToSavedPhotosAlbum(image,self,@selector(image:didFinishSavingWithError:contextInfo:), nil );
    } else {
        if (completeBlock) {
            completeBlock(@"下载图片失败");
        }
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error) {
        if (self.imageCompleteBlock) {
            self.imageCompleteBlock(@"请您进入设置打开允许访问相册开关");
        }
    } else {
        if (self.imageCompleteBlock) {
            self.imageCompleteBlock(@"");
        }
    }
}

- (void)saveVideoToAlbumWithUrl:(NSString *)url complete:(void (^)(NSString * _Nonnull))completeBlock{
    self.videoCompleteBlock = completeBlock;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    NSString *fullPath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"jaibaili.mp4"];

    NSURL *urlNew = [NSURL URLWithString:url];

    NSURLRequest *request = [NSURLRequest requestWithURL:urlNew];

    NSURLSessionDownloadTask *task = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        return [NSURL fileURLWithPath:fullPath];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        [self saveVideo:fullPath];
    }];

    [task resume];
}

- (void)saveVideo:(NSString *)videoPath{
    if (videoPath) {
        NSURL *url = [NSURL URLWithString:videoPath];
        BOOL compatible = UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([url path]);
        if (compatible) {
            UISaveVideoAtPathToSavedPhotosAlbum([url path], self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
    } else {
        if (self.videoCompleteBlock) {
            self.videoCompleteBlock(@"下载视频失败");
        }
    }
}

- (void) savedPhotoImage:(UIImage*)image didFinishSavingWithError: (NSError *)error contextInfo: (void *)contextInfo {
        if (error) {
        if (self.videoCompleteBlock) {
            self.videoCompleteBlock(@"请您进入设置打开允许访问相册开关");
        }
    } else {
        if (self.videoCompleteBlock) {
            self.videoCompleteBlock(@"");
        }
    }
}

@end
