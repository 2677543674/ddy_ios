//
//  LYSavePhotoManager.h
//  DingDingYang
//
//  Created by zhaoyang on 2019/11/13.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LYSavePhotoManager : NSObject

- (void)saveImageToAlbumWithUrl:(NSString *)url complete:(void (^)(NSString *errorStr))completeBlock;


- (void)saveVideoToAlbumWithUrl:(NSString *)url complete:(void (^)(NSString *errorStr))completeBlock;

@end

NS_ASSUME_NONNULL_END
