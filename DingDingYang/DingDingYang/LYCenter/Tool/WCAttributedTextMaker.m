//
//  WCAttributedTextMaker.m
//  WarmCurrent
//
//  Created by JohnnyB0Y on 2019/5/30.
//  Copyright © 2019 广州暖心流网络科技有限公司（本内容仅限于广州暖心流网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

#import "WCAttributedTextMaker.h"

@interface WCAttributedTextMaker ()
<WCAttributedTextAppend>

@property (nonatomic, strong) NSMutableArray<id<WCAttributedTextCreate>> *itemsM;

@end

@implementation WCAttributedTextMaker

#pragma mark - WCAttributedStringAppend
- (void)appendText:(NSString *)text color:(UIColor *)color font:(UIFont *)font
{
    WCAttributedTextItem *item = [WCAttributedTextItem newWithText:text color:color font:font];
    [self appendAttributedStringItem:item];
}

- (void)appendAttributedStringItem:(WCAttributedTextItem *)item
{
    if ([item isKindOfClass:[WCAttributedTextItem class]]) {
        [self.itemsM addObject:item];
    }
}

- (void)appendImage:(UIImage *)image aspectRatio:(CGFloat)aspectRatio font:(UIFont *)font
{
    WCAttributedImageItem *item = [WCAttributedImageItem newWithImage:image aspectRatio:aspectRatio font:font];
    [self appendAttributedImageItem:item];
}

- (void)appendAttributedImageItem:(WCAttributedImageItem *)item
{
    if ([item isKindOfClass:[WCAttributedImageItem class]]) {
        [self.itemsM addObject:item];
    }
}

- (void)appendWhiteSpace
{
    WCAttributedTextItem *item = [WCAttributedTextItem newWithText:@" " color:nil font:nil];
    [self appendAttributedStringItem:item];
}

#pragma mark - ---------- Public Methods ----------
+ (NSAttributedString *)makeAttributedTextUsingBlock:(WCAttributedTextMakerBlock)block
{
    WCAttributedTextMaker *maker = [WCAttributedTextMaker new];
    block ? block(maker) : nil;
    
    if (maker.itemsM.count > 0) {
        NSMutableAttributedString *attrM = [[NSMutableAttributedString alloc] init];
        [maker.itemsM enumerateObjectsUsingBlock:^(id<WCAttributedTextCreate>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([obj respondsToSelector:@selector(createAttributedText)]) {
                NSAttributedString *attr = [obj createAttributedText];
                if (attr) {
                    [attrM appendAttributedString:attr];
                }
            }
            
        }];
        
        return [attrM copy];
    }
    
    return nil;
}

#pragma mark - ---------- Private Methods ----------


#pragma mark - ----------- Getter Methods -----------
- (NSMutableArray<id<WCAttributedTextCreate>> *)itemsM
{
    if (nil == _itemsM) {
        _itemsM = [NSMutableArray arrayWithCapacity:5];
    }
    return _itemsM;
}

@end

@implementation WCAttributedTextItem

#pragma mark - ----------- Life Cycle -----------
- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.text = @"";
        self.color = [UIColor blackColor];
        self.font = [UIFont systemFontOfSize:16];
    }
    
    return self;
}

- (instancetype)initWithText:(NSString *)text color:(UIColor *)color font:(UIFont *)font
{
    self = [self init];
    
    if (self) {
        self.text = text;
        if (color) {
            self.color = color;
        }
        if (font) {
            self.font = font;
        }
    }
    
    return self;
}

+ (instancetype)newWithText:(NSString *)text color:(UIColor *)color font:(UIFont *)font
{
    return [[self alloc] initWithText:text color:color font:font];
}

#pragma mark -
- (NSAttributedString *)createAttributedText
{
    if (self.text.length <= 0) return nil;
    
    NSMutableDictionary *attrDictM = [NSMutableDictionary dictionaryWithCapacity:2];
    attrDictM[NSForegroundColorAttributeName] = self.color;
    attrDictM[NSFontAttributeName] = self.font;
    return [[NSAttributedString alloc] initWithString:self.text attributes:attrDictM];
}

@end


@implementation WCAttributedImageItem

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.aspectRatio = 1;
        self.font = [UIFont systemFontOfSize:16];
    }
    
    return self;
}

- (instancetype)initWithImage:(UIImage *)image aspectRatio:(CGFloat)aspectRatio font:(UIFont *)font
{
    self = [self init];
    
    if (self) {
        self.image = image;
        if (aspectRatio > 0) {
            self.aspectRatio = aspectRatio;
        }
        if (font) {
            self.font = font;
        }
    }
    
    return self;
}

+ (instancetype)newWithImage:(UIImage *)image aspectRatio:(CGFloat)aspectRatio font:(UIFont *)font
{
    return [[self alloc] initWithImage:image aspectRatio:aspectRatio font:font];
}

#pragma mark -
- (NSAttributedString *)createAttributedText
{
    if (nil == self.image) return nil;
    
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = self.image;
    CGFloat height = self.font.pointSize;
    CGFloat width = height * self.aspectRatio;
    CGFloat textPaddingTop = self.font.descender / 2;
    textAttachment.bounds = CGRectMake(0, textPaddingTop, width, height);
    return [NSAttributedString attributedStringWithAttachment:textAttachment];
}

@end
