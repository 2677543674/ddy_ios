//
//  WCAttributedTextMaker.h
//  WarmCurrent
//
//  Created by JohnnyB0Y on 2019/5/30.
//  Copyright © 2019 广州暖心流网络科技有限公司（本内容仅限于广州暖心流网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCAttributedTextItem, WCAttributedImageItem;
@protocol WCAttributedTextAppend, WCAttributedTextCreate;

NS_ASSUME_NONNULL_BEGIN


typedef void(^WCAttributedTextMakerBlock)(id<WCAttributedTextAppend> maker);


@protocol WCAttributedTextAppend <NSObject>

- (void)appendText:(NSString *)text color:(UIColor *)color font:(UIFont *)font;
- (void)appendAttributedStringItem:(WCAttributedTextItem *)item;

- (void)appendImage:(UIImage *)image aspectRatio:(CGFloat)aspectRatio font:(UIFont *)font;
- (void)appendAttributedImageItem:(WCAttributedImageItem *)item;


- (void)appendWhiteSpace;


@end

@protocol WCAttributedTextCreate <NSObject>

- (NSAttributedString *)createAttributedText;

@end

@interface WCAttributedTextMaker : NSObject

+ (NSAttributedString *)makeAttributedTextUsingBlock:(WCAttributedTextMakerBlock)block;

@end


@interface WCAttributedTextItem : NSObject
<WCAttributedTextCreate>

/** 文字 */
@property (nonatomic, copy) NSString *text;
/** 颜色 */
@property (nonatomic, copy) UIColor *color;
/** 字体 */
@property (nonatomic, copy) UIFont *font;

- (instancetype)initWithText:(NSString *)text color:(nullable UIColor *)color font:(nullable UIFont *)font;
+ (instancetype)newWithText:(NSString *)text color:(nullable UIColor *)color font:(nullable UIFont *)font;

@end


@interface WCAttributedImageItem : NSObject
<WCAttributedTextCreate>

/** 图片 */
@property (nonatomic, copy) UIImage *image;
/** 宽高比 */
@property (nonatomic, assign) CGFloat aspectRatio;
/** 字体 */
@property (nonatomic, copy) UIFont *font;

- (instancetype)initWithImage:(UIImage *)image aspectRatio:(CGFloat)aspectRatio font:(nullable UIFont *)font;
+ (instancetype)newWithImage:(UIImage *)image aspectRatio:(CGFloat)aspectRatio font:(nullable UIFont *)font;

@end


NS_ASSUME_NONNULL_END
