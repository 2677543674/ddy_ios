//
//  CustomUMShare.m
//  DingDingYang
//
//  Created by ddy on 2018/4/9.
//

#import "CustomUMShare.h"

@implementation CustomUMShare


/**
 调用分享

 @param model 自定义的分享model
 @param completionBlock 回调结果:  1分享成功  2分享失败
 */
+(void)shareByCustomModel:(CustomShareModel*)model result:(ShareResult)completionBlock{
    
    @try {
        CustomShareModel * shareModel = model;
        if (shareModel.currentVC==nil) {
            if (TopNavc) {
                shareModel.currentVC = TopNavc.topViewController;
            }else{
                UIWindow * window = [UIApplication sharedApplication].keyWindow;
                if ([window.rootViewController isKindOfClass:[MainTabBarVC class]]) {
                    MainTabBarVC * mianVc = (MainTabBarVC*)window.rootViewController ;
                    shareModel.currentVC = mianVc;
                }else{
                    SHOWMSG(@"暂时无法调用分享!")
                    completionBlock(2); return;
                }
            }
        }
        
        // 0或1:微信好友  2:朋友圈  3:QQ好友  4:QQ空间  5:新浪微博
        switch (shareModel.sharePlatform) {
            case 1:{ shareModel.umSharePlatform = UMSocialPlatformType_WechatSession; } break;
            case 2:{ shareModel.umSharePlatform = UMSocialPlatformType_WechatTimeLine; } break;
            case 3:{ shareModel.umSharePlatform = UMSocialPlatformType_QQ; } break;
            case 4:{ shareModel.umSharePlatform = UMSocialPlatformType_Qzone; } break;
            case 5:{ shareModel.umSharePlatform = UMSocialPlatformType_Sina; } break;
            default: break;
        }
        
        // 1:链接 2:图文(文字放粘贴板) 3:纯文本 4:小程序 5:调用系统分享
        switch (shareModel.shareType) {
                
            case 1:{ // 链接
                
                [self shareWebUrl:shareModel result:^(NSInteger result) { completionBlock(result); }];
                
            } break;
                
            case 2:{ // 图文
                
                if (model.imgDataOrAry) {
                    if ([model.imgDataOrAry isKindOfClass:[NSArray class]]) {
                        NSArray * ary = (NSArray*)model.imgDataOrAry;
                        if (ary.count>0) { model.imgDataOrAry = ary[0]; }
                        else{ model.imgDataOrAry = [UIImage imageNamed:@"logo"]; }
                    }
                    [self shareImageOrTxt:shareModel result:^(NSInteger result) { completionBlock(result); }];
                }
                
            } break;
                
            case 3:{ // 纯文本
                
                [self shareImageOrTxt:shareModel result:^(NSInteger result) { completionBlock(result); }];
                
            } break;
                
            case 4:{ // 小程序 如果没有小程序，则分享链接
                
                //            if (SysShareType==1) { // 后台返回使用小程序
                if ([model.miniPreviewImg isKindOfClass:[UIImage class]]) {
                    UIImage * image = (UIImage*)model.miniPreviewImg;
                    NSData  * hdImageData = UIImageJPEGRepresentation(image, 1) ;
                    shareModel.miniPreviewImg =  hdImageData;
                    if (hdImageData.length/1000>127) {
                        NSData * imgData = UIImageJPEGRepresentation(image, 1);
                        NSUInteger length0 = imgData.length ; float fl0 = [FORMATSTR(@"%lu",(unsigned long)length0) floatValue]/1000;
                        NSLog(@"图片转Data压缩前分享的文件大小(内存以1000为单位):  约为：%.2fkb ",fl0);
                        float  bili = 1.0 , fl1 ;
                        for (int i=9; i<10;i--) {
                            bili = [FORMATSTR(@"%d",i) floatValue]/10 ;
                            imgData = UIImageJPEGRepresentation(image,bili);
                            fl1 = [FORMATSTR(@"%lu",(unsigned long)imgData.length) floatValue]/1000;
                            NSLog(@"压缩系数: %.2f   压缩后分享的文件大小约为: %.2fkb ",bili,fl1);
                            if (fl1<=127) {
                                NSLog(@"最终分享的文件大小约为: %.2fkb",fl1);
//                                hdImageData = imgData ;
                                shareModel.miniPreviewImg = imgData;
                                break ;
                            }
                        }
                    }
                }
                
                [self shareMiniProgram:shareModel result:^(NSInteger result) { completionBlock(result); }];
          
            } break;
                
            case 5:{ // 调用系统分享
                
                if ([shareModel.imgDataOrAry isKindOfClass:[UIImage class]]) {
                    shareModel.imgDataOrAry = @[shareModel.imgDataOrAry];
                }
                
                [self presentSystemShare:shareModel result:^(NSInteger result) { completionBlock(result); }];
                
            } break;
                
            default: break;
        }
    } @catch (NSException *exception) {
        SHOWMSG(@"分享操作失败!")
        completionBlock(2);
    } @finally { }


}



/**
 分享链接

 @param model model
 @param resultBlock 回调分享结果 1:成功 2失败
 */
+(void)shareWebUrl:(CustomShareModel*)model result:(void(^)(NSInteger result))resultBlock{
    
    UMSocialMessageObject * messageObject = [UMSocialMessageObject messageObject];
    UMShareWebpageObject * shareObject = [UMShareWebpageObject shareObjectWithTitle:model.share_title descr:model.share_describe thumImage:model.share_thumbImg];
    shareObject.webpageUrl = model.share_url;
    messageObject.shareObject = shareObject;
   
    [[UMSocialManager defaultManager] shareToPlatform:model.umSharePlatform messageObject:messageObject currentViewController:model.currentVC completion:^(id data, NSError *error) {
        if (error) { resultBlock(2); [self errorCode:error.code sharePlatform:model.umSharePlatform]; }
        else{ resultBlock(1); SHOWMSG(@"分享成功!") }
    }];
    
}



/**
 分享图片或文本

 @param model model
 @param resultBlock 回调分享结果 1:成功 2失败
 */
+(void)shareImageOrTxt:(CustomShareModel*)model result:(void(^)(NSInteger result))resultBlock{
    
    // 创建分享消息对象
    UMSocialMessageObject * messageObject = [UMSocialMessageObject messageObject];
    messageObject.text = model.share_text;
    if (model.shareType==2) { // 带有图片的
        UMShareImageObject * shObjc = [[UMShareImageObject alloc]init];
        [shObjc setShareImage:model.imgDataOrAry];
        messageObject.shareObject = shObjc;
    }
    
    [[UMSocialManager defaultManager] shareToPlatform:model.umSharePlatform messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        if (error) { resultBlock(2); [self errorCode:error.code sharePlatform:model.umSharePlatform]; }
        else{ resultBlock(1); SHOWMSG(@"分享成功!") }
    }];
    
}



/**
 分享小程序

 @param model model对象
 @param resultBlock 回调分享结果 1:成功 2失败
 */
+(void)shareMiniProgram:(CustomShareModel*)model result:(void(^)(NSInteger result))resultBlock{
    
    // 创建分享消息对象
    UMSocialMessageObject * messageObject = [UMSocialMessageObject messageObject];
    UMShareMiniProgramObject * shareObject = [UMShareMiniProgramObject shareObjectWithTitle:model.share_title descr:model.share_describe thumImage:model.share_thumbImg];
    shareObject.webpageUrl = model.share_url;
    shareObject.userName = model.miniUserName;
    shareObject.path = model.miniPath;
    messageObject.shareObject = shareObject;
    shareObject.hdImageData = model.miniPreviewImg ;
    shareObject.miniProgramType = model.miniProgramType ;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:UMSocialPlatformType_WechatSession messageObject:messageObject currentViewController:TopNavc.topViewController completion:^(id data, NSError *error) {
        if (error) { resultBlock(2); [self errorCode:error.code sharePlatform:UMSocialPlatformType_WechatSession]; }
        else{ resultBlock(1); SHOWMSG(@"分享成功!") }
    }];
   
}



/**
 调用系统分享

 @param model 对象(主要使用参数 imgDataOrAry [image类型] )
 */
+(void)presentSystemShare:(CustomShareModel*)model result:(void(^)(NSInteger result))resultBlock{
    
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSForegroundColorAttributeName:[UIColor darkGrayColor]};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    UIActivityViewController * activity = [[UIActivityViewController alloc] initWithActivityItems:(NSArray*)model.imgDataOrAry applicationActivities:nil];
    
    // 不需要的 系统默认的分享方式
    activity.excludedActivityTypes = @[UIActivityTypeMail,
                                       UIActivityTypePrint,
                                       UIActivityTypeAirDrop,
                                       UIActivityTypePostToVimeo,
                                       UIActivityTypePostToWeibo,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToTwitter,
                                       UIActivityTypePostToFacebook,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToTencentWeibo];
    
    UIActivityViewControllerCompletionWithItemsHandler shareBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
        
        // 导航栏标题
        UIBarButtonItem *item = [UIBarButtonItem appearance];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.001],
                                     NSForegroundColorAttributeName:[UIColor clearColor]};
        [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
        
        if (completed) {
            NSLog(@"\n\n\nactivityType: %@\n\nreturnedItems: %@\n\n",activityType,returnedItems);
            resultBlock(1);
        }else{
            resultBlock(2);
            NSLog(@"\n\n分享取消，取消或错误信息：\n%@\n\n",activityError);
        }
        
    };
    
    activity.completionWithItemsHandler = shareBlock;
    
    NSLog(@"承载的视图: %@",model.currentVC);
    
    // 判断设备进行页面弹出
    if ([[UIDevice currentDevice].model isEqualToString:@"iPhone"]) {
        [model.currentVC presentViewController:activity animated:YES completion:nil];
    } else if ( [[UIDevice currentDevice].model isEqualToString:@"iPad"] ) {
        UIPopoverPresentationController *popover = activity.popoverPresentationController;
        if (popover) {  popover.permittedArrowDirections = UIPopoverArrowDirectionUp; }
        [model.currentVC presentViewController:activity animated:YES completion:nil];
    } else { /*未知设备*/ }
    
}



+(void)errorCode:(NSInteger)code sharePlatform:(UMSocialPlatformType)type{
    switch (code) {
        case 2001:{ SHOWMSG(@"未配置白名单或分享平台软件版本过低!") }break;
        case 2002:{ SHOWMSG(@"授权失败，无法分享!") }break;
        case 2004:{ SHOWMSG(@"用户信息获取失败!") }break;
        case 2005:{ SHOWMSG(@"分享内容为空,请重试!")} break;
        case 2008:{
            switch (type) {
                case 1:{ SHOWMSG(@"没有安装微信哦!") } break; // 微信
                case 2:{ SHOWMSG(@"没有安装微信哦!") } break; // 朋友圈
                case 4:{ SHOWMSG(@"没有安装QQ哦!") } break; // QQ
                case 5:{ SHOWMSG(@"没有安装QQ哦!") } break; // QQ空间
                default: break;
            }
        } break;
        case 2009:{ SHOWMSG(@"分享已取消!") } break;
        case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试!") } break;
        case 2011:{ SHOWMSG(@"第三方App内部出错!") } break;
        case 2013:{ SHOWMSG(@"不支持该平台的分享!") } break;
        case 2014:{ SHOWMSG(@"第三方平台只支持https链接!") } break;
        default: {SHOWMSG(@"分享失败了!")} break;
    }
}



@end



@implementation CustomShareModel

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.share_url = @"";
        self.share_title = @"";
        self.share_describe = @"";
        self.share_thumbImg = [UIImage imageNamed:@"logo"];
        self.umSharePlatform = UMSocialPlatformType_WechatSession;

        
        self.share_text = @"";
        
        self.imgDataOrAry = [UIImage imageNamed:@"logo"];
        
    }
    return self;
}

// 小程序有关的，将服务器字段赋值给本地定义的分享界面
-(instancetype)initWith:(ServerShareModel*)smodel{
    self = [super init];
    if (self) {
        self.share_title = smodel.title;
        self.share_describe = smodel.des;
        self.share_url = smodel.webpageUrl;
        self.miniUserName = smodel.userName;
        self.miniPath = smodel.path;
    }
    return self;
}

@end



@implementation ServerShareModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.des = REPLACENULL(dic[@"des"]);
        self.path = REPLACENULL(dic[@"path"]);
        self.title = REPLACENULL(dic[@"title"]);
        self.userName = REPLACENULL(dic[@"userName"]);
        self.webpageUrl = REPLACENULL(dic[@"webpageUrl"]);
        self.programType = REPLACENULL(dic[@"programType"]);
        self.hdImageDate = REPLACENULL(dic[@"hdImageDate"]);
        self.shareWebUrl = REPLACENULL(dic[@"shareWebUrl"]);
    }
    return self;
}

@end











