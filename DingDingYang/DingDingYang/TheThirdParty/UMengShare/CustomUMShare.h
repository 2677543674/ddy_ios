//
//  CustomUMShare.h
//  DingDingYang
//
//  Created by ddy on 2018/4/9.
//

#import <Foundation/Foundation.h>

@class CustomShareModel, ServerShareModel;
@interface CustomUMShare : NSObject

typedef void (^ShareResult) (NSInteger result);
@property (copy, nonatomic) ShareResult resultBlock ;
+(void)shareByCustomModel:(CustomShareModel*)model result:(ShareResult)completionBlock;

@end


#pragma mark ===>>> 分享内容，整理成model，以model形式传值

@interface CustomShareModel : NSObject


// 记录将要跳转到分享的视图
@property(nonatomic,strong) UIViewController * currentVC;


/**
 分享类型:  1:链接 2:图片(有文字，文字默认放粘贴板) 3:纯文本 4:小程序 5:调用系统分享(图片)
 */
@property(nonatomic,assign) NSInteger shareType;


/**
 分享平台:  1:微信好友  2:朋友圈  3:QQ好友  4:QQ空间  5:新浪微博  6:微信小程序  8:系统分享
 */
@property(nonatomic,assign) NSInteger sharePlatform;
@property(nonatomic,assign) UMSocialPlatformType umSharePlatform;

//**  分享链接用的内容  **//
@property(nonatomic,copy) NSString * share_url;       // 分享的链接
@property(nonatomic,copy) NSString * share_title;     // 分享出去的标题
@property(nonatomic,copy) NSString * share_describe;  // 分享出去的描述信息
@property(nonatomic,strong)UIImage * share_thumbImg ; // 分享出去的缩略图


//**  分享文本用的内容 **//
@property(nonatomic,copy) NSString * share_text;      // 分享的纯文本


//**  分享图文用的内容，分享图片如需要附带文字，默认使用 share_text 传值 **//
@property(nonatomic,strong) id  imgDataOrAry; // 单个图片或数组


//**  分享小程序(低版本微信分享的是链接，使用上面链接的参数)  **//
@property(nonatomic,copy) NSString * miniPath; // 路径
@property(nonatomic,copy) NSString * miniUserName; // 小程序名
@property(nonatomic,copy) NSString * miniWebpageUrl; // 兼容低版本的url
@property(nonatomic,strong) id       miniPreviewImg; // 分享的图片(img、或data)
@property(nonatomic,assign) NSInteger miniProgramType; // 小程序版本(0:正式版 1:开发板 2:体验版)

-(instancetype)initWith:(ServerShareModel*)smodel;

@end


#pragma mark ===>>> 服务器返回的分享内容model
@interface ServerShareModel : NSObject

@property(nonatomic,copy) NSString * des; // 描述信息
@property(nonatomic,copy) NSString * path; // 路径
@property(nonatomic,copy) NSString * title; // 标题
@property(nonatomic,copy) NSString * userName; // 小程序name
@property(nonatomic,copy) NSString * webpageUrl; // web链接
@property(nonatomic,copy) NSString * programType; // 小程序类型
@property(nonatomic,copy) NSString * hdImageDate; // 小程序码(图片链接)
@property(nonatomic,copy) NSString * shareWebUrl; // 集合页的分享
@property(nonatomic,copy) UIImage  * codeImageDate; // 小程序码下载完成后的图片
-(instancetype)initWithDic:(NSDictionary*)dic;

@end














