//
//  WqeView.h
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>



/****************************************************************************/
#pragma mark -- > 分享好友注册界面
//微信、qq，二维码 按钮创建
@protocol WqeDeledate <NSObject>
-(void)selectIndex:(NSInteger)way;
@end
@interface WqeView : UIView
@property(nonatomic,assign)id<WqeDeledate>deledate;
@end

/****************************************************************************/
#pragma mark -- > 生成海报界面分享
@protocol PtshDeledate <NSObject>
-(void)selectIndex:(NSInteger)way;
@end
@interface PostersShareView : UIView
@property(nonatomic,assign)id<PtshDeledate>deledate;
@end


/****************************************************************************/
#pragma mark -- 更多分享方式
@interface MoreWay : UIView <UIGestureRecognizerDelegate>
@property(nonatomic,strong)void(^index)(NSInteger way);
@property(nonatomic,strong)UIView*bgView;
@property(nonatomic,assign)float height;//弹出框的高度
@property(nonatomic,copy)NSString*urlStr;
@property(nonatomic,strong)UIImageView*ewmImgV;
    
//更多方式选择(分享注册界面)
+(void)showSpView:(UIView*)spV height:(float)hi andSelect:(void(^)(NSInteger way))index;
//弹出二维码界面
+(void)showEWMView:(UIView *)spV content:(NSString*)url height:(float)hi;
//更多方式选择(分享海报界面)
+(void)addPostersView:(UIView*)spV height:(float)height andSelect:(void(^)(NSInteger way))index;
//京东分享
+(void)showSpView:(UIView*)spV height:(float)hi isJD:(BOOL)isJD andSelect:(void(^)(NSInteger way))index ;
@end


/****************************************************************************/
//带有图片和文字的按钮
@interface ShareButton : UIButton
-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName;
@property(nonatomic,strong)UIImageView*imgView;
@property(nonatomic,strong)UILabel*titleLab;
@property(nonatomic,copy)NSString*ImgName,*TitleName;

@end
