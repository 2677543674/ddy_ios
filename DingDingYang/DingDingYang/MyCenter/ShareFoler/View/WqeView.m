//
//  WqeView.m
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "WqeView.h"

#pragma mark === >>> 分享链接给好友
/****************************************************************************/
@implementation WqeView
-(instancetype)init{
    self=[super init];
    if (self) {
        
        self.backgroundColor=[UIColor groupTableViewBackgroundColor];
        NSArray*aryImg=@[@"weixin",@"qq",@"erweima"];
        NSArray*aryTit=@[@"微信好友群",@"手机QQ",@"面对面分享"];
        for (NSInteger i=0; i<3; i++) {
            ShareButton*btn=[[ShareButton alloc]initWithImgName:aryImg[i] titleName:aryTit[i]];
            btn.tag=i+1;
            btn.frame=CGRectMake(ScreenW/3*i,20, ScreenW/3, 66);
            [btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
        }
        UIButton*moreBtn=[UIButton titLe:@"更多邀请方式" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGRAY font:14*HWB];
        moreBtn.layer.cornerRadius=6;
        [moreBtn addShadowLayer];
        [moreBtn addTarget:self action:@selector(moreWay) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:moreBtn];
        [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(25);   make.right.offset(-25);
            make.height.offset(40);  make.top.offset(108);
        }];
        
        if (THEMECOLOR==2) { //主题色为浅色
             [moreBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }
        
    }
    return self;
}

-(void)click:(ShareButton*)btn{
    [self.deledate selectIndex:btn.tag];
}
-(void)moreWay{
    [self.deledate selectIndex:4];
}

@end


#pragma mark === >>> 分享海报给好友
@implementation PostersShareView

-(instancetype)init{
    self=[super init];
    if (self) {
        self.backgroundColor=[UIColor groupTableViewBackgroundColor];
        NSArray*aryImg=@[@"weixin",@"qq",@"new1_baocun"];
        NSArray*aryTit=@[@"微信好友",@"QQ好友",@"保存到相册"];
        for (NSInteger i=0; i<3; i++) {
            ShareButton*btn=[[ShareButton alloc]initWithImgName:aryImg[i] titleName:aryTit[i]];
            btn.tag=i+1;
            btn.frame=CGRectMake(ScreenW/3*i,16*(ScreenW/320), ScreenW/3, 66);
            [btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
        }
        
        UIButton*moreBtn=[UIButton titLe:@"更多邀请方式" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGRAY font:FONT_16];
        moreBtn.layer.cornerRadius=6;
        [moreBtn addShadowLayer];
        [moreBtn addTarget:self action:@selector(moreWay) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:moreBtn];
        [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(25); make.right.offset(-25);
            make.height.offset(40); make.top.offset(66+40*((ScreenW/320)));
        }];

        if (THEMECOLOR==2) { //主题色为浅色
            [moreBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }

    }
    return self;
}
    
-(void)click:(ShareButton*)btn{
    [self.deledate selectIndex:btn.tag];
}
-(void)moreWay{
    [self.deledate selectIndex:4];
}


@end



/****************************************************************************/

@implementation MoreWay
#pragma mark == >> 更多分享操作 分享好友注册


+(void)showSpView:(UIView *)spV height:(float)hi isJD:(BOOL)isJD andSelect:(void (^)(NSInteger))index{
    MoreWay*more=[[MoreWay alloc]init];
    more.index=index;
    more.height=hi;
    [spV addSubview:more];
    [more setJDSubviews];
    [more mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);    make.left.offset(0);
        make.right.offset(0);  make.bottom.offset(0);
    }];
    
   
}

-(void)setJDSubviews{
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0,ScreenH, ScreenW, _height)];
    _bgView.backgroundColor = COLORWHITE;
    [self addSubview:_bgView];
    
    NSArray*aryImg=@[@"微信登录",@"new1_share_pyq",@"qq-1",@"qqkongjian"];
    NSArray*aryTit=@[@"微信",@"朋友圈",@"QQ好友",@"QQ空间"];
    for (NSInteger i=0; i<aryTit.count; i++) {
        ShareButton*btn=[[ShareButton alloc]initWithImgName:aryImg[i] titleName:aryTit[i]];
        btn.tag=i+1;
        btn.frame=CGRectMake(ScreenW/aryTit.count*i,15, ScreenW/aryTit.count,50);
        [btn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:btn];
    }
}


//更多分享操作(分享好友注册)
+(void)showSpView:(UIView *)spV height:(float)hi andSelect:(void (^)(NSInteger))index{
    MoreWay*more=[[MoreWay alloc]init];
    more.index=index;
    more.height=hi;
    [spV addSubview:more];
    [more setSubviews];
    [more mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);    make.left.offset(0);
        make.right.offset(0);  make.bottom.offset(0);
    }];
}
-(void)setSubviews{
    _bgView = [[UIView alloc] initWithFrame:CGRectZero];
    _bgView.backgroundColor = COLORWHITE;
    [self addSubview:_bgView];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(_height);  make.left.offset(0);
        make.right.offset(0);   make.top.equalTo(self.mas_bottom).offset(0);
    }];
    NSArray*aryImg=@[@"qqkongjian",@"new1_share_pyq",@"sina",@"copy"];
    NSArray*aryTit=@[@"QQ空间",@"朋友圈",@"新浪微博",@"复制链接"];
   
    if (IsNeedSinaWeb==2) {
        aryImg=@[@"qqkongjian",@"new1_share_pyq",@"copy"];
        aryTit=@[@"QQ空间",@"朋友圈",@"复制链接"];
    }
    
    for (NSInteger i=0; i<aryTit.count; i++) {
        ShareButton*btn=[[ShareButton alloc]initWithImgName:aryImg[i] titleName:aryTit[i]];
        btn.tag=i+1;
        btn.frame=CGRectMake(ScreenW/aryTit.count*i,15, ScreenW/aryTit.count,50);
        [btn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:btn];
    }
}
-(void)clickShareBtn:(ShareButton*)btn{
    [self removeSelfAction];
    self.index(btn.tag);
}

#pragma mark == >> 更多分享操作 分享好友海报
+(void)addPostersView:(UIView *)spV height:(float)height andSelect:(void (^)(NSInteger))index{
    MoreWay*more=[[MoreWay alloc]init];
    more.index=index;
    more.height=height;
    [spV addSubview:more];
    [more addPostersShareView];
    [more mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);    make.left.offset(0);
        make.right.offset(0);  make.bottom.offset(0);
    }];
}
-(void)addPostersShareView{
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0,ScreenH-NAVCBAR_HEIGHT, ScreenW, _height)];
    _bgView.backgroundColor = COLORWHITE;
    [self addSubview:_bgView];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(_height);   make.left.offset(0);
        make.right.offset(0); make.top.equalTo(self.mas_bottom).offset(0);
    }];
    
    NSArray*aryImg=@[@"qqkongjian",@"new1_share_pyq",@"sina"];
    NSArray*aryTit=@[@"QQ空间",@"朋友圈",@"新浪微博"];
    if (IsNeedSinaWeb==2) {
        aryImg=@[@"qqkongjian",@"new1_share_pyq"];
        aryTit=@[@"QQ空间",@"朋友圈"];
    }
    for (NSInteger i=0; i<aryTit.count; i++) {
        ShareButton*btn=[[ShareButton alloc]initWithImgName:aryImg[i] titleName:aryTit[i]];
        btn.tag=i+1;
        btn.frame=CGRectMake(ScreenW/aryTit.count*i,15, ScreenW/aryTit.count,50);
        [btn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:btn];
    }
}
    
#pragma mark === >>> 显示二维码分享
+(void)showEWMView:(UIView *)spV content:(NSString*)url height:(float)hi{
    MoreWay*more=[[MoreWay alloc]init];
    more.height=hi;
    more.urlStr=url;
    [spV addSubview:more];
    [more mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);    make.left.offset(0);
        make.right.offset(0);  make.bottom.offset(0);
    }];
    [more creatEWMJieMain];
    
}
-(void)creatEWMJieMain{
    _bgView = [[UIView alloc] initWithFrame:CGRectZero];
    _bgView.backgroundColor = COLORWHITE;
    [self addSubview:_bgView];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(0);  make.left.offset(0);
        make.right.offset(0); make.bottom.offset(0);
    }];
    
    UILabel*tishi=[UILabel labText:@"让朋友扫码领取APP登录资格" color:Black102 font:14.0];
    tishi.frame=CGRectMake(0, 0, ScreenW, 40);
    tishi.textAlignment=NSTextAlignmentCenter;
    [_bgView addSubview:tishi];
    
    _ewmImgV=[[UIImageView alloc]init];
    _ewmImgV.frame=CGRectMake(ScreenW/4, 42, ScreenW/2, ScreenW/2);
    [_bgView addSubview:_ewmImgV];
    
    UIButton*cbtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [cbtn setTitle:@"取消" forState:UIControlStateNormal];
    [cbtn setTitleColor:Black102 forState:UIControlStateNormal];
    cbtn.titleLabel.font=Font14;
    [cbtn addTarget:self action:@selector(removeSelfAction) forControlEvents:UIControlEventTouchUpInside];
    cbtn.frame=CGRectMake(0, 44+ScreenW/2, ScreenW, 40);
    [_bgView addSubview:cbtn];
    
    [self gettuigaungErweima];
}

#pragma mark == >> 生成二维码
-(void)gettuigaungErweima{
    // 1.创建过滤器
    CIFilter*filter=[CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    // 3.给过滤器添加数据
    NSString *dataString =_urlStr;
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKeyPath:@"inputMessage"];
    // 4.获取输出的二维码
    CIImage *outputImage = [filter outputImage];
    _ewmImgV.image = [self createNonInterpolatedUIImageFormCIImage:outputImage withSize:1000];
}

//将二维码CIImage转换成UIImage
- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}

//初始化大小
- (instancetype)init{
    self = [super init];
    if (self) {  }
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.0];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSelfAction)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    [self performSelector:@selector(showSelfAction) withObject:nil afterDelay:0.1];
    return self;
}

//显示
-(void)showSelfAction{
    [UIView animateWithDuration:0.26 animations:^{
        _bgView.frame = CGRectMake(0,ScreenH-NAVCBAR_HEIGHT-_height, ScreenW, _height) ;
        [_bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);    make.right.offset(0);
            make.bottom.offset(0);  make.height.offset(_height);
        }];
         [_bgView.superview layoutIfNeeded];
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    }];
}
//移除
-(void)removeSelfAction{
    [UIView animateWithDuration:0.26 animations:^{
        [_bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(_height);  make.left.offset(0);
            make.right.offset(0);  make.top.equalTo(self.mas_bottom).offset(0);
        }];
        [_bgView.superview layoutIfNeeded];
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.0];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
#pragma mark --> tap  delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_bgView.frame, point)) {
        return NO;
    }
    return YES;
}
@end


/****************************************************************************/
@implementation ShareButton
-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName{
    self=[super init];
    if (self) {
        _imgView=[[UIImageView alloc]init];
        _imgView.image=[UIImage imageNamed:ImgName];
        _imgView.layer.masksToBounds=YES;
        [self addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.offset(-16);
            make.top.offset(0);
            make.width.equalTo(_imgView.mas_height);
        }];
        
        _titleLab=[[UILabel alloc]init];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.text=titleName; _titleLab.font=[UIFont systemFontOfSize:12.0*HWB];
        _titleLab.textColor=Black51;
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(13);
            make.bottom.offset(0);
            make.centerX.equalTo(self.mas_centerX);
        }];
    }return self;
}
@end
