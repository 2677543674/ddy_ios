//
//  ABShareCell.m
//  DingDingYang
//
//  Created by ddy on 2017/3/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ABShareCell.h"

@implementation ABShareCell

-(void)setModel:(ABSModel*)model{
    _model=model;
    _nameLab.text=model.name;
    _phoneLab.text=model.phone;
    [_shareBtn setTitle:@"邀请" forState:UIControlStateNormal];
    [_shareBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    _shareBtn.userInteractionEnabled=YES;
    
    [_checkBtn setImage:[[UIImage imageNamed:@"check_no"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]  forState:UIControlStateSelected];
    _checkBtn.userInteractionEnabled=YES;
    NSMutableArray*saveFriendPhone=NSUDTakeData(@"saveFriendPhone");
    for (int i=0; i<saveFriendPhone.count; i++) {
        if ([saveFriendPhone[i] isEqualToString:model.phone]) {
            [_shareBtn setTitle:@"已发送" forState:UIControlStateNormal];
            [_shareBtn setTitleColor:Black102 forState:UIControlStateNormal];
            _shareBtn.userInteractionEnabled=NO;
            [_checkBtn setImage:[UIImage imageNamed:@"check_no"] forState:UIControlStateNormal];
            _checkBtn.userInteractionEnabled=NO;
            break;
        }
    }
    if (model.roleName.length>0) {
        [_shareBtn setTitle:model.roleName forState:UIControlStateNormal];
        _shareBtn.userInteractionEnabled=NO;
        _checkBtn.userInteractionEnabled=NO;
    }
    if ([_shareBtn.titleLabel.text isEqualToString:@"已发送"]) {
        _shareBtn.userInteractionEnabled=NO;
        _checkBtn.userInteractionEnabled=NO;
    }
    
    
    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        [self checkBtn];
        
        [self nameLab];
        
        [self phoneLab];
        
        [self shareBtn];
    }
    return self;
}

-(UIButton *)checkBtn{
    if (!_checkBtn) {
        _checkBtn=[UIButton new];
        [_checkBtn setImage:[UIImage imageNamed:@"check_no"] forState:UIControlStateNormal];
        //一样的图片 改变颜色
        [_checkBtn setImage:[[UIImage imageNamed:@"check_no"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
        [self addSubview:_checkBtn];
        [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.offset(0);
            make.left.offset(0);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(_checkBtn.mas_height);
        }];
    }
    return _checkBtn;
}

-(UILabel*)nameLab{
    if (!_nameLab) {
        _nameLab=[UILabel labText:@"姓名" color:Black51 font:13*HWB];
        _nameLab.adjustsFontSizeToFitWidth=NO;
        _nameLab.lineBreakMode=NSLineBreakByTruncatingMiddle;
        [self addSubview:_nameLab];
        [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_checkBtn.mas_right).offset(0);
            make.centerY.equalTo(self.mas_centerY);
            make.width.offset(70*HWB);
        }];
    }
    return _nameLab ;
}

-(UILabel*)phoneLab{
    if (!_phoneLab) {
        _phoneLab=[UILabel labText:@"电话号码" color:Black51 font:15];
        [self addSubview:_phoneLab];
        [_phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLab.mas_right).offset(10);
            make.centerY.equalTo(self.mas_centerY);
            make.height.offset(20);
        }];
    }
    return _phoneLab ;
}

-(UIButton*)shareBtn{
    if (!_shareBtn) {

        _shareBtn = [UIButton titLe:@"邀请" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR  font:13.5*HWB];
        _shareBtn.layer.cornerRadius = 3 ;
        [self addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10); make.height.offset(33);
            make.width.offset(56);
            make.centerY.equalTo(self.mas_centerY);
        }];
    }
    return _shareBtn ;
}



@end


@implementation ABSModel

-(instancetype)initWith:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        self.name=REPLACENULL(dic[@"name"]);
        self.ifSend=REPLACENULL(dic[@"ifSend"]);
        self.phone=REPLACENULL(dic[@"number"]);
        self.role=[REPLACENULL(dic[@"role"]) integerValue];
        self.roleName=REPLACENULL(dic[@"roleName"]);
        self.level=REPLACENULL(dic[@"level"]);
    }
    return self;
}

@end




