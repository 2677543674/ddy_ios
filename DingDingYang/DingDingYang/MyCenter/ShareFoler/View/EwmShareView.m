//
//  EwmShareView.m
//  DingDingYang
//
//  Created by ddy on 2017/12/12.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "EwmShareView.h"

@implementation EwmShareView

-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = COLORGROUP ;
        _ewmImgV = [[UIImageView alloc]init];
        _ewmImgV.backgroundColor = COLORGROUP ;
        _ewmImgV.userInteractionEnabled = YES ;
        [self addSubview:_ewmImgV];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fangBigg:)];
        [_ewmImgV  addGestureRecognizer:tap];
        [_ewmImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(6);   make.top.offset(6);
            make.bottom.offset(-6); make.right.offset(-6);
        }];
    }
    return self ;
}

-(void)fangBigg:(UIGestureRecognizer*)tapImg{
    [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[_ewmImgV.image]];
}
- (void)drawRect:(CGRect)rect {
    float hw = self.frame.size.width ;
    if (hw==0) { return ; }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetLineWidth(context, 2) ;
    
    [RGBA(239, 196, 206, 1) set];
    CGContextSetLineCap(context, kCGLineCapRound);
    
    //左上角
    CGContextMoveToPoint(context, 1, 12); //起点1
    CGContextAddLineToPoint(context, 1, 1); //第二点坐标
    CGContextAddLineToPoint(context, 12, 1); //第三点坐标
    
    //右上角
    CGContextMoveToPoint(context, hw-12, 1);
    CGContextAddLineToPoint(context, hw-1, 1);
    CGContextAddLineToPoint(context, hw-1, 12);
    
    //右下角
    CGContextMoveToPoint(context, hw-1, hw-12);
    CGContextAddLineToPoint(context, hw-1, hw-1);
    CGContextAddLineToPoint(context, hw-12, hw-1);
    
    //左下角
    CGContextMoveToPoint(context, 1, hw-12);
    CGContextAddLineToPoint(context, 1, hw-1);
    CGContextAddLineToPoint(context, 12, hw-1);
    
    CGContextStrokePath(context);
}


@end





