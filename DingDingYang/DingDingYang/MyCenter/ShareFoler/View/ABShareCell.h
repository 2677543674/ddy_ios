//
//  ABShareCell.h
//  DingDingYang
//
//  Created by ddy on 2017/3/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class ABSModel;
@interface ABShareCell : UITableViewCell
@property(nonatomic,strong)UILabel*nameLab;
@property(nonatomic,strong)UILabel*phoneLab;
@property(nonatomic,strong)UIButton*shareBtn;
@property(nonatomic,strong)UIButton*checkBtn;
@property(nonatomic,strong)ABSModel*model;
@end



@interface ABSModel : NSObject
@property(nonatomic,copy)NSString*name;
@property(nonatomic,copy)NSString*phone;
@property(nonatomic,copy)NSString*ifSend;//是否发送
@property(nonatomic,assign)NSInteger role;//级别(-1非系统会员0消费者1：代理商 2:联合运营商)
//@property(nonatomic,assign)NSInteger type;
@property(nonatomic,copy)NSString*roleName;
@property(nonatomic,copy)NSString*level;
-(instancetype)initWith:(NSDictionary*)dic;
@end




