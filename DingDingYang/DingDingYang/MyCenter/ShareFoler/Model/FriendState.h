//
//  FriendState.h
//  DingDingYang
//
//  Created by ddy on 2018/4/3.
//

#import <Foundation/Foundation.h>

@interface FriendState : NSObject
@property(nonatomic,copy)NSString* mobile;
@property(nonatomic,copy)NSString* roleName;
@property(nonatomic,copy)NSString* level;

-(instancetype)initWith:(NSDictionary*)dic;
@end
