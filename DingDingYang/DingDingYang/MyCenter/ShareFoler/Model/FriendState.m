//
//  FriendState.m
//  DingDingYang
//
//  Created by ddy on 2018/4/3.
//

#import "FriendState.h"

@implementation FriendState
-(instancetype)initWith:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        self.mobile=REPLACENULL(dic[@"mobile"]);
        self.level=REPLACENULL(dic[@"level"]);
        self.roleName=REPLACENULL(dic[@"roleName"]);
    }
    return self;
}
@end
