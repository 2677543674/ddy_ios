//
//  SharePopoverVC.m
//  DingDingYang
//
//  Created by ddy on 2017/12/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SharePopoverVC.h"
#import "CompositePosterImg.h"

@interface SharePopoverVC () <UIGestureRecognizerDelegate>
@property(nonatomic,strong) UIView * backView ;
@property(nonatomic,strong) ShareBtnRegister * shareBtn ;
@property(nonatomic,strong) NSArray * imgAry , * titleAry , *tagAry;
@end

@implementation SharePopoverVC


-(void)shareVideo:(UMSocialPlatformType)SocialPlatformType{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建视频内容对象
    UMShareVideoObject *shareObject = [UMShareVideoObject shareObjectWithTitle:@"分享标题" descr:@"分享内容描述" thumImage:[UIImage imageNamed:@"icon"]];
    //设置视频网页播放地址
    shareObject.videoUrl = self.videoUrl;
    
    //    @"http://video.sina.com.cn/p/sports/cba/v/2013-10-22/144463050817.html";
    //            shareObject.videoStreamUrl = @"这里设置视频数据流地址（如果有的话，而且也要看所分享的平台支不支持）";
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:SocialPlatformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            LOG(@"小程序分享出错:\n\n%@",error)
            switch (error.code) {
                case 2001:{ SHOWMSG(@"未配置白名单或分享平台软件版本过低!") }break;
                case 2002:{ SHOWMSG(@"授权失败，无法分享!") }break;
                case 2004:{ SHOWMSG(@"用户信息获取失败!") }break;
                case 2005:{ SHOWMSG(@"分享内容为空,请重试!")} break;
                case 2008:{ SHOWMSG(@"亲，你还没有安装对应平台的软件哦!") } break;
                case 2009:{ SHOWMSG(@"分享已取消!") } break;
                case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试!") } break;
                case 2011:{ SHOWMSG(@"第三方App内部出错!") } break;
                case 2013:{ SHOWMSG(@"不支持该平台的分享!") } break;
                case 2014:{ SHOWMSG(@"第三方平台只支持https链接!") } break;
                default: {SHOWMSG(@"分享失败了!")} break;
            }
        }else{
            
            SHOWMSG(@"分享成功!")
        }
    }];
}


//分享视频
+(void)shareVideoWithURL:(NSString *)videoUrl{
    
    SharePopoverVC * sharePopover = [[SharePopoverVC alloc]init];
    
    sharePopover.videoUrl = videoUrl;
    
   [TopNavc.topViewController presentViewController:sharePopover animated:NO completion:nil];
}

// 分享小程序 ( 只能分享到微信好友 UMSocialPlatformType_WechatSession )
+(void)shareMiniProgramByGoodsId:(GoodsModel*)goodsMd{
    
    MBShow(@"正在加载...")
    [NetRequest requestTokenURL:url_mini_program_check parameter:nil success:^(NSDictionary *nwdic) {
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
            
            if ([REPLACENULL(nwdic[@"ifHave"]) integerValue]==1) { // 当前app开通了小程序
                
                [NetRequest requestTokenURL:url_mini_program_share parameter:@{@"goodsId":goodsMd.goodsId} success:^(NSDictionary *nwdic) {
                    
                    if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                        
                        SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
                        [downloader downloadImageWithURL:[NSURL URLWithString:goodsMd.goodsPic]  options:SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *  targetURL) {
                            
                        } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                            MBHideHUD
                            if (image!=nil) {
                                
                                NSLog(@"接收到的Data: %.2f",[FORMATSTR(@"%lu",(unsigned long)data.length) floatValue]/1000);
                                NSData  * hdImageData = data ;
                                if (data.length/1000>127) {
                                    NSData * imgData = UIImageJPEGRepresentation(image, 1);
                                    NSUInteger length0 = imgData.length ; float fl0 = [FORMATSTR(@"%lu",(unsigned long)length0) floatValue]/1000;
                                    NSLog(@"图片转Data压缩前分享的文件大小(内存以1000为单位):  约为：%.2fkb ",fl0);
                                    float  bili = 1.0 , fl1 ;
                                    for (int i=9; i<10;i--) {
                                        bili = [FORMATSTR(@"%d",i) floatValue]/10 ;
                                        imgData = UIImageJPEGRepresentation(image,bili);
                                        fl1 = [FORMATSTR(@"%lu",(unsigned long)imgData.length) floatValue]/1000;
                                        NSLog(@"压缩系数: %.2f   压缩后分享的文件大小约为: %.2fkb ",bili,fl1);
                                        if (fl1<=127) {
                                            NSLog(@"最终分享的文件大小约为: %.2fkb",fl1);
                                            hdImageData = imgData ;
                                            break ;
                                        }
                                    }
                                    
                                }
                              
                                // 创建分享消息对象
                                UMSocialMessageObject * messageObject = [UMSocialMessageObject messageObject];
                                
                                UMShareMiniProgramObject * shareObject = [UMShareMiniProgramObject shareObjectWithTitle:goodsMd.goodsName descr:SHARECONTENT thumImage:[UIImage imageNamed:@"logo"]];
                                shareObject.webpageUrl = REPLACENULL(nwdic[@"bean"][@"path"]) ;
                                shareObject.userName = REPLACENULL(nwdic[@"bean"][@"userName"]);
                                shareObject.path = REPLACENULL(nwdic[@"bean"][@"path"]);
                                messageObject.shareObject = shareObject;
                                shareObject.hdImageData = hdImageData ;
                                shareObject.miniProgramType = UShareWXMiniProgramTypeRelease ; // 可选体验版和开发板
                                
                                //调用分享接口
                                [[UMSocialManager defaultManager] shareToPlatform:UMSocialPlatformType_WechatSession messageObject:messageObject currentViewController:TopNavc.topViewController completion:^(id data, NSError *error) {
                                    if (error) {
                                        LOG(@"小程序分享出错:\n\n%@",error)
                                        switch (error.code) {
                                            case 2001:{ SHOWMSG(@"未配置白名单或分享平台软件版本过低!") }break;
                                            case 2002:{ SHOWMSG(@"授权失败，无法分享!") }break;
                                            case 2004:{ SHOWMSG(@"用户信息获取失败!") }break;
                                            case 2005:{ SHOWMSG(@"分享内容为空,请重试!")} break;
                                            case 2008:{ SHOWMSG(@"亲，你还没有安装对应平台的软件哦!") } break;
                                            case 2009:{ SHOWMSG(@"分享已取消!") } break;
                                            case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试!") } break;
                                            case 2011:{ SHOWMSG(@"第三方App内部出错!") } break;
                                            case 2013:{ SHOWMSG(@"不支持该平台的分享!") } break;
                                            case 2014:{ SHOWMSG(@"第三方平台只支持https链接!") } break;
                                            default: {SHOWMSG(@"分享失败了!")} break;
                                        }
                                    }else{
                                        
                                        SHOWMSG(@"分享成功!")
                                    }
                                }];
                            }else{
                                MBHideHUD
                                SHOWMSG(@"商品图片获取失败!")
                            }
                        }];
                        
                    }else{
                        MBHideHUD
                        SHOWMSG(nwdic[@"result"])
                    }
                    
                } failure:^(NSString *failure) {
                    MBHideHUD
                    SHOWMSG(failure)
                }];
            }
            
        }else{
            MBHideHUD
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
    }];
    
    
    
}


// 调用友盟分享链接
+(void)shareUrl:(NSString*)url title:(NSString*)urlTitle des:(NSString*)des thubImg:(UIImage*)img shareType:(NSInteger)type{
    SharePopoverVC * sharePopover = [[SharePopoverVC alloc]init];
    sharePopover.share_url = url;
    sharePopover.share_title = urlTitle ;
    sharePopover.share_describe = des ;
    sharePopover.share_thumbImg = img ;
    sharePopover.share_type = type ;
    [TopNavc.topViewController presentViewController:sharePopover animated:NO completion:nil];
}


+(void)shareWPHGoodsModel:(GoodsModel*)model thubImg:(UIImage*)img shareType:(NSInteger)type{
    SharePopoverVC * sharePopover = [[SharePopoverVC alloc]init];
    sharePopover.goodsModel=model;
    sharePopover.share_thumbImg = img ;
    sharePopover.share_type = type ;
    [TopNavc.topViewController presentViewController:sharePopover animated:NO completion:nil];
}

// 调用系统分享
+(void)openSystemShareImage:(NSMutableArray*)imageAry{
    SharePopoverVC * sharePopover = [[SharePopoverVC alloc]init];
    
    if (imageAry.count>0) {
        id img = imageAry[0];
        if ([img isKindOfClass:[UIImage class]]) {
            [sharePopover presentSystemShare:imageAry];
        }else if ([img isKindOfClass:[NSString class]]){
            NSMutableArray * share_imgAry = [NSMutableArray array];
            MBShow(@"正在加载")
            for (NSInteger i=0; i<imageAry.count; i++) {
                NSLog(@"%@",imageAry[i]);
                NSString * encodingString = [imageAry[i] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
                [downloader downloadImageWithURL:[NSURL URLWithString:encodingString]  options:SDWebImageDownloaderScaleDownLargeImages progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                    NSLog(@"%@",image);
                    NSLog(@"%@",image);
                    NSLog(@"错误:%@",error);
                    if (image!=nil) {
                        [share_imgAry addObject:image];
                        if (share_imgAry.count==imageAry.count) {
                            MBHideHUD;
                            [sharePopover presentSystemShare:share_imgAry];
                        }
                    }
                }];
            }
        }
    }else{
        NSLog(@"图片链接为空。。。。");
    }
}

#pragma mark === >>> 调用系统分享
-(void)presentSystemShare:(NSMutableArray*)dataImgAry{

    
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSForegroundColorAttributeName:[UIColor darkGrayColor]};
    
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:dataImgAry applicationActivities:nil];

    //不显示系统默认的分享方式
    activity.excludedActivityTypes = @[UIActivityTypeAirDrop,
                                       UIActivityTypePostToFacebook,
                                       UIActivityTypePostToTwitter,
                                       UIActivityTypePostToWeibo,
                                       UIActivityTypeMessage,
                                       UIActivityTypeMail,
                                       UIActivityTypePrint,
                                       UIActivityTypeCopyToPasteboard,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo,
                                       UIActivityTypePostToTencentWeibo];
    
    UIActivityViewControllerCompletionWithItemsHandler shareBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
        // 导航栏标题
        UIBarButtonItem *item = [UIBarButtonItem appearance];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.001],
                                     NSForegroundColorAttributeName:[UIColor clearColor]};
        [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
        
        if (completed) {
            NSLog(@"\n\n\nactivityType: %@\n\nreturnedItems: %@\n\n",activityType,returnedItems);
//            plugin com.tencent.mqq.ShareExtension invalidated
        }else{
            NSLog(@"\n\n分享取消，取消或错误信息：\n%@\n\n",activityError);
        }
        
    };
    
    activity.completionWithItemsHandler = shareBlock;
    
    // 判断设备进行页面弹出
    if ([[UIDevice currentDevice].model isEqualToString:@"iPhone"]) {
        
        [TopNavc presentViewController:activity animated:YES completion:nil];
        
    } else if([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
        
        UIPopoverPresentationController *popover = activity.popoverPresentationController;
        if (popover) {
            popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
        }
        [TopNavc presentViewController:activity animated:YES completion:nil];
        
    } else {
        // 未知设备
    }
    
}



-(instancetype)init{
    self=[super init];
    if (self) {  self.modalPresentationStyle = UIModalPresentationCustom ;  }return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"分享" ;
    
    
    if (PID==10104) {
        _share_title = APPNAME ;
        _share_thumbImg = [UIImage imageNamed:LOGONAME];
    }
    
    self.view.backgroundColor = RGBA(0,0, 0, 0.01);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSelf)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
   
    [self shareRegist];
    
    [self performSelector:@selector(showSelf) withObject:self afterDelay:0.1];
}

//-(void)setType:(NSString *)type{
//    _type=type;
//    [self shareRegist];
//}

//分享好友注册
-(void)shareRegist{
   
    if (IsNeedSinaWeb==2) {
        _imgAry = @[@"new1_share_pyq",@"new1_share_wx",@"new1_share_qq",@"new1_share_qqkj"] ;
        _titleAry = @[@"朋友圈",@"微信",@"QQ",@"QQ空间"] ;
        _tagAry   = @[@"0",@"1",@"2",@"3"];
    }else{
        _imgAry = @[@"new1_share_pyq",@"new1_share_wx",@"new1_share_wb",@"new1_share_qq",@"new1_share_qqkj"] ;
        _titleAry = @[@"朋友圈",@"微信",@"新浪微博",@"QQ",@"QQ空间"] ;
        _tagAry   = @[@"0",@"1",@"4",@"2",@"3"];
    }
    
    if (_share_type==2||_share_type==3) {// 3是唯品会
        if (IsNeedSinaWeb==2) {
            _imgAry = @[@"new1_share_pyq",@"new1_share_wx",@"new1_share_qq",@"new1_share_qqkj",@"new1_copy"] ;
            _titleAry = @[@"朋友圈",@"微信",@"QQ",@"QQ空间",@"复制链接"] ;
            _tagAry   = @[@"0",@"1",@"2",@"3",@"5"];
        }else{
            _imgAry = @[@"new1_share_pyq",@"new1_share_wx",@"new1_share_wb",@"new1_share_qq",@"new1_share_qqkj",@"new1_copy"] ;
            _titleAry = @[@"朋友圈",@"微信",@"新浪微博",@"QQ",@"QQ空间",@"复制链接"] ;
            _tagAry   = @[@"0",@"1",@"4",@"2",@"3",@"5"];
        }
    }
    
    [self backView];
}

// 显示底部灰色背景
-(UIView*)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.layer.masksToBounds = YES ;
        _backView.layer.cornerRadius = 6 ;
        _backView.backgroundColor = RGBA(246, 246, 246, 1) ;
        _backView.frame = CGRectMake(0, ScreenH, ScreenW, 130*HWB+90) ;
        [self.view addSubview:_backView];
    }
    return _backView ;
}

//显示（加动画）
-(void)showSelf{

    CGFloat space = 20;
    if (_share_type==1&&_imgAry.count==5) {
        space = 6;
    }

    CGFloat btnWidth = (ScreenW-(_imgAry.count+1)*space)/_imgAry.count;
    CGFloat btnHigh = 60*HWB ;

    if (_share_type==2||_share_type==3) {
        space = 50*HWB;
        btnWidth =(ScreenW-3*space)/3;
    }
    [UIView animateWithDuration:0.26 animations:^{
        self.view.backgroundColor = RGBA(0, 0, 0, 0.5) ;
        _backView.frame = CGRectMake(0, ScreenH-(btnWidth+60), ScreenW, btnWidth+60) ;
        if (_share_type==2||_share_type==3) {
            _backView.frame = CGRectMake(0, ScreenH-(btnWidth*2+60), ScreenW, btnWidth*2+60) ;
        }
    } completion:^(BOOL finished) {
        
    }];
    CGFloat delyTime = 0.05;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.13 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        for (int i = 0; i < _imgAry.count; i++) {
            _shareBtn  = [[ShareBtnRegister alloc]initWithImgName:_imgAry[i] titleName:_titleAry[i]];
            _shareBtn.tag = i ;
            
            //        _shareBtn.center = CGPointMake(ScreenW/2, 180*HWB+90);
            //        _shareBtn.bounds = CGRectMake(0, 0, 40*HWB, 40*HWB);
            if (_share_type==2||_share_type==3) {
                _shareBtn.frame=CGRectMake(space/2+i%3*(btnWidth+space), 20+i/3*(20+btnWidth)+HFRAME(_backView), btnWidth, btnHigh);
            }else{
                _shareBtn.frame=CGRectMake(space*(i+1)+i*btnWidth,HFRAME(_backView)+ 30, btnWidth, btnHigh);
            }
            [_backView addSubview:_shareBtn];
            [_shareBtn addTarget:self action:@selector(clickShareUrl:) forControlEvents:UIControlEventTouchUpInside];
            
            [UIView animateWithDuration:0.5 delay:delyTime * i usingSpringWithDamping:0.8 initialSpringVelocity:15 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                if (_share_type==2||_share_type==3) {
                    _shareBtn.frame=CGRectMake(space/2+i%3*(btnWidth+space), 20+i/3*(20+btnWidth), btnWidth, btnHigh);
                }else{
                    _shareBtn.frame=CGRectMake(space*(i+1)+i*btnWidth, 30, btnWidth, btnHigh);
                }
                
            } completion:^(BOOL finished) {
                
            }];
        }
    });
 
}

//移除
-(void)removeSelf{
    [UIView animateWithDuration:0.26 animations:^{
        self.view.backgroundColor = RGBA(0, 0, 0, 0) ;
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.right.offset(0);
            make.height.offset(130*HWB+90);
            make.top.equalTo(self.view.mas_bottom).offset(0);
        }];
        [_backView.superview layoutIfNeeded];
    }completion:^(BOOL finished) {
        [_backView removeFromSuperview];
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

#pragma mark  == >>  分享按钮点击事件
-(void)clickShareUrl:(ShareBtnRegister*)shareBtn{
    if (_share_type==3) {//唯品会分享单张图片
        switch ([_tagAry[shareBtn.tag] integerValue]) {
            case 0: { [self shareAppType:UMSocialPlatformType_WechatTimeLine]; } break;
            case 1: { [self shareAppType:UMSocialPlatformType_WechatSession]; } break;
            case 2: { [self shareAppType:UMSocialPlatformType_QQ]; } break;
            case 3: { [self shareAppType:UMSocialPlatformType_Qzone]; } break;
            case 4: { [self shareAppType:UMSocialPlatformType_Sina]; } break;
            case 5: { [self copyUrl]; } break;
        }
    }else{
        switch ([_tagAry[shareBtn.tag] integerValue]) {
            case 0: { [self shareWithshareType:UMSocialPlatformType_WechatTimeLine]; } break;
            case 1: { [self shareWithshareType:UMSocialPlatformType_WechatSession]; } break;
            case 2: { [self shareWithshareType:UMSocialPlatformType_QQ]; } break;
            case 3: { [self shareWithshareType:UMSocialPlatformType_Qzone]; } break;
            case 4: { [self shareWithshareType:UMSocialPlatformType_Sina]; } break;
            case 5: { [self copyUrl]; } break;
            default:  break;
        }
    }
    
}

#pragma mark ==>> 调用友盟分享，单张图片
-(void)shareAppType:(UMSocialPlatformType)platformType{
    
    
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UMShareImageObject*imgObject=[[UMShareImageObject alloc]init];
    imgObject.shareImage=_share_thumbImg;
    //分享消息对象设置分享内容对象
    messageObject.shareObject = imgObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            switch (error.code) {
                case 2001:{ SHOWMSG(@"不支持的分享格式或软件版本过低!") }break;
                case 2005:{SHOWMSG(@"生成的分享内容为空,请稍后再试!")} break;
                case 2007:{SHOWMSG(@"未能完成操作")} break;
                case 2008:{
                    if (platformType==1||platformType==2) { SHOWMSG(@"亲，你还没有安装微信哦!") }
                    if (platformType==4||platformType==5) { SHOWMSG(@"亲，你还没有安装QQ哦!") }
                } break;
                case 2009:{ SHOWMSG(@"分享已取消!") }break;
                case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试") }break;
                case 2011:{ SHOWMSG(@"第三方错误") }break;
                default: {SHOWMSG(@"未知错误!")} break;
            }
        }else{
            SHOWMSG(@"分享成功!")
        }
    }];
}

-(void)copyUrl{
    if (_share_type==3) {//唯品会复制
        if ([_goodsModel.platform containsString:@"多麦"]) {
            [ClipboardMonitoring pasteboard:_goodsModel.trackUrl];
        }else{
            [ClipboardMonitoring pasteboard:_goodsModel.cpsUrl];
        }
        
        if ([[UIPasteboard generalPasteboard].string isEqualToString:_goodsModel.cpsUrl]) {
            SHOWMSG(@"复制成功!")
        }else{ SHOWMSG(@"复制失败!") }
    }else{
        [ClipboardMonitoring pasteboard:_share_url] ;
        if ([[UIPasteboard generalPasteboard].string isEqualToString:_share_url]) {
            SHOWMSG(@"复制成功!")
        }else{ SHOWMSG(@"复制失败!") }
    }
}

-(void)shareWithshareType:(UMSocialPlatformType)platformType{
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //分享视频
    if (self.videoUrl) {
        [self shareVideo:platformType];
        return;
    }
    
    if (platformType == UMSocialPlatformType_Sina) {
        
        UMShareImageObject*shObjc=[[UMShareImageObject alloc]init];
        [shObjc setShareImage:[UIImage imageNamed:@"logo"]];
        messageObject.text=[NSString stringWithFormat:@"%@:%@\n%@",APPNAME,_share_describe,_share_url];
        messageObject.shareObject=shObjc;
    }else{
        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:_share_title.length==0?APPNAME:_share_title descr:_share_describe thumImage:_share_thumbImg];
        shareObject.webpageUrl = _share_url ;
        messageObject.shareObject = shareObject ;
    }
   
    
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            switch (error.code) {
                case 2001:{ SHOWMSG(@"不支持的分享格式或软件版本过低!") }break;
                case 2005:{SHOWMSG(@"生成的分享内容为空,请稍后再试!")} break;
                case 2008:{
                    if (platformType==1||platformType==2) { SHOWMSG(@"亲，你还没有安装微信哦!") }
                    if (platformType==4||platformType==5) { SHOWMSG(@"亲，你还没有安装QQ哦!") }
                } break;
                case 2009:{ SHOWMSG(@"分享已取消!") }break;
                case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试!") }break;
                case 2011:{ SHOWMSG(@"第三方错误!") }break;
                default: {SHOWMSG(@"分享失败了!")} break;
            }
        }else{
            [self removeSelf];
            SHOWMSG(@"分享成功！")
        }
    }];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_backView.frame, point)) {  return NO; }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end




@implementation ShareBtnRegister

-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName{
    self=[super init];
    if (self) {
        
        _imgView=[[UIImageView alloc]init];
        _imgView.image=[UIImage imageNamed:ImgName];
        _imgView.layer.masksToBounds = YES;
        [self addSubview:_imgView];
        _imgView.clipsToBounds = YES ;
        _imgView.contentMode = UIViewContentModeScaleAspectFill ;
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.offset(-20);
            make.top.offset(0);
            make.width.equalTo(_imgView.mas_height);
        }];
        
        _titleLab=[[UILabel alloc]init];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.text=titleName; _titleLab.font=[UIFont systemFontOfSize:12.0*HWB];
        _titleLab.textColor=Black51;
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(15);
            make.bottom.offset(0);
            make.centerX.equalTo(self.mas_centerX);
        }];
    }  return self ;
    
}
@end








