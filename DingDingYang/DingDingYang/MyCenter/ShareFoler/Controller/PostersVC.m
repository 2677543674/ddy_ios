//
//  PostersVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "PostersVC.h"
#import "GoodsPoster.h"

@interface PostersVC ()
@property(nonatomic,strong) UIScrollView * portersSV;//展示海报
@property(nonatomic,strong) UIImageView * imgPorter;//海报图片
@property(nonatomic,strong) UIButton * sbtn;//选择按钮
@property(nonatomic,strong) NSMutableArray * imgUrlAry;//海报图片数组
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UIImage * shareImg;
@property(nonatomic,strong) UIView * sharebgView ; //一堆分享按钮的背景视图
@property(nonatomic,strong) NSArray * imageAry, * titleAry , * tagAry ; //分享类型组合
@property(nonatomic,strong) UIButton * rulesBtn ; //奖励规则

@end

@implementation PostersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_modelTitle&&_modelTitle.length>0) {
        self.title = _modelTitle;
    }else{ self.title = @"分享海报"; }
    
    self.view.backgroundColor = COLORGROUP ;
    
    _imgUrlAry = [NSMutableArray array];
    
    [self creatSVView];
    
    [self performSelector:@selector(huoquPosters) withObject:self afterDelay:0.1];

}

-(void)huoquPosters{
    
    // 获取海报的位置坐标等
    MBShow(@"获取海报...")
    [NetRequest requestType:0 url:url_poster_frame ptdic:nil success:^(id nwdic) {
        
        NSArray * mtImgAry = NULLARY(nwdic[@"list"]);
        
        NSMutableArray * imgAry = [NSMutableArray array];
        // 遍历获取所有海报图片链接
        for (NSInteger i=0; i<mtImgAry.count; i++) {
            NSArray * frameAry = mtImgAry[i];
            for (NSInteger b=0; b<frameAry.count; b++) {
                FrameAndColorModel * facModel = [[FrameAndColorModel alloc]initWithUser:(NSDictionary*)frameAry[b]];
                if (facModel.type==1) { // 要绘制网络图片，将链接入数组，统一下载
                    if ([facModel.content hasPrefix:@"http"]) {
                        [imgAry addObject:facModel.content];
                    }
                }
            }
        }
        
        NSMutableArray * ary = [NSMutableArray array];

        if (imgAry.count>0) {

            [NetRequest downloadImageByUrl:imgAry success:^(id results) {
                NSArray * downLoadAry = (NSArray*)results;
                
                for (NSInteger i=0; i<mtImgAry.count; i++) {
                    NSArray * frameAry = mtImgAry[i];
                    NSString * posterUrl = @"";
                    for (NSInteger b=0; b<frameAry.count; b++) {
                        FrameAndColorModel * facModel = [[FrameAndColorModel alloc]initWithUser:(NSDictionary*)frameAry[b]];
                        if (facModel.type==1) {
                            if ([facModel.content hasPrefix:@"http"]) {
                                posterUrl = facModel.content;
                            }
                        }
                    }
                    if (posterUrl.length>0) {
                        [NetRequest downloadImageByUrl:posterUrl success:^(id result) {
                            [self goodsPosterBy:mtImgAry[i] posterImg:(UIImage*)result headImg:PlaceholderImg resultPoster:^(id data) {
                                [ary addObject:(UIImage*)data];
                                if (ary.count==downLoadAry.count) { MBHideHUD
                                    [_imgUrlAry addObjectsFromArray:(NSArray*)ary];
                                    [self addPorterImgView];
                                }
                            }];
                        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                            if (ary.count==downLoadAry.count) { MBHideHUD
                                [_imgUrlAry addObjectsFromArray:(NSArray*)ary];
                                [self addPorterImgView];
                            }
                        }];
                    }
                }
               
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD  if(errorType==2) { SHOWMSG(@"海报下载失败!") }
            }];
        }else{
            MBHideHUD  SHOWMSG(@"海报为空!")
        }
        
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
         MBHideHUD  SHOWMSG(failure)
    }];

}

-(void)goodsPosterBy:(NSArray*)frameAry posterImg:(UIImage*)posterImg headImg:(UIImage*)headImg resultPoster:(void(^)(id data))dataBlock{
    
    @try {
        
        @autoreleasepool {
            
            // 设置画布大小
            CGSize posterSize = CGSizeMake(375,667); // 先给默认的
            if (frameAry.count>0) { // 在从服务器返回数据中找
                FrameAndColorModel * bgFcModel = [[FrameAndColorModel alloc]initWithUser:frameAry[0]];
                if (bgFcModel.type==0) {
                    posterSize = CGSizeMake(bgFcModel.w, bgFcModel.h);
                }
            }
            
            UIGraphicsBeginImageContextWithOptions(posterSize,YES,[UIScreen mainScreen].scale);
            
            for (NSInteger i =0; i<frameAry.count; i++) {
                
                FrameAndColorModel * fcModel = [[FrameAndColorModel alloc]initWithUser:frameAry[i]];

                if (fcModel.x==-1) { fcModel.x = posterSize.width/2; }
                
                switch (fcModel.type) {
                    case 0:{ // 空白画布
                        
                        UIImage * bgImg = [UIImage imageNamed:@"white_bg"];
                        [bgImg drawInRect:CGRectMake(0, 0, fcModel.w, fcModel.h)];
                        
                    }break;
                        
                    case 1:{ // 图片(网络图片)
                        [posterImg drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                    }break;
                        
                    case 2:{ // 文字

                        NSMutableAttributedString * content = [[NSMutableAttributedString alloc]initWithString:fcModel.content] ;
                        [content addAttributes:@{NSForegroundColorAttributeName:fcModel.color} range:NSMakeRange(0, fcModel.content.length)];
                        [content addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fcModel.wordSize]} range:NSMakeRange(0, fcModel.content.length)];
                        [content drawInRect:CGRectMake(fcModel.x, fcModel.y,StringSize(fcModel.content,fcModel.wordSize).width,StringSize(fcModel.content,fcModel.wordSize).height)];

                    }break;

                    case 3:{ // 二维码合成(content返回链接)

                        if ([fcModel.content hasPrefix:@"http"]) {
                            UIImage * ewmimg = EWMImageWithString(fcModel.content);
//                            EWMImageWithStr(fcModel.content,500,[UIImage imageNamed:@"logo"]);
                            [ewmimg  drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                        }

                    }break;

                    case 6:{ // logo

//                        UIImage * logoimg = [UIImage imageNamed:@"logo"];
//                        [logoimg drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];

                    }break;
                        
                    default: break;
                }
                
            }
            
            // 获取绘制图片
            UIImage * resultingImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            dataBlock(resultingImage);
            
            NSData * data = UIImageJPEGRepresentation(resultingImage, 1);
            NSUInteger length = data.length ; float fl = [FORMATSTR(@"%lu",(unsigned long)length) floatValue];
            NSLog(@"合成的图片大小: %.2fkb 约为: %.2fM",fl/1000,fl/1000/1000);
            
        }
        
    } @catch (NSException *exception) {
        dataBlock(@"商品海报生成失败!");
    } @finally {
        
    }
    
}


#pragma mark ===>>> 创建海报
-(void)creatSVView{
    
    float height = (ScreenH-NAVCBAR_HEIGHT)/3; // 底部分享界面的高
    _portersSV = [[UIScrollView alloc]init];
    _portersSV.backgroundColor = COLORWHITE ;
    _portersSV.frame = CGRectMake(0, 0, ScreenW, height*2-20);
    [self.view addSubview:_portersSV];
    
    [self creatShareBtn];

}

#pragma mark ===>>> 创建单张海报、图片和选择按钮处理
-(void)addPorterImgView{
    
    for (UIView * v in _portersSV.subviews) { [v removeFromSuperview]; }
    
    float height=(ScreenH-NAVCBAR_HEIGHT)/3;//底部分享界面的高
    float pwidth=(height*2-80)/1.8;//海报图片的宽度
    for (NSInteger i=0; i<_imgUrlAry.count; i++) {
        _imgPorter = [[UIImageView alloc]init];
        _imgPorter.tag = 10+i;
        _imgPorter.layer.masksToBounds = YES;
        _imgPorter.layer.cornerRadius = 5;
        _imgPorter.userInteractionEnabled = YES;
        _imgPorter.contentMode = UIViewContentModeScaleAspectFill ;
        _imgPorter.frame = CGRectMake(i*(pwidth+10)+10,10, pwidth, height*2-80);
        _imgPorter.image = _imgUrlAry[i];
        [_portersSV addSubview:_imgPorter];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fangBigg:)];
        [_imgPorter  addGestureRecognizer:tap];
        
        _sbtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_sbtn setImage:[UIImage imageNamed:@"graySelect"] forState:UIControlStateNormal];
        _sbtn.selected=NO;  _sbtn.tag=100+i;
        [_sbtn addTarget:self action:@selector(selectShare:) forControlEvents:UIControlEventTouchUpInside];
        _sbtn.frame = CGRectMake(i*(pwidth+10)+(pwidth/2-10),height*2-70, 40, 40);
        [_sbtn setImageEdgeInsets:UIEdgeInsetsMake(13, 8, 3, 8)];
        [_portersSV addSubview:_sbtn];
    }
    _portersSV.contentSize=CGSizeMake((pwidth+10)*_imgUrlAry.count+10, 0);
    
}

-(void)fangBigg:(UIGestureRecognizer*)tapImg{
    [ImgPreviewVC show:self type:0 index:tapImg.view.tag-10 imagesAry:_imgUrlAry];
}

-(void)selectShare:(UIButton*)btn{
    _selectIndex = btn.tag;
    for (NSInteger i=100; i<108; i++) {
        UIButton * slBtn = (UIButton*)[_portersSV viewWithTag:i];
        slBtn.selected=NO;
        [slBtn setImage:[UIImage imageNamed:@"graySelect"] forState:UIControlStateNormal];
    }
    btn.selected = YES;
    [btn setImage:[UIImage imageNamed:@"greenSelect"] forState:UIControlStateNormal];
}

#pragma mark ==>> 创建分享按钮
-(void)creatShareBtn{
    
    _sharebgView = [[UIView alloc]init];
    [self.view addSubview:_sharebgView];
    [_sharebgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_portersSV.mas_bottom).offset(0);
        make.bottom.offset(0);  make.left.offset(0);
        make.right.offset(0);
    }];
    
    if (IsNeedSinaWeb==2) {
        _imageAry = @[@"new1_share_pyq",@"new1_share_wx",@"new1_share_qq",@"new1_share_qqkj",@"new1_baocun"] ;
        _titleAry = @[@"朋友圈",@"微信",@"QQ",@"QQ空间",@"保存到相册"] ;
        _tagAry   = @[@"2",@"1",@"3",@"4",@"6"];
    }else{
        _imageAry = @[@"new1_share_pyq",@"new1_share_wx",@"new1_share_wb",@"new1_share_qq",@"new1_share_qqkj",@"new1_baocun"] ;
        _titleAry = @[@"朋友圈",@"微信",@"新浪微博",@"QQ",@"QQ空间",@"保存到相册"] ;
        _tagAry   = @[@"2",@"1",@"5",@"3",@"4",@"6"];
    }

    for (NSInteger i = 0; i<_imageAry.count; i++) {
        ShareBtnPoster * btn = [[ShareBtnPoster alloc]initWithImgName:_imageAry[i] titleName:_titleAry[i]];
        btn.frame = CGRectMake(((ScreenW-198)/4+66)*(i%3)+(ScreenW-198)/4, (i/3)*88+12, 66, 66);
        btn.imgView.layer.cornerRadius = 24 ;
        btn.tag = i ;
        ADDTARGETBUTTON(btn, clickShareBtn:)
        [_sharebgView addSubview:btn];
    }
    
}

// 分享按钮点击事件
-(void)clickShareBtn:(UIButton*)btn{
    UIImageView * imgView = (UIImageView*)[_portersSV viewWithTag:_selectIndex-90];
    if (imgView.image!=nil) {
        _shareImg = imgView.image;
        [self shareBtnTag:[_tagAry[btn.tag] integerValue]];
    }else{  SHOWMSG(@"请勾选需要使用的海报!") }
}

-(void)shareBtnTag:(NSInteger)way{
    
    NSInteger shtype = way;
    if (shtype==6) {// 保存到相册
        [MethodsClassObjc photoPermissionsNoOpenShowVC:self photoPermissionsType:^(NSInteger type) {
            if (type==1) {
                UIImageWriteToSavedPhotosAlbum(_shareImg,self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
            }
        }];
    }else{
        CustomShareModel * model = [[CustomShareModel alloc]init];
        model.shareType = 2 ;
        model.imgDataOrAry = _shareImg;
        model.sharePlatform = shtype;
        [CustomUMShare shareByCustomModel:model result:^(NSInteger result) { }];
    }

}


//保存图片到相册
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (!error) {SHOWMSG(@"保存成功!")}
    else{SHOWMSG(@"保存失败!");}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end



@implementation ShareBtnPoster
-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName{
    self=[super init];
    if (self) {
        
        _imgView=[[UIImageView alloc]init];
        _imgView.backgroundColor = COLORWHITE ;
        _imgView.image=[UIImage imageNamed:ImgName];
        _imgView.layer.masksToBounds = YES;
        [self addSubview:_imgView];
        _imgView.clipsToBounds = YES ;
        _imgView.contentMode = UIViewContentModeScaleAspectFill ;
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.offset(-18);
            make.top.offset(0);
            make.width.equalTo(_imgView.mas_height);
        }];
        
        _titleLab=[[UILabel alloc]init];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.text=titleName; _titleLab.font=[UIFont systemFontOfSize:12.0*HWB];
        _titleLab.textColor=Black51;
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(13);
            make.bottom.offset(0);
            make.centerX.equalTo(self.mas_centerX);
        }];
        
    }return self;
}
@end












