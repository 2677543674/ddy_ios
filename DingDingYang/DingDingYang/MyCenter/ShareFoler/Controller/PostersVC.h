//
//  PostersVC.h
//  DingDingYang
//
//  Created by ddy on 2017/3/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostersVC : DDYViewController
@property(nonatomic,copy) NSString* modelTitle;
@end

@interface ShareBtnPoster : UIButton
-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName;
@property(nonatomic,strong) UIImageView * imgView;
@property(nonatomic,strong) UILabel * titleLab;
@property(nonatomic,copy) NSString * ImgName,* TitleName ;

@end









