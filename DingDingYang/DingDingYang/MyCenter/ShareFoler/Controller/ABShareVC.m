//
//  ABShareVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/13.
//  Copyright © 2017年 ddy.All rights reserved.
//


#import "ABShareVC.h"
#import "ABShareCell.h"
#import <AddressBook/AddressBook.h>
#import "FriendState.h"

@interface ABShareVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tabView;
@property(nonatomic,strong)NSMutableArray *dataAry;//数据源数组
@property(nonatomic,strong)NSMutableArray *searchAry;//数据源数组
@property(nonatomic,strong)NSMutableArray *selectAry;//选中的数组
@property(nonatomic,assign)NSInteger indexPage;
@property(nonatomic,strong)UIButton* commitBtn;
@property(nonatomic,strong)UITextField* searchText;
@property(nonatomic,assign)NSInteger type; //1:全部好友   2:搜索的好友列表
@end

@implementation ABShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"通讯录好友";
    self.view.backgroundColor=COLORWHITE;
    _type=1;
    /******已发送的用户第二天才可以再次发送****************/
    NSString * last_time = REPLACENULL( NSUDTakeData(@"sms_phone_time") ) ;
    NSString * now_time = NowTimeByFormat(@"yyyy-MM-dd");
    NSInteger result = [now_time compare:last_time];
    if (result!=0) {
        NSUDRemoveData(@"saveFriendPhone")
    }
    NSUDSaveData(now_time, @"sms_phone_time")
    /******已发送的用户第二天才可以再次发送****************/
    
    _dataAry=[NSMutableArray array];
    _searchAry=[NSMutableArray array];
    _selectAry=[NSMutableArray array];
    NSArray *array=[self toObtainAllPhone];
    [array enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
        ABSModel*model=[[ABSModel alloc]initWith:obj];
        [_dataAry addObject:model];
    }];
//    [self rightBtn];
    [self commitBtn];
    [self tabView];
    
    //    if (result!=0) {
    //        [self requestRole];
    //    }else{
    //        NSArray* arr=NSUDTakeData(@"friendArr");
    //        for (int i=0; i<arr.count; i++) {
    //            FriendState* model=[[FriendState alloc]initWith:arr[i]];
    //            for (int j=0; j<_dataAry.count; j++) {
    //                ABSModel* abmobel=_dataAry[j];
    //                NSLog(@"%p %p",abmobel,_dataAry[j]);
    //                if ([model.mobile isEqualToString:abmobel.phone]) {
    //                    abmobel.roleName=model.roleName;
    //                    break;
    //                }
    //            }
    //        }
    //        [_tabView reloadData];
    //    }

}


//-(void)requestRole{
//    NSString* phoneStr=@"";
//    for (int i=0; i<_dataAry.count; i++) {
//        ABSModel*model=_dataAry[i];
//        if (i==0) {
//            phoneStr=model.phone;
//        }else{
//            phoneStr=[NSString stringWithFormat:@"%@,%@",phoneStr,model.phone];
//        }
//    }
//    MBShow(@"正在获取")
//    [NetRequest requestTokenURL:url_get_phoneFriends parameter:@{@"mobiles":phoneStr} success:^(NSDictionary *nwdic) {
//        MBHideHUD
//        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
//
//            NSArray* arr=nwdic[@"list"];
//            NSUDSaveData(arr, @"friendArr")
//            for (int i=0; i<arr.count; i++) {
//                FriendState* model=[[FriendState alloc]initWith:arr[i]];
//                for (int j=0; j<_dataAry.count; j++) {
//                    ABSModel* abmobel=_dataAry[j];
//                    NSLog(@"%p %p",abmobel,_dataAry[j]);
//                    if ([model.mobile isEqualToString:abmobel.phone]) {
//                        abmobel.roleName=model.roleName;
//                        break;
//                    }
//                }
//            }
//            [_tabView reloadData];
//
//        }else{
//            SHOWMSG(nwdic[@"result"])
//        }
//    } failure:^(NSString *failure) {
//        MBHideHUD
//    }];
//
//}

-(void)rightBtn{
    UIButton* rightBtn=[[UIButton alloc]init];
    rightBtn.frame=CGRectMake(0, 0, 44, 44);
    rightBtn.tintColor = Black51 ;
    rightBtn.imageView.contentMode = UIViewContentModeScaleAspectFit ;
    [rightBtn setImage:[[UIImage imageNamed:@"pop_what"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 18, 5, -5)];
    [rightBtn addTarget:self action:@selector(clickRight) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)clickRight{
    [UIAlertController showIn:self title:@"说明" content:@"        推荐通讯录的好友成为你的下级，能够获得更多的推广收益，为了方便推广，可以使用一键发送功能哦，发送给通讯录好友邀请率会更高哦~\n        发送给通讯录好友本平台将收取一定费用" ctAlignment:NSTextAlignmentLeft btnAry:@[@"朕知道了"] indexAction:^(NSInteger indexTag) {
    }];
}

-(void)clickCancel{
    self.selectAry=[NSMutableArray array];
    [_commitBtn setBackgroundColor:RGBA(153.f,153.f,153.f,1)];
    [self.tabView reloadData];
}

-(UIButton*)commitBtn{
    if (!_commitBtn) {
        UIButton* cancelBtn=[UIButton buttonWithTitle:@"取消" font:15*HWB backgroundColor:COLORWHITE cornerRadius:19*HWB];
        ADDTARGETBUTTON(cancelBtn, clickCancel)
        [self.view addSubview:cancelBtn];
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10);
            make.bottom.offset(-7*HWB);
            make.height.offset(35*HWB);
            make.width.offset(100*HWB);
        }];
        
        _commitBtn=[UIButton buttonWithTitle:@"立即发送" font:15*HWB backgroundColor:RGBA(153.f,153.f,153.f,1) cornerRadius:17.5*HWB];
        _commitBtn.tag=8888;
        ADDTARGETBUTTON(_commitBtn, clickShare:)
        [self.view addSubview:_commitBtn];
        [_commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.offset(-7*HWB);
            make.height.offset(35*HWB);
            make.left.equalTo(cancelBtn.mas_right).offset(10);
        }];
    }
    return _commitBtn;
}

-(void)clickSearch{
    if (_searchText.text.length==0) {
        _type=1;
        [self.tabView reloadData];
        return;
    }
    _type=2;
    _searchAry=[NSMutableArray array];
    for (int i=0; i<_dataAry.count; i++) {
        ABSModel* model=_dataAry[i];
        if ([model.name containsString:_searchText.text]) {
            [_searchAry addObject:model];
        }
    }
    [self.tabView reloadData];
    
}

-(UITableView *)tabView{
    if (!_tabView) {
        UIView * searchView = [UIView new];
        searchView.backgroundColor = COLORGROUP;
        searchView.layer.cornerRadius = 16*HWB;
        searchView.clipsToBounds = YES;
        [self.view addSubview:searchView];
        [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5*HWB);  make.height.offset(32*HWB);
            make.left.offset(20*HWB); make.right.offset(-20*HWB);
        }];
        
        UIButton *searchBtn = [UIButton new];
        [searchBtn setBackgroundColor:LOGOCOLOR];
        [searchBtn addTarget:self action:@selector(clickSearch) forControlEvents:UIControlEventTouchUpInside];
        UIImage * newImg = [UIImage imageNamed:@"search_white"] ;

        [searchBtn setImage:newImg forState:UIControlStateNormal];
        [searchView addSubview:searchBtn];
        [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);   make.bottom.offset(0);
            make.right.offset(0); make.width.offset(55*HWB);
        }];
        
        _searchText = [UITextField textColor:Black51 PlaceHorder:@"查询昵称" font:13*HWB];
        [searchView addSubview:_searchText];
        [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.centerY.equalTo(searchView.mas_centerY);
            make.right.equalTo(searchBtn.mas_left).offset(-2);
        }];
        
        
        _tabView=[[UITableView alloc]initWithFrame:CGRectMake(0, 42*HWB, ScreenW, ScreenH-NAVCBAR_HEIGHT-50*HWB-42*HWB) style:UITableViewStylePlain];
        _tabView.delegate=self;
        _tabView.dataSource=self;
        _tabView.rowHeight=44*HWB;
        [self.view addSubview:_tabView];
        [_tabView registerClass:[ABShareCell class] forCellReuseIdentifier:@"ABShareCell"];
    }
    return _tabView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_type==2) {
        return _searchAry.count;
    }
    return _dataAry.count;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ABShareCell*cell=[tableView dequeueReusableCellWithIdentifier:@"ABShareCell" forIndexPath:indexPath];
    
    cell.shareBtn.tag=indexPath.row;
    cell.checkBtn.tag=indexPath.row;
    [cell.shareBtn addTarget:self action:@selector(clickShare:) forControlEvents:UIControlEventTouchUpInside];
    [cell.checkBtn addTarget:self action:@selector(clickCheck:) forControlEvents:UIControlEventTouchUpInside];
    if (_type==2) {
        cell.model=_searchAry[indexPath.row];
    }else{
        cell.model=_dataAry[indexPath.row];
    }
    
    //解决cell的复用问题
    cell.checkBtn.selected=NO;
    for (int i=0; i<_selectAry.count; i++) {
        if ([_selectAry[i] isEqual:cell.model]) {
            cell.checkBtn.selected=YES;
            break;
        }
    }
    
    return cell;
}

-(void)clickCheck:(UIButton*)btn{
    btn.selected=!btn.selected;
    if (_type==2) {
        if (btn.selected==YES) {
            [_selectAry addObject:_searchAry[btn.tag]];
        }else{
            [_selectAry removeObject:_searchAry[btn.tag]];
        }
        if (_selectAry.count>0) {
            [_commitBtn setBackgroundColor:LOGOCOLOR];
            [_commitBtn setTitle:[NSString stringWithFormat:@"立即发送(%ld)",_selectAry.count] forState:UIControlStateNormal];
        }else{
            [_commitBtn setBackgroundColor:RGBA(153.f,153.f,153.f,1)];
            [_commitBtn setTitle:[NSString stringWithFormat:@"立即发送"] forState:UIControlStateNormal];
        }
    }else{
        if (btn.selected==YES) {
            [_selectAry addObject:_dataAry[btn.tag]];
        }else{
            [_selectAry removeObject:_dataAry[btn.tag]];
        }
        if (_selectAry.count>0) {
            [_commitBtn setBackgroundColor:LOGOCOLOR];
            [_commitBtn setTitle:[NSString stringWithFormat:@"立即发送(%ld)",_selectAry.count] forState:UIControlStateNormal];
        }else{
            [_commitBtn setBackgroundColor:RGBA(153.f,153.f,153.f,1)];
            [_commitBtn setTitle:[NSString stringWithFormat:@"立即发送"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark ---> 点击推荐好友
-(void)clickShare:(UIButton*)btn{
    if (btn.tag==8888) {
        if (_selectAry.count==0) {
            return;
        }
    }
    MBShow(@"获取内容...");
    
    [NetRequest requestTokenURL:url_get_pfShare_txt parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        
        if ([nwdic[@"result"]isEqualToString:@"OK"]) {
            NSString*msgStr=REPLACENULL(nwdic[@"msg"]);
            UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
            UMShareSmsObject*smsObjc = [[UMShareSmsObject alloc]init];
            
            NSMutableArray* phone = [NSMutableArray array];
            if (btn.tag==8888) {
                for (int i=0; i<_selectAry.count; i++) {
                    ABSModel* model=_selectAry[i];
                    [phone addObject:model.phone];
                }
            }else{
                ABSModel*model=_dataAry[btn.tag];
                [phone addObject:model.phone];
            }
            
            smsObjc.recipients=phone;
            smsObjc.smsContent=msgStr;
            messageObject.shareObject=smsObjc;
            UIBarButtonItem *item = [UIBarButtonItem appearance];
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                         NSForegroundColorAttributeName:[UIColor darkGrayColor]};
            
            [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
            [[UMSocialManager defaultManager] shareToPlatform:UMSocialPlatformType_Sms messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
                
                if (!error) {
                    NSMutableArray* saveFriendPhone=NSUDTakeData(@"saveFriendPhone");
                    if (btn.tag==8888) {
                        if (saveFriendPhone.count==0) {
                            saveFriendPhone=[NSMutableArray arrayWithArray:phone];
                        }
                        [saveFriendPhone addObjectsFromArray:phone];
                        NSUDSaveData(saveFriendPhone, @"saveFriendPhone");
                        _selectAry=[NSMutableArray array];
                        
                    }else{
                        if (saveFriendPhone.count==0) {
                            saveFriendPhone=[NSMutableArray array];
                        }
                        ABSModel*model=_dataAry[btn.tag];
                        [saveFriendPhone addObject:model.phone];
                        NSUDSaveData(saveFriendPhone, @"saveFriendPhone");
                        [_selectAry removeObject:model];
                    }
                    [self.tabView reloadData];
                }else{
                    switch (error.code) {
                        case 2005:{SHOWMSG(@"生成的分享内容为空,请稍后再试!")} break;
                        case 2009:{ SHOWMSG(@"分享已取消!") }break;
                        case 2011:{ SHOWMSG(@"第三方错误") }break;
                        default: {SHOWMSG(@"分享出错啦，请稍后再试!")} break;
                    }
                }
            }];
            
        }else{  MBHideHUD SHOWMSG(nwdic[@"result"])}
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
    

}


//读取通讯录获取联系人
-(NSMutableArray*)toObtainAllPhone{
    NSMutableArray*msgAry=[NSMutableArray array];
    // 2. 获取所有联系人
    ABAddressBookRef addressBookRef = ABAddressBookCreate();
    CFArrayRef arrayRef = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    long count = CFArrayGetCount(arrayRef);
    for (int i = 0; i < count; i++) {
        //获取联系人对象的引用
        ABRecordRef people = CFArrayGetValueAtIndex(arrayRef, i);
        //获取当前联系人名字
        NSString *name=REPLACENULL((__bridge NSString *)(ABRecordCopyValue(people, kABPersonFirstNameProperty)));
        //姓氏
        NSString *xing=REPLACENULL((__bridge NSString *)(ABRecordCopyValue(people, kABPersonLastNameProperty)));
        NSString*pname=[NSString stringWithFormat:@"%@%@",xing,name];
        
        ABMultiValueRef phones = ABRecordCopyValue(people, kABPersonPhoneProperty);
        NSString *oldPhone =REPLACENULL((__bridge NSString *)(ABMultiValueCopyValueAtIndex(phones, 0)));
        NSRegularExpression *regular = [NSRegularExpression regularExpressionWithPattern:@"[^[0-9.]]"  options:0 error:NULL];
        NSString *newPhone = [regular stringByReplacingMatchesInString:oldPhone options:0 range:NSMakeRange(0, [oldPhone length]) withTemplate:@""];
        
        if (newPhone.length==13&&[newPhone hasPrefix:@"86"]) {
            newPhone=[newPhone substringFromIndex:2];
        }
        if (newPhone.length==14&&[newPhone hasPrefix:@"+86"]) {
            newPhone=[newPhone substringFromIndex:3];
        }
        if (newPhone.length==11&&[newPhone hasPrefix:@"1"]) {
            NSDictionary*dic=@{@"name":pname,@"number":newPhone};
            [msgAry addObject:dic];
        }
        
    }
    
    return msgAry;
}





@end

