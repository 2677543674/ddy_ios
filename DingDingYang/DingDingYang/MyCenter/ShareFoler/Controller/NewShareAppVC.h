//
//  "}} NewShareAppVC.h
//  DingDingYang
//
//  Created by ddy on 2018/1/3.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface NewShareAppVC : DDYViewController
@property(nonatomic,strong) PersonalModel * model ;
@property(nonatomic,copy) NSString* modelTitle;
@end

@interface ShareTextBtn : UIView
@property(nonatomic,strong) UILabel  * contentLab ;
@property(nonatomic,strong) UIButton * shareBtn ;
@end
