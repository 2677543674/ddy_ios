//
//  SharePopoverVC.h
//  DingDingYang
//
//  Created by ddy on 2017/12/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface SharePopoverVC : DDYViewController

/**
 分享链接使用友盟

 @param url web链接
 @param urlTitle 要显示的标题
 @param des 描述
 @param img 缩略图图片
 @param type 分享类型
 */

+(void)shareUrl:(NSString*)url title:(NSString*)urlTitle des:(NSString*)des thubImg:(UIImage*)img shareType:(NSInteger)type;

+(void)shareWPHGoodsModel:(GoodsModel*)model thubImg:(UIImage*)img shareType:(NSInteger)type;


/**
 打开系统分享，分享多图(数组中可以是连接，也可以是图片)

 @param imageAry 图片数组
 */

+(void)openSystemShareImage:(NSMutableArray*)imageAry;



+(void)shareMiniProgramByGoodsId:(GoodsModel*)goodsMd;

//分享视频
+(void)shareVideoWithURL:(NSString *)videoUrl;



@property(nonatomic,assign) NSInteger  share_type ;   // 分享按钮的样式

@property(nonatomic,copy) NSString * share_url ;      // 分享的链接
@property(nonatomic,copy) NSString * share_title ;    // 分享出去的标题
@property(nonatomic,copy) NSString * share_describe ; // 分享出去的描述信息
@property(nonatomic,strong)UIImage * share_thumbImg ; // 分享出去的缩略图

@property(nonatomic,copy) NSString * share_text ;     // 分享的纯文本
@property(nonatomic,strong)GoodsModel* goodsModel;

@property(nonatomic,strong)NSString    *videoUrl;     // 分享的视频

//@property(nonatomic,strong) NSMutableArray * share_imgAry; // 分享图片(多图)


@end






@interface ShareBtnRegister : UIButton
-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName;
@property(nonatomic,strong) UIImageView * imgView;
@property(nonatomic,strong) UILabel * titleLab;
@property(nonatomic,copy) NSString * ImgName,* TitleName ;
@end









