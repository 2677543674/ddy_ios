//
//  "}} NewShareAppVC.m
//  DingDingYang
//
//  Created by ddy on 2018/1/3.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "NewShareAppVC.h"
#import "EwmShareView.h"
#import "SharePopoverVC.h"
#import "PostersVC.h"
#import "ABShareVC.h"
#import <AddressBook/AddressBook.h>

@interface NewShareAppVC ()

@property(nonatomic,strong) UIImageView * bgImgView ;    // 分享背景图
@property(nonatomic,strong) EwmShareView* shareViewEwm ; // 分享二维码的图片
@property(nonatomic,strong) UIView   * shareBgView ;     // 分享邀请码和链接的
@property(nonatomic,strong) UIButton * rulesBtn ;        //奖励规则
@property(nonatomic,strong) ShareTextBtn * stBtn1 , * stBtn2 ; // 邀请码、链接

@property(nonatomic,copy)  NSString * shareText ; // 分享的文本
@property(nonatomic,strong) UIImage * headImg ;   // 用户头像

@end

@implementation NewShareAppVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORWHITE ;
    
    self.title = _modelTitle.length==0?@"分享好友":_modelTitle;
    
    _shareText = @"" ;

    if (_model == nil) {
        _model=[[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
    }

    if ([_model.headImg isKindOfClass:[NSString class]]) {
        SDWebImageManager * manager = [SDWebImageManager sharedManager];
        NSString * key = [manager cacheKeyForURL:[NSURL URLWithString:_model.headImg]];
        SDImageCache* cache = [SDImageCache sharedImageCache];
        _headImg = [cache imageFromDiskCacheForKey:key];
    }else{
        _headImg = _model.headImg ;
    }
    
    if (_headImg==nil) {
        _headImg = [UIImage imageNamed:@"logo"];
    }
    
    [self newShareUI];
}


-(void)newShareUI{
    
    self.view.backgroundColor = COLORGROUP  ;
    _bgImgView = [[UIImageView alloc]init];
    _bgImgView.frame = CGRectMake(0, 0, ScreenW, ScreenW/1.2);
    _bgImgView.image = [UIImage imageNamed:@"new_share_bgImg"];
#if (defined DingDangDingDang)
    _bgImgView.image = [UIImage imageNamed:@"share_friend"];
#endif
    

    
    [self.view addSubview:_bgImgView];
    
    _shareBgView = [[UIView alloc]init];
    _shareBgView.backgroundColor = RGBA(255, 255, 255, 0.95) ;

    _shareBgView.layer.shadowColor = COLORGRAY.CGColor;
    _shareBgView.layer.shadowOffset = CGSizeMake(1,1);
    _shareBgView.layer.shadowOpacity = 0.6 ;
    _shareBgView.layer.shadowRadius = 3 ;
    [self.view addSubview:_shareBgView];
    [_shareBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.offset(30);  make.right.offset(-30);
        make.height.offset(50+144*HWB);
        make.centerY.equalTo(_bgImgView.mas_bottom).offset(0);
    }];
#if HuiDe
    self.view.backgroundColor=RGBA(252, 247, 246, 1);
    _bgImgView.image = [UIImage imageNamed:@"share_friend"];
    _bgImgView.frame = CGRectMake(0, 0, ScreenW, ScreenW/2);
    [_shareBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(30);  make.right.offset(-30);
        make.height.offset(50+144*HWB);
        make.centerY.equalTo(self.view.mas_top).offset(ScreenW/1.2);
    }];
#endif
    
    _stBtn1 = [[ShareTextBtn alloc]init];
    _stBtn1.layer.cornerRadius = 18*HWB ;
    _stBtn1.layer.borderWidth = 0.5 ;
    [_shareBgView addSubview:_stBtn1];
    [_stBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);  make.right.offset(-20);
        make.top.offset(10);
        make.height.offset(36*HWB);
    }];
    
    UIButton* sharePoster=[[UIButton alloc]init];
    sharePoster.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
    [sharePoster setTitle:@"分享专属海报" forState:UIControlStateNormal];
    [sharePoster setBackgroundColor:RGBA(231, 78, 116, 1)];
    sharePoster.layer.cornerRadius=18*HWB;
    sharePoster.clipsToBounds=YES;
    ADDTARGETBUTTON(sharePoster, share_Poster)
    [_shareBgView addSubview:sharePoster];
    [sharePoster mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);  make.right.offset(-20);
        make.top.equalTo(_stBtn1.mas_bottom).offset(10);
        make.height.offset(36*HWB);
    }];
    
    UIButton* shareLianjie=[[UIButton alloc]init];
    shareLianjie.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
    [shareLianjie setTitle:@"分享邀请链接" forState:UIControlStateNormal];
    [shareLianjie setTitleColor:RGBA(255, 103, 86, 1) forState:UIControlStateNormal];
    [shareLianjie setBackgroundColor:[UIColor whiteColor]];
    shareLianjie.layer.borderColor=RGBA(255, 103, 86, 1).CGColor;
    shareLianjie.layer.borderWidth=1;
    shareLianjie.layer.cornerRadius=18*HWB;
    shareLianjie.clipsToBounds=YES;
    ADDTARGETBUTTON(shareLianjie, share_LianJie)
    [_shareBgView addSubview:shareLianjie];
    [shareLianjie mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);  make.right.offset(-20);
        make.top.equalTo(sharePoster.mas_bottom).offset(10);
        make.height.offset(36*HWB);
    }];
    
    UIButton* shareFriend=[[UIButton alloc]init];
    shareFriend.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
    [shareFriend setTitle:@"通讯录好友推荐" forState:UIControlStateNormal];
    [shareFriend setTitleColor:RGBA(255, 103, 86, 1) forState:UIControlStateNormal];
    [shareFriend setBackgroundColor:[UIColor whiteColor]];
    shareFriend.layer.borderColor=RGBA(255, 103, 86, 1).CGColor;
    shareFriend.layer.borderWidth=1;
    shareFriend.layer.cornerRadius=18*HWB;
    shareFriend.clipsToBounds=YES;
    ADDTARGETBUTTON(shareFriend, accessToTheAddressBook)
    [_shareBgView addSubview:shareFriend];
    [shareFriend mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);  make.right.offset(-20);
        make.top.equalTo(shareLianjie.mas_bottom).offset(10);
        make.height.offset(36*HWB);
    }];
    
    
    //二维码
    _shareViewEwm = [[EwmShareView alloc]init];
    _shareViewEwm.ewmImgV.image = EWMImageWithString(_model.shareUrl) ;
    [self.view addSubview:_shareViewEwm];
    [_shareViewEwm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_shareBgView.mas_bottom).offset(10);
        make.height.offset(100*HWB);
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.equalTo(_shareViewEwm.mas_height);
    }];
    
    UILabel * mdmLab = [UILabel labText:@"面对面分享" color:Black102 font:12*HWB];
    [self.view addSubview:mdmLab];
    [mdmLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(_shareViewEwm.mas_bottom).offset(5);
    }];
    
    _stBtn1.contentLab.text = FORMATSTR(@"邀请码: %@",_model.inviteCode) ;
    [_stBtn1.shareBtn setTitle:@"复制邀请码" forState:(UIControlStateNormal)];
    _stBtn1.shareBtn.backgroundColor = RGBA(231, 78, 116, 1) ;
    _stBtn1.layer.borderColor = RGBA(231, 78, 116, 1).CGColor ;
    ADDTARGETBUTTON(_stBtn1.shareBtn, share_YaoQingMa)
    
    if (PID==10107) { // 叮叮羊
        [sharePoster setTitle:@"分享海报" forState:(UIControlStateNormal)];
        [shareLianjie setTitle:@"分享链接" forState:(UIControlStateNormal)];
    }
}
    
    //推荐通讯录好友
-(void)accessToTheAddressBook{
    
    // 判断是否授权
    ABAuthorizationStatus authorizationStatus = ABAddressBookGetAuthorizationStatus();
    if (authorizationStatus == kABAuthorizationStatusNotDetermined) {
        // 请求授权
        ABAddressBookRef addressBookRef = ABAddressBookCreate();
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ABShareVC*shareAB=[[ABShareVC alloc]init];
                    PushTo(shareAB, YES);
                });
            } // 授权成功
        });
    }
    
    //用户已授权
    if (authorizationStatus == kABAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            ABShareVC*shareAB=[[ABShareVC alloc]init];
            PushTo(shareAB, YES);
        });
        
    }
    
    //用户拒绝
    if (authorizationStatus == kABAuthorizationStatusDenied) {
        [self aleartView:FORMATSTR(@"亲，无法访问通讯录，推荐通讯录好友有惊喜哟! 请按以下步骤打开权限：@“设置->隐私->通讯录” 并允许%@访问通讯录",APPNAME)];
    }
    //无法使用通讯录
    if (authorizationStatus == kABAuthorizationStatusRestricted) {
        [self aleartView:@"无法使用通讯录"];
    }
    
}
    
-(void)aleartView:(NSString*)title{
    UIAlertController*alertView=[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction*alertCancel=[UIAlertAction actionWithTitle:@"知道啦" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
    }];
    [alertView addAction:alertCancel];
    [self presentViewController:alertView animated:YES completion:nil];
}
    
//#pragma mark -->上传通讯录
//    //上传通讯录
//-(void)upNumber{
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//        ABShareVC*shareAB=[[ABShareVC alloc]init];
//        PushTo(shareAB, YES);
//    });
//
//}

-(void)share_Poster{
    PostersVC* posterVC=[PostersVC new];
    PushTo(posterVC, YES);
}

// 复制邀请码
-(void)share_YaoQingMa{
    
    if (_model.inviteCode.length==0) {
        SHOWMSG(@"未获取到邀请码，无法复制")
        return ;
    }
    
    [ClipboardMonitoring pasteboard:_model.inviteCode];
    if ([[UIPasteboard generalPasteboard].string isEqual:_model.inviteCode]) {
        SHOWMSG(@"复制成功!")
    }else{
        SHOWMSG(@"复制失败!")
    }


}

// 分享链接
-(void)share_LianJie{
    NSString* content=[NSString stringWithFormat:@"邀请你加入%@！先领券，再购物，更划算",APPNAME];
    if (_model.shareUrl.length==0) {
        SHOWMSG(@"未获取到分享链接，暂时不能分享哦~")
        return ;
    }
    
    if (_shareText.length>0) {

        [SharePopoverVC shareUrl:_model.shareUrl title:content des:_shareText thubImg:_headImg shareType:1];
        
    }else{
        MBShow(@"获取文案...")
        [NetRequest requestTokenURL:url_get_share_text parameter:nil success:^(NSDictionary *nwdic) {
            MBHideHUD
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                if (REPLACENULL( nwdic[@"shareText"] ).length>0) {
                    _shareText = REPLACENULL( nwdic[@"shareText"] ) ;
                }else{  _shareText = SHARECONTENT ;  }
                [SharePopoverVC shareUrl:_model.shareUrl title:content des:_shareText thubImg:_headImg shareType:1];
            }else{
                [SharePopoverVC shareUrl:_model.shareUrl title:content des:SHARECONTENT thubImg:_headImg shareType:1];
            }
        } failure:^(NSString *failure) {
            MBHideHUD
            [SharePopoverVC shareUrl:_model.shareUrl title:content des:SHARECONTENT thubImg:_headImg shareType:1];
        }];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end


@implementation ShareTextBtn

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.layer.masksToBounds = YES ;
        self.clipsToBounds = YES ;
        _shareBtn = [UIButton titLe:@"分享" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13*HWB];
        [self addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);  make.top.offset(0);
            make.bottom.offset(0); make.width.offset(80*HWB);
        }];
        
        _contentLab = [UILabel labText:@"" color:Black51 font:12*HWB];
        _contentLab.clipsToBounds = YES ;
        [self addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);  make.top.offset(0); make.bottom.offset(0);
            make.right.equalTo(_shareBtn.mas_left).offset(-10);
        }];
    }
    return self ;
}

@end


