//
//  NewRankCell.h

//
//  Created by ddy on 2017/12/21.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankingModel.h"
@interface NewRankCell : UITableViewCell
@property(nonatomic,strong) UIImageView * headerImage;
@property(nonatomic,strong) UILabel * rank;
@property(nonatomic,strong) UILabel * name;
@property(nonatomic,strong) UILabel * amount;
@property(nonatomic,strong) UIImageView * sexImage;
@property(nonatomic,strong) RankingModel * model ;

@end
