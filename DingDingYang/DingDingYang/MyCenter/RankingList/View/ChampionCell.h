//
//  ChampionCell.h
//  DingDingYang
//
//  Created by ddy on 2017/6/16.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@class RankingModel , RankHeadView ;
@interface ChampionCell : UITableViewCell

//@property(nonatomic,strong) UIImageView * bgimgV ;
//@property(nonatomic,strong) UIImageView * pimgV ;
//@property(nonatomic,strong) UIImageView * hImgV ;
//@property(nonatomic,strong) UIButton * cImgBtn ;
//@property(nonatomic,strong) UILabel * phoneMoneyLab ;
//@property(nonatomic,strong) UILabel * moneyLab ;
//@property(nonatomic,strong) RankingModel * oneModel ;

@property(nonatomic,strong) RankHeadView * headView1; // 冠军
@property(nonatomic,strong) RankHeadView * headView2; // 亚军
@property(nonatomic,strong) RankHeadView * headView3; // 季军

@property(nonatomic,strong) NSMutableArray * rankListAry ;

@end



@interface RankHeadView : UIView
@property(nonatomic,strong) UIImageView * topImgV;
@property(nonatomic,strong) UIImageView * headImgV;
@property(nonatomic,strong) UIImageView * bottomImgV;
@property(nonatomic,strong) UILabel * nickNameLab ;
@property(nonatomic,strong) UILabel * priceLab ;

@property(nonatomic,assign) NSInteger headType ;

@property(nonatomic,strong) RankingModel * rankModel ;
@end





