//
//  NewRankCell.m

//
//  Created by ddy on 2017/12/21.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "NewRankCell.h"

@implementation NewRankCell


-(void)setModel:(RankingModel *)model{
    _model=model;
    [_headerImage sd_setImageWithURL:[NSURL URLWithString:model.headImg] placeholderImage:PlaceholderImg];
    _name.text = model.name;
    _amount.text = [NSString stringWithFormat:@"￥%@",model.amount];
    
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self rank];
        [self headerImage];
        [self name];
//        [self sexImage];
        [self amount];
        
        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.bottom.offset(0);
            make.height.offset(1);
        }];
  
    }
    return self ;
}


-(UILabel *)rank{
    if (!_rank) {
        _rank = [UILabel new];
        _rank.font=[UIFont systemFontOfSize:13*HWB];
        _rank.textColor=Black51;
        _rank.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:_rank];
        [_rank mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(2*HWB);
            make.bottom.offset(0);
            make.width.equalTo(_rank.mas_height); /// 约束长度等于宽度
        }];
    }
    return _rank;
}


-(UIImageView *)headerImage{
    if (!_headerImage) {
        _headerImage=[UIImageView new];
        _headerImage.image=[UIImage imageNamed:@"头像1"];
        _headerImage.layer.cornerRadius = 20*HWB;//计算后

        _headerImage.clipsToBounds=YES;
        _headerImage.contentMode=UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_headerImage];
        [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_rank.mas_right).offset(2*HWB);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.offset(40*HWB);  make.width.offset(40*HWB);
        }];
    }
    return _headerImage;
}

-(UILabel *)name{
    if (!_name) {
        _name=[UILabel new];
        _name.font=[UIFont systemFontOfSize:13*HWB];
        _name.textColor=Black51;
        _name.text=@"微信昵称";
        [self.contentView addSubview:_name];
        [_name mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_headerImage.mas_right).offset(10*HWB);
            make.height.offset(30);
            make.centerY.equalTo(_headerImage.mas_centerY);
        }];
    }
    return _name;
}

-(UIImageView *)sexImage{
    if (!_sexImage) {
        _sexImage=[UIImageView new];
        _sexImage.image=[UIImage imageNamed:@"男"];
        _sexImage.clipsToBounds=YES;
        _sexImage.contentMode=UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_sexImage];
        [_sexImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_name.mas_centerY);
            make.left.equalTo(_name.mas_right).offset(6*HWB);
//            make.width.equalTo(_sexImage.mas_height); /// 约束长度等于宽度
        }];
    }
    return _sexImage;
}

-(UILabel *)amount{
    if (!_amount) {
        _amount=[UILabel new];
        _amount.font=[UIFont systemFontOfSize:14*HWB];
        _amount.textColor=Black102;
        _amount.text=@"收益";
        [self.contentView addSubview:_amount];
        [_amount mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB);
            make.height.offset(30); /// 约束长度等于宽度
            make.centerY.equalTo(_headerImage.mas_centerY);
        }];
    }
    return _amount;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}











@end
