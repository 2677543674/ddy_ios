//
//  ChampionCell.m
//  DingDingYang
//
//  Created by ddy on 2017/6/16.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ChampionCell.h"
#import "RankingModel.h"

@implementation ChampionCell

-(void)setRankListAry:(NSMutableArray *)rankListAry{
    _rankListAry = rankListAry ;
    if (_rankListAry.count>2) {
        _headView1.rankModel = _rankListAry[0];
        _headView2.rankModel = _rankListAry[1];
        _headView3.rankModel = _rankListAry[2];
    }
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        
        UIImageView * bgimgView = [[UIImageView alloc]init];
        bgimgView.image = [UIImage imageNamed:@"背景图"];
        [self.contentView addSubview:bgimgView];
        [bgimgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
        
        _headView1 = [[RankHeadView alloc]init];
        _headView1.topImgV.image = [UIImage imageNamed:@"冠军1"];
        _headView1.bottomImgV.image = [UIImage imageNamed:@"冠军3"];
        _headView1.headType = 1 ;
        [self.contentView addSubview:_headView1];
        [_headView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-8*HWB);  make.centerX.equalTo(self.mas_centerX);
            make.top.offset(8*HWB);   make.width.offset(ScreenW/3);
        }];
        
        _headView2 = [[RankHeadView alloc]init];
        _headView2.topImgV.image = [UIImage imageNamed:@"亚军1"];
        _headView2.bottomImgV.image = [UIImage imageNamed:@"亚军3"];
        _headView2.headType = 2 ;
        _headView2.headImgV.layer.borderColor = RGBA(133, 191, 254, 1).CGColor ;
        [self.contentView addSubview:_headView2];
        [_headView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-8*HWB);  make.top.offset(26*HWB);
            make.left.offset(20*HWB);  make.right.equalTo(_headView1.mas_left).offset(-20*HWB);
        }];
        
        _headView3 = [[RankHeadView alloc]init];
        _headView3.topImgV.image = [UIImage imageNamed:@"季军1"];
        _headView3.bottomImgV.image = [UIImage imageNamed:@"季军3"];
        _headView3.headType = 2 ;
        _headView3.headImgV.layer.borderColor = RGBA(251, 116, 25, 1).CGColor;
        [self.contentView addSubview:_headView3];
        [_headView3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-8*HWB);  make.top.offset(26*HWB);
            make.right.offset(20*HWB);  make.left.equalTo(_headView1.mas_right).offset(-20*HWB);
        }];
        
    }
    return self ;
}

- (void)awakeFromNib {  [super awakeFromNib];  }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end


@implementation RankHeadView

-(void)setRankModel:(RankingModel *)rankModel{
    _rankModel = rankModel ;
    self.nickNameLab.text = _rankModel.name;
    self.priceLab.text = FORMATSTR(@"￥%@",_rankModel.amount);
    [self.headImgV sd_setImageWithURL:[NSURL URLWithString:_rankModel.headImg] placeholderImage:PlaceholderImg];
}

// 1:大的、冠军   2:季军和亚军
-(void)setHeadType:(NSInteger)headType{
    _headType = headType ;
    
    switch (_headType) {
        case 1:{ // 冠军
            _headImgV.layer.cornerRadius = 30*HWB ;
            
            _headImgV.layer.borderColor = RGBA(234, 95, 91, 1).CGColor;
            
            [_topImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.offset(0); make.centerX.equalTo(self.mas_centerX);
                make.width.offset(46*HWB); make.height.offset(28*HWB);
            }];
            
            [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_topImgV.mas_bottom).offset(-7*HWB);
                make.width.offset(60*HWB); make.height.offset(60*HWB);
                make.centerX.equalTo(self.mas_centerX);
            }];
            
            [_bottomImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_headImgV.mas_bottom).offset(-10*HWB);
                make.width.offset(85*HWB); make.height.offset(25*HWB);
                make.centerX.equalTo(self.mas_centerX);
            }];
            
        } break;
            
        case 2:{ // 亚军、季军
            
            _headImgV.layer.cornerRadius = 25*HWB ;
            _headImgV.layer.borderWidth = 1.5 ;
            
            [_topImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.offset(0); make.centerX.equalTo(self.mas_centerX);
                make.width.offset(35*HWB); make.height.offset(22*HWB);
            }];
            
            [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_topImgV.mas_bottom).offset(-5*HWB);
                make.height.offset(50*HWB); make.width.offset(50*HWB);
                 make.centerX.equalTo(self.mas_centerX);
            }];
            
            [_bottomImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_headImgV.mas_bottom).offset(-7*HWB);
                make.width.offset(68*HWB);  make.height.offset(20*HWB);
                make.centerX.equalTo(self.mas_centerX);
            }];
            
        } break;
            
        default:  break;
    }
    
    
}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        _topImgV = [[UIImageView alloc]init];
        [self addSubview:_topImgV];

        _headImgV = [[UIImageView alloc]init];
        _headImgV.layer.masksToBounds = YES ;
        _headImgV.layer.borderWidth = 1.5 ;
        _headImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _headImgV.image = PlaceholderImg ;
        [self addSubview:_headImgV];

        _bottomImgV = [[UIImageView alloc]init];
        [self addSubview:_bottomImgV];

        [self priceLab];
        [self nickNameLab];
    }
    return self;
}

-(UILabel*)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"金额" color:Black51 font:15*HWB];
        [self addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.offset(0);
        }];
    }
    return _priceLab ;
}

-(UILabel*)nickNameLab{
    if (!_nickNameLab) {
        _nickNameLab = [UILabel labText:@"昵称" color:Black51 font:13*HWB];
        [self addSubview:_nickNameLab];
        [_nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.equalTo(_priceLab.mas_top).offset(-2);
        }];
    }
    return _nickNameLab ;
}

@end











