//
//  RankingModel.h
//  DingDingYang
//
//  Created by ddy on 2017/6/16.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RankingModel : NSObject

@property(nonatomic,copy) NSString * userId ;
@property(nonatomic,copy) NSString * headImg ;
@property(nonatomic,copy) NSString * name ;
@property(nonatomic,copy) NSString * amount ;
@property(nonatomic,copy) NSString * mobile ;
@property(nonatomic,copy) NSString * createTime ;
-(instancetype)initWith:(NSDictionary*)dic ;

@end
