//
//  RankingModel.m
//  DingDingYang
//
//  Created by ddy on 2017/6/16.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "RankingModel.h"

@implementation RankingModel
-(instancetype)initWith:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        
        self.userId = REPLACENULL(dic[@"userId"]) ;
        self.headImg = FORMATSTR(@"%@%@",[REPLACENULL(dic[@"headImg"]) hasPrefix:@"http"]?@"":dns_dynamic_loadimg,REPLACENULL(dic[@"headImg"])) ;
        self.name = REPLACENULL(dic[@"name"]) ;
        self.amount = ROUNDED(REPLACENULL(dic[@"amount"]), 2) ;
        self.mobile = REPLACENULL(dic[@"mobile"]) ;
        self.createTime = REPLACENULL(dic[@"createTime"]) ;
        
    }
    return self ;
}
@end
