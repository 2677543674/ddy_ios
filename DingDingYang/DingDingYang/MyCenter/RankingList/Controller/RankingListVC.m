//
//  RankingListVC.m
//  DingDingYang
//
//  Created by ddy on 2017/6/16.
//  Copyright © 2017年 ddy.All rights reserved.
//


#import "RankingListVC.h"
#import "ChampionCell.h"
#import "RankingModel.h"
#import "NewRankCell.h"

@interface RankingListVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIView * backView;
@property(nonatomic,strong) UIButton * onedayBtn;
@property(nonatomic,strong) UIButton * sevendayBtn;

@property(nonatomic,strong) UIScrollView * scrollView;
@property(nonatomic,strong) UITableView * rklTableV1;
@property(nonatomic,strong) UITableView * rklTableV2;
@property(nonatomic,strong) NSMutableArray * oneAry;
@property(nonatomic,strong) NSMutableArray * sevenAry;

@property(nonatomic,strong) MyCenterNoDataView * nodataView1 ;
@property(nonatomic,strong) MyCenterNoDataView * nodataView2 ;

@end

@implementation RankingListVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = _modelTitle.length==0?@"排行榜":_modelTitle;
   
    self.view.backgroundColor = COLORWHITE ;
    
    _oneAry   = [NSMutableArray array];
    _sevenAry = [NSMutableArray array];

    [self creatViewBtn]; // 顶部单日和七日选择按钮
    
    [self scrollView];
    
    [self clickWaySelectOne];
    
    [self nodataView1];
    [self nodataView2];
}

// 榜单按钮
-(void)creatViewBtn{
    _backView = [[UIView alloc]init];
    [self.view addSubview:_backView];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);  make.height.offset(40*HWB);
        make.left.offset(0); make.right.offset(0);
    }];
    [self onedayBtn];
    [self sevendayBtn];
    
    UIImageView * linesImgV = [[UIImageView alloc]init];
    linesImgV.backgroundColor = Black204 ;
    [_backView addSubview:linesImgV];
    [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backView.mas_centerX);
        make.centerY.equalTo(_backView.mas_centerY);
        make.height.offset(20*HWB); make.width.offset(1);
    }];
}

-(UIButton*)onedayBtn{
    if (!_onedayBtn) {
        _onedayBtn = [UIButton titLe:@"今日英雄榜" bgColor:COLORCLEAR titColorN:Black102 titColorH:Black102 font:14*HWB];
        ADDTARGETBUTTON(_onedayBtn, clickWaySelectOne)
        [_backView addSubview:_onedayBtn];
        [_onedayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0); make.left.offset(0);
            make.right.equalTo(_backView.mas_centerX).offset(-0.5);
        }];
        _onedayBtn.selected = YES ;
    }
    return _onedayBtn ;
}
-(void)clickWaySelectOne{
    _onedayBtn.selected = YES ;
    _sevendayBtn.selected = NO ;
    [_onedayBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    [_sevendayBtn setTitleColor:Black102 forState:UIControlStateNormal];
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    [self huoquRankingList:@{@"type":@"1"}];
}

-(UIButton*)sevendayBtn{
    if (!_sevendayBtn) {
        _sevendayBtn = [UIButton titLe:@"七日英雄榜" bgColor:COLORCLEAR titColorN:Black102 titColorH:Black102 font:14*HWB];
         ADDTARGETBUTTON(_sevendayBtn, clickWaySelectSeven)
        [_backView addSubview:_sevendayBtn];
        [_sevendayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0); make.right.offset(0);
            make.left.equalTo(_backView.mas_centerX).offset(0.5);
        }];
        _sevendayBtn.selected = NO ;
    }
    return _sevendayBtn ;
}
-(void)clickWaySelectSeven{
    _onedayBtn.selected = NO ;
    _sevendayBtn.selected = YES ;
    [_onedayBtn setTitleColor:Black102 forState:UIControlStateNormal];
    [_sevendayBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    [_scrollView setContentOffset:CGPointMake(ScreenW, 0) animated:NO];
    [self huoquRankingList:@{@"type":@"2"}];
}

#pragma mark === >>> 获取排行榜数据
-(void)huoquRankingList:(NSDictionary*)dic{
    
    _nodataView1.hidden = YES ;
    _nodataView2.hidden = YES ;
    
    // 已请求，最新数据
    if ([dic[@"type"] integerValue]==1&&_oneAry.count > 0) {
        [_rklTableV1 reloadData];
        return ;
    }
    if ([dic[@"type"] integerValue]==2&&_sevenAry.count > 0) {
        [_rklTableV2 reloadData];
        return ;
    }
    
    // 有缓存，先读缓存
    if ([dic[@"type"] integerValue]==1&&NSUDTakeData(OneDayRanking)!=nil) {
        NSArray * aryone = NSUDTakeData( OneDayRanking ) ;
        [aryone enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            RankingModel * model = [[RankingModel alloc]initWith:obj];
            [_oneAry addObject:model] ;
        }];
       
        [_rklTableV1 reloadData];
    }
    if ([dic[@"type"] integerValue]==2&&NSUDTakeData( SevenDayRanking )!=nil) {
        NSArray * aryone = NSUDTakeData( SevenDayRanking ) ;
        [aryone enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            RankingModel * model = [[RankingModel alloc]initWith:obj];
            [_sevenAry addObject:model] ;
        }];
        [_rklTableV2 reloadData];
    }


    // 请求最新数据  有可能请求不到数据 所以做缓存
    [NetRequest requestTokenURL:url_ranking_list parameter:dic success:^(NSDictionary *nwdic) {
        
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
    
            NSArray  * ary =  NULLARY( nwdic[@"list"] ) ;

            if ([dic[@"type"] integerValue]==1) {
                [_oneAry removeAllObjects];
                if (ary.count>0) { NSUDSaveData(ary, OneDayRanking) }
                
            }if ([dic[@"type"] integerValue]==2) {
                [_sevenAry removeAllObjects];
                if (ary.count>0) { NSUDSaveData(ary, SevenDayRanking) }
            }
            
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                RankingModel * model = [[RankingModel alloc]initWith:obj];
                if ([dic[@"type"] integerValue]==1) { [_oneAry addObject:model]; }
                if ([dic[@"type"] integerValue]==2) { [_sevenAry addObject:model]; }
            }];
            
            if ([dic[@"type"] integerValue]==1) {
                if (_oneAry.count>0) { _nodataView1.hidden = YES ; } else { _nodataView1.hidden = NO ; }
                [_rklTableV1 reloadData];
            }
            
            if ([dic[@"type"] integerValue]==2) {
                if (_sevenAry.count>0) { _nodataView2.hidden = YES ; } else { _nodataView2.hidden = NO ; }
                [_rklTableV2 reloadData];
            }
            
        }else{
           SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        if (_oneAry.count==0&&_sevenAry.count==0) {
            [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
                [self huoquRankingList:dic];
            }];
        }

    }];
}

-(UIScrollView*)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.backgroundColor = COLORWHITE ;
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.tag = 1000 ;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(ScreenW*2,0);
        [self.view addSubview:_scrollView];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(40*HWB);   make.bottom.offset(0);
            make.left.offset(0);  make.right.offset(0);
        }];
    }
    [self rklTableV1];  [self rklTableV2];
    
    return _scrollView ;
}
#pragma mark ===>>> UIScrollView 代理
// 使用代码改变偏移量时执行
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView.tag==1000) { // 滑动的是scrollView
       [self diaoYongRequest];
    }
}
// 手动滑动结束后执行
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag==1000) { // 滑动的是scrollView
        [self diaoYongRequest];
    }
}
-(void)diaoYongRequest{
    NSInteger  stag = _scrollView.contentOffset.x/ScreenW ;
    if (stag==0) { [self clickWaySelectOne]; }
    if (stag==1) { [self clickWaySelectSeven]; }
}

#pragma mark === >>> UITableVew 创建
-(UITableView*)rklTableV1{
    if (!_rklTableV1) {
        _rklTableV1 = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _rklTableV1.frame = CGRectMake(0, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT-40*HWB);
        _rklTableV1.backgroundColor = COLORWHITE ;
        _rklTableV1.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rklTableV1.delegate = self;  _rklTableV1.dataSource = self;
        _rklTableV1.showsVerticalScrollIndicator = NO ;
        _rklTableV1.tag = 101 ;
        [_scrollView addSubview:_rklTableV1];
        [_rklTableV1 registerClass:[NewRankCell class] forCellReuseIdentifier:@"rankcell"];
        [_rklTableV1 registerClass:[ChampionCell class] forCellReuseIdentifier:@"championcell"];
    }
    return _rklTableV1;
}
-(UITableView*)rklTableV2{
    if (!_rklTableV2) {
        _rklTableV2 = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _rklTableV2.frame = CGRectMake(ScreenW, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT-40*HWB);
        _rklTableV2.backgroundColor = COLORWHITE ;
        _rklTableV2.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rklTableV2.delegate = self;  _rklTableV2.dataSource = self;
        _rklTableV2.showsVerticalScrollIndicator = NO ;
        _rklTableV2.tag = 102 ;
        [_scrollView addSubview:_rklTableV2];
        [_rklTableV2 registerClass:[NewRankCell class] forCellReuseIdentifier:@"rankcell"];
        [_rklTableV2 registerClass:[ChampionCell class] forCellReuseIdentifier:@"championcell"];
    }
    return _rklTableV2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag==101) {
        if (_oneAry.count>2) { return _oneAry.count-2 ; }
    }
    if (tableView.tag==102) {
        if (_sevenAry.count>2) { return _sevenAry.count-2 ; }
    }
    return 0 ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) { return 155*HWB; }
    return 56*HWB;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        ChampionCell * championCell = [tableView dequeueReusableCellWithIdentifier:@"championcell" forIndexPath:indexPath];
        if (tableView.tag==101) { championCell.rankListAry = _oneAry ; }
        if (tableView.tag==102) { championCell.rankListAry = _sevenAry;}
        return championCell ;
    }
//    if (indexPath.row==1) {
//         NewRankCell * rankCell = [tableView dequeueReusableCellWithIdentifier:@"rankcell" forIndexPath:indexPath];
//         rankCell.contentView.backgroundColor=LOGOCOLOR;
//         rankCell.model = _oneAry[indexPath.row+2];
//         rankCell.rank.text = FORMATSTR(@"%d",999);
//         return rankCell;
//    }
    NewRankCell * rankCell = [tableView dequeueReusableCellWithIdentifier:@"rankcell" forIndexPath:indexPath];
    rankCell.contentView.backgroundColor=COLORWHITE;
    if (tableView.tag==101) { rankCell.model = _oneAry[indexPath.row+2]; }
    if (tableView.tag==102) { rankCell.model = _sevenAry[indexPath.row+2]; }
    rankCell.rank.text = FORMATSTR(@"%ld",(long)(indexPath.row+3));
    return rankCell;
}



#pragma mark ===>>> 无数据时显示
-(MyCenterNoDataView*)nodataView1{
    if (!_nodataView1) {
        
        _nodataView1 = [[MyCenterNoDataView alloc]init];
        _nodataView1.imgNameStr = @"我的好友";
        _nodataView1.tishiStr = @"你还没有英雄榜";
        _nodataView1.btnStr = @"立刻去推荐好友吧" ;
        ADDTARGETBUTTON(_nodataView1.tishiBtn, clickRecFriend)
        [_rklTableV1 addSubview:_nodataView1];
        [_nodataView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_rklTableV1.mas_centerX);
            make.centerY.equalTo(_rklTableV1.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        
        _nodataView1.hidden = YES ;
        
    }
    return _nodataView1 ;
}

#pragma mark ===>>> 无数据时显示
-(MyCenterNoDataView*)nodataView2{
    if (!_nodataView2) {
        
        _nodataView2 = [[MyCenterNoDataView alloc]init];
        _nodataView2.imgNameStr = @"我的好友";
        _nodataView2.tishiStr = @"你还没有英雄榜";
        _nodataView2.btnStr = @"立刻去推荐好友吧" ;
        ADDTARGETBUTTON(_nodataView2.tishiBtn, clickRecFriend)
        [_rklTableV2 addSubview:_nodataView2];
        [_nodataView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_rklTableV2.mas_centerX);
            make.centerY.equalTo(_rklTableV2.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        
        _nodataView2.hidden = YES ;
        
    }
    return _nodataView2 ;
}
-(void)clickRecFriend{
    [MyPageJump pushNextVcByModel:nil byType:51];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
