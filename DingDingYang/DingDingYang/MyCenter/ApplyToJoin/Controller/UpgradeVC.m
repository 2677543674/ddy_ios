//
//  UpgradeVC.m
//  DingDingYang
//
//  Created by ddy on 2017/12/27.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "UpgradeVC.h"
#import <AlipaySDK/AlipaySDK.h>
#import "AgentRightModel.h" // 价格选择的model

@interface UpgradeVC () <UITextFieldDelegate>

@property(nonatomic,strong) UIImageView * upImageView ;
@property(nonatomic,strong) UIView * upBgView ;       // 放下面的那一堆升级按钮
@property(nonatomic,strong) UIButton * remdWayUpBtn ; // 推荐多少位好友升级按钮
@property(nonatomic,strong) UIView * codeUpBgView ;   // 通过某码升级的底部
@property(nonatomic,strong) UITextField * codeField ; // 升级码输入框
@property(nonatomic,strong) UIButton * codeUpBtn ;    // 升级按钮
@property(nonatomic,strong) UIButton * moneyUpBtn ;   // 支付升级
@property(nonatomic,assign) CGRect  rectCodeF ;       // 记录输入框转化后相对于 window 的坐标

@end

@implementation UpgradeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORWHITE ;
    
    self.title = _modelTitle.length==0?@"申请加入":_modelTitle;
    
    _priceAry = [NSMutableArray array];
    
    [self upImageView];
    [self upBgView];
    
    //注册键盘高度变化通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    //支付宝支付结果
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(receiveNotificiation:) name:AlipayPayRusult object:nil];

    if (NSUDTakeData(ApplyUpgrade)) {
        NSDictionary * dic = (NSDictionary*)NSUDTakeData(ApplyUpgrade);
        [self parsingDataDic:dic];
    }
    
    [self queryUpgradeIns];
}

// 查询升级信息
-(void)queryUpgradeIns{
  
    [NetRequest requestTokenURL:url_get_Upgrade_Ins parameter:nil success:^(NSDictionary *nwdic) {
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
//            for (UIView * subV in _upBgView.subviews) { [subV removeFromSuperview]; }
            NSUDSaveData(nwdic, ApplyUpgrade)
            [self parsingDataDic:nwdic];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
         SHOWMSG(failure)
    }];
}

// 解析数据
-(void)parsingDataDic:(NSDictionary*)nwdic{
    NSString * url = REPLACENULL(nwdic[@"proxyPooster"]) ;
    NSURL * imgurl = [NSURL URLWithString:FORMATSTR(@"%@%@",[url hasPrefix:@"http"]?@"":dns_dynamic_loadimg,url)] ;
    
    [_upImageView sd_setImageWithURL:imgurl completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            _upImageView.image = [UIImage imageNamed:@"new_join_daili"] ;
        }else{
            if (image.size.height>image.size.width) {
                [UIView animateWithDuration:0.26 animations:^{
                    [_upImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.offset(0);  make.right.offset(0);
                        make.top.offset(18*HWB);   make.height.offset(ScreenW);
                    }];
                    [_upImageView layoutIfNeeded];
                }];
            }else{
                [UIView animateWithDuration:0.26 animations:^{
                    [_upImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.offset(0);  make.right.offset(0); make.top.offset(0);
                        make.height.offset(image.size.height/image.size.width*ScreenW);
                    }];
                    [_upImageView layoutIfNeeded];
                }];
            }
        }
    }];
    
    
    // 是否是正式代理
    _ifUpgrade   = [REPLACENULL(nwdic[@"ifUpgrade"]) integerValue];
    
    // 免费升级的字段
    _ifFree      = [REPLACENULL(nwdic[@"ifFree"]) integerValue];
    _ifNeedFree  = [REPLACENULL(nwdic[@"ifNeedFree"]) integerValue];
    _freeIns = REPLACENULL(nwdic[@"freeIns"]);
    _freeMsg = REPLACENULL(nwdic[@"freeMsg"]);
    
    // 代理码升级的字段
    _ifCode = [REPLACENULL(nwdic[@"ifCode"]) integerValue];
    _onCode = REPLACENULL(nwdic[@"onCode"]) ;
    _codeMsg = REPLACENULL(nwdic[@"codeMsg"]) ;
    
    // 价格升级的字段
    NSArray * list = NULLARY(nwdic[@"list"]) ;
    [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        AgentRightModel * model = [[AgentRightModel alloc]initWithDic:obj];
        [_priceAry addObject:model] ;
    }];
    
    // 请求到数据后 再 创建升级按钮
    [self remdWayUpBtn];
    
    [self codeUpBgView];
    
    [self moneyUpBtn];
    
    if (_ifUpgrade==1) { // 已经升过级了，按钮不能点击了(全部变为灰色)
        [self upSuccessViewStates];
    }
    
    // 隐藏
    if (_ifFree==1) { _remdWayUpBtn.hidden = YES ; }
    if (_ifCode==1) { _codeUpBgView.hidden = YES ; }
    if (_priceAry.count==0) { _moneyUpBtn.hidden = YES ; }
    
}




// 升级正式代理成功! (所有按钮变灰)
-(void)upSuccessViewStates{

    _remdWayUpBtn.backgroundColor = Black204 ;
    _remdWayUpBtn.enabled = NO ;
    [_remdWayUpBtn setTitleColor:COLORWHITE forState:(UIControlStateNormal)];
    
    _codeUpBgView.userInteractionEnabled = NO ;
    _codeField.userInteractionEnabled = NO ;
    _codeUpBgView.layer.borderColor = Black204.CGColor ;
    _codeUpBtn.backgroundColor = Black204 ;
    [_codeUpBtn setTitleColor:COLORWHITE forState:(UIControlStateNormal)];
    
    _moneyUpBtn.enabled = NO ;
    _moneyUpBtn.backgroundColor = Black204 ;
    [_moneyUpBtn boardWidth:1 boardColor:Black204 corner:18*HWB];
    [_moneyUpBtn setTitleColor:COLORWHITE forState:(UIControlStateNormal)];
}

// 上方的图片
-(UIImageView*)upImageView{
    if (!_upImageView) {
        _upImageView = [[UIImageView alloc]init];
        _upImageView.image = [UIImage imageNamed:@"new_join_daili"];
        _upImageView.contentMode = UIViewContentModeScaleAspectFit ;
        [self.view addSubview:_upImageView];
        [_upImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0);
            make.top.offset(0);   make.height.offset(ScreenW/1.237);
        }];
    }
    return _upImageView ;
}

// view 用来放三个升级按钮
-(UIView*)upBgView{
    if (!_upBgView) {
        _upBgView = [[UIView alloc]init];
        [self.view addSubview:_upBgView];
        [_upBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0) ;  make.right.offset(0) ;
            make.bottom.offset(-20*HWB);
#if DingDangDingDang
            make.top.equalTo(_upImageView.mas_bottom).offset(20) ;
#else
            make.top.equalTo(_upImageView.mas_bottom).offset(0) ;
#endif
            
        }];
    }
    return _upBgView ;
}


-(UIButton*)remdWayUpBtn{
    if (!_remdWayUpBtn) {
        _remdWayUpBtn = [UIButton titLe:_freeMsg bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13*HWB];
        _remdWayUpBtn.layer.masksToBounds = YES ;
        _remdWayUpBtn.layer.cornerRadius = 18*HWB ;
        ADDTARGETBUTTON(_remdWayUpBtn, remdWayUp)
        [_upBgView addSubview:_remdWayUpBtn];
        [_remdWayUpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(18*HWB);   make.height.offset(36*HWB);
            make.left.offset(36*HWB);  make.right.offset(-36*HWB);
        }];
        if (THEMECOLOR==2) { // 主题色调为浅色
            [_remdWayUpBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }
    }
    
    
    if (_ifFree==0&&_ifNeedFree==0) { // 提供免费升级
        _remdWayUpBtn.backgroundColor = Black204 ;
        _remdWayUpBtn.enabled = NO ;
    }
   
    return _remdWayUpBtn ;
}


-(UIView*)codeUpBgView{
    if (!_codeUpBgView) {
        _codeUpBgView = [[UIView alloc]init];
        _codeUpBgView.layer.masksToBounds = YES ;
        _codeUpBgView.layer.cornerRadius = 18*HWB ;
        _codeUpBgView.layer.borderColor = LOGOCOLOR.CGColor ;
        _codeUpBgView.layer.borderWidth = 1 ;
        _codeUpBgView.backgroundColor = COLORGROUP ;
        [_upBgView addSubview:_codeUpBgView];
        
        [_codeUpBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (_ifFree==0) { // 提供免费升级
                make.top.equalTo(_remdWayUpBtn.mas_bottom).offset(18*HWB);
            }else{ // 不提供免费升级
                make.top.offset(18*HWB);
            }
            make.left.offset(36*HWB);  make.right.offset(-36*HWB);
            make.height.offset(36*HWB);
        }];
    }
    
    [_codeUpBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        if (_ifFree==0) { // 提供免费升级
            make.top.equalTo(_remdWayUpBtn.mas_bottom).offset(18*HWB);
        }else{ // 不提供免费升级
            make.top.offset(18*HWB);
        }
        make.left.offset(36*HWB);  make.right.offset(-36*HWB);
        make.height.offset(36*HWB);
    }];
    
    [self codeField];   [self codeUpBtn];
    
   
    
    return _codeUpBgView ;
}

-(UITextField*)codeField{
    if (!_codeField) {
        _codeField = [UITextField textColor:Black51 PlaceHorder:_onCode font:13*HWB];
        _codeField.delegate = self ;
        [_codeField addTarget:self action:@selector(returnKeyBoard) forControlEvents:UIControlEventEditingDidEndOnExit];
        [_codeUpBgView addSubview:_codeField];
        [_codeField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5);  make.right.offset(-85*HWB);
            make.centerY.equalTo(_codeUpBgView.mas_centerY);
        }];
    }
    return _codeField;
}

-(UIButton*)codeUpBtn{
    if (!_codeUpBtn) {
        _codeUpBtn = [UIButton titLe:_codeMsg bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13*HWB];
        ADDTARGETBUTTON(_codeUpBtn, codeUp)
        [_codeUpBgView addSubview:_codeUpBtn];
        [_codeUpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);  make.top.offset(0);
            make.bottom.offset(0); make.width.offset(85*HWB);
        }];
        if (THEMECOLOR==2) { // 主题色为浅色
            [_codeUpBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }
    }
    return _codeUpBtn ;
}

-(UIButton*)moneyUpBtn{
    if (!_moneyUpBtn) {
        
        AgentRightModel * model = [[AgentRightModel alloc]initWithDic:@{}] ;
        if (_priceAry.count>0) {  model = _priceAry[0]; }
        
        _moneyUpBtn = [UIButton titLe:model.ins bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:13*HWB];
        [_moneyUpBtn boardWidth:1 boardColor:LOGOCOLOR corner:18*HWB];
        ADDTARGETBUTTON(_moneyUpBtn, moneyUp)
        [_upBgView addSubview:_moneyUpBtn];
        
        if (THEMECOLOR==2) { // 主题色为浅色
            [_moneyUpBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }
        
        [_moneyUpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (_ifCode==0) { // 提供代理码升级
                make.top.equalTo(_codeUpBgView.mas_bottom).offset(18*HWB);
            }else if (_ifFree==0){ // 提供邀免费升级
                make.top.equalTo(_remdWayUpBtn.mas_bottom).offset(18*HWB);
            }else{
                make.top.offset(18*HWB);
            }
            make.left.offset(36*HWB);  make.right.offset(-36*HWB);
            make.height.offset(36*HWB);
        }];
    }
    
    AgentRightModel * model = [[AgentRightModel alloc]initWithDic:@{}] ;
    if (_priceAry.count>0) {  model = _priceAry[0]; }
    [_moneyUpBtn setTitle:model.ins forState:(UIControlStateNormal)];
    [_moneyUpBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        if (_ifCode==0) { // 提供代理码升级
            make.top.equalTo(_codeUpBgView.mas_bottom).offset(18*HWB);
        }else if (_ifFree==0){ // 提供邀免费升级
            make.top.equalTo(_remdWayUpBtn.mas_bottom).offset(18*HWB);
        }else{
            make.top.offset(18*HWB);
        }
        make.left.offset(36*HWB);  make.right.offset(-36*HWB);
        make.height.offset(36*HWB);
    }];
    
    return _moneyUpBtn ;
}

#pragma mark ===>>> 几种按钮升级事件
// 邀请多少人免费升级
-(void)remdWayUp{
    ResignFirstResponder
    MBShow(@"正在升级...")
    [NetRequest requestTokenURL:url_mian_fei_up parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
            _remdWayUpBtn.backgroundColor = Black204 ;
            _remdWayUpBtn.enabled = NO ;
            [_remdWayUpBtn setTitleColor:COLORWHITE forState:(UIControlStateNormal)];
            [self huoQuInfo];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
    

}

// 代理码升级
-(void)codeUp{
    ResignFirstResponder
    if (_codeField.text.length==0) { SHOWMSG(@"您还没有输入哦~") return ; }
    MBShow(@"正在升级...")
    [NetRequest requestTokenURL:url_up_daili_buyCode parameter:@{@"code":_codeField.text}  success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"] ) {
            [self upSuccessViewStates];
            [self huoQuInfo];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

// 支付升级
-(void)moneyUp{
    ResignFirstResponder
    
    AgentRightModel * model = [[AgentRightModel alloc]initWithDic:@{}] ;
    if (_priceAry.count>0) {  model = _priceAry[0]; }
    MBShow(@"正在加载...")
    [NetRequest requestTokenURL:url_get_alipay_payInfo parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]){
            
            NSString * payRequest = REPLACENULL(nwdic[@"payRequest"]) ;
            
            if (payRequest.length>0) { //调用支付宝支付
                [[AlipaySDK defaultService] payOrder:payRequest fromScheme:REDIRECTURL callback:^(NSDictionary *resultDic) {
                    [self payResult:[resultDic[@"resultStatus"] integerValue]];
                }];
            }else{
                SHOWMSG(@"返回信息错误,无法调用支付!")
            }
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        MBHideHUD
    }];
    
}



#pragma mark ===>>> 键盘高度变化、输入框代理
-(void)keyboardFrameChange:(NSNotification*)keyboardInfo{
    NSDictionary *KBInfo = [keyboardInfo userInfo];
    NSValue *aValue = [KBInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat  keyBoardY = keyboardRect.origin.y; //键盘Y坐标
    
//     LOG(@"键盘的Y坐标：%f  转换后的坐标 X: %f   Y: %f   H: %f   W: %f",keyBoardY,_rectCodeF.origin.x,_rectCodeF.origin.y,_rectCodeF.size.height,_rectCodeF.size.width);
    
    if (keyBoardY>=ScreenH) {
        [UIView animateWithDuration:0.26 animations:^{
           self.view.frame = CGRectMake(0,NAVCBAR_HEIGHT, ScreenW, ScreenH-NAVCBAR_HEIGHT);
        } completion:^(BOOL finished) { }];
    }else{

        [UIView animateWithDuration:0.26 animations:^{
            self.view.frame = CGRectMake(0,keyBoardY-_rectCodeF.origin.y-40*HWB+NAVCBAR_HEIGHT, ScreenW,ScreenH-NAVCBAR_HEIGHT);
        } completion:^(BOOL finished) { }];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    _rectCodeF =  [_codeField convertRect:_codeField.frame toView:[UIApplication sharedApplication].keyWindow];
}

-(void)returnKeyBoard{
    NSLog(@"键盘消失啦!") ;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


#pragma mark ==>> 支付宝支付成功的通知
-(void)receiveNotificiation:(NSNotification*)data{
    NSDictionary*dic=data.userInfo;
    NSInteger code= [dic[@"resultStatus"] integerValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self payResult:code];
    });
}

-(void)payResult:(NSInteger)code{
    switch (code) {
        case 9000:{ // 支付宝支付成功
            [self upSuccessViewStates];
            [self huoQuInfo];
        } break;
        case 4000:{ SHOWMSG(@"支付失败!") } break;
        case 6001:{ SHOWMSG(@"支付已取消!") }  break;
        case 6002:{ SHOWMSG(@"网络出错!") }  break;
        default: { SHOWMSG(@"支付出错!") } break;
    }
}



//获取个人信息
-(void)huoQuInfo{
//    MBHideHUD
    MBShow(@"正在处理...")
    [NetRequest requestType:0 url:url_user_info ptdic:nil success:^(id nwdic) {
        MBHideHUD
        NSDictionary * dic = NULLDIC(nwdic[@"user"]);
        NSUDSaveData(dic, UserInfo)
        MBSuccess(@"升级成功!")
        [[NSNotificationCenter defaultCenter] postNotificationName:UserInfoChange object:nil];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD SHOWMSG(failure)
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}



@end









