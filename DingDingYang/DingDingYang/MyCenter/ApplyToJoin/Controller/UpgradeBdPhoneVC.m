//
//  UpgradeBdPhoneVC.m
//  MiZhuan
//
//  Created by ddy on 2018/3/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "UpgradeBdPhoneVC.h"
#import "CustomLBTxtView.h"


#define TextFHi  32*HWB

@interface UpgradeBdPhoneVC ()<UITextFieldDelegate>

@property(nonatomic,strong) UIImageView * upBgView ;  // 放下面的那一堆升级按钮
@property(nonatomic,strong) CustomLBTxtView * mphoneTextF;     // 手机号输入框
@property(nonatomic,strong) CustomLBTxtView * verCodeTextF;    // 验证码输入框
@property(nonatomic,strong) UIButton * codeBtn;    // 获取验证码
@property(nonatomic,strong) UIButton * alipayBtn;  // 提交申请

@end

@implementation UpgradeBdPhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"绑定手机号" ;
    self.view.backgroundColor = COLORWHITE ;
    
    _upBgView = [[UIImageView alloc]init];
    _upBgView.userInteractionEnabled = YES ;
    _upBgView.layer.masksToBounds = YES ;
    _upBgView.layer.cornerRadius = 10*HWB ;
    [self.view addSubview:_upBgView];
    [_upBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(25*HWB); make.right.offset(-25*HWB) ;
        make.centerY.equalTo(self.view.mas_centerY).offset(-80*HWB);
        make.height.offset(100*HWB);
    }];
    
    
    _verCodeTextF = [[CustomLBTxtView alloc]init];
    _verCodeTextF.leftImg = @"yanzhengma" ;
    _verCodeTextF.placeHorder = @"请输入验证码";
    _verCodeTextF.txtField.delegate = self;
    _verCodeTextF.txtField.keyboardType = UIKeyboardTypePhonePad;
    [_upBgView addSubview:_verCodeTextF];
    [_verCodeTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(30*HWB); make.right.offset(-35*HWB);
        make.bottom.offset(-25*HWB);  make.height.offset(TextFHi);
    }];
    
    _codeBtn = [UIButton titLe:@"获取验证码" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:12*HWB];
    [_codeBtn addTarget:self action:@selector(sendValidation) forControlEvents:UIControlEventTouchUpInside];
    [_verCodeTextF addSubview:_codeBtn];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_verCodeTextF.mas_centerY);
        make.right.offset(0); make.width.offset(70*HWB);
        make.height.offset(TextFHi-2);
    }];
    
    
    _mphoneTextF = [[CustomLBTxtView alloc]init];
    _mphoneTextF.leftImg = @"shouji" ;
    _mphoneTextF.placeHorder = @"请输入手机号";
    _mphoneTextF.txtField.delegate = self;
    _mphoneTextF.txtField.keyboardType = UIKeyboardTypePhonePad;
    [_upBgView addSubview:_mphoneTextF];
    [_mphoneTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(30*HWB); make.right.offset(-30*HWB); make.height.offset(TextFHi);
        make.bottom.equalTo(_verCodeTextF.mas_top).offset(-15*HWB);
    }];
    
    [_mphoneTextF.txtField addTarget:self action:@selector(changePhone) forControlEvents:UIControlEventEditingChanged];
    [_verCodeTextF.txtField addTarget:self action:@selector(changeCode) forControlEvents:UIControlEventEditingChanged];

    [_mphoneTextF.txtField addTarget:self action:@selector(returnKeyBoard) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_verCodeTextF.txtField addTarget:self action:@selector(returnKeyBoard) forControlEvents:UIControlEventEditingDidEndOnExit];

    
    _alipayBtn = [UIButton titLe:@"绑定手机" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13*HWB];
    _alipayBtn.layer.masksToBounds = YES ;
    _alipayBtn.layer.cornerRadius = 19*HWB ;
    ADDTARGETBUTTON(_alipayBtn, bdPhoneUp)
    [self.view addSubview:_alipayBtn];
    [_alipayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(50*HWB);  make.right.offset(-50*HWB);
        make.top.equalTo(_upBgView.mas_bottom).offset(25*HWB);
        make.height.offset(38*HWB);
    }];
    
    
}

// 发送验证码
-(void)sendValidation{
    if (_mphoneTextF.txtField.text.length==0) {
        SHOWMSG(@"请输入手机号!")
        return ;
    }
    if (JUDGESTR(PHONE, _mphoneTextF.txtField.text)) {
        MBShow(@"正在发送...")
        NSDictionary * pdic = @{@"mobile":_mphoneTextF.txtField.text,@"t":@"3"};
        [NetRequest requestType:0 url:url_send_code_type ptdic:pdic success:^(id nwdic) {
            MBHideHUD  [_codeBtn startCountDown];
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            MBHideHUD  SHOWMSG(failure)
        }];
        
    }else{
        SHOWMSG(@"请检查手机号是否正确!")
    }
}


-(void)bdPhoneUp{
    if (_mphoneTextF.txtField.text.length==0) { SHOWMSG(@"请输入手机号!") return ; }
    if (_verCodeTextF.txtField.text.length==0) { SHOWMSG(@"请输入验证码!")  return ; }
    if (JUDGESTR(PHONE, _mphoneTextF.txtField.text)==NO) { SHOWMSG(@"请检查手机号是否正确!") return ; }
    NSDictionary * pdic = @{@"mobile":_mphoneTextF.txtField.text,@"randCode":_verCodeTextF.txtField.text};
    MBShow(@"正在提交...")
    [NetRequest requestType:0 url:url_upload_upins2 ptdic:pdic success:^(id nwdic) {
        MBHideHUD  MBSuccess(@"绑定成功!")
        [[NSNotificationCenter defaultCenter] postNotificationName:UserInfoChange object:nil];
        [self performSelector:@selector(goBackMainVC) withObject:self afterDelay:1];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
}

-(void)goBackMainVC{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark ===>>> 输入框监控

-(void)changePhone{ // 手机输入框文字发生变化、去空格
    _mphoneTextF.txtField.text = [_mphoneTextF.txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (JUDGESTR(PHONE, _mphoneTextF.txtField.text)) {
        [_mphoneTextF.txtField resignFirstResponder];
        [_verCodeTextF.txtField becomeFirstResponder];
    }
}
-(void)changeCode{ // 验证码框内容变化 、
    _verCodeTextF.txtField.text = [_verCodeTextF.txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

-(void)returnKeyBoard{  NSLog(@"键盘消失啦!") ; }

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
