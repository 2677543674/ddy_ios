//
//  UpgradeVC.h
//  DingDingYang
//
//  Created by ddy on 2017/12/27.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface UpgradeVC : DDYViewController


@property(nonatomic,copy) NSString * proxyPooster ; // 海报图片地址

// 是否升级为正式代理 0:没有升级(免费升级不属于正式升级) 1:已升级
@property(nonatomic,assign) NSInteger ifUpgrade ;  // 全局判断

// 免费升级阶段
@property(nonatomic,assign) NSInteger ifFree ;     // 0:提供免费升级功能  1:不提供免费升级试功能
@property(nonatomic,assign) NSInteger ifNeedFree ; // 用户是否已升级试用代理 0:已升过级  1:还未升级
@property(nonatomic,copy) NSString * freeIns ;     // 免费升级试用代理说明 (邀请多少人或达到什么条件升级)
@property(nonatomic,copy) NSString * freeMsg ;     // 免费升级显,示在按钮上的文字

// 代理码升级
@property(nonatomic,assign) NSInteger ifCode ;      // 0:有代理码升级功能  1:无代理码升级功能
@property(nonatomic,copy) NSString * onCode ;       // 通过某种码升级，输入框的提示文字
@property(nonatomic,copy) NSString * codeMsg ;      // 申请加入按钮

@property(nonatomic,strong) NSMutableArray * priceAry ; // 支付升级的价格list( 里面存放:AgentRightModel )
@property(nonatomic,copy) NSString* modelTitle;


@end
