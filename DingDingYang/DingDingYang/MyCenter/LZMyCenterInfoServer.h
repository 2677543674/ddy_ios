//
//  LZMyCenterInfoServer.h
//  DingDingYang
//
//  Created by zhaoyang on 2019/9/17.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CmnInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LZMyCenterInfoServer : NSObject

+ (LZMyCenterInfoServer *)shareInstance;

- (void)requestCommission;


@property (strong, nonatomic) CommissionModel *commissionModel;

@end

NS_ASSUME_NONNULL_END
