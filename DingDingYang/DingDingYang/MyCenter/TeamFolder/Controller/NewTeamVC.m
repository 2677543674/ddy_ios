//
//  NewTeamVC.m
//  DingDingYang
//
//  Created by ddy on 2017/12/18.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "NewTeamVC.h"

#import "TeamHeaderVew.h"
#import "TeamCell.h"
#import "TeamModel.h"
#import "RongCloudConversationViewController.h"
#import "RongcloudListViewController.h"
#import "NewShareAppVC.h"

@interface NewTeamVC ()<UITableViewDelegate,UITableViewDataSource,TeamCellDelegate>

@property(nonatomic,strong) MyCenterNoDataView * nodataView ;
@property(nonatomic,strong) NSMutableDictionary * pdic; // 参数
@property(nonatomic,strong) NSMutableArray * dataAry; // 团队所有数据
@property(nonatomic,strong) TeamHeaderVew * headerView;
@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,assign) NSInteger pageIndex; // 页码

@property(nonatomic,strong) UILabel* unReadCount;
@end

@implementation NewTeamVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = _modelTitle.length==0?@"我的团队":_modelTitle;
    self.view.backgroundColor = COLORGROUP ;
    
    _dataAry = [NSMutableArray array];
    _pageIndex = 1 ;
    [self pdic];
    
    
    _headerView = [[TeamHeaderVew alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 120*HWB)];
    ADDTARGETBUTTON(_headerView.searchBtn, clickSearch);
//    ADDTARGETBUTTON(_headerView.leftNumber, clickLeft);
//    ADDTARGETBUTTON(_headerView.centerNumber, clickCenter);
//    ADDTARGETBUTTON(_headerView.rightNumber, clickRight);
    [self.view addSubview:_headerView];
    
    [self tableView];

    [self pdic];

    [self nodataView];
    if (rongcloud_APP_Key.length>0) {
        [self rightButton];
    }
}

-(void)clickLeft{
    UIView* lineView=[self.view viewWithTag:1111];
    lineView.hidden=YES;
    UIView* lineView2=[self.view viewWithTag:2222];
    lineView2.hidden=NO;
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    [_headerView.leftNumber setBackgroundColor:COLORGROUP];
    [_headerView.centerNumber setBackgroundColor:COLORWHITE];
    [_headerView.rightNumber setBackgroundColor:COLORWHITE];
    _pdic[@"role"]=@"0";
    [self.tableView.mj_header beginRefreshing];
}

-(void)clickCenter{
    UIView* lineView=[self.view viewWithTag:1111];
    lineView.hidden=YES;
    UIView* lineView2=[self.view viewWithTag:2222];
    lineView2.hidden=YES;
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    [_headerView.leftNumber setBackgroundColor:COLORWHITE];
    [_headerView.centerNumber setBackgroundColor:COLORGROUP];
    [_headerView.rightNumber setBackgroundColor:COLORWHITE];
    _pdic[@"role"]=@"1";
    [self.tableView.mj_header beginRefreshing];
}

-(void)clickRight{
    UIView* lineView=[self.view viewWithTag:1111];
    lineView.hidden=NO;
    UIView* lineView2=[self.view viewWithTag:2222];
    lineView2.hidden=YES;
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    [_headerView.leftNumber setBackgroundColor:COLORWHITE];
    [_headerView.centerNumber setBackgroundColor:COLORWHITE];
    [_headerView.rightNumber setBackgroundColor:COLORGROUP];
    _pdic[@"role"]=@"2";
    [self.tableView.mj_header beginRefreshing];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    int totalUnreadCount = [[RCIMClient sharedRCIMClient] getTotalUnreadCount];
    if (totalUnreadCount>99) {
        _unReadCount.hidden=NO;
        _unReadCount.text=[NSString stringWithFormat:@"99+"];
    }else if (totalUnreadCount<=0){
        _unReadCount.hidden=YES;
    }else{
        _unReadCount.hidden=NO;
        _unReadCount.text=[NSString stringWithFormat:@"%d",totalUnreadCount];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)clickSearch{
    ResignFirstResponder
    if (_headerView.searchText.text.length>0) {
        _pdic[@"nickName"] = _headerView.searchText.text;
    }else{
        [_pdic removeObjectForKey:@"nickName"];
    }
    UIView* lineView=[self.view viewWithTag:1111];
    lineView.hidden=NO;
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    [_headerView.leftNumber setBackgroundColor:COLORWHITE];
    [_headerView.centerNumber setBackgroundColor:COLORWHITE];
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark ===>>> 聊天记录
-(void)rightButton{

    UIButton* rightBtn = [[UIButton alloc]init];
    rightBtn.frame = CGRectMake(0, 0, 70, 44);

    [rightBtn setTitleColor:Black51 forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [rightBtn setTitle:@"聊天记录" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(liaoTianListClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    //未读消息提醒红点
    int totalUnreadCount = [[RCIMClient sharedRCIMClient] getTotalUnreadCount];
    _unReadCount=[UILabel labText:[NSString stringWithFormat:@"%d",totalUnreadCount] color:[UIColor whiteColor] font:12];
    _unReadCount.layer.cornerRadius=8;
    _unReadCount.clipsToBounds=YES;
    _unReadCount.textAlignment=NSTextAlignmentCenter;
    _unReadCount.backgroundColor=[UIColor redColor];
    [rightBtn addSubview:_unReadCount];
    [_unReadCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(rightBtn.mas_right);
        make.centerY.equalTo(rightBtn.mas_top).offset(5);
        make.height.offset(16);
        make.width.height.offset(24);
    }];
    if (totalUnreadCount<=0) {
        _unReadCount.hidden=YES;
    }else{
        _unReadCount.hidden=NO;
    }
    
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)liaoTianListClick{
    RongcloudListViewController * chatList = [[RongcloudListViewController alloc] init];
    [self.navigationController pushViewController:chatList animated:YES];
}

#pragma mark ===>>> 请求参数，请求数据
-(NSMutableDictionary*)pdic{
    if (!_pdic) {
        _pdic = [NSMutableDictionary dictionary];
        _pdic[@"level"] = @"1" ;
        _pdic[@"p"] = @"1";
    }
    return _pdic ;
}

-(void)queryMyTeamData{
    
    [NetRequest requestTokenURL:url_my_team parameter:_pdic success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray*list = nwdic[@"list"];
            
            if (_pageIndex==1) { //
                _headerView.role0Str = REPLACENULL(nwdic[@"countProxy"]);
//                _headerView.role1Str = REPLACENULL(nwdic[@"countNormal"]);
//                _headerView.remdpStr = @"??";
            }
            
            if (list.count>0) {
                _pageIndex++;
                [list enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL *  stop) {
                    TeamModel*model=[[TeamModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
                 [_tableView.mj_footer endRefreshing];
            } else{
                [_tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
            [_tableView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        [_tableView.mj_header endRefreshing];
        
        if (_dataAry.count==0) {  _nodataView.hidden = NO ; }
        else{ _nodataView.hidden = YES ; }
        
        [_tableView reloadData];
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.tableView chs:^(NSInteger click) {
            [self queryMyTeamData];
        }];
        [_tableView.mj_footer endRefreshing];
        [_tableView.mj_header endRefreshing];
    }];
    
}

#pragma mark ===>>> 无数据时显示
-(MyCenterNoDataView*)nodataView{
    if (!_nodataView) {
        
        _nodataView = [[MyCenterNoDataView alloc]init];
        _nodataView.imgNameStr = @"我的好友";
        _nodataView.tishiStr = @"你还没有好友";
        _nodataView.btnStr = @"立刻去推荐好友吧" ;
        ADDTARGETBUTTON(_nodataView.tishiBtn, clickRecFriend)
        [_tableView addSubview:_nodataView];
        [_nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tableView.mas_centerX);
            make.centerY.equalTo(_tableView.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        
        _nodataView.hidden = YES ;
        
    }
    return _nodataView ;
}

-(void)clickRecFriend{
    NewShareAppVC * shareR = [NewShareAppVC new];
    PushTo(shareR, YES);
}


#pragma mark ===>>> 创建表格
-(UITableView *)tableView{
    if (!_tableView) {
        CGRect rect = CGRectMake(0, 124*HWB, ScreenW, ScreenH-(124*HWB+NAVCBAR_HEIGHT)) ;
        _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
        _tableView.delegate = self;  _tableView.dataSource=self;
        _tableView.backgroundColor = COLORGROUP ;
        _tableView.rowHeight = 68*HWB;
        [self.view addSubview:_tableView];
        
        [_tableView registerClass:[TeamCell class] forCellReuseIdentifier:@"TeamCell"];
        
        __weak typeof(self) weakSelf=self;
        _tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            _pageIndex = 1 ;
            [weakSelf.dataAry removeAllObjects];
            [weakSelf.tableView.mj_footer resetNoMoreData];
            weakSelf.pdic[@"p"] = FORMATSTR(@"%ld",(long)_pageIndex);
            [weakSelf queryMyTeamData];
        }];
        
        _tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            weakSelf.pdic[@"p"] = FORMATSTR(@"%ld",(long)_pageIndex);
            [weakSelf queryMyTeamData];
        }];
        
        [_tableView.mj_header beginRefreshing];
        
    }else{
        [_tableView reloadData];
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataAry.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TeamCell*cell=[tableView dequeueReusableCellWithIdentifier:@"TeamCell" forIndexPath:indexPath];
    cell.delegate = self ;
    cell.model = _dataAry[indexPath.row];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    ResignFirstResponder
}

#pragma mark ===>>> 点击执行聊天
-(void)clickUser:(TeamModel *)modelTeam{
    if (modelTeam.IMUserid.length==0) {
        SHOWMSG(@"该用户未登录");
        return;
    }
    RongCloudConversationViewController *conversationVC = [[RongCloudConversationViewController alloc]init];
    conversationVC.conversationType = 1;
    conversationVC.targetId = modelTeam.IMUserid;
    conversationVC.title = [modelTeam.nickName isEqualToString:@"名称未设置"]?modelTeam.mobile:modelTeam.nickName;
    [self.navigationController setNavigationBarHidden:NO animated: YES];
    RCUserInfo *userinfo = [[RCUserInfo alloc]initWithUserId:modelTeam.IMUserid name:[modelTeam.nickName isEqualToString:@"名称未设置"]?modelTeam.mobile:modelTeam.nickName portrait:modelTeam.headImg] ;
    conversationVC.userinfo=userinfo;
    [[RCIM sharedRCIM] refreshUserInfoCache:userinfo withUserId:userinfo.userId];
    
    [self.navigationController pushViewController:conversationVC animated:YES];

}



@end
