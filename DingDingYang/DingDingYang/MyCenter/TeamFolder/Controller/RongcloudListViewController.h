//
//  RongcloudListViewController.h
//  RongCloudTest
//
//  Created by ddy on 2017/6/26.
//  Copyright © 2017年 Dong. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

@interface RongcloudListViewController : RCConversationListViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

+(void)registerRongCloud;
@end
