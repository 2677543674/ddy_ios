//
//  RongcloudListViewController.m
//  RongCloudTest
//
//  Created by ddy on 2017/6/26.
//  Copyright © 2017年 Dong. All rights reserved.
//

#import "RongcloudListViewController.h"
#import "RongCloudConversationViewController.h"
@interface RongcloudListViewController ()<RCIMClientReceiveMessageDelegate>

@end

@implementation RongcloudListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"聊天列表"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
     [MobClick endLogPageView:@"聊天列表"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self.navigationController setNavigationBarHidden:NO animated: YES];
    self.navigationController.navigationBar.hidden=NO;
    //设置需要显示哪些类型的会话
    self.title = @"聊天列表";
    
    [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE),
                                        @(ConversationType_CHATROOM)]];
//                                        @(ConversationType_DISCUSSION),
//,@(ConversationType_GROUP),
//                                        @(ConversationType_APPSERVICE),
//                                        @(ConversationType_SYSTEM)]
    //设置需要将哪些类型的会话在会话列表中聚合显示
    [self setCollectionConversationType:@[@(ConversationType_DISCUSSION),
                                          @(ConversationType_GROUP)]];

    self.conversationListTableView.tableFooterView=[UIView new];
    self.isShowNetworkIndicatorView = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)onSelectedTableRow:(RCConversationModelType)conversationModelType
         conversationModel:(RCConversationModel *)model
               atIndexPath:(NSIndexPath *)indexPath {
    RongCloudConversationViewController *conversationVC = [[RongCloudConversationViewController alloc]init];
    conversationVC.conversationType =model.conversationType;
    conversationVC.targetId = model.targetId;
    conversationVC.title = model.conversationTitle;
    
    [self.navigationController pushViewController:conversationVC animated:YES];
}

+(void)registerRongCloud{
    // 省见、宝拉甄选
    if (PID==10025||PID==10106) { return; }
    
    [[RCIM sharedRCIM] initWithAppKey:rongcloud_APP_Key];
    PersonalModel *infoModel=[[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
    
    [RCIM sharedRCIM].enablePersistentUserInfoCache=YES;
    [RCIM sharedRCIM].enableMessageAttachUserInfo=YES;
    NSLog(@"%@",infoModel.rongcloudToken);
    [[RCIM sharedRCIM] connectWithToken:infoModel.rongcloudToken  success:^(NSString *userId) {
        NSLog(@"登陆成功。当前登录的用户ID：%@", userId);
    } error:^(RCConnectErrorCode status) {
        NSLog(@"登陆的错误码为:%ld", (long)status);
    } tokenIncorrect:^{
        //token过期或者不正确。
        //如果设置了token有效期并且token过期，请重新请求您的服务器获取新的token
        //如果没有设置token有效期却提示token错误，请检查您客户端和服务器的appkey是否匹配，还有检查您获取token的流程。
        NSLog(@"token错误");
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
