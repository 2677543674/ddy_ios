//
//  RongCloudConversationViewController.m
//  DingDingYang
//
//  Created by ddy on 2017/6/28.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "RongCloudConversationViewController.h"


@interface RongCloudConversationViewController ()

@end

@implementation RongCloudConversationViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [MobClick beginLogPageView:@"聊天"];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"聊天"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self.navigationController setNavigationBarHidden:NO animated: YES];
    self.navigationController.navigationBar.hidden=NO;
    [self.chatSessionInputBarControl.pluginBoardView removeItemWithTag:PLUGIN_BOARD_ITEM_LOCATION_TAG];
 
    PersonalModel *infoModel=[[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
         RCUserInfo *userinfo=[[RCUserInfo alloc]init];
        userinfo.userId = infoModel.cid;
        userinfo.name =infoModel.taobaoAccount;
        userinfo.portraitUri=infoModel.headImg ;
         [RCIM sharedRCIM].currentUserInfo=userinfo;
//        NSLog(@"infoModel.taobaoAccount:%@\ninfoModel.headImg:%@\n infoModel.cid=%@",infoModel.taobaoAccount,infoModel.headImg,infoModel.cid);
//   
//    NSLog(@"%@",[[RCIM sharedRCIM] getUserInfoCache:self.targetId]);
    
//    [self createFMDB];
}


//-(void)createFMDB{
//    NSString *doc =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)  lastObject];
//    
//    NSString *fileName = [doc stringByAppendingPathComponent:@"userinfo.sqlite"];
////    
////    if ([[NSFileManager defaultManager] fileExistsAtPath:fileName]==NO) {
////         NSLog(@"未创建表格！");
//        //2.获得数据库
//        FMDatabase *db = [FMDatabase databaseWithPath:fileName];
//        
//        //3.使用如下语句，如果打开失败，可能是权限不足或者资源不足。通常打开完操作操作后，需要调用 close 方法来关闭数据库。在和数据库交互 之前，数据库必须是打开的。如果资源或权限不足无法打开或创建数据库，都会导致打开失败。
//        if ([db open])
//        {
//            //4.创表
//            
//            BOOL result = [db executeUpdate:@"CREATE TABLE IF NOT EXISTS t_userinfo (userID integer PRIMARY KEY AUTOINCREMENT, name text NOT NULL, urlImg integer NOT NULL);"];
//            if (result)
//            {
//                NSLog(@"创建表成功");
//            }
//            
//            
//        }
//         RCUserInfo *userinfo=[[RCIM sharedRCIM] getUserInfoCache:self.targetId];
//         [db executeUpdate:@"INSERT INTO t_userinfo (userID,name,urlImg) VALUES (?,?,?);",userinfo.userId,userinfo.name,userinfo.portraitUri];
//    
//         [db close];
//
////    }else{
////        
////        //2.获得数据库
////        FMDatabase *db = [FMDatabase databaseWithPath:fileName];
////        
////        //3.使用如下语句，如果打开失败，可能是权限不足或者资源不足。通常打开完操作操作后，需要调用 close 方法来关闭数据库。在和数据库交互 之前，数据库必须是打开的。如果资源或权限不足无法打开或创建数据库，都会导致打开失败。
////        if ([db open])
////        {
////            //4.创表
////                NSLog(@"打开表成功");
////
////        }
////        RCUserInfo *userinfo=[[RCIM sharedRCIM] getUserInfoCache:self.targetId];
////        [db executeUpdate:@"INSERT INTO t_userinfo (userID,name,urlImg) VALUES (?,?,?);",userinfo.userId,userinfo.name,userinfo.portraitUri];
////        
////        [db close];
////
////
////    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
