//
//  RongCloudConversationViewController.h
//  DingDingYang
//
//  Created by ddy on 2017/6/28.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

@interface RongCloudConversationViewController : RCConversationViewController

@property(nonatomic,strong)RCUserInfo *userinfo;
@end
