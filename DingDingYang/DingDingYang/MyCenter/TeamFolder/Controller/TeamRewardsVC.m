
//
//  TeamRewardsVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/9.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "TeamRewardsVC.h"
#import "TeamModel.h"
#import "TeamPersonnelCell.h"
#import "NewShareAppVC.h"

@interface TeamRewardsVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) MyCenterNoDataView * nodataView ;
@property(nonatomic,strong) NSMutableDictionary * pdic; // 参数
@property(nonatomic,strong) NSMutableArray * dataAry; // 所有数据
@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,assign) NSInteger pageIndex; // 页码

@end

@implementation TeamRewardsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _modelTitle.length==0?@"推广奖励明细":_modelTitle;
    self.view.backgroundColor = COLORGROUP ;

    [self pdic];

    [self tableView];
    [self nodataView];

}

-(NSMutableDictionary*)pdic{
    if (!_pdic) {
        _dataAry = [NSMutableArray array] ;
        _pageIndex = 1 ;
        _pdic = [NSMutableDictionary dictionary];
        _pdic[@"level"] = @"1" ;
        _pdic[@"p"] = @"1" ;
    }
    return _pdic ;
}

#pragma mark ===>>> 查询数据
//第一页加载
-(void)queryMyTeamRewareds{
    _pageIndex = 2;  _pdic[@"p"] = @"1" ;
    [_tableView.mj_footer resetNoMoreData];
    [NetRequest requestTokenURL:url_team_reward parameter:_pdic success:^(NSDictionary *nwdic) {
        if (_dataAry.count>0) {
            [_dataAry removeAllObjects];
            [_tableView reloadData];
        }

        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray*list= NULLARY( nwdic[@"list"] ) ;
            if (list.count>0) {
                [list enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL *  stop) {
                    TeamModel*model=[[TeamModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
            }else{
            
            }
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
        [_tableView.mj_header endRefreshing];
        [_tableView reloadData];
        
       
    } failure:^(NSString *failure) {
        [_tableView.mj_header endRefreshing];
        SHOWMSG(failure)
    }];
}
//加载更多
-(void)loadMore{
    NSString*page=FORMATSTR(@"%ld",(long)_pageIndex);
    _pdic[@"p"]=page;
    [NetRequest requestTokenURL:url_team_reward parameter:_pdic success:^(NSDictionary *nwdic) {
        
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray*list= NULLARY( nwdic[@"list"] ) ;
            if (list.count>0) {
                _pageIndex++;
                [list enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL *  stop) {
                    TeamModel*model=[[TeamModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
                [_tableView.mj_footer endRefreshing];
            } else{
                [_tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
            [_tableView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        [_tableView reloadData];
    } failure:^(NSString *failure) {
         [_tableView.mj_footer endRefreshing];
        SHOWMSG(failure)
    }];
}

-(void)queryCommissionData{
    
    [NetRequest requestTokenURL:url_team_reward parameter:_pdic success:^(NSDictionary *nwdic) {
        
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray*list = NULLARY( nwdic[@"list"] ) ;
            if (list.count>0) {
                _pageIndex++;
                [list enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL *  stop) {
                    TeamModel * model = [[TeamModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
                [_tableView.mj_footer endRefreshing];
            } else{
                [_tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
            [_tableView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        
        [_tableView.mj_header endRefreshing];
        [_tableView reloadData];
        
        if (_dataAry.count==0) { _nodataView.hidden = NO ; }
        else{ _nodataView.hidden = YES ; }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
    }];
}

#pragma mark -- tableView表格
-(UITableView*)tableView{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, ScreenW, ScreenH-NAVCBAR_HEIGHT) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate=self; _tableView.dataSource=self;
        [self.view addSubview:_tableView];
        
        [_tableView registerClass:[TeamPersonnelCell class] forCellReuseIdentifier:@"TeamPersonnelCell"];
        
        //下拉刷新
        __weak TeamRewardsVC * selfView = self ;
        
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [selfView.dataAry removeAllObjects];
            [selfView.tableView.mj_footer resetNoMoreData];
            selfView.pageIndex = 1 ;
            selfView.pdic[@"p"] = FORMATSTR(@"%ld",(long)selfView.pageIndex);
            [selfView queryCommissionData];
        }];
        
        _tableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pdic[@"p"] = FORMATSTR(@"%ld",(long)selfView.pageIndex);
            [selfView loadMore];
        }];
        
        [_tableView.mj_header beginRefreshing] ;
    }
    return _tableView ;
}

#pragma mark ===>>> tableView代理

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataAry.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 76;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeamPersonnelCell*cell=[tableView dequeueReusableCellWithIdentifier:@"TeamPersonnelCell" forIndexPath:indexPath];
    cell.jiangLiModel=_dataAry[indexPath.row];
    return cell;
}


#pragma mark ===>>> 没有数据时，提示去推荐好友
-(MyCenterNoDataView*)nodataView{
    if (!_nodataView) {
        
        _nodataView = [[MyCenterNoDataView alloc]init];
        _nodataView.imgNameStr = @"哭";
        _nodataView.tishiStr = @"你还没有奖励";
        _nodataView.btnStr = @"立即去推荐好友吧" ;
        ADDTARGETBUTTON(_nodataView.tishiBtn, nodataClickGotoShare)
        [_tableView addSubview:_nodataView];
        [_nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tableView.mas_centerX);
            make.centerY.equalTo(_tableView.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        
        _nodataView.hidden = YES ;
        
    }
    return _nodataView ;
}


-(void)nodataClickGotoShare{
    [MyPageJump pushNextVcByModel:nil byType:51];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




@end
