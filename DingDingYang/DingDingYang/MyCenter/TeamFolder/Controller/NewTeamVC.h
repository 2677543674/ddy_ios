//
//  NewTeamVC.h
//  DingDingYang
//
//  Created by ddy on 2017/12/18.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@class PersonalModel ;
@interface NewTeamVC : DDYViewController

@property(nonatomic,strong) PersonalModel * pModel ;
@property(nonatomic,copy) NSString* modelTitle;

@end
