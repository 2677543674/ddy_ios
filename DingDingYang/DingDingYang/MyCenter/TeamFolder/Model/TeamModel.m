//
//  TeamModel.m
//  DingDingYang
//
//  Created by ddy on 2017/3/9.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "TeamModel.h"

@implementation TeamModel
-(instancetype)initWithDic:(NSDictionary *)dic{
    self=[super init];
    if (self) {
        self.headImg =  [REPLACENULL(dic[@"headImg"]) hasPrefix:@"http"]?dic[@"headImg"] : FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"headImg"]));
        self.mobile = REPLACENULL(dic[@"mobile"]);
        self.createTime = REPLACENULL(dic[@"createTime"]);
        self.role = [REPLACENULL(dic[@"role"]) integerValue];
        self.nickName = [REPLACENULL(dic[@"nickName"]) stringByRemovingPercentEncoding];
        self.userId = REPLACENULL(dic[@"id"]);
        self.commission = ROUNDED(REPLACENULL(dic[@"commission"]), 2) ;
        self.childs = REPLACENULL(dic[@"childs"]);
        self.consumes = REPLACENULL(dic[@"consumes"]);
        self.proxys = REPLACENULL(dic[@"proxys"]);
        self.ifActivate = [REPLACENULL(dic[@"ifActivate"]) integerValue];
        self.IMUserid = REPLACENULL(dic[@"imUserId"]);
        self.roleName = REPLACENULL(dic[@"roleName"]) ;
        self.taobaoAccount = [REPLACENULL(dic[@"taobaoAccount"]) stringByRemovingPercentEncoding];
        self.rcImToken=REPLACENULL(dic[@"rcImToken"]);
    }
    return self;
}
@end
