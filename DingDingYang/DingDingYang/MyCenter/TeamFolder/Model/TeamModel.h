//
//  TeamModel.h
//  DingDingYang
//
//  Created by ddy on 2017/3/9.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamModel : NSObject
@property(nonatomic,copy) NSString*headImg;//头像
@property(nonatomic,copy) NSString*mobile;//手机号
@property(nonatomic,copy) NSString*createTime;//时间
@property(nonatomic,copy) NSString*childs;//推荐
@property(nonatomic,copy) NSString*proxys;//推荐会员
@property(nonatomic,copy) NSString*consumes;//推荐消费者
//@property(nonatomic,copy)NSString*proxys;//推荐会员
@property(nonatomic,assign)NSInteger role;//用户等级(0消费者 1代理商)
@property(nonatomic,copy) NSString*nickName;//昵称
@property(nonatomic,copy) NSString*userId;//用户id
@property(nonatomic,copy) NSString*commission;//佣金
@property(nonatomic,assign) NSInteger ifActivate;//是否激活
@property(nonatomic,copy) NSString * roleName ; //用户等级
@property(nonatomic,copy) NSString * IMUserid ; //聊天ID
@property(nonatomic,copy) NSString * taobaoAccount;
@property(nonatomic,copy) NSString * rcImToken;
-(instancetype)initWithDic:(NSDictionary*)dic;
@end
