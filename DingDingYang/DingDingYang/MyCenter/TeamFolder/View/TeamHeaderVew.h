//
//  TeamHeaderVew.h
//  DingDingYang
//
//  Created by ddy on 2017/12/18.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamHeaderVew : UIView

@property(nonatomic,strong) UITextField * searchText;
@property(nonatomic,strong) UIButton* searchBtn;
@property(nonatomic,strong) UIButton * leftNumber;
@property(nonatomic,strong) UIButton * centerNumber;
@property(nonatomic,strong) UIButton * rightNumber;

@property(nonatomic,copy) NSString * role0Str ;
@property(nonatomic,copy) NSString * role1Str ;
@property(nonatomic,copy) NSString * remdpStr ;

@end
