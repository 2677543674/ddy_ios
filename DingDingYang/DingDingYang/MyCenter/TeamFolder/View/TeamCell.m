//
//  TeamCell.m
//  DingDingYang
//
//  Created by ddy on 2017/12/18.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "TeamCell.h"

@implementation TeamCell

-(void)setModel:(TeamModel *)model{
    
    _model = model;
    
    [_headerImageV sd_setImageWithURL:[NSURL URLWithString:model.headImg] placeholderImage:[UIImage imageNamed:@"header_null_image"] completed:nil];
    
    _nameLab.text = model.nickName;
    _phoneLab.text = model.mobile;
    _roleLab.text = FORMATSTR(@"  %@  ",_model.roleName);
    _timeLab.text = model.createTime;
    
//    NSMutableParagraphStyle  * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    [paragraphStyle  setLineSpacing:8];
//    NSString * text = FORMATSTR(@"%@：%@人\n%@：%@人",role_name_by(@"0"),model.consumes,role_name_by(@"1"),model.proxys);
//    NSMutableAttributedString  *mString = [[NSMutableAttributedString alloc] initWithString:text];
//    [mString  addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
//    [_numberLab setAttributedText:mString];
    
    _numberLab.text = FORMATSTR(@"%@：%@人",role_name_by(@"0"),model.proxys);
    
}



-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        
        self.backgroundColor = COLORWHITE ;
        [self headerImageV];

        [self nameLab];
        
#if DingDangDingDang
        [self phoneLab];
#endif
        [self roleLab];
        [self timeLab];
        
        if (rongcloud_APP_Key.length>0) {
            [self chatBtn];
        }

        [self numberLab];

        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.bottom.offset(0);
            make.height.offset(1);
        }];
    }
    return self ;
}



-(UIImageView *)headerImageV{
    if (!_headerImageV) {
        _headerImageV = [UIImageView new];
        _headerImageV.image = [UIImage imageNamed:@"logo"] ;
        _headerImageV.layer.cornerRadius = 5*HWB;
        _headerImageV.clipsToBounds = YES;
        _headerImageV.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_headerImageV];
        [_headerImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(6*HWB);   make.bottom.offset(-6*HWB);
            make.left.offset(10*HWB);  make.width.equalTo(_headerImageV.mas_height);
        }];
        
        
    }
    return _headerImageV;
}

-(UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab = [UILabel labText:@"昵称" color:Black51 font:12*HWB];
        [self.contentView addSubview:_nameLab];
        [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(8*HWB);
            make.width.lessThanOrEqualTo(@(80*HWB));
            make.left.equalTo(_headerImageV.mas_right).offset(8*HWB);
        }];
    }
    return _nameLab;
}

-(UILabel *)roleLab{
    if (!_roleLab) {
        _roleLab = [UILabel labText:@"消费者" color:Black102 font:10.5*HWB];
        _roleLab.layer.borderColor = LOGOCOLOR.CGColor;
        _roleLab.layer.cornerRadius = 8*HWB;
        _roleLab.layer.borderWidth = 0.5;
        _roleLab.clipsToBounds = YES;
        [self.contentView addSubview:_roleLab];
        [_roleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            if (_phoneLab!=nil) {
                make.centerY.equalTo(_nameLab.mas_centerY).offset(0); make.height.offset(16*HWB);
                make.left.equalTo(_nameLab.mas_right).offset(5*HWB);
            }else{
                make.centerY.offset(0); make.height.offset(16*HWB);
                make.left.equalTo(_headerImageV.mas_right).offset(8*HWB);
            }
            
        }];
    }
    return _roleLab;
}

-(UILabel *)phoneLab{
    if (!_phoneLab) {
        _phoneLab = [UILabel labText:@"手机号" color:Black102 font:12*HWB];
        [self.contentView addSubview:_phoneLab];
        [_phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.offset(0); make.height.offset(16*HWB);
            make.left.equalTo(_headerImageV.mas_right).offset(8*HWB);
            
        }];
    }
    return _roleLab;
}

-(UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [UILabel labText:@"时间" color:Black102 font:12*HWB];
        [self.contentView addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-8*HWB);
            make.left.equalTo(_headerImageV.mas_right).offset(8*HWB);
        }];
        
    }
    return _nameLab;
}


-(UILabel *)numberLab{
    if (!_numberLab) {
        _numberLab=[UILabel new];
        _numberLab.font=[UIFont systemFontOfSize:12*HWB];
        _numberLab.textColor=Black102;
        _numberLab.text=@"推荐0人";
        _numberLab.numberOfLines=0;
        
        [self.contentView addSubview:_numberLab];
        [_numberLab mas_makeConstraints:^(MASConstraintMaker *make) {
            if (_chatBtn==nil) {
                make.left.offset(ScreenW*0.618);
            }else{
                if (_phoneLab!=nil) {
                    make.right.equalTo(_chatBtn.mas_left);
                }else{
                    make.left.offset(ScreenW/2.0);
                }
            }
            make.centerY.offset(0);
        }];
        
    }
    return _numberLab;
}


-(UIButton *)chatBtn{
    if (!_chatBtn) {
        _chatBtn = [UIButton new];
        if (THEMECOLOR==2) {  _chatBtn.tintColor = OtherColor ;} // 主题色为浅色，渲染色改为黑色

        [_chatBtn setImage:[[UIImage imageNamed:@"聊天图标"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)] forState:UIControlStateNormal];
        _chatBtn.imageEdgeInsets=UIEdgeInsetsMake(0, 0, 0, -18);
        [_chatBtn addTarget:self action:@selector(clickOpenChat) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_chatBtn];
        [_chatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);
            make.centerY.equalTo(self.contentView);
            make.width.offset(70*HWB);
            make.height.offset(50);
 
        }];
    }
    return _chatBtn;
}


//聊天
-(void)clickOpenChat{
    [self.delegate clickUser:_model];
}















@end
