//
//  TeamCell.h
//  DingDingYang
//
//  Created by ddy on 2017/12/18.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamModel.h"

@protocol TeamCellDelegate <NSObject>

@optional

-(void)clickUser:(TeamModel*)modelTeam;

@end

@interface TeamCell : UITableViewCell

@property(nonatomic,strong) UILabel * nameLab;
@property(nonatomic,strong) UILabel * timeLab;
@property(nonatomic,strong) UILabel * roleLab;
@property(nonatomic,strong) UILabel * numberLab;
@property(nonatomic,strong) UILabel * phoneLab;
@property(nonatomic,strong) UIButton * chatBtn;
@property(nonatomic,strong) UIImageView * headerImageV;
@property(nonatomic,strong) TeamModel * model;
@property(nonatomic,assign) id<TeamCellDelegate>delegate ;

@end
