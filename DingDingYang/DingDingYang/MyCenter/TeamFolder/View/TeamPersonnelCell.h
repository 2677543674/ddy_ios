//
//  TeamPersonnelCell.h
//  DingDingYang
//
//  Created by ddy on 2017/3/9.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamModel.h"

@protocol TeamMembersDelegate <NSObject>

@optional

-(void)clickUser:(TeamModel*)modelTeam;

@end

@interface TeamPersonnelCell : UITableViewCell



@property(nonatomic,strong) UIImageView * headImgV;
@property(nonatomic,strong) UILabel * phoneLab;
@property(nonatomic,strong) UILabel * levelLab;
@property(nonatomic,strong) UILabel * timeLab;
@property(nonatomic,strong) UILabel * moneyLab;//佣金
@property(nonatomic,strong) UILabel * countLab;//显示推荐人数(一级显示)
@property(nonatomic,strong) UILabel * ifActivateLab;//是否激活（只显示未激活用户）

@property(nonatomic,strong)TeamModel*model;//团队人员
@property(nonatomic,strong)TeamModel*jiangLiModel;//团队收益
@property(nonatomic,strong)PersonalModel*infoModel;

@property(nonatomic,assign) id<TeamMembersDelegate>delegate ;

@end
