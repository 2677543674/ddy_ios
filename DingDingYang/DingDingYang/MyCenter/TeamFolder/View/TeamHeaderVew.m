//
//  TeamHeaderVew.m
//  PeanutLK
//
//  Created by mac on 2017/12/18.
//  Copyright © 2017年 cj.All rights reserved.
//

#import "TeamHeaderVew.h"

@implementation TeamHeaderVew

// 消费者
-(void)setRole0Str:(NSString *)role0Str{
    _role0Str = role0Str ;
    NSString * leftString = FORMATSTR(@"%@\n%@人",role_name_by(@"0"),_role0Str);
    [_leftNumber setAttributedTitle:[self setNumberOfSpace:leftString] forState:UIControlStateNormal];
}

// 消费者
-(void)setRole1Str:(NSString *)role1Str{
    _role1Str = role1Str ;
//    NSString * centerNumber = FORMATSTR(@"%@\n%@人",role_name_by(@"0"),_role1Str);
//    [_centerNumber setAttributedTitle:[self setNumberOfSpace:centerNumber] forState:UIControlStateNormal];
}

// 运营商
-(void)setRemdpStr:(NSString *)remdpStr{
    _remdpStr = remdpStr ;
//    NSString * rightNumber = FORMATSTR(@"%@\n%@",role_name_by(@"2"),_remdpStr);
//    [_rightNumber setAttributedTitle:[self setNumberOfSpace:rightNumber] forState:UIControlStateNormal];
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=COLORGROUP;
        UIView * backView = [UIView new];
        backView.backgroundColor = LOGOCOLOR ;
        [self addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0);  make.height.offset(90*HWB);
        }];
        
        UIView * searchView = [UIView new];
        searchView.backgroundColor = COLORGROUP;
        searchView.layer.cornerRadius = 16*HWB;
        searchView.clipsToBounds = YES;
        [backView addSubview:searchView];
        [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(15*HWB);  make.height.offset(32*HWB);
            make.left.offset(30*HWB); make.right.offset(-30*HWB);
        }];
        
        _searchBtn = [UIButton new];
        [_searchBtn setBackgroundColor:COLORWHITE];
        UIImage * newImg = [[UIImage imageNamed:@"myf_search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_searchBtn setImage:newImg forState:UIControlStateNormal];
        [searchView addSubview:_searchBtn];
        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);   make.bottom.offset(0);
            make.right.offset(0); make.width.offset(55*HWB);
        }];
        
        _searchText = [UITextField textColor:Black51 PlaceHorder:@"查询昵称" font:13*HWB];
        [searchView addSubview:_searchText];
        [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.centerY.equalTo(searchView.mas_centerY);
            make.right.equalTo(_searchBtn.mas_left).offset(-2);
        }];
        
        UIView* numberShadowView=[UIView new];
        numberShadowView.backgroundColor=[UIColor whiteColor];
        numberShadowView.layer.cornerRadius=4;
        numberShadowView.layer.shadowColor=[UIColor blackColor].CGColor;
        numberShadowView.layer.shadowOpacity=0.15;
        numberShadowView.layer.shadowRadius=4;
        numberShadowView.layer.shadowOffset=CGSizeMake(2, 1);
        [self addSubview:numberShadowView];
        [numberShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(30*HWB);
            make.right.offset(-30*HWB);
            make.centerY.equalTo(backView.mas_bottom);
            make.height.offset(56*HWB);
        }];
        
        UIView* numberView=[UIView new];
        numberView.backgroundColor=[UIColor whiteColor];
        numberView.layer.cornerRadius=4;
        numberView.clipsToBounds=YES;
        
        [numberShadowView addSubview:numberView];
        [numberView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.top.offset(0);
            
        }];
        
//        UIView* lineView=[UIView new];
//        lineView.tag=1111;
//        lineView.backgroundColor=[UIColor groupTableViewBackgroundColor];
//        [numberView addSubview:lineView];
//        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(10);
//            make.left.offset((ScreenW-60*HWB)/2.0);
//            make.width.offset(1);
//            make.bottom.offset(-10);
//        }];
//
//        UIView* lineView2=[UIView new];
//        lineView2.tag=2222;
//        lineView2.backgroundColor=[UIColor groupTableViewBackgroundColor];
//        [numberView addSubview:lineView2];
//        [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(10);
//            make.left.offset((ScreenW-60*HWB)*2.0/3.0);
//            make.width.offset(1);
//            make.bottom.offset(-10);
//        }];
//        lineView2.hidden=YES;
        
        _leftNumber=[UIButton new];
        _leftNumber.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
        _leftNumber.titleLabel.textColor=Black51;
        [_leftNumber setTitleColor:Black51 forState:UIControlStateNormal];
        _leftNumber.titleLabel.numberOfLines=0;
        [numberView addSubview:_leftNumber];
        [_leftNumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
//            make.right.equalTo(lineView.mas_left).offset(0);
            make.bottom.offset(0);
            make.right.offset(0);
        }];
        
//        _centerNumber=[UIButton new];
//        _centerNumber.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
//        _centerNumber.titleLabel.textColor=Black51;
//        _centerNumber.titleLabel.numberOfLines=0;
//        [numberView addSubview:_centerNumber];
//
//
//        [_centerNumber mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(0);
//            make.left.equalTo(lineView.mas_right).offset(0);
//            make.right.offset(0);
//            make.bottom.offset(0);
//        }];
//
//        _rightNumber=[UIButton new];
//        _rightNumber.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
//        _rightNumber.titleLabel.textColor=Black51;
//        _rightNumber.titleLabel.numberOfLines=0;
//        [numberView addSubview:_rightNumber];
//        [_rightNumber mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(0);
//            make.right.offset(0);
//            make.left.equalTo(lineView2.mas_right).offset(0);
//            make.bottom.offset(0);
//        }];
//        _rightNumber.hidden = YES;
        
        [_leftNumber setAttributedTitle:[self setNumberOfSpace:FORMATSTR(@"%@\n0人",role_name_by(@"0"))] forState:UIControlStateNormal];
//        [_centerNumber setAttributedTitle:[self setNumberOfSpace:FORMATSTR(@"%@\n0人",role_name_by(@"1"))] forState:UIControlStateNormal];
//        [_rightNumber setAttributedTitle:[self setNumberOfSpace:FORMATSTR(@"%@\n0人",role_name_by(@"2"))] forState:UIControlStateNormal];
        
        //添加运营商
        //#if DingDangDingDang
        //        [lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        //            make.left.offset((ScreenW-60*HWB)*1.0/3.0);
        //        }];
        //
        //        lineView2.hidden=NO;
        //        [_centerNumber mas_updateConstraints:^(MASConstraintMaker *make) {
        //            make.right.equalTo(lineView2.mas_left).offset(0);
        //        }];
        //
        //        _rightNumber.hidden=NO;
        //#endif
        
    }
    return self;
}

-(NSMutableAttributedString*)setNumberOfSpace:(NSString *)string{
    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle  setLineSpacing:5];
    paragraphStyle.alignment=NSTextAlignmentCenter;
    NSMutableAttributedString  *mString = [[NSMutableAttributedString alloc] initWithString:string];
    [mString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
    return mString;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}


@end
