//
//  TeamPersonnelCell.m
//  DingDingYang
//
//  Created by ddy on 2017/3/9.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "TeamPersonnelCell.h"

@implementation TeamPersonnelCell

// 团队，旧的、已无用
-(void)setInfoModel:(PersonalModel *)infoModel{
    _infoModel=infoModel;
    for (NSDictionary * dic in infoModel.roleListAry) {
        if ([REPLACENULL(dic[@"role"]) integerValue]==_model.role) {
            _levelLab.text = FORMATSTR(@"  %@  ",dic[@"name"]);
            break;
        }
    }
}

-(void)setModel:(TeamModel *)model{
    _model = model;
    _moneyLab.hidden=YES;   _countLab.hidden=YES;  _ifActivateLab.hidden = YES;
    
    [_headImgV sd_setImageWithURL:[NSURL URLWithString:_model.headImg] placeholderImage:PlaceholderImg];
    
    _phoneLab.text=model.mobile;
    _timeLab.text=model.createTime;
    
    if ([model.childs integerValue]>0&&self.accessoryType==UITableViewCellAccessoryDisclosureIndicator) {
        _countLab.hidden=NO;
        _countLab.text = FORMATSTR(@"推荐 %@ 人",model.childs);
    }
    
    if  (model.ifActivate==0) {
        _ifActivateLab.hidden = NO;
        if (self.accessoryType==UITableViewCellAccessoryDisclosureIndicator) {
            [_ifActivateLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.offset(5);  make.height.offset (20);
                make.centerY.equalTo(self.contentView.mas_centerY);

            }];
        }else{
            [_ifActivateLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.offset(-15);  make.height.offset (20);
                make.centerY.equalTo(self.contentView.mas_centerY);
            }];
        }
    }
    
    if (_model.roleName.length>0) {
        _levelLab.text = FORMATSTR(@"  %@  ",_model.roleName);
    }else{
        if   (_model.role==1) { _levelLab.text=@"  代理商  "; }
        else { _levelLab.text=@"  消费者  "; }
    }
    
    if (THEMECOLOR==2){
        _countLab.textColor=Black51;
        _levelLab.textColor=Black51;
        _levelLab.layer.borderColor=Black51.CGColor;
        _ifActivateLab.textColor=Black51;
    }
    
}

// 团队奖励 (代理佣金推广奖励)
-(void)setJiangLiModel:(TeamModel *)jiangLiModel{
    _jiangLiModel=jiangLiModel;
    _levelLab.hidden=YES;
    
    [_headImgV sd_setImageWithURL:[NSURL URLWithString:jiangLiModel.headImg] placeholderImage:PlaceholderImg];
    
    _phoneLab.text=jiangLiModel.mobile;
    
    _timeLab.text=jiangLiModel.createTime;
    
    NSString*jiangLi = FORMATSTR(@"  奖励:¥%@  ",jiangLiModel.commission);
    _moneyLab.text=jiangLi;
    
//    NSDictionary*dic=NULLDIC(NSUDTakeData(@"USERINFO"));
//    NSInteger  role=[STR(dic[@"role"]) integerValue];
//    if (role==0&&_model.role==1) {
//        NSRange titleRange = {0,jiangLi.length};
//        NSMutableAttributedString * mtitle = [[NSMutableAttributedString alloc] initWithString:jiangLi];
//        
//        [mtitle addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
//        
//        _moneyLab.attributedText = mtitle ;
//    }
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        [self headImgV];
        
        [self phoneLab];
        
        [self levelLab];
        
        [self timeLab];
        
        [self moneyLab];
        
        [self countLab];
        
        [self ifActivateLab];
        
        if (CANOPENAlipay==YES||CANOPENWechat==YES) {
            _levelLab.hidden = NO ;
        }else{
            _levelLab.hidden = YES ;
        }
        
        UIImageView * linesImgV  = [[UIImageView alloc]init];
        linesImgV.backgroundColor = Black204 ;
        [self.contentView addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.bottom.offset(0); make.height.offset(0.5);
        }];
        
        if (THEMECOLOR==2) { // 主题色调为浅色
            _moneyLab.textColor = OtherColor ;
        }
        
    }
    return self;
}

//头像
-(UIImageView*)headImgV{
    if (!_headImgV) {
        _headImgV=[[UIImageView alloc]initWithImage:PlaceholderImg];
        _headImgV.backgroundColor = COLORGROUP ;
        _headImgV.layer.shouldRasterize = YES;
        _headImgV.layer.rasterizationScale = [UIScreen mainScreen].scale;
        _headImgV.layer.masksToBounds = YES;
        _headImgV.layer.cornerRadius = 28 ;
        _headImgV.contentMode = UIViewContentModeScaleAspectFill;
        _headImgV.userInteractionEnabled = YES ;
        UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickOpenWhat)];
        [_headImgV addGestureRecognizer:tap];
        
        [self.contentView addSubview:_headImgV];
        [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10); make.centerY.equalTo(self.mas_centerY);
            make.height.offset(56); make.width.offset(56);
        }];
    }
    return _headImgV ;
}

//手机号
-(UILabel*)phoneLab{
    if (!_phoneLab) {
        _phoneLab=[UILabel labText:@"" color:Black51 font:FONT_15];
        [self.contentView addSubview:_phoneLab];
        [_phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(8);
            make.bottom.equalTo(self.mas_centerY).offset(-5);
            make.height.offset(15);
        }];
    }
    return _phoneLab ;

}
//等级
-(UILabel*)levelLab{
    if (!_levelLab) {
        _levelLab=[UILabel labText:@"消费者" color:LOGOCOLOR font:FONT_14];
        _levelLab.layer.cornerRadius=9;
        [_levelLab boardWidth:1 boardColor:LOGOCOLOR];
//        _levelLab.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:_levelLab];
        [_levelLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_phoneLab.mas_right).offset(6);
            make.height.offset(18);
//            make.width.offset(55);
            make.centerY.equalTo(_phoneLab.mas_centerY);
        }];
    }
    return _levelLab ;
}

//时间
-(UILabel*)timeLab{
    if (!_timeLab) {
        _timeLab=[UILabel labText:@"" color:Black51 font:FONT_15];
        [self.contentView addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(8);
            make.top.equalTo(self.mas_centerY).offset(5);
            make.height.offset(15);
        }];
    }
    return _timeLab ;
}

//奖励
-(UILabel*)moneyLab{
    if (!_moneyLab) {
        _moneyLab=[UILabel labText:@"奖励:¥0.0" color:LOGOCOLOR font:FONT_15];
        _moneyLab.backgroundColor = COLORWHITE;
        [self.contentView addSubview:_moneyLab];
        [_moneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10); make.centerY.equalTo(_timeLab.mas_centerY);
            make.height.offset(15);
        }];
    }
    return _moneyLab ;
}

//推荐总人数
-(UILabel*)countLab{
    if (!_countLab) {
        _countLab = [UILabel labText:@"推荐0人" color:LOGOCOLOR font:FONT_14];
        _countLab.hidden = YES;
        [self.contentView addSubview:_countLab];
        [_countLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(5);  make.height.offset (20);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
    }
    return _countLab ;
}
//激活状态
-(UILabel*)ifActivateLab{
    if (!_ifActivateLab) {
        _ifActivateLab = [UILabel labText:@"未登录" color:LOGOCOLOR font:FONT_14];
        _ifActivateLab.hidden = YES;
        [self.contentView addSubview:_ifActivateLab];
        [_ifActivateLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(5);  make.height.offset (20);
            make.centerY.equalTo(self.contentView.mas_centerY);
            
        }];
    }
    return _ifActivateLab ;
}

//头像点击事件
-(void)clickOpenWhat{
    [self.delegate clickUser:_model];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
