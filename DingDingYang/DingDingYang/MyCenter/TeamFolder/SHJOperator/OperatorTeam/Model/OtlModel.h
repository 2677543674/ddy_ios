//
//  OtlModel.h
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OtlModel : NSObject

@property(nonatomic,copy) NSString * consumeCount; // 消费者数
@property(nonatomic,copy) NSString * proxyCount;   // 代理数
@property(nonatomic,copy) NSString * createTime;   // 加入时间
@property(nonatomic,copy) NSString * headImg;      // 头像
@property(nonatomic,copy) NSString * name;         // 昵称

-(instancetype)initWithDic:(NSDictionary*)dic;

@end

NS_ASSUME_NONNULL_END
