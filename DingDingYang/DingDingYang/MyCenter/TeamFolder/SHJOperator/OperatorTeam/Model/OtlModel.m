//
//  OtlModel.m
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "OtlModel.h"

@implementation OtlModel




-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        
        self.consumeCount = REPLACENULL(dic[@"consumeCount"]);
        self.proxyCount = REPLACENULL(dic[@"proxyCount"]);
        self.createTime = REPLACENULL(dic[@"createTime"]);
        
        if ([REPLACENULL(dic[@"headImg"]) hasPrefix:@"http"]) {
            self.headImg =  REPLACENULL(dic[@"headImg"]);
        }else{
            self.headImg = FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"headImg"]));
        }
        
        self.name = REPLACENULL(dic[@"name"]);
    }
    return self;
}

@end
