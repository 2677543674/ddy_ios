//
//  OperatorTeamListVC.m
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "OperatorTeamListVC.h"
#import "OtlCell.h"
#import "OtlModel.h"

@interface OperatorTeamListVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,strong) UIView * levelView;
@property(nonatomic,strong) UIButton * levelBtn; // 按钮
@property(nonatomic,strong) UIImageView * lineImgV;
@property(nonatomic,assign) NSInteger selectTag ; // 记录选择的按钮

@property(nonatomic,strong) NSMutableDictionary * pdic; // 参数
@property(nonatomic,assign) NSInteger pageIndex; // 页码
@property(nonatomic,strong) NSMutableArray * dataAry; // 所有数据

@property(nonatomic,strong) UIView * bottomView;
@property(nonatomic,strong) UILabel * numALab; // A级个数
@property(nonatomic,strong) UILabel * numBLab; // B级个数



@end

@implementation OperatorTeamListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLORGROUP ;

    self.title = FORMATSTR(@"%@团队",role_name_by(@"2"));
    
    _pdic = [NSMutableDictionary dictionary];
    _pdic[@"level"] = @"1" ;
    
    _dataAry = [NSMutableArray array];
    _pageIndex = 1; _selectTag = 0;
    
    // 叮当叮当不要B级运营商
    if (PID!=10102) {
        [self levelView];
        [self creatBottomView];
    }
    
    [self tableView];
    
}

-(UIView*)levelView{
    if (!_levelView) {
        _levelView = [[UIView alloc]init];
        _levelView.frame = CGRectMake(0, 1, ScreenW, 42);
        [self.view addSubview:_levelView];
        NSArray * jibieAry = @[FORMATSTR(@"A级%@",role_name_by(@"2")),FORMATSTR(@"B级%@",role_name_by(@"2"))];
        for (NSInteger i = 0 ; i<jibieAry.count ; i++) {
            _levelBtn = [UIButton titLe:jibieAry[i] bgColor:COLORWHITE titColorN:Black51 titColorH:COLORGRAY font:14.5*HWB];
            _levelBtn.frame=CGRectMake(ScreenW/jibieAry.count*i, 0 , ScreenW/jibieAry.count, 42);
            ADDTARGETBUTTON(_levelBtn, clickSelect:)
            _levelBtn.tag = 10 + i ;
            _levelBtn.selected = NO ;
            [_levelView addSubview:_levelBtn];
            if (i==0) {
                _levelBtn.selected = YES;
                [_levelBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
            }
        }
    }
    [self lineImgV];
    return _levelView;
}

// 下划线
-(UIImageView*)lineImgV{
    if (!_lineImgV) {
        _lineImgV = [[UIImageView alloc]init];
        _lineImgV.backgroundColor = LOGOCOLOR ;
        _levelBtn = (UIButton*)[_levelView viewWithTag:10];
        _lineImgV.center = CGPointMake(_levelBtn.center.x, 41);
        _lineImgV.bounds = CGRectMake(0, 0, 46*HWB , 2);
        [self.view addSubview:_lineImgV];
    }
    
    return _lineImgV;
}

-(void)creatBottomView{
    _bottomView = [[UIView alloc]init];
    _bottomView.backgroundColor = LOGOCOLOR;
    [self.view addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
        make.height.offset(42*HWB+(IS_IPHONE_X?22:0));
    }];
    
    _numALab = [UILabel labText:FORMATSTR(@"A级%@0位",role_name_by(@"2")) color:COLORWHITE font:14*HWB];
    _numALab.textAlignment = NSTextAlignmentCenter;
    [_bottomView addSubview:_numALab];
    [_numALab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(0); make.height.offset(42*HWB);
        make.right.equalTo(_bottomView.mas_centerX);
    }];
    
    _numBLab = [UILabel labText:FORMATSTR(@"B级%@0位",role_name_by(@"2")) color:COLORWHITE font:14*HWB];
    _numBLab.textAlignment = NSTextAlignmentCenter;
    [_bottomView addSubview:_numBLab];
    [_numBLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.offset(0); make.height.offset(42*HWB);
        make.left.equalTo(_bottomView.mas_centerX);
    }];
    
    UIImageView * imageLineV = [[UIImageView alloc]init];
    imageLineV.backgroundColor = COLORWHITE;
    [_bottomView addSubview:imageLineV];
    [imageLineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_bottomView.mas_centerX);
        make.width.offset(1); make.height.offset(20*HWB);
        make.centerY.equalTo(_numALab.mas_centerY);
    }];
    
}

// 选中 AB 级
-(void)clickSelect:(UIButton*)sbtn{
    if ( sbtn.selected==YES ) { return ; } //不允许二次点击
    if ( sbtn.tag == 10 ) { _pdic[@"level"] = @"1" ; }
    if ( sbtn.tag == 11 ) { _pdic[@"level"] = @"2" ; }
    [self changePDic:sbtn.tag];
}

// 修改下划线位置
-(void)changePDic:(NSInteger)level{
    
    for (_levelBtn in _levelView.subviews) {
        _levelBtn.backgroundColor = COLORWHITE ;
        [_levelBtn setTitleColor:Black51 forState:UIControlStateNormal];
        _levelBtn.selected = NO ;
    }
    _levelBtn = (UIButton*)[_levelView viewWithTag:level];
    _levelBtn.selected = YES ;
    [_levelBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
 
    float lineCenterX  = _levelBtn.center.x ;
    
    [UIView animateWithDuration:0.16 animations:^{ _lineImgV.center = CGPointMake(lineCenterX, 41); }];
    
    [_tableView.mj_header beginRefreshing];
    
}

-(void)getOperatorListData{
    [NetRequest requestType:0 url:shj_url_operator_list ptdic:_pdic success:^(id nwdic) {
        
        if (_pageIndex==1) {
            _numALab.text = FORMATSTR(@"A级%@%@位",role_name_by(@"2"),REPLACENULL(nwdic[@"count1"]));
            _numBLab.text = FORMATSTR(@"B级%@%@位",role_name_by(@"2"),REPLACENULL(nwdic[@"count2"]));
            if (_dataAry.count>0) {
                [_dataAry removeAllObjects];
                [_tableView reloadData];
            }
        }
        
        NSArray * list = NULLARY(nwdic[@"list"]);
        [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            OtlModel * model = [[OtlModel alloc]initWithDic:obj];
            [_dataAry addObject:model];
        }];
        [_tableView endRefreshType:list.count isReload:YES];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        SHOWMSG(failure)
        [_tableView endRefreshType:1 isReload:YES];
    }];
}

#pragma mark ===>>> tableView表格
-(UITableView*)tableView{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self; _tableView.dataSource=self;
        _tableView.backgroundColor = COLORGROUP;
        _tableView.rowHeight = 80*HWB;
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            if (PID!=10102) {
                make.bottom.equalTo(_bottomView.mas_top);
                make.top.equalTo(_levelView.mas_bottom);
            } else { make.top.bottom.offset(0); }
        }];
        
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 6*HWB, 0);
        
        [_tableView registerClass:[OtlCell class] forCellReuseIdentifier:@"otl_cell"];
        
        // 下拉刷新
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            weakSelf.pageIndex = 1;
            weakSelf.pdic[@"p"] = @"1";
            [weakSelf getOperatorListData];
        }];
        _tableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            weakSelf.pageIndex++;
            weakSelf.pdic[@"p"] =  FORMATSTR(@"%ld",(long)weakSelf.pageIndex);
            [weakSelf getOperatorListData];
        }];
        
        [_tableView.mj_header beginRefreshing];

    }
    return _tableView ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataAry.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OtlCell * cell = [tableView dequeueReusableCellWithIdentifier:@"otl_cell" forIndexPath:indexPath];
    if (_dataAry.count>indexPath.row) {
        cell.otlModel = _dataAry[indexPath.row];
    }else{ [_tableView reloadData]; }
    return cell;
}




@end
