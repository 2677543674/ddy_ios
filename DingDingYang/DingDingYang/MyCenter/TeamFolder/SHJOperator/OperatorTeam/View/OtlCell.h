//
//  OtlCell.h
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtlModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OtlCell : UITableViewCell

@property(nonatomic,strong) UIView * backView;
@property(nonatomic,strong) UIImageView * headImgV;
@property(nonatomic,strong) UILabel * operatorLab;
@property(nonatomic,strong) UILabel * nickNameLab;
@property(nonatomic,strong) UILabel * peopleNumLab;
@property(nonatomic,strong) UILabel * jionTimeLab;

@property(nonatomic,strong) OtlModel * otlModel;

@end

NS_ASSUME_NONNULL_END
