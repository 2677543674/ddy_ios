//
//  OtlCell.m
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "OtlCell.h"

@implementation OtlCell

-(void)setOtlModel:(OtlModel *)otlModel{
    _otlModel = otlModel;
    [_headImgV sd_setImageWithURL:[NSURL URLWithString:_otlModel.headImg] placeholderImage:[UIImage imageNamed:@"header_null_image"]];
    _nickNameLab.text = _otlModel.name;
    _peopleNumLab.text = FORMATSTR(@"%@: %@人   %@: %@人",role_name_by(@"1"),_otlModel.proxyCount,role_name_by(@"0"),_otlModel.consumeCount);
    _jionTimeLab.text = _otlModel.createTime;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLORGROUP;
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = COLORWHITE;
        [_backView cornerRadius:5*HWB];
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB); make.right.offset(-10*HWB);
            make.top.offset(8*HWB); make.bottom.offset(0);
        }];
        
        _headImgV = [[UIImageView alloc]init];
        [_backView addSubview:_headImgV];
        [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.offset(5*HWB); make.bottom.offset(-5*HWB);
            make.width.equalTo(_headImgV.mas_height);
        }];
        
        _operatorLab = [UILabel labText:role_name_by(@"2") color:COLORWHITE font:12*HWB];
        _operatorLab.backgroundColor = LOGOCOLOR;
        _operatorLab.textAlignment = NSTextAlignmentCenter;
        [_operatorLab cornerRadius:9*HWB];
        [_backView addSubview:_operatorLab];
        [_operatorLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(5*HWB);
            make.top.equalTo(_headImgV.mas_top);
            make.width.offset(47*HWB);
            make.height.offset(18*HWB);
        }];
        
        _nickNameLab = [UILabel labText:@"昵称" color:COLORBLACK font:13*HWB];
        [_backView addSubview:_nickNameLab];
        [_nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_operatorLab.mas_right).offset(3*HWB);
            make.top.equalTo(_headImgV.mas_top);
            make.height.offset(18*HWB);
            make.right.offset(-5);
        }];
        
        NSString * peopleName = FORMATSTR( @"%@: 0人   %@: 0人",role_name_by(@"1"),role_name_by(@"0"));
        _peopleNumLab = [UILabel labText:peopleName color:Black51 font:12*HWB];
        [_backView addSubview:_peopleNumLab];
        [_peopleNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(5*HWB);
            make.centerY.equalTo(_backView.mas_centerY);
        }];
        
        _jionTimeLab = [UILabel labText:@"2018-08-08" color:Black153 font:12*HWB];
        [_backView addSubview:_jionTimeLab];
        [_jionTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(5*HWB);
            make.bottom.equalTo(_headImgV.mas_bottom);
        }];
        
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
