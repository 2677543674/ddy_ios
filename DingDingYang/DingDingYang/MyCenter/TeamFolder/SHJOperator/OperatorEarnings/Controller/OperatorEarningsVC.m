//
//  OperatorEarningsVC.m
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "OperatorEarningsVC.h"
#import "OeHeadCltCell.h"
#import "CmnInfoModel.h"

@interface OperatorEarningsVC () <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * moneyCltView; // 佣金详情
@property(nonatomic,strong) CommissionModel * operatorModel;
@property(nonatomic,strong) UILabel * instructionsLab;


@end

@implementation OperatorEarningsVC


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getOperatorData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = FORMATSTR(@"%@团队收益",role_name_by(@"2"));
    _operatorModel = [[CommissionModel alloc]init];
    [self instructionsLab];
    
}

-(void)getOperatorData{
    [NetRequest requestType:0 url:url_goods_wallet_dm ptdic:@{@"type":@"55"} success:^(id nwdic) {
        [_operatorModel addModelByPlatform:@"1" data:NULLDIC(nwdic[@"content"])];
        [self moneyCltView];
        _instructionsLab.text = _operatorModel.desc;
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        SHOWMSG(failure)
    }];
}


#pragma mark ===>>> 创建CollectionView
-(UICollectionView*)moneyCltView{
    if (!_moneyCltView) {
        UICollectionViewFlowLayout * layout1 = [[UICollectionViewFlowLayout alloc]init];
        layout1.minimumLineSpacing = 0 ; layout1.minimumInteritemSpacing = 0 ;
        _moneyCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout1];
        _moneyCltView.delegate = self ;  _moneyCltView.dataSource = self ;
        _moneyCltView.backgroundColor = COLORGROUP ;
        [self.view addSubview:_moneyCltView];
        [_moneyCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.offset(0);
            make.bottom.offset(-(40*HWB+(IS_IPHONE_X?32:0)));
        }];
        
        [_moneyCltView registerClass:[OeHeadCltCell class] forCellWithReuseIdentifier:@"clt_cell1"];
        [_moneyCltView registerClass:[OeMonthCltCell class] forCellWithReuseIdentifier:@"clt_cell2"];
        [_moneyCltView registerClass:[OeDayCltViewCell class] forCellWithReuseIdentifier:@"clt_day_cell"];
        
        /*
         共三个分区
         
         分区0: 显示: 收益、提现、账单订单入口
         
         分区1: 显示按月消费的数据
         
         分区2: 按天筛选的数据
         
         */
        
        
    }else{ [_moneyCltView reloadData]; }
    return _moneyCltView ;
}

#pragma mark ===>>> 表格代理
// 分区个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}

// 每个分区的cell个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (section) {
        case 0:{ return 1; } break;
        case 1:{ return _operatorModel.monthDataAry.count; } break;
        case 2:{ return 1; } break;
        default:{ return 0; } break;
    }
   
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float wi = FloatKeepInt(ScreenW-24*HWB);
    switch (indexPath.section) {
        case 0:{ return CGSizeMake(ScreenW, 160*HWB); } break;
        case 1:{ return CGSizeMake(wi/2-3, 80*HWB); } break;
        case 2:{ return CGSizeMake(wi, 46*HWB+66*HWB*1); } break;
        default:{ return CGSizeZero; } break;
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==1) {
        return UIEdgeInsetsMake(5*HWB, 12*HWB, 10*HWB, 12*HWB);
    }
    if (section==2) {
        return UIEdgeInsetsMake(0, 12*HWB, 0, 12*HWB);
    }
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==1) { return 6; }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
            
        case 0:{
            
            OeHeadCltCell * oneCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"clt_cell1" forIndexPath:indexPath];
            oneCell.moneyLab.text = _operatorModel.wallet;
            return oneCell;
            
        } break;
            
        case 1:{
            
            OeMonthCltCell * monthCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"clt_cell2" forIndexPath:indexPath];
            monthCell.monthModel = _operatorModel.monthDataAry[indexPath.item];
            return monthCell;
            
        } break;
            
        case 2:{
            OeDayCltViewCell * dayCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"clt_day_cell" forIndexPath:indexPath];
            dayCell.dayDataAry = _operatorModel.dayDataAry;
            return dayCell;
            
        } break;
        
        default:{ return nil; } break;
    }
  
}


-(UILabel*)instructionsLab{
    if (!_instructionsLab) {
        _instructionsLab = [UILabel labText:@"每月下旬结算上月预估收入，本月预估收入则在下月下旬结算" color:Black102 font:12*HWB];
        _instructionsLab.backgroundColor = COLORWHITE;
        _instructionsLab.numberOfLines = 0;
        _instructionsLab.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_instructionsLab];
        [_instructionsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.bottom.offset(IS_IPHONE_X?-32:0);
            make.height.offset(40*HWB);
        }];
    }
    return _instructionsLab;
}

@end



