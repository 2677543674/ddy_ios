//
//  OeHeadCltCell.h
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CmnInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@class OeFBtn;
@interface OeHeadCltCell : UICollectionViewCell

@property(nonatomic,strong) UIView * topBackView;
@property(nonatomic,strong) UILabel * moneyLab;       // 余额
@property(nonatomic,strong) UIButton * withdrawalBtn; // 提现按钮

@property(nonatomic,strong) UIView * functionView;
@property(nonatomic,strong) OeFBtn * teamBillBtn;    // 团队账单
@property(nonatomic,strong) OeFBtn * orderDetailBtn; // 订单明细

@end


@interface OeFBtn : UIButton
@property(nonatomic,strong) UIImageView * topImgV;
@property(nonatomic,strong) UILabel * titleLab;
-(instancetype)initWithTitle:(NSString*)title img:(NSString*)imgName;
@end


#pragma mark ===>>> 省见的运营商收益(按月统计)

@interface  OeMonthCltCell: UICollectionViewCell

@property(nonatomic,strong) UIView * backView;
@property(nonatomic,strong) UILabel * titleLab;   // 标题
@property(nonatomic,strong) UILabel * contentLab; // 数据
@property(nonatomic,strong) UILabel * statesLab;  // 状态(文字颜色后台返回)
@property(nonatomic,strong) CmnInfoModel * monthModel;

@end



#pragma mark ===>>> 今日昨日筛选按钮cell

@interface  OeDayCltViewCell: UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * cltDayView;
@property(nonatomic,strong) NSMutableArray * dayDataAry;   // 按天算的model

@end

@interface  OeDaySltCltCell: UICollectionViewCell
@property(nonatomic,strong) UIButton * selectBtn;    // 按钮
@property(nonatomic,strong) UIImageView * linesImgB; // 按钮左边显示分割线
@property(nonatomic,strong) CmnDayInfoModel * daySltModel;

@end

@interface OeDayInfoCltCell : UICollectionViewCell
@property(nonatomic,strong) UILabel * titleLab ;   // 标题
@property(nonatomic,strong) UILabel * contentLab ; // 数据
@property(nonatomic,strong) CmnInfoModel * dayModel;

@end

NS_ASSUME_NONNULL_END
