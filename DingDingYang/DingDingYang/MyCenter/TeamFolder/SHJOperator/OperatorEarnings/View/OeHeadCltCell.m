//
//  OeHeadCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/10/11.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "OeHeadCltCell.h"
#import "BillVC.h"

#pragma mark ===>>> 省见的运营商收益(按月统计)

@implementation OeHeadCltCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImageView * bgImgV = [UIImageView imgName:@"shj_ote_top_bg"];
        [self.contentView addSubview:bgImgV];
        [bgImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.bottom.offset(-60*HWB);
        }];
        
        
        // 余额和提现
        
        _topBackView = [[UIView alloc]init];
        [self.contentView addSubview:_topBackView];
        [_topBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.bottom.equalTo(self.contentView.mas_centerY);
        }];
        
        UILabel * syyeLab = [UILabel labText:@"收益余额" color:COLORWHITE font:22*HWB];
        syyeLab.font = [UIFont boldSystemFontOfSize:22*HWB];
        [_topBackView addSubview:syyeLab];
        [syyeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(13*HWB);
            make.centerY.equalTo(_topBackView.mas_centerY);
        }];

        UILabel * yLab = [UILabel labText:@"￥" color:COLORWHITE font:15*HWB];
        [_topBackView addSubview:yLab];
        [yLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(syyeLab.mas_right);
            make.bottom.equalTo(syyeLab.mas_bottom).offset(-2*HWB);
        }];
        
        _moneyLab = [UILabel labText:@"0" color:COLORWHITE font:22*HWB];
        _moneyLab.font = [UIFont boldSystemFontOfSize:22*HWB];
        [_topBackView addSubview:_moneyLab];
        [_moneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(yLab.mas_right);
            make.centerY.equalTo(_topBackView.mas_centerY);
        }];
        
        _withdrawalBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_withdrawalBtn setTitle:@"提现" forState:(UIControlStateNormal)];
        [_withdrawalBtn boardWidth:2 boardColor:COLORWHITE corner:0];
        _withdrawalBtn.titleLabel.font = [UIFont boldSystemFontOfSize:15*HWB];
        [_topBackView addSubview:_withdrawalBtn];
        [_withdrawalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_topBackView.mas_centerY);
            make.right.offset(-13*HWB);
            make.height.offset(24*HWB);
            make.width.offset(50*HWB);
        }];
        
        
        // 团队账单和订单明细按钮
        _functionView = [[UIView alloc]init];
        _functionView.backgroundColor = COLORWHITE;
        [_functionView cornerRadius:5*HWB];
        [self.contentView addSubview:_functionView];
        [_functionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.right.offset(-12*HWB);
            make.bottom.offset(-5*HWB);
            make.top.equalTo(self.contentView.mas_centerY);
        }];
        
        _teamBillBtn = [[OeFBtn alloc]initWithTitle:@"团队账单" img:@"shj_ote_bill_d"];
        [_functionView addSubview:_teamBillBtn];
        [_teamBillBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.offset(0);
            make.right.equalTo(_functionView.mas_centerX);
        }];
        
        _orderDetailBtn = [[OeFBtn alloc]initWithTitle:@"订单明细" img:@"shj_ote_order_d"];
        [_functionView addSubview:_orderDetailBtn];
        [_orderDetailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.offset(0);
            make.left.equalTo(_functionView.mas_centerX);
        }];
        
        
        ADDTARGETBUTTON(_withdrawalBtn, clickWayWithdrawal)
        ADDTARGETBUTTON(_teamBillBtn, clickWayTeamBill)
        ADDTARGETBUTTON(_orderDetailBtn, clickWayOrderDetail)
        
    }
    return self;
}

// 提现
-(void)clickWayWithdrawal{
    [MyPageJump withdrawalType:2 platform:55 totalAmount:_moneyLab.text results:^(NSInteger result) {
        
    }];
}

-(void)clickWayTeamBill{
    if (TopNavc) {
        BillVC * bill = [BillVC new];
        bill.billType = 55;
        [TopNavc pushViewController:bill animated:YES];
    }
}

-(void)clickWayOrderDetail{
    if (TopNavc) {
        NewConsumer * newCon = [[NewConsumer alloc]init];
        newCon.commissionType = 55 ;
        [TopNavc pushViewController:newCon animated:YES];
    }
}

@end


@implementation OeFBtn
-(instancetype)initWithTitle:(NSString*)title img:(NSString*)imgName{
    self = [super init];
    if (self) {
        _topImgV = [[UIImageView alloc]init];
        _topImgV.image = [UIImage imageNamed:imgName];
        _topImgV.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_topImgV];
        [_topImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.width.height.offset(20*HWB);
            make.top.offset(15*HWB);
        }];
        
        _titleLab = [UILabel labText:title color:Black51 font:13*HWB];
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
           make.bottom.offset(-15*HWB);
        }];
    }
    return self;
}
@end




#pragma mark ===>>> 省见的运营商收益(按月统计)

@implementation OeMonthCltCell
// 设置数据
-(void)setMonthModel:(CmnInfoModel *)monthModel{
    
    _monthModel = monthModel ;
    _titleLab.text = _monthModel.title ;
    
    if (_monthModel.ifAboutMoney==1) {
        _contentLab.text = FORMATSTR(@"￥%@",_monthModel.value);
    }else{
        _contentLab.text = _monthModel.value ;
    }
    
    _statesLab.text = _monthModel.state ;
    
    if (_monthModel.color.length>0&&[_monthModel.color hasPrefix:@"#"]) {
        _statesLab.textColor = [UIColor getColor:_monthModel.color];
    }else{
        _statesLab.textColor = LOGOCOLOR;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    
        [self backView];
        [self titleLab];
        [self contentLab];
        [self statesLab];
        
    }
    return self;
}

-(UIView*)backView{
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = COLORWHITE;
        [_backView cornerRadius:5*HWB];
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.right.offset(0);
        }];
        
    }
    return _backView;
}

-(UILabel*)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel labText:@"标题标题" color:Black102 font:12*HWB];
        [_backView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);  make.right.offset(-10*HWB);
            make.bottom.equalTo(_backView.mas_centerY).offset(-15*HWB);
        }];
    }
    return _titleLab ;
}

-(UILabel*)contentLab{
    if (!_contentLab) {
        _contentLab = [UILabel labText:@"￥0.0" color:Black51 font:16*HWB];
        [_backView addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB); make.right.offset(-10*HWB);
            make.centerY.equalTo(_backView.mas_centerY);
            make.height.offset(26*HWB) ;
        }];
    }
    return _contentLab ;
}

-(UILabel*)statesLab{
    if (!_statesLab) {
        _statesLab = [UILabel labText:@"结算状态" color:LOGOCOLOR font:12*HWB];
        [_backView addSubview:_statesLab];
        [_statesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.top.equalTo(_backView.mas_centerY).offset(15*HWB);
        }];
    }
    return _statesLab ;
}


@end




#pragma mark ===>>> 今日昨日筛选按钮cell

@implementation OeDayCltViewCell

-(void)setDayDataAry:(NSMutableArray *)dayDataAry{
    _dayDataAry = dayDataAry;
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) { [self cltDayView]; }
    return self;
}
    
-(UICollectionView*)cltDayView{
    if (!_cltDayView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0 ; layout.minimumInteritemSpacing = 0 ;
        _cltDayView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _cltDayView.delegate = self ;  _cltDayView.dataSource = self ;
        _cltDayView.backgroundColor = RGBA(254, 147, 75, 1) ;
        [_cltDayView cornerRadius:5*HWB];
        [self.contentView addSubview:_cltDayView];
        [_cltDayView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
        
        [_cltDayView registerClass:[OeDaySltCltCell class] forCellWithReuseIdentifier:@"clt_cell3"];
        [_cltDayView registerClass:[OeDayInfoCltCell class] forCellWithReuseIdentifier:@"clt_cell4"];
        
    }else{ [_cltDayView reloadData]; }
    return _cltDayView;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _dayDataAry.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) { return _dayDataAry.count; }
    CmnDayInfoModel * dayModel = _dayDataAry[section];
    return dayModel.dataAry.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float wi = FloatKeepInt(ScreenW-24*HWB);
    if (indexPath.section==0) { return CGSizeMake(wi/2, 46*HWB); }
    CmnDayInfoModel * dayModel = _dayDataAry[indexPath.section];
    return CGSizeMake(wi/dayModel.dataAry.count, 66*HWB);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section==0) {
        OeDaySltCltCell * threeCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"clt_cell3" forIndexPath:indexPath];
        threeCell.daySltModel = _dayDataAry[indexPath.item];
        threeCell.selectBtn.tag = indexPath.item ;
        [threeCell.selectBtn addTarget:self action:@selector(clickChangeSelect:) forControlEvents:(UIControlEventTouchUpInside)];
        return threeCell;
    }

    OeDayInfoCltCell * fourCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"clt_cell4" forIndexPath:indexPath];
    for (CmnDayInfoModel * typeModel in _dayDataAry) {
        if (typeModel.isslt == 1) {
            fourCell.dayModel = typeModel.dataAry[indexPath.item] ;
        }
    }
    return fourCell;
}

-(void)clickChangeSelect:(UIButton*)btn{
    for ( CmnDayInfoModel * typeModelAll in _dayDataAry) {
        typeModelAll.isslt = 0 ;
    }
    CmnDayInfoModel * typeModel1 = _dayDataAry[btn.tag];
    typeModel1.isslt = 1 ;
    [_cltDayView reloadData];
}


@end




@implementation OeDaySltCltCell

-(void)setDaySltModel:(CmnDayInfoModel *)daySltModel{
    _daySltModel = daySltModel ;
    [_selectBtn setTitle:_daySltModel.stitle forState:(UIControlStateNormal)];
    if (_daySltModel.isslt==1) {
        [_selectBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        _selectBtn.titleLabel.font = [UIFont systemFontOfSize:14*HWB] ;
        _linesImgB.hidden = NO;
    }else{
        [_selectBtn setTitleColor:Black102 forState:(UIControlStateNormal)];
        _selectBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB] ;
        _linesImgB.hidden = YES;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _selectBtn = [UIButton titLe:@"今日" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:13*HWB];
        [self.contentView addSubview:_selectBtn];
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];

        _linesImgB = [[UIImageView alloc]init];
        _linesImgB.backgroundColor = LOGOCOLOR;
        [self.contentView addSubview:_linesImgB];
        [_linesImgB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_selectBtn.mas_centerY).offset(15*HWB);
            make.centerX.equalTo(_selectBtn.mas_centerX);
            make.height.offset(1.5*HWB); make.width.offset(35*HWB);
        }];
        
    }
    return self;
}

@end



// 显示今日昨日的结果
@implementation OeDayInfoCltCell

-(void)setDayModel:(CmnInfoModel *)dayModel{
    _dayModel = dayModel ;
    _titleLab.text = _dayModel.title ;
    _contentLab.text = _dayModel.value ;
    if (_dayModel.ifAboutMoney==1) {
        _contentLab.text = FORMATSTR(@"￥%@",_dayModel.value);
    }else{ _contentLab.text = _dayModel.value; }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self titleLab];
        [self contentLab];
    }
    return self ;
}

-(UILabel*)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel labText:@"标题" color:COLORWHITE font:12*HWB];
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(-3*HWB);
        }];
    }
    return _titleLab ;
}

-(UILabel*)contentLab{
    if (!_contentLab) {
        _contentLab = [UILabel labText:@"￥0.0" color:COLORWHITE font:15*HWB];
        [self.contentView addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_centerY).offset(2*HWB);
            make.centerX.equalTo(self.contentView.mas_centerX);
        }];
    }
    return _contentLab ;
}
@end


