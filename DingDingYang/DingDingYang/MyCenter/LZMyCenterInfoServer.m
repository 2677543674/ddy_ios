//
//  LZMyCenterInfoServer.m
//  DingDingYang
//
//  Created by zhaoyang on 2019/9/17.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "LZMyCenterInfoServer.h"

@implementation LZMyCenterInfoServer

+ (LZMyCenterInfoServer *)shareInstance{
    static LZMyCenterInfoServer *server;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        server = [[LZMyCenterInfoServer alloc] init];
    });
    
    return server;
}

- (instancetype)init{
    if ([super init]) {
        self.commissionModel = [[CommissionModel alloc] init];
    }
    
    return self;
}

- (void)requestCommission{
    [NetRequest requestType:0 url:url_new_consumer_detail ptdic:@{} success:^(id nwdic) {
        NSDictionary * dic = NULLDIC(nwdic[@"content"]);
        [self.commissionModel addModelByPlatform:@"消费佣金" data:dic];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        
    }];
}

@end
