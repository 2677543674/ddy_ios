//
//  PersonalModel.h
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonalModel : NSObject
@property(nonatomic,strong) id       headImg;         // 头像
@property(nonatomic,strong) id       wxEwm;           // 微信二维码
@property(nonatomic,copy) NSString * activateTime;    // 激活时间
@property(nonatomic,copy) NSString * aliNo;           // 支付宝账户
@property(nonatomic,copy) NSString * createTime;      // 创建时间
@property(nonatomic,copy) NSString * cid;             // 用户id
@property(nonatomic,copy) NSString * lkid;            // 会员系统自带的唯一ID（分享时带上此ID）
@property(nonatomic,copy) NSString * mobile;          // 手机号
@property(nonatomic,copy) NSString * name;            // 姓名
@property(nonatomic,copy) NSString * nickName;        // 昵称
@property(nonatomic,copy) NSString * partnerId;       // 代理商ID
@property(nonatomic,copy) NSString * qq;              // qq账户
@property(nonatomic,copy) NSString * role;            // 用户级别(0:消费者 、1:代理商 2:联合运营商 4:试用代理)
@property(nonatomic,copy) NSString * status;
@property(nonatomic,copy) NSString * taobaoAccount;   // (淘宝，微信、昵称)
@property(nonatomic,copy) NSString * teamCommission;  // 奖励提成收益
@property(nonatomic,copy) NSString * uniteOperationId;
@property(nonatomic,copy) NSString * wallet;          // 代理佣金(个人信息接口获取的)
@property(nonatomic,copy) NSString * walletConsume ;  // 消费佣金(个人信息接口获取的)
@property(nonatomic,copy) NSString * wxNo;            // 微信账号
@property(nonatomic,copy) NSString * ifBindWx;        // 是否绑定微信 1是   0否
@property(nonatomic,copy) NSString * wxName ;         // 昵称(字段过多，个人中心的昵称以此为准)
@property(nonatomic,copy) NSString * roleName ;       // 角色名称
@property(nonatomic,copy) NSString * timeinfo ;       // 代理试用剩余时间、为空时不显示
@property(nonatomic,copy) NSString * rongcloudToken ; // 融云token
@property(nonatomic,copy) NSString * inviteCode ;     // 邀请码

@property(nonatomic,copy) NSString * shareUrl ;       // 分享好友注册的链接 (快站,复制邀请码的方式)
@property(nonatomic,copy) NSString * shareGoodsDamin; // 分享好友注册的链接 (输入手机号领取资格的方式)

@property(nonatomic,strong) NSArray * roleListAry ;   // 放各种角色名称(只有个人中心的参数有值)

//单独接口查询
@property(nonatomic,copy) NSString * nomsgCount;      // 未读消息数量

//单独接口查询 (新版在个人中心不显示，不再查询)
@property(nonatomic,copy) NSString * proxyAmount;     // 代理佣金
@property(nonatomic,copy) NSString * consumeAmount;   // 消费佣金
@property(nonatomic,copy) NSString * myTeam;          // 我的团队人数


-(instancetype)initWithDic:(NSDictionary*)dic;


+(void)getPersonalModel:(void(^)(PersonalModel * pModel))complete failure:(void(^)(NSString * fail))failBlock;

@end



#pragma mark ==>> 个人中心中的其他数据

@interface PersonalOtherModel : NSObject

@property(nonatomic,copy) NSString * name; // 角色名称
@property(nonatomic,copy) NSString * role; // 角色值
@property(nonatomic,copy) NSString * sid;  // 角色ID
@property(nonatomic,copy) NSString * partnerId; // 暂时无用

@end


