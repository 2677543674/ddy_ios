//
//  PersonalModel.m
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "PersonalModel.h"

@implementation PersonalModel


+(void)getPersonalModel:(void(^)(PersonalModel * pModel))complete  failure:(void(^)(NSString * fail))failBlock {
    
    [NetRequest requestType:0 url:url_user_info ptdic:nil success:^(id nwdic) {
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        [dic addEntriesFromDictionary:NULLDIC(nwdic[@"user"])];
        dic[@"roleName"] = REPLACENULL(nwdic[@"roleName"]);
        dic[@"timeinfo"] = REPLACENULL(nwdic[@"timeinfo"]);
        dic[@"roleList"] = NULLARY(nwdic[@"roleList"]);
        NSUDSaveData(dic,UserInfo)
        
        PersonalModel * infoModel = [[PersonalModel alloc]initWithDic:dic];
        complete(infoModel);

        if (infoModel.roleListAry.count>0) {
            NSMutableDictionary * roleDic = [NSMutableDictionary dictionary];
            for (NSDictionary * dic in infoModel.roleListAry) {
                roleDic[REPLACENULL(dic[@"role"])] = REPLACENULL(dic[@"name"]);
            }
            NSUDSaveData(roleDic, cache_role_status)
        }
       
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        failBlock(failure);
    }];

}


-(instancetype)initWithDic:(NSDictionary *)dic
{
    self=[super init];
    if (self) {
        
        self.headImg          =  [REPLACENULL(dic[@"headImg"]) hasPrefix:@"http"] ? REPLACENULL(dic[@"headImg"]) : FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"headImg"])) ;
        self.activateTime     = REPLACENULL(dic[@"activateTime"]);
        self.aliNo            = REPLACENULL(dic[@"aliNo"]);
        self.createTime       = REPLACENULL(dic[@"createTime"]);
        self.cid              = REPLACENULL(dic[@"id"]);
        self.lkid             = REPLACENULL(dic[@"lkid"]);
        self.mobile           = REPLACENULL(dic[@"mobile"]);
        self.name             = REPLACENULL(dic[@"name"]);
        self.nickName         = [REPLACENULL(dic[@"nickName"]) stringByRemovingPercentEncoding];
        self.partnerId        = REPLACENULL(dic[@"partnerId"]);
        self.qq               = REPLACENULL(dic[@"qq"]);
        self.role             = REPLACENULL(dic[@"role"]);
        self.status           = REPLACENULL(dic[@"status"]);
        self.taobaoAccount    = [REPLACENULL(dic[@"taobaoAccount"]) stringByRemovingPercentEncoding];
        self.teamCommission   = REPLACENULL(dic[@"teamCommission"]);
        self.uniteOperationId = REPLACENULL(dic[@"uniteOperationId"]);
        self.wallet           = ROUNDED(REPLACENULL(dic[@"wallet"]), 2) ;
        self.walletConsume    = ROUNDED(REPLACENULL(dic[@"walletConsume"]), 2) ;
        self.wxEwm            = REPLACENULL(dic[@"wxEwm"]);
        self.wxNo             = REPLACENULL(dic[@"wxNo"]);
        self.ifBindWx         = REPLACENULL(dic[@"ifBindWx"]);
        self.shareUrl         = REPLACENULL(dic[@"shareUrl"]);
        self.shareGoodsDamin  = REPLACENULL(dic[@"shareGoodsDamin"]) ;
        
        self.nomsgCount       =  REPLACENULL( NSUDTakeData(UnreadMessages) ) ;
        
        self.proxyAmount      = ROUNDED(REPLACENULL(dic[@"proxyAmount"]), 2) ;
        self.consumeAmount    = ROUNDED(REPLACENULL(dic[@"consumeAmount"]), 2) ;
        self.myTeam           = REPLACENULL(dic[@"myTeam"]) ;
        
        self.roleName         = REPLACENULL(dic[@"roleName"])  ;
        self.timeinfo         = REPLACENULL(dic[@"timeinfo"]) ;
        self.rongcloudToken   = REPLACENULL(dic[@"rcImToken"]) ;
        self.inviteCode       = REPLACENULL(dic[@"inviteCode"]) ;

        self.roleListAry      = NULLARY(dic[@"roleList"]) ;
        self.wxName           = [REPLACENULL(dic[@"wxName"]) stringByRemovingPercentEncoding];
        
//        self.wxName           = REPLACENULL(dic[@"wxName"]);
//        self.nickName         = REPLACENULL(dic[@"nickName"]);
//        self.taobaoAccount    = REPLACENULL(dic[@"taobaoAccount"]);
        
        if (PID==10104) { // 蜜赚只用微信授权
            NSDictionary * dic = (NSDictionary*)NSUDTakeData(Wechat_Info);
            self.taobaoAccount = REPLACENULL(dic[@"nickname"]);
        }
    }
    return self;
}

-(instancetype)init{
    self=[super init];
    if (self) {
        self.activateTime = @"";
        self.aliNo = @"";
        self.createTime = @"";
        self.headImg = @"";
        self.cid = @"";
        self.lkid = @"";
        self.mobile = @"";
        self.name = @"";
        self.nickName = @"";
        self.partnerId = @"";
        self.qq = @"";
        self.role = @"";
        self.status = @"";
        self.taobaoAccount = @"";
        self.teamCommission = @"";
        self.uniteOperationId = @"";
        self.wallet = @"";
        self.wxEwm = @"";
        self.wxNo = @"";
        self.ifBindWx=@"";
        self.shareUrl = @"";
        
        self.nomsgCount = @"";
        
        self.proxyAmount = @"" ;
        self.consumeAmount = @"" ;
        self.myTeam = @"" ;
        
        self.roleName = @"" ;
        self.timeinfo = @"" ;
        self.rongcloudToken = @"" ;
        self.inviteCode = @"" ;
        
        self.roleListAry = @[] ;
    }
    return self;
}


@end





@implementation PersonalOtherModel

@end



