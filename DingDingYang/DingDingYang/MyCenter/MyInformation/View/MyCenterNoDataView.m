//
//  MyCenterNoDataView.m
//  DingDingYang
//
//  Created by ddy on 2018/1/26.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MyCenterNoDataView.h"

@implementation MyCenterNoDataView


-(void)setImgNameStr:(NSString *)imgNameStr{
    _imgNameStr = imgNameStr ;
    _tishiImgV.image = [UIImage imageNamed:_imgNameStr];
}

-(void)setTishiStr:(NSString *)tishiStr{
    _tishiStr = tishiStr ;
    _tishiLab.text = _tishiStr ;
}

-(void)setBtnStr:(NSString *)btnStr{
    _btnStr = btnStr ;
    [_tishiBtn setTitle:_btnStr forState:(UIControlStateNormal)];
}


-(instancetype)init{
    self = [super init];
    if (self) {
        
        _tishiImgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"哭"]];
        _tishiImgV.contentMode = UIViewContentModeScaleAspectFit ;
        [self addSubview:_tishiImgV];
        [_tishiImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.equalTo(self.mas_centerY);
            make.top.offset(0); make.width.offset(120*HWB);
        }];
        
        _tishiLab = [UILabel labText:@"你还没有数据哦" color:Black102 font:13*HWB];
        [self addSubview:_tishiLab];
        [_tishiLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tishiImgV.mas_bottom).offset(10*HWB);
            make.centerX.equalTo(self.mas_centerX);
        }];
        
        _tishiBtn= [UIButton titLe:@"立刻去推荐好友吧" bgColor:COLORCLEAR titColorN:Black102 titColorH:Black102 font:13*HWB];
        [_tishiBtn boardWidth:1 boardColor:Black102 corner:16*HWB];
        [self addSubview:_tishiBtn];
        [_tishiBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tishiLab.mas_bottom).offset(8*HWB);
            make.left.offset(10);  make.right.offset(-10);
            make.height.offset(32*HWB);
            
        }];
    }
    return self ;
}


@end
