//
//  MyCenterNoDataView.h
//  DingDingYang
//
//  Created by ddy on 2018/1/26.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCenterNoDataView : UIView

@property(nonatomic,strong) UIImageView * tishiImgV ;
@property(nonatomic,strong) UILabel     * tishiLab ;
@property(nonatomic,strong) UIButton    * tishiBtn ;

@property(nonatomic,copy) NSString * imgNameStr ;
@property(nonatomic,copy) NSString * tishiStr ;
@property(nonatomic,copy) NSString * btnStr ;


@end
