//
//  PersonalInfoCell.h
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.



#import <UIKit/UIKit.h>

@interface PersonalInfoCell : UITableViewCell

@property(nonatomic,strong) UILabel * promptLabel;
@property(nonatomic,strong) UILabel * textLab;
@property(nonatomic,strong) UIImageView * imgView;

@property(nonatomic,strong) UIView   * backView;
@property(nonatomic,strong) UISwitch * switchView;
@property(nonatomic,strong) UILabel  * swicthLabel;
@property(nonatomic,strong) UIButton * swBtn;

@property(nonatomic,strong) PersonalModel * pModel ;

@property(nonatomic,assign) NSInteger hidenWho ;

@end





