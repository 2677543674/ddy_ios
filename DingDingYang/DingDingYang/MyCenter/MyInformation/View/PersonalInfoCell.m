//
//  PersonalInfoCell.m
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "PersonalInfoCell.h"

@implementation PersonalInfoCell


-(void)setHidenWho:(NSInteger)hidenWho{
    
    _hidenWho = hidenWho ;
    
    switch (_hidenWho) {
        case 1:{ // 显示图片，其它隐藏
            
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.imgView.hidden = NO;
            self.textLab.hidden = YES;
            self.backView.hidden = YES;
            
        } break;
            
        case 2:{ // 显示文字，其他隐藏
            
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.textLab.hidden = NO;
            self.imgView.hidden = YES;
            self.backView.hidden = YES;
            
        } break;
            
        case 3:{ // 显示重要提醒，其他隐藏
            
            self.accessoryType = UITableViewCellAccessoryNone;
            self.backView.hidden = NO;
            self.textLab.hidden = YES;
            self.imgView.hidden = YES;
        
        } break;
            
        default:
            break;
    }
    
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        
        _promptLabel=[[UILabel alloc]init];
        _promptLabel.textColor=Black51;
        _promptLabel.font= [UIFont systemFontOfSize:13.0*HWB] ;
        [self.contentView addSubview:_promptLabel];
        [_promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10); make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.offset(20);
        }];
        
        _imgView=[[UIImageView alloc]init];
        _imgView.layer.masksToBounds = YES;
        _imgView.layer.cornerRadius = 5;
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(5);
            make.top.offset(10); make.bottom.offset(-10);
            make.width.equalTo(_imgView.mas_height);
        }];
        
        _textLab=[UILabel labText:@"未设置" color:Black102 font:13.0*HWB];
        _textLab.textAlignment = NSTextAlignmentRight ;
        [self.contentView addSubview:_textLab];
        [_textLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(5);  make.centerY.equalTo(self.mas_centerY);
            make.height.offset(20); make.width.offset(200*HWB);
        }];
        
        //重要消息提醒
        _backView = [UIView new];
        _backView.backgroundColor=[UIColor clearColor];
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(5);
            make.top.offset(0);
            make.bottom.offset(0);
            make.width.offset(200*HWB);
        }];
        
        _switchView = [[UISwitch alloc]init];
        _switchView.userInteractionEnabled = NO ;
//        [_switchView setOnTintColor:LOGOCOLOR];
        [_backView addSubview:_switchView];
        [_switchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-20*HWB);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        _swicthLabel=[UILabel labText:@"已开启" color:Black102 font:10.0*HWB];
        [_backView addSubview:_swicthLabel];
        [_swicthLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_switchView.mas_left).offset(-5);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        
    }
    return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

