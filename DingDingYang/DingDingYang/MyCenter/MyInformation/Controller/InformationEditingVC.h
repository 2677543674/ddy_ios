//
//  InformationEditingVC.h
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@interface InformationEditingVC : DDYViewController
@property(nonatomic,assign) NSInteger index;
@property(nonatomic,strong) PersonalModel * infoModel;
@end
