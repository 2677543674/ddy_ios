//
//  MyInfoSetVc.h
//  DingDingYang
//
//  Created by ddy on 2017/3/7.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class PersonalModel;

@interface MyInfoSetVc : DDYViewController

@property(nonatomic,strong)PersonalModel * model ;

@end
