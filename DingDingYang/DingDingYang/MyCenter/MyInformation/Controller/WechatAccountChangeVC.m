//
//  WechatAccountChangeVC.m
//  DingDingYang
//
//  Created by ddy on 2018/3/5.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "WechatAccountChangeVC.h"

@interface WechatAccountChangeVC ()
@property(nonatomic,strong)UIButton* commitBtn;
@property(nonatomic,strong)UIButton* sendBtn;
@property(nonatomic,strong)UITextField* textField;
@end

@implementation WechatAccountChangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=COLORGROUP;
    self.title = @"更改微信授权" ;
    
    UIImageView * wechatImgv = [[UIImageView alloc]init];
    wechatImgv.image = [UIImage imageNamed:@"wechat_change"];
    wechatImgv.contentMode=UIViewContentModeScaleAspectFit;
    [self.view addSubview:wechatImgv];
    [wechatImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(60*HWB);
        make.top.offset(30*HWB);  make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    UIView* lineView=[UIView new];
    lineView.backgroundColor=[UIColor colorWithRed:200.f/255.f green:200.f/255.f blue:200.f/255.f alpha:1];
    [self.view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40*HWB);
        make.right.offset(-40*HWB);
        make.top.equalTo(wechatImgv.mas_bottom).offset(56*HWB);
        make.centerX.equalTo(self.view.mas_centerX);
        make.height.offset(1);
    }];
    
    
    UIImageView * yanzhengma = [[UIImageView alloc]init];
    yanzhengma.image = [UIImage imageNamed:@"yanzhengma"];
    yanzhengma.contentMode=UIViewContentModeScaleAspectFit;
    [self.view addSubview:yanzhengma];
    [yanzhengma mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(lineView.mas_bottom).offset(-10);
        make.left.equalTo(lineView.mas_left);
    }];
    
    
    _sendBtn=[UIButton titLe:@"获取验证码" bgColor:[UIColor clearColor] titColorN:SHAREBTNCOLOR titColorH:LOGOCOLOR font:13*HWB];
    _sendBtn.layer.borderColor=SHAREBTNCOLOR.CGColor;
    _sendBtn.layer.borderWidth=1;
    _sendBtn.layer.cornerRadius=4;
    _sendBtn.clipsToBounds=YES;
    ADDTARGETBUTTON(_sendBtn, sendCode);
    [self.view addSubview:_sendBtn];
    [_sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lineView.mas_right);
        make.width.offset(80*HWB);
        make.height.offset(28*HWB);
        make.bottom.equalTo(lineView.mas_bottom).offset(-6);
    }];
    
    _textField=[[UITextField alloc]init];
    _textField.font=[UIFont systemFontOfSize:13*HWB];
    _textField.placeholder=@"请输入验证码";
    _textField.borderStyle=UITextBorderStyleNone;
    [self.view addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(yanzhengma.mas_centerY);
        make.left.equalTo(yanzhengma.mas_right).offset(10);
        make.width.offset(100*HWB);
        make.bottom.equalTo(lineView).offset(-8);
    }];
    
    _commitBtn=[UIButton titLe:@"确定" bgColor:SHAREBTNCOLOR titColorN:[UIColor whiteColor] titColorH:[UIColor whiteColor] font:15*HWB];
    _commitBtn.layer.cornerRadius=18*HWB;
    _commitBtn.clipsToBounds=YES;
    ADDTARGETBUTTON(_commitBtn, clickCommit);
    [self.view addSubview:_commitBtn];
    [_commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(24*HWB);
        make.left.offset(40*HWB);
        make.right.offset(-40*HWB);
        make.height.offset(36*HWB);
    }];
}


-(void)sendCode{

    [NetRequest requestTokenURL:url_send_code_bdAlipay parameter:@{@"mobile":PhoneNumber} success:^(NSDictionary *nwdic) {
        SHOWMSG(@"发送成功")
        [_sendBtn startCountDown];
        
    } failure:^(NSString *failure) {
        SHOWMSG(@"发送失败")
    }];
}


-(void)clickCommit{
    if (_textField.text.length==0) {
        SHOWMSG(@"请输入验证码")
        return;
    }
    [NetRequest requestTokenURL:url_check_code parameter:@{@"mobile":PhoneNumber,@"randCode":_textField.text} success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            [ThirdPartyLogin changeWechat:^(id result) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            } fial:^(NSString *failStr) {
                SHOWMSG(failStr)
            }];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}




@end
