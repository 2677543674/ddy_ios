//
//  InformationEditingVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "InformationEditingVC.h"

@interface InformationEditingVC ()

@property(nonatomic,strong)UIView*tabbarView;
@property(nonatomic,strong)UITextField*taobaoText;//淘宝账户输入框
@property(nonatomic,strong)UIView*backView;//背景框
@property(nonatomic,strong)UITextField*tfield;//输入框
@property(nonatomic,strong)UIButton*codeBtn;//获取验证码
@property(nonatomic,strong)UIButton*upBtn;//提交按钮
@end

@implementation InformationEditingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLORGROUP ;
    
    switch (_index) {
            //        case  0:  {
            //            [self addTabbarView:@"头像设置"];
            ////            [self addHeadLogo];
            //        } break;
        case  1:  {
            self.title=@"昵称";
            [self upTaobaoAccount];
        } break;
        case  2:  {
            self.title=@"支付宝账号";
            [self addApliayAccount];
        }  break;
            //        case  3:  {
            //            [self addTabbarView:@"微信二维码"];
            ////            [self upWechatQrCode];
            //        }  break;
        default:       break;
    }
}



#pragma mark --> 设置昵称
-(void)upTaobaoAccount{
    UIButton*baoCunBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [baoCunBtn setTitle:@"保存" forState:UIControlStateNormal];
    [baoCunBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    [baoCunBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    baoCunBtn.titleLabel.font=Font15;
    baoCunBtn.frame=CGRectMake(0, 0, 50, 44);
    [baoCunBtn addTarget:self action:@selector(upAccount) forControlEvents:UIControlEventTouchUpInside];
    //    [_tabbarView addSubview:baoCunBtn];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:baoCunBtn];
    
    _taobaoText=[UITextField textColor:Black102 PlaceHorder:@"请输入昵称(不能超过15个字符)" font:14.0];
    _taobaoText.backgroundColor=COLORWHITE;
    _taobaoText.text=_infoModel.taobaoAccount;
    _taobaoText.clearButtonMode=UITextFieldViewModeWhileEditing;
    [_taobaoText addTarget:self action:@selector(keyBoardDown) forControlEvents:UIControlEventEditingDidEndOnExit];
    _taobaoText.frame=CGRectMake(10, 0+20, ScreenW-20, 40);
    [self.view addSubview:_taobaoText];
    
}
//上传淘宝账户
-(void)upAccount{
    
    
//        stringByAddingPercentEncodingWithAllowedCharacters
//        NSString * unencodedString = _taobaoText.text ;
//        NSString * encodedString = [unencodedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
//        NSString * encodedString = (NSString *)
//        CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
//                                                                  (CFStringRef)unencodedString,
//                                                                  NULL,
//                                                                  (CFStringRef)@"!*'();:@&=+$,/?%#[]",
//                                                                  kCFStringEncodingUTF8));
//        LOG(@"1：%@\n\n2：%@\n\n解码：%@",encodedString,[str stringByRemovingPercentEncoding]);
//        LOG(@"%@",[_taobaoText.text stringByRemovingPercentEncoding])
    

    ResignFirstResponder
    if (_taobaoText.text.length==0) {  SHOWMSG(@"请输入您的昵称")  return; }
    if (_taobaoText.text.length>15) {  SHOWMSG(@"昵称不能大于15个字符!") return; }
    
        NSString * unencodedString = [_taobaoText.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]] ;
//    NSString * unencodedString = _taobaoText.text ;
    NSLog(@"%@",unencodedString);

    MBShow(@"正在提交...")
    [NetRequest requestType:0 url:url_update_nickName ptdic:@{@"tbAccount":unencodedString} success:^(id nwdic) {
        SHOWMSG(@"提交成功")
        [_infoModel setValue:_taobaoText.text forKey:@"taobaoAccount"];
        [self performSelector:@selector(clickBack) withObject:self afterDelay:0.6];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];

}

#pragma mark --> 添加支付宝账户
-(void)addApliayAccount{
    
    NSArray*aryName = @[@"支付宝账号",@"支付宝实名",@"手机号码",@"验证码"];
    
    NSArray*pAryName = @[@"请输入支付宝账号",@"实名不一致会导致提现失败哦!",PhoneNumber,@"请输入验证码"];
    _backView=[[UIView alloc]init];
    _backView.backgroundColor = COLORWHITE;
    _backView.layer.masksToBounds = YES;
    _backView.layer.cornerRadius = 6;
    _backView.frame = CGRectMake(15,15, ScreenW-30, 210);
    [self.view addSubview:_backView];
    
    _codeBtn=[UIButton titLe:@"获取验证码" bgColor:[UIColor clearColor] titColorN:LOGOCOLOR titColorH:COLORGRAY font:FONT_15];
    //    _codeBtn.layer.cornerRadius=6;
    [_codeBtn addTarget:self action:@selector(sendValidation) forControlEvents:UIControlEventTouchUpInside];
    _codeBtn.frame = CGRectMake(ScreenW-120, 157, 90, 36);
    
    UIView* line = [UIView new];
    line.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [_codeBtn addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_codeBtn).offset(-5);
        make.width.offset(1);
        make.height.offset(28);
        make.centerY.equalTo(_codeBtn);
    }];
    
    
    CGSize size = StringSize(@"支付宝实名", 13*HWB);
    for (NSInteger i=0; i<4; i++) {
        
        UILabel*nameLab=[UILabel labText:aryName[i] color:Black51 font:13*HWB];
        nameLab.frame=CGRectMake(10, 10+50*i, size.width+5,30);
        [_backView addSubview:nameLab];
        
        _tfield = [UITextField textColor:Black51 PlaceHorder:pAryName[i] font:13*HWB];
        _tfield.tag = 10+i;
        [_tfield addTarget:self action:@selector(keyBoardDown) forControlEvents:UIControlEventTouchUpInside];
         _tfield.frame = CGRectMake(size.width+15, 10+50*i,ScreenW-(size.width+45),30);
        [_backView addSubview:_tfield];
        if (i==2) { // 手机号
            if (PhoneNumber.length==11) {
                _tfield.userInteractionEnabled=NO; _tfield.text=pAryName[i];
            }else{
                _tfield.placeholder = @"请输入手机号";
            }
        }
        if (i==3) {
            _tfield.frame=CGRectMake(size.width+15, 10+50*i,ScreenW-(size.width+140),30);
            _tfield.keyboardType=UIKeyboardTypePhonePad;
            [_backView addSubview:_codeBtn];
        }
    }
    
    
    _upBtn = [UIButton titLe:@"提交" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGRAY font:14*HWB];
    _upBtn.layer.cornerRadius = 19*HWB;  [_upBtn addShadowLayer];
    [_upBtn addTarget:self action:@selector(clickUp) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_upBtn];
    [_upBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40*HWB);  make.right.offset(-40*HWB);
        make.height.offset(38*HWB); make.top.equalTo(_backView.mas_bottom).offset(20*HWB);
    }];
    
}

//发送验证码
-(void)sendValidation{
    
    ResignFirstResponder
    UITextField*fieldPhone = (UITextField*)[_backView viewWithTag:12];
    if (fieldPhone.text.length==0) {  SHOWMSG(@"请输入手机号!") return ; }
    if (JUDGESTR(PHONE, fieldPhone.text)) {
        MBShow(@"正在发送...")
        [NetRequest requestTokenURL:url_send_code_bdAlipay parameter:@{@"mobile":PhoneNumber,@"t":@"2"} success:^(NSDictionary *nwdic) {
            MBHideHUD
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                [_codeBtn startCountDown];
            }else{
                SHOWMSG(nwdic[@"result"])
            }
        } failure:^(NSString *failure) {
            MBHideHUD
            SHOWMSG(failure)
        }];
    }else{
        SHOWMSG(@"请检查手机号是否正确!")
    }
   
}

//点击上传支付宝信息
-(void)clickUp{
    ResignFirstResponder

    UITextField*fieldAccount=(UITextField*)[_backView viewWithTag:10];
    UITextField*fieldName=(UITextField*)[_backView viewWithTag:11];
    UITextField*fieldCode=(UITextField*)[_backView viewWithTag:13];
    
    
    if (fieldAccount.text.length==0) { SHOWMSG(@"请输入支付宝账户!")  return ;  }
    if (fieldName.text.length==0) { SHOWMSG(@"请输入您的真实姓名!") return; }
    if (fieldCode.text.length==0) { SHOWMSG(@"请输入验证码!")  return;  }
    
    NSDictionary*dic=@{@"name":fieldName.text,
                       @"aliAccount":fieldAccount.text,
                       @"randCode":fieldCode.text};
    
    MBShow(@"正在提交...")
    [NetRequest requestType:0 url:url_update_apliay ptdic:dic success:^(id nwdic) {
        [PersonalModel getPersonalModel:^(PersonalModel *pModel) {
            MBHideHUD  SHOWMSG(@"绑定成功啦!");
            [_infoModel setValue:fieldAccount.text forKey:@"aliNo"];
            // 先改成nickName 因为model每次在个人中心都会重新请求 name被覆盖
            [_infoModel setValue:fieldName.text forKey:@"nickName"];
            [self performSelector:@selector(clickBack) withObject:self afterDelay:0.3];
        } failure:^(NSString *fail) { MBHideHUD  SHOWMSG(fail) }];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
    
}


-(void)clickBack{ MBHideHUD  PopTo(YES) }

-(void)keyBoardDown{}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end



