//
//  MyInfoSetVc.m
//  DingDingYang
//
//  Created by ddy on 2017/3/7.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MyInfoSetVc.h"
#import "PersonalInfoCell.h"
#import "InformationEditingVC.h"
#import "AlipayVC.h"
#import "WechatAccountChangeVC.h"


@interface MyInfoSetVc ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) UIImagePickerController * imgPickerVC;
@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,strong) NSArray * tsAry;
@property(nonatomic,assign) NSInteger indexUp;   // 记录要上传哪张图片(0:头像 3:微信二维码)
@property(nonatomic,strong) UIImage * selectImg; // 选中的图片


@end

@implementation MyInfoSetVc


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    self.title = @"设置";
    
//    _tsAry = @[@"头像",@"昵称",@"支付宝账号",@"微信绑定",@"淘宝授权",@"重要提醒"];
//
//    if (IsNeedTaoBao==2) {
//        _tsAry=@[@"头像",@"昵称",@"支付宝账号",@"微信绑定",@"重要提醒"];
//    }
    
    _tsAry=@[@"头像",@"昵称",@"支付宝账号",@"微信绑定",@"重要提醒"];
    UIUserNotificationSettings * setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
    if (setting.types == UIUserNotificationTypeNone) {
        _tsAry=@[@"头像",@"昵称",@"支付宝账号",@"微信绑定",@"重要提醒"];
    }else{
        _tsAry=@[@"头像",@"昵称",@"支付宝账号",@"微信绑定"];
    }
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 6, ScreenW, ScreenH-6) style:UITableViewStylePlain];
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 0;
    self.tableView.estimatedSectionHeaderHeight = 0;
    self.tableView.estimatedSectionFooterHeight = 0;
    _tableView.delegate=self; _tableView.dataSource=self;
    _tableView.backgroundColor=LinesColor;
    _tableView.scrollEnabled=NO;
    [self.view addSubview:_tableView];

    [_tableView registerClass:[PersonalInfoCell class] forCellReuseIdentifier:@"PersonalInfoCell"];
    
    _imgPickerVC=[[UIImagePickerController alloc]init];
    _imgPickerVC.delegate = self;
    _imgPickerVC.allowsEditing = YES;
    
    
    UIButton*backLogin=[UIButton titLe:@"退出登录" bgColor:LinesColor titColorN:Black102 titColorH:Black204 font:13*HWB];
    backLogin.layer.cornerRadius=6;
    [backLogin addShadowLayer];
    [backLogin addTarget:self action:@selector(backLoginAndRemove) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backLogin];
    [backLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(30*HWB);
        make.right.offset(-30*HWB);
        make.height.offset(38*HWB);
        make.bottom.equalTo(self.view).offset(-50*HWB);
    }];
}


#pragma mark ===>>> tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _tsAry.count ;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) { return 90*HWB ; }
    return 46 * HWB ;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (section==0||(IsNeedTaoBao==1&&section==4)||(IsNeedTaoBao==2&&section==3)) {
//        return 10;
//    }
    if (section==0) {
        return 10;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PersonalInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalInfoCell" forIndexPath:indexPath];
 
    cell.promptLabel.text = _tsAry[indexPath.section];
    
    switch (indexPath.section) {
        case 0:{ // 头像
            cell.hidenWho = 1 ;
            if ([_model.headImg isKindOfClass:[UIImage class]]) {
                cell.imgView.image=_model.headImg;
            }else{
                [cell.imgView sd_setImageWithURL:[NSURL URLWithString:_model.headImg]];
            }
        } break;
            
        case 1:{ // 昵称
            cell.hidenWho = 2 ;
            if (_model.taobaoAccount.length>0) {
                cell.textLab.text = _model.taobaoAccount;
            }
        } break;
          
        case 2:{ // 支付宝账号
            cell.hidenWho = 2 ;
            if (_model.aliNo.length>0)  {
                cell.textLab.text = @"已绑定";
            }else{
                cell.textLab.text = @"未绑定";
            }
        } break;
            
        case 3:{ // 微信授权
            cell.hidenWho = 2 ;
            if ([_model.ifBindWx isEqualToString:@"1"])  {
                
                cell.textLab.text = @"已绑定";
            }else{
                cell.textLab.text = @"未绑定";
            }
        } break;
            
        case 4:{ // 重要提醒
            
            cell.hidenWho = 3 ;
            UIUserNotificationSettings * setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
            if (setting.types == UIUserNotificationTypeNone) {
                cell.swicthLabel.text = @"已关闭";
                [cell.switchView setOn:NO animated:YES];
            }else{
                cell.swicthLabel.text = @"已开启";
                [cell.switchView setOn:YES animated:YES];
            }
        } break;
            
//        case 5:{ // 重要提醒 (检测本地通知权限是否开启)
//            cell.hidenWho = 3 ;
//            UIUserNotificationSettings * setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
//            if (setting.types == UIUserNotificationTypeNone) {
//                cell.swicthLabel.text = @"已关闭";
//                [cell.switchView setOn:NO animated:YES];
//            }else{
//                cell.swicthLabel.text = @"已开启";
//                [cell.switchView setOn:YES animated:YES];
//            }
//        } break;
            
        default: break;
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{ // 头像
           
            _indexUp = indexPath.section;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
                [self paizhaoORphoto:UIAlertControllerStyleAlert];
            }else{
                [self paizhaoORphoto:UIAlertControllerStyleActionSheet];
            }
            
        } break;
            
        case 1:{ // 昵称
            
            InformationEditingVC * editVC = [InformationEditingVC new];
            editVC.index = indexPath.section;
            editVC.infoModel = _model;
            PushTo(editVC, YES);
            
        } break;
            
        case 2:{ // 支付宝账号
            
            if (_model.aliNo.length>0) { // 已绑定(进入查看页面)
                AlipayVC * VC = [[AlipayVC alloc]init];
                VC.infoModel = _model;
                PushTo(VC, YES);
            }else{
                InformationEditingVC * editVC = [InformationEditingVC new];
                editVC.index = indexPath.section;
                editVC.infoModel = _model;
                PushTo(editVC, YES);
            }
            
        } break;
            
        case 3:{ // 微信授权
            
            if ([_model.ifBindWx isEqualToString:@"1"]) {
                WechatAccountChangeVC* wechat=[WechatAccountChangeVC new];
                PushTo(wechat, YES)
//                SHOWMSG(@"亲,已经绑定过微信啦")
            }else{
                if (CANOPENWechat){ // 安装了微信，打开微信授权
                    [ThirdPartyLogin bdWechatSuccess:^(id result) {
                        _model.ifBindWx = @"1" ;
                        [_tableView reloadData];
                    } fial:^(NSString *failStr) {  SHOWMSG(failStr)  }];
                }
            }
            
        } break;
            
        case 4:{ // 淘宝授权或重要提醒
            NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }else{
                SHOWMSG(@"暂时无法开启!")
            }
            
        } break;
            
        default: break;
    }
}

// 淘宝授权
-(void)cheakIfBindTB{
    [[ALBBSDK sharedInstance] ALBBSDKInit];
    if ([[ALBBSession sharedInstance] isLogin]) {
        SHOWMSG(@"已授权!")
    }else{
        [[ALBBSDK sharedInstance] auth:self successCallback:^(ALBBSession *session) {
            [self.tableView reloadData];
            SHOWMSG(@"授权成功!")
        } failureCallback:^(ALBBSession *session, NSError *error) {
            SHOWMSG(@"授权失败!")
        }];
    }
}


#pragma mark ===>>> 调用相册或相机
-(void)paizhaoORphoto:(UIAlertControllerStyle)stylet{
    
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSForegroundColorAttributeName:[UIColor darkGrayColor]};
    
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    UIAlertController*alertVC=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:stylet];
    UIAlertAction*actionCamera=[UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        
        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
        if(!isCamera){
            MBError(@"无法调用相机!");
            return;
        }
        _imgPickerVC.sourceType= UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_imgPickerVC animated:YES completion:nil];
    }];
    
    UIAlertAction*actionPhote=[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        _imgPickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:_imgPickerVC animated:YES completion:nil];
    }];
    UIAlertAction*actionCancle=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *  action) {  }];
    
    if (CANOPENWechat) {
        [alertVC addAction:actionCamera];
    }
    [alertVC addAction:actionPhote];
    [alertVC addAction:actionCancle];
    [self presentViewController:alertVC animated:YES completion:nil];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        if ([type isEqualToString:@"public.image"]) {
            UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
            UIImageWriteToSavedPhotosAlbum(image, self,@selector(image:didFinishSavingWithError:contextInfo:),nil);
        }
    }else{
        [_imgPickerVC dismissViewControllerAnimated:YES completion:^{
            UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
            _selectImg=image;
            [self upImg];
        }];
    }
}
//点击相册页面的取消按钮返回
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [_imgPickerVC dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (!error) {
        _selectImg=image;
        [_imgPickerVC dismissViewControllerAnimated:YES completion:^{
            [self upImg];
        }];
    }
}

//上传图片
-(void)upImg{
    if (_selectImg==nil) { return;  }
    
    NSString * url = @"";  NSString * fileName = @"";
    if (_indexUp==0) {
        url = url_upload_headimg ; // 上传头像
        fileName = @"headImg" ;
    }if(_indexUp==3){
        url = url_upload_wechatEwm ; // 微信二维码
        fileName=@"wxEwm";
        
        CIDetector*detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{ CIDetectorAccuracy : CIDetectorAccuracyHigh }];
        NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:_selectImg.CGImage]];
        NSString*cidResultStr=@"";
        if(features.count>0) {
            CIQRCodeFeature *feature = [features objectAtIndex:0];
            cidResultStr= feature.messageString;
        }else{
            SHOWMSG(@"亲，你选择图片中未包含二维码!")
            return;
        }
    }
    MBShow(@"正在上传...")
    NSString * upUrl = FORMATSTR(@"%@%@",dns_dynamic_main,url);
    [NetRequest urlUpFile:upUrl ptdic:nil pName:fileName upType:0 data:_selectImg success:^(id nwdic) {
        MBHideHUD  SHOWMSG(@"上传成功!")
        if (_indexUp==0) {
            [_model setValue:_selectImg forKey:@"headImg"];
        }if (_indexUp==3) {
            [_model setValue:_selectImg forKey:@"wxEwm"];
        }
        [_tableView reloadData];
    } progress:^(NSProgress *uploadProgress) {
        
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];

}


#pragma mark ===>>> 其他
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:BDWechatSuccess object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_tableView reloadData];
    
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.001],
                                 NSForegroundColorAttributeName:[UIColor clearColor]};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
}

#pragma mark ===>>> 退出登录弹窗提醒
-(void)backLoginAndRemove{
    
    [UIAlertController showIn:self title:@"提示" content:@"确定要退出当前账户吗?" ctAlignment:NSTextAlignmentCenter btnAry:@[@"确定",@"取消"] indexAction:^(NSInteger indexTag) {
        if (indexTag==0) { [self logOut]; }
    }];
    
}

-(void)logOut{
    
    // 退出登录时、 清除跟账号有关的缓存
    if (TOKEN.length>0) { NSUDRemoveData(@"TOKEN") }
    
    [self clearPersonalInformationCache];
    
    // 删除绑定的别名
    [JPUSHService deleteAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        if (iResCode==0) { NSLog(@"解绑别名成功!"); }
    } seq:666666];
    
    // 删除绑定的标签
    [JPUSHService cleanTags:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
        if (iResCode==0) { NSLog(@"解绑标签成功!"); }
    } seq:666666];
    
    // 将LoginVc 设置成window.rootvc
    [LoginVc showLoginVC:nil fromType:0 loginBlock:^(NSInteger result) { }];
    if (rongcloud_APP_Key.length>0) {
        // 退出融云 且不接受推送
        [[RCIMClient sharedRCIMClient] logout];
    }
    //    if (TBKEY.length>0&&IsNeedTaoBao==1) {
    [[ALBBSDK sharedInstance] ALBBSDKInit];
    if ([[ALBBSession sharedInstance] isLogin]) {
        [[ALBBSDK sharedInstance] logout];
    }
    //    }
    
}

// 清除个人信息有关的缓存 (不影响正常使用)
-(void)clearPersonalInformationCache{
    
    @try {
        if (NSUDTakeData(UserInfo)) { NSUDRemoveData(UserInfo) }               // 用户信息
        if (NSUDTakeData(ShareMuBan)) { NSUDRemoveData(ShareMuBan) }           // 用户分享模板
        if (NSUDTakeData(ApplyUpgrade)) { NSUDRemoveData(ApplyUpgrade) }       // 用户升级信息
        if (NSUDTakeData(PromotionPoster)) { NSUDRemoveData(PromotionPoster) } // 用户分享海报
        if (NSUDTakeData(JD_ConsumerData)) { NSUDRemoveData(JD_ConsumerData) } // 用户京东消费佣金
        if (NSUDTakeData(TB_ConsumerData)) { NSUDRemoveData(TB_ConsumerData) } // 用户淘宝消费佣金
        
        // 清除广告缓存
        if (NSUDTakeData(ad_floating_imgUrl)) { NSUDRemoveData(ad_floating_imgUrl) }
        if (NSUDTakeData(ad_popView_imgUrl)) { NSUDRemoveData(ad_popView_imgUrl) }
        if (NSUDTakeData(ad_launch_imgUrl)) { NSUDRemoveData(ad_launch_imgUrl) }
        if (NSUDTakeData(ad_floating_dic)) { NSUDRemoveData(ad_floating_dic) }
        if (NSUDTakeData(ad_popView_dic)) { NSUDRemoveData(ad_popView_dic) }
        if (NSUDTakeData(ad_launch_dic)) { NSUDRemoveData(ad_launch_dic) }
        if (NSUDTakeData(ad_launch_img)) { NSUDRemoveData(ad_launch_img) }
        
        // 清除动态模块缓存
        if (NSUDTakeData(app_dynamic_my_dic)) { NSUDRemoveData(app_dynamic_my_dic) }
        if (NSUDTakeData(app_dynamic_my_time)) { NSUDRemoveData(app_dynamic_my_time) }
        if (NSUDTakeData(app_dynamic_home_dic)) { NSUDRemoveData(app_dynamic_home_dic) }
        if (NSUDTakeData(app_dynamic_home_time)) { NSUDRemoveData(app_dynamic_home_time) }
        if (NSUDTakeData(app_dynamic_tabbar_dic)) { NSUDRemoveData(app_dynamic_tabbar_dic) }
        if (NSUDTakeData(app_dynamic_tabbar_time)) { NSUDRemoveData(app_dynamic_tabbar_time) }
        
    } @catch (NSException *exception) { } @finally { }
    
}
// 清除一些公共的数据缓存 (不影响正常使用)
-(void)clearPublicInformationCache{
    if (NSUDTakeData(HomeOneList)!=nil) { NSUDRemoveData(HomeOneList) }         // 首页商品数据缓存
    if (NSUDTakeData(KeFuZhongXin)!=nil) { NSUDRemoveData(KeFuZhongXin) }       // 首页商品数据缓存
    if (NSUDTakeData(OneDayRanking)!=nil) { NSUDRemoveData(OneDayRanking) }     // 首页商品数据缓存
    if (NSUDTakeData(SevenDayRanking)!=nil) { NSUDRemoveData(SevenDayRanking) } // 首页商品数据缓存
}


@end

