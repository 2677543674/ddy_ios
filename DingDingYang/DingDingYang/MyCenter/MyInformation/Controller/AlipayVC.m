//
//  AlipayVC.m

//
//  Created by ddy on 2017/12/27.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "AlipayVC.h"
#import "InformationEditingVC.h"

@interface AlipayVC ()
@property(nonatomic,strong)UIView*backView;//背景框
@property(nonatomic,strong)UILabel* account;
@property(nonatomic,strong)UILabel* name;
@end

@implementation AlipayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"支付宝账号";
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [self backView];
}

-(void)viewWillAppear:(BOOL)animated{
    _account.text=[NSString stringWithFormat:@"  支付宝账号      %@",_infoModel.aliNo];
    _name.text=[NSString stringWithFormat:@"  支付宝实名      %@",_infoModel.nickName];
}

-(UIView *)backView{
    if (_backView==nil) {
        _backView=[[UIView alloc]init];
        _backView.backgroundColor=COLORWHITE;
        _backView.layer.masksToBounds=YES;
        _backView.layer.cornerRadius=6;
        _backView.frame=CGRectMake(10,10, ScreenW-20, 100);
        [self.view addSubview:_backView];
        
        _account=[UILabel labText:[NSString stringWithFormat:@"  支付宝账号      %@",_infoModel.aliNo] color:Black51 font:14];
        [_backView addSubview:_account];
        [_account mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(_backView);
            make.height.offset(50);
        }];
        
        _name=[UILabel labText:[NSString stringWithFormat:@"  支付宝实名      %@",_infoModel.nickName] color:Black51 font:14];
        [_backView addSubview:_name];
        [_name mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.equalTo(_backView);
            make.height.offset(50);
        }];
        
        UIButton* changeBtn=[UIButton new];
        [changeBtn setTitle:@"修改" forState:UIControlStateNormal];
        changeBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [changeBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        [changeBtn addTarget:self  action:@selector(clickChange) forControlEvents:UIControlEventTouchUpInside];
        [_backView addSubview:changeBtn];
        [changeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_account.mas_right).offset(0);
            make.width.offset(50*HWB);
            make.top.bottom.centerY.equalTo(_account);
        }];
        
    }
    return _backView;
}

-(void)clickChange{
    InformationEditingVC*editVC=[InformationEditingVC new];
    editVC.index=2;
    editVC.infoModel=_infoModel;
    PushTo(editVC, YES);
}



@end
