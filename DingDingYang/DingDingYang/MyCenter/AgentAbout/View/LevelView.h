//
//  LevelView.h
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LevelView : UIView

@property(nonatomic,strong)UILabel*labOne,*labTwo,*labThree;
@property(nonatomic,copy)NSString*oneStr,*twoStr,*threeStr;
@property(nonatomic,strong)UIColor*bgColor;
@property(nonatomic,strong)UIColor*titColor;
@end
