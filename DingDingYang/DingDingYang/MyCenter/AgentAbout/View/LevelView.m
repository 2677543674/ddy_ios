//
//  LevelView.m
//  DingDingYang
//
//  Created by ddy on 2017/3/8.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "LevelView.h"

@implementation LevelView

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 3 ;
        
        float  width=self.frame.size.width;
        float  height=self.frame.size.height;
        self.backgroundColor=[UIColor clearColor];
        
        _labOne=[[UILabel alloc]init];
        _labOne.backgroundColor=COLORWHITE;
        _labOne.textColor=Black51;
        _labOne.font=Font15;
        _labOne.adjustsFontSizeToFitWidth=YES;
        _labOne.textAlignment=NSTextAlignmentCenter;
        _labOne.frame=CGRectMake(0, 0,width/2-1,height);
        [self addSubview:_labOne];
        
        _labTwo=[[UILabel alloc]init];
        _labTwo.backgroundColor=COLORWHITE;
        _labTwo.textColor=Black51;
        _labTwo.font=Font15;
        _labTwo.adjustsFontSizeToFitWidth=YES;
        _labTwo.textAlignment=NSTextAlignmentCenter;
        _labTwo.frame=CGRectMake(width/2, 0, width/4-1, height);
        [self addSubview:_labTwo];
        
        _labThree=[[UILabel alloc]init];
        _labThree.backgroundColor=COLORWHITE;
        _labThree.textColor=Black51;
        _labThree.font=Font15;
        _labThree.adjustsFontSizeToFitWidth=YES;
        _labThree.textAlignment=NSTextAlignmentCenter;
        _labThree.frame=CGRectMake(width/4*3, 0, width/4, height);
        [self addSubview:_labThree];
        
    }
    return self;
}
-(void)setOneStr:(NSString *)oneStr{
    _labOne.text=oneStr;
}
-(void)setTwoStr:(NSString *)twoStr{
    _labTwo.text=twoStr;
}
-(void)setThreeStr:(NSString *)threeStr{
    _labThree.text=threeStr;
}

-(void)setBgColor:(UIColor *)bgColor{
    _bgColor = bgColor;
    _labOne.backgroundColor = bgColor;
    _labTwo.backgroundColor = bgColor;
    _labThree.backgroundColor = bgColor;
}

-(void)setTitColor:(UIColor *)titColor{
    _titColor = titColor ;
    _labOne.textColor = titColor;
    _labTwo.textColor = titColor;
    _labThree.textColor = titColor;
}


@end
