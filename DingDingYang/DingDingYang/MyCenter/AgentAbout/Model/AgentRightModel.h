//
//  AgentRightModel.h
//  DingDingYang
//
//  Created by ddy on 2017/6/15.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgentRightModel : NSObject

@property(nonatomic,copy) NSString * amount ;       // 支付价格
@property(nonatomic,copy) NSString * bronzeDay ;    // 可用天数 bronzeDay
@property(nonatomic,copy) NSString * arid ;         // id标识
@property(nonatomic,copy) NSString * ins ;          // 描述
@property(nonatomic,copy) NSString * name ;         // 等级名称
@property(nonatomic,copy) NSString * shortDes ;     // 价格单位(永久，一年，一个月等等)
@property(nonatomic,copy) NSString * usePromoCode ; // 0：不能使用试用码 1：可以使用试用码 (所有app都不要试用码了)

-(instancetype)initWithDic:(NSDictionary*)dic ;

@end
