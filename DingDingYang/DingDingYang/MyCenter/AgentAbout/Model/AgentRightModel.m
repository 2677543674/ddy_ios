//
//  AgentRightModel.m
//  DingDingYang
//
//  Created by ddy on 2017/6/15.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "AgentRightModel.h"

@implementation AgentRightModel

-(instancetype)initWithDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        self.amount = REPLACENULL(dic[@"amount"]) ;
        self.bronzeDay = REPLACENULL(dic[@"bronzeDay"]) ;
        self.arid = REPLACENULL(dic[@"id"]) ;
        self.ins = REPLACENULL(dic[@"ins"]) ;
        self.name = REPLACENULL(dic[@"name"]) ;
        self.shortDes = REPLACENULL(dic[@"shortDes"]) ;
        self.usePromoCode = REPLACENULL(dic[@"usePromoCode"]) ;
    }
    return self ;
}

@end
