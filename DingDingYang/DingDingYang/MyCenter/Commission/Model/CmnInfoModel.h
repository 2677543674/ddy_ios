//
//  CmnInfoModel.h
//  DingDingYang
//
//  Created by ddy on 2018/1/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

// 展示的数据
@interface CmnInfoModel : NSObject

@property(nonatomic,copy) NSString * title ; // 标题
@property(nonatomic,copy) NSString * value ; // 数值
@property(nonatomic,copy) NSString * color ; // 颜色
@property(nonatomic,copy) NSString * state ; // 状态(月份的统计才有)
@property(nonatomic,assign) NSInteger ifAboutMoney ; // 是否跟钱有关 1:是(拼接￥符号)  0:不是(不拼￥)

-(instancetype)initWithDic:(NSDictionary*)dic ;

@end

// 按天算，区分哪一天，有筛选类型
@interface CmnDayInfoModel : NSObject
@property(nonatomic,copy)   NSString * stitle ; // 筛选条件标题
@property(nonatomic,copy)   NSString * svalue ; // 筛选条件value
@property(nonatomic,assign) NSInteger  isslt ;  // 是否选中 (0:未选中 1:已选中)
@property(nonatomic,strong) NSMutableArray * dataAry ; // 需展示的数据
-(instancetype)initWithDic:(NSDictionary*)dic;
@end



@interface CommissionModel: NSObject
@property(nonatomic,assign) NSInteger pType; // 平台类型
@property(nonatomic,copy) NSString * ptitle; // 平台标题
@property(nonatomic,copy) NSString * reqUrl; // 请求链接

// 服务器参数
@property(nonatomic,copy) NSString * wallet; // 余额
@property(nonatomic,copy) NSString * desc;   // 描述
@property(nonatomic,strong) NSMutableArray * monthDataAry; // 月份的model
@property(nonatomic,strong) NSMutableArray * dayDataAry;   // 按天算的model

-(void)addModelByPlatform:(NSString*)platform data:(NSDictionary*)dic;

@end


