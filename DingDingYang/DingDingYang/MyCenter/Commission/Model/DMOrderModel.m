//
//  DMOrderModel.m
//  DingDingYang
//
//  Created by ddy on 2018/9/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DMOrderModel.h"

@implementation DMOrderModel

-(instancetype)initWithDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        self.reminder = REPLACENULL(dic[@"reminder"]);
        self.logo = REPLACENULL(dic[@"logo"]);
        self.orderTime = TimeByStamp(REPLACENULL(dic[@"orderTime"]), @"yyyy-MM-dd HH:mm");
        self.confirmTime = TimeByStamp(REPLACENULL(dic[@"confirmTime"]), @"yyyy-MM-dd HH:mm");
        self.orderSn = REPLACENULL(dic[@"orderSn"]);
        self.commission = ROUNDED(REPLACENULL(dic[@"commission"]), 2);
        self.activityName = REPLACENULL(dic[@"activityName"]);
        
        NSArray * detailAry = NULLARY(dic[@"detailList"]);
        
        self.detailListAry = [NSMutableArray array];
        [detailAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            DMOrderDModel * dModel = [[DMOrderDModel alloc]initWithDic:obj];
            dModel.commission = self.commission;
            [self.detailListAry addObject:dModel];
        }];
        
        self.status = [REPLACENULL(dic[@"status"]) integerValue];
        
        switch (self.status) {
            case -1:{ self.statusStr = @"订单无效"; } break;
            case 0:{ self.statusStr = @"未确认"; } break;
            case 1:{ self.statusStr = @"订单已付款"; } break;
            case 2:{ self.statusStr = @"订单已结算"; } break;
            default:  break;
        }
        
    }
    return self;
}

@end


@implementation DMOrderDModel

-(instancetype)initWithDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        self.goodsCateName = REPLACENULL(dic[@"goodsCateName"]);
        self.goodsId = REPLACENULL(dic[@"goodsId"]);
        self.goodsPrice = ROUNDED(REPLACENULL(dic[@"goodsPrice"]), 2);
        self.goodsTa = REPLACENULL(dic[@"goodsTa"]);
        self.orderSn = REPLACENULL(dic[@"orderSn"]);
        self.goodsCate = REPLACENULL(dic[@"goodsCate"]);
        self.ordersPrice = ROUNDED(REPLACENULL(dic[@"ordersPrice"]), 2);
        self.goodsName = REPLACENULL(dic[@"goodsName"]);
    }
    return self;
}

@end
