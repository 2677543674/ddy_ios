//
//  CmnInfoModel.m
//  DingDingYang
//
//  Created by ddy on 2018/1/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "CmnInfoModel.h"

@implementation CmnInfoModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.title = REPLACENULL(dic[@"title"]) ;
        self.value = ROUNDED(REPLACENULL(dic[@"value"]),2);
        self.color = REPLACENULL(dic[@"color"]);
        self.state = REPLACENULL(dic[@"label"]);
        self.ifAboutMoney = [REPLACENULL(dic[@"ifAboutMoney"]) integerValue];
    }
    return self ;
}
@end


@implementation CmnDayInfoModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.isslt = 0 ;
        self.stitle = REPLACENULL(dic[@"title"]) ;
        self.svalue = REPLACENULL(dic[@"value"]) ;
        self.dataAry = [NSMutableArray array];
        NSArray * ary = NULLARY(dic[@"statsList"]);
        [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CmnInfoModel * info = [[CmnInfoModel alloc]initWithDic:obj];
            [_dataAry addObject:info];
        }];
    }
    return self ;
}

@end


#pragma mark ===>>> 多种消费佣金控制

@implementation CommissionModel

-(instancetype)init{
    self = [super init];
    if (self) {
        self.ptitle = @"";
        self.reqUrl = @"";
        self.wallet = @"0.0";
        self.desc = @"";
        self.monthDataAry = [NSMutableArray array];
        self.dayDataAry = [NSMutableArray array];
    }
    return self;
}

-(void)addModelByPlatform:(NSString *)platform data:(NSDictionary *)dic{
    self.desc = REPLACENULL(dic[@"desc"]);
    self.wallet = ROUNDED(REPLACENULL(dic[@"wallet"]), 2);
    
    NSArray * dataAry = NULLARY(dic[@"statsList"]);
    if (dataAry.count>0) {
        
        NSDictionary * dataDic = dataAry[0];
        
        NSArray * mouthAry = NULLARY(dataDic[@"statsIncomeList"]);
        if (self.monthDataAry.count>0&&mouthAry.count>0) { [self.monthDataAry removeAllObjects]; }
        [mouthAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CmnInfoModel * model = [[CmnInfoModel alloc]initWithDic:obj];
            [self.monthDataAry addObject:model];
        }];
        
        NSArray * dayAry = NULLARY(dataDic[@"statsNumList"]);
        if (self.dayDataAry.count>0&&dayAry.count>0) { [self.dayDataAry removeAllObjects]; }
        [dayAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CmnDayInfoModel * model = [[CmnDayInfoModel alloc]initWithDic:obj];
            if (idx==0) { model.isslt = 1 ; } // 第一个选项默认设置为选中状态
            [self.dayDataAry addObject:model];
        }];
    }
    
}

@end

