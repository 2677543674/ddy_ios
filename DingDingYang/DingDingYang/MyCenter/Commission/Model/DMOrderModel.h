//
//  DMOrderModel.h
//  DingDingYang
//
//  Created by ddy on 2018/9/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMOrderModel : NSObject

@property(nonatomic,copy) NSString * reminder;   // 温馨提示
@property(nonatomic,copy) NSString * logo;       // 平台logo
@property(nonatomic,copy) NSString * orderTime;  // 订单时间
@property(nonatomic,copy) NSString * confirmTime; // 结算时间
@property(nonatomic,copy) NSString * orderSn;    // 订单编号
@property(nonatomic,copy) NSString * commission; // 佣金
@property(nonatomic,copy) NSString * activityName; // 平台名称

@property(nonatomic,copy) NSString * statusStr; // 订单状态 ： -1 无效 0 未确认 1 确认 2 结算
@property(nonatomic,assign) NSInteger status;     // 订单状态 ： -1 无效 0 未确认 1 确认 2 结算

@property(nonatomic,strong) NSMutableArray * detailListAry;

-(instancetype)initWithDic:(NSDictionary*)dic;

@end

@interface DMOrderDModel : NSObject
@property(nonatomic,copy) NSString * goodsCateName; // 商品分类名称
@property(nonatomic,copy) NSString * goodsId;     // 商品id
@property(nonatomic,copy) NSString * goodsPrice;  // 商品价格
@property(nonatomic,copy) NSString * goodsTa;     // 购买数量
@property(nonatomic,copy) NSString * orderSn;     // 订单编号
@property(nonatomic,copy) NSString * goodsCate;   // 商品分类id
@property(nonatomic,copy) NSString * ordersPrice; // 购买金额
@property(nonatomic,copy) NSString * goodsName;   // 商品名称
@property(nonatomic,copy) NSString * commission; // 佣金

-(instancetype)initWithDic:(NSDictionary*)dic;

@end



