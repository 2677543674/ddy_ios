//
//  CsitModel.m
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "CsitModel.h"

@implementation CsitModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        self.imgUrl =  [REPLACENULL(dic[@"goodsImg"]) hasPrefix:@"http"]?REPLACENULL(dic[@"goodsImg"]):FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"goodsImg"]));
        self.nameTitle = REPLACENULL(dic[@"name"]);
        self.consumption = ROUNDED(REPLACENULL(dic[@"price"]), 2);
        self.reward = ROUNDED(REPLACENULL(dic[@"commission"]), 2) ;
        self.goodsId = REPLACENULL(dic[@"goodsId"]);
        self.status = [REPLACENULL(dic[@"status"]) integerValue];
        self.createTime = TimeByStamp(REPLACENULL(dic[@"createTime"]), @"yyyy年MM月dd日 HH:mm") ;
        self.buyNum = REPLACENULL(dic[@"buyNum"]);
        self.orderNo = REPLACENULL(dic[@"orderNo"]);
        
        if (REPLACENULL(dic[@"clearingTime"]).length>10) {
            self.clearingTime = FORMATSTR(@"%@ 结算",TimeByStamp(REPLACENULL(dic[@"clearingTime"]), @"yyyy年MM月dd日 HH:mm"))  ;
        }else{
            self.clearingTime = @"订单结算";
        }
        
    }
    return self;
}

@end
