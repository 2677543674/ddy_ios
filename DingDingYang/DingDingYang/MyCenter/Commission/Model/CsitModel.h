//
//  CsitModel.h
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CsitModel : NSObject
@property(nonatomic,copy) NSString * imgUrl ;    // 图片链接
@property(nonatomic,copy) NSString * nameTitle ; // 商品标题
@property(nonatomic,copy) NSString * consumption;// 消费
@property(nonatomic,copy) NSString * reward ;    // 奖励
@property(nonatomic,copy) NSString * goodsId ;   // 商品ID
@property(nonatomic,copy) NSString * createTime; // 订单创建时间
@property(nonatomic,copy) NSString * clearingTime; // 结算时间
@property(nonatomic,copy) NSString * buyNum ;    // 数量
@property(nonatomic,copy) NSString * orderNo ;   // 订单编号
@property(nonatomic,assign)NSInteger status ;    // 状态1:已支付 2:订单结算  3:订单失效

-(instancetype)initWithDic:(NSDictionary*)dic;
@end
