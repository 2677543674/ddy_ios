//
//  NewConsumerCell.m

//
//  Created by ddy on 2017/12/26.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "NewConsumerCell.h"

@implementation NewConsumerCell


-(void)setOrderModel:(CsitModel *)orderModel{
    
    _orderModel = orderModel;
    [_goodsImageV sd_setImageWithURL:[NSURL URLWithString:_orderModel.imgUrl] placeholderImage:PlaceholderImg];
    _priceLab.text  = FORMATSTR(@"￥%@",orderModel.consumption);
    _numberLab.text = FORMATSTR(@"订单号:%@",orderModel.orderNo);
    _timeLab.text   = FORMATSTR(@"%@ 创建",orderModel.createTime);
    _rewardLab.text = FORMATSTR(@"奖励:￥%@",orderModel.reward);
    _nameLab.text   = orderModel.nameTitle;
    
    switch (orderModel.status) {
        case 1:{  _statusLab.text = @"已支付"; }  break;
        case 2:{  _statusLab.text = _orderModel.clearingTime; }  break;
        case 3:{  _statusLab.text = @"订单失效"; }  break;
        default:  break;
    }
    
    if ([_type isEqualToString:@"pdd"]) {
        switch (orderModel.status) {
            case -1:{  _statusLab.text = @"未支付"; }  break;
            case 0:{  _statusLab.text = @"已支付"; }  break;
            case 1:{  _statusLab.text = @"已成团"; }  break;
            case 2:{  _statusLab.text = @"确认收货"; }  break;
            case 3:{  _statusLab.text = @"审核成功"; }  break;
            case 4:{  _statusLab.text = @"审核失败"; }  break;
            case 5:{  _statusLab.text = @"已经结算"; }  break;
            case 8:{  _statusLab.text = @"非多多进宝商品"; }  break;
            default:  break;
        }
    }
    
    if ([_type isEqualToString:@"wph"]) {
        switch (orderModel.status) {
            case -1:{  _statusLab.text = @"已拒绝"; }  break;
            case 3:{  _statusLab.text = @"处理中"; }  break;
            case 4:{  _statusLab.text = @"已完结"; }  break;
            default:  break;
        }
    }
    
    if ([_type isEqualToString:@"mt"]) {
        switch (orderModel.status) {
            case -1:{  _statusLab.text = @"已失效"; }  break;
            case 0:{  _statusLab.text = @"待审核"; }  break;
            case 1:{  _statusLab.text = @"结算"; }  break;
            default:  break;
        }
    }
    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self topBgView];
        
        [self goodsImageV];
        [self nameLab];
        [self priceLab];
        
        [self rewardLab];
        [self numberLab];
        
        
        [self timeLab];
        [self statusLab];
        
        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.bottom.offset(0);
            make.height.offset(6*HWB);
        }];
        
    }
    return self;
}

-(UIView*)topBgView{
    if (!_topBgView) {
        _topBgView = [[UIView alloc]init];
        [self.contentView addSubview:_topBgView];
        [_topBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0); make.bottom.offset(-26*HWB);
        }];
        
        UIView * line = [UIView new];
        line.backgroundColor = COLORGROUP ;
        [_topBgView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0); make.height.offset(1);
            make.left.offset(0);  make.right.offset(0);
        }];
    }
    return _topBgView ;
}

-(UIImageView *)goodsImageV{
    if (!_goodsImageV) {
        _goodsImageV=[UIImageView new];
        _goodsImageV.image=[UIImage imageNamed:@"头像1"];
        _goodsImageV.layer.cornerRadius = 3;
        _goodsImageV.clipsToBounds = YES;
        _goodsImageV.contentMode = UIViewContentModeScaleAspectFill;
        [_topBgView addSubview:_goodsImageV];
        [_goodsImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(8*HWB); make.left.offset(10*HWB);
            make.bottom.offset(-8*HWB);
            make.width.equalTo(_goodsImageV.mas_height);
        }];
    }
    return _goodsImageV;
}

-(UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab = [UILabel labText:@"商品标题商品标题" color:Black51 font:13*HWB];
        [_topBgView addSubview:_nameLab];
        [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsImageV.mas_top); make.right.offset(-5*HWB);
            make.left.equalTo(_goodsImageV.mas_right).offset(8*HWB);
        }];
    }
    return _nameLab;
}


-(UILabel *)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥0.0" color:LOGOCOLOR font:16*HWB];
        if (THEMECOLOR==2) {
            _priceLab.textColor=[UIColor redColor];
        }
        [_topBgView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_goodsImageV.mas_centerY).offset(2);
            make.left.equalTo(_goodsImageV.mas_right).offset(8*HWB);
        }];
    }
    return _priceLab;
}

-(UILabel *)numberLab{
    if (!_numberLab) {
        _numberLab = [UILabel labText:@"订单号:" color:Black102 font:10*HWB];
        [_topBgView addSubview:_numberLab];
        [_numberLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImageV.mas_right).offset(8*HWB);
            make.bottom.equalTo(_goodsImageV.mas_bottom);
        }];
    }
    return _numberLab;
}

-(UILabel *)rewardLab{
    if (!_rewardLab) {
        _rewardLab = [UILabel labText:@"奖励:" color:Black102 font:11.5*HWB];
        if (THEMECOLOR==2) {
            _rewardLab.textColor=[UIColor redColor];
        }
        [_topBgView addSubview:_rewardLab];
        [_rewardLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5*HWB);
            make.bottom.offset(-4*HWB);
        }];
    }
    return _rewardLab;
}

-(UILabel *)timeLab{
    if (!_timeLab) {
    
        _timeLab = [UILabel labText:@"创建时间" color:Black102 font:10*HWB];
        [self.contentView addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);  make.bottom.offset(-6*HWB);
            make.top.equalTo(_topBgView.mas_bottom).offset(0);
        }];
        
    }
    return _timeLab;
}

-(UILabel *)statusLab{
    if (!_statusLab) {
        _statusLab = [UILabel labText:@"状态" color:Black102 font:10*HWB];
        [self.contentView addSubview:_statusLab];
        [_statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);  make.bottom.offset(-6*HWB);
            make.top.equalTo(_topBgView.mas_bottom).offset(0);
        }];
    }
    return _timeLab;
}

@end


@implementation ConsumerHeader

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = LOGOCOLOR ;
        
        UIView * searchView = [UIView new];
        searchView.backgroundColor = COLORGROUP;
        searchView.layer.cornerRadius = 16*HWB ;
        searchView.clipsToBounds=YES;
        [self addSubview:searchView];
        [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY); make.height.offset(32*HWB);
            make.left.offset(30*HWB);  make.right.offset(-30*HWB);
        }];
        
        _searchBtn = [UIButton new];
        [_searchBtn setBackgroundColor:COLORWHITE];
        if (THEMECOLOR==2) {  _searchBtn.tintColor = Black102 ;} // 主题色为浅色，渲染色改为黑色
        UIImage * newImg = [UIImage imageNamed:@"search_white"];
        newImg =[newImg imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_searchBtn setImage:newImg forState:UIControlStateNormal];
        [searchView addSubview:_searchBtn];
        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.right.offset(0);  make.width.offset(55*HWB);
        }];
        
        _searchText = [UITextField textColor:Black51 PlaceHorder:@"搜索订单号" font:13*HWB];
        _searchText.keyboardType = UIKeyboardTypeNumberPad ;
        [searchView addSubview:_searchText];
        [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.right.equalTo(_searchBtn.mas_left).offset(-2);
            make.centerY.equalTo(searchView.mas_centerY);
        }];
        
        
    }
    return self;
}

@end




