//
//  DMOrderCell.m
//  DingDingYang
//
//  Created by ddy on 2018/9/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DMOrderCell.h"

@implementation DMOrderCell

-(void)setDModel:(DMOrderDModel *)dModel{
    _dModel = dModel;
    _goodsTitLab.text = _dModel.goodsName;
    _commissionLab.text = FORMATSTR(@"约%@元",_dModel.commission);
}

-(void)setDModel2:(DMOrderDModel *)dModel2{
    _dModel2 = dModel2;
    _goodsTitLab.text = _dModel2.goodsName;
    _commissionLab.text = FORMATSTR(@"实付款: %@元",_dModel2.ordersPrice);
}

-(void)setModel:(DMOrderModel *)model{
    _model = model;
    _statesLab.text = _model.statusStr;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = RGBA(249, 250, 251, 1);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // 中间部分
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.image = [UIImage imageNamed:@"logo"];
        _goodsImgV.backgroundColor = COLORWHITE;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.top.offset(10*HWB);
            make.bottom.offset(-10*HWB);
            make.width.equalTo(_goodsImgV.mas_height);
        }];
        
        _goodsTitLab = [UILabel labText:@"商品标题" color:Black51 font:12.5*HWB];
        _goodsTitLab.numberOfLines = 2;
        [self.contentView addSubview:_goodsTitLab];
        [_goodsTitLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(12*HWB);
            make.right.offset(-10*HWB);
            make.top.offset(12*HWB);
        }];
        
        _commissionLab = [UILabel labText:@"约:0.0元" color:LOGOCOLOR font:13*HWB];
        [self.contentView addSubview:_commissionLab];
        [_commissionLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(12*HWB);
            make.bottom.offset(-12*HWB);
        }];
        
        _statesLab = [UILabel labText:@"" color:Black102 font:12*HWB];
        [self.contentView addSubview:_statesLab];
        [_statesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_commissionLab.mas_centerY);
        }];
        
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end






@implementation DMOrderCellHead

-(void)setModel:(DMOrderModel *)model{
    _model = model;
//    [_platformImgV sd_setImageWithURL:[NSURL URLWithString:_model.logo] placeholderImage:nil];
    _platformLab.text = _model.activityName;
    _orderNoLab.text = FORMATSTR(@"订单号:%@",_model.orderSn);
}

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView * bgimgv = [[UIImageView alloc]init];
        bgimgv.backgroundColor = COLORGROUP;
        [self.contentView addSubview:bgimgv];
        [bgimgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.offset(0);
            make.height.offset(8*HWB);
        }];
        
        self.contentView.backgroundColor = COLORWHITE;
        
        // 上部分
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.image = [UIImage imageNamed:@"ddy_dmfl_store"];
        [self.contentView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB); make.top.offset(16*HWB);
            make.height.width.offset(14*HWB);
        }];
        
        _platformLab = [UILabel labText:@"来源" color:Black51 font:13*HWB];
        [self.contentView addSubview:_platformLab];
        [_platformLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_platformImgV.mas_right).offset(3*HWB);
            make.centerY.equalTo(_platformImgV.mas_centerY);
        }];
        
        _orderNoLab = [UILabel labText:@"订单号" color:Black51 font:12*HWB];
        [self.contentView addSubview:_orderNoLab];
        [_orderNoLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_platformImgV.mas_centerY);
        }];
    }
    return self;
}

@end




@implementation DMOrderCellFoot

-(void)setModel:(DMOrderModel *)model{
    _model = model;
    _creatTimeLab.text = FORMATSTR(@"创建: %@",_model.orderTime);
    if (_model.confirmTime.length>0) {
        _settlementTimeLab.hidden = NO;
        _settlementTimeLab.text = FORMATSTR(@"结算: %@",_model.confirmTime);
    }else{
        _settlementTimeLab.hidden = YES;
    }
   
}

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE;

        // 下部
        _creatTimeLab = [UILabel labText:@"创建时间" color:Black102 font:12*HWB];
        [self.contentView addSubview:_creatTimeLab];
        [_creatTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
        
        _settlementTimeLab = [UILabel labText:@"结算时间" color:Black102 font:12*HWB];
        [self.contentView addSubview:_settlementTimeLab];
        [_settlementTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_creatTimeLab.mas_centerY);
        }];
    }
    return self;
}

@end






