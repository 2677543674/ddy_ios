//
//  CommissionView.m
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "CommissionView.h"
#import "CmMonthCltCell.h"
#import "BillVC.h"
#import "NewConsumer.h"    // 订单列表页
#import "DMOrdereListVC.h" // 多麦订单列表

@implementation CommissionView

-(void)setCmnModel:(CommissionModel *)cmnModel{
    _cmnModel = cmnModel;
    _cmoneyLab.text = FORMATSTR(@"￥ %@",_cmnModel.wallet);
    [_moneyCltView reloadData];
}

-(instancetype)init{
    self = [super init];
    if (self) {
     
        _cmnModel = [[CommissionModel alloc]init];
        
        [self creatTopMoneyView];
        [self moneyCltView];
        
    }
    return self;
}


-(void)creatTopMoneyView{
    _topMoneyView = [[UIView alloc]init];
    _topMoneyView.frame = CGRectMake(0, 0, ScreenW, 80*HWB);
    _topMoneyView.backgroundColor = LOGOCOLOR ;
    [self addSubview:_topMoneyView];
    
    UILabel * yueLab1 = [UILabel labText:@"余额" color:COLORWHITE font:13*HWB];
    [_topMoneyView addSubview:yueLab1];
    [yueLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15*HWB); make.height.offset(20*HWB);
        make.bottom.equalTo(_topMoneyView.mas_centerY).offset(-5*HWB);
    }];
    
    _cmoneyLab = [UILabel labText:@"¥ 0.0" color:COLORWHITE font:25*HWB];
    [_topMoneyView addSubview:_cmoneyLab];
    [_cmoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(12*HWB);
        make.top.equalTo(_topMoneyView.mas_centerY).offset(-5*HWB);
    }];
    
    _makeDrawalBtn = [UIButton titLe:@"提现" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13*HWB];
    [_makeDrawalBtn boardWidth:1 boardColor:COLORWHITE corner:14.0*HWB];
    [_makeDrawalBtn addShadowLayer];
    ADDTARGETBUTTON(_makeDrawalBtn, clickWithdrawalWay)
    [_topMoneyView addSubview:_makeDrawalBtn];
    [_makeDrawalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15*HWB); make.centerY.equalTo(_topMoneyView.mas_centerY);
        make.width.offset(70*HWB); make.height.offset(28*HWB);
    }];
    
    if (THEMECOLOR==2) { // 主题色为浅色
        yueLab1.textColor = Black51;
        _cmoneyLab.textColor = Black51 ;
        [_makeDrawalBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        _makeDrawalBtn.layer.borderColor = Black51.CGColor ;
    }
}

-(void)clickWithdrawalWay{
//    SHOWMSG(@"点击了提现!")
    [MyPageJump withdrawalType:2 platform:_cmnModel.pType totalAmount:_cmnModel.wallet results:^(NSInteger result) {
        
    }];
}


#pragma mark ===>>> 创建CollectionView
-(UICollectionView*)moneyCltView{
    if (!_moneyCltView) {
        UICollectionViewFlowLayout * layout1 = [[UICollectionViewFlowLayout alloc]init];
        layout1.minimumLineSpacing = 0 ; layout1.minimumInteritemSpacing = 0 ;
        _moneyCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout1];
        _moneyCltView.delegate = self ;  _moneyCltView.dataSource = self ;
        _moneyCltView.backgroundColor = COLORWHITE ;
        _moneyCltView.frame = CGRectMake(0, 80*HWB, ScreenW , ScreenH-NAVCBAR_HEIGHT-80*HWB);
        [self addSubview:_moneyCltView] ;
        
        [_moneyCltView registerClass:[CmMonthCltCell class] forCellWithReuseIdentifier:@"month_clt_cell1"];
        [_moneyCltView registerClass:[CmDayCltCell class] forCellWithReuseIdentifier:@"day_clt_cell1"];
        [_moneyCltView registerClass:[CmDaySltCell class] forCellWithReuseIdentifier:@"day_slt_cltCell1"];
        [_moneyCltView registerClass:[CmImgTxtCell class] forCellWithReuseIdentifier:@"img_txt_cltCell1"];
        
        [_moneyCltView registerClass:[CmTxtReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"des_head_view"];
        [_moneyCltView registerClass:[CmTxtReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"des_foot_view"];
        
        /*
         共四个分区
         分区0: 按月份统计显示的cell
             item: = monthDataAry 的数量
             区尾: 显示提示语
         
         分区1: 入口
             item0: 订单明细下一级入口
             item1: 我的账单下一级入口
        
         分区2: 显示筛选条件，今日昨日或其它
             item: = dayDataAry 的数量
         
         分区3: 对应分区2的条件显示
             item: = dayDataAry 中的 statsList(model.dataAry) 的数量
         */
        
        
    }else{ [_moneyCltView reloadData]; }
    return _moneyCltView ;
}

#pragma mark ===>>> 表格代理
// 分区个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 4 ;
//    if (_cmnModel.dayDataAry.count>0&&_cmnModel.monthDataAry.count>0) { return 4 ; }

//    return 0 ;
}

// 每个分区的cell个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (section==0) { return _cmnModel.monthDataAry.count; }
    if (section==1) { return 2 ; }
    if (section==2) { return _cmnModel.dayDataAry.count; }
    if (section==3) {
        if (_cmnModel.dayDataAry.count>0) {
            CmnDayInfoModel * model = _cmnModel.dayDataAry[0];
            return model.dataAry.count ;
        }
    }

    return 0 ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    

    if (indexPath.section==0) {
        CmMonthCltCell * mothCltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"month_clt_cell1" forIndexPath:indexPath];
        mothCltCell.tag = indexPath.item ;
        if (_cmnModel.monthDataAry.count>indexPath.item) {
            mothCltCell.monthModel = _cmnModel.monthDataAry[indexPath.item];
        }
        return mothCltCell ;
    }
    
    if (indexPath.section==1) {
        CmImgTxtCell * imgtxtCltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"img_txt_cltCell1" forIndexPath:indexPath];
        if (indexPath.item==0) {
            imgtxtCltCell.headimgV.image = [[UIImage imageNamed:@"new_my_order"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
            imgtxtCltCell.titleLab.text = @"订单明细" ;
            
        }
        if (indexPath.item==1) {
            imgtxtCltCell.headimgV.image = [[UIImage imageNamed:@"new_my_bill"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
            imgtxtCltCell.titleLab.text = @"我的账单" ;
        }
        return imgtxtCltCell ;
    }
    
    if (indexPath.section==2) {
        CmDaySltCell * sltCltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"day_slt_cltCell1" forIndexPath:indexPath];
        sltCltCell.tag = indexPath.item ;
        sltCltCell.selectBtn.tag = indexPath.item ;
        if (_cmnModel.dayDataAry.count>indexPath.item) {
            sltCltCell.daySltModel = _cmnModel.dayDataAry[indexPath.item];
        }
        [sltCltCell.selectBtn addTarget:self action:@selector(clickChangeSelect1:) forControlEvents:(UIControlEventTouchUpInside)];
        return sltCltCell ;
    }
    
    if (indexPath.section==3) {
        CmDayCltCell * dayCltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"day_clt_cell1" forIndexPath:indexPath];
        if (_cmnModel.dayDataAry.count>0) {
            for (CmnDayInfoModel * typeModel in _cmnModel.dayDataAry) {
                if (typeModel.isslt == 1) {
                    if (typeModel.dataAry.count>indexPath.item) {
                       dayCltCell.dayModel = typeModel.dataAry[indexPath.item];
                    }
                }
            }
        }
        return dayCltCell ;
    }

    return nil ;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        if (indexPath.item==0) {
            NSLog(@"标题: %@ 链接:%@",_cmnModel.ptitle,_cmnModel.reqUrl);
            if ([_cmnModel.ptitle isEqualToString:@"综合平台"]) {
                DMOrdereListVC * dmOrderVC = [[DMOrdereListVC alloc]init];
                [TopNavc pushViewController:dmOrderVC animated:YES];
            }else{
                NewConsumer * newCon = [[NewConsumer alloc]init];
                if ([_cmnModel.ptitle isEqualToString:@"淘宝"]) {
                    newCon.commissionType = 1 ;
                }
                if ([_cmnModel.ptitle isEqualToString:@"京东"]) {
                    newCon.commissionType = 2 ;
                }
                if ([_cmnModel.ptitle isEqualToString:@"拼多多"]) {
                    newCon.commissionType = 3 ;
                }
                if ([_cmnModel.ptitle isEqualToString:@"唯品会"]) {
                    newCon.commissionType = 4 ;
                }
                if ([_cmnModel.ptitle isEqualToString:@"美团"]) {
                    newCon.commissionType = 6 ;
                }
                
                [TopNavc pushViewController:newCon animated:YES];
            }
        }
        if (indexPath.item==1) {
            BillVC * bill = [BillVC new];
            if ([_cmnModel.ptitle isEqualToString:@"综合平台"]) {
                bill.billType = 5;
            }else if([_cmnModel.ptitle isEqualToString:@"美团"]){
                bill.billType = 60;
            } else{ bill.billType = 2; }
           [TopNavc pushViewController:bill animated:YES];
        }
        
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) { return CGSizeMake(ScreenW/2,80*HWB); }
    if (indexPath.section==1) { return CGSizeMake(ScreenW, 40*HWB); }
    if (indexPath.section==2) {
        return CGSizeMake(ScreenW/_cmnModel.dayDataAry.count-0.5, 36*HWB);
    }
    return CGSizeMake(ScreenW/3, 45*HWB);
}


#pragma mark ===>>> 设置区头和区尾(一个是提示文字，放在第二分区区头，  第二个是结算预估，放在三分区区头)

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqual:UICollectionElementKindSectionFooter]) {
        CmTxtReusableView * footView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"des_foot_view" forIndexPath:indexPath];
        if (indexPath.section==0) {
            footView.rulesBtn.hidden = YES ;
            footView.txtLab.hidden = NO ;
            footView.backgroundColor = RGBA(235, 235, 235, 1) ;
            footView.txtLab.text = _cmnModel.desc;

        }else{
            footView.backgroundColor = COLORWHITE ;
            if (indexPath.section==3) {
                footView.rulesBtn.hidden = NO ;
                footView.txtLab.hidden = YES ;
                ADDTARGETBUTTON(footView.rulesBtn, clickAleart)
            }else{
                footView.rulesBtn.hidden = YES ;
                footView.txtLab.hidden = YES ;
            }
        }
        footView.rulesBtn.hidden = YES ;
        
        return footView;
    }
    
    if ([kind isEqual:UICollectionElementKindSectionHeader]) {
        CmTxtReusableView * headView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"des_head_view" forIndexPath:indexPath];
        headView.txtLab.hidden = YES ;
        headView.rulesBtn.hidden = YES ;
        if (indexPath.section==1) {
            headView.backgroundColor = COLORGROUP ;
        }else{
            headView.backgroundColor = COLORWHITE ;
        }
        return headView ;
    }
    
    return nil ;
}

// 区头
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section==1) { return CGSizeMake(ScreenW, 12*HWB); }
    return CGSizeZero ;
}

//区尾
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section==0) { return  CGSizeMake(ScreenW, 25*HWB); }
    if (section==3) { return CGSizeMake(ScreenW, 50); }
    return CGSizeZero;
}


#pragma mark ===>>> 京东淘宝切换今日和昨日选项

-(void)clickChangeSelect1:(UIButton*)btn{
    for ( CmnDayInfoModel * typeModelAll in _cmnModel.dayDataAry) {
        typeModelAll.isslt = 0 ;
    }
    CmnDayInfoModel * typeModel1 = _cmnModel.dayDataAry[btn.tag];
    typeModel1.isslt = 1 ;
    [_moneyCltView reloadData];
}

-(void)clickAleart{
#if HuiXinGou
    
    [UIAlertController showIn:TopNavc.topViewController title:@"规则说明" content:@" 1、只有在惠心购APP里面领券，或者由惠心购APP分享的淘口令领券，并在淘宝天猫下单的产品，才会有佣金。如果通过其他方式购买的订单，不能给与推广佣金。\n\n2、使用淘宝红包，或者集分宝，天猫购物券的订单，不能给与推广佣金。\n\n3、订单15分钟更新一次，如果在双11这种大活动时期，因网络堵塞，更新时间相应会延长，请耐心等待。\n\n4、惠心购APP每天同步淘宝天猫的优惠券推广产品，想买什么都可以直接到惠心购里面来搜搜看，相关的佣金由店铺给出，因产品众多，偶尔有店铺调整推广计划造成佣金变化而APP显示不对的，请及时在对应产品进行反馈，我们代表其他达人对您的热心表示衷心的感谢。" ctAlignment:NSTextAlignmentLeft btnAry:@[@"知道啦"] indexAction:^(NSInteger indexTag) {
    }];
#else
    [UIAlertController showIn:TopNavc.topViewController title:@"规则说明\n" content:@" 1、下单支付后会在预估收入里查看(会有不定期的延时)。\n\n2、订单在确定收货(结算)后才会呈现在结算预估收入里。 \n\n3、当申请售后(维权)成功后会从预估收入及结算预估收入中剔除。\n\n4、取消订单、退款退货、申请售后维权都会产生预估收入和结算收入的数据变动。" ctAlignment:NSTextAlignmentLeft btnAry:@[@"知道啦"] indexAction:^(NSInteger indexTag) {
    }];
#endif
}

@end
