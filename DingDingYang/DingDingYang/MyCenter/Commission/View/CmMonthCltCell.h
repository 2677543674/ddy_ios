//
//  CmMonthCltCell.h
//  DingDingYang
//
//  Created by ddy on 2018/1/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CmnInfoModel.h"

#pragma mark ===>>> 按月统计要显示的cell

@interface CmMonthCltCell : UICollectionViewCell

@property(nonatomic,strong) UILabel * titleLab ;   // 标题
@property(nonatomic,strong) UILabel * contentLab ; // 数据
@property(nonatomic,strong) UILabel * statesLab ;  // 状态(文字颜色后台返回)

// 是否显示根据cell的位置来判断 (cell的宽为屏幕一半)
@property(nonatomic,strong) UIImageView * linesImgT ; // 顶部分割线
@property(nonatomic,strong) UIImageView * linesImgR ; // 右侧分割线
@property(nonatomic,strong) UIButton * showBtn;
@property(nonatomic,strong) CmnInfoModel * monthModel ;

@end


#pragma mark ===>>> 按天统计要显示的cell

@interface CmDayCltCell : UICollectionViewCell
@property(nonatomic,strong) UILabel * titleLab ;   // 标题
@property(nonatomic,strong) UILabel * contentLab ; // 数据
@property(nonatomic,strong) CmnInfoModel * dayModel ;
@end


// 今日昨日筛选按钮cell
@interface CmDaySltCell : UICollectionViewCell
@property(nonatomic,strong) UIButton * selectBtn ;    // 按钮
@property(nonatomic,strong) UIImageView * linesImgL ; // 按钮左边显示分割线
@property(nonatomic,strong) CmnDayInfoModel * daySltModel ; // 有选择按钮的model
@end

// 图片和文字的cell
@interface CmImgTxtCell : UICollectionViewCell
@property(nonatomic,strong) UIImageView * headimgV ;
@property(nonatomic,strong) UILabel     * titleLab ;
@property(nonatomic,strong) UIButton    * rightBtn ;
@end

// 纯文字的区头或区尾
@interface CmTxtReusableView : UICollectionReusableView
@property(nonatomic,strong) UILabel * txtLab ;
@property(nonatomic,strong) UIButton * rulesBtn ;
@end




