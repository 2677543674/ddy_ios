//
//  CmMonthCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/1/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "CmMonthCltCell.h"


#pragma mark ===>>> 按月份统计的内容

@implementation CmMonthCltCell

// 设置数据
-(void)setMonthModel:(CmnInfoModel *)monthModel{
    
    _monthModel = monthModel ;
    _titleLab.text = _monthModel.title ;
    
    if (_monthModel.ifAboutMoney==1) {
        _contentLab.text = FORMATSTR(@"￥%@",_monthModel.value);
    }else{
        _contentLab.text = _monthModel.value ;
    }
    
    _statesLab.text = _monthModel.state ;
    
    if (_monthModel.color.length>0&&[_monthModel.color hasPrefix:@"#"]) {
        _statesLab.textColor = [UIColor getColor:_monthModel.color];
    }else{
        _statesLab.textColor = LOGOCOLOR ;
    }
    
    
    if (self.tag==0||self.tag==1) {
        _linesImgT.hidden = YES ;
    }else{
        _linesImgT.hidden = NO ;
    }
    
    if (self.tag%2==0) {
        _linesImgR.hidden = NO ;
    }else{
        _linesImgR.hidden = YES ;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self titleLab];
        [self contentLab];
        [self statesLab];
        [self linesImgT];
        [self linesImgR];
#if BaoLaZhenXuan
        [self showBtn];
#endif
    }
    return self ;
}

-(UILabel*)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel labText:@"标题" color:Black102 font:12*HWB];
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB);  make.right.offset(-10*HWB);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(-15*HWB);
        }];
    }
    return _titleLab ;
}

-(UILabel*)contentLab{
    if (!_contentLab) {
        _contentLab = [UILabel labText:@"￥00.00" color:Black51 font:18*HWB];
        [self.contentView addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.right.offset(-10*HWB);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.offset(26*HWB) ;
        }];
    }
    return _contentLab ;
}

-(UILabel*)statesLab{
    if (!_statesLab) {
        _statesLab = [UILabel labText:@"结算状态" color:LOGOCOLOR font:12*HWB];
        [self.contentView addSubview:_statesLab];
        [_statesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB);
            make.top.equalTo(self.contentView.mas_centerY).offset(15*HWB);
        }];
    }
    return _statesLab ;
}

// 分区0和1隐藏不显示 (默认隐藏)
-(UIImageView*)linesImgT{
    if (!_linesImgT) {
        _linesImgT = [[UIImageView alloc]init];
        _linesImgT.backgroundColor = Black204 ;
        [self.contentView addSubview:_linesImgT];
        [_linesImgT mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.right.offset(-15*HWB);
            make.top.offset(0);  make.height.offset(0.5);
        }];
    }
    return _linesImgT ;
}

// indexPath.row对2取余，有余数隐藏，无余数显示
-(UIImageView*)linesImgR{
    if (!_linesImgR) {
        _linesImgR = [[UIImageView alloc]init];
        _linesImgR.backgroundColor = Black204 ;
        [self.contentView addSubview:_linesImgR];
        [_linesImgR mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);    make.width.offset(0.5);
            make.top.offset(10*HWB); make.bottom.offset(-10*HWB);
        }];
    }
    return _linesImgR ;
}

-(UIButton *)showBtn{
    if (_showBtn==nil) {
        _showBtn=[UIButton new];
        [_showBtn setImage:[UIImage imageNamed:@"pop_what"] forState:(UIControlStateNormal)];
        _showBtn.tag=self.tag;
        ADDTARGETBUTTON(_showBtn, clickShow:);
        [self.contentView addSubview:_showBtn];
        [_showBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB);
            make.centerY.equalTo(_statesLab.mas_centerY);
        }];
    }
    return _showBtn;
}

-(void)clickShow:(UIButton*)btn{
    NSString* content=@"";
    if (btn.tag==0) {
        content=@"在本月内确认收货的订单，包括上月的订单在本月确认的";
    }else if (btn.tag==1){
        content=@"在上两个自然月确认收货的订单，本月23号可以提现的佣金金额";
    }else if (btn.tag==2){
        content=@"在本月下的全部订单，包含已经结算的订单佣金";
    }else{
        content=@"上个月下的订单，没有确认收货的订单，包含已经结算的订单佣金";
    }
    
    [UIAlertController showIn:TopNavc.topViewController title:@"温馨提示" content:content ctAlignment:NSTextAlignmentCenter btnAry:@[@"确定"] indexAction:^(NSInteger indexTag) {
    }];
    
}

@end



#pragma mark ===>>> 按天份统计的内容

@implementation CmDayCltCell

-(void)setDayModel:(CmnInfoModel *)dayModel{
    _dayModel = dayModel ;
    _titleLab.text = _dayModel.title ;
    _contentLab.text = _dayModel.value ;
    if (_dayModel.ifAboutMoney==1) {
        _contentLab.text = FORMATSTR(@"￥%@",_dayModel.value);
    }else{
        _contentLab.text = _dayModel.value ;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self titleLab];
        [self contentLab];
    }
    return self ;
}

-(UILabel*)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel labText:@"标题" color:Black102 font:12*HWB];
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB);  make.right.offset(-10*HWB);
//            make.bottom.equalTo(self.contentView.mas_centerY).offset(0);
            make.top.offset(0);
        }];
    }
    return _titleLab ;
}

-(UILabel*)contentLab{
    if (!_contentLab) {
        _contentLab = [UILabel labText:@"￥00.00" color:Black51 font:15*HWB];
        [self.contentView addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.right.offset(-10*HWB);
            make.top.equalTo(self.contentView.mas_centerY).offset(-2*HWB);
        }];
    }
    return _contentLab ;
}
@end


#pragma mark ===>>> 今日昨日筛选按钮

@implementation CmDaySltCell

-(void)setDaySltModel:(CmnDayInfoModel *)daySltModel{
    _daySltModel = daySltModel ;
    [_selectBtn setTitle:_daySltModel.stitle forState:(UIControlStateNormal)];
    if (_daySltModel.isslt==1) {
        [_selectBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        _selectBtn.titleLabel.font = [UIFont systemFontOfSize:15*HWB] ;
//        _selectBtn.backgroundColor = COLORGROUP ;
    }else{
        [_selectBtn setTitleColor:Black102 forState:(UIControlStateNormal)];
        _selectBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB] ;
//        _selectBtn.backgroundColor = COLORWHITE ;
    }
    
    if (self.tag==0) {
        _linesImgL.hidden = YES ;
    }else{
        _linesImgL.hidden = NO ;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _selectBtn = [UIButton titLe:@"今日" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:13*HWB];
        [self.contentView addSubview:_selectBtn];
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        _linesImgL = [[UIImageView alloc]init];
        _linesImgL.hidden = YES ;
        _linesImgL.backgroundColor = Black204 ;
        [self.contentView addSubview:_linesImgL];
        [_linesImgL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);     make.width.offset(0.5);
            make.top.offset(10*HWB); make.bottom.offset(-10*HWB);
        }];
    }
    return self ;
}

@end



#pragma mark ===>>> 有图片、文字、指示箭头的cell

@implementation CmImgTxtCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _headimgV=[[UIImageView alloc]init];
        _headimgV.contentMode = UIViewContentModeScaleAspectFit;
        _headimgV.image = [UIImage imageNamed:@"new_consumer_ddmx"];
        [self.contentView addSubview:_headimgV];
        [_headimgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB);  make.height.offset(18*HWB);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.width.offset(15*HWB);
        }];
        
        
        _titleLab = [UILabel labText:@"订单明细" color:Black102 font:14*HWB];
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(52); make.height.offset(20);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
        
        
        _rightBtn = [[UIButton alloc]init];
        if (THEMECOLOR==1) {
            [_rightBtn setImage:[[UIImage imageNamed:@"new_consumer_right"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)] forState:(UIControlStateNormal)];
        }else{
            [_rightBtn setImage:[UIImage imageNamed:@"new_consumer_right"] forState:(UIControlStateNormal)];
        }
        [self.contentView addSubview:_rightBtn];
        [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-16); make.centerY.equalTo(self.mas_centerY);
            make.height.offset(16); make.width.offset(8);
        }];
        
        
        UIImageView * lines1 = [[UIImageView alloc]init];
        lines1.backgroundColor = Black204 ;
        [self.contentView addSubview:lines1];
        [lines1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.height.offset(0.5);
            make.left.offset(0);  make.right.offset(0);
        }];
    }
    return self ;
}
@end



#pragma mark ===>>> 纯文字的区头

@implementation CmTxtReusableView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _txtLab = [UILabel labText:@"" color:Black102 font:11*HWB];
        _txtLab.numberOfLines = 0 ;
        [self addSubview:_txtLab];
        [_txtLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.left.offset(10*HWB); make.right.offset(-10*HWB);
        }];
        
        _rulesBtn = [UIButton titLe:@"规则说明 ›" bgColor:COLORWHITE titColorN:[UIColor redColor] titColorH:[UIColor redColor] font:13*HWB];
        if (CANOPENWechat==NO) {
            _rulesBtn.hidden=YES;
        }
//        ADDTARGETBUTTON(_rulesBtn, clickAleart)
        [self addSubview:_rulesBtn];
        [_rulesBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.centerX.equalTo(self.mas_centerX);
            make.width.offset(80*HWB); make.height.offset(36);
        }];
        
    }
    return self;
}

@end





