//
//  CommissionView.h
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CmnInfoModel.h"

@interface CommissionView : UIView <UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UIView * topMoneyView; // 显示余额和提现
@property(nonatomic,strong) UILabel * cmoneyLab;   // 显示余额
@property(nonatomic,strong) UIButton * makeDrawalBtn; // 按钮提现

@property(nonatomic,strong) UICollectionView * moneyCltView; // 佣金详情

@property(nonatomic,strong) NSMutableArray *dayDataAry, *monthDataAry;

@property(nonatomic,strong) CommissionModel * cmnModel; 

@end
