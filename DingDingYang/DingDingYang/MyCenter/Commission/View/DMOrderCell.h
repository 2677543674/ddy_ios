//
//  DMOrderCell.h
//  DingDingYang
//
//  Created by ddy on 2018/9/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMOrderModel.h"



@interface DMOrderCell : UITableViewCell

@property(nonatomic,strong) UIImageView * backImgV; // 中间的背景
@property(nonatomic,strong) UIImageView * goodsImgV;
@property(nonatomic,strong) UILabel * goodsTitLab;
@property(nonatomic,strong) UILabel * commissionLab;
@property(nonatomic,strong) UILabel * statesLab;

@property(nonatomic,strong) DMOrderDModel * dModel;
@property(nonatomic,strong) DMOrderDModel * dModel2; // 返利详情显示

@property(nonatomic,strong) DMOrderModel * model;

@end


// 区头
@interface DMOrderCellHead : UITableViewHeaderFooterView
@property(nonatomic,strong) UIImageView * platformImgV;
@property(nonatomic,strong) UILabel * platformLab;
@property(nonatomic,strong) UILabel * orderNoLab;
@property(nonatomic,strong) DMOrderModel * model;
@end


@interface DMOrderCellFoot : UITableViewHeaderFooterView
@property(nonatomic,strong) UILabel * creatTimeLab;      // 订单创建时间
@property(nonatomic,strong) UILabel * settlementTimeLab; // 结算时间
@property(nonatomic,strong) DMOrderModel * model;
@end




