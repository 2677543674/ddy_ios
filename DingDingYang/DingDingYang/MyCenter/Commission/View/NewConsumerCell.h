//
//  NewConsumerCell.h

//
//  Created by ddy on 2017/12/26.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CsitModel.h"
@interface NewConsumerCell : UITableViewCell

@property(nonatomic,strong) UIImageView * goodsImageV;
@property(nonatomic,strong) UIView  * topBgView;
@property(nonatomic,strong) UILabel * nameLab;
@property(nonatomic,strong) UILabel * priceLab;
@property(nonatomic,strong) UILabel * numberLab;
@property(nonatomic,strong) UILabel * rewardLab;
@property(nonatomic,strong) UILabel * timeLab;
@property(nonatomic,strong) UILabel * statusLab;
@property(nonatomic,strong) CsitModel * orderModel ;//订单
@property(nonatomic,copy)   NSString* type;
@end


@interface ConsumerHeader : UIView
@property(nonatomic,strong) UITextField * searchText;
@property(nonatomic,strong) UIButton *    searchBtn;
@end
