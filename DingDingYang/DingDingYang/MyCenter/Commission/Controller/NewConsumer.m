//
//  NewConsumer.m

//
//  Created by ddy on 2017/12/26.
//  Copyright © 2017年 ddy.All rights reserved.
//  订单列表


#import "NewConsumer.h"
#import "CsitModel.h"
#import "NewConsumerCell.h"
#import "NewShareAppVC.h"
#import "CommissionVC.h"

@interface NewConsumer ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UITextFieldDelegate>

@property(nonatomic,strong) MyCenterNoDataView * nodataView ;
@property(nonatomic,strong) ConsumerHeader * headerView;
@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,strong) UIView      * levelView;
@property(nonatomic,strong) UIButton    * levelBtn;
//@property(nonatomic,strong) UIButton    * nodataBtn;
@property(nonatomic,strong) UIImageView * lineImg;

@property(nonatomic,strong) NSMutableDictionary * pdic; // 参数
@property(nonatomic,strong) NSMutableArray * dataAry;   // 所有数据
@property(nonatomic,assign) NSInteger  pageIndex; // 页码
@property(nonatomic,strong) NSArray  * titleAry;
@property(nonatomic,strong) NSString * path;
@end

@implementation NewConsumer

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    
    self.title = @"订单明细";

    _dataAry = [NSMutableArray array];
    
    switch (_commissionType) {
        case 1:{ _path = url_get_consumer_order; } break;
        case 2:{ _path = url_goods_order_jd; } break;
        case 3:{ _path = url_get_order_pdd; } break;
        case 4:{ _path = url_goods_order_wph; } break;
        case 5:{ _path = url_goods_order_dm; } break;
        case 6:{ _path = url_goods_order_mt; } break;
        case 55:{ _path = url_get_consumer_order; } break;
        default: { _path = url_get_consumer_order; } break;
    }
    

    [self pdic];   [self headerView];

    // 全部商品都AB级 (除了多麦的, 叮当 运营商也不要B级)
//    if (_commissionType!=5) {
    _titleAry = @[@"自购",@"粉丝"];
    if ((role_state==2||role_state==3)) {
        _titleAry = @[@"自购",@"粉丝",@"社群"];
    }
    [self levelView]; [self lineImg];

    [self tableView];   [self nodataView];

}

-(ConsumerHeader *)headerView{
    if (!_headerView) {
        _headerView = [[ConsumerHeader alloc]init];
        _headerView.searchText.delegate = self ;
        if (_commissionType==3) {
            _headerView.searchText.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        }
        ADDTARGETBUTTON(_headerView.searchBtn, clickSearch)
        [self.view addSubview:_headerView];
        [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
//            make.top.offset((role_state==2||role_state==3)?41:0);
            make.top.offset(41);
            make.height.offset(33*HWB);
        }];
    }
    return _headerView;
}
// 点击搜索
-(void)clickSearch{
    ResignFirstResponder
    if (_headerView.searchText.text.length>0) {
        if (_commissionType==6) {
           _pdic[@"order_id"] = _headerView.searchText.text;
        }else{
           _pdic[@"orderNo"] = _headerView.searchText.text;
        }
    }else{
        if (_commissionType==6) {
             [_pdic removeObjectForKey:@"order_id"];
        }else{
             [_pdic removeObjectForKey:@"orderNo"];
        }
    }
    [_tableView.mj_header beginRefreshing];
}

-(NSMutableDictionary*)pdic{
    if (!_pdic) {
        _pdic = [NSMutableDictionary dictionary];
        _pdic[@"level"] = @"1";
        if (_commissionType==55) {
            _pdic[@"level"] = @"4";
        }
        
        if (_commissionType==6) {
            _pdic[@"page"] = @"1";
        }else{
            _pdic[@"p"] = @"1";
        }
        
        _pageIndex = 1 ;
    }
    return _pdic ;
}


//创建级别选择按钮
-(UIView*)levelView{
    if (!_levelView) {
        _levelView = [[UIView alloc]init];
        _levelView.frame = CGRectMake(0,0, ScreenW, 40);
        [self.view addSubview:_levelView];
        for (NSInteger i = 0 ; i<_titleAry.count ; i++) {
            _levelBtn = [UIButton titLe:_titleAry[i] bgColor:COLORWHITE titColorN:Black51 titColorH:COLORGRAY font:14*HWB];
            _levelBtn.frame=CGRectMake(ScreenW/_titleAry.count*i, 0 , ScreenW/_titleAry.count, 40);
            ADDTARGETBUTTON(_levelBtn, clickSelect:)
            _levelBtn.tag = 10 + i ;
            _levelBtn.selected = NO ;
            [_levelView addSubview:_levelBtn];
            if (i==0) {
                _levelBtn.selected=YES;
                [_levelBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];

                if (THEMECOLOR==2) { // 主题色为浅色
                    [_levelBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
                    _levelBtn.backgroundColor = LOGOCOLOR ;
                }
            }
        }
    }
    return _levelView ;
}

//下划线
-(UIImageView*)lineImg{
    if (!_lineImg) {
        _lineImg = [[UIImageView alloc]init];
        _lineImg.backgroundColor = LOGOCOLOR ;
        _levelBtn = (UIButton*)[_levelView viewWithTag:10];
        _lineImg.center = CGPointMake(_levelBtn.center.x, 38);
        _lineImg.bounds = CGRectMake(0, 0, 40*HWB, 1.5);
        [self.view addSubview:_lineImg];
        if (THEMECOLOR==2) {  _lineImg.hidden  = YES ;  }
    }

    return _lineImg ;
}
#pragma mark ===>>> 按钮点击事件(A、B级)
-(void)clickSelect:(UIButton*)sbtn{
    [self.view endEditing:YES];
    if (sbtn.selected==YES) { return ; } //不允许二次点击
    [self changePDic:sbtn.tag-10] ;
}

-(void)changePDic:(NSInteger)level{
   
    [_dataAry removeAllObjects];
    [_tableView reloadData];
    
    for (_levelBtn in _levelView.subviews) {
        _levelBtn.backgroundColor = COLORWHITE ;
        [_levelBtn setTitleColor:Black51 forState:UIControlStateNormal];
        _levelBtn.selected = NO ;
    }
    
    _levelBtn = (UIButton*)[_levelView viewWithTag:level+10];
    _levelBtn.selected = YES ;

    float lineCenterX  = _levelBtn.center.x ;
   
    [UIView animateWithDuration:0.16 animations:^{
        [_levelBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        if (THEMECOLOR==2) {
            _levelBtn.backgroundColor = LOGOCOLOR ;
            [_levelBtn setTitleColor:Black51 forState:UIControlStateNormal];
        }
        _lineImg.center = CGPointMake(lineCenterX, 38);
    }];

    if ( level==0 ) { _pdic[@"level"] = (_commissionType==55?@"4":@"1"); }
    if ( level==1 ) { _pdic[@"level"] = (_commissionType==55?@"5":@"2"); }
    if ( level==2 ) { _pdic[@"level"] = @"3" ; }
//    if ( level==0 ) { _pdic[@"level"] = @"1"; }
//    if ( level==1 ) { _pdic[@"level"] = @"3"; }
    
    _pageIndex = 1 ;
    
    [_tableView.mj_header beginRefreshing];

}

#pragma mark ===>>> 获取数据

-(void)queryCommissionData{
    _levelView.userInteractionEnabled = NO;
    [NetRequest requestType:0 url:_path ptdic:_pdic success:^(id nwdic) {
        _levelView.userInteractionEnabled = YES;

        NSArray * list = NULLARY(nwdic[@"list"]);
        [list enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL *  stop) {
            CsitModel*model=[[CsitModel alloc]initWithDic:obj];
            [_dataAry addObject:model];
        }];
        [_tableView endRefreshType:list.count isReload:YES];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        _levelView.userInteractionEnabled = YES;
        SHOWMSG(failure) [_tableView endRefreshType:1 isReload:YES];
    }];

}

#pragma mark ===>>> tableView表格
-(UITableView*)tableView{
    if (!_tableView) {

        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;  _tableView.dataSource = self;
        _tableView.rowHeight = 100 * HWB;
        _tableView.backgroundColor = COLORGROUP ;
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0); make.bottom.offset(0);
            make.top.equalTo(_headerView.mas_bottom).offset(0);
        }];
        
        [_tableView registerClass:[NewConsumerCell class] forCellReuseIdentifier:@"NewConsumerCell"];
        
        //下拉刷新
        __weak NewConsumer * selfView = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [selfView.dataAry removeAllObjects];
            [selfView.tableView.mj_footer resetNoMoreData];
            selfView.pageIndex = 1 ;
            selfView.pdic[@"p"] = FORMATSTR(@"%ld",(long)selfView.pageIndex);
            [selfView queryCommissionData];
        }];
        
        _tableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pageIndex++;
            selfView.pdic[@"p"] = FORMATSTR(@"%ld",(long)selfView.pageIndex);
            [selfView queryCommissionData];
        }];
        
        [_tableView.mj_header beginRefreshing];
    }
    
    return _tableView;
}

#pragma mark ===>>> tableView代理

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataAry.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewConsumerCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewConsumerCell" forIndexPath:indexPath];
    if (_commissionType==3) { cell.type=@"pdd"; } // 标志为拼多多商品
    if (_commissionType==4) { cell.type=@"wph"; } // 标志为唯品会商品
    if (_commissionType==6) { cell.type=@"mt"; }  // 标志为美团商品
    if (_dataAry.count>indexPath.row) {
        cell.orderModel = _dataAry[indexPath.row];
    }else{ [_tableView reloadData]; }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CsitModel * model = _dataAry[indexPath.row];
    if (_commissionType==1) { // 淘宝
        NSURL*url = [NSURL URLWithString:FORMATSTR(@"taobao://detail.tmall.com/item.htm?id=%@",model.goodsId)];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }else{
            WebVC*webv = [WebVC new];
            webv.hidesBottomBarWhenPushed = YES;
            webv.urlWeb = FORMATSTR(@"https://detail.tmall.com/item.htm?id=%@",model.goodsId);
            webv.nameTitle = @"商品详情页";
            PushTo(webv, YES);
        }
    }else if(_commissionType==2){
        [[KeplerApiManager sharedKPService] openKeplerPageWithURL:[NSString stringWithFormat:@"http://item.jd.com/%@.html",model.goodsId] sourceController:self jumpType:2 userInfo:nil];
    }else{
        
    }
}

#pragma mark ===>>> 没有数据时，提示去推荐好友

-(MyCenterNoDataView*)nodataView{
    if (!_nodataView) {
        
        _nodataView = [[MyCenterNoDataView alloc]init];
        _nodataView.imgNameStr = @"哭";
        _nodataView.tishiStr = @"你还没有订单";
        _nodataView.btnStr = @"立即去推荐好友吧" ;
        ADDTARGETBUTTON(_nodataView.tishiBtn, nodataClickGotoShare)
        [_tableView addSubview:_nodataView];
        [_nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tableView.mas_centerX);
            make.centerY.equalTo(_tableView.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        
        _nodataView.hidden = YES ;
        
    }
    return _nodataView ;
}

-(void)nodataClickGotoShare{
  [MyPageJump pushNextVcByModel:nil byType:51];
}


// 键盘输入监控
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length==0) { return YES ; }
    if (_commissionType==3) { return YES; }
    return JUDGESTR(NUMBERPOINT,string);
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];

}

@end

