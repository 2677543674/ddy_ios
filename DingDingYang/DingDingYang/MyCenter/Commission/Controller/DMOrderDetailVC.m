//
//  DMOrderDetailVC.m
//  DingDingYang
//
//  Created by ddy on 2018/9/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DMOrderDetailVC.h"
#import "DMOrderCell.h"

@interface DMOrderDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView * tableView;

@property(nonatomic,strong) UIView * backView1;
@property(nonatomic,strong) UIView * backView2;

@property(nonatomic,strong) UILabel * fanliMoneyLab;

@property(nonatomic,strong) UIView * goodsView;
@property(nonatomic,strong) UIImageView * goodsImgv;
@property(nonatomic,strong) UILabel * goodsTitLab;

@property(nonatomic,strong) UILabel * goodsInfoLab;

@property(nonatomic,strong) UILabel * insLab; // 提示信息




@end

@implementation DMOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = COLORGROUP;
    
    self.title = @"订单信息";

//    UIImageView * topImgV = [[UIImageView alloc]init];
//    topImgV.image = [UIImage imageNamed:@"ddy_dmfl_d_top"];
//    topImgV.frame = CGRectMake(0, 0, ScreenW, ScreenW/4.7);
//    [self.view addSubview:topImgV];
//
//    UILabel * lab = [UILabel labText:@"订单明细" color:COLORWHITE font:15*HWB];
//    [topImgV addSubview:lab];
//    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.offset(12*HWB);
//        make.centerY.equalTo(topImgV.mas_centerY);
//    }];
    
    
    // 返利金额
    _backView1 = [[UIView alloc]init];
    _backView1.backgroundColor = COLORWHITE;
    _backView1.frame = CGRectMake(0, 0, ScreenW, 80*HWB);
    
    UILabel * fanliLab = [UILabel labText:@"返利金额" color:Black51 font:13*HWB];
    [_backView1 addSubview:fanliLab];
    [fanliLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(12*HWB);
        make.bottom.equalTo(_backView1.mas_centerY).offset(-2);
    }];
    
    _fanliMoneyLab = [UILabel labText:@"约0元" color:LOGOCOLOR font:14*HWB];
    [_backView1 addSubview:_fanliMoneyLab];
    [_fanliMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(12*HWB);
        make.top.equalTo(_backView1.mas_centerY).offset(2);;
    }];
    
    
   
//    [self.view addSubview:_backView2];
//    [_backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.offset(0); make.height.offset(280*HWB);
//        make.top.equalTo(_backView1.mas_bottom).offset(8*HWB);
//    }];
    
//    UILabel * orderMsgLab = [UILabel labText:@"订单信息" color:Black102 font:13*HWB];
//    [_backView2 addSubview:orderMsgLab];
//    [orderMsgLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.offset(0);
//        make.left.offset(12*HWB);
//        make.height.offset(35*HWB);
//    }];
    
    // 商品图片和名称
//    _goodsView = [[UIView alloc]init];
//    _goodsView.backgroundColor = COLORGROUP;
//    [_backView2 addSubview:_goodsView];
//    [_goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.offset(0);
//        make.top.offset(35*HWB);
//        make.height.offset(80*HWB);
//    }];
//
//    _goodsImgv = [[UIImageView alloc]init];
//    _goodsImgv.backgroundColor = COLORWHITE;
//    [_goodsView addSubview:_goodsImgv];
//    [_goodsImgv mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.offset(12*HWB);
//        make.width.height.offset(60*HWB);
//        make.centerY.equalTo(_goodsView.mas_centerY);
//    }];
//
//    _goodsTitLab = [UILabel labText:@"商品标题" color:Black102 font:12.5*HWB];
//    _goodsTitLab.numberOfLines = 0;
//    [_goodsView addSubview:_goodsTitLab];
//    [_goodsTitLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_goodsImgv.mas_right).offset(10*HWB);
//        make.right.offset(-10*HWB);
//        make.centerY.equalTo(_goodsView.mas_centerY);
//    }];
    
    // 订单信息
    _backView2 = [[UIView alloc]init];
    _backView2.backgroundColor = COLORWHITE;
    // 订单信息
    _goodsInfoLab = [UILabel labText:@"商品信息" color:Black51 font:12*HWB];
    _goodsInfoLab.numberOfLines = 0;
    [_backView2 addSubview:_goodsInfoLab];
    [_goodsInfoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(12*HWB); make.right.offset(-5*HWB);
        make.top.offset(10*HWB);
    }];
    
    [self setValue];
    [self tableView];
}

#pragma mark -- tableView表格
-(UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, ScreenW, ScreenH-NAVCBAR_HEIGHT) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = COLORGROUP;
        _tableView.delegate = self; _tableView.dataSource=self;
        [self.view addSubview:_tableView];
        
        [_tableView registerClass:[DMOrderCell class] forCellReuseIdentifier:@"cell"];
        
        /*
         两个分区
         分区0：
           item0：订单明细
           区尾：间隔
         
         分区1:
           区头: 标题订单信息
           item: 商品明细
           区尾: 订单其它信息
         
         */
        
    }
    return _tableView ;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) { return 1; }
    return _model.detailListAry.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) { return ScreenW/4.7; }
    return 35*HWB;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) { return 8*HWB; }
    return 160*HWB;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {

        UIImageView * topImgV = [[UIImageView alloc]init];
        topImgV.image = [UIImage imageNamed:@"ddy_dmfl_d_top"];
        topImgV.frame = CGRectMake(0, 0, ScreenW, ScreenW/4.7);
        [self.view addSubview:topImgV];
        
        UILabel * lab = [UILabel labText:@"订单明细" color:COLORWHITE font:15*HWB];
        [topImgV addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.centerY.equalTo(topImgV.mas_centerY);
        }];
        return topImgV;
    }
    UILabel * orderMsgLab = [UILabel labText:@"     订单信息" color:Black102 font:13*HWB];
    orderMsgLab.backgroundColor = COLORWHITE;
    return orderMsgLab;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==0) {
        UIView * foot1 = [[UIView alloc]init];
        foot1.backgroundColor = COLORGROUP;
        return foot1;
    }
    return _backView2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80*HWB;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        static NSString * cellID = @"cell0";
        UITableViewCell * cell0 = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell0==nil) {
            cell0 = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            [cell0.contentView addSubview:_backView1];
        }
        return cell0;
    }
    DMOrderCell * orderCell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    DMOrderDModel * dModel = _model.detailListAry[indexPath.row];
    orderCell.dModel2 = dModel;
    return orderCell;
}


-(void)setValue{
    
    _fanliMoneyLab.text = FORMATSTR(@"约 %@ 元",_model.commission);
//    _goodsImgv.image = [UIImage imageNamed:@"logo"];
//    _goodsTitLab.text = _dModel.goodsName;
    CGFloat sum = [[_model.detailListAry valueForKeyPath:@"@sum.ordersPrice.floatValue"] floatValue];

    NSString * infoStr = FORMATSTR(@"商城名称： %@\n订单金额： %.2f 元\n订单编号： %@\n下单时间： %@\n温馨提示： %@",
                                   _model.activityName,sum,_model.orderSn,_model.orderTime,_model.reminder);
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    NSDictionary * desAttbDic = @{NSFontAttributeName:[UIFont systemFontOfSize:12*HWB],
                                  NSForegroundColorAttributeName:Black51,
                                  NSParagraphStyleAttributeName:paragraphStyle};
    NSMutableAttributedString * attributedInfo = [[NSMutableAttributedString alloc]initWithString:infoStr attributes:desAttbDic];
    
    _goodsInfoLab.attributedText = attributedInfo;
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
