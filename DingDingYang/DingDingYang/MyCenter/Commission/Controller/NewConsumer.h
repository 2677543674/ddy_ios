//
//  NewConsumer.h

//
//  Created by ddy on 2017/12/26.
//  Copyright © 2017年 ddy.All rights reserved.
//  订单列表

#import <UIKit/UIKit.h>

@interface NewConsumer : DDYViewController

@property(nonatomic,strong) PersonalModel * modelp;

// 1:淘宝  2:京东 3:拼多多 4:唯品会 5:多麦(综合平台) 6:美团 55:淘宝运营商
@property(nonatomic,assign) NSInteger  commissionType;

@end
