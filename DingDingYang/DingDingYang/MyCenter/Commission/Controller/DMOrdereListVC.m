//
//  DMOrdereListVC.m
//  DingDingYang
//
//  Created by ddy on 2018/9/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DMOrdereListVC.h"
#import "DMOrderCell.h"
#import "DMOrderModel.h"
#import "DMOrderDetailVC.h"
#import "NewConsumerCell.h"

@interface DMOrdereListVC () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property(nonatomic,strong) UIView      * levelView;
@property(nonatomic,strong) UIButton    * levelBtn;
@property(nonatomic,strong) UIImageView * lineImg;
@property(nonatomic,strong) NSArray  * titleAry;

@property(nonatomic,strong) ConsumerHeader * headerView;

@property(nonatomic,strong) NSMutableDictionary * parameter;
@property(nonatomic,strong) NSMutableArray * dataAry;
@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,assign) NSInteger page;

@end

@implementation DMOrdereListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"订单列表";
    
    _dataAry = [NSMutableArray array];
    
    _titleAry = @[@"自购",@"粉丝"];
    if (role_state==2||role_state==3) {
        _titleAry = @[@"自购",@"粉丝",@"社群"];
        [self levelView]; [self lineImg];
    }
   
    [self headerView];
    
    [self parameter];
    
    [self tableView];

}
-(ConsumerHeader *)headerView{
    if (!_headerView) {
        _headerView = [[ConsumerHeader alloc]init];
        _headerView.searchText.delegate = self ;
        ADDTARGETBUTTON(_headerView.searchBtn, clickSearch)
        [self.view addSubview:_headerView];
        [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.top.offset((role_state==2||role_state==3)?41:0);
            make.height.offset(33*HWB);
        }];
    }
    return _headerView;
}
//创建级别选择按钮
-(UIView*)levelView{
    if (!_levelView) {
        _levelView = [[UIView alloc]init];
        _levelView.frame = CGRectMake(0,0, ScreenW, 40);
        [self.view addSubview:_levelView];
        for (NSInteger i = 0 ; i<_titleAry.count ; i++) {
            _levelBtn = [UIButton titLe:_titleAry[i] bgColor:COLORWHITE titColorN:Black51 titColorH:COLORGRAY font:14*HWB];
            _levelBtn.frame=CGRectMake(ScreenW/_titleAry.count*i, 0 , ScreenW/_titleAry.count, 40);
            ADDTARGETBUTTON(_levelBtn, clickSelect:)
            _levelBtn.tag = 10 + i ;
            _levelBtn.selected = NO ;
            [_levelView addSubview:_levelBtn];
            if (i==0) {
                _levelBtn.selected=YES;
                [_levelBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
                
                if (THEMECOLOR==2) { // 主题色为浅色
                    [_levelBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
                    _levelBtn.backgroundColor = LOGOCOLOR ;
                }
            }
        }
    }
    return _levelView ;
}

//下划线
-(UIImageView*)lineImg{
    if (!_lineImg) {
        _lineImg = [[UIImageView alloc]init];
        _lineImg.backgroundColor = LOGOCOLOR ;
        _levelBtn = (UIButton*)[_levelView viewWithTag:10];
        _lineImg.center = CGPointMake(_levelBtn.center.x, 38);
        _lineImg.bounds = CGRectMake(0, 0, 40*HWB, 1.5);
        [self.view addSubview:_lineImg];
        if (THEMECOLOR==2) {  _lineImg.hidden  = YES ;  }
    }
    
    return _lineImg ;
}
#pragma mark ===>>> 按钮点击事件(A、B级)
-(void)clickSelect:(UIButton*)sbtn{
    [self.view endEditing:YES];
    if (sbtn.selected==YES) { return ; } //不允许二次点击
    [self changePDic:sbtn.tag-10] ;
}

-(void)changePDic:(NSInteger)level{
    
    [_dataAry removeAllObjects];
    [_tableView reloadData];
    
    for (_levelBtn in _levelView.subviews) {
        _levelBtn.backgroundColor = COLORWHITE ;
        [_levelBtn setTitleColor:Black51 forState:UIControlStateNormal];
        _levelBtn.selected = NO ;
    }
    
    _levelBtn = (UIButton*)[_levelView viewWithTag:level+10];
    _levelBtn.selected = YES ;
    
    float lineCenterX  = _levelBtn.center.x ;
    
    [UIView animateWithDuration:0.16 animations:^{
        [_levelBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        if (THEMECOLOR==2) {
            _levelBtn.backgroundColor = LOGOCOLOR ;
            [_levelBtn setTitleColor:Black51 forState:UIControlStateNormal];
        }
        _lineImg.center = CGPointMake(lineCenterX, 38);
    }];
    
    if ( level==0 ) { _parameter[@"level"] = @"1" ; }
    if ( level==1 ) { _parameter[@"level"] = @"2" ; }
    if ( level==2 ) { _parameter[@"level"] = @"3" ; }
    
    _page = 1 ;
    
    [_tableView.mj_header beginRefreshing];
    
}

-(void)clickSearch{
    ResignFirstResponder
    if (_headerView.searchText.text.length>0) {
        _parameter[@"orderNo"] = _headerView.searchText.text;
    }else{
        [_parameter removeObjectForKey:@"orderNo"];
    }
    [_tableView.mj_header beginRefreshing];
}


-(NSMutableDictionary*)parameter{
    if (!_parameter) {
        _page = 1 ;
        _parameter = [NSMutableDictionary dictionary];
        _parameter[@"p"] = @"1" ;
    }
    return _parameter ;
}

-(void)queryOrder{
    NSString * url = url_goods_order_dm;
    [NetRequest requestType:0 url:url ptdic:_parameter success:^(id nwdic) {
        NSArray * listAry = NULLARY(nwdic[@"list"]);
        if (_page==1&&_dataAry.count>0) {
            [_dataAry removeAllObjects];
            [_tableView reloadData];
        }
        [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            DMOrderModel * model = [[DMOrderModel alloc]initWithDic:obj];
            [_dataAry addObject:model];
        }];
        [_tableView endRefreshType:listAry.count isReload:YES];
        if (_dataAry.count==0) { SHOWMSG(@"还没有相关订单哦~") }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        SHOWMSG(failure)
        [_tableView endRefreshType:1 isReload:YES];
    }];
}

#pragma mark -- tableView表格
-(UITableView*)tableView{
    if (!_tableView) {
        CGRect tabRect = CGRectMake(0,41, ScreenW, ScreenH-NAVCBAR_HEIGHT-41);
        if (role_state==2||role_state==3) {
            tabRect = CGRectMake(0,41+33*HWB, ScreenW, ScreenH-NAVCBAR_HEIGHT-41-33*HWB);
        }
        _tableView = [[UITableView alloc]initWithFrame:tabRect style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self; _tableView.dataSource=self;
        _tableView.backgroundColor = COLORGROUP;
        [self.view addSubview:_tableView];
        
        [_tableView registerClass:[DMOrderCell class] forCellReuseIdentifier:@"cell"];
        [_tableView registerClass:[DMOrderCellHead class] forHeaderFooterViewReuseIdentifier:@"head"];
        [_tableView registerClass:[DMOrderCellFoot class] forHeaderFooterViewReuseIdentifier:@"foot"];

        //下拉刷新
        __weak DMOrdereListVC * selfView = self;
        
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [selfView.tableView.mj_footer resetNoMoreData];
            selfView.page = 1 ;
            selfView.parameter[@"p"] = FORMATSTR(@"%ld",(long)selfView.page);
            [selfView queryOrder];
        }];
        
        _tableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.page++;
            selfView.parameter[@"p"] = FORMATSTR(@"%ld",(long)selfView.page);
            [selfView queryOrder];
        }];
        [_tableView.mj_header beginRefreshing];
    }
    return _tableView ;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataAry.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    DMOrderModel * model = _dataAry[section];
    if (model.detailListAry.count>0) { return 1; }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 38*HWB;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 30*HWB;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    DMOrderCellHead * head = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"head"];
    head.model = _dataAry[section];
    return head;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    DMOrderCellFoot * foot = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"foot"];
    foot.model = _dataAry[section];
    return foot;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80*HWB;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DMOrderCell * orderCell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    DMOrderModel * model = _dataAry[indexPath.section];
    DMOrderDModel * dModel = model.detailListAry[indexPath.row];
    orderCell.dModel = dModel;
    orderCell.model = model;
    return orderCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DMOrderDetailVC * orderDetailVC = [[DMOrderDetailVC alloc]init];
    DMOrderModel * model = _dataAry[indexPath.section];
//    DMOrderDModel * dModel = model.detailListAry[indexPath.row];
    orderDetailVC.model = model;
//    orderDetailVC.dModel = dModel;
    [TopNavc pushViewController:orderDetailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
