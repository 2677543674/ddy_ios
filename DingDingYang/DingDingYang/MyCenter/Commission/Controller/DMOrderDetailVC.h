//
//  DMOrderDetailVC.h
//  DingDingYang
//
//  Created by ddy on 2018/9/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DDYViewController.h"
#import "DMOrderModel.h"

@interface DMOrderDetailVC : DDYViewController

@property(nonatomic,strong) DMOrderModel * model;
@property(nonatomic,strong) DMOrderDModel * dModel;

@end
