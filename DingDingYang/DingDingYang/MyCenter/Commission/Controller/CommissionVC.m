//
//  CommissionVC.m
//  DingDingYang
//
//  Created by ddy on 2018/1/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "CommissionVC.h"
#import "CmMonthCltCell.h" // 按月统计，按天统计，和一些其他cltCell集合
#import "CmnInfoModel.h"   // model
#import "NewConsumer.h"    // 订单明细(新)
#import "BillVC.h"
#import "NewWithdrawalVC.h" // 提现(可修改支付宝)
#import "CommissionView.h"

@interface CommissionVC () <UIScrollViewDelegate>

@property(nonatomic,strong) UISegmentedControl * segmentCtrl; // 导航栏上的选择器
@property(nonatomic,strong) UIScrollView * scrollView; // 下方动态数据
@property(nonatomic,strong) CommissionView * moneyView;
@property(nonatomic,strong) NSMutableArray * platformAry; // 平台数组
@property(nonatomic,assign) NSInteger selectType; // 记录选择的索引

@end

@implementation CommissionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _modelTitle.length==0?@"消费佣金":_modelTitle;
    
    self.view.backgroundColor = COLORGROUP;
    
    _selectType = 0 ;

    _platformAry = [NSMutableArray array];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         [self isHaveShowOtherPlatform];
    });
   
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"scrollView的偏移量: %f",_scrollView.contentOffset.x);
    [self requestConsumer];
}

// 检测服务器是否需要显示京东佣金
-(void)isHaveShowOtherPlatform{
    MBShow(@"正在加载...")
    [NetRequest requestType:0 url:url_show_consumer ptdic:nil success:^(id nwdic) {
        
        NSInteger isHaveJd = [REPLACENULL(nwdic[@"ifHaveJd"]) integerValue];
        NSInteger isHaveDm = [REPLACENULL(nwdic[@"ifHaveDuomai"]) integerValue];
        NSInteger isHavePdd = [REPLACENULL(nwdic[@"ifHavePdd"]) integerValue];
        NSInteger isHaveWph = [REPLACENULL(nwdic[@"ifHaveWph"]) integerValue];
#warning ddy_ce-shi(叮叮羊消费佣金加入美团的)
//        NSInteger isHaveMt = [REPLACENULL(nwdic[@"ifHaveMt"]) integerValue];
        NSInteger isHaveMt = YES;
        if (isHaveJd) {
            CommissionModel * model = [[CommissionModel alloc]init];
            model.ptitle = @"京东";  model.pType = 2;
            model.reqUrl = url_jd_consumer;
            [_platformAry addObject:model];
        }
        
        if (isHavePdd) {
            CommissionModel * model = [[CommissionModel alloc]init];
            model.ptitle = @"拼多多";  model.pType = 3;
            model.reqUrl = url_pdd_consumer;
            [_platformAry addObject:model];
        }
        
        if (isHaveWph) {
            CommissionModel * model = [[CommissionModel alloc]init];
            model.ptitle = @"唯品会";  model.pType = 4;
            model.reqUrl = url_goods_wallet_wph;
            [_platformAry addObject:model];
        }
        
        
        CommissionModel * model = [[CommissionModel alloc]init];
        model.ptitle = @"淘宝";   model.pType = 1;
        model.reqUrl = url_new_consumer_detail;
        [_platformAry insertObject:model atIndex:0];
        
        if (PID==10107) { // 叮叮羊
            if (isHaveDm) {
                CommissionModel * model = [[CommissionModel alloc]init];
                model.ptitle = @"综合平台";   model.pType = 51;
                model.reqUrl = url_goods_wallet_dm;
                [_platformAry insertObject:model atIndex:1];
            }
            if (isHaveMt) {
                CommissionModel * model = [[CommissionModel alloc]init];
                model.ptitle = @"美团";  model.pType = 6;
                model.reqUrl = url_goods_wallet_dm;
                if (isHaveDm&&isHaveMt) {
                   [_platformAry insertObject:model atIndex:2];
                }else{
                   [_platformAry insertObject:model atIndex:1];
                }
            }
        }
        
        MBHideHUD  [self creatView];
        
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
}

// 创建控件并请求数据
-(void)creatView{
    
    if (_platformAry.count>1) { [self segmentCtrl]; }
    
    [self scrollView];
    [self creatMoneView];
    [self requestConsumer];

}

// 顶部筛选
-(UISegmentedControl*)segmentCtrl{
    if (!_segmentCtrl) {
        NSMutableArray * titleAty = [NSMutableArray array];
        for (NSInteger i=0; i<_platformAry.count; i++) {
            CommissionModel * model = _platformAry[i];
            [titleAty addObject:model.ptitle];
        }
        _segmentCtrl = [[UISegmentedControl alloc]initWithItems:titleAty];
        _segmentCtrl.backgroundColor = COLORWHITE;
        _segmentCtrl.tintColor = LOGOCOLOR ;
        _segmentCtrl.selectedSegmentIndex = 0 ;
        [_segmentCtrl addTarget:self action:@selector(clickSegmentCtrl) forControlEvents:(UIControlEventValueChanged)];
        NSDictionary * attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:13*HWB] forKey:NSFontAttributeName];
        [_segmentCtrl setTitleTextAttributes:attributes  forState:UIControlStateNormal];
        if (THEMECOLOR==2) { // 主题色为浅色
            NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:Black51,NSForegroundColorAttributeName,[UIFont systemFontOfSize:13.5*HWB],NSFontAttributeName,nil];
            [_segmentCtrl setTitleTextAttributes:dic forState:UIControlStateSelected];
            [_segmentCtrl setTitleTextAttributes:dic forState:UIControlStateNormal];
        }
        
        
        if (PID==10107) { // 叮叮羊加了个多麦和美团
            _segmentCtrl.tintColor = COLORWHITE ;
            NSDictionary * abtsN = @{NSFontAttributeName:[UIFont systemFontOfSize:12*HWB],NSForegroundColorAttributeName:Black51};
            NSDictionary * abtsH = @{NSFontAttributeName:[UIFont systemFontOfSize:12*HWB],NSForegroundColorAttributeName:LOGOCOLOR};
            [_segmentCtrl setTitleTextAttributes:abtsN  forState:UIControlStateNormal];
            [_segmentCtrl setTitleTextAttributes:abtsH  forState:UIControlStateSelected];
            
            _segmentCtrl.frame = CGRectMake(0, 0,ScreenW, 32*HWB);
            [self.view addSubview:_segmentCtrl];
            
            for (NSInteger i=1; i<_platformAry.count; i++) {
                UIImageView * linesImgv = [[UIImageView alloc]init];
                linesImgv.frame = CGRectMake(ScreenW/_platformAry.count*i, 8*HWB,0.5,16*HWB);
                linesImgv.backgroundColor = Black153;
                [self.view addSubview:linesImgv];
            }
            
        }else{
            _segmentCtrl.frame = CGRectMake(ScreenW/2-(30*_platformAry.count), 0,60*_platformAry.count, 30);
            self.navigationItem.titleView = _segmentCtrl;
        }
        
    }
    return _segmentCtrl ;
}

-(void)clickSegmentCtrl{
    CGPoint point = CGPointMake(ScreenW*_segmentCtrl.selectedSegmentIndex, 0);
    [_scrollView setContentOffset:point animated:YES];
    _selectType = _segmentCtrl.selectedSegmentIndex;
    if (_platformAry.count>_selectType) {
        [self requestConsumer];
    }else{ SHOWMSG(@"平台索引获取失败!") }
}

#pragma mark ===>>> 创建scrollView
-(UIScrollView*)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.backgroundColor = COLORWHITE ;
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.tag = 100;
        _scrollView.scrollEnabled = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(ScreenW*_platformAry.count,0);
        [self.view addSubview:_scrollView];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.offset(0);
            make.top.offset(PID==10107?35*HWB:0); // 叮叮羊加了个多麦
        }];
    }
    return _scrollView ;
}

#pragma mark ===>>> UIScrollView 代理
// 使用代码改变偏移量时执行
//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
//    if (scrollView.tag==100) { // 滑动的是scrollView
//        _selectType = scrollView.contentOffset.x/ScreenW;
//
//    }
//}

// 手动滑动结束后执行
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    if (scrollView.tag==100) { // 滑动的是scrollView
//        _selectType = scrollView.contentOffset.x/ScreenW;
//        _segmentCtrl.selectedSegmentIndex = _selectType;
//        [self requestConsumer];
//    }
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.tag==100) { // 滑动的是scrollView
//        NSLog(@"偏移量变化了:%f",scrollView.contentOffset.x);
    }
}

-(void)creatMoneView{
//    if (_platformAry.count>0) {
        float chaju = PID==10107?35*HWB:0;
        for (NSInteger i=0; i<_platformAry.count; i++) {
            _moneyView = [[CommissionView alloc]init];
            _moneyView.frame = CGRectMake(ScreenW*i,0, ScreenW, ScreenH-NAVCBAR_HEIGHT-chaju);
            _moneyView.tag = i + 10000;
            [_scrollView addSubview:_moneyView];
        }

}





#pragma mark ===>>> 请求佣金数据

-(void)requestConsumer{

    NSLog(@" tag值:%ld",(long)_selectType);
    
    if (_platformAry.count>_selectType) {
        CommissionModel * model = _platformAry[_selectType];
        NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
        
        if ([model.ptitle isEqualToString:@"唯品会"]) { pdic[@"type"] = @"4"; }
        if ([model.ptitle isEqualToString:@"综合平台"]) { pdic[@"type"] = @"51"; }
        if ([model.ptitle isEqualToString:@"美团"]) { pdic[@"type"] = @"60"; }

        if (model.monthDataAry.count==0) { MBShow(@"正在查询...") }
        [NetRequest requestType:0 url:model.reqUrl ptdic:pdic success:^(id nwdic) {
            MBHideHUD
            NSDictionary * dic = NULLDIC(nwdic[@"content"]);
            if ([model.ptitle isEqualToString:@"京东"]) { dic = NULLDIC(nwdic[@"stats"]); }
            [model addModelByPlatform:model.ptitle data:dic];
            
            LOG(@"当前选中的view: %@",[_scrollView viewWithTag:_selectType+10000]);
            UIView * cmnView = [_scrollView viewWithTag:_selectType+10000];
            if (cmnView&&[cmnView isKindOfClass:[CommissionView class]]) {
                _moneyView = (CommissionView*)cmnView;
                _moneyView.cmnModel = model;
            }
            
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            MBHideHUD SHOWMSG(failure)
            UIView * cmnView = [_scrollView viewWithTag:_selectType+10000];
            if (cmnView&&[cmnView isKindOfClass:[CommissionView class]]) {
                _moneyView = (CommissionView*)cmnView;
                _moneyView.cmnModel = model;
            }
        }];
    }
    
}

@end

