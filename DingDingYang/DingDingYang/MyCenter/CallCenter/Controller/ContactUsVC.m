//
//  ContactUsVC.m
//  DingDingYang
//
//  Created by ddy on 2019/2/18.
//  Copyright © 2019 jufan. All rights reserved.
//  客服中心

#define WECHATCOLOR  [UIColor colorWithRed:139.f/255.f green:199.f/255.f blue:24.f/255.f alpha:1]
#define QQBTNCOLOR   [UIColor colorWithRed:1.f/255.f green:160.f/255.f blue:235.f/255.f alpha:1]

#import "ContactUsVC.h"

@interface ContactUsVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView * tabView;
@property(nonatomic,strong) NSArray * qqListAry;
@property(nonatomic,strong) NSArray * wxListAry;
@property(nonatomic,copy) NSString * wxEwmImgUrl; // 微信二维码图片
@property(nonatomic,strong) NSDictionary * partnerDic; // 所有参数

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 用UITableView做
    /*
     两个分区
     区头：背景图片
     分区1：QQ客服信息
     分区2：微信客服信息
     
     */
    
    self.title = _modelTitle.length==0?@"客服中心":_modelTitle;
    self.view.backgroundColor = COLORWHITE ;

    _tabView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
    _tabView.backgroundColor = COLORWHITE;
    _tabView.delegate = self;  _tabView.dataSource = self;
    _tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tabView.rowHeight = 50*HWB; _tabView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tabView];
    [_tabView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.offset(0);
    }];
    
    [_tabView registerClass:[ContactUsHead class] forHeaderFooterViewReuseIdentifier:@"service_center_head"];
    [_tabView registerClass:[ContactUsCell class] forCellReuseIdentifier:@"service_center_cell"];
    [_tabView registerClass:[ContactUsFoot class] forHeaderFooterViewReuseIdentifier:@"service_center_foot"];

    
    [NetRequest requestType:0 url:url_kefu_info ptdic:nil success:^(id nwdic) {
        _partnerDic = NULLDIC(nwdic[@"partner"]);
        _qqListAry = NULLARY(_partnerDic[@"qqList"]);
        NSArray * wxary =  NULLARY(_partnerDic[@"wxList"]);
        if (wxary.count>0) {
            _wxListAry = wxary;
        }else{
            _wxListAry = @[@{@"wxNo":REPLACENULL(_partnerDic[@"wxNo"]),@"wxLabel":REPLACENULL(_partnerDic[@"wxLabel"])}];
        }
        [_tabView reloadData];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        SHOWMSG(failure)
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) { return _qqListAry.count; }
    return _wxListAry.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactUsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"service_center_cell" forIndexPath:indexPath];
    if (indexPath.section==0) {
        NSDictionary * qqdic = _qqListAry[indexPath.row];
        cell.openChatBtn.backgroundColor = QQBTNCOLOR;
        NSString * qqTitle = FORMATSTR(@"  %@",REPLACENULL(qqdic[@"qqName"]));
        [cell.openChatBtn setTitle:qqTitle forState:(UIControlStateNormal)];
        [cell.openChatBtn setImage:[UIImage imageNamed:@"whiteQQ"] forState:(UIControlStateNormal)];
    }
    if (indexPath.section==1) {
        NSDictionary * wxdic = _wxListAry[indexPath.row];
        cell.openChatBtn.backgroundColor = WECHATCOLOR;
        NSString * wxTitle = FORMATSTR(@"  %@",REPLACENULL(wxdic[@"wxLabel"]));
        [cell.openChatBtn setTitle:wxTitle forState:(UIControlStateNormal)];
        [cell.openChatBtn setImage:[UIImage imageNamed:@"whiteWX"] forState:(UIControlStateNormal)];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        NSDictionary * qqdic = _qqListAry[indexPath.row];
        NSString  * qqNumber = REPLACENULL(qqdic[@"qqNo"]);
        if (qqNumber.length<5) {
            SHOWMSG(@"QQ号码不正确!") return ;
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://im/chat?chat_type=wpa"]]) {
            NSURL * url=[NSURL URLWithString:[NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=app",qqNumber]];
            [[UIApplication sharedApplication]openURL:url];
        }else{ SHOWMSG(@"还没有安装qq哦!") }
    }
    if (indexPath.section==1) {
        NSDictionary * wxdic = _wxListAry[indexPath.row];
        NSString * wechatNo = REPLACENULL(wxdic[@"wxNo"]);
        if (wechatNo.length>0) {
            [ClipboardMonitoring pasteboard:wechatNo];
            SHOWMSG(@"微信号已复制，请打开微信搜索并添加")
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"weixin://"]];
            });
        }else{ SHOWMSG(@"微信号为空") }
    }
}

// 区头
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) { return ScreenW/2.1; } return 0;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        ContactUsHead * head1 = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"service_center_head"];
        head1.backgroundColor = COLORRED;
        return head1;
    }
    return nil;
}

// 区尾
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==1) { return ScreenW/3+66*HWB; } return 0;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==1) {
        ContactUsFoot * foot2 = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"service_center_foot"];
        NSString * wxEwmUrl = REPLACENULL(_partnerDic[@"wxExm"]);
        wxEwmUrl = [wxEwmUrl hasPrefix:@"http"]?wxEwmUrl:FORMATSTR(@"%@%@",dns_dynamic_loadimg,wxEwmUrl);
        [foot2.kfEwmImgView sd_setImageWithURL:[NSURL URLWithString:wxEwmUrl] placeholderImage:[UIImage imageNamed:@"logo"]];
        foot2.kfTimeLab.text = REPLACENULL(_partnerDic[@"kfTime"]);
        return foot2;
    }
    return nil;
}

@end


// 客服中心头部
@implementation ContactUsHead

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = COLORWHITE;
        
        _topImgView = [[UIImageView alloc]init];
        _topImgView.image = [UIImage imageNamed:@"centerbanner"];
#if HuiDe
        _topImgView.image = [UIImage imageNamed:@"centerBackground"];
#endif
        _topImgView.contentMode = UIViewContentModeScaleAspectFill;
        _topImgView.clipsToBounds = YES;
        [self addSubview:_topImgView];
        [_topImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.bottom.offset(-7*HWB);
        }];
    }
    return self;
}

@end


// QQ或微信号显示的cell
@implementation ContactUsCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _openChatBtn = [[UIButton alloc]init];
        [_openChatBtn cornerRadius:18*HWB];
        _openChatBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13*HWB];
        [_openChatBtn setTitleColor:COLORWHITE forState:(UIControlStateNormal)];
        [self.contentView addSubview:_openChatBtn];
        [_openChatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.left.offset(38*HWB); make.right.offset(-38*HWB);
            make.height.offset(36*HWB);
        }];
        _openChatBtn.userInteractionEnabled = NO;
    }
    return self;
}

@end


// 客服中心底部，显示客服时间，和二维码
@implementation ContactUsFoot

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE;
        _kfTimeLab = [UILabel labText:@"" color:Black102 font:13.5*HWB];
        _kfTimeLab.textAlignment = NSTextAlignmentCenter;
        _kfTimeLab.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_kfTimeLab];
        [_kfTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(7*HWB);
            make.height.offset(15*HWB);
            make.left.offset(10*HWB);
            make.right.offset(-10*HWB);
        }];
        
        _kfEwmImgView = [[UIImageView alloc]init];
        _kfEwmImgView.backgroundColor = COLORGROUP;
        _kfEwmImgView.userInteractionEnabled = YES;
        [self.contentView addSubview:_kfEwmImgView];
        [_kfEwmImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(30*HWB);
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.width.height.offset(ScreenW/3);
        }];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomInToSee:)];
        [_kfEwmImgView addGestureRecognizer:tap];
        
        UILabel * lab1 = [UILabel labText:@"客服二维码" color:LOGOCOLOR font:12*HWB];
        lab1.textAlignment = NSTextAlignmentCenter ;
        if (THEMECOLOR==2) { [lab1 setTextColor:Black51]; }
        [self.contentView addSubview:lab1];
        [lab1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_kfEwmImgView.mas_bottom).offset(10*HWB);
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.height.offset(13*HWB);
        }];
        
    }
    return self;
}

-(void)zoomInToSee:(UIGestureRecognizer*)tapImgView{
    if (_kfEwmImgView.image!=nil) {
        [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[_kfEwmImgView.image]];
    }
}

@end
