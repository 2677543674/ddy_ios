//
//  DingDingYang
//
//  Created by ddy on 2017/6/16.
//  Copyright © 2017年 ddy.All rights reserved.
//


#define WECHATCOLOR  [UIColor colorWithRed:139.f/255.f green:199.f/255.f blue:24.f/255.f alpha:1]
#define QQBTNCOLOR   [UIColor colorWithRed:1.f/255.f green:160.f/255.f blue:235.f/255.f alpha:1]

#import "NewCallCeterVC.h"
#import "WXApi.h"
#import "WXApiObject.h"


@interface NewCallCeterVC ()

@property(nonatomic,strong) UILabel * lab1 , * lab2 , * lab3 ;
@property(nonatomic,strong) UIImageView * ewmImgV ;
@property(nonatomic,strong) UIButton * wechatBtn ;
@property(nonatomic,strong) UIButton * qqSessionBtn ;
@property(nonatomic,strong) UIScrollView * scrollV ;
@property(nonatomic,strong) NSDictionary * dataDic ;
@property(nonatomic,strong) NSMutableArray * qqAry ;
@end

@implementation NewCallCeterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _modelTitle.length==0?@"客服中心":_modelTitle;
    self.view.backgroundColor = COLORWHITE ;
    
    _scrollV = [[UIScrollView alloc]init];
    _scrollV.backgroundColor = COLORGROUP ;
    _scrollV.frame = CGRectMake(0, 0, ScreenW,ScreenH-NAVCBAR_HEIGHT);
    _scrollV.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_scrollV];
    
    if (NSUDTakeData(KeFuZhongXin)) {
        _dataDic = NSUDTakeData(KeFuZhongXin) ;
        [self creatUI];
        
        static dispatch_once_t onceToken; // 有缓存，每个生命周期 只请求一次
        dispatch_once(&onceToken, ^{  [self huoquQQAndWechat]; });
        
    }else{
        [self huoquQQAndWechat];
    }
    
}

-(void)dealloc{
    
}

// 获取客服中心内容
-(void)huoquQQAndWechat{
    [NetRequest requestTokenURL:url_kefu_info parameter:nil success:^(NSDictionary *nwdic) {
        
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            _dataDic = NULLDIC(nwdic[@"partner"]);
            NSUDSaveData(_dataDic, KeFuZhongXin)
            [self creatUI];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        if (!NSUDTakeData(KeFuZhongXin)) {
            SHOWMSG(failure)
            [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
                [self huoquQQAndWechat];
            }];
        }
    }];
}

// 创建UI
-(void)creatUI{
    
    //    顶部图片高(ScreenW/2.1) + 图片和qq按钮间隔(15*HWB) + qq按钮(qqAry.count*(50*HWB)) + 客服时间(50) + 二维码(ScreenW/3) + 40
    
    
    // 移除所有子控件
    for (UIView * subV in _scrollV.subviews) { [subV removeFromSuperview]; }
    
    UIImageView * backImageView = [[UIImageView alloc]init];
    backImageView.image = [UIImage imageNamed:@"centerbanner"];
#if HuiDe
    backImageView.image = [UIImage imageNamed:@"centerBackground"];
#endif
    backImageView.contentMode=UIViewContentModeTop;
    backImageView.frame = CGRectMake(0, 0, ScreenW, ScreenW/2.1);
    
    [_scrollV addSubview:backImageView];
    NSArray* arr=NULLARY(_dataDic[@"qqList"]);
    _qqAry =[NSMutableArray arrayWithArray:arr];
    
    NSMutableDictionary* wxDic=[NSMutableDictionary dictionary];
    
    if (REPLACENULL(_dataDic[@"wxNo"]).length>0||REPLACENULL(_dataDic[@"wxLabel"]).length>0) {
        wxDic[@"qqName"]=_dataDic[@"wxLabel"];
        wxDic[@"qqNo"]=_dataDic[@"wxNo"];
    }
    [_qqAry addObject:wxDic];
    
    float hi = ScreenW/2.1+_qqAry.count*(50*HWB)+ScreenW/3+100 ;
    _scrollV.contentSize = CGSizeMake(0, hi);
    if (hi<ScreenH-NAVCBAR_HEIGHT) {
        _scrollV.contentSize= CGSizeMake(0,ScreenH-NAVCBAR_HEIGHT);
    };
    
    for (NSInteger i=0 ; i<_qqAry.count; i ++ ) {
        NSDictionary * qqdic = _qqAry[i];
        _qqSessionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _qqSessionBtn.tag = 100 + i ;
        _qqSessionBtn.layer.cornerRadius = 18*HWB;
        
        _qqSessionBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13*HWB];
        [_qqSessionBtn setTitle:FORMATSTR(@"  %@",REPLACENULL(qqdic[@"qqName"])) forState:(UIControlStateNormal)];
        NSLog(@"%ld",wxDic.count);
        NSLog(@"%ld",_qqAry.count);
        if (wxDic.count>0&&i==_qqAry.count-1) {
            [_qqSessionBtn setImage:[UIImage imageNamed:@"whiteWX"] forState:(UIControlStateNormal)];
            [_qqSessionBtn addTarget:self action:@selector(copyWechat) forControlEvents:UIControlEventTouchUpInside];
            _qqSessionBtn.backgroundColor = WECHATCOLOR;
        }else{
            [_qqSessionBtn setImage:[UIImage imageNamed:@"whiteQQ"] forState:(UIControlStateNormal)];
            [_qqSessionBtn addTarget:self action:@selector(clickOpenQQsession:) forControlEvents:UIControlEventTouchUpInside];
            _qqSessionBtn.backgroundColor = QQBTNCOLOR ;
        }
        
        _qqSessionBtn.frame = CGRectMake(38*HWB, (ScreenW/2.1+15*HWB)+i*(50*HWB) , ScreenW-76*HWB, 36*HWB);
        [_scrollV addSubview:_qqSessionBtn];
    }
    
    
    // 客服时间
    _lab3 = [UILabel labText:REPLACENULL(_dataDic[@"kfTime"]) color:Black102 font:14*HWB];
    if (_qqAry.count>0) { _lab3.frame = CGRectMake(0, YBOTTOM(_qqSessionBtn)+15, ScreenW, 20);}
    if (_qqAry.count==0){ _lab3.frame = CGRectMake(0, ScreenW/2.1+15, ScreenW, 20);}
    _lab3.textAlignment = NSTextAlignmentCenter ;
    [_scrollV addSubview:_lab3];
    
    
    // 客服二维码
    _lab1 = [UILabel labText:@"客服二维码" color:LOGOCOLOR font:12*HWB];
    _lab1.textAlignment = NSTextAlignmentCenter ;
    if (THEMECOLOR==2) { [_lab1 setTextColor:Black51]; }
    _lab1.frame = CGRectMake(0, YBOTTOM(_lab3)+25+ScreenW/3, ScreenW, 20);
    
    [_scrollV addSubview:_lab1];
    
    
    // 二维码地址
    NSString * urlImgWX = [REPLACENULL(_dataDic[@"wxExm"]) hasPrefix:@"http"]?REPLACENULL(_dataDic[@"wxExm"]):FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(_dataDic[@"wxExm"])) ;
    _ewmImgV = [[UIImageView alloc]init];
    _ewmImgV.backgroundColor = COLORGROUP ;
    
    _ewmImgV.frame = CGRectMake(ScreenW/2-ScreenW/6, YBOTTOM(_lab3)+15, ScreenW/3, ScreenW/3);
    
    [_scrollV addSubview:_ewmImgV];
    _ewmImgV.userInteractionEnabled = YES ;
    [_ewmImgV sd_setImageWithURL:[NSURL URLWithString:urlImgWX] placeholderImage:PlaceholderImg];
    
#if (defined BeiLeBa)||(defined YueNiSuoXiang)
    if (CANOPENWechat==NO) {
        [_ewmImgV setImage:PlaceholderImg];
        _lab1.hidden=YES;
        _ewmImgV.hidden=YES;
    }
#endif
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fangBigg:)];
    [_ewmImgV  addGestureRecognizer:tap];
    
    [_scrollV addSubview:_wechatBtn];
    
    
    
}

-(void)fangBigg:(UIGestureRecognizer*)tapImg{
    if (REPLACENULL(_dataDic[@"wxExm"]).length>0&&_ewmImgV.image!=nil) {
        [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[_ewmImgV.image]];
    }
}

-(void)copyWechat{
    [ClipboardMonitoring pasteboard:_dataDic[@"wxNo"]] ;
    SHOWMSG(@"复制成功!")
    NSInteger openType = [_dataDic[@"type"] integerValue];
    if (openType==2) {
        NSString  *qqNumber= REPLACENULL(_dataDic[@"wxNo"]) ;
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://im/chat?chat_type=wpa"]]) {
            NSURL * url=[NSURL URLWithString:[NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=app",qqNumber]];
            [[UIApplication sharedApplication]openURL:url];
        }else{
            SHOWMSG(@"还没有安装qq哦!")
        }
    }
    if (openType==1) { [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"weixin://"]]; }
}

// 长按保存图片
//-(void)handleLongPress:(UILongPressGestureRecognizer*)longPressRecognizer{
//    if (longPressRecognizer.state == UIGestureRecognizerStateBegan) {
//        UIImageView*imgView=(UIImageView*)longPressRecognizer.view;
//        if (imgView.image!=nil) {
//            UIImageWriteToSavedPhotosAlbum(imgView.image,self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//        }
//    }
//}
////保存图片到相册
//- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
//    if (!error) {SHOWMSG(@"保存成功!")}
//    else{ SHOWMSG(@"保存失败!"); }
//}

// 打开qq临时会话
-(void)clickOpenQQsession:(UIButton*)sessionBtn{
    NSDictionary * dic = _qqAry[sessionBtn.tag-100] ;
    NSString  * qqNumber= REPLACENULL(dic[@"qqNo"]);
    if (qqNumber.length<5) {
        SHOWMSG(@"QQ号码不正确!") return ;
    }
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://im/chat?chat_type=wpa"]]) {
        NSURL * url=[NSURL URLWithString:[NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=app",qqNumber]];
        [[UIApplication sharedApplication]openURL:url];
    }else{
        SHOWMSG(@"还没有安装qq哦!")
    }
}




@end

