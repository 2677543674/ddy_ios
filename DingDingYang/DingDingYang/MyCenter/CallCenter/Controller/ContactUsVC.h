//
//  ContactUsVC.h
//  DingDingYang
//
//  Created by ddy on 2019/2/18.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "DDYViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactUsVC : DDYViewController
@property(nonatomic,copy) NSString* modelTitle;
@end

NS_ASSUME_NONNULL_END


// 客服中心头部
@interface ContactUsHead : UITableViewHeaderFooterView
@property(nonatomic,strong) UIImageView * topImgView;
@end

// QQ或微信号显示的cell
@interface ContactUsCell : UITableViewCell
@property(nonatomic,strong) UIButton * openChatBtn;
@end

// 客服中心底部，显示客服时间，和二维码
@interface ContactUsFoot : UITableViewHeaderFooterView

@property(nonatomic,strong) UILabel * kfTimeLab;
@property(nonatomic,strong) UIImageView * kfEwmImgView;

@end

















