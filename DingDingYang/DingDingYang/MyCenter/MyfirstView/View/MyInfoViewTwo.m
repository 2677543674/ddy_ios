//
//  MyInfoViewTwo.m
//  DingDingYang
//
//  Created by ddy on 2018/1/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MyInfoViewTwo.h"

@implementation MyInfoViewTwo


-(void)setDynamicModel:(MyDynamicModel *)dynamicModel{
    _dynamicModel = dynamicModel ;
    [_cltView reloadData];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.clipsToBounds = YES ;
        self.backgroundColor = COLORGROUP ;
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self cltView];
        _cltView.layer.masksToBounds = YES ;
        _cltView.layer.cornerRadius = 6 * HWB ;
    }
    return self ;
}

-(UICollectionView*)cltView{
    if (!_cltView) {
        float  wh = FloatKeepInt((ScreenW-34)/3) ;
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(wh,wh-10*HWB);
        layout.minimumInteritemSpacing = 1.5 ;
        layout.minimumLineSpacing = 1.5 ;
        
        _cltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _cltView.backgroundColor = COLORGROUP ;
        _cltView.dataSource = self ; _cltView.delegate = self ;
        [_cltView registerClass:[MyImgTxtCltCell1 class] forCellWithReuseIdentifier:@"img_txt_cltcell1"] ;
        [self.contentView addSubview:_cltView];
        [_cltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(15); make.right.offset(-15);
        }];
    }
    return _cltView ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dynamicModel.cateInfoList1.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MyImgTxtCltCell1 * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"img_txt_cltcell1" forIndexPath:indexPath];
    cell.classModel = _dynamicModel.cateInfoList1[indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [MyPageJump pushNextVcByModel:_dynamicModel.cateInfoList1[indexPath.item] byType:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end





@implementation MyImgTxtCltCell1

-(void)setClassModel:(MyClassModel *)classModel{
    _classModel = classModel ;
    _titLab.text = _classModel.title ;
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_classModel.img] placeholderImage:_classModel.placeholderImg];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = COLORWHITE ;
        
        _imgView = [[UIImageView alloc] init];
        _imgView.contentMode = UIViewContentModeScaleAspectFit ;
        _imgView.image = PlaceholderImg ;
        [self addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(2*HWB);
            make.width.offset(26*HWB); make.height.offset(26*HWB);
        }];
        
        _titLab=[UILabel labText:@"标题标题" color:Black51 font:13*HWB];
        [self addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);  make.height.offset(20*HWB);
            make.top.equalTo(self.contentView.mas_centerY).offset(5*HWB);
        }];

    }
    return self;
}


@end







