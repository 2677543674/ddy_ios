//
//  ImgTextCell.m
//  DingDingYang
//
//  Created by ddy on 2017/3/7.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ImgTextCell.h"

@implementation ImgTextCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    //cell高度为46
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        _imgView = [[UIImageView alloc]init];
        _imgView.image = PlaceholderImg ;
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.left.offset(15*HWB);  make.height.offset(20*HWB);
            make.width.offset(20*HWB);
        }];
         
        _titLab = [UILabel labText:@"标题" color:Black102 font:13*HWB];
        [self.contentView addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(45*HWB); make.height.offset(20);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
        
        _redLab = [UILabel labText:@"" color:COLORWHITE font:12*HWB];
        _redLab.backgroundColor = COLORRED ;
        _redLab.layer.masksToBounds = YES ;
        _redLab.layer.cornerRadius = 9 ;
        _redLab.hidden = YES ;
        _redLab.textAlignment = NSTextAlignmentCenter ;
        [self.contentView addSubview:_redLab];
        [_redLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);  make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.offset(18*HWB);  make.width.offset(18*HWB);
        }];
        
        
        
        //重要消息提醒
        _backView = [UIView new];
        _backView.hidden = YES ;
        _backView.backgroundColor=[UIColor clearColor];
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(5);
            make.top.offset(0);
            make.bottom.offset(0);
            make.width.offset(200*HWB);
        }];
        
        _switchView = [[UISwitch alloc]init];
        _switchView.userInteractionEnabled = NO ;
        //        [_switchView setOnTintColor:LOGOCOLOR];
        [_backView addSubview:_switchView];
        [_switchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-20*HWB);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        _swicthLabel=[UILabel labText:@"已开启" color:Black102 font:10.0*HWB];
        [_backView addSubview:_swicthLabel];
        [_swicthLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_switchView.mas_left).offset(-5);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
    }return self;
}

-(void)setNameImg:(NSString *)nameImg{
    _nameImg=nameImg;
    _imgView.image=[UIImage imageNamed:nameImg];
}
-(void)setNameTxt:(NSString *)nameTxt{
    _nameTxt=nameTxt;
    _titLab.text=nameTxt;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
