//
//  MyInfoCellThree.m
//  DingDingYang
//
//  Created by OU on 2019/7/19.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "MyInfoCellThree.h"

@implementation MyInfoCellThree

-(void)setDynamicModel:(MyDynamicModel *)dynamicModel{
    _dynamicModel = dynamicModel ;
//    [_cltView reloadData];
}

-(void)setClipsType:(NSInteger)clipsType{
    
    @try {
        switch (clipsType) {
            case 0:{ // 无圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(-8*HWB);  make.bottom.offset(8*HWB);
                }];
            } break;
                
            case 1:{ // 1:上半部分圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);  make.bottom.offset(8*HWB);
                }];
                
            } break;
                
            case 2:{ // 2:下半部分圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(-8*HWB);  make.bottom.offset(-1.5);
                }];
            } break;
                
            case 3:{ // 3:全部圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);  make.bottom.offset(-1.5);
                }];
            } break;
                
            default: break;
        }
    } @catch (NSException *exception) { } @finally { }
    
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    //cell高度为 45 * HWB
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLORGROUP  ;
//        _backView = [[UIView alloc]init];
//        _backView.clipsToBounds = YES ;
//        [self.contentView addSubview:_backView];
//        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(0);  make.bottom.offset(0);
//            make.left.offset(15); make.right.offset(-15);
//        }];
        
        [self functionView];
        
    }return self;
}

-(MyInfoCellThreeView*)functionView{
    if (!_functionView) {
        _functionView = [[MyInfoCellThreeView alloc]init];
        [self.contentView addSubview:_functionView];
        [_functionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);  make.right.offset(-15);
//            make.centerY.equalTo(_bgimgView.mas_bottom);
//            make.height.offset(70*HWB) ;
            make.top.bottom.offset(0);
        }];
    }
    return _functionView ;
}

@end

#pragma mark ===>>> 个人中心里的三个功能自定义按钮的三个功能块
@implementation MyInfoCellThreeView

-(void)setMyModel:(MyDynamicModel *)myModel{
    
    _myModel = myModel ;
    [_cltView reloadData];
    
}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = COLORWHITE ;
        self.layer.masksToBounds = YES ;
        self.layer.cornerRadius = 6*HWB ;
        self.clipsToBounds = YES ;
        
        _bgImgV = [[UIImageView alloc]init]; // 加背景图
        //        _bgImgV.image = [UIImage imageNamed:@"wdbg"];
        [self addSubview:_bgImgV];
        [_bgImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [self cltView];
//        [self DistansionLabel];
        
    }
    return self ;
}

//-(UILabel *)DistansionLabel{
//    if (!_DistansionLabel) {
//        _DistansionLabel = [UILabel new];
//        _DistansionLabel.text = @"其它服务";
//        _DistansionLabel.hidden = true;
//        [self addSubview:_DistansionLabel];
//        [_DistansionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(21);
//            make.left.offset(13);
//        }];
//    }
//    return _DistansionLabel;
//}

-(UICollectionView*)cltView{
    if (!_cltView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumInteritemSpacing = 1.5 ;  layout.minimumLineSpacing = 1.5 ;
        _cltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _cltView.backgroundColor = COLORCLEAR ;
        _cltView.dataSource = self ; _cltView.delegate = self ;
        [_cltView registerClass:[MyInfoCellThreeViewCell1 class] forCellWithReuseIdentifier:@"img_txt_infocellthree"] ;
        [self addSubview:_cltView];
        [_cltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.bottom.offset(0);
            make.left.offset(0);
            make.right.offset(0);
        }];
    }
    return _cltView ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _myModel.cateInfoList1.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MyInfoCellThreeViewCell1 * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"img_txt_infocellthree" forIndexPath:indexPath];
    cell.tag = indexPath.item;
    if (_myModel.cateInfoList1.count>indexPath.item) {
        cell.myModel = _myModel.cateInfoList1[indexPath.item];
    }else{ [_cltView reloadData]; }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float wh = FloatKeepInt((ScreenW-30-_myModel.cateInfoList1.count*2)/_myModel.cateInfoList1.count);
    return CGSizeMake(wh, 70*HWB);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [MyPageJump pushNextVcByModel:_myModel.cateInfoList1[indexPath.item] byType:0];
}

@end



#pragma mark ===>>> 个人中心里的个人信息中自定义cltcell

@implementation MyInfoCellThreeViewCell1

-(void)setMyModel:(MyClassModel *)myModel{
    _myModel = myModel ;
    _titleLab.text = _myModel.title ;
    [_iconImgV sd_setImageWithURL:[NSURL URLWithString:_myModel.img] placeholderImage:_myModel.placeholderImg];
//    if (self.tag==0) { _linesImgV.hidden = YES ; }else{ _linesImgV.hidden = NO ; }
    _linesImgV.hidden = YES ;
    if (myModel.listViewType==50) {
        if ([[RCIMClient sharedRCIMClient] getTotalUnreadCount]>0) {
            _redPoint.hidden=NO;
        }else{
            _redPoint.hidden=YES;
        }
    }else{
        _redPoint.hidden=YES;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        _iconImgV = [[UIImageView alloc] init];
        _iconImgV.contentMode = UIViewContentModeScaleAspectFit ;
        _iconImgV.image = PlaceholderImg ;
        [self addSubview:_iconImgV];
        [_iconImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.contentView.mas_top).offset(16*HWB);
            make.width.offset(23*HWB); make.height.offset(23*HWB);
        }];
        
        _titleLab=[UILabel labText:@"标题标题" color:Black51 font:13*HWB];
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);  make.height.offset(20*HWB);
            make.top.equalTo(self.iconImgV.mas_bottom).offset(12*HWB);
        }];
        
        _linesImgV = [[UIImageView alloc]init];
        _linesImgV.backgroundColor = Black204 ;
        [self.contentView addSubview:_linesImgV];
        [_linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);     make.height.offset(38*HWB);
            make.width.offset(0.5);  make.centerY.equalTo(self.mas_centerY);
        }];
        
        _redPoint= [[UIImageView alloc]init];
        _redPoint.layer.masksToBounds = YES ;
        _redPoint.layer.cornerRadius = 4 ;
        _redPoint.backgroundColor = COLORRED ;
        [self.contentView addSubview:_redPoint];
        [_redPoint mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_iconImgV.mas_right);
            make.centerY.equalTo(_iconImgV.mas_top);
            //            make.top.offset(-2);   make.right.offset(2);
            make.height.offset(8); make.width.offset(8);
        }];
    }
    return self;
}

@end


#pragma mark ===>>> 空的区头或区脚

@implementation MyInfoCellThreeViewNull
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.clipsToBounds = YES ;
    }
    return self ;
}
@end

