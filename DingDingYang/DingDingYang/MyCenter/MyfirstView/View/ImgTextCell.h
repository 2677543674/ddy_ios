//
//  ImgTextCell.h
//  DingDingYang
//
//  Created by ddy on 2017/3/7.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImgTextCell : UITableViewCell

@property(nonatomic,strong) UIImageView * imgView; // 前面的图片
@property(nonatomic,strong) UILabel * titLab; // 标题
@property(nonatomic,strong) UILabel * redLab; // 未读消息提醒(红色圆角标示符)

@property(nonatomic,copy) NSString * nameImg, * nameTxt;

// 蜜赚将关于我们改为设置，并加上推送提醒
@property(nonatomic,strong) UIView   * backView;
@property(nonatomic,strong) UISwitch * switchView;
@property(nonatomic,strong) UILabel  * swicthLabel;
@property(nonatomic,strong) UIButton * swBtn;


@end


