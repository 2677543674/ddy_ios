//
//  MyInfoCellTwo.m
//  DingDingYang
//
//  Created by OU on 2019/7/19.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "MyInfoCellTwo.h"

@implementation MyInfoCellTwo

-(void)setDynamicModel:(MyDynamicModel *)dynamicModel{
    
    _dynamicModel = dynamicModel ;
    [_imgUrlAry removeAllObjects];
    for (NSInteger i =0; i<_dynamicModel.cateInfoList1.count; i++) {
        @try {
            MyClassModel * model = _dynamicModel.cateInfoList1[i] ;
            [_imgUrlAry addObject:model.img];
        } @catch (NSException *exception) { } @finally { }
        
    }
    
    if (_imgUrlAry&&_imgUrlAry.count>0) {
        _sdcsView.imageURLStringsGroup = _imgUrlAry;
    }else{
        _sdcsView.imageURLStringsGroup = @[];
    }
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    //cell高度为 45 * HWB
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLORGROUP;
        self.contentView.clipsToBounds = YES ;
        _imgUrlAry = [NSMutableArray array];

        [self sdcsView];
        
    }
    return self;
}

#pragma mark ---> 创建轮播图
-(SDCycleScrollView*)sdcsView{
    if (!_sdcsView) {
        _sdcsView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero imageURLStringsGroup:nil];
        _sdcsView.layer.masksToBounds = YES;
        _sdcsView.layer.cornerRadius = 8;
        _sdcsView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _sdcsView.backgroundColor = COLORWHITE ;
        _sdcsView.delegate = self;
        _sdcsView.currentPageDotColor = LOGOCOLOR ;
        _sdcsView.pageDotColor = COLORWHITE;
        _sdcsView.autoScrollTimeInterval = 3.0;
        _sdcsView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_sdcsView];
        [_sdcsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5);  make.bottom.offset(-5);
            make.left.offset(15); make.right.offset(-15);
        }];
    }
    return _sdcsView ;
}

//轮播图代理
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
//    [PageJump pushAccordingToThe:_dynamicModel.cateInfoList1[index]];
    
    [MyPageJump pushNextVcByModel:_dynamicModel.cateInfoList1[index] byType:0];
    
    //    if (_dataAry.count>index) {
    //        if (TopNavc!=nil) {
    //            BHModel * model = _dataAry[index];
    //            switch (model.todo) {
    //
    //                case  1:{
    //                    GoodsDetailsVC * detailVc = [[GoodsDetailsVC alloc]init] ;
    //                    detailVc.hidesBottomBarWhenPushed = YES ;
    //                    detailVc.dmodel = model.goodsMd ;
    //                    [TopNavc pushViewController:detailVc  animated:YES] ;
    //                } break;
    //
    //                case  2:{
    //                    if ([model.link containsString:@"http"]) {
    //                        if ([model.link containsString:@"weixin.qq.com"]) {
    //                            if (CANOPENAlipay==NO&&CANOPENWechat==NO) {  return ;  }
    //                        }
    //                        WebVC * web = [[WebVC alloc]init];
    //                        web.urlWeb = model.link ;
    //                        web.nameTitle = @"详情页" ;
    //                        web.hidesBottomBarWhenPushed = YES ;
    //                        [TopNavc pushViewController:web animated:YES];
    //                    }
    //                } break;
    //
    //                case  3:{
    //                    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:model.link]]) {
    //                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.link]];
    //                    }else{ SHOWMSG(@"无法打开链接!") }
    //                } break;
    //
    //                default:{
    //                    if ([model.link hasPrefix:@"http"]) {
    //                        WebVC * web = [[WebVC alloc]init];
    //                        web.urlWeb = model.link ;
    //                        web.nameTitle = @"详情页" ;
    //                        web.hidesBottomBarWhenPushed = YES ;
    //                        [TopNavc pushViewController:web animated:YES];
    //                    }
    //                }
    //                    break;
    //            }
    //        }
    //    }
    
}

@end
