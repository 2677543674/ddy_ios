//
//  MyInfoCellThree.h
//  DingDingYang
//
//  Created by OU on 2019/7/19.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MyInfoCellThreeView;

@interface MyInfoCellThree : UITableViewCell

@property(nonatomic,strong) UIView * backView ;
@property(nonatomic,strong) UIImageView * bgimgView ; // 背景图
@property(nonatomic,strong) UIImageView * imgView; // 前面的图片
@property(nonatomic,strong) UILabel * titLab;      // 标题
@property(nonatomic,strong) UILabel * redLab;      // 未读消息提醒(红色圆角标示符)

@property(nonatomic,strong) MyDynamicModel * dynamicModel ;
/**
 圆角类型 0:清除layer  1:上半部分  2:下半部分  3:全部
 */
@property(nonatomic,assign) NSInteger clipsType ;  // cell要加的圆角类型 1:上半部分  2:下半部分  3:全部


@property(nonatomic,strong) MyInfoCellThreeView * functionView ;


@end


// 个人信息下方的几个分类
@class MyDynamicModel,FunctionOneCltCelll ;
@interface MyInfoCellThreeView : UIView <UICollectionViewDelegate,UICollectionViewDataSource>
//@property(nonatomic,strong)UILabel    *DistansionLabel;//其他服务
@property(nonatomic,strong) UIImageView * bgImgV ; // 背景图
@property(nonatomic,strong) UICollectionView * cltView;
@property(nonatomic,strong) MyDynamicModel * myModel;
@end


//个人中心钱包，推广佣金，团队自定义按钮
@interface MyInfoCellThreeViewCell1 : UICollectionViewCell
@property(nonatomic,strong) UIImageView * linesImgV ; // 分割线
@property(nonatomic,strong) UIImageView * iconImgV ;  // 图片
@property(nonatomic,strong) UIImageView * redPoint ;  // 小红点
@property(nonatomic,strong) UILabel * titleLab ;      // 标题
@property(nonatomic,strong) MyClassModel * myModel ;
@end


@interface MyInfoCellThreeViewNull : UITableViewHeaderFooterView
@end
