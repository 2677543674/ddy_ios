//
//  MyInfoCellTwo.h
//  DingDingYang
//
//  Created by OU on 2019/7/19.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyInfoCellTwo : UITableViewCell <SDCycleScrollViewDelegate>

@property(nonatomic,strong) SDCycleScrollView * sdcsView ;

@property(nonatomic,strong) NSMutableArray * imgUrlAry ; //图片url数组
//@property(nonatomic,strong) NSMutableArray * dataAry ; //所有数据
@property(nonatomic,strong) MyDynamicModel * dynamicModel ;

@property(nonatomic,strong) NSMutableArray * urlAry ; //详情页(图片)
@property(nonatomic,strong) NSArray * imgAry ; //本地图片

@end

NS_ASSUME_NONNULL_END
