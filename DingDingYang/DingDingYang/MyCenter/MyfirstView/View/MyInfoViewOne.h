//
//  MyInfoViewOne.h
//  DingDingYang
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@class FunctionViewCardOne ;
// 我的个人信息
@interface MyInfoViewOne : UITableViewHeaderFooterView

@property(nonatomic,strong) UIImageView * bgimgView;    // 背景图片(毛玻璃效果)
@property(nonatomic,strong) UILabel * topTitleLab ;     // 标题
@property(nonatomic,strong) UIButton * mesgBtn ;        // 消息按钮
@property(nonatomic,strong) UIButton * bigMesgBtn ;     // 大的消息按钮
@property(nonatomic,strong) UIImageView * redPointImg ; // 消息红点显示图片

@property(nonatomic,strong) UIView   * bgView ;      // 放头像，昵称，手机号，角色名称
@property(nonatomic,strong) UIImageView * headImgV ; // 头像
@property(nonatomic,strong) UILabel  * nickNameLab ; // 显示昵称，微信号，淘宝号，姓名等
@property(nonatomic,strong) UILabel  * phoneNumLab ; // 手机号/换成邀请码

@property(nonatomic,strong)UIButton    *commandCInviteCodeBTn; //复制邀请码
@property(nonatomic,strong) UIButton * levelBtn ;    // 显示角色

@property(nonatomic,strong) UIButton * setBtn ;      // 设置按钮

//@property(nonatomic,strong) FunctionViewOne * functionView ;

@property(nonatomic,strong)FunctionViewCardOne    *functionCardView;

@property(nonatomic,strong) PersonalModel * pmodel ;
@end

@class MyDynamicModel;
@interface FunctionViewCardOne:UIView
@property(nonatomic,strong)UIImageView    *topImageView;//淘宝权益

@property(nonatomic,strong)UILabel    *monthMoneyLabel;
@property(nonatomic,strong) UIControl    *monthMoneyControl;//本月预估>
@property(nonatomic,strong)UILabel    *todayMoneyLabel;
@property(nonatomic,strong)UILabel    *todayMoneyStringLabel;//今日预估收入
@property(nonatomic,strong)UILabel    *monthThinkMoneyLael;
@property(nonatomic,strong)UILabel    *monthThinkMoneyStringLabel;//本月预估收入
@property(nonatomic,strong)UILabel    *monthMoneyStringLabel;
@property(nonatomic,strong)UIView    *lineView;
@property(nonatomic,strong)UIImageView    *tipImageView;

@property(nonatomic,strong) MyDynamicModel * myModel;
@end




// 个人信息下方的几个分类
@class MyDynamicModel,FunctionOneCltCelll ;
@interface FunctionViewOne : UIView <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UIImageView * bgImgV ; // 背景图
@property(nonatomic,strong) UICollectionView * cltView;
@property(nonatomic,strong) MyDynamicModel * myModel;
@end


//个人中心钱包，推广佣金，团队自定义按钮
@interface FunctionOneCltCelll : UICollectionViewCell
@property(nonatomic,strong) UIImageView * linesImgV ; // 分割线
@property(nonatomic,strong) UIImageView * iconImgV ;  // 图片
@property(nonatomic,strong) UIImageView * redPoint ;  // 小红点
@property(nonatomic,strong) UILabel * titleLab ;      // 标题
@property(nonatomic,strong) MyClassModel * myModel ;
@end


@interface MyInfoViewNull : UITableViewHeaderFooterView
@end

















