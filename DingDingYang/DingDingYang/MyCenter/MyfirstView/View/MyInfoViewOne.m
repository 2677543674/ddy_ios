//
//  MyInfoViewOne.m
//  DingDingYang
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MyInfoViewOne.h"
#import "NewShareAppVC.h"
#import "LZMyCenterInfoServer.h"
#pragma mark ===>>> 个人信息顶部区头

@implementation MyInfoViewOne

-(void)setPmodel:(PersonalModel *)pmodel{
    _pmodel = pmodel ;
    
    if ([_pmodel.headImg isKindOfClass:[NSString class]]) {
        [_headImgV sd_setImageWithURL:_pmodel.headImg placeholderImage:[UIImage imageNamed:@"header_null_image"]];
        
    }else if([_pmodel.headImg isKindOfClass:[UIImage class]]){
        _headImgV.image = _pmodel.headImg ;
    }
    
    // 后台有关昵称的字段太乱，这里多次判空
    NSString * nickName = _pmodel.wxName ;
    if (_pmodel.wxName.length==0) { nickName = _pmodel.taobaoAccount ; }
    if (nickName.length==0) { nickName = _pmodel.nickName; }
    if (PID==10104) { // 蜜赚直接读微信的昵称
        if (NSUDTakeData(Wechat_Info)!=nil) {
            NSDictionary * wechatDic = (NSDictionary*)NSUDTakeData(Wechat_Info);
            nickName = REPLACENULL(wechatDic[@"nickname"]);
        }
    }
    if (nickName.length==0) { nickName = @"未设置"; }
    _nickNameLab.text = nickName ;
    if (StringSize(nickName, 12.5*HWB).width+5>100*HWB) {
        // 昵称过长，需要限制
        [_nickNameLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(10);
            make.bottom.equalTo(_headImgV.mas_centerY).offset(-4*HWB);
            make.width.offset(100*HWB);
        }];
    }else{
        [_nickNameLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(10);
            make.bottom.equalTo(_headImgV.mas_centerY).offset(-4*HWB);
        }];
    }

    // 后台查询信息较慢，可能查询失败
    if (_pmodel.roleName.length>0) {
        _levelBtn.hidden = NO ;
        [_levelBtn setTitle:FORMATSTR(@"  %@  ",_pmodel.roleName)  forState:UIControlStateNormal];
    }else{
        _levelBtn.hidden = YES ;
    }
    //手机号换成邀请码
//    _phoneNumLab.text = _pmodel.mobile ;
//    if (_pmodel.mobile.length==11) {
//       _phoneNumLab.text = [_pmodel.mobile stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
//    }
}



-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = COLORGROUP ;
    
        [self bgimgView];
                
       [self mesgBtn];
        
        [self bgView];
       
        [self functionCardView];
        
        [self setBtn];
        
        [self bigMesgBtn];
        
    }
    return self ;

}

-(void)clickPushToCeShi{
    [MyPageJump pushNextVcByModel:nil byType:1000];
}

-(UIImageView*)bgimgView{
    if (!_bgimgView) {
        _bgimgView = [[UIImageView alloc]init];
        _bgimgView.backgroundColor = COLORGROUP ;
//        _bgimgView.image = PlaceholderImg ;
        _bgimgView.contentMode = UIViewContentModeScaleAspectFill ;
        _bgimgView.clipsToBounds = YES ;
        [self.contentView addSubview:_bgimgView];
        [_bgimgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(-(NAVCBAR_HEIGHT-44));
            make.height.mas_equalTo(185*HWB);
            make.left.offset(0);  make.right.offset(0);
        }];
        
        _bgimgView.image = [UIImage imageNamed:@"mzh_my_headbg2"];
        //橙色图片换红色
//        _bgView.backgroundColor= [UIColor colorWithRed:253 green:109 blue:142 alpha:1];
        
        
        // 叮当:10102、蜜源:10016、叮叮羊:10107、今日有券:10109
        if (PID==10102||PID==10016||PID==10102||PID==10109) {
            _bgimgView.image = [UIImage imageNamed:@"background"];
        }
        if (PID==10110) { // 惠得
            _bgimgView.image = [UIImage imageNamed:@"hd_my_headbg"];
        }
        
    }
    return _bgimgView ;
}

-(UILabel*)topTitleLab{
    if (!_topTitleLab) {
        _topTitleLab = [UILabel labText:@"我的" color:COLORWHITE font:14*HWB];
        [self.contentView addSubview:_topTitleLab];
        [_topTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(12);
            make.height.offset(20);
            make.centerX.equalTo(self.contentView.mas_centerX);
        }];
    }
    return _topTitleLab ;
}

-(UIButton*)mesgBtn{
    if (!_mesgBtn) {
        _mesgBtn = [[UIButton alloc]init];
        [_mesgBtn setBackgroundImage:[UIImage imageNamed:@"new_mesgbtn"] forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(_mesgBtn, pushToMsg)
        [self.contentView addSubview:_mesgBtn];
        [_mesgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-40*HWB);
            make.width.offset(20*HWB);
            make.height.offset(19*HWB);
//            make.centerY.equalTo(_nickNameLab.mas_centerY);
            make.top.offset(27*HWB);
        }];
    }
    [self redPointImg];
    return _mesgBtn ;
}

-(UIButton *)bigMesgBtn{
    if (!_bigMesgBtn) {
        _bigMesgBtn=[[UIButton alloc]init];
        ADDTARGETBUTTON(_bigMesgBtn, pushToMsg)
        [self.contentView addSubview:_bigMesgBtn];
        [_bigMesgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_mesgBtn.mas_centerX); make.width.offset(40*HWB);
            make.height.offset(40*HWB); make.centerY.equalTo(_mesgBtn.mas_centerY);
        }];
    }
    return _bigMesgBtn;
}

-(UIImageView*)redPointImg{
    if (!_redPointImg) {
        _redPointImg = [[UIImageView alloc]init];
        _redPointImg.layer.masksToBounds = YES ;
        _redPointImg.layer.cornerRadius = 4 ;
        _redPointImg.backgroundColor = COLORRED ;
        _redPointImg.hidden = YES ;
        [_mesgBtn addSubview:_redPointImg];
        [_redPointImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(-2);   make.right.offset(2);
            make.height.offset(8); make.width.offset(8);
        }];
    }
    return _redPointImg ;
}

-(FunctionViewCardOne *)functionCardView{
    if (!_functionCardView) {
        _functionCardView = [[FunctionViewCardOne alloc]init];
        [self.contentView addSubview:_functionCardView];
        [_functionCardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);  make.right.offset(-15);
            make.top.equalTo(_bgimgView.mas_bottom).offset(-50*HWB);
            make.height.mas_equalTo(174*HWB) ;
        }];
    }
    return _functionCardView ;
}
//8月界面修改
//-(FunctionViewOne*)functionView{
//    if (!_functionView) {
//        _functionView = [[FunctionViewOne alloc]init];
//        [self.contentView addSubview:_functionView];
//        [_functionView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.offset(15);  make.right.offset(-15);
//            make.centerY.equalTo(_bgimgView.mas_bottom).offset(60);
//            make.height.mas_equalTo(174*HWB) ;
//        }];
//    }
//    return _functionView ;
//}

-(UIView*)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0);
            make.top.offset(0);
            make.bottom.offset(-70*HWB-20);
        }];
    }
    
    [self headImgV];    // 头像
    [self nickNameLab]; // 昵称
    [self phoneNumLab]; // 手机号
    [self commandCInviteCodeBTn];//复制邀请码
    if (CANOPENWechat==YES) {
        [self levelBtn];    // 身份状态
    }
    
    return _bgView ;
}

//头像
-(UIImageView*)headImgV{
    if (!_headImgV) {
        _headImgV = [[UIImageView alloc]init];
        _headImgV.layer.masksToBounds = YES;
        _headImgV.layer.cornerRadius = 28*HWB ;
        _headImgV.contentMode = UIViewContentModeScaleAspectFill;
        _headImgV.backgroundColor = COLORGROUP ;
        _headImgV.image = [UIImage imageNamed:@"logo"] ;
        [_bgView addSubview:_headImgV];
        [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(25); make.top.equalTo(_mesgBtn.mas_centerY).offset(-5);
            make.height.offset(56*HWB); make.width.offset(56*HWB);
        }];
                
    }
    return _headImgV;
}

//
-(UILabel*)nickNameLab{
    if (!_nickNameLab) {
        _nickNameLab=[UILabel labText:@"昵称未设置" color:COLORWHITE font:12.5*HWB];
        [_bgView addSubview:_nickNameLab];
#if MiYuan
        _nickNameLab.textColor=Black51;
#endif
        [_nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(_headImgV.mas_top);
            make.left.equalTo(_headImgV.mas_right).offset(10);
            make.bottom.equalTo(_headImgV.mas_centerY).offset(-4*HWB);
        }];
    }
    return _nickNameLab ;
}

//代理商级别
-(UIButton*)levelBtn{
    if (!_levelBtn) {
        _levelBtn=[UIButton titLe:@"  用户  " bgColor:[UIColor clearColor] titColorN:COLORWHITE titColorH:COLORWHITE font:11*HWB];

        [_levelBtn boardWidth:1 boardColor:COLORWHITE corner:8*HWB];
#if MiYuan
        [_levelBtn setTitleColor:Black51 forState:UIControlStateNormal];
        _levelBtn.layer.borderColor=Black51.CGColor;
#endif
        [_bgView addSubview:_levelBtn];
        [_levelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_headImgV.mas_right).offset(10);
//            make.centerY.equalTo(_headImgV.mas_centerY);
            make.height.offset(16*HWB);
            make.left.equalTo(_nickNameLab.mas_right).offset(3);
            make.centerY.equalTo(_nickNameLab.mas_centerY);
          
        }];
    }
    return _levelBtn;
}

//电话号码
-(UILabel*)phoneNumLab{
    if (!_phoneNumLab) {
        _phoneNumLab=[UILabel labText:@"手机号" color:COLORWHITE font:13*HWB];
#if MiYuan
        _phoneNumLab.textColor=Black51;
#endif
        [_bgView addSubview:_phoneNumLab];
//手机号换成验证码
        NSDictionary* pdic= NSUDTakeData(UserInfo);

        _phoneNumLab.text = [NSString stringWithFormat:@"邀请码:%@",pdic[@"inviteCode"]];
        [_phoneNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(10);
//            make.bottom.equalTo(_headImgV.mas_bottom);
            make.top.equalTo(_headImgV.mas_centerY).offset(4*HWB);
        }];
    }
    return _phoneNumLab ;
}

-(UIButton *)commandCInviteCodeBTn{
    if (!_commandCInviteCodeBTn) {
        _commandCInviteCodeBTn = [UIButton new];
        [_commandCInviteCodeBTn setTitle:@"复制" forState:UIControlStateNormal];
        [_commandCInviteCodeBTn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_commandCInviteCodeBTn setBackgroundColor:[UIColor colorWithHexString:@"ea7d94"]];
        _commandCInviteCodeBTn.layer.cornerRadius = 8;
        _commandCInviteCodeBTn.layer.masksToBounds = true;
        _commandCInviteCodeBTn.titleLabel.font = [UIFont systemFontOfSize:13];
//        [_commandCInviteCodeBTn sizeToFit];
          [_bgView addSubview:_commandCInviteCodeBTn];
        [_commandCInviteCodeBTn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_phoneNumLab.mas_right).offset(10);
            make.centerY.mas_equalTo(_phoneNumLab);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(20);
        }];
        [_commandCInviteCodeBTn addTarget:self action:@selector(copyInviteCode) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return  _commandCInviteCodeBTn;

}

- (void)copyInviteCode{
       NSDictionary* pdic= NSUDTakeData(UserInfo);
    if (pdic[@"inviteCode"]) {
       [ClipboardMonitoring pasteboard:pdic[@"inviteCode"]];
        SHOWMSG(@"复制成功!")
    }else{ SHOWMSG(@"复制失败!") }
   
}


-(UIButton*)setBtn{
    if (!_setBtn) {
        _setBtn = [[UIButton alloc]init];
//        _setBtn.backgroundColor = RGBA(0, 0, 0, 0.5) ;
//        _setBtn.layer.masksToBounds = YES ;
//        _setBtn.layer.cornerRadius = 12*HWB ;
        [_setBtn setImage:[UIImage imageNamed:@"new_my_set1"] forState:(UIControlStateNormal)];
//        [_setBtn setTitle:@" 设置  ›" forState:UIControlStateNormal];
//        [_setBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10*HWB)];
//        [_setBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -4*HWB, 0, 0)];
//        _setBtn.titleLabel.font = [UIFont systemFontOfSize:12*HWB];
        ADDTARGETBUTTON(_setBtn, clickSet)
        [_bgView addSubview:_setBtn];
        [_setBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-13);
            make.centerY.equalTo(_mesgBtn.mas_centerY);
            make.width.offset(20*HWB);
            make.height.offset(19*HWB);
        }];
    }
    return _setBtn ;
}

// 设置按钮点击
-(void)clickSet{
    [MyPageJump pushNextVcByModel:nil byType:2];
}

// 进入消息界面
-(void)pushToMsg{
     [MyPageJump pushNextVcByModel:nil byType:1];
}

@end

@implementation FunctionViewCardOne

-(instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = true;
        [self makeUpSubview];
        [self layoutSubView];
        
    }
    return self;
}

- (void)makeUpSubview{
    __block NSString *todayValue,*lastDayValue,*currentMonthValue;
    
    [[LZMyCenterInfoServer shareInstance].commissionModel.monthDataAry enumerateObjectsUsingBlock:^(CmnInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.title isEqualToString:@"本月消费预估收入"]) {
            currentMonthValue = obj.value;
        }
    }];
    
    [[LZMyCenterInfoServer shareInstance].commissionModel.dayDataAry enumerateObjectsUsingBlock:^(CmnDayInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.stitle isEqualToString:@"今日"]) {
            for (CmnInfoModel *model in obj.dataAry) {
                if ([model.title isEqualToString:@"成交预估收入"]) {
                    todayValue = model.value;
                }
            }
            
        }else if ([obj.stitle isEqualToString:@"昨日"]) {
            for (CmnInfoModel *model in obj.dataAry) {
                if ([model.title isEqualToString:@"成交预估收入"]) {
                    lastDayValue = model.value;
                }
            }
          
        }
    }];
    
    self.topImageView = [UIImageView new];
    self.topImageView.image = [UIImage imageNamed:@"taobaoshouyi"];
    [self addSubview:_topImageView];
    
    self.monthMoneyLabel = [UILabel new];
    self.monthMoneyLabel.text = currentMonthValue;
    self.monthMoneyLabel.font = [UIFont systemFontOfSize:40];
    [self addSubview:_monthMoneyLabel];
    
    self.monthMoneyStringLabel = [UILabel new];
     self.monthMoneyStringLabel.font = [UIFont systemFontOfSize:20];
    self.monthMoneyStringLabel.text = @"本月预估 >";
    [self addSubview:_monthMoneyStringLabel];
    
    self.todayMoneyLabel = [UILabel new];
    self.todayMoneyLabel.text = todayValue;
    self.todayMoneyLabel.font = [UIFont systemFontOfSize:24];
    [self addSubview:_todayMoneyLabel];
    
    self.todayMoneyStringLabel = [UILabel new];
    self.todayMoneyStringLabel.text = @"今日预估收入(元)";
     self.todayMoneyStringLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_todayMoneyStringLabel];
    
    self.monthThinkMoneyLael = [UILabel new];
    self.monthThinkMoneyLael.text = lastDayValue;
    self.monthThinkMoneyLael.font = [UIFont systemFontOfSize:24];
    [self addSubview:_monthThinkMoneyLael];
    
    self.monthThinkMoneyStringLabel = [UILabel new];
    self.monthThinkMoneyStringLabel.text = @"昨日预估收入(元)";
    self.monthThinkMoneyStringLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_monthThinkMoneyStringLabel];
    
    self.lineView = [UIView new];
    _lineView.backgroundColor = [UIColor blackColor];
    [self addSubview:_lineView];
    
    self.tipImageView = [UIImageView new];
    [self addSubview:self.tipImageView];
    
    self.monthMoneyControl = ({
        UIControl *control = [[UIControl alloc] init];
        [control addTarget:self action:@selector(bgClick) forControlEvents:UIControlEventTouchUpInside];
        
        control;
    });
    
    [self addSubview:self.monthMoneyControl];
}

- (void)layoutSubView{
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(self);
        make.width.mas_equalTo(83);
        make.height.mas_equalTo(25);
    }];
    
    [self.monthMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.offset(25);
    }];
    
    [self.monthMoneyStringLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.monthMoneyLabel.mas_bottom).offset(8*HWB);
        make.centerX.equalTo(self.mas_centerX).offset(10*HWB);
    }];
    
    [self.todayMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_left).offset(74*HWB);
        make.top.offset(106*HWB);
    }];
    
    [self.todayMoneyStringLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.todayMoneyLabel.mas_bottom).offset(6.5*HWB);
        make.centerX.equalTo(self.todayMoneyLabel.mas_centerX);
    }];
    
    [self.monthThinkMoneyLael mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.todayMoneyLabel);
        make.centerX.equalTo(self.mas_right).offset(-74*HWB);
    }];
    
    [self.monthThinkMoneyStringLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.todayMoneyStringLabel.mas_top);
        make.centerX.equalTo(self.monthThinkMoneyLael.mas_centerX);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(126*HWB);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(20);
    }];
    
    [self.monthMoneyControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.bottom.equalTo(self.monthMoneyStringLabel.mas_bottom);
    }];
}

- (void)bgClick{
    MyClassModel *model = [[MyClassModel alloc] init];
    model.title = @"消费佣金";
    model.listViewType = 60;
    
    [MyPageJump pushNextVcByModel:model byType:60];
}

@end

#pragma mark ===>>> 个人中心里的三个功能自定义按钮的三个功能块
@implementation FunctionViewOne

-(void)setMyModel:(MyDynamicModel *)myModel{
    
    _myModel = myModel ;
    [_cltView reloadData];

}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = COLORWHITE ;
        self.layer.masksToBounds = YES ;
        self.layer.cornerRadius = 6*HWB ;
        self.clipsToBounds = YES ;
        
        _bgImgV = [[UIImageView alloc]init]; // 加背景图
//        _bgImgV.image = [UIImage imageNamed:@"wdbg"];
        [self addSubview:_bgImgV];
        [_bgImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [self cltView];
        
        
    }
    return self ;
}


-(UICollectionView*)cltView{
    if (!_cltView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumInteritemSpacing = 1.5 ;  layout.minimumLineSpacing = 1.5 ;
        _cltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _cltView.backgroundColor = COLORCLEAR ;
        _cltView.dataSource = self ; _cltView.delegate = self ;
        [_cltView registerClass:[FunctionOneCltCelll class] forCellWithReuseIdentifier:@"img_txt_cltcell"] ;
        [self addSubview:_cltView];
        [_cltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
    }
    return _cltView ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _myModel.cateInfoList1.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FunctionOneCltCelll * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"img_txt_cltcell" forIndexPath:indexPath];
    cell.tag = indexPath.item;
    if (_myModel.cateInfoList1.count>indexPath.item) {
        cell.myModel = _myModel.cateInfoList1[indexPath.item];
    }else{ [_cltView reloadData]; }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float wh = FloatKeepInt((ScreenW-30-_myModel.cateInfoList1.count*2)/_myModel.cateInfoList1.count);
    return CGSizeMake(wh, 70*HWB);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [MyPageJump pushNextVcByModel:_myModel.cateInfoList1[indexPath.item] byType:0];
}

@end



#pragma mark ===>>> 个人中心里的个人信息中自定义cltcell

@implementation FunctionOneCltCelll

-(void)setMyModel:(MyClassModel *)myModel{
    _myModel = myModel ;
    _titleLab.text = _myModel.title ;
    [_iconImgV sd_setImageWithURL:[NSURL URLWithString:_myModel.img] placeholderImage:_myModel.placeholderImg];
    if (self.tag==0) { _linesImgV.hidden = YES ; }else{ _linesImgV.hidden = NO ; }
    
    if (myModel.listViewType==50) {
        if ([[RCIMClient sharedRCIMClient] getTotalUnreadCount]>0) {
            _redPoint.hidden=NO;
        }else{
            _redPoint.hidden=YES;
        }
    }else{
        _redPoint.hidden=YES;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        _iconImgV = [[UIImageView alloc] init];
        _iconImgV.contentMode = UIViewContentModeScaleAspectFit ;
        _iconImgV.image = PlaceholderImg ;
        [self addSubview:_iconImgV];
        [_iconImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(2*HWB);
            make.width.offset(23*HWB); make.height.offset(23*HWB);
        }];
        
        _titleLab=[UILabel labText:@"标题标题" color:Black51 font:13*HWB];
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);  make.height.offset(20*HWB);
            make.top.equalTo(self.contentView.mas_centerY).offset(3*HWB);
        }];
        
        _linesImgV = [[UIImageView alloc]init];
        _linesImgV.backgroundColor = Black204 ;
        [self.contentView addSubview:_linesImgV];
        [_linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);     make.height.offset(38*HWB);
            make.width.offset(0.5);  make.centerY.equalTo(self.mas_centerY);
        }];
        
        _redPoint= [[UIImageView alloc]init];
        _redPoint.layer.masksToBounds = YES ;
        _redPoint.layer.cornerRadius = 4 ;
        _redPoint.backgroundColor = COLORRED ;
        [self.contentView addSubview:_redPoint];
        [_redPoint mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_iconImgV.mas_right);
            make.centerY.equalTo(_iconImgV.mas_top);
//            make.top.offset(-2);   make.right.offset(2);
            make.height.offset(8); make.width.offset(8);
        }];
    }
    return self;
}

@end




#pragma mark ===>>> 空的区头或区脚

@implementation MyInfoViewNull
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.clipsToBounds = YES ;
//        self.contentView.backgroundColor = [UIColor redColor];
    }
    return self ;
}
@end


