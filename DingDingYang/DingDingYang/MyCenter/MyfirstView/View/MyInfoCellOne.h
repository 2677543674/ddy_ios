//
//  MyInfoCellOne.h
//  DingDingYang
//
//  Created by ddy on 2018/1/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInfoCellOne : UITableViewCell

@property(nonatomic,strong) UIView * backView ;
@property(nonatomic,strong) UIImageView * bgimgView ; // 背景图
@property(nonatomic,strong) UIImageView * imgView; // 前面的图片
@property(nonatomic,strong) UILabel * titLab;      // 标题
@property(nonatomic,strong) UILabel * redLab;      // 未读消息提醒(红色圆角标示符)

@property(nonatomic,strong) MyClassModel * classModel ;
/**
 圆角类型 0:清除layer  1:上半部分  2:下半部分  3:全部
 */
@property(nonatomic,assign) NSInteger clipsType ;  // cell要加的圆角类型 1:上半部分  2:下半部分  3:全部



@end
