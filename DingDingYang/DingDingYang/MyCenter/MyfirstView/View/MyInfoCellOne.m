//
//  MyInfoCellOne.m
//  DingDingYang
//
//  Created by ddy on 2018/1/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MyInfoCellOne.h"

@implementation MyInfoCellOne

-(void)setClassModel:(MyClassModel *)classModel{
    _classModel = classModel ;
    
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_classModel.img] placeholderImage:_classModel.placeholderImg];
    _titLab.text = _classModel.title ;
}


-(void)setClipsType:(NSInteger)clipsType{
    
    @try {
        switch (clipsType) {
            case 0:{ // 无圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(-8*HWB);  make.bottom.offset(8*HWB);
                }];
            } break;
                
            case 1:{ // 1:上半部分圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);  make.bottom.offset(8*HWB);
                }];
                
            } break;
                
            case 2:{ // 2:下半部分圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(-8*HWB);  make.bottom.offset(-1.5);
                }];
            } break;
                
            case 3:{ // 3:全部圆角
                [_bgimgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);  make.bottom.offset(-1.5);
                }];
            } break;
                
            default: break;
        }
    } @catch (NSException *exception) { } @finally { }
   
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    //cell高度为 45 * HWB
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLORGROUP  ;
        _backView = [[UIView alloc]init];
        _backView.clipsToBounds = YES ;
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(15); make.right.offset(-15);
        }];
        
        _bgimgView = [[UIImageView alloc]init];
        _bgimgView.backgroundColor = COLORWHITE ;
        _bgimgView.layer.masksToBounds = YES ;
        _bgimgView.layer.cornerRadius = 6*HWB ;
        [_backView addSubview:_bgimgView];
        [_bgimgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(-1.5);
            make.left.offset(0); make.right.offset(0);
        }];
        
        _imgView = [[UIImageView alloc]init];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        [_backView addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView.mas_centerY);
            make.left.offset(10*HWB);  make.height.offset(20*HWB);
            make.width.offset(20*HWB);
        }];
        
        _titLab = [UILabel labText:@"标题" color:Black51 font:13*HWB];
        [_backView addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_imgView.mas_right).offset(10*HWB);
            make.centerY.equalTo(_backView.mas_centerY);
            make.height.offset(20);
        }];
        
        _redLab = [UILabel labText:@"" color:COLORWHITE font:12*HWB];
        _redLab.backgroundColor = COLORRED ;
        _redLab.layer.masksToBounds = YES ;
        _redLab.layer.cornerRadius = 9 ;
        _redLab.hidden = YES ;
        _redLab.textAlignment = NSTextAlignmentCenter ;
        [_backView addSubview:_redLab];
        [_redLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);  make.centerY.equalTo(_backView.mas_centerY);
            make.height.offset(18*HWB);  make.width.offset(18*HWB);
        }];
        
        UIImageView * rightImgV = [[UIImageView alloc]init];
        rightImgV.image = [UIImage imageNamed:@"next_arrow"];
        rightImgV.contentMode = UIViewContentModeScaleAspectFit ;
        [_backView addSubview:rightImgV];
        [rightImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView.mas_centerY);
            make.right.offset(-10*HWB);  make.height.offset(15*HWB);
            make.width.offset(10*HWB);
        }];
        
        
        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = COLORGROUP ;
        [_backView addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0) ;  make.right.offset(0);
            make.bottom.offset(0); make.height.offset(1.5);
        }];
        
    }return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
