//
//  MyInfoViewTwo.h
//  DingDingYang
//
//  Created by ddy on 2018/1/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInfoViewTwo : UITableViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * cltView;
@property(nonatomic,strong) MyDynamicModel * dynamicModel ;
@end


@interface MyImgTxtCltCell1 : UICollectionViewCell
@property(nonatomic,strong) UIImageView * imgView ;
@property(nonatomic,strong) UILabel * titLab ;
@property(nonatomic,strong) MyClassModel * classModel ;
@end

