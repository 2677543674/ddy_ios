//
//  MyDynamicModel.h
//  DingDingYang
//
//  Created by ddy on 2018/1/10.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyDynamicModel : NSObject

@property(nonatomic,copy) NSString * styleType ;       // 类型
@property(nonatomic,strong) NSMutableArray * cateInfoList1 ;  // 功能块1 (服务器字段)
@property(nonatomic,strong) NSMutableArray * cateInfoList2 ;  // 功能块2 (服务器字段)
// 本地字段、、、、
@property(nonatomic,strong) NSArray * cateInfoList ;   // 将有面两个字段的组合到这个字段，组成新的数据格式

-(instancetype)initWith:(NSDictionary*)dic ;

/**
 获取动态化个人中心数据

 @param mydynamicBlock  来自服务器请求并解析完成的数据
 @param defaultBlock    默认数据或缓存数据
 @param failBlock       失败原因(无太大作用)
 */
+(void)dynamicPersonalModel:(void(^)(NSMutableArray * roleAry0 , NSMutableArray * roleAry1, NSMutableArray * roleAry2))mydynamicBlock defaultModel:(void(^)(NSMutableArray * defaultrAry0 , NSMutableArray * defaultrAry1, NSMutableArray * defaultrAry2))defaultBlock fail:(void(^)(NSString*failStr))failBlock;


@end


@interface MyClassModel : NSObject
//**  标注  **//
@property(nonatomic,strong) UIImage * placeholderImg ; // 默认图片(可能是logo，或者是本地图片，用defaultImg字段来判断)
@property(nonatomic,assign) NSInteger listViewType ; // 跳转页面的类型
@property(nonatomic,copy) NSString * img ;           // 要显示的网络图片
@property(nonatomic,copy) NSString * title ;         // 标题
@property(nonatomic,copy) NSString * defaultImg ;    // 默认图片名称(服务器返回的字段，此处用来当做本地默认字段使用)
@property(nonatomic,copy) NSString * mid ;
@property(nonatomic,copy) NSString * link ;
@property(nonatomic,copy) NSString * name ;
@property(nonatomic,copy) NSString * openType ;
@property(nonatomic,copy) NSString * params ;
@property(nonatomic,copy) NSString * platform ;
@property(nonatomic,copy) NSString * status ;
@property(nonatomic,copy) NSString * subtitle ;

-(instancetype)initWith:(NSDictionary*)dic ;
@end

/*
 
 个人中心本地默认数据 defaultImg:图片名   listViewType:跳转类型    title:标题
 
 @{@"defaultImg":@"my_def_qian_bao",@"listViewType":@"58",@"title":@"钱包"},
 @{@"defaultImg":@"my_def_tgyj",@"listViewType":@"60",@"title":@"推广佣金"},
 @{@"defaultImg":@"my_def_tuan_dui",@"listViewType":@"50",@"title":@"团队"}
 @{@"defaultImg":@"my_def_tb_buy_car",@"listViewType":@"61",@"title":@"淘宝购物车"},
 @{@"defaultImg":@"my_def_share_app",@"listViewType":@"51",@"title":@"分享APP"},
 @{@"defaultImg":@"my_def_share_poster",@"listViewType":@"62",@"title":@"APP海报分享"},
 @{@"defaultImg":@"my_def_join",@"listViewType":@"52",@"title":@"申请加入"},
 @{@"defaultImg":@"my_def_my_order",@"listViewType":@"67",@"title":@"福利订单"},
 @{@"defaultImg":@"my_def_phb",@"listViewType":@"53",@"title":@"排行榜"}
 @{@"defaultImg":@"my_def_video_teach",@"listViewType":@"63",@"title":@"视频教程"},
 @{@"defaultImg":@"my_def_call_center",@"listViewType":@"54",@"title":@"联系客服"},
 @{@"defaultImg":@"my_def_about",@"listViewType":@"56",@"title":@"关于我们"}
 @{@"defaultImg":@"my_def_help",@"listViewType":@"55",@"title":@"帮助中心"},
 
 
 */
















