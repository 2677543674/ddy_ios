//
//  MyPageJump.h
//  DingDingYang
//
//  Created by ddy on 2018/1/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@class MyClassModel ;

@interface MyPageJump : NSObject


/**
 个人中心跳转页面

 @param mcModel 个人中心动态数据model
 @param type 本地跳转填写的类型，mcModel为空必须填写type，不然不跳转
 */
+(void)pushNextVcByModel:(MyClassModel*)mcModel byType:(NSInteger)type;

///**
// 检测更新
//
// @param stateBlock  
// */
//+(void)checkAppUpdateBlock:(void(^)( NSInteger stateCode ,NSString*downLoadUrl))stateBlock;


/**
 版本检测
 
 @param stateBlock
 
 stateCode：状态值   downLoadUrl:下载链接或错误说明   updateContent:更新说明
 1: 发现新版，只提示更新
 10: 发现新版，需要强制更新
 2: 当前已是最新版本
 3: 无法检测更新或检查更新失败
 
 4: 当前为测试版或等待审核版
 5: APP已经审核成功，本地服务器还未修改版本号
 
 */
+(void)newcheckAppUpdateBlock:(void(^)(NSInteger stateCode,NSString * downLoadUrl,NSString*updateContent,NSString*newVersion))stateBlock;


+(void)withdrawalType:(NSInteger)type platform:(NSInteger)platform totalAmount:(NSString*)amount results:(void(^)(NSInteger result))resultBlock;

@end
