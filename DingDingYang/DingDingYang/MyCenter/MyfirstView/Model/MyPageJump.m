//
//  MyPageJump.m
//  DingDingYang
//
//  Created by ddy on 2018/1/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MyPageJump.h"

#import "TestTheEntranceVC.h"

#import "TeamRewardsVC.h"     // 团队奖励(代理佣金)
#import "NewTeamVC.h"         // 新的团队
#import "NewOrmdVC.h"         // 新的官方推荐
#import "NewShareAppVC.h"     // 新的分享app
#import "NewCallCeterVC.h"    // 新的联系客服
#import "NewAboutVC.h"        // 新的关于我们
#import "HelpCenterVC.h"      // 帮助中心
#import "CommissionVC.h"      // 新的消费佣金 (京东和淘宝，有选项卡的)
#import "PostersVC.h"
#import "RankingListVC.h"
#import "MyWalletVC.h"
#import "UpgradeVC.h"
#import "MyInfoSetVc.h"
#import "MomentsVC.h"
#import "UnGoodsListVC.h"
#import "MyNewWalletVC.h"
#import "WPHGoodsListVC.h"
#import "UpgradeBdPhoneVC.h"   // 升级代理绑定手机号界面，蜜赚的
#import "BangDingPhoneVC.h"    // 绑定手机号(小程序用户)
#import "InformationEditingVC.h" // 昵称或支付宝账号修改
#import "NewWithdrawalVC.h"    // 提现页面
#import "OperatorTeamListVC.h" // 省见的运营商列表
#import "OperatorEarningsVC.h" // 省见的运营商收益
#import "ContactUsVC.h"        // 客服中心


@implementation MyPageJump

+(void)pushNextVcByModel:(MyClassModel*)mcModel byType:(NSInteger)type{
    
    if (TOKEN.length==0) {
        [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) { }];
        return ;
    }
    
    // 跳转类型，兼容服务器和本地
    NSInteger  pushType = type ;
    if (mcModel) { pushType = mcModel.listViewType ; }
    
    // 从缓存读取个人信息
    PersonalModel * pmodel ;
    if (TOKEN.length>0&&NSUDTakeData(UserInfo)!=nil) {
        NSDictionary * dic = NSUDTakeData(UserInfo) ;
        pmodel = [[PersonalModel alloc]initWithDic:dic];
    }else{
        pmodel = [[PersonalModel alloc]init];
    }
    NSLog(@"%@",mcModel.title);
    
    // 蜜赚、点击设置进入关于我们，关于的标题改为设置
    if (PID==10104&&type==2) { pushType = 56; }
    
    switch (pushType) { // 判断类型
            
        case 1:{ // 系统消息
            
            MessageVC * msgvc = [[MessageVC alloc]init];
            msgvc.modelP = pmodel;
            msgvc.msgtype = 0 ;
            msgvc.hidesBottomBarWhenPushed=YES;
            [TopNavc pushViewController:msgvc animated:YES];
            
        } break;
            
        case 2:{ // 设置
            
            MyInfoSetVc * infoVC = [[MyInfoSetVc alloc]init];
            infoVC.model = pmodel;
            infoVC.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:infoVC animated:YES];
            
        } break;
            
            
        case 21 :{ // 运营商团队列表(省见)
            OperatorTeamListVC * otlVC = [[OperatorTeamListVC alloc]init];
            otlVC.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:otlVC animated:YES];
        } break;
            
        case 22 :{ // 运营商团队收益(省见)
            OperatorEarningsVC * earningVC = [[OperatorEarningsVC alloc]init];
            earningVC.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:earningVC animated:YES];
        } break;
            
        case 23 :{ // 运营商分公司(省见)
            if (mcModel.link.length > 0) {
                [WebVC pushWebViewUrlOrStr:mcModel.link title:mcModel.title isSHowType:0];
            }
//            NSString * url = @"http://120.77.226.151:8061/app/company/login.api";
        } break;
            
        case 50:{ // 我的团队
            NewTeamVC * teamVC = [NewTeamVC new];
            teamVC.modelTitle=mcModel.title;
            teamVC.pModel = pmodel ;
            teamVC.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:teamVC animated:YES];
            
        } break;
            
        case 51:{ // 分享好友
            
            NewShareAppVC * shareVC = [[NewShareAppVC alloc]init];
            shareVC.modelTitle=mcModel.title;
            shareVC.model = pmodel;
            shareVC.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:shareVC animated:YES];
            
        } break;
            
        case 52:{ // 加入代理 (有小程序的app需要判断有没有绑定手机号)
            
            // 省见、一秒券、叮当、叮叮羊有小程序
            if (PID==10025||PID==10022||PID==10102||PID==10107) {
                if (pmodel.mobile.length>0) {
                    UpgradeVC * upvc = [[UpgradeVC alloc]init];
                    upvc.modelTitle=mcModel.title;
                    upvc.hidesBottomBarWhenPushed = YES ;
                    [TopNavc pushViewController:upvc animated:YES];
                }else{
                    UpgradeBdPhoneVC * upbdvc = [[UpgradeBdPhoneVC alloc]init];
                    upbdvc.hidesBottomBarWhenPushed = YES ;
                    [TopNavc pushViewController:upbdvc animated:YES];
                }
            }else{
                UpgradeVC * upvc = [[UpgradeVC alloc]init];
                upvc.modelTitle = mcModel.title;
                upvc.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:upvc animated:YES];
            }
            
        } break;
            
        case 53:{ // 排行榜(重写了)
            
            RankingListVC * rklvc = [[RankingListVC alloc]init];
            rklvc.modelTitle = mcModel.title;
            rklvc.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:rklvc animated:YES];
            
        } break;
            
        case 54:{ // 客服中心
            if (PID==10107) { // 叮叮羊用新的
                ContactUsVC * cusVc = [[ContactUsVC alloc]init];
                cusVc.modelTitle=mcModel.title;
                cusVc.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:cusVc animated:YES];
            }else{
                NewCallCeterVC * callcenter = [[NewCallCeterVC alloc]init];
                callcenter.modelTitle=mcModel.title;
                callcenter.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:callcenter animated:YES];
            }
        } break;
            
        case 55:{ // 帮助说明
            
            HelpCenterVC* help=[HelpCenterVC new];
            help.modelTitle=mcModel.title;
            help.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:help animated:YES];
            
        } break;
            
        case 56:{ // 关于我们
            
            NewAboutVC * about = [[NewAboutVC alloc]init];
            about.modelTitle=mcModel.title;
            about.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:about animated:YES];
            
        } break;
            
        case 57:{ // 宝贝收藏
            
            
        } break;
            
        case 58:{ // 我的钱包
//#if DEBUG
            MyNewWalletVC * walletVC = [[MyNewWalletVC alloc]init];
            walletVC.modelTitle=mcModel.title;
            walletVC.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:walletVC animated:YES];
//#else
//            MyWalletVC * walletVC = [[MyWalletVC alloc]init];
//            walletVC.modelTitle=mcModel.title;
//            walletVC.hidesBottomBarWhenPushed = YES;
//            walletVC.modelInfo = pmodel;
//            [TopNavc pushViewController:walletVC animated:YES];
//#endif
            
        } break;
            
        case 59:{ // 团队佣金奖励
            
            TeamRewardsVC * teamR = [[TeamRewardsVC alloc]init];
            teamR.model = pmodel;
            teamR.modelTitle=mcModel.title;
            teamR.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:teamR animated:YES];
            
        } break;
            
        case 60:{ // 消费佣金
            
            CommissionVC * consumvc = [[CommissionVC alloc]init];
            consumvc.modelInfo = pmodel ;
            consumvc.modelTitle=mcModel.title;
            consumvc.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:consumvc animated:YES];
            
            
        } break;
            
        case 61:{ // 购物车
            [[ALBBSDK sharedInstance] ALBBSDKInit];
            
            if ([[ALBBSession sharedInstance] isLogin]) {
                [TradeWebVC showCustomBaiChuan:@"" url:@"" pagetype:4];
            }else{
                ALBBSDK * albbSDK = [ALBBSDK sharedInstance];
                [albbSDK setAppkey:TBKEY];
                [albbSDK setAuthOption:NormalAuth];
                [albbSDK auth:TopNavc.topViewController successCallback:^(ALBBSession *session){
                    [TradeWebVC showCustomBaiChuan:@"" url:@"" pagetype:4];
                } failureCallback:^(ALBBSession *session,NSError *error){
                    NSLog(@"session == %@,\n error == %@",session,error);
                    SHOWMSG(@"授权失败或已取消授权!");
                    
                }];
            }
    
        } break;
            
        case 62:{ // 分享海报
            
            PostersVC * poster = [[PostersVC alloc]init];
            poster.modelTitle=mcModel.title;
            poster.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:poster animated:YES];
            
        } break;
            
        case 63:{ // 视频教程
            
            
        } break;
            
        case 64:{ // 新手上路
            
            NewRoadVC * onroad = [[NewRoadVC alloc]init];
//            onroad.mcsModel=mcModel;
            onroad.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:onroad animated:YES];
            
        } break;
            
        case 65:{ // 官方推荐
            NewOrmdVC * ormdVC = [[NewOrmdVC alloc]init];
            ormdVC.modelTitle=mcModel.title;
            ormdVC.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:ormdVC animated:YES];
        } break;
            
        case 66:{ // 福利商城
            
            FreeOrderVC * freeOrderVC = [[FreeOrderVC alloc]init];
            freeOrderVC.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:freeOrderVC animated:YES];
            
        } break;
            
        case 67:{ // 福利订单
            
            MyWelfareVC *  welfareVC = [[MyWelfareVC alloc]init];
            welfareVC.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:welfareVC animated:YES];

        } break;
            
        case 68:{ // 朋友圈
            
            MomentsVC * mvc = [MomentsVC new];
            mvc.mtitle=mcModel.title;
            mvc.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:mvc animated:YES];
            
        } break;
            
        case 69:{ // 拼多多
            
            UnGoodsListVC* mvc=[UnGoodsListVC new];
            mvc.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:mvc animated:YES];
            
        } break;
            
        case 70:{ // 唯品会
            
            WPHGoodsListVC * uglvc = [[WPHGoodsListVC alloc]init];
            uglvc.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:uglvc animated:YES];
            
        } break;
            
            
        case 1000:{ // 个人中心测试入口
            
            TestTheEntranceVC * testVC = [[TestTheEntranceVC alloc]init];
            testVC.infoModel = pmodel ;
            testVC.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:testVC animated:YES];
            
        } break;
            
        default:{
            SHOWMSG(@"该功能还未上线哦，请等待版本更新!")
        } break;
            
    }
    
    
}


/**
 提现

 @param type 1:代理费提现(佣金余额提现) 2:消费佣金提现  3:拉新余额提现
 @param platform 1:淘宝 2:京东 3:拼多多 4:唯品会 5:综合平台 6:美团 55:淘宝运营商佣金提现 (type为2才用平台判断)
 @param amount 消费佣金或代理佣金的剩余总余额
 @param resultBlock  提现结果回调 1:提现成功
 */
+(void)withdrawalType:(NSInteger)type platform:(NSInteger)platform totalAmount:(NSString*)amount results:(void(^)(NSInteger result))resultBlock{
    if (TOKEN.length>0) {
        PersonalModel * model = [[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
        if (model.mobile.length>0) { // 有手机号
            if (model.aliNo.length>0) { // 有支付宝账号
                
                if ([amount floatValue]>0) {
                    // 然后根据提现类型和剩余金额检测是否可以提现
                    MBShow(@"正在检测...")
                    NSString * checkWdUrl = type==1?url_check_wd_daiLi:url_check_wd_consumer;
                    NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
                    if (type==3) { pdic[@"type"] = @"42"; }
                    if (type==2) {
                        switch (platform) {
                            case 2:{ pdic[@"type"] = @"2"; } break;
                            case 3:{ pdic[@"type"] = @"3"; } break;
                            case 4:{ pdic[@"type"] = @"4"; } break;
                            case 51:{ pdic[@"type"] = @"51"; } break;
                            case 6:{ pdic[@"type"] = @"60"; } break;
                            case 55:{ pdic[@"type"] = @"55"; } break;
                            default:{ } break;
                        }
                    }
                    
                    if ([pdic allKeys].count>0) { checkWdUrl = url_check_wd; }
                    [NetRequest requestType:0 url:checkWdUrl ptdic:pdic success:^(id nwdic) {
                        MBHideHUD
                        NSInteger ifWd = [REPLACENULL(nwdic[@"ifWd"]) integerValue];
                        if ([checkWdUrl isEqualToString:url_check_wd]) {
                            ifWd = [REPLACENULL(nwdic[@"flag"]) integerValue];
                        }
                        if (ifWd==1) { // 可以提现
                            NewWithdrawalVC * newWdVC = [NewWithdrawalVC new];
                            newWdVC.withdrawalType = type;
                            newWdVC.platform = platform;
                            newWdVC.money = amount;
                            [TopNavc pushViewController:newWdVC animated:YES];
                            newWdVC.WithdrawalResultBlock = ^(NSInteger resultType) {
                                if (resultType==1) { resultBlock(1); }
                            };
                            
                        }else{ SHOWMSG(REPLACENULL(nwdic[@"msg"])) }
                    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                        MBHideHUD  SHOWMSG(failure)
                    }];
                }else{ SHOWMSG(@"余额不足!") }
                
            }else{ // 去绑定支付宝账号
                
                InformationEditingVC * alipayNoVC = [[InformationEditingVC alloc]init];
                alipayNoVC.infoModel = model; alipayNoVC.index = 2;
                alipayNoVC.hidesBottomBarWhenPushed = YES;
                [TopNavc pushViewController:alipayNoVC animated:YES];
            }
        }else{ // 去绑定手机号
            if (PID==10104) { // 蜜赚
                UpgradeBdPhoneVC * upbdvc = [[UpgradeBdPhoneVC alloc]init];
                upbdvc.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:upbdvc animated:YES];
            }else{
                BangDingPhoneVC * bdPhoneVC = [[BangDingPhoneVC alloc]init];
                bdPhoneVC.BdResultBlock = ^(BOOL bdResult) { NSLog(@"绑定成功"); };
                [TopNavc pushViewController:bdPhoneVC animated:YES];
            }
        }
    }
}

/*
+(void)checkAppUpdateBlock:(void (^)(NSInteger, NSString *))stateBlock{
    
    if (APPLEID.length>0) {
        NSString *urlStr = FORMATSTR(@"https://itunes.apple.com/cn/lookup?id=%@",APPLEID) ;
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager POST:urlStr parameters:nil progress:nil success:^(NSURLSessionDataTask *  task, id   responseObject) {
            NSString*str=[[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            LOG(@"检测更新返回的JSON:\n\n%@\n",str)
            NSDictionary * appInfoDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            NSArray * resultArray = NULLARY([appInfoDict objectForKey:@"results"]);
            if (resultArray.count>0) {
                NSDictionary * dic = [resultArray lastObject];
                NSString * uploadUrl = REPLACENULL(dic[@"trackViewUrl"]);
                NSString * appStoreVersion = REPLACENULL(dic[@"version"]);
                NSString * bundleId_apple = REPLACENULL(dic[@"bundleId"]);
                if (appStoreVersion.length>0) {
                    // bundleId一致 并且 App Store的版本号大于本地的版本号，才允许提示更新，避免更新成其他app的链接
                    NSString * bundleId_local = [[NSBundle mainBundle] bundleIdentifier];
                    NSString * currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
                    if ([bundleId_local isEqual:bundleId_apple]) {
                        if ([appStoreVersion compare:currentVersion]==1) {
                            
                           stateBlock(1,uploadUrl);
                        }else {
                           stateBlock(2,@"当前已是最新版本!");
                        }
                    }else{
                        stateBlock(3,@"应用ID跟当前app不匹配!");
                    }
                }else{
                    stateBlock(3,@"最新版本号获取失败!");
                }
            }else{
                stateBlock(3,@"APP未上架或已暂时下架!");
            }
            
        } failure:^(NSURLSessionDataTask *  task, NSError *  error) {
            stateBlock(3,@"检测失败，请重试!");
        }];
    }else{
        stateBlock(3,@"应用ID未设置，不能检测更新!");
    }
    
}
*/


+(void)newcheckAppUpdateBlock:(void (^)(NSInteger, NSString *,NSString*,NSString*))stateBlock{
    
    if (APPLEID.length>0) {
        NSString * urlStr = FORMATSTR(@"https://itunes.apple.com/cn/lookup?id=%@",APPLEID);
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        LOG(@"App信息获取接口: %@",urlStr)
        // 苹果服务器
        [manager POST:urlStr parameters:nil progress:nil success:^(NSURLSessionDataTask *  task, id   responseObject) {
            NSString * str = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            LOG(@"app再App Store中的内容:\n%@",str)
            NSDictionary * appInfoDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            NSArray * resultArray = NULLARY([appInfoDict objectForKey:@"results"]);
            
            // 本地版本和app服务器对比
            [NetRequest requestType:0 url:url_update_version ptdic:nil success:^(id nwdic) {
                // 本地服务器参数
//                NSString * newVersion = REPLACENULL(nwdic[@"lastest"]);
//                NSString * newContent = REPLACENULL(nwdic[@"content"]);
//                NSString * updateUrl = REPLACENULL(nwdic[@"iosdownload"]);
                NSInteger upgradeForce = [REPLACENULL(nwdic[@"upgradeForce"]) integerValue];
                
                // App Store参数(若数组为空则是app被下架了)
                if (resultArray.count>0) {
                    NSDictionary * dic = [resultArray lastObject];
                    NSString * uploadUrl = REPLACENULL(dic[@"trackViewUrl"]);
                    NSString * appStoreVersion = REPLACENULL(dic[@"version"]);
                    NSString * bundleId_apple = REPLACENULL(dic[@"bundleId"]);
                    NSString * updateContent = REPLACENULL(dic[@"releaseNotes"]);
                    
                    if (updateContent.length==0) { updateContent = @"发现新版本！"; }
                    
                    if (appStoreVersion.length>0) {
                        // bundleId一致 并且 App Store的版本号大于本地的版本号，才允许提示更新
                        NSString * bundleId_local = [[NSBundle mainBundle] bundleIdentifier];
                        NSString * currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
                        
                        if ([bundleId_local isEqual:bundleId_apple]) {
                            // 本地版本 和 App Store 版本对比
                            NSInteger result = [appStoreVersion compare:currentVersion];
                            switch (result) {
                                // 有更新
                                case 1:{ stateBlock(upgradeForce,uploadUrl,updateContent,appStoreVersion); } break;
                                case 0:{ stateBlock(2,@"当前已是最新版本!",@"",@""); } break;
                                // 本地版本号大于线上版本，说明是还未上线的测试版
                                case -1:{ stateBlock(4,@"当前版本还未更新",@"",@""); } break;
                                    
                                default: break;
                            }
                        }else{ stateBlock(3,@"应用ID跟当前app不匹配!",@"",@""); }
                    }else{ stateBlock(3,@"最新版本号获取失败!",@"",@""); }
                    
                }else{ stateBlock(6,@"app已暂时下架",@"",@""); }
                
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                //本地版本服务器异常，应显示生活和新闻
                stateBlock(5,failure,@"",@"");
            }];
            
        } failure:^(NSURLSessionDataTask *  task, NSError *  error) {
            // 获取苹果服务器失败
            stateBlock(3,@"检测失败，请重试!",@"",@"");
        }];
        
    }else{ stateBlock(3,@"应用ID未设置，不能检测更新!",@"",@""); }
    
}



@end
