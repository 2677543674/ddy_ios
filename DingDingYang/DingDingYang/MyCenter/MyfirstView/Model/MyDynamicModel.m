//
//  MyDynamicModel.m
//  DingDingYang
//
//  Created by ddy on 2018/1/10.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MyDynamicModel.h"


@implementation MyDynamicModel


+(void)dynamicPersonalModel:(void (^)(NSMutableArray *, NSMutableArray *, NSMutableArray *))mydynamicBlock defaultModel:(void (^)(NSMutableArray *, NSMutableArray *, NSMutableArray *))defaultBlock fail:(void (^)(NSString *))failBlock{
    // 首次调用，先返回默认的(或者缓存的)、
    [self parsingData:nil data:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1,NSMutableArray *roleAry2) {
        defaultBlock(roleAry0,roleAry1,roleAry2);
    } fail:^(NSString *str) {  }];
    
    // 判断是否有缓存，是否时间已过一天、过了一天才会去读新的数据
    if (NSUDTakeData(app_dynamic_my_dic)!=nil) {
        NSString * now_time = NowTimeByFormat(@"yyyy-MM-dd") ;
        NSString * last_time = REPLACENULL(NSUDTakeData(app_dynamic_my_time));
        NSInteger result = [now_time compare:last_time];
        if (result!=0) {
            [self getModulePersonalInfoDataAry:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1, NSMutableArray *roleAry2) {
                mydynamicBlock(roleAry0,roleAry1,roleAry2);
            } fail:^(NSString *str) {  }];
        }
    }else{
        // 无缓存，直接请求网路
        [self getModulePersonalInfoDataAry:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1, NSMutableArray *roleAry2) {
            mydynamicBlock(roleAry0,roleAry1,roleAry2);
        } fail:^(NSString *str) { }];
    }
#if DEBUG
    [self getModulePersonalInfoDataAry:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1, NSMutableArray *roleAry2) {
        mydynamicBlock(roleAry0,roleAry1,roleAry2);
    } fail:^(NSString *str) { }];
#endif
}

+(void)getModulePersonalInfoDataAry:(void(^)(NSMutableArray * roleAry0 , NSMutableArray * roleAry1, NSMutableArray * roleAry2))dataBlock fail:(void(^)(NSString*str))fail{
    
    [NetRequest requestType:0 url:url_module_personal ptdic:nil success:^(id nwdic) {
        
        NSArray * ary1 = NULLARY(nwdic[@"list0"][@"list"]);
        NSArray * ary2 = NULLARY(nwdic[@"list1"][@"list"]);
        NSArray * ary3 = NULLARY(nwdic[@"list3"][@"list"]);
        
        if (ary1.count>0||ary2.count>0||ary3.count>0) {
            // 有数据，存储个人中心动态数据
            NSUDSaveData(nwdic,app_dynamic_my_dic)
            NSString * time = NowTimeByFormat(@"YYYY-MM-dd") ;
            NSUDSaveData(time, app_dynamic_my_time)
            
            [self parsingData:nwdic data:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1 , NSMutableArray *roleAry2) {
                dataBlock(roleAry0,roleAry1,roleAry2);
            } fail:^(NSString *str) {  }];
            
        }else{
            
            // 网络获取失败，先读缓存，无缓存读默认
            [self parsingData:nil data:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1 , NSMutableArray *roleAry2) {
                dataBlock(roleAry0,roleAry1,roleAry2);
            } fail:^(NSString *str) {  }];
            
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        // 网络获取失败，先读缓存，无缓存读默认
        [self parsingData:nil data:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1 , NSMutableArray *roleAry2) {
            dataBlock(roleAry0,roleAry1,roleAry2);
        } fail:^(NSString *str) {  }];
    }];
    
}


+(void)parsingData:(NSDictionary*)dic data:(void(^)(NSMutableArray * roleAry0 , NSMutableArray * roleAry1, NSMutableArray *roleAry2))dataBlock fail:(void(^)(NSString*str))fail{
    
    NSMutableArray * roleAry0 = [NSMutableArray array];
    NSMutableArray * roleAry1 = [NSMutableArray array];
    NSMutableArray * roleAry2 = [NSMutableArray array];

    NSDictionary * ndic = dic ;
    if (ndic==nil) {
        if (NSUDTakeData(app_dynamic_my_dic)!=nil) {
            ndic =  NSUDTakeData(app_dynamic_my_dic) ;
        }else{
            //(本地无数据存储，网络又获取失败)  执行默认的样式、提前在本地写好字典格式，对应服务器json格式
           ndic = [self defaultStyle];
        }
    }
    
    NSArray * list0 = NULLARY(ndic[@"list0"][@"list"]) ;
    [list0 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MyDynamicModel * model0 = [[MyDynamicModel alloc]initWith:obj];
        if (model0.cateInfoList1.count>0) {
            model0.cateInfoList = (NSArray*)model0.cateInfoList1;
            [roleAry0 addObject:model0];
        }
        if (model0.cateInfoList2.count>0) {
            MyDynamicModel * model0_2 = [[MyDynamicModel alloc]initWith:obj];
            model0_2.cateInfoList = (NSArray*)model0_2.cateInfoList2;
            [roleAry0 addObject:model0_2];
        }
    }];
    
    NSArray * list1 = NULLARY(ndic[@"list1"][@"list"]) ;
    [list1 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MyDynamicModel * model1 = [[MyDynamicModel alloc]initWith:obj];
        if (model1.cateInfoList1.count>0) {
            model1.cateInfoList = (NSArray*)model1.cateInfoList1;
            [roleAry1 addObject:model1];
        }
        if (model1.cateInfoList2.count>0) {
            MyDynamicModel * model1_2 = [[MyDynamicModel alloc]initWith:obj];
            model1_2.cateInfoList = (NSArray*)model1_2.cateInfoList2;
            [roleAry1 addObject:model1_2];
        }
    }];
    
    NSArray * list2 = NULLARY(ndic[@"list2"][@"list"]) ;
    [list2 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MyDynamicModel * model2 = [[MyDynamicModel alloc]initWith:obj];
        if (model2.cateInfoList1.count>0) {
            model2.cateInfoList = (NSArray*)model2.cateInfoList1;
            [roleAry2 addObject:model2];
        }
        if (model2.cateInfoList2.count>0) {
            MyDynamicModel * model2_2 = [[MyDynamicModel alloc]initWith:obj];
            model2_2.cateInfoList = (NSArray*)model2_2.cateInfoList2;
            [roleAry2 addObject:model2_2];
        }
    }];
    
    // 无运营商的暂时用代理的
    if (roleAry2.count==0) { [roleAry2 addObjectsFromArray:roleAry1]; }
    
    if (roleAry0.count>=2||roleAry1.count>=2||roleAry2.count>=2) {
        dataBlock(roleAry0,roleAry1,roleAry2);
    }else{ fail(@"解析失败了!"); }
    
}

+(NSDictionary*)defaultStyle{
 
    // 消费者显示
    NSDictionary * dicRole0 =
    @{@"list":@[@{@"styleType":@"2-3-1-1",
                  @"cateInfoList":@[
                          @{@"defaultImg":@"my_def_qian_bao",@"listViewType":@"58",@"title":@"钱包"},
                          @{@"defaultImg":@"my_def_tgyj",@"listViewType":@"60",@"title":@"推广奖励"},
                          @{@"defaultImg":@"my_def_my_order",@"listViewType":@"67",@"title":@"福利订单"} ],
                  },
                @{@"styleType":@"2-5-1-1",
                  @"cateInfoList":@[
                          @{@"defaultImg":@"my_def_join",@"listViewType":@"52",@"title":@"申请加入"},
                          @{@"defaultImg":@"my_def_share_app",@"listViewType":@"51",@"title":@"分享APP"},
                          @{@"defaultImg":@"my_def_share_poster",@"listViewType":@"62",@"title":@"APP海报分享"}]
                  },
                @{@"styleType":@"2-5-1-1",
                  @"cateInfoList":@[
                          @{@"defaultImg":@"my_def_call_center",@"listViewType":@"54",@"title":@"联系客服"},
                          @{@"defaultImg":@"my_def_about",@"listViewType":@"56",@"title":@"关于我们"} ],
                  }
                ]
      };
    
    // 代理商显示
    NSDictionary * dicRole1 =
    @{@"list":@[@{@"styleType":@"2-3-1-1",
                  @"cateInfoList":@[
                          @{@"defaultImg":@"my_def_qian_bao",@"listViewType":@"58",@"title":@"钱包"},
                          @{@"defaultImg":@"my_def_tgyj",@"listViewType":@"60",@"title":@"推广佣金"},
                          @{@"defaultImg":@"my_def_tuan_dui",@"listViewType":@"50",@"title":@"团队"} ],
                  @"cateInfoList1":@[]
                  },
                
                @{@"styleType":@"2-5-1-1",
                  @"cateInfoList": @[
                          @{@"defaultImg":@"my_def_share_app",@"listViewType":@"51",@"title":@"分享APP"},
                          @{@"defaultImg":@"my_def_share_poster",@"listViewType":@"62",@"title":@"APP海报分享"},
                          @{@"defaultImg":@"my_def_my_order",@"listViewType":@"67",@"title":@"福利订单"}]
                  },
                
                @{@"styleType":@"2-5-1-1",
                  @"cateInfoList":@[
                          @{@"defaultImg":@"my_def_call_center",@"listViewType":@"54",@"title":@"联系客服"},
                          @{@"defaultImg":@"my_def_about",@"listViewType":@"56",@"title":@"关于我们"}]
                  }
                ]
      };
    
    // 运营商显示
    NSDictionary * dicRole2 =
    @{@"list":@[@{@"styleType":@"2-3-1-1",
                  @"cateInfoList":@[
                          @{@"defaultImg":@"my_def_qian_bao",@"listViewType":@"58",@"title":@"钱包"},
                          @{@"defaultImg":@"my_def_tgyj",@"listViewType":@"60",@"title":@"推广佣金"},
                          @{@"defaultImg":@"my_def_tuan_dui",@"listViewType":@"50",@"title":@"团队"} ],
                  @"cateInfoList1":@[]
                  },
                
                @{@"styleType":@"2-5-1-1",
                  @"cateInfoList": @[
                          @{@"defaultImg":@"my_def_share_app",@"listViewType":@"51",@"title":@"分享APP"},
                          @{@"defaultImg":@"my_def_share_poster",@"listViewType":@"62",@"title":@"APP海报分享"},
                          @{@"defaultImg":@"my_def_my_order",@"listViewType":@"67",@"title":@"福利订单"}]
                  },
                
                @{@"styleType":@"2-5-1-1",
                  @"cateInfoList":@[
                          @{@"defaultImg":@"my_def_call_center",@"listViewType":@"54",@"title":@"联系客服"},
                          @{@"defaultImg":@"my_def_about",@"listViewType":@"56",@"title":@"关于我们"}]
                  }
                ]
      };
        
    NSDictionary * defaultStyle = @{@"list0":dicRole0,@"list1":dicRole1,@"list2":dicRole2};
    
    return defaultStyle;
}


#pragma mark ===>>> 个人中心动态化: 模块类型、 消费者model、代理商model
-(instancetype)initWith:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        
        _styleType = REPLACENULL(dic[@"styleType"]) ;
        
        _cateInfoList1 = [NSMutableArray array];
        _cateInfoList2 = [NSMutableArray array];
        
        NSArray * mtAry1 = NULLARY(dic[@"cateInfoList"]);
        NSArray * mtAry2 = NULLARY(dic[@"cateInfoList1"]);
        
        [mtAry1 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MyClassModel * model1 = [[MyClassModel alloc]initWith:obj];
            [_cateInfoList1 addObject:model1];
        }];
        [mtAry2 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MyClassModel * model2 = [[MyClassModel alloc]initWith:obj];
            [_cateInfoList2 addObject:model2];
        }];
        
    }
    return self ;
}


@end




#pragma mark ===>>> 个人中心动态化: 名称、图标、跳转页面type
@implementation MyClassModel

-(instancetype)initWith:(NSDictionary*)dic{
    self = [super init];
    if (self) {

        self.mid          = REPLACENULL(dic[@"id"]);
        
        NSString * imgurl = REPLACENULL(dic[@"newImgs"]);
        if (imgurl.length>0) {
            self.img = [imgurl hasPrefix:@"http"]?imgurl:FORMATSTR(@"%@%@",dns_dynamic_loadimg,imgurl);
        }else{ self.img = @"" ; }
       
        self.link         = REPLACENULL(dic[@"link"]);
        self.listViewType = [REPLACENULL(dic[@"listViewType"]) integerValue];
        self.name         = REPLACENULL(dic[@"name"]);
        self.openType     = REPLACENULL(dic[@"openType"]);
        self.params       = REPLACENULL(dic[@"params"]);
        self.platform     = REPLACENULL(dic[@"platform"]);
        self.status       = REPLACENULL(dic[@"status"]);
        self.subtitle     = REPLACENULL(dic[@"subtitle"]);
        self.title        = REPLACENULL(dic[@"title"]);
        
        self.defaultImg   = REPLACENULL(dic[@"defaultImg"]) ;

        self.placeholderImg = PlaceholderImg ;

        if (![_img hasPrefix:@"http"]) {
            if (self.defaultImg.length>0) {
                self.placeholderImg = [UIImage imageNamed:self.defaultImg];
            }
        }else{
            switch (_listViewType) { // 服务器图片拉取失败，则使用本地默认
                case 50:{ self.placeholderImg = [UIImage imageNamed:@"my_def_tuan_dui"]; } break;
                case 51:{ self.placeholderImg = [UIImage imageNamed:@"my_def_share_app"]; } break;
                case 52:{ self.placeholderImg = [UIImage imageNamed:@"my_def_join"]; } break;
                case 53:{ self.placeholderImg = [UIImage imageNamed:@"my_def_phb"]; } break;
                case 54:{ self.placeholderImg = [UIImage imageNamed:@"my_def_call_center"]; } break;
                case 55:{ self.placeholderImg = [UIImage imageNamed:@"my_def_help"]; } break;
                case 56:{ self.placeholderImg = [UIImage imageNamed:@"my_def_about"]; } break;
//                case 57:{ self.placeholderImg = [UIImage imageNamed:@""]; } break; // 宝贝收藏
                case 58:{ self.placeholderImg = [UIImage imageNamed:@"my_def_qian_bao"]; } break;
                case 59:{ self.placeholderImg = [UIImage imageNamed:@"my_def_tgyj"]; } break; // 团队佣金奖励
                case 60:{ self.placeholderImg = [UIImage imageNamed:@"my_def_tgyj"]; } break;
                case 61:{ self.placeholderImg = [UIImage imageNamed:@"my_def_tb_buy_car"]; } break;
                case 62:{ self.placeholderImg = [UIImage imageNamed:@"my_def_share_poster"]; } break;
                case 63:{ self.placeholderImg = [UIImage imageNamed:@"my_def_video_teach"]; } break;
                case 64:{ self.placeholderImg = [UIImage imageNamed:@"my_def_new_road"]; } break;
//                case 65:{ self.placeholderImg = [UIImage imageNamed:@""]; } break; // 官方推荐
                case 66:{ self.placeholderImg = [UIImage imageNamed:@"my_free_order"]; } break;
                case 67:{ self.placeholderImg = [UIImage imageNamed:@"my_def_my_order"]; } break;
                default:  break;
            }
        }
        
    }
    return self ;
}

@end




