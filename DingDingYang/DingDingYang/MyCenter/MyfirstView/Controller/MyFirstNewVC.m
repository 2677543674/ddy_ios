//
//  MyFirstNewVC.m
//  DingDingYang
//
//  Created by ddy on 2018/1/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MyFirstNewVC.h"

#import "MyInfoViewOne.h"  // 顶部个人信息和三个功能块
#import "MyInfoViewTwo.h"  // 方块形状的功能块
#import "MyInfoCellOne.h"  // 其他列表形状
#import "MyInfoCellTwo.h"  // 轮播图
#import "MyInfoCellThree.h"// 1行4个
#import "MyDynamicModel.h" // 动态模块化的数据
#import "SystemMsgModel.h"
@interface MyFirstNewVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView * tableView ;
//@property(nonatomic,strong) UIView * topView ;
//@property(nonatomic,strong) UIButton * mesgBtn ; //消息按钮
//@property(nonatomic,strong) UIImageView * redPointImg ; // 消息红点显示图片

@property(nonatomic,strong) MyDynamicModel * headModel0 ; // 消费者的头部三个功能
@property(nonatomic,strong) MyDynamicModel * headModel1 ; // 代理商的头部三个功能
@property(nonatomic,strong) MyDynamicModel * headModel2 ; // 运营商的头部三个功能
@property(nonatomic,strong) NSMutableArray * roleAry0 ; // 消费者(动态化数据Model)
@property(nonatomic,strong) NSMutableArray * roleAry1 ; // 代理商(动态化数据Model)
@property(nonatomic,strong) NSMutableArray * roleAry2 ; // 运营商(动态化数据Model)

@property(nonatomic,strong) PersonalModel * infoModel ;
@property(nonatomic,strong) UIImageView * introImageView;
@property(nonatomic,strong) NSArray * introImageArray;
@property(nonatomic,assign) NSInteger index;

@property(nonatomic,assign) NSInteger showType; // 0:显示消费者 1:显示代理 2:运营商

@end

@implementation MyFirstNewVC


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated: animated];
    
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.001],
                                 NSForegroundColorAttributeName:[UIColor clearColor]};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [self huoQuInfo];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

//获取个人信息(每次都加载)
-(void)huoQuInfo{
    
    [PersonalModel getPersonalModel:^(PersonalModel *pModel) {
        _infoModel = pModel ;
        [self changeShowType];
        [_tableView reloadData]; // 刷新
        static dispatch_once_t onceToken;  // 绑定推送
        dispatch_once(&onceToken, ^{
            [ThirdPartyLogin bangding_JPushTags];
        });
        
    } failure:^(NSString *fail) {
        [_tableView reloadData];
        // roleName为0说明个人信息是通过登录返回的
        if (_infoModel.roleName.length==0) { SHOWMSG(fail) }
    }];
    
}

-(void)changeShowType{
    switch (role_state) {
        case 1:{ _showType = 1; } break;
        case 2:{ _showType = 2; } break;
        case 3:{ _showType = 2; } break;
        case 4:{ _showType = 1; } break;
        default:{ _showType = 0; } break;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    
    _roleAry0 = [NSMutableArray array];
    _roleAry1 = [NSMutableArray array];
    _roleAry2 = [NSMutableArray array];
    
    // 个人信息的缓存
    if (NSUDTakeData(UserInfo)!=nil) {
        NSDictionary * userdic = (NSDictionary*) NSUDTakeData(UserInfo) ;
        _infoModel = [[PersonalModel alloc]initWithDic:userdic];
    }else{
        _infoModel = [[PersonalModel alloc]init];
    }

    [self changeShowType];
    
    __weak MyFirstNewVC * selfView = self;
    // 获取动态模块数据
    [MyDynamicModel dynamicPersonalModel:^(NSMutableArray *roleAry0, NSMutableArray *roleAry1,NSMutableArray *roleAry2) {
        NSLog(@"从服务器拿的样式。。。。。。。。。。");
        [selfView todealWithAry0:roleAry0 ary1:roleAry1 ary2:roleAry2];
    } defaultModel:^(NSMutableArray *defaultrAry0, NSMutableArray *defaultrAry1,NSMutableArray *defaultrAry2) {
        NSLog(@"默认或缓存样式。。。。。。。。。。。");
        [selfView todealWithAry0:defaultrAry0 ary1:defaultrAry1  ary2:defaultrAry2];
    } fail:^(NSString *failStr) {  }];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:UserInfoChange object:nil];

#if DingDangDingDang
    [self creatMyCenterGuidePage];
#endif
    
}

// 将头部的2-3-1-1拆出来，剩余的重新组合成新的数组
-(void)todealWithAry0:(NSMutableArray*)ary0 ary1:(NSMutableArray*)ary1 ary2:(NSMutableArray*)ary2{
    
    [_roleAry0 removeAllObjects];
    [_roleAry1 removeAllObjects];
    [_roleAry2 removeAllObjects];
    
    for (MyDynamicModel * model0 in ary0) {
        if ([model0.styleType isEqualToString:@"2-3-1-1"]) {
            _headModel0 = model0 ;
        }else{
            [_roleAry0 addObject:model0];
        }
    }
    for (MyDynamicModel * model1 in ary1) {
        if ([model1.styleType isEqualToString:@"2-3-1-1"]) {
            _headModel1 = model1 ;
        }else{
            [_roleAry1 addObject:model1];
        }
    }
    for (MyDynamicModel * model2 in ary2) {
        if ([model2.styleType isEqualToString:@"2-3-1-1"]) {
            _headModel2 = model2 ;
        }else{
            [_roleAry2 addObject:model2];
        }
    }
    
    [self tableView];
}


#pragma mark ===>>> 创建UITableView
//创建表格
-(UITableView*)tableView{
    if (!_tableView) {
        
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate=self; _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO ;
        _tableView.backgroundColor = COLORGROUP ;
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.right.offset(0);
            make.bottom.offset(0); make.top.offset(0);
        }];
        
        [_tableView registerClass:[MyInfoViewNull class] forHeaderFooterViewReuseIdentifier:@"null_head_foot"];
        [_tableView registerClass:[MyInfoViewOne class] forHeaderFooterViewReuseIdentifier:@"myinfo_head_one"];
        [_tableView registerClass:[MyInfoViewTwo class] forCellReuseIdentifier:@"myinfo_two_cell"];
        [_tableView registerClass:[MyInfoCellOne class] forCellReuseIdentifier:@"img_text_cell"];
        [_tableView registerClass:[MyInfoCellTwo class] forCellReuseIdentifier:@"myinfo_carousel_cell"];
        [_tableView registerClass:[MyInfoCellThree class] forCellReuseIdentifier:@"myinfo_fourrow_cell"];

    }else{
        [_tableView reloadData];
    }
    return _tableView;
}

// 分区个数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    switch (_showType) {
        case 1:{ return _roleAry1.count; } break;
        case 2:{ return _roleAry2.count; } break;
        default:{ return _roleAry0.count; } break;
    }
}

// cell个数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    MyDynamicModel * mdModel ;
    switch (_showType) {
        case 1:{ mdModel = _roleAry1[section]; } break;
        case 2:{ mdModel = _roleAry2[section]; } break;
        default: { mdModel = _roleAry0[section]; } break;
    }
    if ([mdModel.styleType isEqualToString:@"2-6-1-1"] ) {  return 1 ; }
    if ([mdModel.styleType isEqualToString:@"2-5-1-4"] ) {  return 1 ; }
    if ([mdModel.styleType isEqualToString:@"2-5-1-5"] ) {  return 1 ; }
    return mdModel.cateInfoList.count ;
    
}

- (void)showBadgeOnItemIndex:(int)index{

    //移除之前的小红点
    [self removeBadgeOnItemIndex:index];
    //新建小红点
    UIView *badgeView = [[UIView alloc]init];
    badgeView.tag = 888 + index;
    badgeView.backgroundColor = [UIColor redColor];
    CGRect tabFrame = self.tabBarController.tabBar.frame;
    
    //确定小红点的位置
    float percentX = (index +0.6) / 5;
    CGFloat x = ceilf(percentX * tabFrame.size.width);
    CGFloat y = ceilf(0.1 * tabFrame.size.height);
    badgeView.frame = CGRectMake(x, y, 8, 8);
    badgeView.layer.cornerRadius = badgeView.frame.size.width/2;
    
    [self.tabBarController.tabBar addSubview:badgeView];
    
}

- (void)removeBadgeOnItemIndex:(int)index{
    
    //按照tag值进行移除
    for (UIView *subView in self.tabBarController.tabBar.subviews) {
        if (subView.tag == 888+index) {
            [subView removeFromSuperview];
        }
    }
}

#pragma mark ===>>> 区头
// 区头
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) { return 300*HWB ; }
    return 1;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) { // 区头信息(简单的个人资料)
        MyInfoViewOne * oneHeadView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"myinfo_head_one"];
        oneHeadView.pmodel = _infoModel ;
        switch (_showType) {
            case 1:{ oneHeadView.functionCardView.myModel = _headModel1; } break;
            case 2:{ oneHeadView.functionCardView.myModel = _headModel2; } break;
            default:{ oneHeadView.functionCardView.myModel = _headModel0; } break;
        }
       
        int totalUnreadCount = [[RCIMClient sharedRCIMClient] getTotalUnreadCount];
        if (totalUnreadCount>0) {
            //有未读融云消息显示小红点
            [self showBadgeOnItemIndex:4];
        }else{
            //判断是否有未读邮件（奖励消息）
            [NetRequest requestTokenURL:url_no_readMsg parameter:@{@"type":@"1"} success:^(NSDictionary *nwdic) {
                if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
                    if ([REPLACENULL(nwdic[@"count"]) integerValue]>0) {
                        oneHeadView.redPointImg.hidden=NO;
                         [self showBadgeOnItemIndex:4];
                    }else{
                        //系统消息
                        [NetRequest requestTokenURL:url_system_msg parameter:@{@"p":@"1"} success:^(NSDictionary *nwdic) {
                            if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
                                
                                NSArray * listAry = NULLARY(nwdic[@"list"]);
                                NSMutableArray* sysdataAry=[NSMutableArray array];
                                [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    SystemMsgModel * model = [[SystemMsgModel alloc]initWithDic:obj];
                                    [sysdataAry addObject:model];
                                }];
                                if (listAry.count>0) {
                                    SystemMsgModel * model = sysdataAry[0];
                                    if (![model.createTime isEqualToString:NSUDTakeData(@"newMesTime")]) {
                                        oneHeadView.redPointImg.hidden=NO;
                                        [self showBadgeOnItemIndex:4];
                                    }else{
                                        [self removeBadgeOnItemIndex:4];
                                        oneHeadView.redPointImg.hidden=YES;
                                    }
                                }else{
                                    [self removeBadgeOnItemIndex:4];
                                    oneHeadView.redPointImg.hidden=YES;
                                }
                            }
                            
                        } failure:^(NSString *failure) {
                            
                        }];
                    }
                }
                
            } failure:^(NSString *failure) {
                
            }];
        }
        
        
        return oneHeadView ;
    }
    
    MyInfoViewNull * nullView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"null_head_foot"];
    return nullView ;
}

// 区尾
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    MyInfoViewNull * nullView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"null_head_foot"];
    return nullView ;
}

// cell高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        MyDynamicModel * mdModel;
        switch (_showType) {
            case 1:{ mdModel = _roleAry1[indexPath.section]; } break;
            case 2:{ mdModel = _roleAry2[indexPath.section]; } break;
            default: { mdModel = _roleAry0[indexPath.section]; } break;
        }
        
        if ([mdModel.styleType isEqualToString:@"2-6-1-1"]) {
            // 每行三个、可能会有多个
            float  wh = FloatKeepInt((ScreenW-34)/3)-10*HWB ;
            if (mdModel.cateInfoList.count%3==0) {
                float hi = mdModel.cateInfoList.count/3*wh+1+mdModel.cateInfoList.count/3;
                return hi ;
            }else{
                float hi = (mdModel.cateInfoList.count/3+1)*wh+2+mdModel.cateInfoList.count/3;
                return hi ;
            }
        }
        
        if ([mdModel.styleType isEqualToString:@"2-5-1-4"]) {
            return 76*HWB;
        }
        
        if ([mdModel.styleType isEqualToString:@"2-5-1-5"]) {
            // 每行4个、可能会有多个
            float  wh = FloatKeepInt((ScreenW-34)/4)-10*HWB ;
            if (mdModel.cateInfoList.count%4==0) {
                float hi = mdModel.cateInfoList.count/4*wh+1+mdModel.cateInfoList.count/4;
                return 1.3*hi ;
            }else{
                float hi = (mdModel.cateInfoList.count/4+1)*wh+2+mdModel.cateInfoList.count/4;
                return hi*1.3 ;
            }
        }

        
        return 45 * HWB ;
        
    } @catch (NSException *exception) { [_tableView reloadData]; } @finally { }
    
}

// cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    @try {
        MyDynamicModel * myModel;
        switch (_showType) {
            case 1:{ myModel = _roleAry1[indexPath.section]; } break;
            case 2:{ myModel = _roleAry2[indexPath.section]; } break;
            default: { myModel = _roleAry0[indexPath.section]; } break;
        }
        
        if ([myModel.styleType isEqualToString:@"2-6-1-1"]) {
            MyInfoViewTwo * twoCell = [tableView dequeueReusableCellWithIdentifier:@"myinfo_two_cell" forIndexPath:indexPath];
            twoCell.dynamicModel = myModel ;
            return twoCell ;
        }
        
        if ([myModel.styleType isEqualToString:@"2-5-1-4"]) {
            MyInfoCellTwo * twoCell = [tableView dequeueReusableCellWithIdentifier:@"myinfo_carousel_cell" forIndexPath:indexPath];
            twoCell.dynamicModel = myModel;
            return twoCell ;
        }
        
        if ([myModel.styleType isEqualToString:@"2-5-1-5"]) {
            MyInfoCellThree * twoCell = [tableView dequeueReusableCellWithIdentifier:@"myinfo_fourrow_cell" forIndexPath:indexPath];
            twoCell.functionView.myModel = myModel;
 
            return twoCell ;
        }
        
        MyInfoCellOne * cell = [tableView dequeueReusableCellWithIdentifier:@"img_text_cell" forIndexPath:indexPath];
        cell.classModel = myModel.cateInfoList[indexPath.row];
        if (myModel.cateInfoList.count==1) {  cell.clipsType = 3 ; }
        if (myModel.cateInfoList.count==2) {
            if (indexPath.row==0) { cell.clipsType = 1 ; }
            if (indexPath.row==1) { cell.clipsType = 2 ;}
        }
        if (myModel.cateInfoList.count>2) {
            if (indexPath.row==0) {  cell.clipsType = 1 ; }
            else if (indexPath.row==myModel.cateInfoList.count-1){ cell.clipsType = 2 ; }
            else { cell.clipsType = 0 ; }
        }
        return cell;
    } @catch (NSException *exception) { [_tableView reloadData]; } @finally { }
   
}

#pragma mark ===>>> tableView选中事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        MyDynamicModel * myModel;
        switch (_showType) {
            case 1:{ myModel = _roleAry1[indexPath.section]; } break;
            case 2:{ myModel = _roleAry2[indexPath.section]; } break;
            default: { myModel = _roleAry0[indexPath.section]; } break;
        }
        MyClassModel * mcModel = myModel.cateInfoList[indexPath.row];
        if (mcModel) { [MyPageJump pushNextVcByModel:mcModel byType:0]; }
        
    } @catch (NSException *exception) { } @finally { }
}

#pragma mark ===>>> 极光绑定: 别名(手机号、唯一) 标签( 代理商状态 )

// 用户身份状态发生变化
-(void)changeState{
    [ThirdPartyLogin bangding_JPushTags] ; // 重新绑定推送标签
}

-(void)addTagOrAlias{
    
    [JPUSHService setAlias:_infoModel.mobile completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        if (iResCode==0) { NSLog(@"绑定别名成功!"); }
    } seq:666666];
    
    [JPUSHService setTags:[NSSet setWithArray:@[_infoModel.role]] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
        if (iResCode==0) { NSLog(@"绑定标签成功!"); }
    } seq:666666];
    
}

#pragma mark ===>>> 个人中心的引导页
-(void)creatMyCenterGuidePage{
    if (CANOPENWechat==YES) {
        if ([NSUDTakeData(IfFirstOpenUserCenter) length]==0){
            _introImageView =[[UIImageView alloc]init];
            _introImageView.tag=300;
            _introImageView.contentMode=UIViewContentModeScaleAspectFit;
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(nextStep)];
            [_introImageView addGestureRecognizer:tap];
            [[UIApplication sharedApplication].keyWindow addSubview:_introImageView];
            [_introImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.offset(0);make.left.offset(0);
                make.bottom.offset(0);make.right.offset(0);
            }];
            _introImageView.userInteractionEnabled = YES;
            _introImageArray = @[@"个人中心引导页1",@"个人中心引导页2",@"个人中心引导页3"];
            _index = 0;
            [_introImageView setImage:[UIImage imageNamed:_introImageArray[_index]]];
        }
    }
}
-(void)nextStep{
    _index++;
    if (_index<_introImageArray.count) {
        [_introImageView setImage:[UIImage imageNamed:_introImageArray[_index]]];
    }else {
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:IfFirstOpenUserCenter];
        [[[UIApplication sharedApplication].keyWindow viewWithTag:300] removeFromSuperview];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
