//
//  NewAboutVC.h
//  DingDingYang
//
//  Created by ddy on 2018/1/16.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewAboutVC : DDYViewController
@property(nonatomic,copy) NSString* modelTitle;
@end
