//
//  NewAboutVC.m
//  DingDingYang
//
//  Created by ddy on 2018/1/16.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "NewAboutVC.h"
#import "LoginVc.h"
#import "WebVC.h"
#import "GoodsFeedBackVC.h"
#import "ImgTextCell.h"
#import "PersonalInfoCell.h"

@interface NewAboutVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIView * instructionsView;
@property(nonatomic,strong) UITableView * tabView;
@property(nonatomic,strong) NSArray * imgAry, * txtAry;
@property(nonatomic,strong) NSString* uploadUrl;

@end

@implementation NewAboutVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if (PID==10104) { // 蜜赚不要关于，改为设置
        self.title = @"设置" ;
    }else{
        self.title = _modelTitle.length==0?@"关于我们":_modelTitle;
    }
    

    self.view.backgroundColor = COLORGROUP ;
    
    [self instructionsView];
    [self tabView];
    
    
//    UIButton*backLogin=[UIButton titLe:@"退出登录" bgColor:LinesColor titColorN:Black102 titColorH:Black204 font:13*HWB];
//    backLogin.layer.cornerRadius=6;
//    [backLogin addShadowLayer];
//    [backLogin addTarget:self action:@selector(backLoginAndRemove) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:backLogin];
//    [backLogin mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.offset(30*HWB);   make.right.offset(-30*HWB);
//        make.height.offset(38*HWB);  make.top.equalTo(_tabView.mas_bottom).offset(50*HWB);
//    }];
    [_tabView reloadData];
    
}

-(UIView*)instructionsView{
    if (!_instructionsView) {
        
        _instructionsView = [[UIView alloc]init] ;
        _instructionsView.backgroundColor = COLORGROUP ;
        _instructionsView.frame = CGRectMake(0, 0, ScreenW, ScreenH/2-95);
        [self.view addSubview:_instructionsView] ;
        
        //logo图标
        UIImageView*logoImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:LOGONAME]];
        [_instructionsView addSubview:logoImg];
        [logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(HFRAME(_instructionsView)/2.5);
            make.bottom.equalTo(_instructionsView.mas_centerY);
            make.centerX.equalTo(_instructionsView.mas_centerX);
            make.width.offset(HFRAME(_instructionsView)/2.5);
        }];
        
        //版本号
        UILabel*banben=[UILabel labText:APPNAMEVERSION color:Black51 font:16*HWB];
        [_instructionsView addSubview:banben];
        [banben mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.top.equalTo(logoImg.mas_bottom).offset(10);
        }];
#if HuiDe
        logoImg.image=[UIImage imageNamed:@"ZZ"];
        banben.numberOfLines=2;
        banben.textAlignment=NSTextAlignmentCenter;
        banben.text=[NSString stringWithFormat:@"%@\n天天双十一，网购能省钱就是赚钱",APPNAME];
        [banben mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.top.equalTo(logoImg.mas_bottom).offset(5);
        }];
#endif
    }
    return _instructionsView;
}

-(UITableView*)tabView{
    if (!_tabView) {
        _tabView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tabView.delegate = self;  _tabView.dataSource = self;
        [self.view addSubview:_tabView];
        [_tabView registerClass:[ImgTextCell class] forCellReuseIdentifier:@"imgtextcell"];
        [_tabView registerClass:[PersonalInfoCell class] forCellReuseIdentifier:@"PersonalInfoCell"];
        _tabView.backgroundColor = COLORGROUP;
        _tabView.scrollEnabled = NO;
        NSString*currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString* text=[NSString stringWithFormat:@"当前版本%@",currentVersion];
        if (PID==10104) { // 蜜赚，将关于改为设置，原关于我们的功能保留，加上重要提醒
            if (CANOPENAlipay==YES||CANOPENWechat==YES) {
                _imgAry=@[@"about_clear",@"about_feedback",@"about_protocol",@"about_version",@"about_tixing"];
                _txtAry=@[@"清除缓存",@"用户反馈",@"服务条款及隐私协议",text,@"重要提醒"];
            }else{
                _imgAry=@[@"about_clear",@"about_feedback",@"about_protocol",@"about_tixing"];
                _txtAry=@[@"清除缓存",@"用户反馈",@"服务条款及隐私协议",@"重要提醒"];
            }
            
        }else{
            if (CANOPENAlipay==YES||CANOPENWechat==YES) {
                _imgAry=@[@"about_clear",@"about_feedback",@"about_protocol",@"about_version"];
                _txtAry=@[@"清除缓存",@"用户反馈",@"服务条款及隐私协议",text];
            }else{
                _imgAry=@[@"about_clear",@"about_feedback",@"about_protocol"];
                _txtAry=@[@"清除缓存",@"用户反馈",@"服务条款及隐私协议"];
            }
        }
#if YueNiSuoXiang
        _imgAry=@[@"about_clear",@"about_feedback"];
        _txtAry=@[@"清除缓存",@"用户反馈"];
#endif
        [_tabView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.height.offset(_imgAry.count*45*HWB+10*HWB);
            make.centerY.equalTo(self.view.mas_centerY).offset(0);
        }];
        
    }
    return _tabView;
}

#pragma mark ===>>> tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _imgAry.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40*HWB ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==3) { return 15*HWB ; }
    return 5*HWB;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    ImgTextCell * cell = [tableView dequeueReusableCellWithIdentifier:@"imgtextcell" forIndexPath:indexPath];
    
    cell.backView.hidden = YES ;
    cell.nameTxt = _txtAry[indexPath.section];
    
    cell.imgView.image = [[UIImage imageNamed:_imgAry[indexPath.section]] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
    if (THEMECOLOR==2) {  cell.imgView.image = [UIImage imageNamed:_imgAry[indexPath.section]]; }

    if (indexPath.section==3) {
        
        UIButton * btn = [UIButton titLe:@"正在检测更新..." bgColor:COLORCLEAR titColorN:Black102 titColorH:Black102 font:11*HWB];
        btn.titleLabel.numberOfLines = 0 ;
        [cell.contentView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5);  make.top.offset(1);
            make.bottom.offset(-1);
        }];
    
        [MyPageJump newcheckAppUpdateBlock:^(NSInteger stateCode, NSString *downLoadUrl, NSString *updateContent, NSString *newVersion) {
            switch (stateCode) {
                    
                case 0:{ // 有新版本
                    [btn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
                    [btn setTitle:@"有新版本,请点击更新!" forState:UIControlStateNormal];
                    [btn addTarget:self action:@selector(clickUpdateApp) forControlEvents:UIControlEventTouchUpInside];
                    _uploadUrl = downLoadUrl;
                }  break;
                    
                default:{ [btn setTitle:downLoadUrl forState:UIControlStateNormal]; } break;
            }
        }];
      
    }
    
    if (indexPath.section==4) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backView.hidden = NO ;
        UIUserNotificationSettings * setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
        if (setting.types == UIUserNotificationTypeNone) {
            cell.swicthLabel.text = @"已关闭";
            [cell.switchView setOn:NO animated:YES];
        }else{
            cell.swicthLabel.text = @"已开启";
            [cell.switchView setOn:YES animated:YES];
        }
    }
    
    return cell;
}

- (void)clickUpdateApp{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:_uploadUrl]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_uploadUrl]];
    }else{ SHOWMSG(@"更新失败!"); }
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView*view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 6)];
    view.backgroundColor= COLORGROUP;
    return view;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
            
        case  0:{
            MBShow(@"正在清除...")
            [self clearPersonalInformationCache];
            [self clearPublicInformationCache];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
                    MBHideHUD   MBSuccess(@"清除成功!")
                }];
            });
        } break;
            
        case  1:{
            
            GoodsFeedBackVC * gfbVC = [[GoodsFeedBackVC alloc]init];
            gfbVC.hidesBottomBarWhenPushed = YES ;
            gfbVC.title = @"用户反馈";
            PushTo(gfbVC, YES)
        } break;
            
        case 2:{ // 点击用户协议
            
            WebVC * web = [[WebVC alloc]init];
            web.nameTitle = @"用户协议" ;
            web.urlWeb = web_url_user_agreement ;
            web.hidesBottomBarWhenPushed = YES ;
            [self.navigationController pushViewController:web animated:YES];
            
        } break;
            
        case 3:{ // 检测更新
            
        }break;
            
        case 4:{ // 开启推送权限
            NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }else{
                SHOWMSG(@"暂时无法开启!")
            }
        }
        
        default:   break;
    }
}


#pragma mark ===>>> 退出登录弹窗提醒
-(void)backLoginAndRemove{
    
    [UIAlertController showIn:self title:@"提示" content:@"确定要退出当前账户吗?" ctAlignment:NSTextAlignmentCenter btnAry:@[@"确定",@"取消"] indexAction:^(NSInteger indexTag) {
        if (indexTag==0) { [self logOut]; }
    }];

}

-(void)logOut{
    
    // 退出登录时、 清除跟账号有关的缓存
    if (TOKEN.length>0) { NSUDRemoveData(@"TOKEN") }
    
    [self clearPersonalInformationCache];
    
    // 删除绑定的别名
    [JPUSHService deleteAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        if (iResCode==0) { NSLog(@"解绑别名成功!"); }
    } seq:666666];
    
    // 删除绑定的标签
    [JPUSHService cleanTags:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
        if (iResCode==0) { NSLog(@"解绑标签成功!"); }
    } seq:666666];
    
    // 将LoginVc 设置成window.rootvc
    [LoginVc showLoginVC:nil fromType:0 loginBlock:^(NSInteger result) { }];
    if (rongcloud_APP_Key.length>0) {
        // 退出融云 且不接受推送
        [[RCIMClient sharedRCIMClient] logout];
    }
//    if (TBKEY.length>0&&IsNeedTaoBao==1) {
        [[ALBBSDK sharedInstance] ALBBSDKInit];
        if ([[ALBBSession sharedInstance] isLogin]) {
            [[ALBBSDK sharedInstance] logout];
        }
//    }
    
}

// 清除个人信息有关的缓存 (不影响正常使用)
-(void)clearPersonalInformationCache{
    
    @try {
        if (NSUDTakeData(UserInfo)) { NSUDRemoveData(UserInfo) }               // 用户信息
        if (NSUDTakeData(ShareMuBan)) { NSUDRemoveData(ShareMuBan) }           // 用户分享模板
        if (NSUDTakeData(ApplyUpgrade)) { NSUDRemoveData(ApplyUpgrade) }       // 用户升级信息
        if (NSUDTakeData(PromotionPoster)) { NSUDRemoveData(PromotionPoster) } // 用户分享海报
        if (NSUDTakeData(JD_ConsumerData)) { NSUDRemoveData(JD_ConsumerData) } // 用户京东消费佣金
        if (NSUDTakeData(TB_ConsumerData)) { NSUDRemoveData(TB_ConsumerData) } // 用户淘宝消费佣金
        
        // 清除广告缓存
        if (NSUDTakeData(ad_floating_imgUrl)) { NSUDRemoveData(ad_floating_imgUrl) }
        if (NSUDTakeData(ad_popView_imgUrl)) { NSUDRemoveData(ad_popView_imgUrl) }
        if (NSUDTakeData(ad_launch_imgUrl)) { NSUDRemoveData(ad_launch_imgUrl) }
        if (NSUDTakeData(ad_floating_dic)) { NSUDRemoveData(ad_floating_dic) }
        if (NSUDTakeData(ad_popView_dic)) { NSUDRemoveData(ad_popView_dic) }
        if (NSUDTakeData(ad_launch_dic)) { NSUDRemoveData(ad_launch_dic) }
        if (NSUDTakeData(ad_launch_img)) { NSUDRemoveData(ad_launch_img) }
        
        // 清除动态模块缓存
        if (NSUDTakeData(app_dynamic_my_dic)) { NSUDRemoveData(app_dynamic_my_dic) }
        if (NSUDTakeData(app_dynamic_my_time)) { NSUDRemoveData(app_dynamic_my_time) }
        if (NSUDTakeData(app_dynamic_home_dic)) { NSUDRemoveData(app_dynamic_home_dic) }
        if (NSUDTakeData(app_dynamic_home_time)) { NSUDRemoveData(app_dynamic_home_time) }
        if (NSUDTakeData(app_dynamic_tabbar_dic)) { NSUDRemoveData(app_dynamic_tabbar_dic) }
        if (NSUDTakeData(app_dynamic_tabbar_time)) { NSUDRemoveData(app_dynamic_tabbar_time) }

    } @catch (NSException *exception) { } @finally { }
    
}
// 清除一些公共的数据缓存 (不影响正常使用)
-(void)clearPublicInformationCache{
    if (NSUDTakeData(HomeOneList)!=nil) { NSUDRemoveData(HomeOneList) }         // 首页商品数据缓存
    if (NSUDTakeData(KeFuZhongXin)!=nil) { NSUDRemoveData(KeFuZhongXin) }       // 首页商品数据缓存
    if (NSUDTakeData(OneDayRanking)!=nil) { NSUDRemoveData(OneDayRanking) }     // 首页商品数据缓存
    if (NSUDTakeData(SevenDayRanking)!=nil) { NSUDRemoveData(SevenDayRanking) } // 首页商品数据缓存
}

@end

