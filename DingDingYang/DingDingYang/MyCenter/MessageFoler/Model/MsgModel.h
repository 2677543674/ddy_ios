//
//  MsgModel.h
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MsgModel : NSObject
@property(nonatomic,copy)NSString*content;//内容
@property(nonatomic,copy)NSString*createTime;//消息产生时间
@property(nonatomic,copy)NSString*cid;//id
@property(nonatomic,copy)NSString*userId;//用户id
-(instancetype)initWithDic:(NSDictionary*)dic;
@end
