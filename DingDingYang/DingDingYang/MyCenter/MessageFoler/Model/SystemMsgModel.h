//
//  SystemMsgModel.h
//  DingDingYang
//
//  Created by ddy on 2017/3/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemMsgModel : NSObject
@property(nonatomic,copy)NSString*sid;//消息id
@property(nonatomic,copy)NSString*title;
@property(nonatomic,copy)NSString*createTime;
@property(nonatomic,copy)NSString*ifRead;//是否已读(1:已读)
-(instancetype)initWithDic:(NSDictionary*)dic;
@end
