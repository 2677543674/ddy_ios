//
//  SystemMsgModel.m
//  DingDingYang
//
//  Created by ddy on 2017/3/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SystemMsgModel.h"

@implementation SystemMsgModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        self.sid = REPLACENULL(dic[@"id"]);
        self.title = REPLACENULL(dic[@"title"]);
        self.createTime = REPLACENULL(dic[@"createTime"]);
        NSTimeInterval timeC = [REPLACENULL(dic[@"createTime"]) doubleValue];
        NSString*timeStr = [self timeStamp:timeC andFormatter:@"MM月dd日"];
        self.createTime = REPLACENULL(timeStr);
        self.ifRead = REPLACENULL(dic[@"ifRead"]);
    }
    return self;
}
-(NSString *)timeStamp:(NSTimeInterval)stamp andFormatter:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //（@"YYYY年MM月dd日hh:mm:ss:SSS"）设置格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:format];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:stamp/1000];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}
@end
