//
//  MsgModel.m
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MsgModel.h"

@implementation MsgModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        self.content=REPLACENULL(dic[@"content"]);
        self.cid=REPLACENULL(dic[@"id"]);
        self.userId=REPLACENULL(dic[@"userId"]);
        
        self.createTime = TimeByStamp(REPLACENULL(dic[@"createTime"]),@"yyyy年MM月dd日 HH:mm");
        
//        NSTimeInterval timeC=[REPLACENULL(dic[@"createTime"]) doubleValue];
//        NSString*timeStr=[self timeStamp:timeC andFormatter:@"YYYY年MM月dd日 HH:mm"];
//        self.createTime=REPLACENULL(timeStr);
    }
    return self;
}

-(NSString *)timeStamp:(NSTimeInterval)stamp andFormatter:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //（@"YYYY年MM月dd日hh:mm:ss:SSS"）设置格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:format];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:stamp/1000];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}


@end
