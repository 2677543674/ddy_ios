//
//  SystemMsgCell.h
//  DingDingYang
//
//  Created by ddy on 2017/3/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SystemMsgModel.h"
@interface SystemMsgCell : UITableViewCell
@property(nonatomic,strong)UIImageView*logoImgV;
@property(nonatomic,strong)UILabel*titLab;
@property(nonatomic,strong)UILabel*timeLab;
@property(nonatomic,strong)SystemMsgModel*symodel;
@end













