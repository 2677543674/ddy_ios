//
//  MsgRewardCell.m
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MsgRewardCell.h"

@implementation MsgRewardCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style  reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor=COLORGROUP;
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        _timeLab=[UILabel labText:@"2017年03月08日" color:[UIColor lightGrayColor] font:13.0];
        _timeLab.layer.cornerRadius=6;
        _timeLab.backgroundColor=LinesColor;
        [self addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.height.offset(12);
            make.top.offset(10);
        }];
        
        _logoImg=[[UIImageView alloc]init];
        _logoImg.image=[UIImage imageNamed:LOGONAME];
        _logoImg.layer.shouldRasterize = YES;
        _logoImg.layer.rasterizationScale = [UIScreen mainScreen].scale;
        _logoImg.layer.masksToBounds=YES;
        _logoImg.layer.cornerRadius = 20 ;
        [self addSubview:_logoImg];
        [_logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(16);  make.top.offset(28);
            make.height.offset(40); make.width.offset(40);
        }];
        
        
        UIImage*oldImg=[UIImage imageNamed:@"msgback"];
        UIImage*newImg=[oldImg stretchableImageWithLeftCapWidth:100 topCapHeight:25];
        _bgImg=[[UIImageView alloc]init];
        _bgImg.image=newImg;
        [self addSubview:_bgImg];
        [_bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_logoImg.mas_right).offset(5);
            make.right.offset(-59); make.bottom.offset(-5);
            make.top.offset(32);
        }];
        
        _contentLab=[UILabel labText:@"消息内容" color:Black102 font:14.0*HWB];
        _contentLab.numberOfLines=0;
        [_bgImg addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(18);  make.right.offset(-5);
            make.top.offset(10); make.bottom.offset(-10);
        }];
        
    }
    return self;
}

-(void)setRmodel:(MsgModel *)rmodel{
    _rmodel=rmodel;
    _timeLab.text=rmodel.createTime;
    _contentLab.text=rmodel.content;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
