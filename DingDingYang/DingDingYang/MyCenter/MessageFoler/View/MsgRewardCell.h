//
//  MsgRewardCell.h
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MsgModel.h"
@interface MsgRewardCell : UITableViewCell
@property(nonatomic,strong)UIImageView*logoImg;
@property(nonatomic,strong)UILabel*timeLab;
@property(nonatomic,strong)UILabel*contentLab;
@property(nonatomic,strong)UIImageView*bgImg;
@property(nonatomic,strong)MsgModel*rmodel;
@end
