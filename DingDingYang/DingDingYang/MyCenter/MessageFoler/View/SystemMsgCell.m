//
//  SystemMsgCell.m
//  DingDingYang
//
//  Created by ddy on 2017/3/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SystemMsgCell.h"

@implementation SystemMsgCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style  reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator ;
        
//        NSLog(@"%f",self.contentView.frame.origin.y);
        
        _timeLab=[UILabel labText:@"03月08日" color:Black102 font:13.0*HWB];
//        CGSize size = StringSize(@"03月08日", 13.0);
        [self.contentView addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_centerY).offset(2);
            make.left.offset(15);
        }];
        
        _titLab=[UILabel labText:@"标题" color:Black51 font:14.0*HWB];
        [self.contentView addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);
            make.right.offset(0);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(-1);
        }];
        
        UIImageView * linesImg = [[UIImageView alloc]init];
        linesImg.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImg];
        [linesImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.left.offset(0);
            make.right.offset(10); make.height.offset(1);
        }];
        
    }
    return self;
}

-(void)setSymodel:(SystemMsgModel *)symodel{
    _symodel=symodel;
    _titLab.text=symodel.title;
    _timeLab.text=symodel.createTime;
}


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
