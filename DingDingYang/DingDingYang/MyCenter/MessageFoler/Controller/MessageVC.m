//
//  MessageVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MessageVC.h"
#import "MsgModel.h"
#import "MsgRewardCell.h"
#import "SystemMsgCell.h"
#import "SystemMsgModel.h"

@interface MessageVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSMutableArray * reddataAry; // 奖励消息
@property(nonatomic,strong) NSMutableArray * sysdataAry; // 系统消息

@property(nonatomic,strong) UIScrollView * scrollView ;
@property(nonatomic,strong) UITableView  * tableView1;
@property(nonatomic,strong) UITableView  * tableView2;

@property(nonatomic,assign) NSInteger  pageIndex1;
@property(nonatomic,assign) NSInteger  pageIndex2;

@property(nonatomic,strong) UIView   * msgbtnView;  // 选择按钮背景视图
@property(nonatomic,strong) UIButton * msgBtn;      // 按钮
@property(nonatomic,strong) UIImageView * lineImg;  // 下划线
@property(nonatomic,assign) NSInteger  selectTag;   // 选中的哪一个 0:奖励消息  1:系统消息

@property(nonatomic,strong) MyCenterNoDataView * nodataView1 ;
@property(nonatomic,strong) MyCenterNoDataView * nodataView2 ;

@end

@implementation MessageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"系统邮件";
    
    
    self.view.backgroundColor = COLORWHITE;
    
    _reddataAry = [NSMutableArray array];
    _sysdataAry = [NSMutableArray array];
    
    _pageIndex1 = 1 ; _pageIndex2 = 1 ;
    
    if ([NSUDTakeData(TeamStatus) isEqualToString:@"open"]||[NSUDTakeData(OrderStatus) isEqualToString:@"open"]) {
        self.title = @"消息";
        [self msgbtnView]; // (不要奖励消息，只保留系统邮件)
        [self lineImg];
        [self creatScrollView];
        [self tableView1];
    }else{
        _selectTag = 1 ; // 系统消息
    }
    
    [self tableView2];
    [self creatNodataView];

}

#pragma mark ===>>> 顶部选择按钮

-(UIView*)msgbtnView{
    NSArray * titleary = @[@"奖励消息",@"系统邮件"];
    if (!_msgbtnView) {
        _msgbtnView = [[UIView alloc]init];
        _msgbtnView.frame = CGRectMake(0, 0, ScreenW, 40);
        [self.view addSubview:_msgbtnView];
        for (NSInteger i = 0 ; i<titleary.count ; i++) {
            _msgBtn = [UIButton titLe:titleary[i] bgColor:COLORWHITE titColorN:Black51 titColorH:COLORGRAY font:14*HWB];
            _msgBtn.frame = CGRectMake(ScreenW/titleary.count*i, 0 , ScreenW/titleary.count, 38);
            ADDTARGETBUTTON(_msgBtn, clickSelect:)
            _msgBtn.tag = 10 + i ;
            _msgBtn.selected = NO ;
            [_msgbtnView addSubview:_msgBtn];
            if (i == _selectTag) {
                _msgBtn.selected = YES;
                [_msgBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
            }
        }
    }
    return _msgbtnView ;
}
// 下划线
-(UIImageView*)lineImg{
    if (!_lineImg) {
        _lineImg = [[UIImageView alloc]init];
        _lineImg.backgroundColor = LOGOCOLOR ;
        _msgBtn = (UIButton*)[_msgbtnView viewWithTag:10+_selectTag];
        _lineImg.center = CGPointMake(_msgBtn.center.x, 37);
        _lineImg.bounds = CGRectMake(0, 0, 55*HWB , 1.5);
        [self.view addSubview:_lineImg];
    }
    return _lineImg ;
}

#pragma mark ===>>> 按钮点击事件
-(void)clickSelect:(UIButton*)sbtn{
    _selectTag = sbtn.tag - 10 ;
    
    for (_msgBtn in _msgbtnView.subviews) {
        [_msgBtn setTitleColor:Black51 forState:UIControlStateNormal];
        _msgBtn.selected = NO ;
    }
    _msgBtn = (UIButton*)[_msgbtnView viewWithTag:sbtn.tag];
    _msgBtn.selected = YES ;
    [_msgBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    float lineCenterX  = _msgBtn.center.x ;
    [UIView animateWithDuration:0.16 animations:^{  _lineImg.center = CGPointMake(lineCenterX, 37);  }];
    _scrollView.contentOffset = CGPointMake(_selectTag*ScreenW, 0);
    
    if (_selectTag==0) { if (_reddataAry.count==0) { [_tableView1.mj_header beginRefreshing]; } }
    if (_selectTag==1) { if (_sysdataAry.count==0) { [_tableView2.mj_header beginRefreshing]; } }
    
}

#pragma mark ===>>> 请求奖励消息或系统消息
-(void)requestMessageData{
    
    NSString * url = _selectTag==0?url_reward_msg:url_system_msg;
    NSString * page = _selectTag==0?FORMATSTR(@"%ld",(long)_pageIndex1):FORMATSTR(@"%ld",(long)_pageIndex2);
    [NetRequest requestType:0 url:url ptdic:@{@"p":page,@"type":@"1"} success:^(id nwdic) {
        NSArray * listAry = NULLARY(nwdic[@"list"]);
        
        [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([url isEqualToString:url_reward_msg]) {
                MsgModel * model = [[MsgModel alloc]initWithDic:obj];
                [_reddataAry addObject:model];
            }
            if ([url isEqualToString:url_system_msg]) {
                SystemMsgModel * model = [[SystemMsgModel alloc]initWithDic:obj];
                [_sysdataAry addObject:model];
            }
        }];
        
        if (_sysdataAry.count!=0) {
            SystemMsgModel * model = _sysdataAry[0];
            NSUDSaveData(model.createTime, @"newMesTime");
        }

        if ([url isEqualToString:url_reward_msg]) {
            [_tableView1 endRefreshType:listAry.count isReload:YES];
            if (_reddataAry.count==0) { _nodataView1.hidden = NO; } else { _nodataView1.hidden = YES; }
        }
        if ([url isEqualToString:url_system_msg]) {
            [_tableView2 endRefreshType:listAry.count isReload:YES];
            if (_sysdataAry.count==0) { _nodataView2.hidden = NO; } else { _nodataView2.hidden = YES; }
        }
      
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        if ([url isEqualToString:url_reward_msg]) { [_tableView1 endRefreshType:1 isReload:YES]; }
        if ([url isEqualToString:url_system_msg]) { [_tableView2 endRefreshType:1 isReload:YES]; }
        SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestMessageData];
        }];
    }];
    
    
    /*
    [NetRequest requestTokenURL:url parameter:@{@"p":page,@"type":@"1"} success:^(NSDictionary *nwdic) {
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
            
            NSArray * listAry = NULLARY(nwdic[@"list"]);
            
            [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([url isEqualToString:url_reward_msg]) {
                    MsgModel * model = [[MsgModel alloc]initWithDic:obj];
                    [_reddataAry addObject:model];
                }
                if ([url isEqualToString:url_system_msg]) {
                    SystemMsgModel * model = [[SystemMsgModel alloc]initWithDic:obj];
                    [_sysdataAry addObject:model];
                }
            }];
            
            if (_sysdataAry.count!=0) {
                SystemMsgModel * model = _sysdataAry[0];
                NSUDSaveData(model.createTime, @"newMesTime");
            }
            
            
            if (listAry.count>0) { // 消息不为空
                if ([url isEqualToString:url_reward_msg]) { [_tableView1.mj_footer endRefreshing]; _pageIndex1 ++ ; }
                if ([url isEqualToString:url_system_msg]) { [_tableView2.mj_footer endRefreshing]; _pageIndex2 ++ ; }
            }else{
                if ([url isEqualToString:url_reward_msg]) { [_tableView1.mj_footer endRefreshingWithNoMoreData]; }
                if ([url isEqualToString:url_system_msg]) { [_tableView2.mj_footer endRefreshingWithNoMoreData]; }
            }
            
        }else{
            if ([url isEqualToString:url_reward_msg]) {
                [_tableView1.mj_header endRefreshing];
                [_tableView1.mj_footer endRefreshing];
            }
            if ([url isEqualToString:url_system_msg]) {
                [_tableView2.mj_header endRefreshing];
                [_tableView2.mj_footer endRefreshing];
            }
            SHOWMSG(nwdic[@"result"])
        }
        
        if ([url isEqualToString:url_reward_msg]) {
            [_tableView1.mj_header endRefreshing];
            [_tableView1 reloadData];
            if (_reddataAry.count==0) { _nodataView1.hidden = NO ; }
            else{  _nodataView1.hidden = YES ; }
        }
        if ([url isEqualToString:url_system_msg]) {
            [_tableView2.mj_header endRefreshing];
            [_tableView2 reloadData];
            if (_sysdataAry.count==0) { _nodataView2.hidden = NO ; }
            else{ _nodataView2.hidden = YES ; }
        }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestMessageData];
        }];

        if (_tableView1) {
            [_tableView1.mj_header endRefreshing];
            [_tableView1.mj_footer endRefreshing];
        }
        
        [_tableView2.mj_header endRefreshing];
        [_tableView2.mj_footer endRefreshing];
    }];*/
}

#pragma mark ===>>> scrollView

-(void)creatScrollView{
    
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.backgroundColor = COLORWHITE ;
    _scrollView.pagingEnabled = YES;
    _scrollView.delegate = self;
    _scrollView.tag = 1000 ;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(ScreenW*2,0);
    [self.view addSubview:_scrollView];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(40);   make.bottom.offset(0);
        make.left.offset(0);  make.right.offset(0);
    }];
    
}

-(UITableView*)tableView1{
    if (!_tableView1) {
        
        // 奖励消息
        _tableView1 = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView1.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView1.backgroundColor = COLORGROUP ;
        _tableView1.delegate=self; _tableView1.dataSource=self;
        _tableView1.frame = CGRectMake(0, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT-40);
        _tableView1.tag = 101 ;
        [_scrollView addSubview:_tableView1];

        [_tableView1 registerClass:[MsgRewardCell class] forCellReuseIdentifier:@"msgrewardcell"];
        
        __weak MessageVC * selfView = self;
        _tableView1.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            selfView.pageIndex1=1;
            [selfView.tableView1.mj_footer resetNoMoreData];
//            [selfView.reddataAry removeAllObjects];
            [selfView requestMessageData];
        }];
        _tableView1.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pageIndex1++;
            [selfView requestMessageData];
        }];
        if (_selectTag==0) { [_tableView1.mj_header beginRefreshing]; }
        
    }
    return _tableView1 ;
}

-(UITableView*)tableView2{
    if (!_tableView2) {
        // 系统消息
        _tableView2 = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView2.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView2.backgroundColor = COLORGROUP ;
        _tableView2.delegate=self; _tableView2.dataSource=self;
        _tableView2.tag = 102 ;

        if (_scrollView!=nil) {
            _tableView2.frame = CGRectMake(ScreenW, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT-40);
            [_scrollView addSubview:_tableView2];
        }else{
            _tableView2.frame = CGRectMake(0, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT);
            [self.view addSubview:_tableView2];
        }
        
        [_tableView2 registerClass:[SystemMsgCell class] forCellReuseIdentifier:@"systemmsgcell"];
        
        __weak MessageVC * selfView = self;
        _tableView2.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [selfView.tableView2.mj_footer resetNoMoreData];
//            [selfView.sysdataAry removeAllObjects];
            selfView.pageIndex2=1;
            [selfView requestMessageData];
        }];
        _tableView2.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pageIndex2++;
            [selfView requestMessageData];
        }];
        
        
        if (_selectTag==1) { [_tableView2.mj_header beginRefreshing]; }
    }
    return _tableView2 ;
}

#pragma mark ===>>> UIScrollView 代理

// 使用代码改变偏移量时执行
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView.tag==1000) { // 滑动的是scrollView
        NSInteger  stag = scrollView.contentOffset.x/ScreenW ;
        _msgBtn = (UIButton*)[_msgbtnView viewWithTag:stag+10];
        [self clickSelect:_msgBtn];
    }
}
// 手动滑动结束后执行
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag==1000) { // 滑动的是scrollView
        NSInteger  stag = scrollView.contentOffset.x/ScreenW ;
        _msgBtn = (UIButton*)[_msgbtnView viewWithTag:stag+10];
        [self clickSelect:_msgBtn];
    }
}

#pragma mark ===>>> UITableView 代理

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag==101) { return _reddataAry.count; }
    if (tableView.tag==102) { return _sysdataAry.count; }
    return 0 ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==101){
        MsgModel * model = _reddataAry[indexPath.row];
        CGRect rect = StringRect(model.content, ScreenW-143, 14.0*HWB);
        return 70 + rect.size.height ;
    }
    return 66;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView.tag==101) {
        MsgRewardCell * recell = [tableView dequeueReusableCellWithIdentifier:@"msgrewardcell" forIndexPath:indexPath];
        if (_reddataAry.count>indexPath.row) {
            recell.rmodel = _reddataAry[indexPath.row];
        }else{ [_tableView1 reloadData]; }
        return recell;
    }
    if (tableView.tag==102) {
        SystemMsgCell * stcell = [[SystemMsgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"systemmsgcell"];
        if (_sysdataAry.count>indexPath.row) {
            stcell.symodel = _sysdataAry[indexPath.row];
        }else{ [_tableView1 reloadData]; }
        return stcell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==102) {
        SystemMsgModel * model = _sysdataAry[indexPath.row];
        MBShow(@"正在获取...")
        [NetRequest requestType:0 url:url_msg_details ptdic:@{@"id":model.sid} success:^(id nwdic) {
            NSString * htmlString = REPLACENULL(nwdic[@"content"][@"content"]);
            if (htmlString.length>0) {
                WebVC * webV = [WebVC new];
                webV.urlWeb = htmlString;
                webV.nameTitle = @"消息详情";
                PushTo(webV, YES)
            }else{ SHOWMSG(@"当前消息已过期!") }
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            MBHideHUD  SHOWMSG(failure)
        }];

    }
}

#pragma mark ===>>> 没有数据时，提示信息
-(void)creatNodataView{
    if (_scrollView!=nil) {
        _nodataView1 = [[MyCenterNoDataView alloc]init];
        _nodataView1.tishiBtn.hidden = YES ;
        _nodataView1.imgNameStr = @"new_msg";
        _nodataView1.tishiStr = @"你还没有奖励消息";
        [_tableView1 addSubview:_nodataView1];
        [_nodataView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tableView1.mas_centerX);
            make.centerY.equalTo(_tableView1.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        _nodataView1.hidden = YES ;
    }
    
    _nodataView2 = [[MyCenterNoDataView alloc]init];
    _nodataView2.tishiBtn.hidden = YES ;
    _nodataView2.imgNameStr = @"new_msg";
    _nodataView2.tishiStr = @"你还没有邮件";
    [_tableView2 addSubview:_nodataView2];
    [_nodataView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_tableView2.mas_centerX);
        make.centerY.equalTo(_tableView2.mas_centerY).offset(-20*HWB);
        make.height.offset(135*HWB);  make.width.offset(150*HWB);
    }];
    _nodataView2.hidden = YES ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

