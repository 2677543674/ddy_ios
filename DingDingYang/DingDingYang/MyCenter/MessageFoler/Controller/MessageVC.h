//
//  MessageVC.h
//  DingDingYang
//
//  Created by ddy on 2017/3/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalModel.h"
@interface MessageVC : DDYViewController

@property(nonatomic,assign) NSInteger msgtype ; //0:奖励消息 1:系统消息
@property(nonatomic,strong)PersonalModel*modelP;


@end
