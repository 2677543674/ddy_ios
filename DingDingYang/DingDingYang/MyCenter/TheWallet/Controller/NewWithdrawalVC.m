//
//  NewWithdrawalVC.m
//  DingDingYang
//
//  Created by ddy on 2018/1/13.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "NewWithdrawalVC.h"
#import "InformationEditingVC.h"
#import "WdSuccesV.h"
@interface NewWithdrawalVC ()<UITextFieldDelegate>

@property(nonatomic,strong)UIView*backView;//背景框
@property(nonatomic,strong)PersonalModel* model;
@property(nonatomic,strong)UITextField* textField;
@property(nonatomic,strong)UILabel* yue;
@property(nonatomic,strong)UILabel* account;


@end

@implementation NewWithdrawalVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _account.text = [NSString stringWithFormat:@"到账支付宝:  %@",_model.aliNo];
    // 修改支付宝后 从新获取一次服务器
    [PersonalModel getPersonalModel:^(PersonalModel *pModel) {
        _model = pModel ;
        _account.text = [NSString stringWithFormat:@"到账支付宝:  %@",_model.aliNo];
    } failure:^(NSString *fail) { }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"提现";
    self.view.backgroundColor = COLORGROUP;
     _model = [[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
    [self backView];
    [self commitBtn];
}

-(UIView *)backView{
    if (_backView==nil) {
        _backView=[[UIView alloc]init];
        _backView.backgroundColor = COLORWHITE;
        _backView.layer.masksToBounds = YES;
        _backView.layer.cornerRadius = 8*HWB;
        _backView.frame=CGRectMake(10*HWB,15*HWB, ScreenW-20*HWB, 130*HWB);
        [self.view addSubview:_backView];
       
        _account = [UILabel labText:[NSString stringWithFormat:@"到账支付宝:  %@",_model.aliNo] color:Black51 font:13*HWB];
        [_backView addSubview:_account];
        [_account mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB);  make.top.offset(0);
            make.height.offset(30*HWB);
        }];
        
        UILabel* name = [UILabel labText:@"提现余额" color:Black51 font:13*HWB];
        [_backView addSubview:name];
        [name mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB);  make.height.offset(30*HWB);
            make.top.equalTo(_account.mas_bottom);
        }];
        
  
        UIButton * changeBtn = [UIButton titLe:@"修改" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:13*HWB];
        ADDTARGETBUTTON(changeBtn, clickChange)
        [_backView addSubview:changeBtn];
        [changeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_account.mas_right).offset(2);
            make.width.offset(40*HWB);
            make.top.bottom.equalTo(_account);
        }];
       
        UILabel* label=[UILabel labText:@"￥" color:[UIColor blackColor] font:22*HWB];
        [_backView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(name.mas_bottom).offset(0);
            make.left.offset(15*HWB);
            make.height.offset(40*HWB);
            make.width.offset(24*HWB);
        }];
        
        _textField = [UITextField new];
        _textField.placeholder=@"请输入金额";
        _textField.font = [UIFont systemFontOfSize:15*HWB];
        _textField.keyboardType = UIKeyboardTypeDecimalPad ;
        _textField.delegate = self;
        [_backView addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right).offset(10*HWB);
            make.centerY.equalTo(label.mas_centerY);
            make.height.offset(40*HWB);
            make.right.offset(-10*HWB);
        }];
        
        UIView* line = [UIView new];
        line.backgroundColor = COLORGROUP;
        [_backView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_textField.mas_bottom).offset(3*HWB);
            make.left.right.offset(0); make.height.offset(1);
        }];
        
        UILabel* yue = [UILabel labText:[NSString stringWithFormat:@"可提现余额:￥%@",_money] color:Black102 font:13*HWB];
        [_backView addSubview:yue];
        [yue mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom).offset(0);
            make.left.offset(15*HWB);  make.bottom.offset(0);
        }];
    
    }
    return _backView;
}

-(void)clickChange{
    InformationEditingVC * editVC = [InformationEditingVC new];
    editVC.index = 2; editVC.infoModel = _model;
    PushTo(editVC, YES);
}

-(void)commitBtn{
    UIButton * btn = [UIButton new];
    [btn setTitle:@"提现" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:LOGOCOLOR];
    btn.layer.cornerRadius = 19*HWB ;
    btn.clipsToBounds=YES;
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView.mas_bottom).offset(20*HWB);
        make.left.offset(40*HWB); make.right.offset(-40*HWB);
        make.height.offset(38*HWB);
    }];
}

-(void)clickBtn{

    // 返款金额提现
    if (_withdrawalType == 1 ) { [self withdrawRequset:url_withdrawal_daiLi]; }
    // 消费佣金提现
    if (_withdrawalType == 2 ) {
        switch (_platform) { // 京东和唯品会一个接口
            case 1:{ [self withdrawRequset:url_withdrawal_xiaoFei]; } break; // 1:淘宝
            case 2:{ [self withdrawRequset:url_goods_withdraw_wph]; } break; // 2:京东
            case 3:{ [self withdrawRequset:url_withdrawal_pdd];     } break; // 3:拼多多
            case 4:{ [self withdrawRequset:url_goods_withdraw_wph]; } break; // 4:唯品会
            case 5:{ [self withdrawRequset:url_goods_withdraw_wph]; } break; // 5:多麦综合平台
            default:{[self withdrawRequset:url_goods_withdraw_wph]; } break; // 其它提现
        }
    }
    // 拉新提现
    if (_withdrawalType==3) { [self withdrawRequset:url_goods_withdraw_wph]; }
    
}

-(void)withdrawRequset:(NSString *)url{
    
    MBShow(@"申请提现...")
    
    NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
    if (_withdrawalType==2) { pdic[@"type"] = FORMATSTR(@"%ld",(long)_platform); }
    if (_withdrawalType==3) { pdic[@"type"] = @"42"; }
    if (_withdrawalType==2&&_platform==6) { pdic[@"type"] = @"60"; }

    pdic[@"amount"] = _textField.text;  pdic[@"wdBy"] = @"1";
    
    [NetRequest requestType:0 url:url ptdic:pdic success:^(id nwdic) { MBHideHUD
        // 创建提现成功界面
        [WdSuccesV showInView:self.view frame:CGRectMake(0,0, ScreenW, ScreenH-NAVCBAR_HEIGHT) withType:@"1" complet:^(NSInteger tag) {
            if (self.WithdrawalResultBlock) { self.WithdrawalResultBlock(1); }
            PopTo(YES)
        }];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length==0) { return YES ; }
    if ([textField.text isEqualToString:@"0"]&&[string isEqualToString:@"0"]) { return NO ; }
    NSString * tfrepStr = FORMATSTR(@"%@%@",textField.text,string);
    return JUDGESTR(TwoPointNumber,tfrepStr) ;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
