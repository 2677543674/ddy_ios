//
//  NewWithdrawalVC.h
//  DingDingYang
//
//  Created by ddy on 2018/1/13.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewWithdrawalVC : DDYViewController

@property(nonatomic,assign) NSInteger withdrawalType ; // 1:代理费提现 2:消费佣金提现 3: 拉新余额提现
@property(nonatomic,assign) NSInteger platform ; // 1:淘宝  2:京东  3:拼多多 4：唯品会 5: 综合平台提现 6:美团 55淘宝运营商（ withdrawalType 为2才有用）
@property(nonatomic,copy) NSString * money ; // 消费佣金或代理佣金的余额

// 回调1提现成功了需要刷新
@property(nonatomic,copy)void(^WithdrawalResultBlock)(NSInteger resultType);



@end
