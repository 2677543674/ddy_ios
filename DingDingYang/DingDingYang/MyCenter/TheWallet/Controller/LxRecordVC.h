//
//  LxRecordVC.h
//  DingDingYang
//
//  Created by mac on 2019/4/9.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "DDYViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LxRecordVC : DDYViewController

@end


@interface LxRecordSearchView : UIView
@property(nonatomic,strong) UITextField * searchText;
@property(nonatomic,strong) UIButton* searchBtn;
@end


NS_ASSUME_NONNULL_END
