//
//  BangDingPhoneVC.h
//  DingDingYang
//
//  Created by ddy on 2018/9/5.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface BangDingPhoneVC : DDYViewController

@property(nonatomic,copy)void(^BdResultBlock)(BOOL bdResult);

@end
