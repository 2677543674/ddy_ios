//
//  LxRecordVC.m
//  DingDingYang
//
//  Created by mac on 2019/4/9.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "LxRecordVC.h"
#import "LaXInRecordCell.h"
#import "LxRecordModel.h"

@interface LxRecordVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) LxRecordSearchView * searchView;
@property(nonatomic,strong) UITableView * lxTabView;
@property(nonatomic,strong) NSMutableArray * lxDataAey;
@property(nonatomic,assign) NSInteger page;

@end

@implementation LxRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"拉新明细";
    _page = 1;
    _lxDataAey = [NSMutableArray array];
    [self searchView];
    [self lxTabView];
    
}

-(LxRecordSearchView*)searchView{
    if (!_searchView) {
        _searchView = [[LxRecordSearchView alloc]init];
        [self.view addSubview:_searchView];
        [_searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.offset(0);
            make.height.offset(80*HWB);
        }];
        ADDTARGETBUTTON(_searchView.searchBtn, clickSearch)
    }
    return _searchView;
}

-(void)clickSearch{
    ResignFirstResponder
    _page = 1;
//    [self gainLxJiLu];
    [_lxTabView.mj_header beginRefreshing];
}

-(void)gainLxJiLu{
    NSDictionary * pdic = @{@"pageNum":FORMATSTR(@"%ld",(long)_page),@"mobile":REPLACENULL(_searchView.searchText.text)};
    [NetRequest requestType:0 url:uel_lx_record ptdic:pdic success:^(id nwdic) {
        
//        if (_page==1) {
//            [_lxDataAey removeAllObjects];
//            [_lxTabView reloadData];
//        }
//        
        NSArray * dataAry = NULLARY(nwdic[@"list"]);
        [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            LxRecordModel * model = [[LxRecordModel alloc]initWithDic:obj];
            [_lxDataAey addObject:model];
        }];
        [_lxTabView endRefreshType:dataAry.count isReload:YES];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        SHOWMSG(failure)
        [_lxTabView endRefreshType:1 isReload:YES];
    }];
    
    
}


#pragma mark ===>>> UITableView

-(UITableView*)lxTabView{
    if (!_lxTabView) {
        _lxTabView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _lxTabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _lxTabView.delegate = self; _lxTabView.dataSource = self;
        _lxTabView.backgroundColor = COLORWHITE;
        [self.view addSubview:_lxTabView];
        [_lxTabView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.searchView.mas_bottom);
            make.left.right.bottom.offset(0);
        }];
        [_lxTabView registerClass:[LaXInRecordCell class] forCellReuseIdentifier:@"laxin_cell"];
        
        __weak LxRecordVC * selfView = self;

        _lxTabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [selfView.lxDataAey removeAllObjects];
            [selfView.lxTabView.mj_footer resetNoMoreData];
            selfView.page = 1 ;
            [selfView gainLxJiLu];
        }];
        
        _lxTabView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.page++;
            [selfView gainLxJiLu];
        }];
        [_lxTabView.mj_header beginRefreshing];
        
    }
    return _lxTabView;
}

#pragma mark ===>>> cell代理

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _lxDataAey.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    LxRecordModel * lxModel = _lxDataAey[indexPath.row];
    if ([lxModel.clearingTime isEqualToString:@"0"]) { return 60*HWB; }
    return 90*HWB;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LaXInRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:@"laxin_cell" forIndexPath:indexPath];
    if (indexPath.row<_lxDataAey.count) {
        cell.lxModel = _lxDataAey[indexPath.row];
    }
    return cell;
}

// 选中
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    ResignFirstResponder
}

//#pragma mark ===>>> 区头区尾
//// 区头
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 0;
//}
//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    EmptyTabHFView * headEmpty = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"empty_head"];
//    return headEmpty;
//}
//
//// 区尾
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 0;
//}
//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    EmptyTabHFView * footEmpty = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"empty_foot"];
//    return footEmpty;
//}


@end



@implementation LxRecordSearchView

-(instancetype)init{
    self = [super init];
    if (self) {
        
        UIView * backView = [UIView new];
        backView.backgroundColor = LOGOCOLOR ;
        [self addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0);  make.height.offset(80*HWB);
        }];
        
        UIView * searchView = [UIView new];
        searchView.backgroundColor = COLORGROUP;
        searchView.layer.cornerRadius = 16*HWB;
        searchView.clipsToBounds = YES;
        [backView addSubview:searchView];
        [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(32*HWB);
            make.left.offset(30*HWB);
            make.right.offset(-30*HWB);
            make.centerY.equalTo(backView.mas_centerY);
        }];
        
        _searchBtn = [UIButton new];
        [_searchBtn setBackgroundColor:COLORWHITE];
        UIImage * newImg = [[UIImage imageNamed:@"myf_search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_searchBtn setImage:newImg forState:UIControlStateNormal];
        [searchView addSubview:_searchBtn];
        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);   make.bottom.offset(0);
            make.right.offset(0); make.width.offset(55*HWB);
        }];
        
        _searchText = [UITextField textColor:Black51 PlaceHorder:@"手机后四位查询" font:13*HWB];
        _searchText.keyboardType = UIKeyboardTypeNumberPad;
        [searchView addSubview:_searchText];
        [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.centerY.equalTo(searchView.mas_centerY);
            make.right.equalTo(_searchBtn.mas_left).offset(-2);
        }];
        
    }
    return self;
}

@end



