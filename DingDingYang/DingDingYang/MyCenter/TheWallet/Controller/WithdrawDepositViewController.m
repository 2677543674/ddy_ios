//
//  WithdrawDepositViewController.m
//  DingDingYang
//
//  Created by ddy on 26/07/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "WithdrawDepositViewController.h"
#import "WdSuccesV.h"
#import "InformationEditingVC.h"

@interface WithdrawDepositViewController ()<UITextFieldDelegate>

@property(nonatomic,copy) NSString * requestType;

@end

@implementation WithdrawDepositViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"提现";
    
    [self changeLabelFont];
    
    [self getWdBy]; // 查询提现方式
    
    _totalAmountLabel.textColor = LOGOCOLOR ;
    _withdrawalBtn.backgroundColor = LOGOCOLOR ;
    if (THEMECOLOR==2) {
        _totalAmountLabel.textColor = Black51 ;
        [_withdrawalBtn setTitleColor:Black51 forState:UIControlStateNormal];
    }
    
}

-(void)getWdBy{
    //    MBShow(@"正在查询...")
    [NetRequest requestTokenURL:url_withdraw_type parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray *array=nwdic[@"list"];
            for (NSDictionary *dic in array) {
                if ([dic[@"wdBy"] intValue]==2) {
                    self.WeChatBackView.hidden=NO;
                    self.AliBtnTopConstraint.constant=66;
                    
                    if ([_infoModel.ifBindWx isEqualToString:@"1"]) {
                        _requestType=@"2";
                        _WeChatSelectedImg.image=[UIImage imageNamed:@"单选拷贝2"];
                        
                    }else if(_infoModel.aliNo.length>1){
                        _requestType=@"1";
                        _AliSelectedImg.image=[UIImage imageNamed:@"单选拷贝2"];
                    }
                }else{
                    self.WeChatBackView.hidden=YES;
                    self.AliBtnTopConstraint.constant=8;
                    
                    if(_infoModel.aliNo.length>1){
                        _requestType=@"1";
                        _AliSelectedImg.image=[UIImage imageNamed:@"单选拷贝2"];
                        
                    }
                    
                    _AliNameLabel.text=dic[@"name"];
                    _AliDesLabel.text=dic[@"des"];
                }
            }
            
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

-(void)changeLabelFont{
    
    NSMutableAttributedString * noteStr;    NSRange redRange;
    redRange = NSMakeRange(1, [NSString stringWithFormat:@"%d",[_commissionOrWallet intValue]].length);
    noteStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%.2f",[_commissionOrWallet floatValue]]];
    [noteStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:35] range:redRange];
    
    [_totalAmountLabel setAttributedText:noteStr];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)WeChatBtnClick:(id)sender {
    if ([_infoModel.ifBindWx isEqualToString:@"0"]) {
        ALERT(@"您还未绑定微信，请在个人资料中绑定后再提现")
    }else{
        _requestType=@"2";
        _WeChatSelectedImg.image=[UIImage imageNamed:@"单选拷贝2"];
        _AliSelectedImg.image=[UIImage imageNamed:@"单选"];
    }
}
- (IBAction)AliBtnClick:(id)sender {
    if (_infoModel.aliNo.length<1) {
        ALERT(@"您还未绑定支付宝，请在个人资料中绑定后再提现")
    }else{
        _requestType=@"1";
        _AliSelectedImg.image=[UIImage imageNamed:@"单选拷贝2"];
        _WeChatSelectedImg.image=[UIImage imageNamed:@"单选"];
    }
}
- (IBAction)confirmBtnClick:(id)sender {
    
    ResignFirstResponder
    if (_amountTextField.text.length>0) {
        if (!_requestType) {  SHOWMSG(@"亲,请选择提现方式!")   return;  }
        
        if (_withdrawalType == 1 ) {
            if ([_amountTextField.text floatValue]<=[_infoModel.wallet floatValue]) {
                [self withdrawRequset:url_withdrawal_daiLi];
            }else{
                SHOWMSG(@"亲,没那么多余额呦!")
            }
        }else if (_withdrawalType ==2 ) {
            if ([_amountTextField.text floatValue]<=[_commissionOrWallet floatValue]) {
                [self withdrawRequset:url_withdrawal_xiaoFei];
            }else{
                SHOWMSG(@"亲,没那么多余额呦!")
            }
        }else if (_withdrawalType ==3 ) {
            if ([_amountTextField.text floatValue]<=[_commissionOrWallet floatValue]) {
                [self withdrawRequset:url_Withdraw_Deposit];
            }else{
                SHOWMSG(@"亲,没那么多余额呦!")
            }
        }
        
    }else{
        SHOWMSG(@"请输入提现金额!")
    }
}


-(void)withdrawRequset:(NSString *)url{
    
    MBShow(@"申请提现...")
    
    [NetRequest requestTokenURL:url parameter:@{@"amount":_amountTextField.text,@"wdBy":_requestType} success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            //创建提现成功界面
            [WdSuccesV showInView:self.view frame:CGRectMake(0,0, ScreenW, ScreenH-NAVCBAR_HEIGHT) withType:@"1" complet:^(NSInteger tag) {
                PopTo(YES)
            }];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length==0) { return YES;  }
    return JUDGESTR(NUMBERPOINT, string);
}

- (IBAction)keyBoareddown:(UITextField *)sender { }

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
@end

