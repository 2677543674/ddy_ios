//
//  WithdrawDepositViewController.h
//  DingDingYang
//
//  Created by ddy on 26/07/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WithdrawDepositViewController : DDYViewController
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;

@property (weak, nonatomic) IBOutlet UIView *WeChatBackView;
@property (weak, nonatomic) IBOutlet UIButton *WeChatBtn;
- (IBAction)WeChatBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *WeChatSelectedImg;
@property (weak, nonatomic) IBOutlet UILabel *weChatNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *WeChatDesLabel;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *AliBtnTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *AliBtn;
- (IBAction)AliBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *AliSelectedImg;
@property (weak, nonatomic) IBOutlet UILabel *AliNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *AliDesLabel;


@property (weak, nonatomic) IBOutlet UIButton *withdrawalBtn; //提现按钮
- (IBAction)confirmBtnClick:(id)sender;

@property(nonatomic,strong)PersonalModel *infoModel;
@property(nonatomic,copy) NSString * commissionOrWallet ; //消费佣金或代理佣金的余额
@property(nonatomic,assign) NSInteger withdrawalType ; //1:代理费提现 2: 消费佣金提现
@end
