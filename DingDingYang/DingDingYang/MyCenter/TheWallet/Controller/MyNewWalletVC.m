//
//  MyNewWalletVC.m
//  DingDingYang
//
//  Created by ddy on 2018/6/27.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MyNewWalletVC.h"
#import "BillCell.h"
#import "InformationEditingVC.h"
#import "NewWithdrawalVC.h"
#import "MyCenterNoDataView.h"
#import "LxRecordVC.h"

@interface MyNewWalletVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) MyCenterNoDataView * nodataView ;
@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong)UILabel* yueLabel;
@property(nonatomic,strong)UILabel* jiangliyugu;
@property(nonatomic,strong)UILabel* jiangliyugu2;
@property(nonatomic,strong)NSMutableArray* dataAry;
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,assign)BOOL isPush;


@property(nonatomic,strong) UIImageView * topBackImgv; // 顶部背景图
@property(nonatomic,strong) UILabel * fanKuanLab; // 返款

@property(nonatomic,strong) UIView * laXinBackView; // 拉新
@property(nonatomic,strong) UILabel * laXinLab;     // 拉新
@property(nonatomic,strong) UILabel * thisMothLab;  // 本月预估奖励
@property(nonatomic,strong) UILabel * lastMothLab;  // 上月预估奖励

@property(nonatomic,strong) UIButton * withdrawalBtn1; // 返款奖励提现
@property(nonatomic,strong) UIButton * withdrawalBtn2; // 拉新奖励提现


@property(nonatomic,copy) NSString * strMoneyFanKuan;  // 返款
@property(nonatomic,copy) NSString * strMoneyLaXin;    // 拉新
@property(nonatomic,copy) NSString * strMoneyThisMoth; // 本月
@property(nonatomic,copy) NSString * strMoneyLastMoth; // 上月


@end

@implementation MyNewWalletVC

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_isPush==NO) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }else{
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    //点击之后的界面
    _isPush = NO;
    if ([[self.navigationController.viewControllers lastObject] isKindOfClass:[MyFirstNewVC class]] ) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    }else{
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _modelTitle.length==0?@"钱包":_modelTitle;
    self.view.backgroundColor = COLORWHITE;
    _dataAry = [NSMutableArray array];
    _page = 1;
    _isPush=YES;
    [self topView];
    [self tableView];
    [self nodataView];
}


-(void)topView{
    
    _topBackImgv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wallet_backgroud"]];
    _topBackImgv.userInteractionEnabled = YES;
    [self.view addSubview:_topBackImgv];
    [_topBackImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(NAVCBAR_HEIGHT+ScreenW/2);
    }];
    
    
    UILabel* titleLabel = [[UILabel alloc]init];
    titleLabel.text = _modelTitle.length==0?@"钱包":_modelTitle;;
    titleLabel.textColor=COLORWHITE;
    [_topBackImgv addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(44); make.top.offset(NAVCBAR_HEIGHT-44);
        make.centerX.equalTo(_topBackImgv.mas_centerX);
    }];
    
    UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.tintColor = COLORWHITE;
    [backBtn setImage:[[UIImage imageNamed:@"goback"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(13.5, 13, 13.5, 17)];
    backBtn.frame = CGRectMake(0,NAVCBAR_HEIGHT-44, 40, 44);
    ADDTARGETBUTTON(backBtn, clickBack)
    [_topBackImgv addSubview:backBtn];
    
    
    _fanKuanLab = [UILabel labText:@"佣金余额: ￥0.0" color:COLORWHITE font:13*HWB];
    [_topBackImgv addSubview:_fanKuanLab];
    [_fanKuanLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15*HWB);
        make.top.offset(NAVCBAR_HEIGHT+10);
        make.height.offset(26*HWB);
    }];

    _withdrawalBtn1 = [UIButton titLe:@"立即提现" bgColor:RGBA(0, 0, 0, 0.4) titColorN:COLORWHITE font:13*HWB];
    [_withdrawalBtn1 corner:13*HWB];
    ADDTARGETBUTTON(_withdrawalBtn1, clickTianXianFanKuan)
    [_topBackImgv addSubview:_withdrawalBtn1];
    [_withdrawalBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15*HWB);
        make.height.offset(26*HWB);
        make.width.offset(76*HWB);
        make.centerY.equalTo(_fanKuanLab.mas_centerY);
    }];
    

    // 拉新奖励
    _laXinBackView = [[UIView alloc]init];
    [_laXinBackView boardWidth:0.8 boardColor:RGBA(255, 153, 78, 1) cornerRadius:0];
    [_topBackImgv addSubview:_laXinBackView];
    [_laXinBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(16*HWB); make.bottom.offset(1);
        make.right.offset(-16*HWB);
        make.top.offset(NAVCBAR_HEIGHT+20+26*HWB);
    }];
    
    
    UILabel * laxinyue = [UILabel labText:@"拉新余额(元)" color:COLORWHITE font:13*HWB];
    [_laXinBackView addSubview:laxinyue];
    [laxinyue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0); make.height.offset(25*HWB);
        make.centerX.equalTo(_laXinBackView.mas_centerX);
    }];
    
    _laXinLab = [UILabel labText:@"0" color:COLORWHITE font:14*HWB];
    [_laXinBackView addSubview:_laXinLab];
    [_laXinLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(20*HWB); make.height.offset(25*HWB);
        make.centerX.equalTo(_laXinBackView.mas_centerX);
    }];
    
    _thisMothLab = [UILabel labText:@"￥0\n本月预估奖励" color:COLORWHITE font:13*HWB];
    _thisMothLab.textAlignment = NSTextAlignmentCenter;
    _thisMothLab.numberOfLines = 2;
    [_laXinBackView addSubview:_thisMothLab];
    [_thisMothLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0); make.centerY.equalTo(_laXinBackView.mas_centerY);
        make.right.equalTo(_laXinBackView.mas_centerX);
    }];
    
    _lastMothLab = [UILabel labText:@"￥0\n上月预估奖励" color:COLORWHITE font:13*HWB];
    _lastMothLab.textAlignment = NSTextAlignmentCenter;
    _lastMothLab.numberOfLines = 2;
    [_laXinBackView addSubview:_lastMothLab];
    [_lastMothLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(0); make.centerY.equalTo(_laXinBackView.mas_centerY);
        make.left.equalTo(_laXinBackView.mas_centerX);
    }];
    
    _withdrawalBtn2 = [UIButton titLe:@"立即提现" bgColor:RGBA(0, 0, 0, 0.4) titColorN:COLORWHITE font:13*HWB];
    [_withdrawalBtn2 corner:13*HWB];
    ADDTARGETBUTTON(_withdrawalBtn2, clickTiXian)
    [_laXinBackView addSubview:_withdrawalBtn2];
    [_withdrawalBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(26*HWB);
        make.width.offset(76*HWB);
        make.bottom.offset(-10);
        make.centerX.equalTo(_thisMothLab.mas_centerX);
    }];

    UIButton * lxmxBtn = [UIButton titLe:@"拉新明细" bgColor:RGBA(0, 0, 0, 0.4) titColorN:COLORWHITE font:13*HWB];
    [lxmxBtn corner:13*HWB];
    ADDTARGETBUTTON(lxmxBtn, clickLaXinMingXi)
    [_laXinBackView addSubview:lxmxBtn];
    [lxmxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(26*HWB);
        make.width.offset(76*HWB);
        make.bottom.offset(-10);
        make.centerX.equalTo(_lastMothLab.mas_centerX);
    }];
}


-(void)clickBack{ PopTo(YES); }

// 返款提现(原代理佣金提现)
-(void)clickTianXianFanKuan{
    
    [MyPageJump withdrawalType:1 platform:0 totalAmount:_strMoneyFanKuan results:^(NSInteger result) {
        _page = 1;
        _dataAry = [NSMutableArray array];
        [self queryBill];
    }];

}

-(void)clickTiXian{
    [MyPageJump withdrawalType:3 platform:0 totalAmount:_strMoneyLaXin results:^(NSInteger result) {
        _page = 1;
        _dataAry = [NSMutableArray array];
        [self queryBill];
    }];
}

// 拉新明细
-(void)clickLaXinMingXi{
    LxRecordVC * lxvc = [[LxRecordVC alloc]init];
    PushTo(lxvc, YES)
}


#pragma mark ===>>> 无数据时显示
-(MyCenterNoDataView*)nodataView{
    if (!_nodataView) {
        
        _nodataView = [[MyCenterNoDataView alloc]init];
//        _nodataView.imgNameStr = @"我的好友";
//        _nodataView.tishiStr = @"";
        _nodataView.tishiBtn.hidden=YES;
        [_tableView addSubview:_nodataView];
        [_nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tableView.mas_centerX);
            make.centerY.equalTo(_tableView.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        
        _nodataView.hidden = YES ;
        
    }
    return _nodataView ;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor=LinesColor;
        _tableView.separatorColor=[UIColor clearColor];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.rowHeight=60;
        _tableView.tableFooterView=[UIView new];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_topBackImgv.mas_bottom).offset(0);
            make.left.right.bottom.offset(0);
        }];
        
        [_tableView registerClass:[BillCell class] forCellReuseIdentifier:@"BillCell"];
        
        //下拉刷新
        __weak MyNewWalletVC * selfView = self;
        
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            selfView.page=1;
            [selfView.dataAry removeAllObjects];
            [selfView.tableView.mj_footer resetNoMoreData];
            [selfView queryBill];
        }];
        
        _tableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [selfView queryBill];
        }];
        [_tableView.mj_header beginRefreshing];
    }
    return _tableView;
}


-(void)queryBill{
    
    [NetRequest requestType:0 url:url_wallet_details ptdic:@{@"p":FORMATSTR(@"%ld",(long)_page)} success:^(id nwdic) {
        
        _strMoneyFanKuan = ROUNDED(REPLACENULL(nwdic[@"remainder"]), 2);
        _strMoneyLaXin = ROUNDED(REPLACENULL(nwdic[@"lxRemainder"]), 2);
        _strMoneyThisMoth = ROUNDED(REPLACENULL(nwdic[@"lxWallet"]), 2);
        _strMoneyLastMoth = ROUNDED(REPLACENULL(nwdic[@"prelxWallet"]), 2);
        
        _laXinLab.text = _strMoneyLaXin;
        _fanKuanLab.text = FORMATSTR(@"佣金余额: ￥%@",_strMoneyFanKuan);
        _thisMothLab.text = FORMATSTR(@"￥%@\n本月预估奖励",_strMoneyThisMoth);
        _lastMothLab.text = FORMATSTR(@"￥%@\n上月预估奖励",_strMoneyLastMoth);

        NSArray * ary = NULLARY(nwdic[@"list"]) ;

        if (ary.count>0) {
            _nodataView.hidden = NO ;
            _page ++ ;
            NSMutableArray*array = [NSMutableArray array];
            for (NSInteger i=0; i<ary.count; i++) {
                BillModel*model1 = [[BillModel alloc]initWithDic:ary[i]];
                [array addObject:model1];
                if (i==ary.count-1) {
                    if (array.count>0) {
                        [_dataAry addObject:array];
                    }
                }else{
                    //拿model1的日期和model2的日期对比，如果将要解析的数据时间和正在解析的数据不等，清空数组重
                    BillModel * model2 = [[BillModel alloc]initWithDic:ary[i+1]];
                    if (![model2.ymTime isEqual:model1.ymTime]) {
                        NSArray*ary=[NSArray arrayWithArray:array];
                        [_dataAry addObject:ary];
                        [array removeAllObjects];
                    }
                }
            }
        }else{ _nodataView.hidden = YES ; }
        
        [_tableView endRefreshType:ary.count isReload:YES];
        
        if (_dataAry.count==0) { _nodataView.hidden = NO ; }
        else{ _nodataView.hidden = YES ; }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [_tableView endRefreshType:1 isReload:YES];
        SHOWMSG(failure)
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataAry.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableArray*datAry = _dataAry[section];
    return datAry.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BillCell * cell = [tableView dequeueReusableCellWithIdentifier:@"BillCell" forIndexPath:indexPath];
    NSArray* ary = (NSArray*)_dataAry[indexPath.section];
    BillModel * model = ary[indexPath.row];
    cell.model = model;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeadBillCell2 * head = [[HeadBillCell2 alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 30)];
    NSArray * ary = _dataAry[section];
    BillModel*model = ary[0];
    head.timeLab.text=model.ymTime;
    return head ;
}










@end
