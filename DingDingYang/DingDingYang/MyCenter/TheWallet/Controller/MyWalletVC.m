//
//  MyWalletVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/9.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MyWalletVC.h"
#import "TeamRewardsVC.h"
#import "InformationEditingVC.h"
#import "BillVC.h"

#import "NewWithdrawalVC.h"


@interface MyWalletVC ()

@property(nonatomic,strong)UIImageView * tiXianImg;
@property(nonatomic,strong)UILabel * myMoney, * myMoneyNum ;
@property(nonatomic,strong)UIButton * tiXianBtn , * rewardDetail , * settlement ;//提现、奖励明细、账单


//新的钱包页面
@property(nonatomic,strong) UITableView * wltTabView ;
@property(nonatomic,strong) UIView * topView,*moneyView;
@property(nonatomic,strong) UILabel * cmoneyLab; //显示余额
@property(nonatomic,strong) UIButton * makeDrawalBtn; //提现或赚钱
@property(nonatomic,strong) NSMutableArray * walletAry ;

//新旧钱包都使用
@property(nonatomic,strong) NSString * money ; //余额

@end

@implementation MyWalletVC

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self huoquYuE];  // 获取代理佣金消费余额
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _modelTitle.length==0?@"钱包":_modelTitle;
    
    self.view.backgroundColor=COLORWHITE;

    [self oldWallet];
   
//    [self newWallet];

}

//获取代理佣金余额
-(void)huoquYuE{
    [NetRequest requestTokenURL:url_user_money_team parameter:nil success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            _money = ROUNDED(REPLACENULL( nwdic[@"proxyAmount"] ), 3) ;
           _myMoneyNum.text=FORMATSTR(@"¥ %@", ROUNDED(REPLACENULL( nwdic[@"proxyAmount"] ), 3) );
        }else{
            SHOWMSG(nwdic[@"result"])
        }
 } failure:^(NSString *failure) { SHOWMSG(failure) }];
}

#pragma mark ===>>> 旧的钱包(显示的是代理佣金)
-(void)oldWallet{
    _money = _modelInfo.proxyAmount ;
    [self creaLabAndImg];
    [self creatBtn];
}

//代理佣金余额
-(void)creaLabAndImg{
    _myMoneyNum=[[UILabel alloc]init];
    _myMoneyNum.textColor=Black51;
    _myMoneyNum.font=[UIFont systemFontOfSize:30];
    _myMoneyNum.text=FORMATSTR(@"¥ %@",_modelInfo.proxyAmount);
    [self.view addSubview:_myMoneyNum];
    [_myMoneyNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.equalTo(self.view.mas_centerY).offset(-60);
        make.height.offset(26);
    }];
    
    _myMoney=[UILabel labText:@"我的余额" color:Black51 font:15.0];
    [self.view addSubview:_myMoney];
    [_myMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.height.offset(15); make.bottom.equalTo(_myMoneyNum.mas_top).offset(-15);
    }];
    
    _tiXianImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"钱包图标"]];
    [self.view addSubview:_tiXianImg];
    [_tiXianImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(15); make.bottom.equalTo(_myMoney.mas_top).offset(-10);
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.equalTo(_tiXianImg.mas_height);
    }];
}

//按钮
-(void)creatBtn{
    
    _tiXianBtn = [UIButton titLe:@"提现" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGROUP font:14*HWB];
    _tiXianBtn.layer.cornerRadius = 19*HWB ;
    [_tiXianBtn addShadowLayer];
    ADDTARGETBUTTON(_tiXianBtn, tiXianClick)
    [self.view addSubview:_tiXianBtn];
    [_tiXianBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40*HWB); make.right.offset(-40*HWB);
        make.height.offset(38*HWB); make.top.equalTo(_myMoneyNum.mas_bottom).offset(38*HWB);
    }];
    
    
    NSString * redTitle =@"推广奖励明细" ;
    _rewardDetail = [UIButton titLe:redTitle bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:COLORGRAY font:14*HWB];
    

    [_rewardDetail boardWidth:1 boardColor:LOGOCOLOR corner:19*HWB];
    [_rewardDetail addShadowLayer];
    ADDTARGETBUTTON(_rewardDetail, toViewrewardDetails)
    [self.view addSubview:_rewardDetail];
    [_rewardDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40*HWB); make.right.offset(-40*HWB);
        make.height.offset(38*HWB); make.top.equalTo(_tiXianBtn.mas_bottom).offset(20*HWB);
    }];
    
    _settlement = [UIButton titLe:@"我的账单" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:COLORGRAY font:14*HWB];
    [_settlement boardWidth:1 boardColor:LOGOCOLOR corner:19*HWB];
    [_settlement addShadowLayer];
    ADDTARGETBUTTON(_settlement, settlementDetails)
    [self.view addSubview:_settlement];
    [_settlement mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40*HWB); make.right.offset(-40*HWB);
        make.height.offset(38*HWB); make.top.equalTo(_rewardDetail.mas_bottom).offset(20*HWB);

    }];
    
    if (THEMECOLOR==2) {
        [_tiXianBtn setTitleColor:Black51 forState:UIControlStateNormal];
        [_rewardDetail setTitleColor:Black51 forState:UIControlStateNormal];
        [_settlement setTitleColor:Black51 forState:UIControlStateNormal];
    }
    if (CANOPENWechat==NO) {
        _rewardDetail.hidden=YES;
        [_settlement mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(40*HWB); make.right.offset(-40*HWB);
            make.height.offset(38*HWB); make.top.equalTo(_tiXianBtn.mas_bottom).offset(20*HWB);
        }];
    }
}


#pragma mark === >>> 按钮点击事件
// 提现
-(void)tiXianClick{

    [MyPageJump withdrawalType:1 platform:0 totalAmount:_money results:^(NSInteger result) {
        
    }];
    
    /*
    if (_modelInfo.aliNo.length<1) {
        if (role_state==0&&PID==10104) { // 蜜赚需要先升级代理绑定手机号(绑手机号需要升级代理)，才能绑定支付宝，
            [MyPageJump pushNextVcByModel:nil byType:52];
        }else{
            InformationEditingVC*alipayEnditingVC = [InformationEditingVC new];
            alipayEnditingVC.index = 2;
            alipayEnditingVC.infoModel = _modelInfo;
            PushTo(alipayEnditingVC, YES);
        }
    
    }else{
        if ([_money floatValue]==0) {
            SHOWMSG(@"余额不足!")
            return ;
        }
        MBShow(@"正在检测...")
        [NetRequest requestTokenURL:url_check_wd_daiLi parameter:nil success:^(NSDictionary *nwdic) {
            MBHideHUD
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSInteger ifWd = [REPLACENULL(nwdic[@"ifWd"])  integerValue];
                if(ifWd==1){
                    NewWithdrawalVC * newVC = [NewWithdrawalVC new];
                    newVC.withdrawalType = 1 ; //代理佣金提现
                    newVC.money = _money ;
                    PushTo(newVC, YES);
                }else{
                    SHOWMSG(nwdic[@"msg"])
                }

            }else{
                SHOWMSG(nwdic[@"result"])
            }
        } failure:^(NSString *failure) {
            MBHideHUD
            SHOWMSG(failure)
        }];
    }*/

}

-(NSString*)weekdayString{
    
    NSArray *weekdays = @[@"周日",@"周一", @"周二", @"周三", @"周四", @"周五", @"周六"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:[NSDate date]];

    if (theComponents.weekday-1>6) {
        return @"周日";
    }
    return [weekdays objectAtIndex:theComponents.weekday-1];
}


//代理商推广明细奖励
-(void)toViewrewardDetails{
    TeamRewardsVC * teamR = [TeamRewardsVC new];
    teamR.model = _modelInfo;
    teamR.hidesBottomBarWhenPushed = YES ;
    PushTo(teamR, YES);
}

//我的账单
-(void)settlementDetails{
    BillVC*bill = [BillVC new];
    bill.billType = 2 ;
    PushTo(bill, YES)
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




@end
