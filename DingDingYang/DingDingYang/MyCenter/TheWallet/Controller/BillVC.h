//
//  BillVC.h
//  DingDingYang
//
//  Created by ddy on 2017/4/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillVC : DDYViewController
 // 1：淘宝 2：京东  3：拼多多  4唯品会 5:综合平台 6:美团 55:淘宝运营商账单
@property(nonatomic,assign) NSInteger billType ;
@end


//测试数据

/*
 NSArray*ary=@[@{@"amount":@"100",@"createTime":@"1490936769315",@"type":@"1"},
 @{@"amount":@"200",@"createTime":@"1488481383000",@"type":@"2"},
 @{@"amount":@"300",@"createTime":@"1488567783000",@"type":@"3"},
 @{@"amount":@"400",@"createTime":@"1487530983000",@"type":@"4"},
 @{@"amount":@"500",@"createTime":@"1487487902000",@"type":@"4"},
 @{@"amount":@"600",@"createTime":@"1486339688000",@"type":@"3"},
 @{@"amount":@"700",@"createTime":@"1484561898000",@"type":@"2"},
 @{@"amount":@"800",@"createTime":@"1484565498000",@"type":@"1"},
 @{@"amount":@"900",@"createTime":@"1484219898000",@"type":@"1"},
 @{@"amount":@"101",@"createTime":@"1481541498000",@"type":@"2"},
 @{@"amount":@"102",@"createTime":@"1481515932000",@"type":@"3"},
 @{@"amount":@"103",@"createTime":@"1478833871000",@"type":@"4"},
 @{@"amount":@"104",@"createTime":@"1476065410000",@"type":@"1"},
 @{@"amount":@"105",@"createTime":@"1476155470000",@"type":@"2"},
 @{@"amount":@"106",@"createTime":@"1470885070000",@"type":@"3"},
 @{@"amount":@"107",@"createTime":@"1470614888000",@"type":@"4"},
 @{@"amount":@"108",@"createTime":@"1465164366000",@"type":@"4"},
 @{@"amount":@"109",@"createTime":@"1457215566000",@"type":@"2"},
 @{@"amount":@"110",@"createTime":@"1425593166000",@"type":@"1"},
 @{@"amount":@"120",@"createTime":@"1425593066000",@"type":@"1"},
 @{@"amount":@"130",@"createTime":@"1425592066000",@"type":@"1"},
 @{@"amount":@"140",@"createTime":@"1425591066000",@"type":@"1"},
 ];
 
 
 NSArray*ary=@[@{@"amount":@"6666",@"createTime":@"1425581066000",@"type":@"1"},
 @{@"amount":@"200",@"createTime":@"1425571066000",@"type":@"2"},
 @{@"amount":@"300",@"createTime":@"1425561066000",@"type":@"3"},
 @{@"amount":@"400",@"createTime":@"1425551066000",@"type":@"4"},
 @{@"amount":@"500",@"createTime":@"1425541066000",@"type":@"4"},
 @{@"amount":@"600",@"createTime":@"1425531066000",@"type":@"3"},
 @{@"amount":@"700",@"createTime":@"1425521066000",@"type":@"2"},
 @{@"amount":@"800",@"createTime":@"1425511066000",@"type":@"1"},
 @{@"amount":@"900",@"createTime":@"1425501066000",@"type":@"1"},
 @{@"amount":@"101",@"createTime":@"1415591066000",@"type":@"2"},
 @{@"amount":@"102",@"createTime":@"1415581066000",@"type":@"3"},
 @{@"amount":@"103",@"createTime":@"1425491066000",@"type":@"4"},
 @{@"amount":@"104",@"createTime":@"1424591066000",@"type":@"1"},
 @{@"amount":@"105",@"createTime":@"1415591066000",@"type":@"2"},
 @{@"amount":@"106",@"createTime":@"1325591066000",@"type":@"3"},
 @{@"amount":@"107",@"createTime":@"1325581066000",@"type":@"4"},
 @{@"amount":@"108",@"createTime":@"1325571066000",@"type":@"4"},
 @{@"amount":@"109",@"createTime":@"1325591026000",@"type":@"2"},
 @{@"amount":@"110",@"createTime":@"1325591016000",@"type":@"1"},
 @{@"amount":@"120",@"createTime":@"1325590066000",@"type":@"1"},
 @{@"amount":@"130",@"createTime":@"1225591066000",@"type":@"1"},
 @{@"amount":@"140",@"createTime":@"1221591066000",@"type":@"1"},
 ];
 */
