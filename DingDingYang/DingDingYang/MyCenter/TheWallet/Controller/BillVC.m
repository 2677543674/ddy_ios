//
//  BillVC.m
//  DingDingYang
//
//  Created by ddy on 2017/4/5.
//  Copyright © 2017年 ddy.All rights reserved.
// 账单

#import "BillVC.h"
#import "BillCell.h"
#import "BillModel.h"

@interface BillVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) MyCenterNoDataView * nodataView ;
@property(nonatomic,strong) NSMutableDictionary * parameter ;
@property(nonatomic,strong) NSMutableArray * dataAry;
@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,assign) NSInteger page;

@end

@implementation BillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLORWHITE;
    self.title = @"我的账单";
    _dataAry = [NSMutableArray array];
    
    [self parameter];
    
    [self tableView];
    
    [self nodataView];
    
}

-(NSMutableDictionary*)parameter{
    if (!_parameter) {
        _page = 1 ;
        _parameter = [NSMutableDictionary dictionary];
        _parameter[@"p"] = @"1" ;
        if (_billType==5||_billType==55) {
            _parameter[@"type"] = FORMATSTR(@"%ld",(long)_billType);
        }else if(_billType==6){
            _parameter[@"type"] = @"60";
        }else{
            _parameter[@"billType"] = FORMATSTR(@"%ld",(long)_billType);
        }
    }
    return _parameter ;
}


#pragma mark -- tableView表格
-(UITableView*)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0,0, ScreenW, ScreenH-NAVCBAR_HEIGHT) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self; _tableView.dataSource=self;
        _tableView.rowHeight = 60;
        [self.view addSubview:_tableView];
        [_tableView registerClass:[BillCell class] forCellReuseIdentifier:@"BillCell"];
        [_tableView registerClass:[HeadBillCell class] forHeaderFooterViewReuseIdentifier:@"HeadBillCell"];
        
        //下拉刷新
        __weak BillVC * selfView = self;
//        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(queryBill)];
        
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [selfView.dataAry removeAllObjects];
            [selfView.tableView.mj_footer resetNoMoreData];
            selfView.page = 1 ;
            selfView.parameter[@"p"] = FORMATSTR(@"%ld",(long)selfView.page);
            [selfView queryBill];
        }];
        
        _tableView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.page++;
            selfView.parameter[@"p"] = FORMATSTR(@"%ld",(long)selfView.page);
            [selfView queryBill];
        }];
        [_tableView.mj_header beginRefreshing];
    }
    return _tableView ;
}

#pragma mark --> 查询交易记录
-(void)queryBill{
    
    NSString * billUrl = url_wallet_details;
    if (_billType==5) { billUrl = url_dm_bill; }
    [NetRequest requestType:0 url:billUrl ptdic:_parameter success:^(id nwdic) {
        NSArray * ary = NULLARY(nwdic[@"list"]);

        NSMutableArray * array = [NSMutableArray array];
        for (NSInteger i=0; i<ary.count; i++) {
            BillModel * model1 = [[BillModel alloc]initWithDic:ary[i]];
            [array addObject:model1];
            if (i==ary.count-1) {
                if (array.count>0) {
                    [_dataAry addObject:array];
                }
            }else{
                //拿model1的日期和model2的日期对比，如果将要解析的数据时间和正在解析的数据不等，清空数组重
                BillModel * model2 = [[BillModel alloc]initWithDic:ary[i+1]];
                if (![model2.ymTime isEqual:model1.ymTime]) {
                    NSArray*ary=[NSArray arrayWithArray:array];
                    [_dataAry addObject:ary];
                    [array removeAllObjects];
                }
            }
        }
        [_tableView endRefreshType:1 isReload:YES];
        if (_dataAry.count==0) { _nodataView.hidden = NO ; }
        else{ _nodataView.hidden = YES ; }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        SHOWMSG(failure)  [_tableView endRefreshType:1 isReload:NO];
    }];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataAry.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 32;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableArray * datAry = _dataAry[section];
    return datAry.count;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSArray * ary = _dataAry[section];
    BillModel*model = ary[0];
    NSString * title = model.ymTime ;
    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BillCell*cell = [tableView dequeueReusableCellWithIdentifier:@"BillCell" forIndexPath:indexPath];
    NSArray*ary = (NSArray*)_dataAry[indexPath.section];
    BillModel * model = ary[indexPath.row];
    cell.model = model;
    return cell;
}


#pragma mark ===>>> 没有数据时，提示去推荐好友
-(MyCenterNoDataView*)nodataView{
    if (!_nodataView) {
        
        _nodataView = [[MyCenterNoDataView alloc]init];
        _nodataView.tishiBtn.hidden = YES ;
        _nodataView.imgNameStr = @"暂无记录";
        _nodataView.tishiStr = @"暂无记录";
        [_tableView addSubview:_nodataView];
        [_nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tableView.mas_centerX);
            make.centerY.equalTo(_tableView.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
        
        _nodataView.hidden = YES ;
        
    }
    return _nodataView ;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
