//
//  BillModel.h
//  DingDingYang
//
//  Created by ddy on 2017/4/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BillModel : NSObject

@property(nonatomic,assign) NSInteger  billType;  // 类型1:代理商团队奖励(+) 2:提成(-) 3:提成失败返还(+)  4:消费佣金提成(+)
@property(nonatomic,copy) NSString * typeName ;   // 类型名称(服务器返回)
@property(nonatomic,copy) NSString * amount;      // 金额
@property(nonatomic,copy) NSString * createTime;  // 时间(毫秒)
@property(nonatomic,copy) NSString * ymTime;      // 年月字符串(进行月份分割)



-(instancetype)initWithDic:(NSDictionary*)dic;

@end
