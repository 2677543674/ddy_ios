//
//  LxRecordModel.h
//  DingDingYang
//
//  Created by mac on 2019/4/9.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LxRecordModel : NSObject


@property(nonatomic,copy) NSString * activityId; // 活动类型
@property(nonatomic,copy) NSString * amount;     // 金额
@property(nonatomic,copy) NSString * doTime;     // 创建时间
@property(nonatomic,copy) NSString * lxid;       //
@property(nonatomic,copy) NSString * mobile;     // 手机号
@property(nonatomic,copy) NSString * status;     // 结算类型
@property(nonatomic,copy) NSString * statusStr;  // 结算类型文字:待审核、结算、失效
@property(nonatomic,copy) NSString * type;       // 拉新类型
@property(nonatomic,copy) NSString * typeStr;    // 拉新类型:首购、绑卡
@property(nonatomic,copy) NSString * clearingTime; // 结算时间

-(instancetype)initWithDic:(NSDictionary*)dic;


@end

NS_ASSUME_NONNULL_END
