//
//  LxRecordModel.m
//  DingDingYang
//
//  Created by mac on 2019/4/9.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "LxRecordModel.h"

@implementation LxRecordModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        
        self.activityId = REPLACENULL(dic[@"activityId"]);
        self.amount = REPLACENULL(dic[@"amount"]);
        self.doTime = REPLACENULL(dic[@"doTime"]);
        self.lxid = REPLACENULL(dic[@"id"]);
        self.mobile = REPLACENULL(dic[@"mobile"]);
        self.status = REPLACENULL(dic[@"status"]);
        self.statusStr = REPLACENULL(dic[@"statusStr"]);
        self.type = REPLACENULL(dic[@"type"]);
        self.typeStr = REPLACENULL(dic[@"typeStr"]);
        self.clearingTime = REPLACENULL(dic[@"clearingTime"]);
        
    }
    return self;
}
@end
