//
//  BillModel.m
//  DingDingYang
//
//  Created by ddy on 2017/4/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "BillModel.h"

@implementation BillModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        
        self.amount = ROUNDED(REPLACENULL(dic[@"amount"]), 2) ;
        
        NSString*timeStr = TimeByStamp(REPLACENULL(dic[@"createTime"]),@"yyyy年MM月dd日 HH:mm");
       
        self.createTime = timeStr;
        
        self.billType = [dic[@"type"] integerValue];
        
        self.ymTime = TimeByStamp(REPLACENULL(dic[@"createTime"]),@"yyyy年 MM月");

        self.typeName = REPLACENULL(dic[@"typeName"]) ;
    }
    return self;
}

@end
