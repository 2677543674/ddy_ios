//
//  WdSuccesV.h
//  DingDingYang
//
//  Created by ddy on 2017/3/22.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WdSuccesV : UIView
@property(nonatomic,strong)void(^blockTag)(NSInteger tag);
@property(nonatomic,strong)NSString *type;
+(void)showInView:(UIView*)view frame:(CGRect)rectF withType:(NSString *)type complet:(void(^)(NSInteger tag))blockTag;
@end
