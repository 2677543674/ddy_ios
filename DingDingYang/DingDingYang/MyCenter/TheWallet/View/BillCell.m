//
//  BillCell.m
//  DingDingYang
//
//  Created by ddy on 2017/4/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "BillCell.h"

@implementation BillCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _typeLab = [UILabel labText:@"" color:Black51 font:12*HWB];
        [self.contentView addSubview:_typeLab];
        [_typeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.height.offset(16);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(-4);
        }];
        
        _timeLab = [UILabel labText:@"" color:Black153 font:11*HWB];
        [self.contentView addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.top.equalTo(self.mas_centerY).offset(4);
            make.height.offset(15);
        }];
        
        _moneyLab = [UILabel labText:@"" color:COLORBLACK font:12*HWB];
        _moneyLab.font = [UIFont boldSystemFontOfSize:12*HWB];
        [self.contentView addSubview:_moneyLab];
        [_moneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-12); make.height.offset(16);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
        
        //表格分割线
        UIImageView*lineImg=[[UIImageView alloc]init];
        lineImg.backgroundColor=COLORGROUP;
        lineImg.frame=CGRectMake(0, 59, ScreenW, 1);
        [self.contentView addSubview:lineImg];
        
        if (THEMECOLOR==2) {
            _typeLab.textColor = OtherColor ;
            _moneyLab.textColor = OtherColor ;
        }
    }
    return self;
}

-(void)setModel:(BillModel *)model{
    _model = model;
    _timeLab.text = _model.createTime;
    
    _moneyLab.text = _model.amount ;
    if ([_model.amount floatValue]>0&&![_model.amount hasPrefix:@"+"]) {
        _moneyLab.text = FORMATSTR(@"+%@",_model.amount);
    }
    
    if (_model.typeName.length>0) { // 交易类型服务器返回了
        _typeLab.text = _model.typeName ;
    }else{
        switch (model.billType) {
            case  1: {
                _typeLab.text = @"代理商团队奖励";
                _moneyLab.text = FORMATSTR(@"+%@",model.amount);
            } break;
                
            case  2: {
                _typeLab.text = @"代理佣金提现";
                if ([model.amount containsString:@"-"]) {
                    _moneyLab.text = _model.amount ;
                }else{
                    _moneyLab.text = FORMATSTR(@"-%@",model.amount);
                }
                
            } break;
                
            case  3: {
                _typeLab.text = @"提现失败返还金额";
                _moneyLab.text = FORMATSTR(@"+%@",model.amount);
            } break;
                
            case  4: {
                _typeLab.text = @"消费佣金提成";
                _moneyLab.text = FORMATSTR(@"+%@",model.amount);
            } break;
                
            case  5: {
                _typeLab.text = @"消费佣金提现";
                if ([model.amount containsString:@"-"]) {
                    _moneyLab.text = _model.amount ;
                }else{
                    _moneyLab.text = FORMATSTR(@"-%@",model.amount);
                }
            } break;
                
            default:{
                _typeLab.text = @"";
            } break;
        }
    }
    

    
}


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end


@implementation HeadBillCell
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = LinesColor;
        self.timeLab = [UILabel labText:@"2017" color:Black102 font:FONT_17];
        [self.contentView addSubview:_timeLab];
    }
    return self;
}
@end


@implementation HeadBillCell2
-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = LinesColor;
        self.timeLab = [UILabel labText:@"2017" color:Black102 font:11*HWB];
        [self addSubview:_timeLab];
        [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.bottom.offset(0);
            make.left.offset(15);
        }];
    }
    return self;
}


//-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
//    self=[super initWithReuseIdentifier:reuseIdentifier];
//    if (self) {
//        self.backgroundColor = LinesColor;
//        self.timeLab = [UILabel labText:@"2017" color:Black102 font:11*HWB];
//        [self.contentView addSubview:_timeLab];
//    }
//    return self;
//}
@end








