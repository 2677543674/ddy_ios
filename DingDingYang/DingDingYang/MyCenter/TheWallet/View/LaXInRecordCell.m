//
//  LaXInRecordCell.m
//  DingDingYang
//
//  Created by mac on 2019/4/9.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "LaXInRecordCell.h"

@implementation LaXInRecordCell

-(void)setLxModel:(LxRecordModel *)lxModel{
    _lxModel = lxModel;
    
    _phoneLab.text = _lxModel.mobile;
    _jiangliLab.text = FORMATSTR(@"%@ %@",_lxModel.typeStr,_lxModel.amount);
    _creatTimeLab.text = FORMATSTR(@"创建时间: %@",TimeByStamp(_lxModel.doTime, @"yyyy-MM-dd hh:ss:SSS"));
    _stateLab.text = FORMATSTR(@"状态: %@",_lxModel.statusStr);
    
    if ([_lxModel.clearingTime isEqualToString:@"0"]) {
        _creatTimeLab.hidden = YES;
        _endTimeLab.text = FORMATSTR(@"创建时间: %@",TimeByStamp(_lxModel.doTime, @"yyyy-MM-dd hh:ss:SSS"));
    }else{
        _creatTimeLab.hidden = NO;
        _endTimeLab.text = FORMATSTR(@"结算时间: %@",TimeByStamp(_lxModel.clearingTime, @"yyyy-MM-dd hh:ss:SSS"));
    }
    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _phoneLab = [UILabel labText:@"****" color:Black51 font:13*HWB];
        [self.contentView addSubview:_phoneLab];
        [_phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(12*HWB);
            make.left.offset(10*HWB);
            make.height.offset(15*HWB);
        }];
        
        _jiangliLab = [UILabel labText:@"首购 " color:Black51 font:13*HWB];
        [self.contentView addSubview:_jiangliLab];
        [_jiangliLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_phoneLab.mas_right).offset(10*HWB);
            make.centerY.equalTo(_phoneLab.mas_centerY);
        }];
        
        _creatTimeLab = [UILabel labText:@"创建时间: " color:Black153 font:12.5*HWB];
        [self.contentView addSubview:_creatTimeLab];
        [_creatTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
        
        _endTimeLab = [UILabel labText:@"结算时间: " color:Black153 font:12.5*HWB];
        [self.contentView addSubview:_endTimeLab];
        [_endTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.bottom.offset(-12*HWB);
        }];
        
        _stateLab = [UILabel labText:@"状态: " color:Black153 font:12.5*HWB];
        [self.contentView addSubview:_stateLab];
        [_stateLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-12*HWB);
            make.left.equalTo(_endTimeLab.mas_right).offset(20*HWB);
//            make.right.offset(-15*HWB);
        }];
        
        UIView * linesView = [[UIView alloc]init];
        linesView.backgroundColor = Black204;
        [self.contentView addSubview:linesView];
        [linesView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.offset(0);
            make.height.offset(0.5*HWB);
        }];
        
    }
    return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
