//
//  WdSuccesV.m
//  DingDingYang
//
//  Created by ddy on 2017/3/22.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "WdSuccesV.h"

@implementation WdSuccesV
+(void)showInView:(UIView*)view frame:(CGRect)rectF withType:(NSString *)type complet:(void(^)(NSInteger tag))blockTag{
    WdSuccesV*wdview=[[WdSuccesV alloc]initWithFrame:rectF];
    wdview.backgroundColor=COLORWHITE;
    wdview.blockTag=blockTag;
    wdview.type=type;
    [view addSubview:wdview];
    [wdview addView];
}
-(void)addView{
    
    
    UILabel*centerLab=[UILabel labText:@"将在48小时内收到款项，请留意支付宝！" color:Black102 font:FONT_14];
    if ([_type isEqualToString:@"2"]) {
        centerLab.text=@"将在48小时内收到款项，请留意微信钱包！";
    }
    [self addSubview:centerLab];
    [centerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.bottom.equalTo(self.mas_centerY).offset(-30);
        make.height.offset(14);
    }];
    
    UILabel*succesLab=[UILabel labText:@"提现申请成功" color:[UIColor blackColor] font:26];
    [self  addSubview:succesLab];
    [succesLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.bottom.equalTo(centerLab.mas_top).offset(-36);
        make.height.offset(25);
    }];
    
    UIImageView*imgView=[[UIImageView alloc]init];
    imgView.image=[UIImage imageNamed:@"txchengong"];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(50); make.bottom.equalTo(succesLab.mas_top).offset(-40);
        make.centerX.equalTo(self.mas_centerX); make.width.equalTo(imgView.mas_height);
    }];

    UIButton*tankBtn=[UIButton titLe:@"谢主隆恩" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:[UIColor grayColor] font:FONT_16];
    tankBtn.layer.cornerRadius=6;
    [tankBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:tankBtn];
    [tankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40);  make.right.offset(-40);
        make.height.offset(46); make.top.equalTo(self.mas_centerY);
    }];
}
-(void)clickBack
{
    self.blockTag(1);
}
@end
