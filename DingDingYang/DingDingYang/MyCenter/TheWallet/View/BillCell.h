//
//  BillCell.h
//  DingDingYang
//
//  Created by ddy on 2017/4/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BillModel.h"
@interface BillCell : UITableViewCell

@property(nonatomic,strong)UILabel*typeLab;//类型
@property(nonatomic,strong)UILabel*timeLab;//产生时间
@property(nonatomic,strong)UILabel*moneyLab;//变动的金额
@property(nonatomic,strong)BillModel*model;

@end



@interface HeadBillCell : UITableViewHeaderFooterView
@property(nonatomic,strong)UILabel*timeLab;//时间(月份)
@end


@interface HeadBillCell2 : UIView
@property(nonatomic,strong)UILabel*timeLab;//时间(月份)
@end


