//
//  LaXInRecordCell.h
//  DingDingYang
//
//  Created by mac on 2019/4/9.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LxRecordModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LaXInRecordCell : UITableViewCell

@property(nonatomic,strong) UILabel * phoneLab;
@property(nonatomic,strong) UILabel * jiangliLab;
@property(nonatomic,strong) UILabel * creatTimeLab;
@property(nonatomic,strong) UILabel * endTimeLab;
@property(nonatomic,strong) UILabel * stateLab;

@property(nonatomic,strong) LxRecordModel * lxModel;

@end

NS_ASSUME_NONNULL_END
