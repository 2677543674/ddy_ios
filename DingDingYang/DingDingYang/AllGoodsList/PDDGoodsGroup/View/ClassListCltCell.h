//
//  ClassListCltCell.h
//  DingDingYang
//
//  Created by ddy on 2018/3/15.
//

#import <UIKit/UIKit.h>

@class ClassListCltCell;
@protocol ClassListCltCellDelegate <NSObject>
// datadic: 已请求过的数据和当前使用的参数
-(void)nowCell:(ClassListCltCell*)ctcell nowClassTypeAllData:(NSDictionary*)datadic;
// 记录偏移量
-(void)nowCell:(ClassListCltCell*)ctcell offsetY:(NSString*)floaty;
// 返回已选商品或删除商品
-(void)nowCell:(ClassListCltCell*)ctcell selectGoodsModel:(GoodsModel*)model isSelect:(BOOL)select;
// 将要开始拖动
-(void)beginDragNowCell:(ClassListCltCell *)ctcell;

@end

@interface ClassListCltCell : UICollectionViewCell <SortingSelectDelegatePDD,UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionViewFlowLayout * layout;
@property(nonatomic,strong) UICollectionView * classCltView;
@property(nonatomic,copy)   NSString * defultName;
@property(nonatomic,assign) id<ClassListCltCellDelegate>delegate;

// 记录数据
@property(nonatomic,assign) BOOL isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典 

@property(nonatomic,copy)   NSDictionary * allDic ; // 请求参数和商品列表组成的数据
@property(nonatomic,strong) NSMutableDictionary * parameter ; // 请求参数
@property(nonatomic,strong) NSMutableArray * bannerListAry; // banner图数组
@property(nonatomic,strong) NSMutableArray * classListAry ; // 商品列表
@property(nonatomic,assign) NSInteger  pageNum; // 页面
@property(nonatomic,copy) NSString * selectType; // 选中的排序类型
@property(nonatomic,copy) NSString * lastStopY; // 记录滑动停止位置(刷新时定位用)
@property(nonatomic,assign) float scrollStopY; // 记录滑动停止位置(判断是在上拉还是下拉使用)

@end



