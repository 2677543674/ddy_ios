//
//  ClassListCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/3/15.
//  能左右滑动的商品列表cell

#import "ClassListCltCell.h"
#import "HOneCltCell1.h"
#import "PDDGoodsCltCellH.h"

@implementation ClassListCltCell

// 是否开始选择
-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    [_classCltView reloadData];
}

-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}

-(void)setLastStopY:(NSString *)lastStopY{
    _lastStopY = lastStopY;
}
-(void)setAllDic:(NSDictionary *)allDic{
    @try {
        _allDic = allDic;
        _pageNum = [allDic[@"parameter"][@"st"] integerValue];
        
        NSDictionary * pdic = NULLDIC(allDic[@"parameter"]);
        _parameter = [NSMutableDictionary dictionaryWithDictionary:pdic];
        
        _selectType = REPLACENULL(_parameter[@"sortType"]);
        
        NSMutableArray * bannerAry = [NSMutableArray arrayWithArray:NULLARY(allDic[@"banner"])];
        if (bannerAry.count>0) {
            _bannerListAry = [NSMutableArray arrayWithArray:bannerAry];
        }else{ _bannerListAry = [NSMutableArray array]; }
        
        NSMutableArray * ary = [NSMutableArray arrayWithArray:NULLARY(allDic[@"dataList"])];
        if (ary.count==0) {
            _classListAry = [NSMutableArray array];
            [_classCltView reloadData];
            [_classCltView.mj_header beginRefreshing];
        }else{
            _classListAry = [NSMutableArray arrayWithArray:ary];
            if ([_lastStopY floatValue]>0) {
                [_classCltView setContentOffset:CGPointMake(0, [_lastStopY floatValue])];
            }
            [_classCltView reloadData];
        }
        // LOG(@"分类名称:%@  已加入的数组个数:%lu  请求所用参数:%@",allDic[@"title"],(unsigned long)ary.count,pdic)

    } @catch (NSException *exception) {
        SHOWMSG(@"数据出错了,建议关闭并重新打开本页面!")
    } @finally {
        [_classCltView.mj_footer resetNoMoreData];
    }
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _pageNum = 1 ;

        [self classCltView];
    }
    return self ;
}

-(void)loadClassGoodsData{
    
    [NetRequest requestTokenURL:url_goods_list_pdd parameter:_parameter success:^(NSDictionary *nwdic) {
        MBHideHUD
        @try {
            
            if (_pageNum==1) {
                NSArray * bannerAry = NULLARY(nwdic[@"bannerList"]);
                if (_bannerListAry.count>0) { [_bannerListAry removeAllObjects]; }
                [bannerAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    BHModel * model = [[BHModel alloc]initWithDic:obj];
                    [_bannerListAry addObject:model];
                }];
                [_classCltView reloadData];
            }
            
            NSArray * listAry = NULLARY(nwdic[@"list"]);
            [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsModel * model = [[GoodsModel alloc]initWithDic:obj];
                model.platform = @"拼多多";
                [_classListAry addObject:model];
//                NSIndexPath * indexPath = [NSIndexPath indexPathForItem:_classListAry.count-1 inSection:1];
//                [_classCltView insertItemsAtIndexPaths:@[indexPath]];
            }];
            
            [_classCltView endRefreshType:listAry.count isReload:YES];
            
        } @catch (NSException *exception) {
//            NSArray * listAry = NULLARY(nwdic[@"list"]);
//            [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                GoodsModel * model = [[GoodsModel alloc]initWithDic:obj];
//                [_classListAry addObject:model];
//            }];
//            [_classCltView endRefreshType:listAry.count isReload:YES];
        } @finally {
            @try {
                NSDictionary * pdic = (NSDictionary*)_parameter;
                NSArray * aryList = (NSArray*)_classListAry;
                NSArray * banList = (NSArray*)_bannerListAry;
                NSDictionary * dic = @{@"parameter":pdic,@"banner":banList,@"dataList":aryList};
                [self.delegate nowCell:self nowClassTypeAllData:dic];
            } @catch (NSException *exception) {
                NSLog(@"数据缓存失败!");
            } @finally { }
        }
    } failure:^(NSString *failure) {
        [_classCltView endRefreshType:1 isReload:YES]; MBHideHUD
        SHOWMSG(failure)
    }];
  
}




-(UICollectionView*)classCltView{
    if (!_classCltView) {
        
        _layout = [[UICollectionViewFlowLayout alloc]init];
        if (@available(iOS 9.0, *)) {
            _layout.sectionHeadersPinToVisibleBounds = YES;
        }

        _classCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:_layout];
        _classCltView.delegate = self ; _classCltView.dataSource = self ;
//        _classCltView.showsVerticalScrollIndicator = NO ;
        _classCltView.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_classCltView];
        [_classCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0); make.bottom.offset(0);
        }];
        
        [_classCltView registerClass:[PDDCltSortingHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head"];
        [_classCltView registerClass:[HOneCltCell1 class] forCellWithReuseIdentifier:@"hone_clt_cell1"];
        [_classCltView registerClass:[PDDGoodsCltCellH class] forCellWithReuseIdentifier:@"goods_cell1"];
        [_classCltView registerClass:[PDDGoodsCltCellV class] forCellWithReuseIdentifier:@"goods_cell2"];
        
        
        // 下拉刷新
        __weak ClassListCltCell * selfView = self;
        _classCltView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            selfView.pageNum = 1;
            selfView.parameter[@"st"] = @"1";
            [selfView.classListAry removeAllObjects];
//            [selfView.classCltView reloadData];
            [selfView loadClassGoodsData];
        }];
        _classCltView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pageNum++;
            selfView.parameter[@"st"] = FORMATSTR(@"%ld",(long)selfView.pageNum);
            [selfView loadClassGoodsData];
        }];
    }else{  [_classCltView reloadData];  }
    return _classCltView ;
}


#pragma mark ===>>> UICollectionView代理

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2 ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) { return 1 ; }
    return _classListAry.count ;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        HOneCltCell1 * oneclt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hone_clt_cell1" forIndexPath:indexPath];
        oneclt1.dataAry = _bannerListAry;
        return oneclt1 ;
    }
    
    if (GoodsListStylePdd==1) { // 列表
        PDDGoodsCltCellH * goodsCell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_cell1" forIndexPath:indexPath];
        if (_classListAry.count>=indexPath.item+1) {
            [goodsCell1 isStartS:_isStartSelect sgidDic:_isSltGoodsidDic gModel:_classListAry[indexPath.item]];
        }else{ [_classCltView reloadData]; }
        goodsCell1.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
            [self.delegate nowCell:self selectGoodsModel:goodsModel isSelect:ifSelect];
        };
        return goodsCell1 ;
    }else{ // 方块
        PDDGoodsCltCellV * goodsCell2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_cell2" forIndexPath:indexPath];
        if (_classListAry.count>=indexPath.item+1) {
            [goodsCell2 isStartS:_isStartSelect sgidDic:_isSltGoodsidDic gModel:_classListAry[indexPath.item]];
        }else{ [_classCltView reloadData]; }
        goodsCell2.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
            [self.delegate nowCell:self selectGoodsModel:goodsModel isSelect:ifSelect];
        };
        return goodsCell2 ;
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1){
         [PageJump pushToGoodsDetail:_classListAry[indexPath.item] pushType:3];
    }
}

#pragma mark ===>>> UICollectionViewLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        if (_bannerListAry.count>0) { return  CGSizeMake(ScreenW, ScreenW/2.5); }
        return CGSizeMake(ScreenW, 1);
    }
    
    if (GoodsListStylePdd==1) {
        return CGSizeMake(ScreenW, FloatKeepInt(115*HWB));
    }else{
        return CGSizeMake(ScreenW/2-3, ScreenW/2-3+90*HWB);
    }
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    if (section==1) {
        if(GoodsListStylePdd==2) { return 6 ; }
        else{ return 1 ; }
    }
    return 0 ;
   
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    if (section==1) { if(GoodsListStylePdd==2) { return 6 ; } }
    return 0;
    
}

#pragma mark ===>>> 区头区尾代理

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqual:UICollectionElementKindSectionHeader]) {
        if (indexPath.section==1) {
            PDDCltSortingHead * screenHead = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head" forIndexPath:indexPath];
            screenHead.delegate = self;
            screenHead.pddScreenView.styleType = _selectType;
            return screenHead;
        }
    }
    return  nil ;
}
    
//区头
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if(section==1){ return CGSizeMake(ScreenW, 32*HWB); }
    return CGSizeZero ;
   
}
//区尾
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
//    return CGSizeZero ;
//}

#pragma mark ===>>> 排序条件选择后的代理方法
-(void)pddSelectGoodsScreenSortingType:(NSString *)sortingType{

    switch ([sortingType integerValue]) {
        case 0:{ // 按综合处理
            if (![_selectType isEqual:sortingType]) {
                _selectType = sortingType;
                [_parameter removeObjectForKey:@"sortType"];
                [self reloadData];
            }
        } break;
        
        default:{
            if (![_selectType isEqual:sortingType]) {
                _selectType = sortingType;
                _parameter[@"sortType"] = sortingType;
                [self reloadData];
            }
        } break;
    }
}
-(void)reloadData{
    MBShow(@"正在加载...")
    _pageNum = 1;
    _parameter[@"st"] = @"1";
    [_classListAry removeAllObjects];
    NSLog(@"%@",_classListAry);
//    [_classCltView reloadData];
    [self loadClassGoodsData];
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _scrollStopY = scrollView.contentOffset.y;
    [self.delegate beginDragNowCell:self];
}

//// 滑动时的偏移量
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (@available(iOS 9.0, *)) {
//        if (scrollView.contentOffset.y>_scrollStopY) {
//            _layout.sectionHeadersPinToVisibleBounds = NO;
//        }else{
//            _layout.sectionHeadersPinToVisibleBounds = YES;
//        }
//    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (decelerate==0) {
        NSString * contentOffsetY = FORMATSTR(@"%.0f",scrollView.contentOffset.y);
        if ([contentOffsetY floatValue]>0) {
            [self.delegate nowCell:self offsetY:contentOffsetY];
        }
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSString * contentOffsetY = FORMATSTR(@"%.0f",scrollView.contentOffset.y);
    if ([contentOffsetY floatValue]>0) {
        [self.delegate nowCell:self offsetY:contentOffsetY];
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    NSString * contentOffsetY = FORMATSTR(@"%.0f",scrollView.contentOffset.y);
    if ([contentOffsetY floatValue]>0) {
        [self.delegate nowCell:self offsetY:contentOffsetY];
    }
}

@end



