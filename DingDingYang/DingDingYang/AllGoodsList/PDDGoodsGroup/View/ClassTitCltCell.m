//
//  ClassTitCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/3/15.
//  可左右滑动的商品标题

#import "ClassTitCltCell.h"

@implementation ClassTitCltCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _titleLab = [UILabel labText:@"标题" color:Black51 font:13*HWB];
        _titleLab.textAlignment = NSTextAlignmentCenter ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
    }
    return self;
}

@end
