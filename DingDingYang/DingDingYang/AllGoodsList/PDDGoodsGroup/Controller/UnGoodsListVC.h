//
//  UnGoodsListVC.h
//  DingDingYang
//
//  Created by ddy on 2018/3/12.
//



@interface UnGoodsListVC : DDYViewController


+(void)pushToGoodsClassByModel:(MClassModel*)model;
@property(nonatomic,strong) MClassModel * mcModel ;

// 商品分类(对应themeId参数): 1:首页(首页无用) 2:九块九  3:十九块九  4:超级折扣  5:今日上新
//@property(nonatomic,copy) NSString * goodsType; // 自己写的参数 0:分类

+(void)pushToGoodsClassByDic:(NSDictionary*)dic;
@property(nonatomic,strong) NSDictionary * pdic;

/*
 
 分享商品
 URL:/share/pddPromotionMade?clientId=&clientSecret=&pid=&parameter
 请求的参数:
 token      登录用户的令牌
 goodsId    商品ID
 goodsName  商品名称
 price      商品价格
 endPrice   券后价
 couponMoney   优惠券金额(没有优惠券时，传0)
 ifBuy  立即购买，或者领券下载填 1   默认空
 openType  3:返回小程序的链接    默认可不填
 clientSecret  拼多多的PID信息API返回
 clientId 拼多多的PID信息API返回
 pid   拼多多的PID信息API返回
 parameter   拼多多的PID信息API返回
 返回的参数:
 url    推广购买的链接
 pwd          分享的内容
 
 
 
 获取拼多多的PID信息API
 URL:/app/user/getPddInfo.api?token=
 请求的参数:
 token      登录用户的令牌
 返回的参数:
 parameter    参数信息的加密字符
 pddPid   PID
 clientId     应用KEY-开放平台的应用key
 clientSecret 应用Secret-开放平台的应用Secret
 
 */


@end
