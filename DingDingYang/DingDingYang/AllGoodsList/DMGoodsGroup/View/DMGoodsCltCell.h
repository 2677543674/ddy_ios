//
//  DMGoodsCltCell.h
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//  多麦商品列表

#import <UIKit/UIKit.h>

@interface DMGoodsCltCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     // 商品图片
@property(nonatomic,strong) UILabel      * goodsTitle ;    // 商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   // 券后价
@property(nonatomic,strong) UILabel      * priceLab ;      // 原价
@property(nonatomic,strong) UILabel      * salesLab ;      // 销量
@property(nonatomic,strong) UIImageView  * fromIma ;       // 来源
@property(nonatomic,strong) UIButton     * shareBtn;

@property(nonatomic,strong) GoodsModel   * goodsModel;

@end
