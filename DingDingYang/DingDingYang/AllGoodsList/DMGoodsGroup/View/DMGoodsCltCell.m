//
//  DMGoodsCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//  多麦商品列表

#import "DMGoodsCltCell.h"
#import "CreateShareImageView6.h"

@implementation DMGoodsCltCell

-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    _goodsTitle.text = goodsModel.goodsName ;
    
    [_fromIma sd_setImageWithURL:[NSURL URLWithString:_goodsModel.gfromUrl] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    _endPriceLab.text = FORMATSTR(@"￥%@",goodsModel.endPrice) ;
    
    _salesLab.text = FORMATSTR(@"销量:%@",goodsModel.sales);
    
    _priceLab.text = FORMATSTR(@"市场价:￥%@",goodsModel.price) ;
    
    _endPriceLab.text = FORMATSTR(@"  平台价:￥%@  ",goodsModel.endPrice) ;
    
    if (role_state!=0) {
        [_shareBtn setTitle:FORMATSTR(@"  分享赚:￥%@  ",goodsModel.commission) forState:(UIControlStateNormal)];
    }else{
        //消费者
        [_shareBtn setTitle:@"   去分享   " forState:(UIControlStateNormal)];
    }
    
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self goodsImgV];
        [self goodsTitle];
        
        [self endPriceLab];
        [self fromIma];
        [self salesLab];
        [self priceLab];
        [self shareBtn];
    }
    return self;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.contentMode=UIViewContentModeScaleAspectFill;
        _goodsImgV.clipsToBounds=YES;
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.top.offset(12*HWB);
            make.height.width.offset(93*HWB);
        }];
        
    }
    return _goodsImgV;
}



-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _goodsTitle.numberOfLines=2;
        [self.contentView addSubview:_goodsTitle];
        [_goodsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10);
            make.top.equalTo(_goodsImgV.mas_top).offset(5*HWB);
            //            make.height.offset(35*HWB);
        }];
    }
    return _goodsTitle ;
}

-(UIImageView*)fromIma{
    if (!_fromIma) {
        _fromIma = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logo"]];
        _fromIma.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_fromIma];
        [_fromIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(_endPriceLab.mas_top).offset(-8*HWB);
            make.height.offset(15*HWB); make.width.offset(40*HWB);
        }];
    }
    return _goodsImgV;
}


-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"销量：0" color:Black102 font:10*HWB];
        _salesLab.hidden=YES;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
            //            make.width.offset(100);
        }];
    }
    return _salesLab ;
}

-(UILabel *)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥8888.88" color:[UIColor grayColor] font:10*HWB];
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_fromIma.mas_right).offset(5*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
        }];
    }
    return _priceLab ;
}


-(UILabel *)endPriceLab{
    if (!_endPriceLab) {
        
        _endPriceLab = [UILabel labText:@"￥8888.88" color:RGBA(246, 73, 72, 1) font:11.5*HWB];
        _endPriceLab.backgroundColor=RGBA(249, 229, 228, 1);
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(_goodsImgV.mas_bottom).offset(-5*HWB);
            make.height.offset(22*HWB);
        }];
    }
    return _endPriceLab ;
}


-(UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn=[UIButton new];
        _shareBtn.titleLabel.font=[UIFont systemFontOfSize:10.5*HWB];
        _shareBtn.layer.cornerRadius=3;
        _shareBtn.clipsToBounds=YES;
        _shareBtn.backgroundColor=RGBA(246, 73, 72, 1);
        [_shareBtn addTarget:self action:@selector(shareGoods) forControlEvents:(UIControlEventTouchUpInside)];
        [_shareBtn setTitleColor:COLORWHITE forState:(UIControlStateNormal)];
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.bottom.equalTo(_endPriceLab.mas_bottom);
            make.height.offset(22*HWB);
        }];
    }
    return _shareBtn;
}

-(void)shareGoods{

    if (_goodsImgV.image==nil) {
        SHOWMSG(@"图片下载未完成")
    }else{
        CreateShareImageView6 * view = [[CreateShareImageView6 alloc]initWithFrame:CGRectMake(-2000, -2000, ScreenW, ScreenW*1.42)];
        view.backgroundColor=COLORWHITE;
        [self.contentView addSubview:view];
        view.goodsImage=_goodsImgV.image;
//        view.model=_goodsModel;
        view.dmModel = _goodsModel;
        UIImage*shareImage = [self convertViewToImage:view];
        [view removeFromSuperview];

//        if (TARGET_IPHONE_SIMULATOR) { // 模拟器(显示预览)
//            [ImageBrowserViewController show:TopNavc.topViewController type:0 index:0 imagesBlock:^NSArray *{
//                return @[shareImage] ;
//            }];
//        }else{
            [SharePopoverVC shareWPHGoodsModel:_goodsModel thubImg:shareImage shareType:3];
//        }
    }
    
}

-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



@end
