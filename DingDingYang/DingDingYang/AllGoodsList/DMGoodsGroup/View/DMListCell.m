//
//  DMListCell.m
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DMListCell.h"
#import "HOneCltCell1.h"
#import "DMGoodsCltCell.h"

@implementation DMListCell

// 是否开始选择
-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    [_classCltView reloadData];
}

-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}

-(void)setLastStopY:(NSString *)lastStopY{
    _lastStopY = lastStopY;
}
-(void)setAllDic:(NSDictionary *)allDic{
    @try {
        _allDic = allDic;
        _pageNum = [allDic[@"parameter"][@"p"] integerValue];
        
        NSDictionary * pdic = NULLDIC(allDic[@"parameter"]);
        _parameter = [NSMutableDictionary dictionaryWithDictionary:pdic];
        
        
        if (REPLACENULL(_parameter[@"sort"]).length==0) {
            _selectType = @"0";
            _parameter[@"sort"]=@"0";
        }else{
            _selectType = REPLACENULL(_parameter[@"sort"]);
        }
        
        NSMutableArray * bannerAry = [NSMutableArray arrayWithArray:NULLARY(allDic[@"banner"])];
        if (bannerAry.count>0) {
            _bannerListAry = [NSMutableArray arrayWithArray:bannerAry];
        }else{ _bannerListAry = [NSMutableArray array]; }
        
        NSMutableArray * ary = [NSMutableArray arrayWithArray:NULLARY(allDic[@"dataList"])];
        if (ary.count==0) {
            _classListAry = [NSMutableArray array];
            [_classCltView reloadData];
            [_classCltView.mj_header beginRefreshing];
        }else{
            _classListAry = [NSMutableArray arrayWithArray:ary];
            if ([_lastStopY floatValue]>0) {
                [_classCltView setContentOffset:CGPointMake(0, [_lastStopY floatValue])];
            }
            [_classCltView reloadData];
        }
        // LOG(@"分类名称:%@  已加入的数组个数:%lu  请求所用参数:%@",allDic[@"title"],(unsigned long)ary.count,pdic)
        
    } @catch (NSException *exception) {
        SHOWMSG(@"数据出错了,建议关闭并重新打开本页面!")
    } @finally {
        [_classCltView.mj_footer resetNoMoreData];
    }
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _pageNum = 1 ;
        [self classCltView];
    }
    return self ;
}

-(void)loadClassGoodsData{
    
    
    [NetRequest requestType:0 url:url_goods_list_dm ptdic:_parameter success:^(id nwdic) {
        MBHideHUD
        if (_pageNum==1) {
            if (_classListAry.count>0) {
                [_classListAry removeAllObjects];
                [_classCltView reloadData];
            }
        }
        
        NSArray * listAry = NULLARY(nwdic[@"goodsList"]);
        [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            GoodsModel * model = [[GoodsModel alloc]initWithDM:obj];
            [_classListAry addObject:model];
        }];
    
        [_classCltView endRefreshType:listAry.count isReload:YES];

    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [_classCltView endRefreshType:1 isReload:YES];
        MBHideHUD  SHOWMSG(failure)
    }];
    
}




-(UICollectionView*)classCltView{
    if (!_classCltView) {
        
        _layout = [[UICollectionViewFlowLayout alloc]init];
        _layout.itemSize = CGSizeMake(ScreenW, 116*HWB);
        _layout.minimumLineSpacing = 1;
        _classCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:_layout];
        _classCltView.delegate = self ; _classCltView.dataSource = self ;
        //        _classCltView.showsVerticalScrollIndicator = NO ;
        _classCltView.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_classCltView];
        [_classCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0); make.bottom.offset(0);
        }];
        
        [_classCltView registerClass:[DMCltSortingHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head"];
        [_classCltView registerClass:[HOneCltCell1 class] forCellWithReuseIdentifier:@"hone_clt_cell1"];
        [_classCltView registerClass:[DMGoodsCltCell class] forCellWithReuseIdentifier:@"goods_cell11"];
        
        
        // 下拉刷新
        __weak DMListCell * selfView = self;
        _classCltView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            selfView.pageNum = 1;
            selfView.parameter[@"p"] = @"1";
            [selfView loadClassGoodsData];
        }];
        _classCltView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pageNum++;
            selfView.parameter[@"p"] = FORMATSTR(@"%ld",(long)selfView.pageNum);
            [selfView loadClassGoodsData];
        }];
    }else{  [_classCltView reloadData];  }
    return _classCltView ;
}


#pragma mark ===>>> UICollectionView代理

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _classListAry.count ;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DMGoodsCltCell * goodsCell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_cell11" forIndexPath:indexPath];
//    NSLog(@"_classListAry的个数:%lu 多麦商品第%ld个列表",(unsigned long)_classListAry.count,indexPath.item);
    if (_classListAry.count>indexPath.item) {
        goodsCell1.goodsModel = _classListAry[indexPath.item];
    }else{ [_classCltView reloadData]; }
    return goodsCell1;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    GoodsModel* model = _classListAry[indexPath.item];
    [PageJump pushToGoodsDetail:model pushType:5];
//    if ([_act isEqualToString:@"1"]) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.cpsUrl]];
//    }else{
//    WebVC * web = [[WebVC alloc]init];
//    web.urlWeb = model.goodsUrl;
//    web.hidesBottomBarWhenPushed = YES ;
//    [TopNavc pushViewController:web animated:YES];
//    }
        
}

#pragma mark ===>>> 区头区尾代理

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqual:UICollectionElementKindSectionHeader]) {
        DMCltSortingHead * screenHead = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head" forIndexPath:indexPath];
        screenHead.delegate = self;  screenHead.dmScreenView.styleType = _selectType;
        return screenHead;
    }
    return  nil ;
}

// 区头
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(ScreenW, 32*HWB);
}


#pragma mark ===>>> 排序条件选择后的代理方法
-(void)dmSelectGoodsScreenSortingType:(NSString *)sortingType{
    
    switch ([sortingType integerValue]) {
        case 0:{ // 按综合处理
            if (![_selectType isEqual:sortingType]) {
                _selectType = sortingType;
                _parameter[@"sort"] = sortingType;
                [self reloadData];
            }
        } break;
            
        default:{
            if (![_selectType isEqual:sortingType]) {
                _selectType = sortingType;
                _parameter[@"sort"] = sortingType;
                [self reloadData];
            }
        } break;
    }
}
-(void)reloadData{
    MBShow(@"正在加载...")
    _pageNum = 1;
    _parameter[@"p"] = @"1";
    [self loadClassGoodsData];
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _scrollStopY = scrollView.contentOffset.y;
}

// 滑动时的偏移量
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (@available(iOS 9.0, *)) {
//        if (scrollView.contentOffset.y>_scrollStopY) {
//            _layout.sectionHeadersPinToVisibleBounds = NO;
//        }else{
//            _layout.sectionHeadersPinToVisibleBounds = YES;
//        }
//    }
}


@end
