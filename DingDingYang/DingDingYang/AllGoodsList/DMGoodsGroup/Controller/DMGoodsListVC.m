//
//  DMGoodsListVC.m
//  DingDingYang
//
//  Created by ddy on 2018/9/4.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DMGoodsListVC.h"
#import "DMListCell.h"
#import "ClassTitCltCell.h"
#import "GoodsClassModel.h"
#import "GoodsListCltCell1.h"
#import "SearchOldVC.h"

@interface DMGoodsListVC () <UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
@property(nonatomic,strong) UICollectionView * classTitView; // 标题选项卡
@property(nonatomic,strong) UICollectionView * goodsCltView; // 商品选项卡
@property(nonatomic,strong) UIImageView * linesImgv; // 横的下划线

// 记录当前选中状态和左右两侧标题或表格的位置(用于动画效果)
@property(nonatomic,assign) NSInteger selectIndex; // 记录选中的分区
@property(nonatomic,assign) float scrollX;          // 当前选中标题后的标题表格偏移量
@property(nonatomic,assign) float lastCellCenterX ; // 当前选中的cell停留的中间点
@property(nonatomic,assign) float leftWidth;   // 左侧标题宽
@property(nonatomic,assign) float centerWidth; // 当前选中的标题的宽
@property(nonatomic,assign) float rightWidth;  // 右侧标题的宽

// 记录子cell的所有数据
@property(nonatomic,strong) NSMutableArray * selectTitAry; // 可供选择的标题数组
@property(nonatomic,strong) NSMutableArray * allDataAry; // 存放所有数据
@property(nonatomic,strong) NSMutableArray * allListCtY; // 存放记录所有列表的偏移量
@end

@implementation DMGoodsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLORWHITE;
    
    self.title = _mcModel.title.length>0?_mcModel.title:@"综合平台";
    
    _selectTitAry = [NSMutableArray array];
    
    [GoodsClassModel getGoodsClassModelAryCacheDM:^(NSMutableArray *modelCacheAry) {
        NSLog(@"商品分类读取缓存中的数据。。。。。。");
        [self addSelectAry:modelCacheAry];
    } newDataFromNetWork:^(NSMutableArray *modelNewAry) {
        NSLog(@"商品分类从服务器获取的数据。。。。。。");
        [self addSelectAry:modelNewAry];
    }];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:LoginSuccess object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:ChangeGoodsList object:nil];
}

-(void)changeState{
    [_goodsCltView reloadData];
}

#pragma mark ===>>> 初始化一些内容
-(void)addSelectAry:(NSMutableArray*)ary{
    [_selectTitAry removeAllObjects];
    [_selectTitAry addObjectsFromArray:(NSArray*)ary];
    [self initPtdicData];
    [self classTitView];
    [self goodsCltView];
}

// 根据标题的个数，调整数据的存储个数
-(void)initPtdicData{
    _allDataAry = [NSMutableArray array];
    _allListCtY = [NSMutableArray array];
    for (NSInteger i=0; i<_selectTitAry.count; i++) {
        GoodsClassModel * model = _selectTitAry[i];
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"parameter"] = @{@"p":@"1",@"cateId":model.gcId};
        dic[@"dataList"] = @[];  dic[@"banner"] = @[];
//        if (_mcModel) {
//            NSMutableDictionary * par = [NSMutableDictionary dictionary];
//            [par addEntriesFromDictionary:_mcModel.params];
//            par[@"categoryType"] = model.gcId; par[@"pageNum"] = @"1";
//            dic[@"parameter"] = par;
//        }
        [_allDataAry addObject:dic];
        [_allListCtY addObject:@"0"];
    }
}



#pragma mark ===>>> 上方标题选项列表
-(UICollectionView*)classTitView{
    if (!_classTitView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _classTitView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _classTitView.delegate = self ; _classTitView.dataSource = self ;
        _classTitView.showsHorizontalScrollIndicator = NO ;
        _classTitView.backgroundColor = COLORWHITE ;
        _classTitView.tag = 100;
        [self.view addSubview:_classTitView];
        [_classTitView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(-40*HWB);
            make.top.offset(0); make.height.offset(30*HWB);
        }];
        
        [_classTitView registerClass:[ClassTitCltCell class] forCellWithReuseIdentifier:@"class_title"];
        
        
        GoodsClassModel * model0 = _selectTitAry[0];
        
        _linesImgv = [[UIImageView alloc]init];
        _linesImgv.backgroundColor = LOGOCOLOR ;
        float wd = StringSize(model0.title, 13*HWB).width + 26*HWB ;
        _linesImgv.center = CGPointMake(wd/2, 30*HWB-1);
        _linesImgv.bounds = CGRectMake(0, 0, wd-26*HWB,1.5);
        [_classTitView addSubview:_linesImgv];
        
        _lastCellCenterX = wd/2;
        _selectIndex = 0;
        _leftWidth = 0;
        _centerWidth = StringSize(model0.title, 13*HWB).width;
        if (_selectTitAry.count>1) {
            GoodsClassModel * model1 = _selectTitAry[1];
            _rightWidth = StringSize(model1.title, 13*HWB).width;
        }else{
            _rightWidth = 0 ;
        }
        
        UIButton* searchBtn=[UIButton new];
        [searchBtn setImage:[UIImage imageNamed:@"sh_imgn"] forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(searchBtn, clickSearch);
        [self.view addSubview:searchBtn];
        [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);
            make.top.offset(0);
            make.height.offset(30*HWB);
            make.width.offset(40*HWB);
        }];
        
    }else{ [_classTitView reloadData]; }
    return _classTitView;
}

-(void)clickSearch{
    SearchOldVC * oldVC = [SearchOldVC new];
    oldVC.hidesBottomBarWhenPushed = YES ;
    oldVC.searchType = 5;
    PushTo(oldVC, YES)
}


#pragma mark ===>>> 商品列表
-(UICollectionView*)goodsCltView{
    if (!_goodsCltView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _goodsCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _goodsCltView.delegate = self ; _goodsCltView.dataSource = self ;
        _goodsCltView.showsHorizontalScrollIndicator = NO ;
        _goodsCltView.pagingEnabled = YES ;
        _goodsCltView.backgroundColor = COLORGROUP ;
        _goodsCltView.tag = 200;
        [self.view addSubview:_goodsCltView];
        [_goodsCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0);
            make.top.offset(36*HWB); make.bottom.offset(0);
        }];
        [_goodsCltView registerClass:[DMListCell class] forCellWithReuseIdentifier:@"class_list"];
    }
    //    else{ [_goodsCltView reloadData]; }
    return _goodsCltView;
}

#pragma mark ===>>> UICollectionView代理

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _selectTitAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==100) { // 标题
        GoodsClassModel * model = _selectTitAry[indexPath.item];
        ClassTitCltCell * titleCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"class_title" forIndexPath:indexPath];
        titleCell.titleLab.text = model.title;
        titleCell.titleLab.textColor = Black51 ;
        titleCell.titleLab.font = [UIFont systemFontOfSize:13*HWB];
        if (indexPath.item==_selectIndex) {
            _linesImgv.center = CGPointMake(titleCell.center.x,30*HWB-1);
            float wi = StringSize(model.title, 13*HWB).width;
            _linesImgv.bounds = CGRectMake(0, 0, wi , 1.5);
            titleCell.titleLab.textColor = LOGOCOLOR ;
            titleCell.titleLab.font = [UIFont systemFontOfSize:14*HWB];
        }
        return titleCell;
    }
    
    // 商品列表
    DMListCell * listCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"class_list" forIndexPath:indexPath];
    listCell.tag = indexPath.item+10;
    listCell.lastStopY = _allListCtY[indexPath.item];
    listCell.allDic = _allDataAry[indexPath.item];
    return listCell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==100) { // 选中标题
        if (indexPath.item==_selectIndex) { return ; }
        _selectIndex = indexPath.item;
        [self changeFrameState:1];
    }
}

#pragma mark ===>>> CollectionViewFlowLayout代理
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==100) {
        GoodsClassModel * model = _selectTitAry[indexPath.item];
        return CGSizeMake(StringSize(model.title, 13*HWB).width+26*HWB, 30*HWB);
    }
    return CGSizeMake(ScreenW, _goodsCltView.frame.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0 ;
}

#pragma mark ===>>> UIScrollView 代理监控滑动事件
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.tag==200) {
        @try {
            float sclOffset = scrollView.contentOffset.x ;
            
            if (_lastCellCenterX>0) {  // 向右滑动
                
                if (sclOffset>_scrollX&&sclOffset<_selectTitAry.count*ScreenW) {
                    float bili = ScreenW/(_rightWidth + 26*HWB) ; // 比例
                    float chaju = sclOffset - _scrollX ;
                    float linsW = _centerWidth + chaju/bili ;
                    float lineCenterX = _lastCellCenterX+chaju/bili/2 ;
                    _linesImgv.center = CGPointMake(lineCenterX, 30*HWB-1);
                    _linesImgv.bounds = CGRectMake(0, 0, linsW, 1.5);
                }
                
                if (sclOffset<_scrollX&&sclOffset>0) { // 向左滑动
                    float bili = ScreenW/(_leftWidth + 26*HWB) ; // 比例
                    float chaju = _scrollX - sclOffset;
                    float linsW = _centerWidth + chaju/bili ;
                    float lineCenterX = _lastCellCenterX-chaju/bili/2 ;
                    _linesImgv.center = CGPointMake(lineCenterX, 30*HWB-1);
                    _linesImgv.bounds = CGRectMake(0, 0, linsW, 1.5);
                }
            }
            
        } @catch (NSException *exception) { } @finally { }
    }
}

// 手指滑动结束
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag==200) { // 商品列表滑动
        NSInteger row1 =  _goodsCltView.contentOffset.x/ScreenW ;
        _selectIndex = row1 ;
        [self changeFrameState:2];
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView.tag==200) {
        [_goodsCltView reloadData];
    }
}

#pragma mark ===>>> 记录偏移和所需控件的位置
-(void)changeFrameState:(NSInteger)type{
    //  type 1: 点击上面标题触发的偏移  2:滑动触发的偏移
    @try {
        
        // 修改偏移量
        _scrollX = _selectIndex * ScreenW;
        GoodsClassModel * modelCentre = _selectTitAry[_selectIndex];
        _centerWidth  = StringSize(modelCentre.title,13*HWB).width;
        
        if (_selectIndex>0) {
            GoodsClassModel * modelLeft = _selectTitAry[_selectIndex-1];
            _leftWidth = StringSize(modelLeft.title,13*HWB).width;
        } else{ _leftWidth = 0 ; }
        
        if (_selectIndex<_selectTitAry.count-1) {
            GoodsClassModel * modelRight = _selectTitAry[_selectIndex+1];
            _rightWidth = StringSize(modelRight.title,13*HWB).width;
        }else{ _rightWidth = 0; }
        
        
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:_selectIndex inSection:0];
        [_classTitView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [_goodsCltView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        
        if (type==1) { [_goodsCltView reloadItemsAtIndexPaths:@[indexPath]] ;}
        ClassTitCltCell * selectCell = (ClassTitCltCell*)[_classTitView cellForItemAtIndexPath:indexPath];
        if (selectCell&&[selectCell isKindOfClass:[ClassTitCltCell class]]) {
            _lastCellCenterX = selectCell.center.x ;
            [UIView animateWithDuration:0.2 animations:^{
                _linesImgv.center = CGPointMake(selectCell.center.x,30*HWB-1);
                _linesImgv.bounds = CGRectMake(0, 0, _centerWidth , 1.5);
                selectCell.titleLab.textColor = LOGOCOLOR ;
            }];
        }else{
            _lastCellCenterX = 0 ;
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                @try {
                    if (_lastCellCenterX>=0&&_selectTitAry!=nil) {
                        _lastCellCenterX = _centerWidth/2+13*HWB ;
                        for (NSInteger i=0; i<_selectIndex; i++) {
                            GoodsClassModel * model = _selectTitAry[i];
                            _lastCellCenterX = StringSize(model.title, 13*HWB).width+26*HWB + _lastCellCenterX;
                        }
                    }
                } @catch (NSException *exception) { } @finally { }
            });
        }
        
    } @catch (NSException *exception) { // 如果上面代码执行失败，则执行下面代码
        
        _scrollX = _selectIndex * ScreenW;
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:_selectIndex inSection:0];
        ClassTitCltCell * selectCell = (ClassTitCltCell*)[_classTitView cellForItemAtIndexPath:indexPath];
        if (selectCell&&[selectCell isKindOfClass:[ClassTitCltCell class]]) {
            GoodsClassModel * model = _selectTitAry[_selectIndex];
            float wi = StringSize(model.title, 13*HWB).width;
            [UIView animateWithDuration:0.2 animations:^{
                _linesImgv.center = CGPointMake(selectCell.center.x,30*HWB-1);
                _linesImgv.bounds = CGRectMake(0, 0, wi , 1.5);
                selectCell.titleLab.textColor = LOGOCOLOR ;
            }];
        }
        [_classTitView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [_goodsCltView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
    } @finally {
        [_classTitView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}




@end
