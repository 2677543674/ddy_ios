//
//  WPHGoodsListVC.h
//  DingDingYang
//
//  Created by ddy on 2018/7/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPHGoodsListVC : DDYViewController
+(void)pushToGoodsClassByModel:(MClassModel*)model;
@property(nonatomic,strong) MClassModel * mcModel ;

+(void)pushToGoodsClassByDic:(NSDictionary*)dic;
@property(nonatomic,strong) NSDictionary * pdic;
@end
