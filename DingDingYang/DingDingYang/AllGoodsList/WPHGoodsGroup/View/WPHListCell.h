//
//  WPHListCell.h
//  DingDingYang
//
//  Created by ddy on 2018/7/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPHSortingView.h"

@interface WPHListCell : UICollectionViewCell <SortingSelectDelegateWPH,UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionViewFlowLayout * layout;
@property(nonatomic,strong) UICollectionView * classCltView;
@property(nonatomic,copy)   NSString * defultName;

// 记录数据
@property(nonatomic,assign) BOOL isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典

@property(nonatomic,copy)   NSDictionary * allDic ; // 请求参数和商品列表组成的数据
@property(nonatomic,strong) NSMutableDictionary * parameter ; // 请求参数
@property(nonatomic,strong) NSMutableArray * bannerListAry; // banner图数组
@property(nonatomic,strong) NSMutableArray * classListAry ; // 商品列表
@property(nonatomic,assign) NSInteger  pageNum; // 页面
@property(nonatomic,copy) NSString * selectType; // 选中的排序类型
@property(nonatomic,copy) NSString * lastStopY; // 记录滑动停止位置(刷新时定位用)
@property(nonatomic,assign) float scrollStopY; // 记录滑动停止位置(判断是在上拉还是下拉使用)
@property(nonatomic,copy)NSString* act;//0:浏览器打开  1：APP打开

@end







