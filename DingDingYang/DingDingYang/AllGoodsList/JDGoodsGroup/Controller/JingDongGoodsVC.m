//
//  JingDongGoodsVC.m
//  DingDingYang
//
//  Created by 广州云鸽信息科技有限公司 on 2017/11/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "JingDongGoodsVC.h"
#import "SearchOldVC.h"
#import "WqeView.h"
#import "JingDongCell.h"

@interface JingDongGoodsVC ()<UITextFieldDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)NSMutableArray*listAry;//列表数据
@property(nonatomic,assign)NSInteger pageNum;//页数
@property(nonatomic,strong)NSMutableDictionary*parameter;//参数
@property(nonatomic,strong) SearchScreenView * screenView ;
@property(nonatomic,strong) UITableView * universalListCltv ;
@property(nonatomic,strong) NSString* path;
@property(nonatomic,strong)NSString* searchKey;
@property(nonatomic,copy)  NSString* act;
@end

@implementation JingDongGoodsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (TOKEN.length==0) {
        [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) {
            if (result == 1) {
                
            }
            if (result == 2) {
                TopNavc.topViewController.tabBarController.selectedIndex = 0 ;
            }
        }];
        return;
    }
    
    
    self.view.backgroundColor=[UIColor whiteColor];
    self.title=_titleStr.length>0?_titleStr:@"京东精选";
    _searchKey=@"";
    _act = @"0";
    _path = [NSString stringWithFormat:@"%@%@",dns_dynamic_jingd,@"r/find"];
    _pageNum=2;
    
    _listAry=[NSMutableArray array];
    self.view.backgroundColor = COLORGROUP ;
    [self searchView];
    [self parameter];
    [self screenView];
    [self universalListCltv];
    [_universalListCltv.mj_header beginRefreshing];
    
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"%f",self.view.frame.size.height);
}

-(void)searchView{
        UIView* whiteView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, ScreenW*80/375.0)];
        whiteView.backgroundColor=[UIColor whiteColor];
        
        [self.view addSubview:whiteView];
        UIButton* searchBtn = [UIButton titLe:@"" bgColor:COLORGROUP titColorN:Black204 titColorH:Black204 font:13.5*HWB];
        searchBtn.tag=500;
        searchBtn.titleLabel.font = [UIFont systemFontOfSize:13.5*HWB];
        searchBtn.frame = CGRectMake(30, ScreenW*50/375.0/2-16, ScreenW-60, 32);
        searchBtn.layer.masksToBounds = YES ;
        searchBtn.layer.cornerRadius = 5.0 ;
        searchBtn.contentHorizontalAlignment = UIViewContentModeLeft ;
        searchBtn.titleLabel.textAlignment=NSTextAlignmentLeft;
        [searchBtn addTarget:self action:@selector(clickSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
        [whiteView addSubview:searchBtn];
        
        UIImageView* searchImgV = [[UIImageView alloc]init];
        searchImgV.image = [UIImage imageNamed:@"jd_search_img"];
        searchImgV.frame = CGRectMake(5, 7, 20, 20);
        [searchBtn addSubview:searchImgV];
        UILabel* sortNameLabel=[UILabel labText:@"宝贝名称、关键词" color:Black204 font:12*HWB];
        sortNameLabel.tag=800;
        sortNameLabel.frame=CGRectMake(30, 7,ScreenW-120, 20);
        [searchBtn addSubview:sortNameLabel];

        whiteView.tag=400;
        UIImageView* ima= [ [UIImageView alloc]initWithImage:[UIImage imageNamed:@"jd_ifshow_coupons"]];
        ima.contentMode = UIViewContentModeScaleAspectFit;
        [whiteView addSubview:ima];
        [ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(searchBtn.mas_bottom).offset(15);
            make.left.offset(40);
        }];
    
        UILabel* label=[UILabel new];
        label.text=@"仅显示优惠券商品";
        label.textColor=[UIColor grayColor];
        label.font=[UIFont systemFontOfSize:15];
        [whiteView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(ima.mas_right).offset(8);
            make.centerY.equalTo(ima);
        }];

        UISwitch *switchView = [[UISwitch alloc]init];
        [switchView setOn: YES animated: YES];
        switchView.onTintColor=LOGOCOLOR;
        [switchView addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            switchView.transform = CGAffineTransformMakeScale( 0.7*HWB, 0.7*HWB);//缩放
        [whiteView addSubview:switchView];
        [switchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@-40);
            make.centerY.equalTo(ima);
        }];
}

-(void)switchAction:(UISwitch*)switchView{
    BOOL isButtonOn = [switchView isOn];
    if (isButtonOn) {
        _parameter[@"ifGetNoCoupon"] = @"0";//开
        [_universalListCltv.mj_header beginRefreshing];
    }else {
        _parameter[@"ifGetNoCoupon"] = @"1";//关
        [_universalListCltv.mj_header beginRefreshing];
    }
}

-(void)clickSearchBtn:(UIButton*)btn{
    if (TopNavc!=nil) {
        UILabel* sortNameLabel=[self.view viewWithTag:800];
        SearchOldVC*search=[[SearchOldVC alloc]init];
        search.hidesBottomBarWhenPushed=YES;
        search.searchType=2;
        search.resultlb = ^(NSString *str) {
            if ([str isEqualToString:@""]) {
                sortNameLabel.text=@"宝贝名称、关键词";
            }else{
                sortNameLabel.text=str;
            }
            
            _searchKey=str;
            [_universalListCltv.mj_header beginRefreshing];
            
        };
        [TopNavc pushViewController:search animated:NO];
    }
}

-(SearchScreenView*)screenView{
    if (!_screenView) {

        _screenView = [[SearchScreenView alloc]initWithFrame:CGRectMake(0, ScreenW*80/375.0, ScreenW, 38)];
        _screenView.tag=99;
        [self.view addSubview:_screenView];
        
        [_screenView selectBlock:^(NSString *sortType) {
            _parameter[@"sortType"] = sortType ;
            [_universalListCltv.mj_header beginRefreshing];
        }];
    }
    return _screenView;
}


-(UITableView*)universalListCltv{
    if (!_universalListCltv) {
        _universalListCltv=[[UITableView alloc]initWithFrame:CGRectMake(0, 39+ScreenW*80/375.0, ScreenW, ScreenH-ScreenW*80/375.0-39-NAVCBAR_HEIGHT) style:UITableViewStylePlain];
        if (_isOnePage==YES) {
            _universalListCltv=[[UITableView alloc]initWithFrame:CGRectMake(0, 39+ScreenW*80/375.0, ScreenW, ScreenH-ScreenW*80/375.0-39-NAVCBAR_HEIGHT-TABBAR_HEIGHT) style:UITableViewStylePlain];
        }
        _universalListCltv.separatorStyle=0;
        _universalListCltv.delegate = self ;
        _universalListCltv.dataSource = self ;
        _universalListCltv.backgroundColor = COLORGROUP ;
        [self.view addSubview:_universalListCltv];
        _universalListCltv.tag=200;
        _universalListCltv.tableFooterView = [[UIView alloc] init];

        [_universalListCltv registerClass:[JingDongCell class] forCellReuseIdentifier:@"JingDongCell"];
        //下拉刷新
        __weak JingDongGoodsVC * SelfView = self ;
        
        _universalListCltv.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [SelfView searchGoodsData:_universalListCltv.tag];
        }];
        _universalListCltv.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreSearchData:_universalListCltv.tag];
        }];
        
    }
    return _universalListCltv ;
}



//参数
-(NSMutableDictionary*)parameter{
    if (!_parameter) {
        _parameter = [NSMutableDictionary dictionary];
        _parameter[@"st"] = @"1" ;
        _parameter[@"key"] = _searchKey ;
        _parameter[@"type"] = @"3" ;
        _parameter[@"ifGetNoCoupon"] = @"0"; // 传参：ifGetNoCoupon  1:无优惠券   0:有优惠券
    }
    return _parameter;
}

-(void)initSwitch{
    if ([NSUDTakeData(@"showCouponBtn") isEqualToString:@"0"]) {
        UIView * whiteView=[self.view viewWithTag:400];
        whiteView.frame = CGRectMake(0, 0, ScreenW, ScreenW*50/375.0);
        _screenView.frame=CGRectMake(0, ScreenW*50/375.0, ScreenW, 38);
        _universalListCltv.frame = CGRectMake(0, 39+ScreenW*50/375.0, ScreenW, ScreenH-ScreenW*50/375.0-39-NAVCBAR_HEIGHT);
        if (_isOnePage==YES) {
            _universalListCltv.frame = CGRectMake(0, 39+ScreenW*50/375.0, ScreenW, ScreenH-ScreenW*50/375.0-39-NAVCBAR_HEIGHT-TABBAR_HEIGHT);
        }
    }
    if (NSUDTakeData(@"showCouponBtn")==nil) {
        NSUDSaveData(@"1", @"showCouponBtn") //首次进入设置为1
    }
    
}

#pragma mark -- 点击搜索商品
-(void)searchGoodsData:(NSInteger)tag{
    _pageNum = 2 ;
    _parameter[@"st"] = @"1" ;
    _parameter[@"p"] = @"1" ;
    _parameter[@"key"]=_searchKey;
    [_universalListCltv.mj_footer resetNoMoreData];
    
    [NetRequest requestTokenURL:_path parameter:_parameter success:^(NSDictionary *nwdic) {
        _act=[nwdic[@"act"] stringValue];
        NSLog(@"%@",nwdic[@"act"]);
            //showCouponBtn 有值的时候不显示优惠券按钮
        if(nwdic[@"showCouponBtn"]==nil){
            if ([NSUDTakeData(@"showCouponBtn") isEqualToString:@"0"]) {
                [self initSwitch];
            }
            NSUDSaveData(@"1", @"showCouponBtn")
        }else{
            if ([NSUDTakeData(@"showCouponBtn") isEqualToString:@"1"]) {
                NSUDSaveData(@"0", @"showCouponBtn")
                [self initSwitch];
            }
            
        }
        
            
        if (_listAry.count>0) {
            [_listAry removeAllObjects];

            [_universalListCltv reloadData];
        }else{
            if([nwdic[@"result"] isEqualToString:@"OK"]){
                NSArray*lAry= NULLARY( nwdic[@"list"] ) ;
                if (lAry.count>0) {
                    GOODSNUMRESULTS(REPLACENULL(nwdic[@"totalCount"]));
                }
            }
        }
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry= NULLARY( nwdic[@"list"] ) ;
            if (lAry.count>0) {
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_listAry addObject:model];
                }];
            }else{
                SHOWMSG(@"亲，您搜索的商品已被抢完，下次早点来哦!")
            }
        }else{
               SHOWMSG(nwdic[@"result"])
        }
        [_universalListCltv.mj_header endRefreshing];
        [_universalListCltv reloadData];
        
    } failure:^(NSString *failure) {
        [_universalListCltv.mj_header endRefreshing];

        SHOWMSG(failure)
    }];
}
// 加载更多
-(void)loadMoreSearchData:(NSInteger)tag{
    _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum);
    _parameter[@"p"] = FORMATSTR(@"%ld",(long)_pageNum);
    [NetRequest requestTokenURL:_path parameter:_parameter success:^(NSDictionary *nwdic) {

        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray * lAry = NULLARY( nwdic[@"list"] ) ;
            if (lAry.count>0) {
                _pageNum++;
                [_universalListCltv.mj_footer endRefreshing];
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_listAry addObject:model];
                }];
            }else{
                [_universalListCltv.mj_footer endRefreshingWithNoMoreData];
            }
                
        }else{
            [_universalListCltv.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        [_universalListCltv reloadData];
        
    } failure:^(NSString *failure) {
        [_universalListCltv.mj_header endRefreshing];
        SHOWMSG(failure)
    }];
}

#pragma mark === >>> TableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.0*ScreenW/375.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _listAry.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JingDongCell * cell = [tableView dequeueReusableCellWithIdentifier:@"JingDongCell" forIndexPath:indexPath];
    [cell.getTicketsBtn addTarget:self action:@selector(clickTicket:) forControlEvents:UIControlEventTouchUpInside];
    cell.getTicketsBtn.tag=indexPath.row;
    cell.selectionStyle = 0;
    if (_listAry.count>indexPath.row) {
         cell.goodsModel=_listAry[indexPath.row];
    }else{
        [_universalListCltv reloadData];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GoodsModel * model = _listAry[indexPath.row];
    NSDictionary* parameter = @{@"goodsId":model.goodsId};
    MBShow(@"正在加载...")
    [NetRequest requestType:0 url:FORMATSTR(@"%@goods/link",dns_dynamic_jingd) ptdic:parameter success:^(id nwdic) {
        MBHideHUD
        if (REPLACENULL(nwdic[@"url"]).length>0) {
            if ([_act isEqualToString:@"1"]) {
                NSURL * url = [NSURL URLWithString:REPLACENULL(nwdic[@"url"])];
                [[UIApplication sharedApplication] openURL:url];
            }else{
                [[KeplerApiManager sharedKPService] openKeplerPageWithURL:nwdic[@"url"] sourceController:self jumpType:2 userInfo:nil];
            }
            
        }else{
            if ([_act isEqualToString:@"1"]) {
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://item.jd.com/%@.html",model.goodsId]];
                [[UIApplication sharedApplication] openURL:url];
            }else{
                [[KeplerApiManager sharedKPService] openKeplerPageWithURL:[NSString stringWithFormat:@"http://item.jd.com/%@.html",model.goodsId] sourceController:self jumpType:2 userInfo:nil];
            }
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD SHOWMSG(failure)
    }];
   
}

-(void)takeOrderJingDong:(GoodsModel*)jdModel{
    MBShow(@"正在加载...")
    NSString * getQuanUrl = FORMATSTR(@"%@goods/link",dns_dynamic_jingd);
    [NetRequest requestType:0 url:getQuanUrl ptdic:@{@"goodsId":jdModel.goodsId} success:^(id nwdic) {
        MBHideHUD
        if (REPLACENULL(nwdic[@"url"]).length>0) {
            if ([_act isEqualToString:@"1"]) {
                NSURL * url = [NSURL URLWithString:REPLACENULL(nwdic[@"url"])];
                [[UIApplication sharedApplication] openURL:url];
            }else{
                [[KeplerApiManager sharedKPService] openKeplerPageWithURL:nwdic[@"url"] sourceController:self jumpType:2 userInfo:nil];
            }
        }else{
            if ([_act isEqualToString:@"1"]) {
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://item.jd.com/%@.html",jdModel.goodsId]];
                [[UIApplication sharedApplication] openURL:url];
            }else{
                [[KeplerApiManager sharedKPService] openKeplerPageWithURL:[NSString stringWithFormat:@"http://item.jd.com/%@.html",jdModel.goodsId] sourceController:self jumpType:2 userInfo:nil];
            }
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD SHOWMSG(failure)
    }];
}

-(void)clickTicket:(UIButton*)btn{
    if (_listAry.count>btn.tag) {
        GoodsModel * model = _listAry[btn.tag];
        if (role_state==0||[btn.titleLabel.text isEqualToString:@"购买"]) {
            [self takeOrderJingDong:model];
        }else{
            [MoreWay showSpView:[UIApplication sharedApplication].keyWindow height:80+NAVCBAR_HEIGHT-64 isJD:YES andSelect:^(NSInteger way) {
                
                switch (way) {
                    case 1 : { // 微信好友
                        if (CANOPENWechat) {
                            [self shareToPlatformType:UMSocialPlatformType_WechatSession andModel:model];
                        }else{ SHOWMSG(@"亲，还没有安装微信哦!") }
                    } break;
                        
                    case 2 : { // 微信朋友圈
                        if (CANOPENWechat) {
                            [self shareToPlatformType:UMSocialPlatformType_WechatTimeLine andModel:model];
                        } else { SHOWMSG(@"亲，还没有安装微信哦!") }
                    } break;
                        
                    case 3 : { // qq好友
                        if (CANOPENQQ) { [self shareToPlatformType:UMSocialPlatformType_QQ andModel:model];
                        } else { SHOWMSG(@"亲，还没有安装QQ哦!") }
                    } break;
                        
                    case 4 : { // 空间
                       if (CANOPENQQ) { [self shareToPlatformType:UMSocialPlatformType_Qzone andModel:model];
                       } else { SHOWMSG(@"亲，还没有安装QQ哦!") }
                    } break;
                
                    default:{ } break;
                }
            }];
        }
    }

}

#pragma mark === >>> 调用分享友盟
// 分享方法
- (void)shareToPlatformType:(UMSocialPlatformType)platformType andModel:(GoodsModel*)model{
    
    NSDictionary * parameter = @{@"goodsId":model.goodsId};
    
    [NetRequest requestType:0 url:FORMATSTR(@"%@goods/share.api",dns_dynamic_jingd) ptdic:parameter success:^(id nwdic) {
        MBHideHUD
        NSString * shareText = REPLACENULL(nwdic[@"shareContent"]);
        if (shareText.length==0) { shareText = SHARECONTENT; }
        [ClipboardMonitoring pasteboard:REPLACENULL(shareText)];
        SHOWMSG(@"分享的文本内容已复制，请手动粘贴~")
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            UMSocialMessageObject * messageObject = [UMSocialMessageObject messageObject];
            if (platformType==4) {
                messageObject.text = shareText;;
            }else{
                if (platformType!=5) { messageObject.text=shareText;  }
                SDImageCache *cacheImg = [SDImageCache sharedImageCache];
                UIImage * img = [cacheImg imageFromCacheForKey:model.goodsPic];
                UMShareImageObject*shObjc=[[UMShareImageObject alloc]init];
                [shObjc setShareImage:img];
                messageObject.shareObject=shObjc;
            }
            
            // 调用分享接口
            [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
                
                if (error) {
                    switch (error.code) {
                        case 2001:{ SHOWMSG(@"不支持的分享格式或软件版本过低!") }break;
                        case 2005:{SHOWMSG(@"生成的分享内容为空,请稍后再试!")} break;
                        case 2006:{SHOWMSG(@"未能完成操作!")} break;
                        case 2008:{
                            if (platformType==1||platformType==2) { SHOWMSG(@"亲，你还没有安装微信哦!") }
                            if (platformType==3||platformType==4) { SHOWMSG(@"亲，你还没有安装QQ哦!") }
                        } break;
                        case 2009:{ SHOWMSG(@"分享已取消!") }break;
                        case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试") }break;
                        case 2011:{ SHOWMSG(@"第三方错误") }break;
                        default: {SHOWMSG(@"分享出错啦")} break;
                    }
                }else{
                    SHOWMSG(@"分享成功!")
                }
            }];
        });
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}



@end



@implementation SearchScreenView


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        
        _aryTit = @[@"综合",@"优惠券",@"价格"];
        
        [self creatBtn];
    }
    return self;
}


-(void)creatBtn{
    
    for (NSInteger i=0;i<_aryTit.count;i++) {
        CGRect rect = CGRectMake(i*(ScreenW/_aryTit.count), 0, ScreenW/_aryTit.count-1, 38);
        _sbtn = [[ScBtn alloc]initWithFrame:rect];
        _sbtn.tag = 10+i ;
        _sbtn.titLab.text = _aryTit[i];
        _sbtn.selected = NO;
        [_sbtn addTarget:self action:@selector(selectView:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sbtn];
        if (i==0) {
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
        }
    }
}

//按钮事件
-(void)selectView:(ScBtn*)ctr{

    if (ctr.tag==11) { // 优惠券排序
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            if (btn.tag!=11) { btn.selected = NO ; }
        }
        if (ctr.selected==YES) {
            ctr.stateType = 2 ;
            ctr.selected = NO ;
            self.select(@"5") ;
        }else{
            ctr.stateType = 1 ;
            ctr.selected = YES ;
            self.select(@"6") ;
        }
    }else if (ctr.tag==12) {  //价格
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            if (btn.tag!=12) { btn.selected = NO ; }
        }
        if (ctr.selected==YES) {
            ctr.stateType = 2 ;
            ctr.selected = NO ;
            self.select(@"3") ;
        }else{
            ctr.stateType = 1 ;
            ctr.selected = YES ;
            self.select(@"4") ;
        }
    }else{
        
        if (ctr.selected==YES) { return; }
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            btn.selected = NO;
        }
        ctr.stateType = 1 ;
        ctr.selected = YES ;
        if (ctr.tag==10)  { self.select(@"") ; }
        
    }
 
}

-(void)selectBlock:(SelectType)type {
    self.select = type ;
}

@end


