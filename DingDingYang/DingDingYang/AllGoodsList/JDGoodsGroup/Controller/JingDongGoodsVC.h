//
//  JingDongGoodsVC.h
//  DingDingYang
//
//  Created by 广州云鸽信息科技有限公司 on 2017/11/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScreeningView.h"

@interface JingDongGoodsVC : DDYViewController
@property(nonatomic,assign)BOOL isOnePage;
@property(nonatomic,strong)NSString* titleStr;
@end


@interface SearchScreenView : UIView

@property(nonatomic,strong) NSArray * aryTit; //筛选条件标题

@property(nonatomic,strong) ScBtn * sbtn;

typedef void(^SelectType)(NSString * sortType);
@property(nonatomic,strong) SelectType   select ;

-(void)selectBlock:(SelectType)type ;

@property(nonatomic,copy) NSString * isSelect ;
@property(nonatomic,assign)NSInteger selectBtnTag;

@end


/*
@interface SearchSBtn : UIButton

@property(nonatomic,strong) UILabel * titLab ; //标题文字
@property(nonatomic,strong) UIImageView * imgView ;//箭头图片
@property(nonatomic,strong) UIImageView * linesIV ;

@property(nonatomic,assign) NSInteger stateType;//(0:原状态、1:高亮箭头朝下、2:高亮箭头朝上)
@end
*/
