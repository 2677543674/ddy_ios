//
//  JingDongCell.h
//  DingDingYang
//
//  Created by ddy on 2018/3/19.
//

#import <UIKit/UIKit.h>

@interface JingDongCell : UITableViewCell

@property (strong, nonatomic) UIImageView *goodsImage;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *endPriceLabel;
@property (strong, nonatomic) UILabel *priceLabel;
@property (strong, nonatomic) UIButton *getTicketsBtn;
@property (strong, nonatomic) UIImageView *priceImage;
@property (nonatomic, strong) GoodsModel * goodsModel ;

@end

