//
//  JingDongCell.m
//  DingDingYang
//
//  Created by ddy on 2018/3/19.
//

#import "JingDongCell.h"

@implementation JingDongCell


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    _goodsModel=goodsModel;
    
    [_goodsImage sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] placeholderImage:PlaceholderImg];
    _endPriceLabel.font = [UIFont systemFontOfSize:15*HWB];
    _nameLabel.text = _goodsModel.goodsName;
    _endPriceLabel.text = FORMATSTR(@"￥%@",_goodsModel.endPrice);
    _priceLabel.text = FORMATSTR(@"原价:￥%@",_goodsModel.price);
    
    if (role_state==0) {
        [_getTicketsBtn setTitle:@"领券" forState:0];
        //无券或者原价等于券后价 隐藏领券按钮
        if ([_goodsModel.price isEqualToString:_goodsModel.endPrice]){
            _priceLabel.hidden=YES;
            _getTicketsBtn.hidden=YES;
        }else{
            _priceLabel.hidden=NO;
            _getTicketsBtn.hidden=NO;
            _getTicketsBtn.hidden=NO;
        }
    }else{
        _getTicketsBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        _getTicketsBtn.titleLabel.numberOfLines=2;
        _getTicketsBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [_getTicketsBtn setTitle:[NSString stringWithFormat:@"分享赚约\n￥%@",_goodsModel.commission] forState:0];
        if ([_goodsModel.commission isEqualToString:@"0"]) {
            _getTicketsBtn.titleLabel.font=[UIFont systemFontOfSize:14*HWB];
            [_getTicketsBtn setTitle:@"购买" forState:0];
        }
    }
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self goodsImage];
        [self nameLabel];
        [self priceImage];
        [self endPriceLabel];
        [self priceLabel];
        [self getTicketsBtn];
    }
    return self;
}


-(UIImageView *)goodsImage{
    if (!_goodsImage) {
        _goodsImage = [[UIImageView alloc]initWithImage:PlaceholderImg];
        [self.contentView addSubview:_goodsImage];
        [_goodsImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.offset(12);
            make.bottom.offset(-12);
            make.width.equalTo(_goodsImage.mas_height);
        }];
    }
    return _goodsImage;
}

-(UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel=[UILabel new];
        _nameLabel.text=@"商品名称商品名称";
        _nameLabel.font=[UIFont systemFontOfSize:13*HWB];
        _nameLabel.numberOfLines=2;
        _nameLabel.textColor=Black51;
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImage.mas_right).offset(10);
            make.top.equalTo(_goodsImage.mas_top);
            make.right.offset(-10);
        }];
    }
    return _nameLabel;
}

-(UIImageView *)priceImage{
    if (!_priceImage) {
        _priceImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"JDPriceImage"]];
        [self.contentView addSubview:_priceImage];
        [_priceImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_goodsImage.mas_bottom).offset(-5);
            make.left.equalTo(_nameLabel.mas_left);
        }];
    }
    return _priceImage;
}

-(UILabel *)endPriceLabel{
    if (!_endPriceLabel) {
        _endPriceLabel = [UILabel new];
        _endPriceLabel.text = @"￥8888.8";
        _endPriceLabel.textColor = LOGOCOLOR;
        _endPriceLabel.font = [UIFont systemFontOfSize:13*HWB];
        [self.contentView addSubview:_endPriceLabel];
        [_endPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_priceImage.mas_centerY);
            make.left.equalTo(_priceImage.mas_right).offset(5);
        }];
    }
    return _endPriceLabel;
}


-(UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [UILabel new];
        _priceLabel.text=@"原价:￥8888.8";
        _priceLabel.textColor=Black102;
        _priceLabel.font=[UIFont systemFontOfSize:10.5*HWB];
        [self.contentView addSubview:_priceLabel];
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_left).offset(5);
            make.bottom.equalTo(_priceImage.mas_top).offset(-5);
        }];
        
        UIView * line = [UIView new];
        line.backgroundColor = Black102;
        [_priceLabel addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_priceLabel.mas_centerY);
            make.left.right.offset(0);
            make.height.offset(1);
        }];
    }
    return _priceLabel;
}

-(UIButton *)getTicketsBtn{
    if (!_getTicketsBtn) {
        _getTicketsBtn = [UIButton new];
        _getTicketsBtn.layer.borderWidth=1;
        _getTicketsBtn.layer.borderColor=LOGOCOLOR.CGColor;
        _getTicketsBtn.layer.cornerRadius=4;
        _getTicketsBtn.clipsToBounds=YES;
        _getTicketsBtn.backgroundColor=COLORWHITE;
        _getTicketsBtn.titleLabel.font=[UIFont systemFontOfSize:14*HWB];
        [_getTicketsBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        [_getTicketsBtn setTitle:@"领券" forState:UIControlStateNormal];
        [self.contentView addSubview:_getTicketsBtn];
        [_getTicketsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_priceImage.mas_centerY);
            make.height.offset(28*HWB);
            make.width.offset(56*HWB);
        }];
    }
    return _getTicketsBtn;
}



@end
