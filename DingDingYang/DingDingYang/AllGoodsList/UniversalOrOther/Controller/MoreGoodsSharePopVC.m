//
//  MoreGoodsSharePopVC.m
//  DingDingYang
//
//  Created by ddy on 2018/4/13.
//

#define pl_share_title   @"pl_share_title"  // 批量分享标题
#define pl_share_content @"pl_share_content"  // 批量分享填写的文案


#import "MoreGoodsSharePopVC.h"

@interface MoreGoodsSharePopVC ()<UITextViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong) UIView * backView;
@property(nonatomic,strong) UITextField * titleText;
@property(nonatomic,strong) UITextView  * contentText;
@property(nonatomic,strong) UIButton * cancelBtn;
@property(nonatomic,strong) UIButton * shareBtn;
@property(nonatomic,assign) float textFieldY; //标记输入框位置

@property(nonatomic,assign) float backVH; // 背景的高度
@end

@implementation MoreGoodsSharePopVC


+(void)showShareEditView:(UIViewController*)fatherVc shareGoodsIdAry:(NSArray*)idAry goodsAry:(NSArray*)gdsary{
    MoreGoodsSharePopVC * shareEditVc = [[MoreGoodsSharePopVC alloc]init];
    shareEditVc.goodsIdAry = idAry;
    shareEditVc.gdsary = gdsary;
    [fatherVc presentViewController:shareEditVc animated:NO completion:nil];
}

-(instancetype)init{
    self = [super init];
    if (self) { self.modalPresentationStyle = UIModalPresentationCustom; }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"商品集合页分享文案编辑";
    
    self.view.backgroundColor = COLORCLEAR;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSelf)];
    tap.delegate = self; [self.view addGestureRecognizer:tap];
    
    if (SysGoodsShareType==1) { // 小程序
        _backVH = 160*HWB;
    }else{
        _backVH = 260*HWB;
    }
    
    [self backView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.26 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showViewAndAnimation];
    });
    
   
}

-(void)keyboardFrameChange:(NSNotification*)keyboardInfo{
    NSDictionary *KBInfo = [keyboardInfo userInfo];
    NSValue *aValue = [KBInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat  keyBoardY=keyboardRect.origin.y;
    
    if (keyBoardY<ScreenH) {
        [UIView animateWithDuration:0.26 animations:^{
            [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.view.mas_centerX);
                make.width.offset(260*HWB);
                make.height.offset(_backVH);
                make.bottom.offset(-(ScreenH-keyBoardY)-20*HWB);
            }];
            [_backView.superview layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }else{
        [UIView animateWithDuration:0.26 animations:^{
            [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.view.mas_centerX);
                make.centerY.equalTo(self.view.mas_centerY);
                make.width.offset(260*HWB);
                make.height.offset(_backVH);
            }];
            [_backView.superview layoutIfNeeded];
        } completion:^(BOOL finished) { }];
    }
   
}


-(UIView*)backView{
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.clipsToBounds = YES;
        _backView.backgroundColor = COLORWHITE;
        _backView.center = self.view.center;
        [_backView cornerRadius:10*HWB];
        _backView.alpha = 0;
        _backView.userInteractionEnabled = YES;
        _backView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        [self.view addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.centerY.equalTo(self.view.mas_centerY);
            make.width.offset(260*HWB);
            make.height.offset(_backVH);
        }];
        
        
        UILabel * bigTitLab = [UILabel labText:@"编辑分享文案" color:Black51 font:15*HWB];
        [_backView addSubview:bigTitLab];
        [bigTitLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(10*HWB); make.height.offset(20*HWB);
            make.centerX.equalTo(_backView.mas_centerX);
        }];
        
        UILabel * shareTitLab = [UILabel labText:@"分享标题" color:Black102 font:13*HWB];
        [_backView addSubview:shareTitLab];
        [shareTitLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.top.offset(40*HWB);
            make.height.offset(15*HWB);
        }];
        
        _titleText = [UITextField textColor:Black102 PlaceHorder:@"请输入分享的标题" font:13*HWB];
        [_titleText boardWidth:1 boardColor:Black204 corner:8*HWB];
        [_titleText addTarget:self action:@selector(keyBoardDown) forControlEvents:(UIControlEventEditingDidEndOnExit)];
        _titleText.delegate = self;
        [_backView addSubview:_titleText];
        [_titleText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.right.offset(-15*HWB);
            make.top.offset(60*HWB); make.height.offset(30*HWB);
        }];
        if (NSUDTakeData(pl_share_title)) {
            _titleText.text = (NSString*)NSUDTakeData(pl_share_title);
        }
        
        if (SysGoodsShareType==2) { // web页面(需要标题和内容)
            UILabel * contentLab = [UILabel labText:@"分享文案" color:Black102 font:13*HWB];
            [_backView addSubview:contentLab];
            [contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.offset(15*HWB); make.height.offset(15*HWB);
                make.top.offset(100*HWB);
            }];
            
            _contentText = [[UITextView alloc]init];
            _contentText.delegate = self ;
            _contentText.textColor = Black102 ;
            _contentText.backgroundColor = COLORWHITE ;
            _contentText.font = [UIFont systemFontOfSize:13*HWB];
            _contentText.textContainerInset = UIEdgeInsetsMake(5,5,5,5);
            
            [_contentText boardWidth:1 boardColor:Black204 cornerRadius:8*HWB];
            [_backView addSubview:_contentText];
            [_contentText mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.offset(15*HWB); make.right.offset(-15*HWB);
                make.top.offset(120*HWB); make.bottom.offset(-65*HWB);
            }];
            
            if (NSUDTakeData(pl_share_content)) {
                _contentText.text = (NSString*)NSUDTakeData(pl_share_content);
            }
        }
        _cancelBtn = [UIButton titLe:@"取消" bgColor:Black204 titColorN:COLORWHITE font:14*HWB];
        [_cancelBtn cornerRadius:16*HWB];
        [_backView addSubview:_cancelBtn];
        [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(32*HWB);
            make.left.offset(16*HWB); make.bottom.offset(-17*HWB);
            make.right.equalTo(_backView.mas_centerX).offset(-8*HWB);
        }];
        
        _shareBtn = [UIButton titLe:@"分享" bgColor:LOGOCOLOR titColorN:COLORWHITE font:14*HWB];
        [_shareBtn cornerRadius:16*HWB];
        [_backView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(32*HWB);
            make.right.offset(-16*HWB); make.bottom.offset(-17*HWB);
            make.left.equalTo(_backView.mas_centerX).offset(8*HWB);
        }];
        
        ADDTARGETBUTTON(_cancelBtn, cancelShare)
        ADDTARGETBUTTON(_shareBtn, startShare)
    }
    return _backView;
}

// 点击分享
-(void)startShare{

    if (_titleText.text==nil||_titleText.text.length==0) {
        MBError(@"请输入分享标题!")  return ;
    }
    
    NSUDSaveData(_titleText.text, pl_share_title)
    
    @try {
        __block UIImage * suoLuoTuImg = [UIImage imageNamed:@"logo"];
        if (_gdsary.count>0) {
            GoodsModel * model = _gdsary[0];
            [NetRequest downloadImageByUrl:model.smallPic success:^(id result) {
                suoLuoTuImg = result;
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {  }];
        }
        
//        if (SysGoodsShareType==1) { // 小程序
//            ResignFirstResponder
//            NSDictionary * pdic = @{@"type":@"0",@"goodsIds":[_goodsIdAry componentsJoinedByString:@","]};
//
//            [NetRequest requestTokenURL:url_mini_program_share parameter:pdic success:^(NSDictionary *nwdic) {
//
//                ServerShareModel * model = [[ServerShareModel alloc]initWithDic:nwdic[@"bean"]];
//                CustomShareModel * shareModel = [[CustomShareModel alloc] initWith:model];
//                shareModel.miniPreviewImg = suoLuoTuImg;
//                shareModel.share_title = _titleText.text;
//                shareModel.shareType = 4;
//                [CustomUMShare shareByCustomModel:shareModel result:^(NSInteger result) {
//                    // 1: 分享成功
//                    if (result==1) { [self removeSelf]; }
//                }];
//
//            } failure:^(NSString *failure) {
//                SHOWMSG(failure)
//            }];
//
//        }else{
//
            if (_contentText.text.length>0) {
                ResignFirstResponder
                NSUDSaveData(_titleText.text, pl_share_content)
                NSDictionary * pdic = @{@"nums":@(_goodsIdAry.count),@"goodsIds":[_goodsIdAry componentsJoinedByString:@","]};
                // 分享web页面
                [NetRequest requestTokenURL:url_goods_poster_frame parameter:pdic success:^(NSDictionary *nwdic) {
                    if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                        CustomShareModel * shareModel = [[CustomShareModel alloc] init];
                        shareModel.share_url = REPLACENULL(nwdic[@"domain"]);
                        shareModel.share_describe = _contentText.text;
                        shareModel.share_title = _titleText.text;
                        shareModel.share_thumbImg = suoLuoTuImg;
                        shareModel.sharePlatform = 1; // 分享到微信好友
                        shareModel.shareType = 1; // 分享web页面
                        [CustomUMShare shareByCustomModel:shareModel result:^(NSInteger result) {
                            // 1: 分享成功
                            if (result==1) { [self removeSelf]; }
                        }];
                    }else{
                        SHOWMSG(nwdic[@"result"])
                    }
                   
                } failure:^(NSString *failure) {
                    SHOWMSG(failure)
                }];
            }else{
                MBError(@"请输入分享内容描述!")
            }
            
//        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
   
}



// 取消分享
-(void)cancelShare{
    if ([_titleText isFirstResponder]||[_contentText isFirstResponder]) {
        ResignFirstResponder
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self removeSelf];
        });
    }else{
        [self removeSelf];
    }
}



-(void)showViewAndAnimation{
   
    [UIView animateWithDuration:0.26 animations:^{
        self.view.backgroundColor = RGBA(0, 0, 0, 0.3);
        _backView.alpha = 1;
        _backView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        [_titleText becomeFirstResponder];
    }];
    
}

-(void)removeSelf{
    
    [UIView animateWithDuration:0.26 animations:^{
        self.view.backgroundColor = COLORCLEAR;
        _backView.alpha = 0;
        _backView.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        ResignFirstResponder
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

-(void)keyBoardDown{ }


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_backView.frame, point)) {
        return NO;
    }
    if ([_titleText isFirstResponder]||[_contentText isFirstResponder]) {
        ResignFirstResponder
        return NO;
    }
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
