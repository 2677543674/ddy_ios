//
//  UnGoodsClasslistBigVC.h
//  DingDingYang
//
//  Created by ddy on 2018/12/18.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "DDYViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UnGoodsClasslistBigVC : DDYViewController
@property(nonatomic,strong) MClassModel * mcModel;

// 1:淘宝 2:京东 3:拼多多 4:唯品会 5:多麦
@property(nonatomic,assign) NSInteger goodsSource;
@end

NS_ASSUME_NONNULL_END



/*
switch (_goodsSource) {
        
    case 1:{ // 淘宝
        
    } break;
        
    case 2:{ // 京东
        
    } break;
        
    case 3:{ // 拼多多
        
    } break;
        
    case 4:{ // 唯品会
        
    } break;
        
    case 5:{ // 多麦
        
    } break;
        
    default: break;
}
*/
