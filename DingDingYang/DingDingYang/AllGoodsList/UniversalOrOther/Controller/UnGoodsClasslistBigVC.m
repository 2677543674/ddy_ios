//
//  UnGoodsClasslistBigVC.m
//  DingDingYang
//
//  Created by ddy on 2018/12/18.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "UnGoodsClasslistBigVC.h"
#import "GoodsClassModel.h"
#import "DidSelectShareView.h"
#import "GoodsPoster.h"
#import "MoreGoodsSharePopVC.h"
#import "SearchOldVC.h"

#import "UnGoodsClassTitCell.h"

@interface UnGoodsClasslistBigVC () <UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UnClassListCltCellDelegate>

@property(nonatomic,strong) UICollectionView * classTitView; // 标题选项卡
@property(nonatomic,strong) UICollectionView * goodsCltView; // 商品选项卡
@property(nonatomic,strong) UIImageView * linesImgv; // 横的下划线

// 记录当前选中状态和左右两侧标题或表格的位置(用于动画效果)
@property(nonatomic,assign) NSInteger selectIndex; // 记录选中的分区
@property(nonatomic,assign) float scrollX;          // 当前选中标题后的标题表格偏移量
@property(nonatomic,assign) float lastCellCenterX ; // 当前选中的cell停留的中间点
@property(nonatomic,assign) float leftWidth;   // 左侧标题宽
@property(nonatomic,assign) float centerWidth; // 当前选中的标题的宽
@property(nonatomic,assign) float rightWidth;  // 右侧标题的宽

// 记录子cell的所有数据
@property(nonatomic,strong) NSMutableArray * selectTitAry; // 可供选择的标题数组
@property(nonatomic,strong) NSMutableArray * allDataAry; // 存放所有数据
@property(nonatomic,strong) NSMutableArray * allListCtY; // 存放记录所有列表的偏移量

// 批量选择分享有关
@property(nonatomic,strong) DidSelectGoodsCltView * didSelectCltView;
@property(nonatomic,strong) NSMutableDictionary * didSelectGoodsDic; // 记录已选商品id和所在的cell的tag(索引)
@property(nonatomic,strong) DidSelectShareView * didShareSView; // 开始选择后的弹窗按钮
@property(nonatomic,strong) NSMutableArray * didSelectGoodsAry; // 用来记录选择的商品(商品model)
@property(nonatomic,strong) UIButton * didShareBtn;
@end

@implementation UnGoodsClasslistBigVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _selectTitAry = [NSMutableArray array];
    _didSelectGoodsAry = [NSMutableArray array];
    _didSelectGoodsDic = [NSMutableDictionary dictionary];
    
    
    switch (_goodsSource) {
            
        case 1:{ // 淘宝
            
        } break;
           
        case 2:{ // 京东
            self.title = @"京东";
            
            if (TOKEN.length==0) {
                [LoginVc showLoginVC:self fromType:0 loginBlock:^(NSInteger result) {
                    if (result == 1) {
                        [GoodsClassModel getGoodsClassModelAryCacheJD:^(NSMutableArray *modelCacheAry) {
                            [self addSelectAry:modelCacheAry];
                        } newDataFromNetWork:^(NSMutableArray *modelNewAry) {
                            [self addSelectAry:modelNewAry];
                        }];
                    }
//                    if (result == 2) {
//                        TopNavc.topViewController.tabBarController.selectedIndex = 0 ;
//                    }
                }];
                return;
            }else{
                [GoodsClassModel getGoodsClassModelAryCacheJD:^(NSMutableArray *modelCacheAry) {
                    [self addSelectAry:modelCacheAry];
                } newDataFromNetWork:^(NSMutableArray *modelNewAry) {
                    [self addSelectAry:modelNewAry];
                }];
            }
           
        } break;
            
        case 3:{ // 拼多多
            self.title = @"拼多多";
            [GoodsClassModel getGoodsClassModelAryCachePdd:^(NSMutableArray *modelCacheAry) {
                [self addSelectAry:modelCacheAry];
            } newDataFromNetWork:^(NSMutableArray *modelNewAry) {
                [self addSelectAry:modelNewAry];
            }];
        } break;
            
        case 4:{ // 唯品会
            self.title = @"唯品会";
            [GoodsClassModel getGoodsClassModelAryCacheWPH:^(NSMutableArray *modelCacheAry) {
                [self addSelectAry:modelCacheAry];
            } newDataFromNetWork:^(NSMutableArray *modelNewAry) {
                [self addSelectAry:modelNewAry];
            }];
        } break;
            
        case 5:{ // 多麦
            self.title = @"多麦";
            [GoodsClassModel getGoodsClassModelAryCacheDM:^(NSMutableArray *modelCacheAry) {
                [self addSelectAry:modelCacheAry];
            } newDataFromNetWork:^(NSMutableArray *modelNewAry) {
                [self addSelectAry:modelNewAry];
            }];
        } break;
            
        default:{ }break;
    }
    
    if (_mcModel&&_mcModel.title.length>0) {
        self.title = _mcModel.title;
    }
    
}

#pragma mark ===>>> 初始化一些内容
-(void)addSelectAry:(NSMutableArray*)ary{
    [_selectTitAry removeAllObjects];
    [_selectTitAry addObjectsFromArray:(NSArray*)ary];
    
    [self initPtdicData];
    [self classTitView];
    [self goodsCltView];
 
//    [self didShareSView];
    
}

// 根据标题的个数，调整数据的存储个数
-(void)initPtdicData{
    _allDataAry = [NSMutableArray array];
    _allListCtY = [NSMutableArray array];
    for (NSInteger i=0; i<_selectTitAry.count; i++) {
        
        GoodsClassModel * model = _selectTitAry[i];
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        
        NSLog(@"分类id：%@ 名称：%@",model.gcId,model.title);
        
        switch (_goodsSource) {
           
            case 1:{ // 淘宝
                
            } break;
                
            case 2:{ // 京东
                dic[@"parameter"] = @{@"st":@"1",@"themeId":model.gcId};
                dic[@"dataList"] = @[];  dic[@"banner"] = @[];
            } break;
                
            case 3:{ // 拼多多
                dic[@"parameter"] = @{@"st":@"1",@"catId":model.gcId};
                dic[@"dataList"] = @[];  dic[@"banner"] = @[];
                if (_mcModel) {
                    NSMutableDictionary * par = [NSMutableDictionary dictionary];
                    [par addEntriesFromDictionary:_mcModel.params];
                    par[@"catId"] = model.gcId; par[@"st"] = @"1";
                    dic[@"parameter"] = par;
                }
            } break;
                
            case 4:{ // 唯品会
                dic[@"parameter"] = @{@"pageNum":@"1",@"categoryType":model.gcId};
                dic[@"dataList"] = @[];  dic[@"banner"] = @[];
                if (_mcModel) {
                    NSMutableDictionary * par = [NSMutableDictionary dictionary];
                    [par addEntriesFromDictionary:_mcModel.params];
                    par[@"categoryType"] = model.gcId; par[@"pageNum"] = @"1";
                    dic[@"parameter"] = par;
                }
            } break;
                
            case 5:{ // 多麦
                dic[@"parameter"] = @{@"p":@"1",@"cateId":model.gcId};
                dic[@"dataList"] = @[];  dic[@"banner"] = @[];
            } break;
                
            default: break;
        }
        
        [_allDataAry addObject:dic];
        [_allListCtY addObject:@"0"];
    }

}



#pragma mark ===>>> 上方标题列表
-(UICollectionView*)classTitView{
    if (!_classTitView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _classTitView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _classTitView.delegate = self ; _classTitView.dataSource = self ;
        _classTitView.showsHorizontalScrollIndicator = NO ;
        _classTitView.backgroundColor = COLORWHITE ;
        _classTitView.tag = 100;
        [self.view addSubview:_classTitView];
        [_classTitView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(-40*HWB);
            make.top.offset(0); make.height.offset(30*HWB);
        }];
        
        [_classTitView registerClass:[UnGoodsClassTitCell class] forCellWithReuseIdentifier:@"class_title"];
        
        // 计算和记录首个标题的宽高
        GoodsClassModel * model0 = _selectTitAry[0];
        
        _linesImgv = [[UIImageView alloc]init];
        _linesImgv.backgroundColor = LOGOCOLOR ;
        float wd = StringSize(model0.title, 13*HWB).width + 26*HWB ;
        _linesImgv.center = CGPointMake(wd/2, 30*HWB-1);
        _linesImgv.bounds = CGRectMake(0, 0, wd-26*HWB,1.5);
        [_classTitView addSubview:_linesImgv];
        
        _lastCellCenterX = wd/2;
        _selectIndex = 0;
        _leftWidth = 0;
        _centerWidth = StringSize(model0.title, 13*HWB).width;
        if (_selectTitAry.count>1) {
            GoodsClassModel * model1 = _selectTitAry[1];
            _rightWidth = StringSize(model1.title, 13*HWB).width;
        }else{
            _rightWidth = 0 ;
        }
        
        // 右侧搜索按钮
        UIButton * searchBtn = [UIButton new];
        [searchBtn setImage:[UIImage imageNamed:@"sh_imgn"] forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(searchBtn, clickSearch);
        [self.view addSubview:searchBtn];
        [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0); make.top.offset(0);
            make.height.offset(30*HWB);
            make.width.offset(40*HWB);
        }];
        
    } else { [_classTitView reloadData]; }
    
    return _classTitView;
}

-(void)clickSearch{
    SearchOldVC * oldVC = [SearchOldVC new];
    oldVC.hidesBottomBarWhenPushed = YES ;
    oldVC.searchType = _goodsSource;
    PushTo(oldVC, YES)
}


#pragma mark ===>>> 下方商品列表
-(UICollectionView*)goodsCltView{
    if (!_goodsCltView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _goodsCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _goodsCltView.delegate = self ; _goodsCltView.dataSource = self ;
        _goodsCltView.showsHorizontalScrollIndicator = NO ;
        _goodsCltView.pagingEnabled = YES ;
        _goodsCltView.backgroundColor = COLORGROUP ;
        _goodsCltView.tag = 200;
        [self.view addSubview:_goodsCltView];
        [_goodsCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0);
            make.top.offset(36*HWB); make.bottom.offset(0);
        }];
        [_goodsCltView registerClass:[UnGoodsClassListCell class] forCellWithReuseIdentifier:@"class_list"];
        
    } else { [_goodsCltView reloadData]; }
    return _goodsCltView;
}

#pragma mark ===>>> UICollectionView代理

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _selectTitAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==100) { // 标题
        GoodsClassModel * model = _selectTitAry[indexPath.item];
        UnGoodsClassTitCell * titleCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"class_title" forIndexPath:indexPath];
        titleCell.titleLab.text = model.title;
        titleCell.titleLab.textColor = Black51 ;
        titleCell.titleLab.font = [UIFont systemFontOfSize:13*HWB];
        if (indexPath.item==_selectIndex) {
            _linesImgv.center = CGPointMake(titleCell.center.x,30*HWB-1);
            float wi = StringSize(model.title, 13*HWB).width;
            _linesImgv.bounds = CGRectMake(0, 0, wi , 1.5);
            titleCell.titleLab.textColor = LOGOCOLOR ;
            titleCell.titleLab.font = [UIFont systemFontOfSize:14*HWB];
        }
        return titleCell;
    }
    
    // 商品列表
    UnGoodsClassListCell * listCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"class_list" forIndexPath:indexPath];
    listCell.goodsType = _goodsSource; listCell.tag = indexPath.item+10; listCell.delegate = self;
    [listCell addParIsStartSlt:_didShareBtn.selected diSltGDic:_didSelectGoodsDic lastStopY:_allListCtY[indexPath.item]];
    listCell.allDic = _allDataAry[indexPath.item];
    return listCell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==100) { // 选中标题
        if (indexPath.item==_selectIndex) { return ; }
        _selectIndex = indexPath.item;
        [self changeFrameState:1];
    }
}

#pragma mark ===>>> CollectionViewFlowLayout代理
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag==100) {
        GoodsClassModel * model = _selectTitAry[indexPath.item];
        return CGSizeMake(StringSize(model.title, 13*HWB).width+26*HWB, 30*HWB);
    }
    return CGSizeMake(ScreenW, _goodsCltView.frame.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0 ;
}

#pragma mark ===>>> UIScrollView 代理监控滑动事件
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (_didShareSView) {
        if (_didShareSView.checkBtn.selected==YES) {
            _didShareSView.checkBtn.selected=NO;
            [_didSelectCltView hiddenSelfAndAnimation];
        }
    }
    
    if (scrollView.tag==200) {
        @try {
            float sclOffset = scrollView.contentOffset.x ;
            
            if (_lastCellCenterX>0) {  // 向右滑动
                
                if (sclOffset>_scrollX&&sclOffset<_selectTitAry.count*ScreenW) {
                    float bili = ScreenW/(_rightWidth + 26*HWB) ; // 比例
                    float chaju = sclOffset - _scrollX ;
                    float linsW = _centerWidth + chaju/bili ;
                    float lineCenterX = _lastCellCenterX+chaju/bili/2 ;
                    _linesImgv.center = CGPointMake(lineCenterX, 30*HWB-1);
                    _linesImgv.bounds = CGRectMake(0, 0, linsW, 1.5);
                }
                
                if (sclOffset<_scrollX&&sclOffset>0) { // 向左滑动
                    float bili = ScreenW/(_leftWidth + 26*HWB) ; // 比例
                    float chaju = _scrollX - sclOffset;
                    float linsW = _centerWidth + chaju/bili ;
                    float lineCenterX = _lastCellCenterX-chaju/bili/2 ;
                    _linesImgv.center = CGPointMake(lineCenterX, 30*HWB-1);
                    _linesImgv.bounds = CGRectMake(0, 0, linsW, 1.5);
                }
            }
            
        } @catch (NSException *exception) { } @finally { }
    }
}

// 手指滑动结束
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag==200) { // 商品列表滑动
        NSInteger row1 =  _goodsCltView.contentOffset.x/ScreenW ;
        _selectIndex = row1 ;
        [self changeFrameState:2];
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView.tag==200) {
        [_goodsCltView reloadData];
    }
}

#pragma mark ===>>> 记录偏移和所需控件的位置
-(void)changeFrameState:(NSInteger)type{
    //  type 1: 点击上面标题触发的偏移  2:滑动触发的偏移
    @try {
        
        // 修改偏移量
        _scrollX = _selectIndex * ScreenW;
        GoodsClassModel * modelCentre = _selectTitAry[_selectIndex];
        _centerWidth  = StringSize(modelCentre.title,13*HWB).width;
        
        if (_selectIndex>0) {
            GoodsClassModel * modelLeft = _selectTitAry[_selectIndex-1];
            _leftWidth = StringSize(modelLeft.title,13*HWB).width;
        } else{ _leftWidth = 0 ; }
        
        if (_selectIndex<_selectTitAry.count-1) {
            GoodsClassModel * modelRight = _selectTitAry[_selectIndex+1];
            _rightWidth = StringSize(modelRight.title,13*HWB).width;
        }else{ _rightWidth = 0; }
        
        
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:_selectIndex inSection:0];
        [_classTitView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [_goodsCltView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        
        if (type==1) { [_goodsCltView reloadItemsAtIndexPaths:@[indexPath]] ;}
        UnGoodsClassTitCell * selectCell = (UnGoodsClassTitCell*)[_classTitView cellForItemAtIndexPath:indexPath];
        if (selectCell&&[selectCell isKindOfClass:[UnGoodsClassTitCell class]]) {
            _lastCellCenterX = selectCell.center.x ;
            [UIView animateWithDuration:0.2 animations:^{
                _linesImgv.center = CGPointMake(selectCell.center.x,30*HWB-1);
                _linesImgv.bounds = CGRectMake(0, 0, _centerWidth , 1.5);
                selectCell.titleLab.textColor = LOGOCOLOR ;
            }];
        }else{
            _lastCellCenterX = 0 ;
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                @try {
                    if (_lastCellCenterX>=0&&_selectTitAry!=nil) {
                        _lastCellCenterX = _centerWidth/2+13*HWB ;
                        for (NSInteger i=0; i<_selectIndex; i++) {
                            GoodsClassModel * model = _selectTitAry[i];
                            _lastCellCenterX = StringSize(model.title, 13*HWB).width+26*HWB + _lastCellCenterX;
                        }
                    }
                } @catch (NSException *exception) { } @finally { }
            });
        }
        
    } @catch (NSException *exception) { // 如果上面代码执行失败，则执行下面代码
        
        _scrollX = _selectIndex * ScreenW;
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:_selectIndex inSection:0];
        UnGoodsClassTitCell * selectCell = (UnGoodsClassTitCell*)[_classTitView cellForItemAtIndexPath:indexPath];
        if (selectCell&&[selectCell isKindOfClass:[UnGoodsClassTitCell class]]) {
            GoodsClassModel * model = _selectTitAry[_selectIndex];
            float wi = StringSize(model.title, 13*HWB).width;
            [UIView animateWithDuration:0.2 animations:^{
                _linesImgv.center = CGPointMake(selectCell.center.x,30*HWB-1);
                _linesImgv.bounds = CGRectMake(0, 0, wi , 1.5);
                selectCell.titleLab.textColor = LOGOCOLOR ;
            }];
        }
        [_classTitView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [_goodsCltView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
    } @finally {
        [_classTitView reloadData];
    }
    
}

#pragma mark ===>>> 记录商品列表滑动位置和列表数据的代理
// 列表开始滑动
-(void)beginDragNowCell:(UnGoodsClassListCell *)ctcell{
    if (_didShareSView) {
        if (_didShareSView.checkBtn.selected==YES) {
            _didShareSView.checkBtn.selected=NO;
            [_didSelectCltView hiddenSelfAndAnimation];
        }
    }
}

// 列表数据
-(void)nowCell:(UnGoodsClassListCell*)ctcell nowClassTypeAllData:(NSDictionary*)datadic{
    NSDictionary * dic = datadic ;
    [_allDataAry replaceObjectAtIndex:ctcell.tag-10 withObject:dic];
}

// 偏移量
-(void)nowCell:(UnGoodsClassListCell*)ctcell offsetY:(NSString*)floaty{
    [_allListCtY replaceObjectAtIndex:ctcell.tag-10 withObject:floaty];
}


#pragma mark ===>>> //**** 批量选择分享有关  ****//

-(UIButton*)didShareBtn{
    if (!_didShareBtn) {
        _didShareBtn = [[UIButton alloc]init];
        _didShareBtn.frame = CGRectMake(0, 0, 52*HWB, 33);
        _didShareBtn.titleLabel.font = [UIFont systemFontOfSize:12*HWB];
        [_didShareBtn setTitle:@"批量分享" forState:(UIControlStateNormal)];
        [_didShareBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        [_didShareBtn addTarget:self action:@selector(startSelectGoods) forControlEvents:UIControlEventTouchUpInside];
        _didShareBtn.selected = NO;
        UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithCustomView:_didShareBtn];
        self.navigationItem.rightBarButtonItem = rightItem;
        
    }
    return _didShareBtn;
}

-(DidSelectShareView*)didShareSView{
    if (!_didShareSView) {
        _didShareSView = [[DidSelectShareView alloc]init];
        [self.view addSubview:_didShareSView];
        [_didShareSView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.view.mas_centerY).offset(-60*HWB);
            make.height.offset(180*HWB);  make.width.offset(42*HWB);
            make.right.offset(-10*HWB);
        }];
        __weak UnGoodsClasslistBigVC * selfView = self;
        _didShareSView.touchUpType = ^(NSInteger type) {
            switch (type) {
                case 0:{ // 隐藏已选列表
                    [selfView.didSelectCltView hiddenSelfAndAnimation];
                } break;
                    
                case 1:{ // 显示已选列表
                    if (!selfView.didSelectCltView) { [selfView didSelectCltView]; }
                    if (selfView.didSelectGoodsAry.count==0) {
                        SHOWMSG(@"你还没有选择商品哦!")
                    }else{
                        selfView.didSelectCltView.dataAry = selfView.didSelectGoodsAry;
                        [selfView.didSelectCltView showSelfAndAnimation];
                    }
                } break;
                    
                case 2:{ // 微信分享(小程序或web集合页)
                    if (_didSelectGoodsAry.count>=3) {
                        NSArray * allAry = [selfView.didSelectGoodsDic allKeys];
                        [MoreGoodsSharePopVC showShareEditView:selfView shareGoodsIdAry:allAry goodsAry:selfView.didSelectGoodsAry];
                    }else{
                        SHOWMSG(@"至少选择三个商品哦~")
                    }
                    
                } break;
                    
                case 3:{ // 朋友圈分享(合成海报)
                    if (_didSelectGoodsAry.count>=3) {
                        MBShow(@"正在加载...")
                        [GoodsPoster createShareGoodsPosterBy:selfView.didSelectGoodsAry goodsidAry:[selfView.didSelectGoodsDic allKeys] resultPoster:^(id data) {
                            MBHideHUD
                            if ([data isKindOfClass:[UIImage class]]) {
                                if (TARGET_IPHONE_SIMULATOR) { // 模拟器(显示预览)
                                    [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[data]];
                                } else { // 真机打开微信朋友圈分享
                                    CustomShareModel * shareModel = [[CustomShareModel alloc] init];
                                    shareModel.imgDataOrAry = data; //
                                    shareModel.sharePlatform = 2; // 分享朋友圈
                                    shareModel.shareType = 2; // 分享图片
                                    [CustomUMShare shareByCustomModel:shareModel result:^(NSInteger result) {
                                        if (result==1) { // 1: 分享成功
                                        }
                                    }];
                                }
                            }else{
                                SHOWMSG((NSString*)data)
                            }
                        }];
                    }else{ SHOWMSG(@"至少选择三个商品哦~") }
                } break;
                    
                default:  break;
            }
        };
    }
    return _didShareSView;
}

// 点击批量选择执行
-(void)startSelectGoods{
    
    if (_didShareBtn.selected==NO) { // 开始选择
        _didShareBtn.selected = YES;
        _didShareBtn.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_didShareBtn) { _didShareBtn.enabled = YES; }
        });
        [_didShareBtn setTitle:@"取消分享" forState:(UIControlStateNormal)];
        [_didShareSView showSelfAndAnimation];
        
    }else{ // 取消选择
        [_didSelectGoodsAry removeAllObjects];
        [_didSelectGoodsDic removeAllObjects];
        _didShareSView.allCount = 0;
        _didShareBtn.selected = NO;
        _didShareBtn.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_didShareBtn) { _didShareBtn.enabled = YES; }
        });
        [_didShareBtn setTitle:@"批量分享" forState:(UIControlStateNormal)];
        [_didShareSView hiddenSelfAndAnimation];
        if (_didShareSView.checkBtn.selected==YES&&_didSelectCltView!=nil) {
            [_didSelectCltView hiddenSelfAndAnimation];
        }
        
    }
    
    [self.goodsCltView reloadData];
    
}

#pragma mark => 展示已选商品的cltView
-(DidSelectGoodsCltView*)didSelectCltView{
    if (!_didSelectCltView) {
        _didSelectCltView = [[DidSelectGoodsCltView alloc]init];
        _didSelectCltView.frame = CGRectMake(0,HFRAME(self.view), ScreenW, 160*HWB);
        [self.view addSubview:_didSelectCltView];
        __weak UnGoodsClasslistBigVC * selfView = self;
        _didSelectCltView.removeSelectGoodsBlock = ^(GoodsModel * sgModel, NSInteger selectTag) {
            [selfView deleteSelectGoods:sgModel andSelectTag:selectTag];
        };
    }
    return _didSelectCltView;
}
// 从已选商品删除商品
-(void)deleteSelectGoods:(GoodsModel*)gmodel andSelectTag:(NSInteger)tag{
    @try {
        self.didShareSView.allCount = self.didSelectGoodsAry.count;
        // 此处开始遍历整个大字典，根据已选字典存值，确定所在大字典数据位置，修改，并刷新
        
        if (REPLACENULL(_didSelectGoodsDic[gmodel.goodsId]).length>0) {
            
            NSString * tsgStr = _didSelectGoodsDic[gmodel.goodsId];
            NSInteger celltag = [tsgStr integerValue];
            NSDictionary * allDic = _allDataAry[celltag];
            NSMutableDictionary * mtAllDic = [NSMutableDictionary dictionaryWithDictionary:allDic];
            NSMutableArray * myGoodsMdAry = [NSMutableArray arrayWithArray:NULLARY(mtAllDic[@"dataList"])];
            // 此处遍历的步骤可有可无
            for (GoodsModel * model in myGoodsMdAry) {
                if ([model.goodsId isEqual:gmodel.goodsId]&&model.isSelect==YES) {
                    model.isSelect = NO;
                    break;
                }
            }
            mtAllDic[@"dataList"] = myGoodsMdAry;
            [_allDataAry replaceObjectAtIndex:celltag withObject:mtAllDic];
            [_didSelectGoodsDic removeObjectForKey:gmodel.goodsId];
        }
    } @catch (NSException *exception) {
        if ([[_didSelectGoodsDic allKeys] containsObject:gmodel.goodsId]) {
            [_didSelectGoodsDic removeObjectForKey:gmodel.goodsId];
        }
    } @finally {
        if (_didSelectGoodsAry.count==0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.26 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                _didShareSView.checkBtn.selected = NO;
                [_didSelectCltView hiddenSelfAndAnimation];
            });
        }
        [_goodsCltView reloadData];
    }
    
}

// 选择或取消选择商品
-(void)nowCell:(UnGoodsClassListCell *)ctcell selectGoodsModel:(GoodsModel *)model isSelect:(BOOL)select{
    
    @try {
        if (select) {
            NSInteger sclttag = ctcell.tag-10;
            _didSelectGoodsDic[model.goodsId] = FORMATSTR(@"%ld",(long)sclttag);
            [_didSelectGoodsAry addObject:model];
        }else{
            
            if ([_didSelectGoodsAry containsObject:model]) {
                [_didSelectGoodsAry removeObject:model];
            }else{
                NSMutableArray * ary = [NSMutableArray arrayWithArray:_didSelectGoodsAry];
                for (NSInteger i=0;i<ary.count;i++) {
                    GoodsModel * gmd = ary[i];
                    if ([gmd.goodsId isEqual:model.goodsId]) {
                        [_didSelectGoodsAry removeObjectAtIndex:i];
                        break;
                    }
                }
            }
            
            if ([[_didSelectGoodsDic allKeys] containsObject:model.goodsId]) {
                [_didSelectGoodsDic removeObjectForKey:model.goodsId];
            }
            if (_didShareSView.checkBtn.selected==YES) {
                
            }
        }
        if (_didShareSView.checkBtn.selected==YES) {
            _didSelectCltView.dataAry = _didSelectGoodsAry;
        }
    } @catch (NSException *exception) {
        
    } @finally {
        if (_didSelectGoodsAry.count==1) {
            [_didShareSView showSelfAndAnimation];
        }
        _didShareSView.allCount = _didSelectGoodsAry.count;
    }
    
}

@end
