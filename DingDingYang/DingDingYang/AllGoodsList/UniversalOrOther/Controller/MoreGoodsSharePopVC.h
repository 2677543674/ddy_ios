//
//  MoreGoodsSharePopVC.h
//  DingDingYang
//
//  Created by ddy on 2018/4/13.
//


@interface MoreGoodsSharePopVC : DDYViewController

/**
 调用弹出文本编辑框，编辑分享标题和分享内容，分享到微信好友

 @param fatherVc 要进来的父vc
 @param idAry 商品id数组
 @param gdsary 商品model数组
 */
+(void)showShareEditView:(UIViewController*)fatherVc shareGoodsIdAry:(NSArray*)idAry goodsAry:(NSArray*)gdsary;

@property(nonatomic,strong) NSArray * goodsIdAry;
@property(nonatomic,strong) NSArray * gdsary;

@end

