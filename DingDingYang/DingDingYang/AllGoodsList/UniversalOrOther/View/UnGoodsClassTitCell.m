//
//  UnGoodsClassTitCell.m
//  DingDingYang
//
//  Created by ddy on 2018/12/18.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "UnGoodsClassTitCell.h"
#import "HOneCltCell1.h"
#import "PDDGoodsCltCellH.h"
#import "WPHGoodsCltCell.h"

@implementation UnGoodsClassTitCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _titleLab = [UILabel labText:@"标题" color:Black51 font:13*HWB];
        _titleLab.textAlignment = NSTextAlignmentCenter ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
    }
    return self;
}

@end



@implementation UnGoodsClassListCell


-(void)addParIsStartSlt:(BOOL)isStartSlt diSltGDic:(NSMutableDictionary *)diSltGDic lastStopY:(NSString *)lastStopY{
    _isStartSelect = isStartSlt;
    _didSltGoodsidDic = diSltGDic;
    _lastStopY = lastStopY;
//    [_classCltView reloadData];
}
// 是否开始选择
//-(void)setIsStartSelect:(BOOL)isStartSelect{
//    _isStartSelect = isStartSelect;
//    [_classCltView reloadData];
//}
//
//-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
//    _didSltGoodsidDic = isSltGoodsidDic;
//}
//
//-(void)setLastStopY:(NSString *)lastStopY{
//    _lastStopY = lastStopY;
//}
-(void)setAllDic:(NSDictionary *)allDic{
    @try {
        
        _allDic = allDic;
        
        switch (_goodsType) {
            case 2:{ _pageNum = [allDic[@"parameter"][@"st"] integerValue]; } break; // 京东
            case 3:{ _pageNum = [allDic[@"parameter"][@"st"] integerValue]; } break; // 拼多多
            case 4:{ _pageNum = [allDic[@"parameter"][@"pageNum"] integerValue]; } break; // 唯品会
            case 5:{ _pageNum = [allDic[@"parameter"][@"p"] integerValue]; } break; // 多麦
            default:{ } break;
        }
        
        NSDictionary * pdic = NULLDIC(allDic[@"parameter"]);
        _parameter = [NSMutableDictionary dictionaryWithDictionary:pdic];
        
        _selectType = REPLACENULL(_parameter[@"sortType"]);
        
        NSMutableArray * bannerAry = [NSMutableArray arrayWithArray:NULLARY(allDic[@"banner"])];
        if (bannerAry.count>0) {
            _bannerListAry = [NSMutableArray arrayWithArray:bannerAry];
        }else{ _bannerListAry = [NSMutableArray array]; }
        
        NSMutableArray * ary = [NSMutableArray arrayWithArray:NULLARY(allDic[@"dataList"])];
        if (ary.count==0) {
            _classListAry = [NSMutableArray array];
            [_classCltView reloadData];
            [_classCltView.mj_header beginRefreshing];
        }else{
            _classListAry = [NSMutableArray arrayWithArray:ary];
            if ([_lastStopY floatValue]>0) {
                [_classCltView setContentOffset:CGPointMake(0, [_lastStopY floatValue])];
            }
            [_classCltView reloadData];
        }
        // LOG(@"分类名称:%@  已加入的数组个数:%lu  请求所用参数:%@",allDic[@"title"],(unsigned long)ary.count,pdic)
        
    } @catch (NSException *exception) {
        SHOWMSG(@"数据出错了,建议关闭并重新打开本页面!")
    } @finally {
        [_classCltView.mj_footer resetNoMoreData];
    }
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _pageNum = 1 ;
        [self classCltView];
    }
    return self ;
}

-(void)loadClassGoodsData{
    
    NSString * goodsUrl = @"";
    
    switch (_goodsType) {
            
        case 2:{ // 京东
            goodsUrl = url_goods_list_jd;
            _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum);
        } break;
            
        case 3:{ // 拼多多
            goodsUrl = url_goods_list_pdd;
            _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum);
        } break;
            
        case 4:{ // 唯品会
            goodsUrl = url_goods_list_wph;
            _parameter[@"pageNum"] = FORMATSTR(@"%ld",(long)_pageNum);
        } break;
            
        case 5:{ // 多麦
            goodsUrl = url_goods_list_dm;
            _parameter[@"p"] = FORMATSTR(@"%ld",(long)_pageNum);
        } break;
            
        default:{ } break;
    }
    
    if (goodsUrl.length==0) { SHOWMSG(@"本地请求链接错误！") return; }
    
    [NetRequest requestType:0 url:goodsUrl ptdic:_parameter success:^(id nwdic) {
        MBHideHUD
        @try {
            
            _act = REPLACENULL(nwdic[@"act"]);
            
            if (_pageNum==1) {
                NSArray * bannerAry = NULLARY(nwdic[@"bannerList"]);
                if (_bannerListAry.count>0) { [_bannerListAry removeAllObjects]; }
                [bannerAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    BHModel * model = [[BHModel alloc]initWithDic:obj];
                    [_bannerListAry addObject:model];
                }];
                [_classListAry removeAllObjects];
                [_classCltView reloadData];
            }
            
            NSArray * listAry = NULLARY(nwdic[@"list"]);
            if (_goodsType==5) { listAry = NULLARY(nwdic[@"goodsList"]); }
            
            [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                switch (_goodsType) {
                        
                    case 4:{ // 唯品会
                        GoodsModel * model = [[GoodsModel alloc]initWithWPH:obj];
                        [_classListAry addObject:model];
                    } break;
                        
                    case 5:{ // 多麦
                        GoodsModel * model = [[GoodsModel alloc]initWithDM:obj];
                        [_classListAry addObject:model];
                    } break;
                        
                    default: { // 京东和拼多多
                        GoodsModel * model = [[GoodsModel alloc]initWithDic:obj];
                        if (_goodsType==2) { model.platform = @"京东"; }
                        if (_goodsType==3) { model.platform = @"拼多多"; }
                        [_classListAry addObject:model];
                    }break;
                }
            }];
            
            [_classCltView endRefreshType:listAry.count isReload:YES];
            
        } @catch (NSException *exception) {
            
        } @finally {
            @try {
                NSDictionary * pdic = (NSDictionary*)_parameter;
                NSArray * aryList = (NSArray*)_classListAry;
                NSArray * banList = (NSArray*)_bannerListAry;
                NSDictionary * dic = @{@"parameter":pdic,@"banner":banList,@"dataList":aryList};
                [self.delegate nowCell:self nowClassTypeAllData:dic];
            } @catch (NSException *exception) {
                NSLog(@"数据缓存失败!");
            } @finally { }
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD SHOWMSG(failure)
        [_classCltView endRefreshType:1 isReload:YES];
    }];
    
}




-(UICollectionView*)classCltView{
    if (!_classCltView) {
        
        _layout = [[UICollectionViewFlowLayout alloc]init];
//        if (@available(iOS 9.0, *)) {
//            _layout.sectionHeadersPinToVisibleBounds = YES;
//        }
        
        _classCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:_layout];
        _classCltView.delegate = self ; _classCltView.dataSource = self ;
        _classCltView.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_classCltView];
        [_classCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0); make.bottom.offset(0);
        }];
        
        // 顶部排序选项：唯品会、拼多多、京东、多麦
        [_classCltView registerClass:[WPHCltSortingHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head_wph"];
        [_classCltView registerClass:[PDDCltSortingHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head_pdd"];
        [_classCltView registerClass:[JDCltSortingHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head_jd"];
        [_classCltView registerClass:[DMCltSortingHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"screen_head_dm"];

        // 顶部可能存在的轮播图
        [_classCltView registerClass:[HOneCltCell1 class] forCellWithReuseIdentifier:@"hone_clt_cell1"];
        
        // 拼多多
        [_classCltView registerClass:[PDDGoodsCltCellH class] forCellWithReuseIdentifier:@"pdd_goods_cell1"];
        [_classCltView registerClass:[PDDGoodsCltCellV class] forCellWithReuseIdentifier:@"pdd_goods_cell2"];
        
        // 京东、唯品会、多麦 (这三个样式一样,先用唯品会的类，之后再改名)
        [_classCltView registerClass:[WPHGoodsCltCell class] forCellWithReuseIdentifier:@"un_goods_cell"];
        
        // 下拉刷新
        __weak UnGoodsClassListCell * selfView = self;
        _classCltView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            selfView.pageNum = 1;
            [selfView loadClassGoodsData];
        }];
        _classCltView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pageNum++;
            [selfView loadClassGoodsData];
        }];
    }else{  [_classCltView reloadData];  }
    
    return _classCltView ;
}


#pragma mark ===>>> UICollectionView代理

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2 ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) { return 1 ; }
    return _classListAry.count ;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        HOneCltCell1 * oneclt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hone_clt_cell1" forIndexPath:indexPath];
        oneclt1.dataAry = _bannerListAry;
        return oneclt1 ;
    }
    
    switch (_goodsType) {
            
        case 3:{ // 拼多多
            if (GoodsListStylePdd==1) { // 列表
                PDDGoodsCltCellH * goodsCell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"pdd_goods_cell1" forIndexPath:indexPath];
                if (_classListAry.count>indexPath.item) {
                    [goodsCell1 isStartS:_isStartSelect sgidDic:_didSltGoodsidDic gModel:_classListAry[indexPath.item]];
                }else{ [_classCltView reloadData]; }
                goodsCell1.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                    [self.delegate nowCell:self selectGoodsModel:goodsModel isSelect:ifSelect];
                };
                return goodsCell1 ;
            }else{ // 方块
                PDDGoodsCltCellV * goodsCell2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"pdd_goods_cell2" forIndexPath:indexPath];
                if (_classListAry.count>indexPath.item) {
                    [goodsCell2 isStartS:_isStartSelect sgidDic:_didSltGoodsidDic gModel:_classListAry[indexPath.item]];
                }else{ [_classCltView reloadData]; }
                goodsCell2.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                    [self.delegate nowCell:self selectGoodsModel:goodsModel isSelect:ifSelect];
                };
                return goodsCell2 ;
            }
        } break;
        
        default:{
            WPHGoodsCltCell * goodsCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"un_goods_cell" forIndexPath:indexPath];
            goodsCell.goodsSource = _goodsType;
            if (_classListAry.count>indexPath.item) {
                goodsCell.goodsModel = _classListAry[indexPath.item];
            }else{ [_classCltView reloadData]; }
            return goodsCell ;
        } break;
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1){
        if (_goodsType==2||_goodsType==3) {
          [PageJump pushToGoodsDetail:_classListAry[indexPath.item] pushType:_goodsType];
        }else{
            GoodsModel * model = _classListAry[indexPath.item];
            
            NSString * goodsUrl = model.cpsUrl;
            if (_goodsType==5) { goodsUrl = model.goodsUrl; }
            
            if ([_act isEqualToString:@"1"]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:goodsUrl]];
            }else{
                WebVC * web = [[WebVC alloc]init];
                web.urlWeb = goodsUrl ;
                web.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:web animated:YES];
            }
        }
        
    }
}

#pragma mark ===>>> UICollectionViewLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        if (_bannerListAry.count>0) { return CGSizeMake(ScreenW, ScreenW/2.5); }
        return CGSizeMake(ScreenW, 1);
    }
    
    switch (_goodsType) {
            
        case 3:{ // 拼多多
            if (GoodsListStylePdd==1) {
                return CGSizeMake(ScreenW, FloatKeepInt(115*HWB));
            }else{
                return CGSizeMake(ScreenW/2-3, ScreenW/2-3+90*HWB);
            }
        } break;
            
        default:{  return CGSizeMake(ScreenW, 116*HWB); } break;
    }
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    if (section==1) {
       if(GoodsListStylePdd==2) { return 6; } else { return 1 ; }
    }
    return 0 ;
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    if (section==1) { if(GoodsListStylePdd==2) { return 6 ; } }
    return 0;
    
}

#pragma mark ===>>> 区头区尾代理
-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqual:UICollectionElementKindSectionHeader]) {
        if (indexPath.section==1) {
            switch (_goodsType) {
        
                case 2:{ // 京东
                    JDCltSortingHead * jdHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"screen_head_jd" forIndexPath:indexPath];
                    jdHead.delegate = self;
                    jdHead.jdScreenView.styleType = _selectType;
                    return jdHead;
                } break;
                    
                case 3:{ // 拼多多
                    PDDCltSortingHead * pddHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"screen_head_pdd" forIndexPath:indexPath];
                    pddHead.delegate = self;
                    pddHead.pddScreenView.styleType = _selectType;
                    return pddHead;
                } break;
                    
                case 4:{ // 唯品会
                    WPHCltSortingHead * wphHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"screen_head_wph" forIndexPath:indexPath];
                    wphHead.delegate = self;
                    wphHead.wphScreenView.styleType = _selectType;
                    return wphHead;
                } break;
                    
                case 5:{ // 多麦
                    DMCltSortingHead * dmHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"screen_head_dm" forIndexPath:indexPath];
                    dmHead.delegate = self;
                    dmHead.dmScreenView.styleType = _selectType;
                    return dmHead;
                } break;
                    
                default:{ return nil; } break;
            }
           
        }
    }
    return  nil ;
}

// 区头
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if(section==1){ return CGSizeMake(ScreenW, 32*HWB); }
    return CGSizeZero ;
    
}
// 区尾
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
//    return CGSizeZero ;
//}

#pragma mark ===>>> 排序条件选择后的代理方法
// 唯品会
-(void)wphSelectGoodsScreenSortingType:(NSString *)sortingType{
    [self goodsTypeOf:4 andSortingType:sortingType];
}
// 拼多多
-(void)pddSelectGoodsScreenSortingType:(NSString *)sortingType{
    [self goodsTypeOf:3 andSortingType:sortingType];
}
// 京东
-(void)jdSelectGoodsScreenSortingType:(NSString *)sortingType{
    [self goodsTypeOf:2 andSortingType:sortingType];
}
// 多麦
-(void)dmSelectGoodsScreenSortingType:(NSString *)sortingType{
    [self goodsTypeOf:5 andSortingType:sortingType];
}

// 判断不同的平台使用不同的参数名
-(void)goodsTypeOf:(NSInteger)goodsType andSortingType:(NSString *)sortingType{
    switch ([sortingType integerValue]) {
        case 0:{ // 按综合处理
            if (![_selectType isEqual:sortingType]) {
                _selectType = sortingType;
                switch (_goodsType) {
                    case 2:{ [_parameter removeObjectForKey:@"sortType"]; } break; // 京东
                    case 3:{ [_parameter removeObjectForKey:@"sortType"]; } break; // 拼多多
                    case 4:{ [_parameter removeObjectForKey:@"sortType"]; } break; // 唯品会
                    case 5:{ [_parameter removeObjectForKey:@"sort"]; } break; // 多麦
                    default:{ } break;
                }
                [self reloadData];
            }
        } break;
            
        default:{
            if (![_selectType isEqual:sortingType]) {
                _selectType = sortingType;
                switch (_goodsType) {
                    case 2:{ _parameter[@"sortType"] = sortingType; } break; // 京东
                    case 3:{ _parameter[@"sortType"] = sortingType; } break; // 拼多多
                    case 4:{ _parameter[@"sortType"] = sortingType; } break; // 唯品会
                    case 5:{ _parameter[@"sort"] = sortingType; } break; // 多麦
                    default:{ } break;
                }
                [self reloadData];
            }
        } break;
    }
}


-(void)reloadData{
    MBShow(@"正在加载...")
    _pageNum = 1;
    [self loadClassGoodsData];
}


#pragma mark ===>>> 监控滑动，记录上次停留的位置
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _scrollStopY = scrollView.contentOffset.y;
    [self.delegate beginDragNowCell:self];
}

// 滑动时的偏移量
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (decelerate==0) {
        NSString * contentOffsetY = FORMATSTR(@"%.0f",scrollView.contentOffset.y);
        if ([contentOffsetY floatValue]>0) {
            [self.delegate nowCell:self offsetY:contentOffsetY];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSString * contentOffsetY = FORMATSTR(@"%.0f",scrollView.contentOffset.y);
    if ([contentOffsetY floatValue]>0) {
        [self.delegate nowCell:self offsetY:contentOffsetY];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    NSString * contentOffsetY = FORMATSTR(@"%.0f",scrollView.contentOffset.y);
    if ([contentOffsetY floatValue]>0) {
        [self.delegate nowCell:self offsetY:contentOffsetY];
    }
}

@end

