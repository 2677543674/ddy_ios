//
//  JDSortingView.h
//  DingDingYang
//
//  Created by ddy on 2018/12/26.
//  Copyright © 2018 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JDSortingView : UIView

@property(nonatomic,strong) UIButton * zongHeBtn;   // 综合
@property(nonatomic,strong) UIButton * couponsBtn;  // 优惠券
@property(nonatomic,strong) UIButton * endPriceBtn; // 券后价

typedef void (^JDSelectType) (NSString * stype);
@property (copy, nonatomic) JDSelectType selectBlock ;

+(void)creatIn:(UIView*)supView frame:(CGRect)rect selectType:(JDSelectType)selectBlock;

@property(nonatomic,copy) NSString * styleType; // 赋值，可修改当前选中的状态

@end



#pragma mark ===>>> 京东排序区头

@protocol SortingSelectDelegateJD <NSObject>
/**
 在cltcell的区头中选则排序后，产生回调，触发此代理
 
 @param sortingType 返回筛选条件要上传服务器的字段
 */
-(void)jdSelectGoodsScreenSortingType:(NSString*)sortingType;
@end

@interface JDCltSortingHead : UICollectionReusableView

@property(nonatomic,strong) JDSortingView * jdScreenView; // 商品筛选条件
@property(nonatomic,assign) id<SortingSelectDelegateJD>delegate;

@end

NS_ASSUME_NONNULL_END
