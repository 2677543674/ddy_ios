//
//  JDSortingView.m
//  DingDingYang
//
//  Created by ddy on 2018/12/26.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "JDSortingView.h"

@implementation JDSortingView


#pragma mark ===>>> 加号方法创建条件选择项
+(void)creatIn:(UIView *)supView frame:(CGRect)rect selectType:(SelectType)selectBlock{
    JDSortingView * sortView = [[JDSortingView alloc]init];
    sortView.frame = rect;
    sortView.selectBlock = selectBlock ;
    [supView addSubview:sortView];
}

#pragma mark ===>>> 通过属性值修改选中样式
-(void)setStyleType:(NSString *)styleType{
    
//    1综合 ； 7：销量升序；8 销量降序 ，9 佣金升序 ，10 佣金降序
    
    switch ([styleType integerValue]) {
        case 0:{ [self isSelectZongHe]; } break;  // 综合
        case 7:{ [self isSelectCoupons:YES]; } break;  // 销量 (从小到大排序)
        case 8:{ [self isSelectCoupons:NO]; } break;   // 销量 (从大到小排序)
        case 9:{ [self isSelectEndPrice:YES]; } break; // 佣金 (从小到大排序)
        case 10:{ [self isSelectEndPrice:NO]; } break; // 佣金 (从大到小排序)
      
        default:  break;
    }
    
}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = COLORWHITE;
        
        _zongHeBtn = [UIButton titLe:@"综合" bgColor:COLORCLEAR titColorN:LOGOCOLOR font:13*HWB];
        _zongHeBtn.selected = YES ;
        [self addSubview:_zongHeBtn];
        [_zongHeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.offset(0);
            make.width.offset(ScreenW/3);
        }];
        
        _couponsBtn = [UIButton image:@"down_up" titLe:@"销量" bgColor:COLORCLEAR titColorN:Black51 font:12.5*HWB];
        _couponsBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -15*HWB, 0, 0);
        _couponsBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 5,-60*HWB);
//        _couponsBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -25*HWB, 0, 0);
//        _couponsBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 5,-70*HWB);
        [self addSubview:_couponsBtn];
        [_couponsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_zongHeBtn.mas_right);
            make.top.offset(0); make.bottom.offset(0);
            make.width.offset(ScreenW/3);
        }];
        
        _endPriceBtn = [UIButton image:@"down_up" titLe:@"佣金" bgColor:COLORCLEAR titColorN:Black51 font:12.5*HWB];
        _endPriceBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -15*HWB, 0, 0);
        _endPriceBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 5,-60*HWB);
        [self addSubview:_endPriceBtn];
        [_endPriceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.offset(0);
            make.width.offset(ScreenW/3);
        }];
        
        
        ADDTARGETBUTTON(_zongHeBtn, clickZongHe)
        ADDTARGETBUTTON(_couponsBtn, clickCoupons)
        ADDTARGETBUTTON(_endPriceBtn, clickEndPrice)
        
        // 底部显示个分割线
        UIImageView * bottomLineImg = [[UIImageView alloc]init];
        bottomLineImg.backgroundColor = RGBA(235.f,235.f,235.f,1);
        [self addSubview:bottomLineImg];
        [bottomLineImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.offset(0);
            make.height.offset(0.5);
        }];
        
    }
    return self ;
}


#pragma mark ==>> 点击综合
// 综合排序
-(void)clickZongHe{
    if (_zongHeBtn.selected==NO) {
        [self isSelectZongHe];
        _selectBlock(@"0");
    }
    [self userEnabledNO];
}
-(void)isSelectZongHe{
    [self changeZongHeBtnSelect:YES];
    [self changeCouponsBtnSelect:2];
    [self changeEndPriceBtnSelect:2];
}


#pragma mark ==>> 点击销量
// 销量排序
-(void)clickCoupons{
    if (_couponsBtn.selected==NO) {
        [self isSelectCoupons:YES];
        _selectBlock(@"7");
    }else{
        [self isSelectCoupons:NO];
        _selectBlock(@"8");
    }
    [self userEnabledNO];
}
-(void)isSelectCoupons:(NSInteger)yn{
    [self changeZongHeBtnSelect:NO];
    [self changeCouponsBtnSelect:yn];
    [self changeEndPriceBtnSelect:2];
}

#pragma mark ==>> 点击佣金

-(void)clickEndPrice{
    if (_endPriceBtn.selected==NO) {
        [self isSelectEndPrice:YES];
        _selectBlock(@"9");
    }else{
        [self isSelectEndPrice:NO];
        _selectBlock(@"10");
    }
    [self userEnabledNO];
}
// 选中的是佣金
-(void)isSelectEndPrice:(NSInteger)yn{
    [self changeZongHeBtnSelect:NO];
    [self changeCouponsBtnSelect:2];
    [self changeEndPriceBtnSelect:yn];
}


#pragma mark ===>>> 修改按钮样式
// 改变综合按钮的状态
-(void)changeZongHeBtnSelect:(BOOL)yn{
    _zongHeBtn.selected = yn;
    if (yn) { // 选中
        _zongHeBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
        [_zongHeBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
    }else{ // 未选中
        _zongHeBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*HWB];
        [_zongHeBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
    }
}

// 改变销量按钮的状态
-(void)changeCouponsBtnSelect:(NSInteger)state{
    switch (state) {
        case 0: { // 从大到小排序
            _couponsBtn.selected = NO ;
            [_couponsBtn setImage:[UIImage imageNamed:@"down_H2"] forState:(UIControlStateNormal)];
            _couponsBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_couponsBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 1: { // 从小到大排序
            _couponsBtn.selected = YES ;
            [_couponsBtn setImage:[UIImage imageNamed:@"up_H2"] forState:(UIControlStateNormal)];
            _couponsBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_couponsBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 2: { // 未选中状态
            _couponsBtn.selected = NO;
            _couponsBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*HWB];
            [_couponsBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
            [_couponsBtn setImage:[UIImage imageNamed:@"down_up"] forState:(UIControlStateNormal)];
        } break;
            
        default: break;
    }
}

// 改变佣金按钮的状态
-(void)changeEndPriceBtnSelect:(NSInteger)state{
    switch (state) {
        case 0: { // 从大到小排序
            _endPriceBtn.selected = NO ;
            [_endPriceBtn setImage:[UIImage imageNamed:@"down_H2"] forState:(UIControlStateNormal)];
            _endPriceBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_endPriceBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 1: { // 从小到大排序
            _endPriceBtn.selected = YES ;
            [_endPriceBtn setImage:[UIImage imageNamed:@"up_H2"] forState:(UIControlStateNormal)];
            _endPriceBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_endPriceBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 2: { // 未选中状态
            _endPriceBtn.selected = NO;
            _endPriceBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*HWB];
            [_endPriceBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
            [_endPriceBtn setImage:[UIImage imageNamed:@"down_up"] forState:(UIControlStateNormal)];
        } break;
            
        default: break;
    }
}


#pragma mark ==>> 点击完成之后，禁止快速点击
-(void)userEnabledNO{
    _zongHeBtn.enabled = NO;
    _couponsBtn.enabled = NO;
    _endPriceBtn.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (_zongHeBtn&&_endPriceBtn&&_couponsBtn) {
            _zongHeBtn.enabled = YES;
            _couponsBtn.enabled = YES;
            _endPriceBtn.enabled = YES;
        }
    });
}
@end





#pragma mark ===>>> 京东排序区头
@implementation JDCltSortingHead

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _jdScreenView = [[JDSortingView alloc]init];
        _jdScreenView.backgroundColor = COLORWHITE ;
        [self addSubview:_jdScreenView];
        [_jdScreenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(-1);
            make.left.offset(0);  make.right.offset(0);
        }];
        __weak JDCltSortingHead * selfView = self ;
        _jdScreenView.selectBlock = ^(NSString *stype) {
            [selfView.delegate jdSelectGoodsScreenSortingType:stype];
        };
        
    }
    return self;
}
@end
