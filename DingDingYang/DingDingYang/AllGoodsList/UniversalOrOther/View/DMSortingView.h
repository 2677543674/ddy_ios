//
//  DMSortingView.h
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMSortingView : UIView

@property(nonatomic,strong) UIButton * zongHeBtn;   // 综合
//@property(nonatomic,strong) UIButton * salesBtn;    // 销量
@property(nonatomic,strong) UIButton * endPriceBtn; // 券后价
@property(nonatomic,strong) UIButton * cmRatioBtn;  // 佣金比例

typedef void (^SelectType) (NSString * stype);
@property (copy, nonatomic) SelectType selectBlock ;

+(void)creatIn:(UIView*)supView frame:(CGRect)rect selectType:(SelectType)selectBlock;

@property(nonatomic,copy) NSString * styleType; // 赋值，可修改当前选中的状态

@end




#pragma mark ===>>> 多麦排序区头

@protocol SortingSelectDelegateDM <NSObject>
/**
 在cltcell的区头中选则排序后，产生回调，触发此代理
 
 @param sortingType 返回筛选条件要上传服务器的字段
 */
-(void)dmSelectGoodsScreenSortingType:(NSString*)sortingType;
@end

@interface DMCltSortingHead : UICollectionReusableView

@property(nonatomic,strong) DMSortingView * dmScreenView; // 商品筛选条件
@property(nonatomic,assign) id<SortingSelectDelegateDM>delegate;

@end
