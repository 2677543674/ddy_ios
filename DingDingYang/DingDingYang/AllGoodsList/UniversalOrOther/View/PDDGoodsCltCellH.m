//
//  PDDGoodsCltCellH.m
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "PDDGoodsCltCellH.h"

@implementation PDDGoodsCltCellH

-(void)isStartS:(BOOL)isStartS sgidDic:(NSMutableDictionary*)sGidDic gModel:(GoodsModel*)gModel{
    _isStartSelect = isStartS;
    _isSltGoodsidDic = sGidDic;
    _goodsModel = gModel;
    [self setGoodsModel:gModel];
}

-(void)setGoodsModel:(GoodsModel *)goodsModel{
    _goodsModel = goodsModel;
    //    dispatch_async(dispatch_get_main_queue(), ^{
    
    __weak PDDGoodsCltCellH * selfCell = self;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image) {
            if (image.size.height<20&&image.size.width<20) {
                selfCell.goodsModel.smallPic = selfCell.goodsModel.goodsPic;
                [selfCell.goodsImgV sd_setImageWithURL:[NSURL URLWithString:selfCell.goodsModel.goodsPic] placeholderImage:PlaceholderImg];
            }
        }else{
            selfCell.goodsModel.smallPic = selfCell.goodsModel.goodsPic;
            [selfCell.goodsImgV sd_setImageWithURL:[NSURL URLWithString:selfCell.goodsModel.goodsPic] placeholderImage:PlaceholderImg];
        }
    }];
    
    _salesLab.text = _goodsModel.strSales;
    _oldPriceLab.text = _goodsModel.strPrice;
    _endPriceLab.text = _goodsModel.endPrice;
    _goodsNameLab.text = _goodsModel.goodsName;
    
    if (role_state==0) {
        _commissionLab.text=@"去分享";
    }else{
        _commissionLab.text = _goodsModel.strCommission;
    }
    // 判断来源
    
    //        if (_goodsModel.platform) {
    //            _platformImgV.hidden = NO;
    //            _platformImgV.image = [UIImage imageNamed:@"pddicon"];
    //        }else{
    //            _platformImgV.hidden = YES;
    //        }
    
    if ([_goodsModel.couponMoney floatValue]>0) {
        _vouchersBtn.hidden = NO;
        _oldPriceLab.hidden = NO;
        // 判断优惠券
        if (_goodsModel.attributedCoupon) {
            [_vouchersBtn setAttributedTitle:_goodsModel.attributedCoupon forState:(UIControlStateNormal)];
        }else{ [_vouchersBtn setTitle:_goodsModel.strCouponMoney forState:(UIControlStateNormal)]; }
        
        // 修改优惠券的大小
        [_vouchersBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            if (_platformImgV.hidden) { make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB); }
            else{  make.left.equalTo(_platformImgV.mas_right).offset(8*HWB); }
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(10*HWB);
            make.height.offset(15*HWB); make.width.offset(_goodsModel.couponMoneyWi);
        }];
        
        [_salesLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersBtn.mas_right).offset(8*HWB);
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(10*HWB);
            make.height.offset(15*HWB);
        }];
        
    }else{
        _oldPriceLab.hidden = YES;
        _vouchersBtn.hidden = YES;
        //            if (_goodsModel.platform) {
        //                [_salesLab mas_updateConstraints:^(MASConstraintMaker *make) {
        //                    make.left.equalTo(_platformImgV.mas_right).offset(8*HWB);
        //                    make.top.equalTo(_goodsNameLab.mas_bottom).offset(10*HWB);
        //                    make.height.offset(15*HWB);
        //                }];
        //            }else{
        //                [_salesLab mas_updateConstraints:^(MASConstraintMaker *make) {
        //                    make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB);
        //                    make.top.equalTo(_goodsNameLab.mas_bottom).offset(10*HWB);
        //                    make.height.offset(15*HWB);
        //                }];
        //            }
    }
    
    
    if (_isStartSelect) { // 开始选择
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = COLORCLEAR;
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {
            
        } @finally { }
        
    }else{ _selectBtn.hidden = YES; }
    
    //    });
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
        [self selectBtn];
        [self goodsNameLab];
        [self platformImgV];
        [self vouchersBtn];
        [self salesLab];
        [self endPriceLab];
        [self oldPriceLab];
        [self shareBtn];
        if (CANOPENWechat==NO) { _shareBtn.hidden = YES; }
        
    }
    return self ;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.backgroundColor = COLORGROUP ;
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill;
        _goodsImgV.clipsToBounds = YES;
        _goodsImgV.userInteractionEnabled = YES;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB); make.top.offset(12*HWB);
            make.bottom.offset(-12*HWB);
            make.width.equalTo(_goodsImgV.mas_height);
        }];
    }
    return _goodsImgV ;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_selectBtn setImageEdgeInsets:UIEdgeInsetsMake(-33*HWB, -33*HWB, 33*HWB, 33*HWB)];
        [_goodsImgV addSubview:_selectBtn];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}


-(UILabel*)goodsNameLab{
    if (!_goodsNameLab) {
        _goodsNameLab = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        [self.contentView addSubview:_goodsNameLab];
        [_goodsNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB);
            make.right.offset(-10*HWB);
            make.top.offset(13*HWB);
        }];
    }
    return _goodsNameLab ;
}

-(UIImageView*)platformImgV{
    if (!_platformImgV) {
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.layer.masksToBounds = YES;
        _platformImgV.layer.cornerRadius = 3*HWB ;
        _platformImgV.image = [UIImage imageNamed:@"pddicon"];
        [self.contentView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB);
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(10*HWB);
            make.height.offset(15*HWB);  make.width.offset(15*HWB);
        }];
    }
    return _platformImgV;
}

-(UIButton*)vouchersBtn{
    if (!_vouchersBtn) {
        _vouchersBtn=[UIButton titLe:@"券┊￥88" bgColor:COLORWHITE titColorN:COLORWHITE font:10*HWB];
        [_vouchersBtn setBackgroundImage:[UIImage imageNamed:@"goods_quan_bg"] forState:(UIControlStateNormal)];
        //        _vouchersBtn = [UIButton bgImg: titLe:@"券┊￥88" titColorN:COLORWHITE font:10*HWB];
        UIImage * oldImg = [UIImage imageNamed:@"goods_quan_bg"];
        UIImage * newImg = [oldImg stretchableImageWithLeftCapWidth:30 topCapHeight:22.5];
        _vouchersBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_vouchersBtn setBackgroundImage:newImg forState:(UIControlStateNormal)];
        [self.contentView addSubview:_vouchersBtn];
        [_vouchersBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (_platformImgV.hidden) { make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB); }
            else{  make.left.equalTo(_platformImgV.mas_right).offset(8*HWB); }
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(10*HWB);
            make.height.offset(15*HWB); make.width.offset(50*HWB);
        }];
    }
    return _vouchersBtn ;
}

-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"销量: 88" color:Black102 font:10*HWB];
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersBtn.mas_right).offset(8*HWB);
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(10*HWB);
            make.height.offset(15*HWB);
        }];
    }
    return _salesLab ;
}

-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        UILabel * yLab = [UILabel labText:@"￥" color:[UIColor redColor] font:10*HWB];
        [self.contentView addSubview:yLab];
        [yLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-14*HWB);
            make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB);
            //            make.left.offset();
        }];
        
        _endPriceLab = [UILabel labText:@"88" color:[UIColor redColor] font:16*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-13*HWB);
            make.left.equalTo(yLab.mas_right).offset(0);
            //            make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB);
        }];
    }
    return _endPriceLab ;
}

-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"原价:￥88" color:Black153 font:10*HWB];
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB);
            make.bottom.equalTo(_endPriceLab.mas_top).offset(-7*HWB);
        }];
        // 当成删除线使用
        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = Black153;
        [_oldPriceLab addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0); make.height.offset(1);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
        }];
    }
    return _oldPriceLab ;
}


-(UIButton*)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [[UIButton alloc]init];
        ADDTARGETBUTTON(_shareBtn, clickShare)
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5*HWB); make.bottom.offset(-10*HWB);
            make.width.offset(70*HWB); make.height.offset(48*HWB);
        }];
        
        //        UIImageView * shareImgv = [UIImageView imgName:@"list_share_img"];
        UIImageView * shareImgv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"list_share_img"]];
        [_shareBtn addSubview:shareImgv];
        [shareImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-20*HWB); make.centerX.equalTo(_shareBtn.mas_centerX);
            make.height.offset(28*HWB);  make.width.offset(28*HWB);
        }];
        
        _commissionLab = [UILabel labText:@"赚￥88" color:[UIColor redColor] font:12*HWB];
        _commissionLab.textAlignment = NSTextAlignmentCenter ;
        _commissionLab.adjustsFontSizeToFitWidth = YES ;
        [_shareBtn addSubview:_commissionLab];
        [_commissionLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-3*HWB);
            make.left.offset(0);
            make.right.offset(0);
        }];
        
    }
    return _shareBtn ;
}

#pragma mark ==>> 按钮相应事件
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = COLORCLEAR;
            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
    }
}

// 点击分享
-(void)clickShare{
    [PageJump shareGoods:_goodsModel goodsImgAry:nil];
}


@end








/*************************************************************************************************************/

#pragma mark ===>>>《方块、百年中的拼多多样式》

@implementation PDDGoodsCltCellV

-(void)isStartS:(BOOL)isStartS sgidDic:(NSMutableDictionary*)sGidDic gModel:(GoodsModel*)gModel{
    _isStartSelect = isStartS;
    _isSltGoodsidDic = sGidDic;
    [self setGoodsModel:gModel];
}

-(void)setGoodsModel:(GoodsModel *)goodsModel{
    _goodsModel = goodsModel;
    //    dispatch_async(dispatch_get_main_queue(), ^{
    
    __weak PDDGoodsCltCellV * selfCell = self;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image) {
            if (image.size.height<30&&image.size.width<30) {
                selfCell.goodsModel.smallPic = selfCell.goodsModel.goodsPic;
                [selfCell.goodsImgV sd_setImageWithURL:[NSURL URLWithString:selfCell.goodsModel.goodsPic] placeholderImage:PlaceholderImg];
            }
        }else{
            selfCell.goodsModel.smallPic = selfCell.goodsModel.goodsPic;
            [selfCell.goodsImgV sd_setImageWithURL:[NSURL URLWithString:selfCell.goodsModel.goodsPic] placeholderImage:PlaceholderImg];
        }
    }];
    
    _salesLab.text = _goodsModel.strSales;
    _oldPriceLab.text = _goodsModel.strPrice;
    _goodsNameLab.text = _goodsModel.goodsName;
    _endPriceLab.text = _goodsModel.strEndPrice;
    _commissionLab.text = _goodsModel.strCommission;
    
    // 商品来源
    if (_goodsModel.platform) {
        _platformImgV.hidden = NO;
        _platformImgV.image = [UIImage imageNamed:_goodsModel.strPlatform];
    }else{
        _platformImgV.hidden = YES;
    }
    
    if ([_goodsModel.couponMoney floatValue]>0) {
        _vouchersBtn.hidden = NO;
        _oldPriceLab.hidden = NO;
        // 判断优惠券
        if (_goodsModel.attributedCoupon) {
            [_vouchersBtn setAttributedTitle:_goodsModel.attributedCoupon forState:(UIControlStateNormal)];
        }else{ [_vouchersBtn setTitle:_goodsModel.strCouponMoney forState:(UIControlStateNormal)]; }
        
        // 修改优惠券的大小
        [_vouchersBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            if (_platformImgV.hidden) { make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB); }
            else{  make.left.equalTo(_platformImgV.mas_right).offset(8*HWB); }
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(8*HWB);
            make.height.offset(15*HWB); make.width.offset(_goodsModel.couponMoneyWi);
        }];
        
        [_salesLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersBtn.mas_right).offset(8*HWB);
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(8*HWB);
            make.height.offset(15*HWB);
        }];
        
    }else{
        _oldPriceLab.hidden = YES;
        _vouchersBtn.hidden = YES;
        if (_goodsModel.platform) {
            [_salesLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_platformImgV.mas_right).offset(8*HWB);
                make.top.equalTo(_goodsNameLab.mas_bottom).offset(8*HWB);
                make.height.offset(15*HWB);
            }];
        }else{
            [_salesLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_goodsImgV.mas_right).offset(10*HWB);
                make.top.equalTo(_goodsNameLab.mas_bottom).offset(8*HWB);
                make.height.offset(15*HWB);
            }];
        }
    }
    
    if (_isStartSelect) { // 开始选择
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = COLORCLEAR;
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {
            
        } @finally { }
        
    }else{ _selectBtn.hidden = YES; }
    
    //    });
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        [self goodsImgV];
        [self selectBtn];
        [self goodsNameLab];
        [self platformImgV];
        [self vouchersBtn];
        [self salesLab];
        [self endPriceLab];
        [self oldPriceLab];
        [self shareBtn];
        if (CANOPENWechat==NO) { _shareBtn.hidden = YES; }
    }
    return self ;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill;
        _goodsImgV.clipsToBounds = YES;
        _goodsImgV.userInteractionEnabled = YES;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.top.offset(0);
            make.right.offset(0); make.height.equalTo(_goodsImgV.mas_width);
        }];
    }
    return _goodsImgV ;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        float wi = (ScreenW-5*HWB)/4-15*HWB;
        [_selectBtn setImageEdgeInsets:UIEdgeInsetsMake(-wi, -wi, wi, wi)];
        [_goodsImgV addSubview:_selectBtn];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

-(UILabel*)goodsNameLab{
    if (!_goodsNameLab) {
        _goodsNameLab = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        [self.contentView addSubview:_goodsNameLab];
        [_goodsNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.right.offset(-8*HWB);
            make.top.equalTo(_goodsImgV.mas_bottom).offset(5*HWB);
        }];
    }
    return _goodsNameLab ;
}

-(UIImageView*)platformImgV{
    if (!_platformImgV) {
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.layer.masksToBounds = YES;
        _platformImgV.layer.cornerRadius = 3*HWB ;
        //        _platformImgV.image = [UIImage imageNamed:@"source_tianmao"];
        [self.contentView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(8*HWB);
            make.height.offset(15*HWB);  make.width.offset(15*HWB);
        }];
    }
    return _platformImgV;
}

-(UIButton*)vouchersBtn{
    if (!_vouchersBtn) {
        _vouchersBtn=[UIButton titLe:@"券┊￥0" bgColor:COLORWHITE titColorN:COLORWHITE font:11*HWB];
        [_vouchersBtn setBackgroundImage:[UIImage imageNamed:@"goods_quan_bg"] forState:(UIControlStateNormal)];
        _vouchersBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        UIImage * oldImg = [UIImage imageNamed:@"goods_quan_bg"];
        UIImage * newImg = [oldImg stretchableImageWithLeftCapWidth:30 topCapHeight:22.5];
        [_vouchersBtn setBackgroundImage:newImg forState:UIControlStateNormal];
        [self.contentView addSubview:_vouchersBtn];
        [_vouchersBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(8*HWB);
            make.height.offset(15*HWB); make.width.offset(50*HWB);
            if (_platformImgV.hidden) { make.left.offset(10*HWB); }
            else{  make.left.equalTo(_platformImgV.mas_right).offset(8*HWB); }
        }];
    }
    return _vouchersBtn ;
}

-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"销量: 88" color:Black102 font:10*HWB];
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersBtn.mas_right).offset(6*HWB);
            make.top.equalTo(_goodsNameLab.mas_bottom).offset(8*HWB);
            make.height.offset(16*HWB);
        }];
    }
    return _salesLab ;
}

-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"￥88" color:LOGOCOLOR font:14*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-8*HWB);
            make.left.offset(10*HWB);
        }];
    }
    return _endPriceLab ;
}

-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"原价:￥88" color:Black153 font:10*HWB];
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);
            make.bottom.equalTo(_endPriceLab.mas_top).offset(-3*HWB);
        }];
        // 当成删除线使用
        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = Black153;
        [_oldPriceLab addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0); make.height.offset(1);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
        }];
    }
    return _oldPriceLab ;
}


-(UIButton*)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [[UIButton alloc]init];
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-2*HWB); make.bottom.offset(0);
            make.width.offset(60*HWB); make.height.offset(45*HWB);
        }];
        
        UIImageView * shareImgv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"list_share_img"]];
        [_shareBtn addSubview:shareImgv];
        [shareImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-25*HWB); make.centerX.equalTo(_shareBtn.mas_centerX);
            make.height.offset(19*HWB);  make.width.offset(19*HWB);
        }];
        
        _commissionLab = [UILabel labText:@"赚￥88" color:LOGOCOLOR font:11*HWB];
        _commissionLab.textAlignment = NSTextAlignmentCenter ;
        _commissionLab.adjustsFontSizeToFitWidth = YES ;
        [_shareBtn addSubview:_commissionLab];
        [_commissionLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(23*HWB);
            make.left.offset(0);
            make.right.offset(0);
        }];
        
        ADDTARGETBUTTON(_shareBtn, shareGoods)
    }
    return _shareBtn ;
}



#pragma mark ==>> 按钮相应事件
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = COLORCLEAR;
            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
        
    }
}


-(void)shareGoods{
    [PageJump shareGoods:_goodsModel goodsImgAry:nil];
}

@end






