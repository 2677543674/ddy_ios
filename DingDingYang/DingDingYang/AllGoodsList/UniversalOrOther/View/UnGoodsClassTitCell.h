//
//  UnGoodsClassTitCell.h
//  DingDingYang
//
//  Created by ddy on 2018/12/18.
//  Copyright © 2018 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WPHSortingView.h"
#import "PDDSortingView.h"
#import "JDSortingView.h"
#import "DMSortingView.h"


NS_ASSUME_NONNULL_BEGIN


@interface UnGoodsClassTitCell : UICollectionViewCell
@property(nonatomic,strong) UILabel * titleLab;
@end



#pragma mark === 商品列表(带有分类的分类小列表)

@class UnGoodsClassListCell;
@protocol UnClassListCltCellDelegate <NSObject>
// datadic: 已请求过的数据和当前使用的参数
-(void)nowCell:(UnGoodsClassListCell*)ctcell nowClassTypeAllData:(NSDictionary*)datadic;
// 记录偏移量
-(void)nowCell:(UnGoodsClassListCell*)ctcell offsetY:(NSString*)floaty;
// 返回已选商品或删除商品
-(void)nowCell:(UnGoodsClassListCell*)ctcell selectGoodsModel:(GoodsModel*)model isSelect:(BOOL)select;
// 将要开始拖动
-(void)beginDragNowCell:(UnGoodsClassListCell *)ctcell;

@end

// 商品列表
@interface UnGoodsClassListCell : UICollectionViewCell<UICollectionViewDelegate,UICollectionViewDataSource,SortingSelectDelegatePDD,SortingSelectDelegateWPH,SortingSelectDelegateJD,SortingSelectDelegateDM>

@property(nonatomic,strong) UICollectionViewFlowLayout * layout;
@property(nonatomic,strong) UICollectionView * classCltView;
@property(nonatomic,copy)   NSString * defultName;

@property(nonatomic,assign) id<UnClassListCltCellDelegate>delegate;

-(void)addParIsStartSlt:(BOOL)isStartSlt diSltGDic:(NSMutableDictionary*)diSltGDic lastStopY:(NSString*)lastStopY;

// 1:淘宝 2:京东 3:拼多多 4:唯品会 5:多麦
@property(nonatomic,assign) NSInteger goodsType; // 商品列表类型

@property(nonatomic,copy) NSString * act; // 如果为1，用浏览器打开

// 记录数据
@property(nonatomic,assign) BOOL isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * didSltGoodsidDic; //已选的商品id字典

@property(nonatomic,copy)   NSDictionary * allDic ; // 请求参数和商品列表组成的数据
@property(nonatomic,strong) NSMutableDictionary * parameter ; // 请求参数
@property(nonatomic,strong) NSMutableArray * bannerListAry; // banner图数组
@property(nonatomic,strong) NSMutableArray * classListAry ; // 商品列表
@property(nonatomic,assign) NSInteger  pageNum; // 页面
@property(nonatomic,copy) NSString * selectType; // 选中的排序类型
@property(nonatomic,copy) NSString * lastStopY; // 记录滑动停止位置(刷新时定位用)
@property(nonatomic,assign) float scrollStopY; // 记录滑动停止位置(判断是在上拉还是下拉使用)

@end

NS_ASSUME_NONNULL_END
