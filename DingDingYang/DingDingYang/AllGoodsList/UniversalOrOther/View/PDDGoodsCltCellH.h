//
//  PDDGoodsCltCellH.h
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDDGoodsCltCellH : UICollectionViewCell

@property(nonatomic,strong) UIImageView * goodsImgV;     // 商品图片
@property(nonatomic,strong) UILabel     * goodsNameLab;  // 商品标题
@property(nonatomic,strong) UIImageView * platformImgV;  // 商品来源图片
@property(nonatomic,strong) UIButton    * vouchersBtn;   // 券
@property(nonatomic,strong) UILabel     * salesLab;      // 销量
@property(nonatomic,strong) UILabel     * oldPriceLab;   // 原价
@property(nonatomic,strong) UILabel     * endPriceLab;   // 券后价

@property(nonatomic,strong) UIButton    * shareBtn;      // 分享按钮
@property(nonatomic,strong) UILabel     * commissionLab; // 赚的佣金
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮

@property(nonatomic,strong) GoodsModel  * goodsModel ;

//*** 商品多选有关 ***//
-(void)isStartS:(BOOL)isStartS sgidDic:(NSMutableDictionary*)sGidDic gModel:(GoodsModel*)gModel;
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end




#pragma mark ===>>> 第九种样式 《方块、百年中的拼多多样式》

@interface PDDGoodsCltCellV : UICollectionViewCell

@property(nonatomic,strong) UIImageView * goodsImgV;     // 商品图片
@property(nonatomic,strong) UILabel     * goodsNameLab;  // 商品标题
@property(nonatomic,strong) UIImageView * platformImgV;  // 商品来源图片
@property(nonatomic,strong) UIButton    * vouchersBtn;   // 券
@property(nonatomic,strong) UILabel     * salesLab;      // 销量
@property(nonatomic,strong) UILabel     * oldPriceLab;   // 原价
@property(nonatomic,strong) UILabel     * endPriceLab;   // 券后价

@property(nonatomic,strong) UIButton    * shareBtn;      // 分享按钮
@property(nonatomic,strong) UILabel     * commissionLab; // 赚的佣金

@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮

@property(nonatomic,strong) GoodsModel  * goodsModel ;

//*** 商品多选有关 ***//
-(void)isStartS:(BOOL)isStartS sgidDic:(NSMutableDictionary*)sGidDic gModel:(GoodsModel*)gModel;
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end
