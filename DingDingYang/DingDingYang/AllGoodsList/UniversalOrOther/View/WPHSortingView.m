//
//  WPHSortingView.m
//  DingDingYang
//
//  Created by ddy on 2018/7/17.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "WPHSortingView.h"

@implementation WPHSortingView

#pragma mark ===>>> 加号方法创建条件选择项
+(void)creatIn:(UIView *)supView frame:(CGRect)rect selectType:(SelectType)selectBlock{
    WPHSortingView * sortView = [[WPHSortingView alloc]init];
    sortView.frame = rect;
    sortView.selectBlock = selectBlock ;
    [supView addSubview:sortView];
}

#pragma mark ===>>> 通过属性值修改选中样式
-(void)setStyleType:(NSString *)styleType{
    
    switch ([styleType integerValue]) {
        case 0:{ [self isSelectZongHe]; } break; // 综合
        case 1:{ [self isSelectSales]; } break; // 销量
        case 2:{ [self isSelectCmRatio:YES]; } break; // 佣金比例 (从小到大排序)
        case 3:{ [self isSelectCmRatio:NO];  } break; // 佣金比例 (从大到小排序)
        case 4:{ [self isSelectEndPrice:YES];} break; // 券后价升序 (从小到大排序)
        case 5:{ [self isSelectEndPrice:NO]; } break; // 券后价降序 (从大到小排序)
        default:  break;
    }

}


-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = COLORWHITE;

        
        _zongHeBtn = [UIButton titLe:@"综合" bgColor:COLORCLEAR titColorN:LOGOCOLOR font:13*HWB];
        _zongHeBtn.selected = YES ;
        [self addSubview:_zongHeBtn];
        [_zongHeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.top.offset(0);
            make.bottom.offset(0); make.width.offset(ScreenW/4);
        }];
        
        _endPriceBtn = [UIButton image:@"down_up" titLe:@"价格" bgColor:COLORCLEAR titColorN:Black51 font:12.5*HWB];
        _endPriceBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -15*HWB, 0, 0);
        _endPriceBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 5,-60*HWB);
        [self addSubview:_endPriceBtn];
        [_endPriceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_zongHeBtn.mas_right);
            make.top.offset(0); make.bottom.offset(0);
            make.width.offset(ScreenW/4);
        }];
        
        _salesBtn = [UIButton image:@"down_N1" titLe:@"销量" bgColor:COLORCLEAR titColorN:Black51 font:12.5*HWB];
        _salesBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -15*HWB, 0, 0);
        _salesBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0,-60*HWB);
        [self addSubview:_salesBtn];
        [_salesBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceBtn.mas_right);
            make.top.offset(0); make.bottom.offset(0);
            make.width.offset(ScreenW/4);
        }];
        
        _cmRatioBtn = [UIButton image:@"down_up" titLe:@"佣金" bgColor:COLORCLEAR titColorN:Black51 font:12.5*HWB];
        _cmRatioBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -15*HWB, 0, 0);
        _cmRatioBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0,-60*HWB);
        [self addSubview:_cmRatioBtn];
        [_cmRatioBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_salesBtn.mas_right);
            make.top.offset(0); make.bottom.offset(0);
            make.width.offset(ScreenW/4);
        }];
        
        
        ADDTARGETBUTTON(_zongHeBtn, clickZongHe)
        ADDTARGETBUTTON(_endPriceBtn, clickEndPrice)
        ADDTARGETBUTTON(_salesBtn, clickSales)
        ADDTARGETBUTTON(_cmRatioBtn, clickCmRatio)
        
        
        UIImageView * bottomLineImg = [[UIImageView alloc]init];
        bottomLineImg.backgroundColor = RGBA(235.f,235.f,235.f,1);
        [self addSubview:bottomLineImg];
        [bottomLineImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.offset(0);
            make.height.offset(0.5);
        }];
        
    }
    return self ;
}

#pragma mark ===>>> 按钮点击事件

#pragma mark => 综合
// 点击综合
-(void)clickZongHe{
    if (_zongHeBtn.selected==NO) {
        [self isSelectZongHe];
        _selectBlock(@"0");
    }
    [self userEnabledNO];
}
// 选中的是综合
-(void)isSelectZongHe{
    [self changeZongHeBtnSelect:YES];
    [self changeSalesBtnSelect:NO];
    [self changeCmRatioBtnSelect:2];
    [self changeEndPriceBtnSelect:2];
}

#pragma mark => 销量
// 点击销量
-(void)clickSales{
    if (_salesBtn.selected==NO) {
        [self isSelectSales];
        _selectBlock(@"1");
    }
    [self userEnabledNO];
}
// 选中的是销量
-(void)isSelectSales{
    [self changeSalesBtnSelect:YES];
    [self changeZongHeBtnSelect:NO];
    [self changeCmRatioBtnSelect:2];
    [self changeEndPriceBtnSelect:2];
}

#pragma mark => 券后价
// 点击券后价
-(void)clickEndPrice{
    if (_endPriceBtn.selected==NO) {
        [self isSelectEndPrice:YES];
        _selectBlock(@"4");
    }else{
        [self isSelectEndPrice:NO];
        _selectBlock(@"5");
    }
    [self userEnabledNO];
}
// 选中的是券后价
-(void)isSelectEndPrice:(NSInteger)yn{
    [self changeSalesBtnSelect:NO];
    [self changeZongHeBtnSelect:NO];
    [self changeCmRatioBtnSelect:2];
    [self changeEndPriceBtnSelect:yn];
}

#pragma mark => 佣金比例
// 点击佣金比例
-(void)clickCmRatio{
    if (_cmRatioBtn.selected==NO) {
        [self isSelectCmRatio:YES];
        _selectBlock(@"2");
    }else{
        [self isSelectCmRatio:NO];
        _selectBlock(@"3");
    }
    [self userEnabledNO];
}
// 选中的是佣金比例
-(void)isSelectCmRatio:(BOOL)yn{
    [self changeSalesBtnSelect:NO];
    [self changeZongHeBtnSelect:NO];
    [self changeEndPriceBtnSelect:2];
    [self changeCmRatioBtnSelect:yn];
}

#pragma mark ===>>> 修改按钮样式
// 改变综合按钮的状态
-(void)changeZongHeBtnSelect:(BOOL)yn{
    _zongHeBtn.selected = yn;
    if (yn) { // 选中
        _zongHeBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
        [_zongHeBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
    }else{ // 未选中
        _zongHeBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*HWB];
        [_zongHeBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
    }
}

// 改变价格按钮的状态
-(void)changeEndPriceBtnSelect:(NSInteger)state{
    switch (state) {
        case 0: { // 从大到小排序
            _endPriceBtn.selected = NO ;
            [_endPriceBtn setImage:[UIImage imageNamed:@"down_H2"] forState:(UIControlStateNormal)];
            _endPriceBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_endPriceBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 1: { // 从小到大排序
            _endPriceBtn.selected = YES ;
            [_endPriceBtn setImage:[UIImage imageNamed:@"up_H2"] forState:(UIControlStateNormal)];
            _endPriceBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_endPriceBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 2: { // 未选中状态
            _endPriceBtn.selected = NO;
            _endPriceBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*HWB];
            [_endPriceBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
            [_endPriceBtn setImage:[UIImage imageNamed:@"down_up"] forState:(UIControlStateNormal)];
        } break;
            
        default: break;
    }
}

// 改变销量按钮状态
-(void)changeSalesBtnSelect:(BOOL)yn{
    _salesBtn.selected = yn;
    if (yn) { // 选中
        _salesBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
        [_salesBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        [_salesBtn setImage:[UIImage imageNamed:@"down_H1"] forState:(UIControlStateNormal)];
    }else{ // 未选中
        _salesBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*HWB];
        [_salesBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        [_salesBtn setImage:[UIImage imageNamed:@"down_N1"] forState:(UIControlStateNormal)];
    }
}

// 改变佣金比例按钮状态
-(void)changeCmRatioBtnSelect:(NSInteger)state{
    switch (state) {
        case 0: { // 从大到小排序
            _cmRatioBtn.selected = NO ;
            [_cmRatioBtn setImage:[UIImage imageNamed:@"down_H2"] forState:(UIControlStateNormal)];
            _cmRatioBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_cmRatioBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 1: { // 从小到大排序
            _cmRatioBtn.selected = YES ;
            [_cmRatioBtn setImage:[UIImage imageNamed:@"up_H2"] forState:(UIControlStateNormal)];
            _cmRatioBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
            [_cmRatioBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        } break;
            
        case 2: { // 未选中状态
            _cmRatioBtn.selected = NO;
            _cmRatioBtn.titleLabel.font = [UIFont systemFontOfSize:12.5*HWB];
            [_cmRatioBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
            [_cmRatioBtn setImage:[UIImage imageNamed:@"down_up"] forState:(UIControlStateNormal)];
        } break;
            
        default: break;
    }
}


#pragma mark ==>> 点击完成之后，禁止快速点击
-(void)userEnabledNO{
    _zongHeBtn.enabled = NO;
    _salesBtn.enabled = NO;
    _endPriceBtn.enabled = NO;
    _cmRatioBtn.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (_zongHeBtn&&_salesBtn&&_endPriceBtn&&_cmRatioBtn) {
            _zongHeBtn.enabled = YES;
            _salesBtn.enabled = YES;
            _endPriceBtn.enabled = YES;
            _cmRatioBtn.enabled = YES;
        }
    });
}

@end



#pragma mark ===>>> 唯品会的排序区头


@implementation WPHCltSortingHead

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _wphScreenView = [[WPHSortingView alloc]init];
        _wphScreenView.backgroundColor = COLORWHITE ;
        [self addSubview:_wphScreenView];
        [_wphScreenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(-1);
            make.left.offset(0);  make.right.offset(0);
        }];
        __weak WPHCltSortingHead * selfView = self ;
        _wphScreenView.selectBlock = ^(NSString *stype) {
            [selfView.delegate wphSelectGoodsScreenSortingType:stype];
        };
        
        
    }
    return self;
}

@end
