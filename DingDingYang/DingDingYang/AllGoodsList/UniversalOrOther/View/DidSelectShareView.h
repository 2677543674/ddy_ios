//
//  DidSelectShareView.h
//  DingDingYang
//
//  Created by ddy on 2018/4/11.
//  已选商品点击查看，或分享

#import <UIKit/UIKit.h>

@class DidShareBtn;


#pragma mark ===>>> 批量选择分享的侧边按钮
@interface DidSelectShareView : UIView

@property(nonatomic,strong) UIButton * checkBtn;    // 查看按钮
@property(nonatomic,strong) UILabel  * redPointLab; // 红点按钮
@property(nonatomic,strong) UIView   * backView;    // 分享按钮背景图
@property(nonatomic,strong) DidShareBtn * wechatBtn1;  // 微信
@property(nonatomic,strong) DidShareBtn * wechatBtn2;  // 朋友圈
@property(nonatomic,strong) UILabel     * shareLab;    // 分享

@property(nonatomic,assign) NSInteger allCount; // 记录已选商品总数

// 0:隐藏已选列表 1:显示已选列表 2:分享到微信 3:分享到朋友圈
typedef void(^SelectBtnTagBlock)(NSInteger type);
@property(copy,nonatomic) SelectBtnTagBlock  touchUpType;

-(void)showSelfAndAnimation; 
-(void)hiddenSelfAndAnimation;
    
@end


#pragma mark ===>>> 展示已选商品
@interface DidSelectGoodsCltView : UIView <UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UILabel * titleLab;
@property(nonatomic,strong) UICollectionView * cltView;
@property(nonatomic,strong) NSMutableArray * dataAry;


typedef void(^RemoveIsSelectGoodsBlock)(GoodsModel * sgModel ,NSInteger selectTag);
@property(copy,nonatomic) RemoveIsSelectGoodsBlock  removeSelectGoodsBlock;

-(void)showSelfAndAnimation;
-(void)hiddenSelfAndAnimation;

@end

// 已选商品显示的cell
@interface DidSltCtCell : UICollectionViewCell
@property(nonatomic,strong) UIImageView * goodsImgV; 
@property(nonatomic,strong) UIButton * removeBtn;
@property(nonatomic,strong) GoodsModel * goodsModel;
@end


#pragma mark ===>>> 分享按钮
@interface DidShareBtn : UIButton
@property(nonatomic,strong) UIImageView * imageV;
@property(nonatomic,strong) UILabel * titLab;
-(instancetype)initWithImgName:(NSString*)img titleName:(NSString*)title;
@end







