//
//  WPHGoodsCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/9/6.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "WPHGoodsCltCell.h"
#import "CreateShareImageView6.h"
#import "WqeView.h"

@implementation WPHGoodsCltCell


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    NSString * sourceName = @"";
    
    _salesLab.hidden = YES;
    
    switch (_goodsSource) {
        case 2:{
             _salesLab.hidden = NO;
            sourceName = @"专享价";
            _fromIma.image = [UIImage imageNamed:@"source_jingdong"];
        } break;
            
        case 3:{
             sourceName = @"拼多多";
            _fromIma.image = [UIImage imageNamed:@"pddicon"];
        } break;
            
        case 4:{
            sourceName = @"唯品会";
            _fromIma.image = [UIImage imageNamed:@"wphIcon"];
        } break;
            
        case 5:{
            sourceName = @"综合平台";
            NSURL * fromUrl = [NSURL URLWithString:_goodsModel.gfromUrl];
            [_fromIma sd_setImageWithURL:fromUrl placeholderImage:PlaceholderImg];
            [_fromIma mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.offset(40*HWB);
            }];
        } break;
        default: break;
    }
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    _goodsTitle.text = goodsModel.goodsName ;
    _endPriceLab.text = FORMATSTR(@"￥%@",goodsModel.endPrice) ;
    _salesLab.text = FORMATSTR(@"销量: %@",goodsModel.sales);
    _priceLab.text = FORMATSTR(@"市场价: ￥%@",goodsModel.price) ;
    _endPriceLab.text = FORMATSTR(@"  %@: ￥%@  ",sourceName,goodsModel.endPrice);

    if (role_state!=0) {
        [_shareBtn setTitle:FORMATSTR(@"  分享赚: ￥%@  ",goodsModel.commission) forState:(UIControlStateNormal)];
    }else{
        [_shareBtn setTitle:@"   去分享   " forState:(UIControlStateNormal)];
    }
    
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self goodsImgV];
        [self goodsTitle];
        
        [self endPriceLab];
        [self fromIma];
        [self salesLab];
        [self priceLab];
        [self shareBtn];
    }
    return self;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.contentMode=UIViewContentModeScaleAspectFill;
        _goodsImgV.clipsToBounds=YES;
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.top.offset(12*HWB);
            make.height.width.offset(93*HWB);
        }];
        
    }
    return _goodsImgV;
}



-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _goodsTitle.numberOfLines=2;
        [self.contentView addSubview:_goodsTitle];
        [_goodsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10);
            make.top.equalTo(_goodsImgV.mas_top).offset(5*HWB);
            //            make.height.offset(35*HWB);
        }];
    }
    return _goodsTitle ;
}

-(UIImageView*)fromIma{
    if (!_fromIma) {
        _fromIma = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wphIcon"]];
        _fromIma.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_fromIma];
        [_fromIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(_endPriceLab.mas_top).offset(-8*HWB);
            make.height.offset(14*HWB);
            make.width.offset(14*HWB);
        }];
    }
    return _goodsImgV;
}


-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"销量：0" color:Black102 font:10*HWB];
//        _salesLab.hidden=YES;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
            //            make.width.offset(100);
        }];
    }
    return _salesLab ;
}

-(UILabel *)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥8888.88" color:[UIColor grayColor] font:10*HWB];
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_fromIma.mas_right).offset(5*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
        }];
    }
    return _priceLab ;
}


-(UILabel *)endPriceLab{
    if (!_endPriceLab) {
        
        _endPriceLab = [UILabel labText:@"￥8888.88" color:RGBA(246, 73, 72, 1) font:11.5*HWB];
        _endPriceLab.backgroundColor=RGBA(249, 229, 228, 1);
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(_goodsImgV.mas_bottom).offset(-5*HWB);
            make.height.offset(22*HWB);
        }];
    }
    return _endPriceLab ;
}


-(UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn=[UIButton new];
        _shareBtn.titleLabel.font=[UIFont systemFontOfSize:10.5*HWB];
        _shareBtn.layer.cornerRadius=3;
        _shareBtn.clipsToBounds=YES;
        _shareBtn.backgroundColor=RGBA(246, 73, 72, 1);
        [_shareBtn addTarget:self action:@selector(clickShareGoods) forControlEvents:(UIControlEventTouchUpInside)];
        [_shareBtn setTitleColor:COLORWHITE forState:(UIControlStateNormal)];
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.bottom.equalTo(_endPriceLab.mas_bottom);
            make.height.offset(22*HWB);
        }];
    }
    return _shareBtn;
}

-(void)clickShareGoods{
    
    if (_goodsSource==2) { // 京东
        [PageJump shareGoods:_goodsModel goodsImgAry:(NSMutableArray*)@[_goodsModel.goodsPic]];
//        [MoreWay showSpView:[UIApplication sharedApplication].keyWindow height:80+NAVCBAR_HEIGHT-64 isJD:YES andSelect:^(NSInteger way) {
//            MBShow(@"正在加载...")
//            [NetRequest requestType:0 url:url_goods_share_jd ptdic:@{@"goodsId":_goodsModel.goodsId} success:^(id nwdic) {
//                MBHideHUD
//                NSString * shareText = REPLACENULL(nwdic[@"shareContent"]);
//                if (shareText.length==0) { shareText = SHARECONTENT; }
//                [ClipboardMonitoring pasteboard:REPLACENULL(shareText)];
////                [UIPasteboard generalPasteboard].string = REPLACENULL(shareText);
//                SHOWMSG(@"分享的文本内容已复制，请手动粘贴~")
//
//                CustomShareModel * model = [[CustomShareModel alloc]init];
//                model.shareType = 2; model.currentVC = TopNavc.topViewController;
//                model.imgDataOrAry = _goodsImgV.image;
//                model.sharePlatform = way;
//                [CustomUMShare shareByCustomModel:model result:^(NSInteger result) { }];
//
//            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
//                MBHideHUD  SHOWMSG(failure)
//            }];
//        }];
        
    }else{ // 唯品会、多麦
        if (_goodsImgV.image==nil) {
            SHOWMSG(@"图片下载未完成")
        }else{
            CreateShareImageView6 * view = [[CreateShareImageView6 alloc]initWithFrame:CGRectMake(-2000, -2000, ScreenW, ScreenW*1.42)];
            view.backgroundColor = COLORWHITE;
            [self.contentView addSubview:view];
            view.goodsImage = _goodsImgV.image;
            view.model = _goodsModel;
            UIImage * shareImage = [self convertViewToImage:view];
            [view removeFromSuperview];
            
            if (TARGET_IPHONE_SIMULATOR) { // 模拟器(显示预览)
                [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[shareImage]];
            }else{
                [SharePopoverVC shareWPHGoodsModel:_goodsModel thubImg:shareImage shareType:3];
            }
        }
    }
    
}
















-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end
