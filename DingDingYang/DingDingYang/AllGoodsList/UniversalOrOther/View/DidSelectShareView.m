//
//  DidSelectShareView.m
//  DingDingYang
//
//  Created by ddy on 2018/4/11.
//


#define didst_bg_color RGBA(0, 0, 0, 0.6)

#import "DidSelectShareView.h"

@implementation DidSelectShareView

-(void)setAllCount:(NSInteger)allCount{
    _allCount = allCount;
    _redPointLab.text = FORMATSTR(@"%ld",(long)_allCount);
    [UIView animateWithDuration:0.26 delay:0 usingSpringWithDamping:0.26 initialSpringVelocity:5.f options:UIViewAnimationOptionCurveEaseOut animations:^{
        _redPointLab.transform = CGAffineTransformMakeScale(1.8, 1.8);
    } completion:^(BOOL finished) {
        _redPointLab.transform = CGAffineTransformMakeScale(1, 1);
    }];
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.hidden = YES;
        
        _checkBtn = [UIButton bgImage:@"bgimg"];
        _checkBtn.backgroundColor = didst_bg_color;
        [self addSubview:_checkBtn];
        [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(2); make.right.offset(-2); make.bottom.offset(0);
            make.height.equalTo(_checkBtn.mas_width);
        }];
        
    
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = didst_bg_color ;
        _backView.clipsToBounds = YES;
        [_backView cornerRadius:2];
        [self addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.bottom.offset(-46*HWB);
            make.height.offset(0.1);
        }];
        
        _wechatBtn1 = [[DidShareBtn alloc]initWithImgName:@"new1_share_wx" titleName:@"微信"];
        [_backView addSubview:_wechatBtn1];
        [_wechatBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView.mas_centerY).offset(10*HWB);
            make.centerX.equalTo(_backView.mas_centerX);
            make.width.offset(40*HWB);
            make.height.offset(47*HWB);
        }];
        
        _wechatBtn2 = [[DidShareBtn alloc]initWithImgName:@"new1_share_pyq" titleName:@"朋友圈"];
        [_backView addSubview:_wechatBtn2];
        [_wechatBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_backView.mas_centerY);
            make.centerX.equalTo(_backView.mas_centerX);
            make.width.offset(40*HWB);
            make.height.offset(47*HWB);
        }];
        
        _shareLab = [UILabel labText:@"- 分享 -" color:COLORWHITE font:10*HWB];
        [_backView addSubview:_shareLab];
        [_shareLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(2); make.centerX.equalTo(_backView.mas_centerX);
        }];
        
        // 红点显示最顶层(需要动画效果)
        _redPointLab = [UILabel labText:@"0" color:COLORWHITE font:14*HWB];
        _redPointLab.textAlignment = NSTextAlignmentCenter;
        _redPointLab.backgroundColor = [UIColor redColor];
        [_redPointLab cornerRadius:8*HWB];
        [self addSubview:_redPointLab];
        [_redPointLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.width.height.offset(16*HWB);
            make.top.equalTo(_checkBtn.mas_top).offset(-3*HWB);
        }];
        
        _checkBtn.alpha = 0;
        _backView.alpha = 0;
        _redPointLab.alpha = 0;
        
        ADDTARGETBUTTON(_checkBtn, clickCheck)
        ADDTARGETBUTTON(_wechatBtn1, clickWechat1)
        ADDTARGETBUTTON(_wechatBtn2, clickWechat2)

    }
    return self;
}

-(void)clickCheck{
    if (_checkBtn.selected==YES) {
        _checkBtn.selected = NO;
        self.touchUpType(0);
    }else{
        _checkBtn.selected = YES;
        self.touchUpType(1);
    }
}

-(void)clickWechat1{ self.touchUpType(2); }
-(void)clickWechat2{ self.touchUpType(3); }


// 显示动画
-(void)showSelfAndAnimation{
    self.hidden = NO;
    [_checkBtn corner:(self.frame.size.width-4)/2];
    [UIView animateWithDuration:0.26 animations:^{
        
        _checkBtn.alpha = 1 ;
        _redPointLab.alpha = 1;
        _backView.alpha = 1;
        
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.bottom.offset(-46*HWB);
        }];
        
        [_backView.superview layoutIfNeeded];
        
    } completion:^(BOOL finished) { }];
    
}

// 隐藏动画
-(void)hiddenSelfAndAnimation{
    
    [UIView animateWithDuration:0.26 animations:^{
        _backView.alpha = 0;
        _checkBtn.alpha = 0 ;
        _redPointLab.alpha = 0;
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.bottom.offset(-46*HWB);
            make.height.offset(0.1);
        }];
        [_backView.superview layoutIfNeeded];
     
    } completion:^(BOOL finished) { self.hidden = YES; }];

}

@end



#pragma mark ===>>> 展示已选商品

@implementation DidSelectGoodsCltView

-(void)setDataAry:(NSMutableArray *)dataAry{
    _dataAry = dataAry;
    [_cltView reloadData];
}

-(instancetype)init{
    self = [super init];
    if (self) {
    
        self.backgroundColor = COLORWHITE ;
        self.layer.shadowColor = COLORBLACK.CGColor;
        self.layer.shadowOpacity = 0.2f;
        self.layer.shadowOffset = CGSizeMake(0,-3);
        self.layer.cornerRadius = 3;

        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = Black204;
        linesImgV.frame = CGRectMake(50*HWB, 22*HWB, ScreenW-100*HWB, 1);
        [self addSubview:linesImgV];
        
        UILabel * tishiLab = [UILabel labText:@"  已选择  " color:Black102 font:13*HWB];
        tishiLab.backgroundColor = COLORWHITE;
        [self addSubview:tishiLab];
        [tishiLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(linesImgV.mas_centerX);
            make.centerY.equalTo(linesImgV.mas_centerY);
        }];
    
        [self cltView];
        
    }
    return self;
}
-(UICollectionView*)cltView{
    if (!_cltView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        layout.itemSize = CGSizeMake(90*HWB,90*HWB) ;
        layout.minimumInteritemSpacing = 5*HWB ;
        layout.sectionInset = UIEdgeInsetsMake(0, 15*HWB, 0, 15*HWB);
        _cltView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 45*HWB, ScreenW, 90*HWB) collectionViewLayout:layout];
        _cltView.delegate = self ; _cltView.dataSource = self ;
        _cltView.showsHorizontalScrollIndicator = NO ;
        _cltView.backgroundColor = COLORWHITE ;
        [self addSubview:_cltView];
//        [_cltView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.offset(0); make.right.offset(0);
//            make.top.offset(30*HWB);  make.bottom.offset(0);
//        }];
        
        [_cltView registerClass:[DidSltCtCell class] forCellWithReuseIdentifier:@"slt_cltCell"];
        
    }
    return _cltView ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DidSltCtCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"slt_cltCell" forIndexPath:indexPath];
    cltCell.goodsModel = _dataAry[indexPath.row];
    cltCell.removeBtn.tag = indexPath.item;
    ADDTARGETBUTTON(cltCell.removeBtn,remove:)
    return cltCell ;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushToGoodsDetail:_dataAry[indexPath.item] pushType:0];
}

// 删除了某个已选的商品
-(void)remove:(UIButton*)btn{

    @try {
        GoodsModel * model = _dataAry[btn.tag];
        [_dataAry removeObjectAtIndex:btn.tag];
        self.removeSelectGoodsBlock(model, btn.tag);
    } @catch (NSException *exception) {
         SHOWMSG(@"删除失败，请再试一次!")
    } @finally {
        [_cltView reloadData];
    }
   
}


-(void)showSelfAndAnimation{
    [UIView animateWithDuration:0.26 animations:^{
        self.transform = CGAffineTransformMakeTranslation(0,-160*HWB);
    }];
}

-(void)hiddenSelfAndAnimation{
    [UIView animateWithDuration:0.26 animations:^{
         self.transform = CGAffineTransformIdentity;
    }];
}


@end

// 已选商品显示的cell
@implementation DidSltCtCell

-(void)setGoodsModel:(GoodsModel *)goodsModel{
    _goodsModel = goodsModel;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] placeholderImage:[UIImage imageNamed:@"logo"]];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _goodsImgV = [[UIImageView alloc]init];
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(8*HWB);  make.right.offset(-8*HWB);
            make.bottom.left.offset(0);
        }];
        
        _removeBtn = [UIButton bgImage:@"gdfdelete"];
        [self.contentView addSubview:_removeBtn];
        [_removeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.offset(0);
            make.height.width.offset(16*HWB);
        }];
    
    }
    return self;
}
@end



#pragma mark ===>>> 分享按钮

@implementation DidShareBtn

-(instancetype)initWithImgName:(NSString*)img titleName:(NSString*)title{
    self = [super init];
    if (self) {
        
        _imageV = [[UIImageView alloc]init];
        _imageV.image=[UIImage imageNamed:img];
        _imageV.backgroundColor = COLORWHITE;
        [_imageV cornerRadius:16*HWB];
        [self addSubview:_imageV];
        _imageV.contentMode = UIViewContentModeScaleAspectFill ;
        [_imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.centerX.equalTo(self.mas_centerX);
            make.width.height.offset(32*HWB);
        }];
        
        _titLab = [[UILabel alloc]init];
        _titLab.font=[UIFont systemFontOfSize:10.0*HWB];
        _titLab.textAlignment = NSTextAlignmentCenter;
        _titLab.textColor = COLORWHITE;
        _titLab.text = title;
        [self addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);
            make.centerX.equalTo(self.mas_centerX);
        }];
    }  return self ;
    
}

@end




