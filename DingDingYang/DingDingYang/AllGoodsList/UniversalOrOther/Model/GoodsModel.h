//
//  GoodsModel.h
//  DingDingYang
//
//  Created by ddy on 2017/3/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShopModel.h"
//商品信息model
@interface GoodsModel : NSObject
@property(nonatomic,assign) NSInteger ifGet ;        // 0：否，1：是，2：即将(淘宝抢购商品)
@property(nonatomic,copy) NSString * goodsPic ;      // 商品图片URL地址
@property(nonatomic,copy) NSString * goodsName ;     // 商品名称
@property(nonatomic,copy) NSString * price ;         // 商品价格(天猫)
@property(nonatomic,copy) NSString * activityId ;    // 活动id
@property(nonatomic,copy) NSString * catId ;         // 商品分类ID
@property(nonatomic,copy) NSString * commission ;    // 所赚的佣金
@property(nonatomic,copy) NSString * couponMoney ;   // 优惠券金额
@property(nonatomic,copy) NSString * endPrice ;      // 商品券后价格
@property(nonatomic,copy) NSString * goodsId ;       // 商品ID
@property(nonatomic,copy) NSString * sales ;         // 总销量
@property(nonatomic,copy) NSString * smallPic ;      // 商品图片小图
@property(nonatomic,copy) NSString * couponSurplus ; // 优惠券剩余数量
@property(nonatomic,copy) NSString * endDays ;       // 剩余天数
@property(nonatomic,copy) NSString * video ;         // 视频链接
@property(nonatomic,copy) NSString * itemUrl ;       // 查券页面,百川的商品有此参数,淘宝或天猫商品详情页链接
@property(nonatomic,copy) NSString * desStr ;        // 商品推荐语 ( goods/detail.api接口获取 )
@property(nonatomic,copy) NSString * platform ;      // 商品平台类型(天猫、淘宝)
@property(nonatomic,copy) NSString * url;            // 商品海报链接
@property(nonatomic,copy) NSString * keyWord;        // 首页头条商品关键字
@property(nonatomic,strong) ShopModel * shopModel;   // 商铺信息
@property(nonatomic,assign) BOOL isSelect;           // 是否选中
/** 官方推荐中记录选中的状态 **/
@property(nonatomic,copy) NSString * ormdSelect ;    // 官方推荐选中

#pragma mark ===>>> 在列表显示，需要拼接文字的(直接拼接好,自己加的参数名) 百年老农
@property(nonatomic,copy) NSString * strSales; // @"销量:88"
@property(nonatomic,copy) NSString * strPrice; // @"原价￥88"
@property(nonatomic,copy) NSString * strEndPrice;    // @"￥88"
@property(nonatomic,copy) NSString * strPlatform;    // 商品来源
@property(nonatomic,copy) NSString * strCommission;  // @"赚￥88"
@property(nonatomic,copy) NSString * strCouponMoney; // @"券┊￥88"
@property(nonatomic,copy) NSMutableAttributedString * attributedCoupon; // 券
@property(nonatomic,copy) NSMutableAttributedString * attributedCommission; // 首页的分享赚
@property(nonatomic,assign) float  couponMoneyWi; // 券的宽度
@property(nonatomic,copy) NSMutableAttributedString * attributedGoodsName; // 详情页商品标题
@property(nonatomic,assign) float titleHi; // 标题的高

@property(nonatomic,strong) NSArray * goodsGalleryUrls; // 商品轮播图数组
@property(nonatomic,strong) NSArray * shopService;      // 店铺服务
@property(nonatomic,copy) NSString * couponRemainQuantity; // 优惠券剩余数量
@property(nonatomic,copy) NSString * mallName;  // 店铺名
@property(nonatomic,copy) NSString * evalScore; // 评分

@property(nonatomic,strong) NSMutableAttributedString * attributedDesc; // 商品推荐语
@property(nonatomic,assign) float desHi; // 推荐语高度
@property(nonatomic,copy) NSString * couponEndTime;  // 优惠券到期时间

-(instancetype)initWithDic:(NSDictionary*)dic;

-(void)addDetailDic:(NSDictionary*)dic; // 百年老农的方法


#pragma mark ===>>> 唯品会
@property(nonatomic,copy) NSString * cpsUrl;  // 唯品会商品链接
@property(nonatomic,assign) BOOL  ifNet;  // 唯品会商品链接
-(instancetype)initWithWPH:(NSDictionary*)dic;


#pragma mark ===>>> 多麦
@property(nonatomic,strong)NSArray * gdImgAry; // 商品详情轮播
@property(nonatomic,copy) NSString * gfromUrl; // 商品来源的图片
@property(nonatomic,copy) NSString * goodsUrl; // 商品链接
@property(nonatomic,copy) NSString * trackUrl; // 推广链接
@property(nonatomic,copy) NSString * rate;     // 佣金比例

-(instancetype)initWithDM:(NSDictionary*)dic;




/**
 返回一个头部带有天猫淘宝图片的字符串(AttributedString)

 @param model 商品model
 @param font  字体
 @param color 字体颜色
 @param frame 天猫淘宝的图片大小
 @return  带有图片属性的可变字符串
 */
+(NSMutableAttributedString*)attributedTitleBy:(GoodsModel*)model font:(UIFont*)font tcolor:(UIColor*)color imgF:(CGRect)frame;


//http://www.jianshu.com/p/870eb4b4170a (字典快速赋值)
-(void)setValue:(id)value forUndefinedKey:(NSString *)key;


@end




