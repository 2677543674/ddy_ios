//
//  GoodsPoster.m
//  DingDingYang
//
//  Created by ddy on 2018/4/12.
//

#import "GoodsPoster.h"

@implementation GoodsPoster

+(void)createShareGoodsPosterBy:(NSMutableArray*)goodsAry goodsidAry:(NSArray*)idAry resultPoster:(void(^)(id data))dataBlock{
    NSNumber * num = @(goodsAry.count);
    NSDictionary * pdic = @{@"nums":num,@"goodsIds":[idAry componentsJoinedByString:@","]};
    
    [NetRequest requestType:0 url:url_goods_poster_frame ptdic:pdic success:^(id nwdic) {
        NSArray * frameAry = NULLARY(nwdic[@"list"]);
        NSString * domain = REPLACENULL(nwdic[@"domain"]);
//        NSMutableArray * imgUrlAry = [NSMutableArray array];
//        for (NSInteger i=0; i<goodsAry.count; i++) {
//            GoodsModel * model = goodsAry[i];
//            [imgUrlAry addObject:model.smallPic];
//        }
//        [NetRequest downloadImageByUrl:imgUrlAry success:^(id result) {
            // 集合页链接(生成二维码)
            [self GoodsPosterBy:frameAry goodsAry:goodsAry domain:domain resultPoster:^(id data) {
                dataBlock(data);
            }];
//        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
       
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        dataBlock(failure);
    }];
    
}


+(void)GoodsPosterBy:(NSArray*)frameAry goodsAry:(NSMutableArray*)goodsAry domain:(NSString*)domain resultPoster:(void(^)(id data))dataBlock{
    
    
    @try {
        
        // goodsStyle == 2||4||5||8 加载的是大图
        
        
        @autoreleasepool {
            
            NSInteger num_img=0, num_tit=0, num_pric=0, num_endp=0;
            
            // 设置画布大小
            CGSize posterSize = CGSizeMake(375,667); // 先给默认的
            if (frameAry.count>0) { // 在从服务器返回数据中找
                FrameAndColorModel * bgFcModel = [[FrameAndColorModel alloc]initWith:frameAry[0]];
                if (bgFcModel.type==0) {
                    posterSize = CGSizeMake(bgFcModel.w, bgFcModel.h);
                }
            }
            
            UIGraphicsBeginImageContextWithOptions(posterSize,YES,[UIScreen mainScreen].scale);
            
            for (NSInteger i =0; i<frameAry.count; i++) {
                
                FrameAndColorModel * fcModel = [[FrameAndColorModel alloc]initWith:frameAry[i]];
                if (fcModel.x==-1) { fcModel.x = posterSize.width/2; }
                switch (fcModel.type) {
                    case 0:{ // 空白画布
                        
                        UIImage * bgImg = [UIImage imageNamed:@"white_bg"];
                        [bgImg drawInRect:CGRectMake(0, 0, fcModel.w, fcModel.h)];
                        
                    }break;
                        
                    case 1:{ // 图片(网络图片)
                        
                        if ([fcModel.content hasPrefix:@"http"]) {
                            [NetRequest downloadImageByUrl:fcModel.content success:^(id result) {
                                UIImage * image = (UIImage*)result;
                                [image  drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
                        }
                        
                    }break;
                        
                    case 2:{ // 文字
                        
                        NSMutableAttributedString * content = [[NSMutableAttributedString alloc]initWithString:fcModel.content] ;
                        [content addAttributes:@{NSForegroundColorAttributeName:fcModel.color} range:NSMakeRange(0, fcModel.content.length)];
                        [content addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fcModel.wordSize]} range:NSMakeRange(0, fcModel.content.length)];
                        [content drawInRect:CGRectMake(fcModel.x, fcModel.y,StringSize(fcModel.content,fcModel.wordSize).width,StringSize(fcModel.content,fcModel.wordSize).height)];
                        
                    }break;
                        
                    case 3:{ // 二维码合成(content返回链接)
                        
                        if ([fcModel.content hasPrefix:@"http"]) {
                            UIImage * ewmimg = EWMImageWithStr(fcModel.content,500,nil);
                            [ewmimg  drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                        }else{
                            UIImage * ewmimg = EWMImageWithStr(domain,500,nil);
                            [ewmimg  drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                        }
                      
                    }break;
                        
                    case 4:{ // 小程序码
                        if ([fcModel.content hasPrefix:@"http"]) {
                            UIImage * ewmimg = EWMImageWithStr(fcModel.content,500,nil);
                            [ewmimg  drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                        }else{
                            UIImage * ewmimg = EWMImageWithStr(domain,500,nil);
                            [ewmimg  drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                        }
//                        if (shmd.codeImageDate) {
//                            [shmd.codeImageDate drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
//                        }
                    }break;
                        
                    case 5:{ // 本地图片
                        
                        GoodsModel * gmd = goodsAry[num_img];
                        NSString * imgUrl = gmd.smallPic;
                        NSInteger type = GoodsListStyle;
                        if (type==2||type==4||type==5||type==8) { imgUrl = gmd.goodsPic; }
                        [NetRequest downloadImageByUrl:imgUrl success:^(id result) {
                            UIImage * image = (UIImage*)result;
                            [image drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
                        num_img++;
                        
                    }break;
                        
                    case 6:{ // logo
//                        UIImage * logoimg = [UIImage imageNamed:@"logo"];
//                        [logoimg drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                    }break;
                        
                    case 7:{ // 商品名称
                        
                        GoodsModel * gmd = goodsAry[num_tit];
                        NSMutableAttributedString * goodsName = [[NSMutableAttributedString alloc]initWithString:gmd.goodsName] ;
                        [goodsName addAttributes:@{NSForegroundColorAttributeName:fcModel.color} range:NSMakeRange(0, gmd.goodsName.length)];
                        [goodsName addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fcModel.wordSize]} range:NSMakeRange(0, gmd.goodsName.length)];
                        float wi = posterSize.width-30;
                        if (fcModel.w>0) {
                            wi = fcModel.w;
                        }else{
                            if (goodsAry.count==3) {
                                if (num_tit>0) {
                                    wi = (posterSize.width-35)/2;
                                }
                            }
                            if (goodsAry.count>=4) {
                                if (num_tit>0) {
                                    wi = (posterSize.width-40)/3;
                                }
                            }
                        }
                        
                        [goodsName drawInRect:CGRectMake(fcModel.x, fcModel.y,wi,StringSize(gmd.goodsName,fcModel.wordSize).height)];
                        num_tit++;
                        
                    }break;
                        
                    case 8:{ // 价格
                        
                        GoodsModel * gmd = goodsAry[num_pric];
                        NSMutableAttributedString * goodsPrice = [[NSMutableAttributedString alloc]initWithString:gmd.strPrice] ;
                        [goodsPrice addAttributes:@{NSForegroundColorAttributeName:fcModel.color} range:NSMakeRange(0, gmd.strPrice.length)];
                        [goodsPrice addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fcModel.wordSize]} range:NSMakeRange(0, gmd.strPrice.length)];
                        [goodsPrice addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, gmd.strPrice.length)];
                        [goodsPrice drawInRect:CGRectMake(fcModel.x, fcModel.y, StringSize(gmd.strPrice,fcModel.wordSize).width, StringSize(gmd.strPrice,fcModel.wordSize).height)];
                        num_pric++;
                        
                    }break;
                        
                    case 9:{ // 券后价
                        
                        GoodsModel * gmd = goodsAry[num_endp];
                        NSMutableAttributedString * endPrice = [[NSMutableAttributedString alloc]initWithString:gmd.strEndPrice] ;
                        [endPrice addAttributes:@{NSForegroundColorAttributeName:fcModel.color} range:NSMakeRange(0, gmd.strEndPrice.length)];
                        [endPrice addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fcModel.wordSize]} range:NSMakeRange(0, gmd.strEndPrice.length)];
                        [endPrice drawInRect:CGRectMake(fcModel.x, fcModel.y, StringSize(gmd.strEndPrice,fcModel.wordSize).width, StringSize(gmd.strEndPrice,fcModel.wordSize).height)];
                        num_endp++;
                        
                    }break;
                        
                    case 10:{ // 图形(给长宽)
                        UIImage * bgimg = [UIColor createColorImg:fcModel.bg_16 alpha:fcModel.tm];
                        [bgimg drawInRect:CGRectMake(fcModel.x, fcModel.y, fcModel.w, fcModel.h)];
                    }break;
                        
                    default: break;
                }
                
            }
            
            // 获取绘制图片
            UIImage * resultingImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            dataBlock(resultingImage);
            
            NSData * data = UIImageJPEGRepresentation(resultingImage, 1);
            NSUInteger length = data.length ; float fl = [FORMATSTR(@"%lu",(unsigned long)length) floatValue];
            NSLog(@"合成的图片大小:约为：%.2fkb,%.2fM",fl/1000,fl/1000/1000);
            
        }
        
    } @catch (NSException *exception) {
        dataBlock(@"商品海报生成失败!");
    } @finally {
        
    }
}

@end




@implementation FrameAndColorModel

-(instancetype)initWith:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        
        _type = [REPLACENULL(dic[@"type"]) integerValue];
        
        _bg_16 = REPLACENULL(dic[@"bg"]);
        if ([REPLACENULL(dic[@"bg"]) hasPrefix:@"#"]) {
           self.bg = [UIColor colorWithHexString:REPLACENULL(dic[@"bg")]];
        }
        
//        _content = REPLACENULL(dic[@"content"]);
        
        _color_16 = REPLACENULL(dic[@"color"]);
        if ([REPLACENULL(dic[@"color"]) hasPrefix:@"#"]) {
            self.color = [UIColor colorWithHexString:REPLACENULL(dic[@"color")]];
        }
        
        _weigth = [REPLACENULL(dic[@"weigth"]) integerValue];
                                                                 
        // 以下字体和坐标已换算成IOS使用

        _wordSize = [REPLACENULL(dic[@"wordSize"]) floatValue]/2;

        _tm = [REPLACENULL(dic[@"tm"]) floatValue]/100;
       
        _w = [REPLACENULL(dic[@"w"]) floatValue]/2;
        _h = [REPLACENULL(dic[@"h"]) floatValue]/2;
        if([REPLACENULL(dic[@"w"]) floatValue]!=-1){
           _x = [REPLACENULL(dic[@"x"]) floatValue]/2;
        }else{  _x = -1;  }
                                                                
        _y = [REPLACENULL(dic[@"y"]) floatValue]/2;

         NSString * cnt = REPLACENULL(dic[@"content"]);
         if(cnt.length>0&&_type==1){
             _content = [cnt hasPrefix:@"http"]?cnt:FORMATSTR(@"%@%@",dns_dynamic_loadimg,cnt);
         }else{
             _content = REPLACENULL(dic[@"content"]);
         }
                                                                 
    }
    return self;
}
         
                                                                 
-(instancetype)initWithUser:(NSDictionary*)mydic{
    self = [super init];
    if (self) {
        
        _type = [REPLACENULL(mydic[@"type"]) integerValue];
        
        _bg_16 = REPLACENULL(mydic[@"bg"]);
        if ([REPLACENULL(mydic[@"bg"]) hasPrefix:@"#"]) {
            self.bg = [UIColor colorWithHexString:REPLACENULL(mydic[@"bg")]];
        }
                                                              
        _color_16 = REPLACENULL(mydic[@"color"]);
        if ([REPLACENULL(mydic[@"color"]) hasPrefix:@"#"]) {
           self.color = [UIColor colorWithHexString:REPLACENULL(mydic[@"color")]];
        }

        _weigth = [REPLACENULL(mydic[@"weigth"]) integerValue];

        // 以下字体和坐标已换算成IOS使用

        _wordSize = [REPLACENULL(mydic[@"wordSize"]) floatValue]/2;

        _tm = [REPLACENULL(mydic[@"tm"]) floatValue]/100;

        if(REPLACENULL(mydic[@"width"]).length>0){
           _w = [REPLACENULL(mydic[@"width"]) floatValue]/2;
        }else{
           _w = [REPLACENULL(mydic[@"w"]) floatValue]/2;
        }
                                                                
        if(REPLACENULL(mydic[@"height"]).length>0){
            _h = [REPLACENULL(mydic[@"height"]) floatValue]/2;
        }else{
            _h = [REPLACENULL(mydic[@"h"]) floatValue]/2;
        }

        if([REPLACENULL(mydic[@"w"]) floatValue]!=-1){
           _x = [REPLACENULL(mydic[@"x"]) floatValue]/2;
        }else{  _x = -1;  }

        _y = [REPLACENULL(mydic[@"y"]) floatValue]/2;
                                                                
        NSString * cnt = REPLACENULL(mydic[@"content"]);
        if(cnt.length>0&&_type==1){
            _content = [cnt hasPrefix:@"http"]?cnt:FORMATSTR(@"%@%@",dns_dynamic_loadimg,cnt);
        }else{
            _content = REPLACENULL(mydic[@"content"]);
        }
    }
        return self;
}

                                                                 
@end





