//
//  GoodsPoster.h
//  DingDingYang
//
//  Created by ddy on 2018/4/12.
//  合成商品海报

#import <Foundation/Foundation.h>


@interface GoodsPoster : NSObject

+(void)createShareGoodsPosterBy:(NSMutableArray*)goodsAry goodsidAry:(NSArray*)idAry resultPoster:(void(^)(id data))dataBlock;

@end



@interface FrameAndColorModel : NSObject

/** type
 0: 空白画布（第一张）
 1: 图片
 2: 文字
 3: 二维码合成(content返回链接)
 4: 小程序码
 5: 本地图片
 6: logo
 7: 商品名称
 8: 价格
 9: 券后价
 10: 图形(给长宽)
 */
@property(nonatomic,assign) NSInteger type;

@property(nonatomic,copy) UIColor * bg; // 背景颜色(可为空)
@property(nonatomic,copy) NSString * bg_16; // 背景色(十六进制)
@property(nonatomic,copy) NSString * content; // 内容文字(可为空)
@property(nonatomic,copy) UIColor  * color;   // 颜色(可为空)
@property(nonatomic,copy) NSString * color_16;
@property(nonatomic,assign) NSInteger weigth; // 1:字体粗
@property(nonatomic,assign) float wordSize; // 字体大小
@property(nonatomic,assign) float tm;// 透明度
@property(nonatomic,assign) float w; // 宽
@property(nonatomic,assign) float h; // 高
@property(nonatomic,assign) float x; // -1表示放中间(文字都是放中间)
@property(nonatomic,assign) float y; // y坐标

-(instancetype)initWith:(NSDictionary*)dic;

-(instancetype)initWithUser:(NSDictionary*)mydic;

@end






