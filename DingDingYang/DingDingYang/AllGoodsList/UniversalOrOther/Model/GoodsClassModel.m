//
//  GoodsClassModel.m
//  DingDingYang
//
//  Created by ddy on 2018/3/24.
//

#import "GoodsClassModel.h"

#define cacheT_goods_class  @"goods_class_title_array" // 缓存内容
#define cacheA_goods_class  @"goods_class_save_time"   // 上一次的缓存时间

#define cacheT_goods_class_wph  @"goods_class_title_array_wph" // 缓存内容
#define cacheA_goods_class_wph  @"goods_class_save_time_wph"   // 上一次的缓存时间

#define cacheT_goods_class_dm  @"goods_class_title_array_dm" // 缓存内容
#define cacheA_goods_class_dm  @"goods_class_save_time_dm"   // 上一次的缓存时间

#define cacheT_goods_class_jd  @"goods_class_title_array_jd" // 缓存内容
#define cacheA_goods_class_jd  @"goods_class_save_time_jd"   // 上一次的缓存时间


@implementation GoodsClassModel


#pragma mark ===>>> 拼多多的分类

+(void)getGoodsClassModelAryCachePdd:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock{
    
    if (NSUDTakeData(cacheA_goods_class)) { // 本就有缓存
        [NetRequest compareRequestTime:cacheT_goods_class result:^(NSInteger resultType) {
            // 先返回默认值
            NSMutableArray * cacheAry = [NSMutableArray array];
            NSArray * ary = NULLARY(NSUDTakeData(cacheA_goods_class));
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithDic:obj];
                [cacheAry addObject:classModel];
            }];
            completeCacheBlock(cacheAry);
            if (resultType==0) { // 需要重新请求
                [self qingQiuIsCache:0 success:^(NSArray *dataAry) {
                    if (![ary isEqual:dataAry]) {
                        [cacheAry removeAllObjects];
                        [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithDic:obj];
                            [cacheAry addObject:classModel];
                        }];
                        completeNewBlock(cacheAry);
                        NSLog(@"新数据跟原来数据相比发生了变化，回调新数据。。。。");
                    }else{
                        NSLog(@"重新请求的数据和缓存的数据一样，不再回调，减少刷新。。。。");
                    }
                } error:^(NSString *error) {
                    NSLog(@"需要重新请求时请求失败: %@",error);
                }];
            }
        }];
    }else{
        //        completeCacheBlock(nil);
        NSMutableArray * goodsModelAry = [NSMutableArray array];
        [self qingQiuIsCache:0 success:^(NSArray *dataAry) {
            NSLog(@"首次请求的数据。。。。。。。。。");
            [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithDic:obj];
                [goodsModelAry addObject:classModel];
            }];
            completeNewBlock(goodsModelAry);
        } error:^(NSString *error) { SHOWMSG(error) }];
    }
}

+(void)qingQiuIsCache:(NSInteger)yn success:(void(^)(NSArray *dataAry))completeS error:(void(^)(NSString *error))completeF{
    [NetRequest requestTokenURL:url_goods_class_pdd parameter:@{@"type":@"1"} success:^(NSDictionary *nwdic) {
        NSArray * ary = NULLARY(nwdic[@"list"]);
        if (ary.count>0) {
            NSString * time = NowTimeByFormat(@"yyyy-MM-dd") ;
            NSUDSaveData(time, cacheT_goods_class)
            NSUDSaveData(ary,cacheA_goods_class)
            completeS(ary);
        }else{
            completeF(@"未获取到商品分类!");
            
        }
    } failure:^(NSString *failure) {
        completeF(failure);
    }];
}

-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.gcId = REPLACENULL(dic[@"id"]);
        self.title = REPLACENULL(dic[@"name"]);
        self.image = REPLACENULL(dic[@"img"]);
    }
    return self;
}


#pragma mark ===>>> 唯品会的分类
//唯品会的
+(void)getGoodsClassModelAryCacheWPH:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock{
    
    if (NSUDTakeData(cacheA_goods_class_wph)) { // 本就有缓存
        [NetRequest compareRequestTime:cacheT_goods_class_wph result:^(NSInteger resultType) {
            // 先返回默认值
            NSMutableArray * cacheAry = [NSMutableArray array];
            NSArray * ary = NULLARY(NSUDTakeData(cacheA_goods_class_wph));
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithWPH:obj];
                [cacheAry addObject:classModel];
            }];
            completeCacheBlock(cacheAry);
            if (resultType==0) { // 需要重新请求
                [self qingQiuIsCacheWPH:0 success:^(NSArray *dataAry) {
                    if (![ary isEqual:dataAry]) {
                        [cacheAry removeAllObjects];
                        [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithWPH:obj];
                            [cacheAry addObject:classModel];
                        }];
                        completeNewBlock(cacheAry);
                        NSLog(@"新数据跟原来数据相比发生了变化，回调新数据。。。。");
                    }else{
                        NSLog(@"重新请求的数据和缓存的数据一样，不再回调，减少刷新。。。。");
                    }
                } error:^(NSString *error) {
                    NSLog(@"需要重新请求时请求失败: %@",error);
                }];
            }
        }];
    }else{
        NSMutableArray * goodsModelAry = [NSMutableArray array];
        [self qingQiuIsCacheWPH:0 success:^(NSArray *dataAry) {
            NSLog(@"首次请求的数据。。。。。。。。。");
            [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithWPH:obj];
                [goodsModelAry addObject:classModel];
            }];
            completeNewBlock(goodsModelAry);
        } error:^(NSString *error) { SHOWMSG(error) }];
    }
}

+(void)qingQiuIsCacheWPH:(NSInteger)yn success:(void(^)(NSArray *dataAry))completeS error:(void(^)(NSString *error))completeF{
    [NetRequest requestTokenURL:url_goods_class_wph parameter:@{@"type":@"1"} success:^(NSDictionary *nwdic) {
        NSArray * ary = NULLARY(nwdic[@"list"]);
        if (ary.count>0) {
            NSString * time = NowTimeByFormat(@"yyyy-MM-dd") ;
            NSUDSaveData(time, cacheT_goods_class_wph)
            NSUDSaveData(ary,cacheA_goods_class_wph)
            completeS(ary);
        }else{
            completeF(@"未获取到商品分类!");
            
        }
    } failure:^(NSString *failure) {
        completeF(failure);
    }];
}

-(instancetype)initWithWPH:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.gcId = REPLACENULL(dic[@"categoryId"]);
        self.title = REPLACENULL(dic[@"categoryName"]);
        self.image = REPLACENULL(dic[@"img"]);
    }
    return self;
}


#pragma mark ===>>> 多麦的分类

+(void)getGoodsClassModelAryCacheDM:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock{
    
    if (NSUDTakeData(cacheA_goods_class_dm)) { // 本就有缓存
        [NetRequest compareRequestTime:cacheT_goods_class_dm result:^(NSInteger resultType) {
            // 先返回默认值
            NSMutableArray * cacheAry = [NSMutableArray array];
            NSArray * ary = NULLARY(NSUDTakeData(cacheA_goods_class_dm));
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithDM:obj];
                [cacheAry addObject:classModel];
            }];
            completeCacheBlock(cacheAry);
            if (resultType==0) { // 需要重新请求
                [self qingQiuIsCacheDM:0 success:^(NSArray *dataAry) {
                    if (![ary isEqual:dataAry]) {
                        [cacheAry removeAllObjects];
                        [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithDM:obj];
                            [cacheAry addObject:classModel];
                        }];
                        completeNewBlock(cacheAry);
                        NSLog(@"新数据跟原来数据相比发生了变化，回调新数据。。。。");
                    }else{
                        NSLog(@"重新请求的数据和缓存的数据一样，不再回调，减少刷新。。。。");
                    }
                } error:^(NSString *error) {
                    NSLog(@"需要重新请求时请求失败: %@",error);
                }];
            }
        }];
    }else{
        NSMutableArray * goodsModelAry = [NSMutableArray array];
        [self qingQiuIsCacheDM:0 success:^(NSArray *dataAry) {
            NSLog(@"首次请求的数据。。。。。。。。。");
            [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithDM:obj];
                [goodsModelAry addObject:classModel];
            }];
            completeNewBlock(goodsModelAry);
        } error:^(NSString *error) { SHOWMSG(error) }];
    }
}

+(void)qingQiuIsCacheDM:(NSInteger)yn success:(void(^)(NSArray *dataAry))completeS error:(void(^)(NSString *error))completeF{

    [NetRequest requestTokenURL:url_goods_class_dm parameter:@{@"type":@"1"} success:^(NSDictionary *nwdic) {
        NSArray * ary = NULLARY(nwdic[@"cateList"]);
        if (ary.count>0) {
            NSString * time = NowTimeByFormat(@"yyyy-MM-dd") ;
            NSUDSaveData(time, cacheT_goods_class_dm)
            NSUDSaveData(ary,cacheA_goods_class_dm)
            completeS(ary);
        }else{
            completeF(@"未获取到商品分类!");
        }
    } failure:^(NSString *failure) {
        completeF(failure);
    }];
}

-(instancetype)initWithDM:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.gcId = REPLACENULL(dic[@"id"]);
        self.title = REPLACENULL(dic[@"name"]);
        self.image = REPLACENULL(dic[@"img"]);
    }
    return self;
}



#pragma mark ===>>> 京东的分类

+(void)getGoodsClassModelAryCacheJD:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock{
    
    if (NSUDTakeData(cacheA_goods_class_jd)) { // 本就有缓存
        [NetRequest compareRequestTime:cacheT_goods_class_jd result:^(NSInteger resultType) {
            // 先返回默认值
            NSMutableArray * cacheAry = [NSMutableArray array];
            NSArray * ary = NULLARY(NSUDTakeData(cacheA_goods_class_jd));
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithJD:obj];
                [cacheAry addObject:classModel];
            }];
            completeCacheBlock(cacheAry);
            if (resultType==0) { // 需要重新请求
                [self qingQiuIsCacheJD:0 success:^(NSArray *dataAry) {
                    if (![ary isEqual:dataAry]) {
                        [cacheAry removeAllObjects];
                        [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithJD:obj];
                            [cacheAry addObject:classModel];
                        }];
                        completeNewBlock(cacheAry);
                        NSLog(@"新数据跟原来数据相比发生了变化，回调新数据。。。。");
                    }else{
                        NSLog(@"重新请求的数据和缓存的数据一样，不再回调，减少刷新。。。。");
                    }
                } error:^(NSString *error) {
                    NSLog(@"需要重新请求时请求失败: %@",error);
                }];
            }
        }];
    }else{
        NSMutableArray * goodsModelAry = [NSMutableArray array];
        [self qingQiuIsCacheJD:0 success:^(NSArray *dataAry) {
            NSLog(@"首次请求的数据。。。。。。。。。");
            [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                GoodsClassModel * classModel = [[GoodsClassModel alloc]initWithJD:obj];
                [goodsModelAry addObject:classModel];
            }];
            completeNewBlock(goodsModelAry);
        } error:^(NSString *error) { SHOWMSG(error) }];
    }
}

+(void)qingQiuIsCacheJD:(NSInteger)yn success:(void(^)(NSArray *dataAry))completeS error:(void(^)(NSString *error))completeF{

    [NetRequest requestTokenURL:url_goods_class_jd parameter:nil success:^(NSDictionary *nwdic) {
        NSArray * ary = NULLARY(nwdic[@"list"]);
        if (ary.count>0) {
            NSString * time = NowTimeByFormat(@"yyyy-MM-dd") ;
            NSUDSaveData(time, cacheT_goods_class_jd)
            NSUDSaveData(ary,cacheA_goods_class_jd)
            completeS(ary);
        }else{
            completeF(@"未获取到商品分类!");
        }
    } failure:^(NSString *failure) {
        completeF(failure);
    }];
}

-(instancetype)initWithJD:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.gcId = REPLACENULL(dic[@"cateId"]);
        self.title = REPLACENULL(dic[@"name"]);
        self.image = REPLACENULL(dic[@"img"]);
    }
    return self;
}

@end

