//
//  GoodsModel.m
//  DingDingYang
//
//  Created by ddy on 2017/3/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "GoodsModel.h"

@implementation GoodsModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        if ((NSNull *)dic == [NSNull null]) { return nil; }
        
        self.couponMoney = ROUNDED( REPLACENULL(dic[@"couponMoney"]),2);
        self.commission = ROUNDED(REPLACENULL(dic[@"commission"]),2);
        self.goodsGalleryUrls = NULLARY(dic[@"goodsGalleryUrls"]);
        self.endPrice = ROUNDED(REPLACENULL(dic[@"endPrice"]),2);
        self.couponSurplus = REPLACENULL(dic[@"couponSurplus"]);
        self.ifGet = [REPLACENULL(dic[@"ifGet"]) integerValue];
        self.price = ROUNDED(REPLACENULL(dic[@"price"]),2);
        self.activityId = REPLACENULL(dic[@"activityId"]);
        self.goodsName = REPLACENULL(dic[@"goodsName"]);
        self.platform = REPLACENULL(dic[@"platform"]);
        self.goodsPic = REPLACENULL(dic[@"goodsPic"]);
        self.smallPic = REPLACENULL(dic[@"smallPic"]);
        self.goodsId = REPLACENULL(dic[@"goodsId"]);
        self.itemUrl = REPLACENULL(dic[@"itemUrl"]);
        self.endDays = REPLACENULL(dic[@"endDays"]);
        self.keyWord = REPLACENULL(dic[@"keyWord"]);
        self.catId = REPLACENULL(dic[@"catId"]);
        self.sales = REPLACENULL(dic[@"sales"]);
        self.video = REPLACENULL(dic[@"video"]);
        self.desStr = REPLACENULL(dic[@"des"]);
        self.url = @"";
        

        
        // 提前拼接好的参数(省去在cell上的拼接过程)
        _strSales = FORMATSTR(@"销量:%@",_sales);
        _strPrice = FORMATSTR(@"原价:￥%@",_price);
        _strEndPrice = FORMATSTR(@"￥%@",_endPrice);
        _strCommission = FORMATSTR(@"赚￥%@",_commission);
        _strCouponMoney = FORMATSTR(@"券┊￥%@",_couponMoney);
        _couponMoneyWi = StringSize(_strCouponMoney,10*HWB).width+2*HWB;
        
        @try { // 一些带属性的字符串，直接在model中处理，不要在cell上处理
            // 优惠券
            NSString * couponStr = FORMATSTR(@"券  ￥%@",_couponMoney);
            _attributedCoupon = [[NSMutableAttributedString alloc] initWithString:couponStr];
            NSRange couponRange =  {0,couponStr.length} ;
            [_attributedCoupon addAttribute:NSForegroundColorAttributeName value:COLORWHITE range:couponRange];
            [_attributedCoupon addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:10*HWB] range:couponRange];
            [_attributedCoupon addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:8*HWB] range:NSMakeRange(3, 1)];
            [_attributedCoupon addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:9*HWB] range:NSMakeRange(0, 1)];
            [_attributedCoupon addAttribute:NSKernAttributeName value:@-0.3 range:couponRange];
            NSTextAttachment * attrLineImg = [[NSTextAttachment alloc] init];
            attrLineImg.image = [UIImage imageNamed:@"goods_quan_line"];
            attrLineImg.bounds = CGRectMake(0, -1.5, 1, 10*HWB);
            NSAttributedString * attrLine = [NSAttributedString attributedStringWithAttachment:attrLineImg];
            [_attributedCoupon insertAttributedString:attrLine atIndex:2];
            
            // 商品标题
//            NSString * goodsTitle = self.goodsName;
//            if (_strPlatform) { goodsTitle = FORMATSTR(@"     %@",self.goodsName); }
//            NSMutableParagraphStyle * paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];
//            [paragraphStyle2 setLineSpacing:2.5];
//            NSDictionary * goodsNameDic = @{NSForegroundColorAttributeName:Black51,
//                                            NSFontAttributeName:[UIFont systemFontOfSize:13.5*HWB],
//                                            NSParagraphStyleAttributeName:paragraphStyle2,
//                                            NSKernAttributeName:@1
//                                            };
//            _attributedGoodsName = [[NSMutableAttributedString alloc]initWithString:goodsTitle attributes:goodsNameDic];
//            CGRect rectTitle = [goodsTitle boundingRectWithSize:CGSizeMake(ScreenW-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:goodsNameDic context:nil];
//            _titleHi = rectTitle.size.height;
            
            
        } @catch (NSException *exception) { } @finally { }
    
    }
    return self;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}


// 给model新加参数 ，商品详情页,
-(void)addDetailDic:(NSDictionary*)dic{
    
     // 详情页多的参数(拼多多)
    if ([_platform isEqualToString:@"拼多多"]) {
        self.goodsGalleryUrls = NULLARY(dic[@"goodsGalleryUrls"]);
        self.shopService = NULLARY(dic[@"shopService"]);
        
        self.couponRemainQuantity = REPLACENULL(dic[@"couponRemainQuantity"]);
        self.couponEndTime = REPLACENULL(dic[@"couponEndTime"]);
        self.desStr = REPLACENULL(dic[@"goodsDesc"]);
        self.evalScore = REPLACENULL(dic[@"evalScore"]);
        self.mallName = REPLACENULL(dic[@"mallName"]);
    }
    
    // 给推荐语设置间距并计算高度
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:2];
    NSDictionary * desAttbDic = @{NSFontAttributeName:[UIFont systemFontOfSize:11.5*HWB],
                                  NSForegroundColorAttributeName:Black102,
                                  NSParagraphStyleAttributeName:paragraphStyle,
                                  NSKernAttributeName:@1
                                  };
    self.attributedDesc = [[NSMutableAttributedString alloc]initWithString:_desStr attributes:desAttbDic];
    CGRect rectDes = [_desStr boundingRectWithSize:CGSizeMake(ScreenW-30-40*HWB, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:desAttbDic context:nil];
    _desHi = rectDes.size.height+25*HWB;
    
    if (!_attributedGoodsName) {
        // 商品标题
        NSString * goodsTitle = self.goodsName;
        if (_platform&&_platform.length>0) { goodsTitle = FORMATSTR(@"      %@",self.goodsName); }
        NSMutableParagraphStyle * paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle2 setLineSpacing:2.5];
        NSDictionary * goodsNameDic = @{NSForegroundColorAttributeName:Black51,
                                        NSFontAttributeName:[UIFont systemFontOfSize:13.5*HWB],
                                        NSParagraphStyleAttributeName:paragraphStyle2,
                                        NSKernAttributeName:@1
                                        };
        NSString * gNameStr = (_platform&&_platform.length>0)?FORMATSTR(@" %@",self.goodsName):self.goodsName;
        _attributedGoodsName = [[NSMutableAttributedString alloc]initWithString:gNameStr attributes:goodsNameDic];
        CGRect rectTitle = [goodsTitle boundingRectWithSize:CGSizeMake(ScreenW-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:goodsNameDic context:nil];
        _titleHi = rectTitle.size.height;
        
        if (_platform&&_platform.length>0) {
            NSTextAttachment * baoYoutext = [[NSTextAttachment alloc] init];
            if (![_platform isEqualToString:@"多麦"]) {
                
                NSAttributedString * attrBaoYou = [NSAttributedString attributedStringWithAttachment:baoYoutext];
                if ([_platform containsString:@"天猫"]) {
                    baoYoutext.image = [UIImage imageNamed:@"tmallicon"];
                }else if([_platform containsString:@"拼多多"]){
                    baoYoutext.image = [UIImage imageNamed:@"pddicon"]; // 拼多多
                }else if([_platform containsString:@"淘宝"]){
                    baoYoutext.image = [UIImage imageNamed:@"tbicon"]; // 淘宝
                }else if([_platform containsString:@"京东"]){
                    baoYoutext.image = [UIImage imageNamed:@"source_jingdong"]; // 京东
                }else{
                    
                }
                baoYoutext.bounds = CGRectMake(0, -3*HWB, 15*HWB, 15*HWB);
                [_attributedGoodsName insertAttributedString:attrBaoYou atIndex:0];
            }else{
                if ([_platform containsString:@"多麦"]) {
                    //                [NetRequest downloadImageByUrl:_gfromUrl success:^(id result) {
                    //                    UIImage * limg = (UIImage*)result;
                    //                    baoYoutext.image = limg;
                    //                    baoYoutext.bounds = CGRectMake(0, -3*HWB, limg.size.width/limg.size.height*16*HWB, 16*HWB);
                    //                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    //                    UIImage * limg = [UIImage imageNamed:@"logo"];
                    //                    baoYoutext.image = limg;
                    //                    baoYoutext.bounds = CGRectMake(0, -3*HWB, 16*HWB, 16*HWB);
                    //                }];
                }
            }
        }
    }
    
    if (self.goodsPic.length==0&&self.smallPic.length==0) {
        if (self.goodsGalleryUrls.count>0) {
            self.goodsPic = self.goodsGalleryUrls[0];
            self.smallPic = self.goodsGalleryUrls[0];
        }
    }
    
}



+(NSMutableAttributedString*)attributedTitleBy:(GoodsModel*)model font:(UIFont*)font tcolor:(UIColor*)color imgF:(CGRect)frame{
    NSString * goodsName = FORMATSTR(@" %@",model.goodsName)  ; // 拼接一个空格
    NSRange goodsNameRg =  {0,goodsName.length} ;
    NSMutableAttributedString * attrGoodsName = [[NSMutableAttributedString alloc] initWithString:goodsName];
    [attrGoodsName addAttributes:@{NSForegroundColorAttributeName:color} range:goodsNameRg];
    [attrGoodsName addAttributes:@{NSFontAttributeName:font} range:goodsNameRg];
    
    NSTextAttachment * baoYoutext = [[NSTextAttachment alloc] init];
    
    if ([model.platform containsString:@"天猫"]) {
        baoYoutext.image = [UIImage imageNamed:@"tmallicon"];
    }
    if([model.platform containsString:@"拼多多"]){
        baoYoutext.image = [UIImage imageNamed:@"pddicon"]; // 拼多多
    }
    if([model.platform containsString:@"淘宝"]){
        baoYoutext.image = [UIImage imageNamed:@"tbicon"]; // 淘宝
    }
    if (frame.size.height>0&&frame.size.width>0) {
        baoYoutext.bounds = frame ;
    }else{
        baoYoutext.bounds = CGRectMake(0, -3*HWB, 16*HWB, 16*HWB);
    }
    if ([model.platform containsString:@"多麦"]) {
        [NetRequest downloadImageByUrl:model.gfromUrl success:^(id result) {
            UIImage * limg = (UIImage*)result;
            baoYoutext.image = limg;
            baoYoutext.bounds = CGRectMake(0, -3*HWB, limg.size.width/limg.size.height*16*HWB, 16*HWB);
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            UIImage * limg = [UIImage imageNamed:@"logo"];
            baoYoutext.image = limg;
            baoYoutext.bounds = CGRectMake(0, -3*HWB, 16*HWB, 16*HWB);
        }];
    }
    
    NSAttributedString * attrBaoYou = [NSAttributedString attributedStringWithAttachment:baoYoutext];
    [attrGoodsName insertAttributedString:attrBaoYou atIndex:0];
    return  attrGoodsName ;
}


#pragma mark ===>>> 唯品会model
-(instancetype)initWithWPH:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.goodsPic = REPLACENULL(dic[@"imageUrl"]);
        self.goodsName = REPLACENULL(dic[@"title"]);
        self.goodsId = REPLACENULL(dic[@"skuId"]);
        self.price = ROUNDED(REPLACENULL(dic[@"marketPrice"]), 2);
        self.commission = ROUNDED(REPLACENULL(dic[@"commissionValueNewcust"]),2);
        self.endPrice = ROUNDED(REPLACENULL(dic[@"vipPrice"]), 2);
        self.platform = @"唯品会";
        self.cpsUrl = REPLACENULL(dic[@"cpsUrl"]) ;
        self.ifNet = [dic[@"ifNet"] boolValue];
    }
    return self;
}

#pragma mark ===>>> 多麦model
-(instancetype)initWithDM:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        NSString * imgurl = REPLACENULL(dic[@"imgs"]);
        NSArray * imageAry = [imgurl componentsSeparatedByString:@","];
       
        if (imageAry.count>0) {
            self.goodsPic = imageAry[0];
            self.gdImgAry = imageAry;
        }else{
            self.goodsPic = @"";
            self.gdImgAry = @[];
        }

        self.goodsName = REPLACENULL(dic[@"name"]);
        self.goodsId = REPLACENULL(dic[@"goodsId"]);
        self.gfromUrl = REPLACENULL(dic[@"iconUrl"]);
        self.price = ROUNDED(REPLACENULL(dic[@"oldPrice"]), 2);
        self.endPrice = ROUNDED(REPLACENULL(dic[@"newPrice"]), 2);
        self.commission = ROUNDED(REPLACENULL(dic[@"commission"]),2);
        self.platform = @"多麦";
        
        self.goodsUrl = REPLACENULL(dic[@"url"]);
        self.trackUrl = REPLACENULL(dic[@"trackUrl"]);
        
        self.sales = @"0";
        self.couponMoney = @"0";
        
    }
    return self;
}








@end
