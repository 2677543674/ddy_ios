//
//  GoodsClassModel.h
//  DingDingYang
//
//  Created by ddy on 2018/3/24.
//

#import <Foundation/Foundation.h>

@interface GoodsClassModel : NSObject

/**
 获取商品分类 (返回model数组类型)

 @param completeCacheBlock 从缓存中的回调
 @param completeNewBlock 从接口返回中回调
 */
+(void)getGoodsClassModelAryCachePdd:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock;

// 唯品会
+(void)getGoodsClassModelAryCacheWPH:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock;

// 多麦
+(void)getGoodsClassModelAryCacheDM:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock;

// 京东
+(void)getGoodsClassModelAryCacheJD:(void(^)(NSMutableArray * modelCacheAry))completeCacheBlock newDataFromNetWork:(void(^)(NSMutableArray * modelNewAry))completeNewBlock;

@property(nonatomic,copy) NSString * gcId;  // 分类ID
@property(nonatomic,copy) NSString * title; // 标题
@property(nonatomic,copy) NSString * image; // 图片

-(instancetype)initWithDic:(NSDictionary*)dic;
-(instancetype)initWithWPH:(NSDictionary*)dic;
-(instancetype)initWithDM:(NSDictionary*)dic;
-(instancetype)initWithJD:(NSDictionary*)dic;

@end
