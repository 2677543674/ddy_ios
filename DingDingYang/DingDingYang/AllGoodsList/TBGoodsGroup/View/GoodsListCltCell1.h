//
//  GoodsListCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>


/**********************************************************************************/

#pragma mark ===>>>

@interface GoodsListCltCell1 : UICollectionViewCell

@property(nonatomic,strong) UIView      * leftView;         // 最底部(左边View)
@property(nonatomic,strong) UIImageView * goodsImgV;        // 商品图片
@property(nonatomic,strong) UILabel     * goodsTitleLab;    // 商品标题
@property(nonatomic,strong) UIImageView * platformImgV;     // 商品来源图片
@property(nonatomic,strong) UILabel     * catPLab;          // 天猫价
@property(nonatomic,strong) UILabel     * salesLab;         // 销量
@property(nonatomic,strong) UILabel     * vouchersPLab;     // 券后价:
@property(nonatomic,strong) UILabel     * vouchersMoneyLab; // 券后价价格
@property(nonatomic,strong) UILabel     * baoyouLab;        // 包邮
@property(nonatomic,strong) UIButton     * playVideoBtn ;   // 播放视频
@property(nonatomic,strong) UIView      * rightView;        // 右边View
@property(nonatomic,strong) UILabel     * orderLab;         // 下单立省
@property(nonatomic,strong) UIButton    * takeOrderBtn;     // 下单按钮

@property(nonatomic,assign) float         cellH;            // 表格高度

@property(nonatomic,strong) GoodsModel * goodsModel ;

//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end



/**********************************************************************************/

#pragma mark ===>>>

@interface GoodsListCltCell2 : UICollectionViewCell

@property(nonatomic,strong) UIView      * leftView;         // 最底部(左边View)
@property(nonatomic,strong) UIImageView * goodsImgV;        // 商品图片
@property(nonatomic,strong) UILabel     * goodsTitleLab;    // 商品标题
@property(nonatomic,strong) UIImageView * platformImgV;     // 商品来源图片
@property(nonatomic,strong) UILabel     * catPLab;          // 天猫价
@property(nonatomic,strong) UILabel     * salesLab;         // 销量
@property(nonatomic,strong) UILabel     * vouchersPLab;     // 券后价:
@property(nonatomic,strong) UILabel     * vouchersMoneyLab; // 券后价价格
@property(nonatomic,strong) UIButton     * playVideoBtn ;   // 播放视频
@property(nonatomic,strong) UIView      * rightView;        // 领券view
@property(nonatomic,strong) UILabel     * orderLab;         // 下单立省
@property(nonatomic,strong) UIButton    * takeOrderBtn;     // 下单按钮

@property(nonatomic,assign) float         cellH;            // 表格高度

@property(nonatomic,strong) GoodsModel * goodsModel ;

//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end



/**********************************************************************************/

#pragma mark ===>>>

@interface GoodsListCltCell3 : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     // 商品图片
@property(nonatomic,strong) UIButton     * couponsLab ;    // 优惠券
@property(nonatomic,strong) UILabel      * goodsTitle ;    // 商品标题
@property(nonatomic,strong) UILabel      * baoyouLab  ;    // 包邮
@property(nonatomic,strong) UILabel      * endPriceLab ;   // 券后价
@property(nonatomic,strong) UILabel      * endCouponsLab ; // 券后
@property(nonatomic,strong) UILabel      * salesLab ;      // 销量
@property(nonatomic,strong) UIButton     * playVideoBtn ;  // 播放视频
@property(nonatomic,strong) GoodsModel * goodsModel ;

//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end



/**********************************************************************************/

#pragma mark ===>>>

@interface GoodsListCltCell4 : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     // 商品图片
@property(nonatomic,strong) UIButton     * playVideoBtn ;  // 播放视频
@property(nonatomic,strong) UILabel      * goodsTitleLab ; // 商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   // 券后价
@property(nonatomic,strong) UILabel      * catPLab ;       // 天猫价(原价)
@property(nonatomic,strong) UILabel      * salesLab ;      // 销量
@property(nonatomic,strong) UIButton     * couponsBtn ;    // 优惠券(无券隐藏)
//@property(nonatomic,strong) UILabel      * noCouponsLab ;  // 无券提示
@property(nonatomic,strong) UIButton     * shareBtn ;      // 分享赚，消费者隐藏
@property(nonatomic,strong) GoodsModel   * goodsModel ;
@property(nonatomic,strong) UIView       * backView;
@property(nonatomic,strong) UIButton     * youHuiQuan;     //优惠券
@property(nonatomic,strong) UIImageView * platformImgV;     // 商品来源图片

//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end



/**********************************************************************************/

#pragma mark ===>>>

@interface GoodsListCltCell5 : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     //商品图片
@property(nonatomic,strong) UIButton     * couponsLab ;    //优惠券
@property(nonatomic,strong) UILabel      * goodsTitle ;    //商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   //券后价
@property(nonatomic,strong) UILabel      * endCouponsLab ; //券后
@property(nonatomic,strong) UILabel      * salesLab ;      //销量
@property(nonatomic,strong) UILabel      * fenXiangZhuan;  //分享赚
@property(nonatomic,strong) UILabel      * fenXiangZhuanText;  //分享赚
@property(nonatomic,strong) UIButton     * youHuiQuan;     //优惠券
@property(nonatomic,strong) UIImageView  * icon;        //商品图片
@property(nonatomic,strong) UILabel      * pointNum;
@property(nonatomic,strong) UILabel      * catPLab;          //天猫价
@property(nonatomic,strong) UIView* backView;
@property(nonatomic,strong) UIImageView * platformImgV;     // 商品来源图片
@property(nonatomic,strong) UIButton     * playVideoBtn ;  // 播放视频
@property(nonatomic,strong) GoodsModel * goodsModel ;

//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end



/**********************************************************************************/

#pragma mark ===>>>

@interface GoodsListCltCell6 : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     // 商品图片
@property(nonatomic,strong) UIButton     * extendBtn ;     // 推广按钮
@property(nonatomic,strong) UIButton     * buyBtn ;     // 下单按钮
@property(nonatomic,strong) UILabel      * goodsTitle ;    // 商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   // 券后价
@property(nonatomic,strong) UILabel      * priceLab ;      // 原价
@property(nonatomic,strong) UILabel      * salesLab ;      // 销量
@property(nonatomic,strong) UIImageView  * fromIma ;       // 来源
@property(nonatomic,strong) UILabel      * fenxiangzhuan ; // 销量
@property(nonatomic,strong) UILabel      * shareLabel;
@property(nonatomic,strong) UIImageView  * shareIma;
@property(nonatomic,strong) UIButton     * smallImaBtn;
@property(nonatomic,strong) UIButton     * shareBtn;
@property(nonatomic,strong) UILabel      * youHuiQuan;     //优惠券
@property(nonatomic,strong) UIButton     * playVideoBtn ;  // 播放视频
@property(nonatomic,strong) GoodsModel   * goodsModel ;

//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end



/**********************************************************************************/

#pragma mark ===>>>

@interface GoodsListCltCell7 : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     //商品图片
@property(nonatomic,strong) UIButton     * extendBtn ;     //推广按钮
@property(nonatomic,strong) UILabel      * goodsTitle ;    //商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   //券后价
@property(nonatomic,strong) UILabel      * priceLab ;      //原价
@property(nonatomic,strong) UILabel      * salesLab ;
@property(nonatomic,strong) UIImageView  * fromIma ;         //来源
@property(nonatomic,strong) UILabel      * shareLabel;
@property(nonatomic,strong) UIImageView  * shareIma;
@property(nonatomic,strong) UIButton     * smallImaBtn;
@property(nonatomic,strong) UIButton     * shareBtn;
@property(nonatomic,strong) UILabel      * smallQuanLabel;
@property(nonatomic,strong) UILabel      * quanLabel;
@property(nonatomic,strong) UIButton     * playVideoBtn ;  // 播放视频
@property(nonatomic,strong) GoodsModel   * goodsModel ;
@property(nonatomic,strong) UILabel      * fenXiangZhuan ;
//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end


/**********************************************************************************/

#pragma mark ===>>> 第十种样式 《方块、惠得，代理商和消费者有区别》

@interface GoodsListCltCell10 : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     // 商品图片
@property(nonatomic,strong) UIButton     * couponsLab ;    // 优惠券
@property(nonatomic,strong) UILabel      * goodsTitle ;    // 商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   // 券后价
@property(nonatomic,strong) UILabel      * endCouponsLab ; // 券后
@property(nonatomic,strong) UILabel      * salesLab ;      // 销量
@property(nonatomic,strong) UILabel      * fenXiangZhuan;  // 分享赚
@property(nonatomic,strong) UILabel      * fenXiangZhuanText; // 分享赚
@property(nonatomic,strong) UIButton     * youHuiQuan;     // 优惠券
@property(nonatomic,strong) UIImageView  * icon;           // 商品图片
@property(nonatomic,strong) UILabel      * pointNum;
@property(nonatomic,strong) UILabel      * catPLab;        // 天猫价
@property(nonatomic,strong) UIView       * backView;
@property(nonatomic,strong) UIImageView  * platformImgV;   // 商品来源图片
@property(nonatomic,strong) UIButton     * playVideoBtn;   // 播放视频
@property(nonatomic,strong) GoodsModel   * goodsModel;

//*** 商品多选有关 ***//
@property(nonatomic,strong) UIButton    * selectBtn;     // 选择图片按钮
@property(nonatomic,assign) BOOL          isStartSelect; // 0:取消选择  1:开始选择
@property(nonatomic,strong) NSMutableDictionary * isSltGoodsidDic; //已选的商品id字典
typedef void (^SelectGoodsBlock) (GoodsModel * goodsModel , BOOL ifSelect);
@property (copy, nonatomic) SelectGoodsBlock goodsBlock ;

@end



