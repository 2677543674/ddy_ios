//
//  GoodsListCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define LEFTWIDTH   ([UIScreen mainScreen].bounds.size.width-16)/4

#import "GoodsListCltCell1.h"
#import "AVPalyerVC.h"
#import "UIView+CornerRadius.h"
#import "CreateShareImageView6.h"


/*************************************************************************************************************/

#pragma mark ===>>> 第一种样式 《列表、默认样式、第一版样式》

@implementation GoodsListCltCell1

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}

-(void)setGoodsModel:(GoodsModel *)goodsModel{
    _goodsModel = goodsModel ;
//    dispatch_async(dispatch_get_main_queue(), ^{
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    
    _goodsTitleLab.text = FORMATSTR(@"      %@",_goodsModel.goodsName);
    if ([_goodsModel.platform containsString:@"天猫"]) {
        _platformImgV.image = [UIImage imageNamed:@"tmallicon"];
    }else if([_goodsModel.platform containsString:@"2"]){
        _platformImgV.image = [UIImage imageNamed:@"pddicon"];
    }else{
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
    }
    
    _catPLab.text = FORMATSTR(@"原价: %@元",_goodsModel.price) ;
    
    _salesLab.text = FORMATSTR(@"月销%@",_goodsModel.sales) ;
    
    
    if ([_goodsModel.couponMoney floatValue]>0){ // 有券显示券后价
       _vouchersMoneyLab.text = FORMATSTR(@"%@元",_goodsModel.endPrice) ;
    }else{ //无券显示原价
       _vouchersMoneyLab.text = FORMATSTR(@"%@元",_goodsModel.price);
    }
    
    
// 先判断列表页显示状态(分享赚、下单省)   再分别判断有券无券，修改UI和显示效果
    
    if ( LISTBTNTYPE == 2) {
        if (role_state==0) {
           
            if ([_goodsModel.couponMoney floatValue]>0) { // 消费者有券
                _orderLab.text = @"下单立省" ;
                [_takeOrderBtn setTitle:FORMATSTR(@"¥%@",_goodsModel.couponMoney) forState:UIControlStateNormal];
                _takeOrderBtn.hidden = NO ;
                [_orderLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(_rightView.mas_centerX);
                    make.bottom.equalTo(_rightView.mas_centerY).offset(-10);

                }];
             }else{    // 消费者无券
                _orderLab.text = @"立即下单" ;
                _takeOrderBtn.hidden = YES ;
                [_orderLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(_rightView.mas_centerX);
                    make.centerY.equalTo(_rightView.mas_centerY);
                }];
            }
            
        } else { // 代理商显示分享赚
            _takeOrderBtn.hidden = NO ;
            _orderLab.text = @"分享赚" ;
            [_takeOrderBtn setTitle:FORMATSTR(@"¥%@",_goodsModel.commission) forState:UIControlStateNormal];
            [_orderLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_rightView.mas_centerX);
                make.bottom.equalTo(_rightView.mas_centerY).offset(-10);
            }];
        }
    }
    if (LISTBTNTYPE==1) { // 某些app方要求，无论是何种身份状态，都显示下单
        [_takeOrderBtn setTitle:FORMATSTR(@"¥%@",_goodsModel.couponMoney) forState:UIControlStateNormal];
    }
    
    if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {

        } @finally { }

    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }


}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORGROUP ;

        _cellH = FloatKeepInt(100*HWB) - 5 ;
        [self leftView];
        [self rightView];
        
        [self goodsImgV];
        
        [self goodsTitleLab];
        [self platformImgV];
        [self catPLab];
        [self salesLab];
        [self vouchersPLab];
        [self vouchersMoneyLab];
//        [self baoyouLab];
        
        [self orderLab];
        [self takeOrderBtn];
        [self playVideoBtn];

        [self selectBtn];
        if (THEMECOLOR==2) {
            _vouchersMoneyLab.textColor = OtherColor ;
            _baoyouLab.textColor = Black51 ;
            _orderLab.textColor = Black51 ;
            [_takeOrderBtn setTitleColor:Black51 forState:UIControlStateNormal];
            _baoyouLab.textColor = Black51 ;
        }
    }
    return self ;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

/** 左侧底部View **/
-(UIView*)leftView{
    if (!_leftView) {
        _leftView = [[UIView alloc]init];
        _leftView.backgroundColor = COLORWHITE ;
        _leftView.frame = CGRectMake(5, 5, LEFTWIDTH*3+3, _cellH);
        [self.contentView addSubview:_leftView];
        
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        [shapeLayer setBounds:_leftView.bounds];
        [shapeLayer setPosition:_leftView.center];
        [shapeLayer setFillColor:[UIColor clearColor].CGColor];
        [shapeLayer setStrokeColor:COLORGROUP.CGColor];
        [shapeLayer setLineWidth:6];
        [shapeLayer setLineJoin:kCALineJoinRound];
        [shapeLayer setLineCap:kCALineCapButt];
        [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:@5, @5, nil]];
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL,-4,-2);
        CGPathAddLineToPoint(path, NULL,-4,_cellH-6);
        [shapeLayer setPath:path]; CGPathRelease(path);
        [_leftView.layer addSublayer:shapeLayer];
        
    }
    return _leftView;
}

/** 右侧View **/
-(UIView*)rightView{
    if (!_rightView) {
        _rightView = [[UIView alloc]init];
        _rightView.backgroundColor = LOGOCOLOR ;
        _rightView.frame = CGRectMake(8+LEFTWIDTH*3-1, 5, LEFTWIDTH+1 , _cellH);
        UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takeOrder)];
        [_rightView  addGestureRecognizer:tap];
        [self.contentView addSubview:_rightView];
        [_rightView cornerSideType:kLQQSideTypeRight withCornerRadius:10.f];
    }
    return _rightView;
}

/** 商品图片 **/
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.backgroundColor = COLORWHITE;
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _goodsImgV.clipsToBounds = YES ;
        _goodsImgV.frame = CGRectMake(6, 8*HWB, _cellH-16*HWB, _cellH-16*HWB);
        _goodsImgV.layer.masksToBounds = YES ;
        _goodsImgV.layer.cornerRadius = 3*HWB ;
        [_leftView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}

/** 商品标题 **/
-(UILabel*)goodsTitleLab{
    if (!_goodsTitleLab) {
        _goodsTitleLab = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        _goodsTitleLab.numberOfLines = 2 ;
        [_leftView addSubview:_goodsTitleLab];
        [_goodsTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.offset(11*HWB);  make.right.offset(-5);
        }];
    }
    return _goodsTitleLab;
}

/** 商品来源图片 **/
-(UIImageView*)platformImgV{
    if (!_platformImgV) {
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
        [_leftView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.offset(11*HWB); make.height.offset(14*HWB);
            make.width.offset(14*HWB);
        }];
    }
    return _platformImgV ;
}

/** 天猫价 **/
-(UILabel*)catPLab{
    if (!_catPLab) {
        _catPLab = [UILabel labText:@"原价:00.00元" color:Black102 font:10*HWB];
        [_leftView addSubview:_catPLab];
        [_catPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.equalTo(_leftView.mas_centerY).offset(3);
            make.height.offset(14);
        }];
    }
    return _catPLab;
}

/** 销量 **/
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"月销0" color:Black102 font:10*HWB];
        [_leftView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-6);  make.height.offset(14);
            make.centerY.equalTo(_catPLab.mas_centerY);
        }];
    }
    return _salesLab;
}

/** 券后价 **/
-(UILabel*)vouchersPLab{
    if (!_vouchersPLab) {
        _vouchersPLab = [UILabel labText:@"券后价: " color:Black51 font:12*HWB];
        [_leftView addSubview:_vouchersPLab];
        [_vouchersPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.bottom.offset(-10*HWB);
        }];
    }
    return _vouchersPLab;
}

/** 券后价价格 **/
-(UILabel*)vouchersMoneyLab{
    if (!_vouchersMoneyLab) {
        _vouchersMoneyLab = [UILabel labText:@"00.00元" color:LOGOCOLOR font:14*HWB];
        [_leftView addSubview:_vouchersMoneyLab];
        [_vouchersMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersPLab.mas_right);
            make.centerY.equalTo(_vouchersPLab.mas_centerY);
        }];
    }
    return _vouchersMoneyLab;
}

/** 包邮 **/
-(UILabel*)baoyouLab{
    if (!_baoyouLab) {
        _baoyouLab = [UILabel labText:@"包邮" color:COLORWHITE font:10*HWB];
        _baoyouLab.backgroundColor = LOGOCOLOR;
        _baoyouLab.textAlignment = NSTextAlignmentCenter ;
        _baoyouLab.layer.masksToBounds = YES ;
        _baoyouLab.layer.cornerRadius = 8;
        _baoyouLab.layer.shouldRasterize = YES;
        _baoyouLab.layer.rasterizationScale = [UIScreen mainScreen].scale;
        [_leftView addSubview:_baoyouLab];
        [_baoyouLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersMoneyLab.mas_right).offset(3);
            make.height.offset(16);
            make.width.offset(33);
            make.centerY.equalTo(_vouchersPLab.mas_centerY);
        }];
    }
    return _baoyouLab;
    
}

/**下单立省 **/
-(UILabel*)orderLab{
    if (!_orderLab) {
        _orderLab = [UILabel labText:@"下单立省" color:COLORWHITE font:12*HWB];
        [_rightView addSubview:_orderLab];
        [_orderLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_rightView.mas_centerX);
            make.bottom.equalTo(_rightView.mas_centerY).offset(-10);
        }];
    }
    return _orderLab;
}

/** 按钮，显示价格 **/
-(UIButton*)takeOrderBtn{
    if (!_takeOrderBtn) {
        _takeOrderBtn = [UIButton titLe:@"¥0" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:13*HWB];
        _takeOrderBtn.layer.cornerRadius = 16 ;
        ADDTARGETBUTTON(_takeOrderBtn, takeOrder)
        [_rightView addSubview:_takeOrderBtn];
        [_takeOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_rightView.mas_centerY).offset(-2);
            make.height.offset(32);
            make.width.offset(68*HWB);
            make.centerX.equalTo(_rightView.mas_centerX);
        }];
    }
    return _takeOrderBtn;
}

-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) { // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}

-(void)takeOrder{
    
    if (role_state==0) { [PageJump takeOrder:_goodsModel]; }

    else{
        if (LISTBTNTYPE==1) { [PageJump takeOrder:_goodsModel]; }
        else {[PageJump shareGoods:_goodsModel goodsImgAry:nil]; }
    }
    
}

-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}
#pragma mark ==>> 按钮相应事件
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
//        if ([_isSltGoodsidDic allKeys].count<9) {
//            _goodsModel.isSelect = YES;
//            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
//            [UIView animateWithDuration:0.26 animations:^{
//                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
//            }];
//            self.goodsBlock(_goodsModel, YES);
//        }else{
//            SHOWMSG(@"一次最多只能分享九个商品哦~")
//        }
    }
}

@end


/*************************************************************************************************************/

#pragma mark ===>>> 第二种样式 《列表、标题加粗》

@implementation GoodsListCltCell2

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    

    _goodsTitleLab.text = FORMATSTR(@"      %@",_goodsModel.goodsName);
    if ([_goodsModel.platform containsString:@"天猫"]) {
        _platformImgV.image = [UIImage imageNamed:@"tmallicon"];
    }else if([_goodsModel.platform containsString:@"2"]){
        _platformImgV.image = [UIImage imageNamed:@"pddicon"];
    }else{
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
    }
    
    if (goodsModel.video.length==0) {
        _playVideoBtn.hidden=YES;
    }else{
        _playVideoBtn.hidden=NO;
    }
    
    _catPLab.text = FORMATSTR(@"原价: %@元",_goodsModel.price) ;
    
    _salesLab.text = FORMATSTR(@"月销%@",_goodsModel.sales) ;
    
    if ([_goodsModel.couponMoney floatValue]>0){ // 有券显示券后价
        _vouchersMoneyLab.text = FORMATSTR(@"%@元",_goodsModel.endPrice) ;
    }else{ //无券显示原价
        _vouchersMoneyLab.text = FORMATSTR(@"%@元",_goodsModel.price);
    }
    
    
    if ( LISTBTNTYPE == 2) {
        if (role_state==0) {
            if ([_goodsModel.couponMoney floatValue]>0) {
                _orderLab.text = @"下单立省" ;
                [_takeOrderBtn setTitle:FORMATSTR(@"%@元券",_goodsModel.couponMoney) forState:UIControlStateNormal];
                _takeOrderBtn.hidden = NO ;
                [_orderLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);  make.left.offset(0);
                    make.right.offset(0); make.bottom.equalTo(_rightView.mas_centerY);
                }];
            }else{  // 无券
                _orderLab.text = @"立即下单" ;
                _takeOrderBtn.hidden = YES ;
                [_orderLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);  make.bottom.offset(0);
                    make.left.offset(0);  make.right.offset(0);
                }];
            }
        } else {
            _orderLab.text = @"分享赚" ;
            [_takeOrderBtn setTitle:FORMATSTR(@"¥%@",_goodsModel.commission) forState:UIControlStateNormal];
            _takeOrderBtn.hidden = NO ;
            [_orderLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.offset(0);  make.left.offset(0);
                make.right.offset(0); make.bottom.equalTo(_rightView.mas_centerY);
            }];
        }
    }
    if (LISTBTNTYPE==1) {
        [_takeOrderBtn setTitle:FORMATSTR(@"¥%@",_goodsModel.couponMoney) forState:UIControlStateNormal];
    }
    if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {

        } @finally { }

    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _cellH = FloatKeepInt(100*HWB)-3;
//        if (ScreenH==480) { _cellH = ScreenH/5-6;}
        
        [self leftView];

        [self goodsImgV];
        [self goodsTitleLab];
        [self platformImgV];
        [self catPLab];
        [self salesLab];
        
        [self vouchersPLab];
        [self vouchersMoneyLab];
        
        [self rightView];
        [self orderLab];
        [self takeOrderBtn];
        [self playVideoBtn];
        [self selectBtn];
    }
    return self ;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

-(UIView*)leftView{
    if (!_leftView) {
        _leftView = [[UIView alloc]init];
        _leftView.backgroundColor = COLORWHITE ;
        _leftView.frame = CGRectMake(0, 1, ScreenW , _cellH);
        [self.contentView addSubview:_leftView];
    }
    return _leftView;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init] ;
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.backgroundColor = COLORGROUP ;
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _goodsImgV.clipsToBounds = YES ;
        _goodsImgV.frame = CGRectMake(6, 10*HWB, _cellH-20*HWB, _cellH-20*HWB);
        _goodsImgV.layer.masksToBounds = YES ;
        _goodsImgV.layer.cornerRadius = 3*HWB ;
        [_leftView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}

-(UILabel*)goodsTitleLab{
    if (!_goodsTitleLab) {
        _goodsTitleLab = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        _goodsTitleLab.font = [UIFont boldSystemFontOfSize:13*HWB];
        [_leftView addSubview:_goodsTitleLab];
        [_goodsTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.offset(10*HWB);  make.right.offset(-5);
        }];
    }
    return _goodsTitleLab;
}
/** 商品来源图片 **/
-(UIImageView*)platformImgV{
    if (!_platformImgV) {
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
        [_leftView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.offset(10*HWB); make.height.offset(15*HWB);
            make.width.offset(15*HWB);
        }];
    }
    return _platformImgV ;
}
-(UILabel*)catPLab{
    if (!_catPLab) {
        _catPLab = [UILabel labText:@"原价:00.00元" color:Black102 font:12*HWB];
        [_leftView addSubview:_catPLab];
        [_catPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.bottom.equalTo(_leftView.mas_centerY).offset(-3*HWB);
            make.height.offset(16);
        }];
    }
    return _catPLab;
}

-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"月销0" color:Black102 font:12*HWB];
        [_leftView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-20);
            make.centerY.equalTo(_catPLab.mas_centerY);
        }];
    }
    return _salesLab;
}

-(UILabel*)vouchersPLab{
    if (!_vouchersPLab) {
        _vouchersPLab = [UILabel labText:@"券后价:" color:Black51 font:12*HWB];
        [_leftView addSubview:_vouchersPLab];
        [_vouchersPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.equalTo(_leftView.mas_centerY).offset(3*HWB);
        }];
    }
    return _vouchersPLab;
}

-(UILabel*)vouchersMoneyLab{
    if (!_vouchersMoneyLab) {
        _vouchersMoneyLab = [UILabel labText:@"¥ 00.00元" color:LOGOCOLOR font:15*HWB];
        [_leftView addSubview:_vouchersMoneyLab];
        [_vouchersMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.bottom.offset(-8*HWB);
        }];
    }
    return _vouchersMoneyLab;
}

-(UIView*)rightView{
    if (!_rightView) {
        _rightView = [[UIView alloc]init];
        _rightView.backgroundColor = LOGOCOLOR ;
        [_leftView addSubview:_rightView];
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_leftView.mas_centerY).offset(3*HWB);
            make.bottom.offset(-8*HWB); make.right.offset(-12);
            make.width.offset(66*HWB);
        }];
        
//        [_rightView cornerSideType:kLQQSideTypeAll withCornerRadius:6*HWB];
        _rightView.layer.masksToBounds = YES ;
        _rightView.layer.shouldRasterize = YES;
        _rightView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        _rightView.layer.cornerRadius = 8*HWB ;
        _rightView.layer.borderWidth = 1 ;
        _rightView.layer.borderColor = LOGOCOLOR.CGColor ;
        
        UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takeOrder)];
        [_rightView  addGestureRecognizer:tap];
    }
    return _rightView;
}

-(UILabel*)orderLab{
    if (!_orderLab) {
        _orderLab = [UILabel labText:@"领券购买" color:COLORWHITE font:12*HWB];
        _orderLab.backgroundColor = LOGOCOLOR ;
        _orderLab.textAlignment = NSTextAlignmentCenter ;
        [_rightView addSubview:_orderLab];
        [_orderLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.left.offset(0);
            make.right.offset(0); make.bottom.equalTo(_rightView.mas_centerY);
        }];
    }
    return _orderLab;
}

-(UIButton*)takeOrderBtn{
    if (!_takeOrderBtn) {
        _takeOrderBtn = [UIButton titLe:@"¥0" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:12*HWB];
        ADDTARGETBUTTON(_takeOrderBtn, takeOrder)
        [_rightView addSubview:_takeOrderBtn];
        [_takeOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_rightView.mas_centerY);
            make.bottom.offset(0);
            make.left.offset(0);
            make.right.offset(0);
        }];
    }
    return _takeOrderBtn;
}

-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) {  // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}

-(void)takeOrder{
    if (role_state==0) { [PageJump takeOrder:_goodsModel]; }
    
    else{
        if (LISTBTNTYPE==1) { [PageJump takeOrder:_goodsModel]; }
        else {[PageJump shareGoods:_goodsModel goodsImgAry:nil]; }
    }
}

-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}

// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
//        if ([_isSltGoodsidDic allKeys].count<9) {
//            _goodsModel.isSelect = YES;
//            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
//            [UIView animateWithDuration:0.26 animations:^{
//                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
//            }];
//            self.goodsBlock(_goodsModel, YES);
//        }else{
//            SHOWMSG(@"一次最多只能分享九个商品哦~")
//        }
    }
}
@end





/*************************************************************************************************************/

#pragma mark ===>>> 第三种样式 《方块》

@implementation GoodsListCltCell3

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;

    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    _goodsTitle.text = _goodsModel.goodsName ;
    _endPriceLab.text = FORMATSTR(@"%@",_goodsModel.endPrice) ;
    _salesLab.text = FORMATSTR(@"月销%@",_goodsModel.sales) ;
    
    if ([_goodsModel.couponMoney floatValue]>0){ // 有券显示券后价
        _endPriceLab.text = FORMATSTR(@"%@",_goodsModel.endPrice) ;
    }else{ //无券显示原价
        _endPriceLab.text = FORMATSTR(@"%@",_goodsModel.price);
    }
    if (goodsModel.video.length==0) {
        _playVideoBtn.hidden=YES;
    }else{
        _playVideoBtn.hidden=NO;
    }
    if (role_state!=0) {
        [_couponsLab setTitle:FORMATSTR(@"分享赚 ¥%@",_goodsModel.commission) forState:UIControlStateNormal];
    }else{
        [_couponsLab setTitle:FORMATSTR(@"优惠券 ¥%@",_goodsModel.couponMoney) forState:UIControlStateNormal];
        if ([_goodsModel.couponMoney floatValue]==0) {
            [_couponsLab setTitle:@"立即下单" forState:UIControlStateNormal];
        }
    }
    
        if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {

        } @finally { }

    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }

}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
         [self goodsTitle];
        
        [self couponsLab];
        
        [self endCouponsLab];
        [self endPriceLab];
        
        [self salesLab];
        [self playVideoBtn];
        [self selectBtn];
        if (THEMECOLOR==2) {
            [_couponsLab setTitleColor:Black51 forState:(UIControlStateNormal)];
            _endPriceLab.textColor = OtherColor ;
        }
        
    }
    return self;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.frame = CGRectMake(0, 0, ScreenW/2-2, ScreenW/2-2);
//        _goodsImgV.image = [UIImage imageNamed:@"logo"];
        [self.contentView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}

//商品标题
-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
//        _goodsTitle.textAlignment =NSTextAlignmentCenter ;
        _goodsTitle.frame = CGRectMake(3,ScreenW/2+1 ,ScreenW/2-8, 22*HWB );
        [self.contentView addSubview:_goodsTitle];
    }
    return _goodsTitle ;
}

//优惠券
-(UIButton*)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (PID==10022) {
            [_couponsLab setBackgroundImage:[UIImage imageNamed:@"gd_coupons_bg"] forState:UIControlStateNormal];
        }else{
            _couponsLab.tintColor = LOGOCOLOR ;
            [_couponsLab setBackgroundImage:[[UIImage imageNamed:@"gd_coupons_bg"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)] forState:UIControlStateNormal];
        }
        [_couponsLab setTitle:@"优惠券 ¥00" forState:UIControlStateNormal];
        [_couponsLab setTitleColor:COLORWHITE forState:UIControlStateNormal];
        _couponsLab.titleLabel.font = [UIFont systemFontOfSize:12*HWB];
        _couponsLab.frame = CGRectMake(5, ScreenW/2+25*HWB , 90*HWB, 20*HWB);
        _couponsLab.titleLabel.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_couponsLab];
        ADDTARGETBUTTON(_couponsLab, takeOrder)
    }
    return _couponsLab ;
}

//券后
-(UILabel*)endCouponsLab{
    if (!_endCouponsLab) {
        _endCouponsLab = [UILabel labText:@"券后价:" color:Black102 font:12*HWB];
        [self.contentView addSubview:_endCouponsLab];
        [_endCouponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5);
            make.top.equalTo(_couponsLab.mas_bottom).offset(6*HWB);
        }];
    }
    return _endCouponsLab ;
}

//券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"00.00" color:LOGOCOLOR font:13.5*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endCouponsLab.mas_right).offset(1);
            make.centerY.equalTo(_endCouponsLab.mas_centerY);
//            make.bottom.equalTo(_endCouponsLab.mas_bottom).offset(2);
        }];
        UILabel * yuanLab = [UILabel labText:@"元" color:LOGOCOLOR font:12*HWB];
        [self.contentView addSubview:yuanLab];
        [yuanLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(1);
            make.centerY.equalTo(_endCouponsLab.mas_centerY);
        }];
        
    }
    return _endPriceLab;
}

//销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"月销00" color:Black102 font:12*HWB];
        _salesLab.textAlignment = NSTextAlignmentRight;
        _salesLab.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5);
            make.centerY.equalTo(_endCouponsLab.mas_centerY);
        }];
    }
    return _salesLab ;
}

-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) { // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}

-(void)takeOrder{
    if (role_state==0) { [PageJump takeOrder:_goodsModel]; }
    
    else{
        if (LISTBTNTYPE==1) { [PageJump takeOrder:_goodsModel]; }
        else {[PageJump shareGoods:_goodsModel goodsImgAry:nil]; }
    }
}

-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
//        if ([_isSltGoodsidDic allKeys].count<9) {
//            _goodsModel.isSelect = YES;
//            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
//            [UIView animateWithDuration:0.26 animations:^{
//                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
//            }];
//            self.goodsBlock(_goodsModel, YES);
//        }else{
//            SHOWMSG(@"一次最多只能分享九个商品哦~")
//        }
    }
}

@end




/*************************************************************************************************************/

#pragma mark ===>>> 第四种样式 《方块、花生日记的最先上的商品列表》

@implementation GoodsListCltCell4

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    
    _goodsTitleLab.text = FORMATSTR(@"      %@",_goodsModel.goodsName);
    if ([_goodsModel.platform containsString:@"天猫"]) {
        _platformImgV.image = [UIImage imageNamed:@"tmallicon"];
    }else if([_goodsModel.platform containsString:@"2"]){
        _platformImgV.image = [UIImage imageNamed:@"pddicon"];
    }else{
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
    }
    if (goodsModel.video.length==0) {
        _playVideoBtn.hidden=YES;
    }else{
        _playVideoBtn.hidden=NO;
    }
    // 券后价
    NSRange rg =  {1,ROUNDED(_goodsModel.endPrice, 0).length} ;
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_goodsModel.endPrice)];
    [dlStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16*HWB]} range:rg];
    _endPriceLab.attributedText = dlStr ;
    
    // 原价(天猫价)
    NSRange titleRange = {0,_goodsModel.price.length+1};
    NSMutableAttributedString * mtitle = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_goodsModel.price)];
    [mtitle addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    _catPLab.attributedText = mtitle ;
    
    _salesLab.text = FORMATSTR(@"已购%@",_goodsModel.sales) ;
    
    
    
    

    [_couponsBtn setTitle:_goodsModel.couponMoney  forState:(UIControlStateNormal)];

    
    if (role_state!=0) {  // 代理显示分享赚
        _shareBtn.hidden = NO;
        NSRange sharerg1 = {5,_goodsModel.commission.length};
        NSMutableAttributedString * dlStr21 = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"分享赚:￥%@",_goodsModel.commission)];
        [dlStr21 addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15*HWB]} range:sharerg1];
        [_shareBtn setAttributedTitle:dlStr21 forState:(UIControlStateNormal)];
    }else{  // 消费者隐藏
        _shareBtn.hidden = YES ;
    }
    
    if ([_goodsModel.couponMoney floatValue]>0) { // 有券
        _couponsBtn.hidden = NO ;    _catPLab.hidden = NO ;
    }else{   //无券(可能优惠券失效，或无券)
        _couponsBtn.hidden = YES ;   _catPLab.hidden = YES ;
        
        // 券后价显示成原价
        NSRange rg =  {1,ROUNDED(_goodsModel.price, 0).length} ;
        NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_goodsModel.price)];
        [dlStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16*HWB]} range:rg];
        _endPriceLab.attributedText = dlStr ;
    }
    
    // 有视频单和无视频单
    if (_goodsModel.video.length>0&&[_goodsModel.video hasPrefix:@"http"]) {
        _playVideoBtn.hidden = NO ;
    }else{
        _playVideoBtn.hidden = YES ;
    }
    
        if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {

        } @finally { }

    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }

}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
        [self playVideoBtn];
        [self goodsTitle];
        [self platformImgV];
        [self endPriceLab];
        [self catPLab];
        [self salesLab];
        
        [self couponsBtn];
        [self shareBtn];
        [self selectBtn];
        if (THEMECOLOR==2) {
            _endPriceLab.textColor = OtherColor ;
            [_couponsBtn setTitleColor:OtherColor forState:(UIControlStateNormal)];
            [_shareBtn setTitleColor:OtherColor forState:(UIControlStateNormal)];
        }
    }
    return self;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

/** 商品来源图片 **/
-(UIImageView*)platformImgV{
    if (!_platformImgV) {
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
        [self.contentView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsTitleLab.mas_left);
            make.top.equalTo(_goodsTitleLab.mas_top).offset(3*HWB);
            make.height.offset(14*HWB);
            make.width.offset(14*HWB);
        }];
    }
    return _platformImgV ;
}

//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.frame = CGRectMake(0, 0, ScreenW/2-2, ScreenW/2-2);
        [self.contentView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}
-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) { // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}
//商品标题
-(UILabel*)goodsTitle{
    if (!_goodsTitleLab) {
        _goodsTitleLab = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        _goodsTitleLab.numberOfLines = 2 ;
        _goodsTitleLab.frame = CGRectMake(8*HWB,ScreenW/2+1 ,ScreenW/2-16*HWB-4, 36*HWB );
        [self.contentView addSubview:_goodsTitleLab];
    }
    return _goodsTitleLab ;
}


//券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"00.00元" color:LOGOCOLOR font:11*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsTitleLab.mas_bottom).offset(5);
            make.left.offset(6*HWB) ;
        }];
    }
    return _endPriceLab;
}

// 天猫价
-(UILabel*)catPLab{
    if (!_catPLab) {
        _catPLab = [UILabel labText:@"￥0" color:Black102 font:11*HWB];
        [self.contentView addSubview:_catPLab];
        [_catPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(8*HWB);
            make.bottom.equalTo(_endPriceLab.mas_bottom).offset(-2);
        }];
    }
    return _catPLab ;
    
}

//销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"月销00" color:Black102 font:11*HWB];
        _salesLab.textAlignment = NSTextAlignmentRight;
        _salesLab.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-6*HWB); make.centerY.equalTo(_endPriceLab.mas_centerY);
        }];
    }
    return _salesLab ;
}

//优惠券
-(UIButton*)couponsBtn{
    if (!_couponsBtn) {
        _couponsBtn = [[UIButton alloc]init];
        UIImage*oldImg=[UIImage imageNamed:@"goods_list4_quan"];
        _couponsBtn.tintColor = LOGOCOLOR ;
        oldImg = [UIImage imageNamed:@"券"];
        if (THEMECOLOR==2) { //主题色为浅色，不开启渲染
            [_couponsBtn setBackgroundImage:oldImg forState:UIControlStateNormal];
        }else{

            [_couponsBtn setBackgroundImage:oldImg forState:UIControlStateNormal];
        }
        
        [_couponsBtn setTitle:@"0元" forState:UIControlStateNormal];
        [_couponsBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        _couponsBtn.titleLabel.font = [UIFont systemFontOfSize:11.5*HWB];
        _couponsBtn.titleLabel.adjustsFontSizeToFitWidth = YES ;
        [_couponsBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 16*HWB, 0, 0)];
        [self.contentView addSubview:_couponsBtn];
        ADDTARGETBUTTON(_couponsBtn, takeOrder)
        [_couponsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(8*HWB); make.bottom.offset(-8*HWB);
            make.height.offset(15*HWB); make.width.offset(52*HWB);
        }];
    }
    return _couponsBtn ;
}


-(UIButton*)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [[UIButton alloc]init];
        [_shareBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        [_shareBtn setTitle:@"分享赚:" forState:(UIControlStateNormal)];
        _shareBtn.titleLabel.font = [UIFont systemFontOfSize:10.0*HWB];
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-6*HWB);
            make.bottom.offset(-2*HWB);
        }];
        ADDTARGETBUTTON(_shareBtn, takeOrder)
    }
    return _shareBtn ;
}


#pragma mark ==>> 播放视频
-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}

// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
//        if ([_isSltGoodsidDic allKeys].count<9) {
//            _goodsModel.isSelect = YES;
//            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
//            [UIView animateWithDuration:0.26 animations:^{
//                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
//            }];
//            self.goodsBlock(_goodsModel, YES);
//        }else{
//            SHOWMSG(@"一次最多只能分享九个商品哦~")
//        }
    }
}

-(void)takeOrder{
    if (role_state==0) { [PageJump takeOrder:_goodsModel]; }
    else{  [PageJump shareGoods:_goodsModel goodsImgAry:nil]; }
}

@end



/*************************************************************************************************************/

#pragma mark ===>>> 第五种样式 《方块、代理商在下方多出高度显示分享赚》

@implementation GoodsListCltCell5  //return  CGSizeMake(ScreenW/2-2,ScreenW/2 + 90*HWB？70*HWB);代理商 消费者

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] placeholderImage:PlaceholderImg] ;
        }
    }];
   
//    _goodsTitle.text = [NSString stringWithFormat:@"%@",_goodsModel.goodsName] ;
    
    _goodsTitle.text = FORMATSTR(@"      %@",_goodsModel.goodsName);
    if ([_goodsModel.platform containsString:@"天猫"]) {
        _platformImgV.image = [UIImage imageNamed:@"tmallicon"];
    }else if([_goodsModel.platform containsString:@"2"]){
        _platformImgV.image = [UIImage imageNamed:@"pddicon"];
    }else{
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
    }
    if (goodsModel.video.length==0) {
        _playVideoBtn.hidden=YES;
    }else{
        _playVideoBtn.hidden=NO;
    }
    _catPLab.text = [NSString stringWithFormat:@"¥%@",_goodsModel.price];
    _salesLab.text = FORMATSTR(@"销量%@件",_goodsModel.sales) ;
    _endPriceLab.text = FORMATSTR(@"%@",_goodsModel.endPrice);
    
    [_youHuiQuan setTitle:FORMATSTR(@"¥%@",_goodsModel.couponMoney) forState:UIControlStateNormal];
    if ([_goodsModel.couponMoney floatValue]>0){
        _backView.hidden=NO;
        
    }else{ //无券
        UIView * line=[self viewWithTag:555];
        line.hidden=YES;
        _backView.hidden=YES;
        _catPLab.hidden=YES;
        [_salesLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5*HWB);
            make.top.equalTo(_goodsTitle.mas_bottom).offset(5);
        }];
    }
    
    UIView* view = [self viewWithTag:666];
    if (role_state==0) {
        view.hidden=YES;
        
    }else{
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"分享赚￥%@",goodsModel.commission)];
        [str addAttribute:NSForegroundColorAttributeName value:LOGOCOLOR range:NSMakeRange(3,str.length-3)];

        _fenXiangZhuan.attributedText = str;
        view.hidden=NO;
    }
    
        if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {

        } @finally { }

    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }
    
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
        [self goodsTitle];
        [self platformImgV];
        [self salesLab];
        [self youHuiQuan];
        [self endPriceLab];
        [self catPLab];
        [self fenXiangZhuan];
        [self playVideoBtn];
        [self selectBtn];
    }
    return self;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

/** 商品来源图片 **/
-(UIImageView*)platformImgV{
    if (!_platformImgV) {
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
        [self.contentView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsTitle.mas_left);
            make.centerY.equalTo(_goodsTitle.mas_centerY);
            make.height.offset(15*HWB);
            make.width.offset(15*HWB);
        }];
    }
    return _platformImgV ;
}

//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.frame = CGRectMake(0, 0, ScreenW/2-2, ScreenW/2-2);
        _goodsImgV.image = [UIImage imageNamed:@"logo"];
        [self.contentView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}

-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        _goodsTitle.numberOfLines=2;
        _goodsTitle.textAlignment =NSTextAlignmentLeft ;
        _goodsTitle.frame = CGRectMake(5*HWB,ScreenW/2+1 ,ScreenW/2-8, 21*HWB );
        [self.contentView addSubview:_goodsTitle];
        
    }
    return _goodsTitle ;
}

//销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"已售00" color:Black102 font:11*HWB];
        _salesLab.textAlignment = NSTextAlignmentRight;
        _salesLab.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5);
            make.top.equalTo(_goodsTitle.mas_bottom).offset(5);
            
        }];
        
    }
    return _salesLab ;
}

//优惠券
-(UIButton *)youHuiQuan{
    if (!_youHuiQuan) {
        _backView = [UIView new];
        _backView.backgroundColor = COLORWHITE ;
        _backView.layer.borderWidth = 1;
        _backView.layer.borderColor = LOGOCOLOR.CGColor;
        _backView.layer.cornerRadius = 4;
        _backView.clipsToBounds = YES;
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_salesLab.mas_centerY);
            make.width.offset(60*HWB);
            make.height.offset(16*HWB);
            make.left.offset(5*HWB);
        }];
        
        UILabel * quan = [UILabel labText:@"券" color:COLORWHITE font:11*HWB];

        if (THEMECOLOR==2) {
            quan = [UILabel labText:@"券" color:Black51 font:11*HWB];
        }
        quan.textAlignment=NSTextAlignmentCenter;
        quan.backgroundColor=LOGOCOLOR;
        [_backView addSubview:quan];
        [quan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView.mas_centerY);
            make.width.offset(22*HWB);
            make.height.offset(16*HWB);
            make.left.offset(0);
        }];
        
        _youHuiQuan = [UIButton buttonWithType:UIButtonTypeCustom];
        [_youHuiQuan setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        if (THEMECOLOR==2) {
             [_youHuiQuan setTitleColor:Black51 forState:UIControlStateNormal];
        }
        [_youHuiQuan setBackgroundColor:[UIColor whiteColor]];
        _youHuiQuan.titleLabel.font = [UIFont systemFontOfSize:11*HWB];
        [_backView addSubview:_youHuiQuan];
        [_youHuiQuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(quan.mas_right).offset(0);
            make.centerY.equalTo(_backView.mas_centerY);
            make.width.offset(36*HWB);
            make.height.offset(16*HWB);
        }];
        
        UIButton* takeOrderBtn=[UIButton new];
        [takeOrderBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        [_backView addSubview:takeOrderBtn];
        [takeOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
    }
    return _youHuiQuan;
}

//券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        UILabel* lab = [UILabel labText:@"￥" color:LOGOCOLOR font:10*HWB];
        [self.contentView addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5*HWB);
            make.top.equalTo(_youHuiQuan.mas_bottom).offset(8*HWB);
        }];
        
        _endPriceLab = [UILabel labText:@"00" color:LOGOCOLOR font:15*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lab.mas_right).offset(2);
            make.top.equalTo(_youHuiQuan.mas_bottom).offset(5);
        }];

    }
    return _endPriceLab;
}

-(UILabel *)catPLab{
    if (_catPLab==nil) {
        _catPLab=[UILabel labText:@"$" color:Black102 font:11*HWB];
        [self.contentView addSubview:_catPLab];
        [_catPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(5*HWB);
            make.bottom.equalTo(_endPriceLab.mas_bottom).offset(-1*HWB);
        }];
        
        UIView* line=[UIView new];
        line.backgroundColor=Black102;
        line.tag=555;
        [self.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_catPLab.mas_centerY);
            make.width.equalTo(_catPLab.mas_width);
            make.height.equalTo(@1);
            make.left.equalTo(_catPLab.mas_left);
        }];
    }
    return _catPLab;
}

-(UILabel *)fenXiangZhuan{
    if (!_fenXiangZhuan) {
        UIView* view=[UIView new];
        view.tag=666;
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(22*HWB);
            make.left.right.offset(0);
            make.top.equalTo(_endPriceLab.mas_bottom).offset(5*HWB);
        }];
        
        UIView* line=[UIView new];
        line.backgroundColor=[UIColor groupTableViewBackgroundColor];
        [view addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.left.right.top.offset(0);
        }];
        
        UIButton* shareBtn=[UIButton new];
        [shareBtn addTarget:self action:@selector(shareGoods) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:shareBtn];
        [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
        
        _fenXiangZhuan = [UILabel labText:@"" color:Black51 font:11*HWB];
        _fenXiangZhuan.textColor=Black102;
        [self.contentView addSubview:_fenXiangZhuan];
        [_fenXiangZhuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5*HWB);
            make.centerY.equalTo(view.mas_centerY);
        }];
        
        UIImageView* ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gl_share_zhuan1"]];
        [view addSubview:ima];
        [ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5*HWB);
            make.top.offset(5*HWB);
            make.bottom.offset(-5*HWB);
            make.width.equalTo(ima.mas_height);
        }];
        
        UILabel* label=[UILabel labText:@"分享" color:Black102 font:11*HWB];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(ima.mas_centerY);
            make.right.equalTo(ima.mas_left).offset(-3*HWB);
        }];
        
    }
    return _fenXiangZhuan ;
}

-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) { // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}


-(void)takeOrder{
    [PageJump takeOrder:_goodsModel];
}

-(void)shareGoods{
    [PageJump shareGoods:_goodsModel goodsImgAry:nil];
}

-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
        
//        if ([_isSltGoodsidDic allKeys].count<9) {
//            _goodsModel.isSelect = YES;
//            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
//            [UIView animateWithDuration:0.26 animations:^{
//                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
//            }];
//            self.goodsBlock(_goodsModel, YES);
//        }else{
//            SHOWMSG(@"一次最多只能分享九个商品哦~")
//        }
    }
}

@end



/*************************************************************************************************************/

#pragma mark ===>>> 第六种样式 《列表、领券形状(左右两边有半圆点)》

@implementation GoodsListCltCell6   // CGSizeMake(ScreenW, 116*HWB) ;

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] placeholderImage:PlaceholderImg] ;
        }
    }];

    
    _goodsTitle.text = goodsModel.goodsName ;
    
    _endPriceLab.text = FORMATSTR(@"￥%@",goodsModel.endPrice) ;
    
    _salesLab.text = FORMATSTR(@"销量：%@",goodsModel.sales);
    
    _priceLab.text = FORMATSTR(@"原价￥%@",goodsModel.price) ;
    
    _endPriceLab.text = FORMATSTR(@"￥%@",goodsModel.endPrice) ;
    
    _shareLabel.text=FORMATSTR(@"赚￥%@",goodsModel.commission) ;
    
    if ([_goodsModel.platform containsString:@"天猫"]) {
        _fromIma.image = [UIImage imageNamed:@"tmallicon"];
    }else if([_goodsModel.platform containsString:@"2"]){
        _fromIma.image = [UIImage imageNamed:@"pddicon"];
    }else{
        _fromIma.image = [UIImage imageNamed:@"tbicon"];
    }
    if (goodsModel.video.length==0) {
        _playVideoBtn.hidden=YES;
    }else{
        _playVideoBtn.hidden=NO;
    }
    _youHuiQuan.text=FORMATSTR(@"￥%@",goodsModel.couponMoney);
    [_smallImaBtn setTitle:FORMATSTR(@"券￥%@",goodsModel.couponMoney) forState:UIControlStateNormal];
    
    
    UIView* lineView = [self.contentView viewWithTag:555];
    if ([goodsModel.couponMoney floatValue]>0) { // 有券
        lineView.hidden=NO;
        _priceLab.hidden=NO;
        _extendBtn.hidden=NO;
        _smallImaBtn.hidden=NO;
        _buyBtn.hidden=YES;
    }else{  // 无券
        lineView.hidden=YES;
        _priceLab.hidden=YES;
        _extendBtn.hidden=YES;
        _smallImaBtn.hidden=YES;
        _buyBtn.hidden=NO;
    }
    if (role_state!=0) {
        //代理商
        _shareIma.hidden=NO;
        _shareLabel.hidden=NO;
        _shareBtn.hidden=NO;
        _extendBtn.hidden=YES;
        _buyBtn.hidden=YES;
        if ([goodsModel.couponMoney floatValue]>0) {
            _smallImaBtn.hidden=NO;
            [_salesLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_smallImaBtn.mas_right).offset(5*HWB);
                make.centerY.equalTo(_smallImaBtn.mas_centerY);
                make.width.offset(100);
            }];
            
        }else{
            _smallImaBtn.hidden=YES;
            [_salesLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_fromIma.mas_right).offset(5*HWB);
                make.centerY.equalTo(_fromIma.mas_centerY);
                make.width.offset(100);
            }];
        }
        
    }else{
        //消费者
        _shareIma.hidden=YES;
        _shareLabel.hidden=YES;
        _shareBtn.hidden=YES;
        _smallImaBtn.hidden=YES;
    }
    
    if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {

        } @finally { }

    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self goodsImgV];
        [self goodsTitle];
        [self fromIma];
        [self salesLab];
        [self endPriceLab];
        [self priceLab];
        [self shareLabel];
        [self extendBtn];
        [self buyBtn];
        [self shareBtn];
        [self smallImaBtn];
        [self playVideoBtn];
        [self selectBtn];
    }
    return self;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.top.offset(12*HWB);
            make.height.width.offset(93*HWB);
        }];
        
    }
    return _goodsImgV;
}



-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        _goodsTitle.numberOfLines=2;
        [self.contentView addSubview:_goodsTitle];
        [_goodsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10);
            make.top.equalTo(_goodsImgV.mas_top);
            make.height.offset(30*HWB);
        }];
        
        
    }
    return _goodsTitle ;
}

-(UIImageView*)fromIma{
    if (!_fromIma) {
        _fromIma = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tbicon"]];
        [self.contentView addSubview:_fromIma];
        [_fromIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.top.equalTo(_goodsTitle.mas_bottom).offset(5*HWB);
            make.height.offset(14*HWB);
            make.width.offset(14*HWB);
        }];
    }
    return _goodsImgV;
}


-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"销量：88888" color:Black102 font:10*HWB];
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_fromIma.mas_right).offset(5*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
            make.width.offset(100);
        }];
    }
    return _salesLab ;
}


-(UILabel *)endPriceLab{
    if (!_endPriceLab) {
        
        _endPriceLab = [UILabel labText:@"￥8888.88" color:LOGOCOLOR font:15*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
        }];
    }
    return _endPriceLab ;
}

-(UILabel *)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥8888.88" color:[UIColor grayColor] font:10*HWB];
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(_endPriceLab.mas_top).offset(-5*HWB);
            make.height.offset(14*HWB);
        }];
        UIView* lineView=[UIView new];
        lineView.tag=555;
        lineView.backgroundColor=[UIColor grayColor];
        [self.contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_priceLab);
            make.width.equalTo(_priceLab.mas_width);
            make.height.offset(1);
        }];
    }
    return _priceLab ;
}

-(UIButton *)extendBtn{
    if (_extendBtn==nil) {
        _extendBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _extendBtn.clipsToBounds = YES ;
        [_extendBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        UIImage * quanImg = [[UIImage imageNamed:@"mini_vouchers"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
        [_extendBtn setBackgroundImage:quanImg forState:(UIControlStateNormal)];
        [self.contentView addSubview:_extendBtn];
        [_extendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
            make.width.offset(78*HWB);
            make.height.offset(30*HWB);
        }];
        
        float viewWidth=(30*HWB-10-2*3*HWB)/3.0;
       
        UILabel* lingquan=[UILabel labText:@"领\n券" color:COLORWHITE font:10.5*HWB];
        lingquan.numberOfLines = 2;
        [_extendBtn addSubview:lingquan];
        [lingquan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(viewWidth/2.0+4*HWB);
            make.top.bottom.offset(0);
            make.width.offset(12*HWB);
        }];
        
        UIImageView * linesImgv = [[UIImageView alloc]init];
        linesImgv.image = [UIImage imageNamed:@"xu_xian"];
        [_extendBtn addSubview:linesImgv];
        [linesImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lingquan.mas_right).offset(4*HWB);
            make.top.bottom.offset(0); make.width.offset(1);
        }];
        
        _youHuiQuan=[UILabel labText:@"￥10" color:COLORWHITE font:14*HWB];
        _youHuiQuan.textAlignment = NSTextAlignmentCenter;
        [_extendBtn addSubview:_youHuiQuan];
        [_youHuiQuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lingquan.mas_right).offset(5*HWB);
            make.top.bottom.offset(0);
            make.right.offset(-viewWidth/2.0);
        }];
        
        if ( THEMECOLOR == 2 ) { // 主题色为浅色
//            lingquan.textColor =  Black51 ;
//            _youHuiQuan.textColor = Black51;
        }
    }
    return _extendBtn;
}

-(UIButton *)buyBtn{
    if (_buyBtn==nil) {
        _buyBtn = [UIButton new];
        _buyBtn.clipsToBounds = YES ;
        [_buyBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        UIImage * quanImg = [[UIImage imageNamed:@"mini_vouchers"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
        [_buyBtn setBackgroundImage:quanImg forState:(UIControlStateNormal)];
        [_buyBtn setTitle:@"立即下单" forState:(UIControlStateNormal)];
        _buyBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [self.contentView addSubview:_buyBtn];
        [_buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
            make.width.offset(78*HWB);
            make.height.offset(30*HWB);
        }];
    }
    return _buyBtn;
}


-(UILabel *)shareLabel{
    if (!_shareLabel) {
        _shareLabel = [UILabel labText:@"分享赚" color:LOGOCOLOR font:11*HWB];
        _shareLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:_shareLabel];
        [_shareLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-6*HWB);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
            make.height.offset(15*HWB);
            make.width.offset(64*HWB);
        }];
    }
    return _shareLabel;
}

-(UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn=[UIButton new];
        [_shareBtn setImage:[UIImage imageNamed:@"gl_share"] forState:UIControlStateNormal];
        [_shareBtn addTarget:self action:@selector(shareGoods) forControlEvents:UIControlEventTouchUpInside];
        [_shareBtn setBackgroundColor:LOGOCOLOR];
        _shareBtn.layer.cornerRadius=13*HWB;
        _shareBtn.clipsToBounds=YES;
        _shareBtn.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_shareLabel.mas_centerX);
            make.bottom.equalTo(_shareLabel.mas_top);
            make.height.offset(26*HWB);
            make.width.offset(26*HWB);
        }];
    }
    return _shareBtn;
}

-(UIButton *)smallImaBtn{
    if (!_smallImaBtn) {
        
        _smallImaBtn = [UIButton titLe:@"券￥" bgColor:COLORCLEAR titColorN:COLORWHITE titColorH:COLORWHITE font:10*HWB];
        UIImage * quanImg = [[UIImage imageNamed:@"mini_vouchers"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
        if (THEMECOLOR==2){
            quanImg = [UIImage imageNamed:@"mini_vouchers"];
        }
//        if (THEMECOLOR==2) { [_smallImaBtn setTitleColor:Black51 forState:(UIControlStateNormal)]; }
        [_smallImaBtn setBackgroundImage:quanImg forState:(UIControlStateNormal)];
        
        [_smallImaBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_smallImaBtn];
        [_smallImaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_fromIma.mas_right).offset(3*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
            make.width.offset(50*HWB);  make.height.offset(14*HWB);
        }];

    }
    return _smallImaBtn;
}

-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) { // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}


-(void)takeOrder{
    [PageJump takeOrder:_goodsModel];
}

-(void)shareGoods{
    [PageJump shareGoods:_goodsModel goodsImgAry:nil];
}

-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
//        if ([_isSltGoodsidDic allKeys].count<9) {
//            _goodsModel.isSelect = YES;
//            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
//            [UIView animateWithDuration:0.26 animations:^{
//                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
//            }];
//            self.goodsBlock(_goodsModel, YES);
//        }else{
//            SHOWMSG(@"一次最多只能分享九个商品哦~")
//        }
    }
}


@end


/*************************************************************************************************************/

#pragma mark ===>>> 第七种样式 《列表、》

@implementation GoodsListCltCell7   //104*HWB

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] placeholderImage:PlaceholderImg] ;
        }
    }];

    if (goodsModel.video.length==0) {
        _playVideoBtn.hidden=YES;
    }else{
        _playVideoBtn.hidden=NO;
    }
    _goodsTitle.text = goodsModel.goodsName ;
    
    _endPriceLab.text = FORMATSTR(@"￥%@",goodsModel.endPrice) ;
    
    _salesLab.text = FORMATSTR(@"销量：%@",goodsModel.sales);
    
    _priceLab.text = FORMATSTR(@"%@",goodsModel.price) ;
    
    _endPriceLab.text = FORMATSTR(@"￥%@",goodsModel.endPrice) ;
    
    _shareLabel.text=FORMATSTR(@"赚￥%@",goodsModel.commission) ;
    
    if ([_goodsModel.platform containsString:@"天猫"]) {
        _fromIma.image = [UIImage imageNamed:@"tmallicon"];
    }else if([_goodsModel.platform containsString:@"2"]){
        _fromIma.image = [UIImage imageNamed:@"pddicon"];
    }else{
        _fromIma.image = [UIImage imageNamed:@"tbicon"];
    }
    
    
    _smallQuanLabel.text=FORMATSTR(@"¥%@",goodsModel.couponMoney);
    
    _quanLabel.text=FORMATSTR(@"¥%@",goodsModel.couponMoney);
    
    UIView* lineView = [self.contentView viewWithTag:555];
    if ([goodsModel.couponMoney floatValue]>0) { // 有券
        lineView.hidden=NO;
        _priceLab.hidden=NO;
        _extendBtn.hidden=NO;
        _smallImaBtn.hidden=NO;
    }else{  // 无券
        lineView.hidden=YES;
        _priceLab.hidden=YES;
        _extendBtn.hidden=YES;
        _smallImaBtn.hidden=YES;
    }
#if DingDangDingDang
    UIView* view = [self viewWithTag:666];
    if (role_state==0) {
        view.hidden=YES;
        
    }else{
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"分享赚￥%@",goodsModel.commission)];
        [str addAttribute:NSForegroundColorAttributeName value:HRGCOLOR range:NSMakeRange(3,str.length-3)];
        _fenXiangZhuan.attributedText = str;
        view.hidden=NO;
        [_endPriceLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.top.equalTo(_fromIma.mas_bottom).offset(5*HWB);
        }];
        
        [_extendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5*HWB);
            make.bottom.equalTo(_endPriceLab.mas_bottom).offset(-10*HWB); //flag
            make.width.offset(64*HWB);
            make.height.offset(20*HWB);
        }];
    }

    _shareIma.hidden=YES;
    _shareLabel.hidden=YES;
    _shareBtn.hidden=YES;
    _smallImaBtn.hidden=YES;
#else
    if (role_state!=0) {
        //代理商
        _shareIma.hidden=NO;
        _shareLabel.hidden=NO;
        _shareBtn.hidden=NO;
        _extendBtn.hidden=YES;
        
        if ([goodsModel.couponMoney floatValue]>0) {
            _smallImaBtn.hidden=NO;
            [_salesLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_smallImaBtn.mas_right).offset(5*HWB);
                make.centerY.equalTo(_smallImaBtn.mas_centerY);
                make.width.offset(100);
            }];
        }else{
            [_salesLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_fromIma.mas_right).offset(5*HWB);
                make.centerY.equalTo(_fromIma.mas_centerY);
                make.width.offset(100);
            }];
            _smallImaBtn.hidden=YES;
        }
        
    }else{
        //消费者
        _shareIma.hidden=YES;
        _shareLabel.hidden=YES;
        _shareBtn.hidden=YES;
        _smallImaBtn.hidden=YES;
    }

#endif
        if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {

        } @finally { }

    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }
}



-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;

        [self goodsImgV];

        [self goodsTitle];

        [self fromIma];

        [self salesLab];

        [self endPriceLab];

        [self priceLab];

        [self shareLabel];

        [self extendBtn];

        [self shareBtn];

        [self smallImaBtn];
        
        [self playVideoBtn];
        
        [self selectBtn];
#if DingDangDingDang
        if (role_state!=0) {
            [self fenXiangZhuan];
        }
        
#endif

    }
    return self;
}


-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.backgroundColor = COLORGROUP ;
        _goodsImgV.layer.cornerRadius=6;
        _goodsImgV.clipsToBounds=YES;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.top.offset(12*HWB);
            make.height.width.offset(80*HWB);
        }];
    }
    return _goodsImgV;
}



-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题" color:Black51 font:12*HWB];
        _goodsTitle.numberOfLines=2;
        [self.contentView addSubview:_goodsTitle];
        [_goodsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10);
            make.top.equalTo(_goodsImgV.mas_top).offset(-2);
            make.height.offset(30*HWB);
        }];
    }
    return _goodsTitle ;
}

-(UIImageView*)fromIma{
    if (!_fromIma) {
        _fromIma = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tbicon"]];
        [self.contentView addSubview:_fromIma];
        [_fromIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.top.equalTo(_goodsTitle.mas_bottom).offset(5*HWB);
            make.height.offset(14*HWB);
            make.width.offset(14*HWB);
        }];
    }
    return _goodsImgV;
}


-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"销量：88888" color:Black102 font:10*HWB];
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_fromIma.mas_right).offset(5*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
            make.width.offset(100);
        }];
    }
    return _salesLab ;
}


-(UILabel *)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"￥8888.88" color:LOGOCOLOR font:15*HWB];
        if (THEMECOLOR==2) {
#if DingDangDingDang
            _endPriceLab = [UILabel labText:@"￥8888.88" color:HRGCOLOR font:15*HWB];
#endif
            _endPriceLab.textColor=Black51;
        }
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
        }];
    }
    return _endPriceLab ;
}

-(UILabel *)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥8888.88" color:[UIColor grayColor] font:10*HWB];
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(5*HWB);
            make.bottom.equalTo(_endPriceLab.mas_bottom);
            make.height.offset(14*HWB);
        }];
        UIView* lineView=[UIView new];
        lineView.tag=555;
        lineView.backgroundColor=[UIColor grayColor];
        [self.contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_priceLab);
            make.width.equalTo(_priceLab.mas_width);
            make.height.offset(1);
        }];
    }
    return _priceLab ;
}

-(UIButton *)extendBtn{
    if (_extendBtn==nil) {
        
        _extendBtn=[UIButton new];
        _extendBtn.layer.cornerRadius=4;
        _extendBtn.layer.borderColor=LOGOCOLOR.CGColor;
#if DingDangDingDang
        _extendBtn.layer.borderColor=HRGCOLOR.CGColor;
#endif
        
        _extendBtn.layer.borderWidth=1;
        _extendBtn.clipsToBounds=YES;
        [_extendBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_extendBtn];
        [_extendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5*HWB);
            make.bottom.equalTo(_endPriceLab.mas_bottom);
            make.width.offset(64*HWB);
            make.height.offset(20*HWB);
        }];
        
        UILabel* lingquan=[UILabel new];
        lingquan.text=@"领券";
        lingquan.font=[UIFont systemFontOfSize:9*HWB];
        lingquan.textColor=[UIColor whiteColor];
        if(THEMECOLOR == 2){
            lingquan.textColor=Black51;
        }
        lingquan.textAlignment=NSTextAlignmentCenter;
        lingquan.backgroundColor=LOGOCOLOR;
#if DingDangDingDang
        lingquan.backgroundColor=HRGCOLOR;
        lingquan.textColor=[UIColor whiteColor];
#endif
        [_extendBtn addSubview:lingquan];
        [lingquan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.equalTo(_extendBtn);
            make.width.offset(64*HWB/3.0*1.4);
        }];
        
        _quanLabel = [UILabel new];
        _quanLabel.text=@"券11";
        _quanLabel.font=[UIFont systemFontOfSize:9*HWB];
        _quanLabel.textColor=LOGOCOLOR;
        if (THEMECOLOR==2) {
            _quanLabel.textColor=Black51;
        }
#if DingDangDingDang
        _quanLabel.textColor=HRGCOLOR;
#endif
        _quanLabel.textAlignment=NSTextAlignmentCenter;
        _quanLabel.backgroundColor=[UIColor whiteColor];
        [_extendBtn addSubview:_quanLabel];
        [_quanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.bottom.equalTo(_extendBtn);
            make.width.offset(64*HWB/3.0*1.6);
        }];
    }
    return _extendBtn;
}
-(UILabel *)shareLabel{
    if (!_shareLabel) {
        _shareLabel = [UILabel labText:@"分享赚" color:LOGOCOLOR font:10*HWB];
        if (THEMECOLOR==2) {
            _shareLabel = [UILabel labText:@"分享赚" color:Black51 font:10*HWB];
        }
#if DingDangDingDang
        _shareLabel = [UILabel labText:@"分享赚" color:HRGCOLOR font:10*HWB];
#endif
        _shareLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:_shareLabel];
        [_shareLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-6*HWB);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
            make.height.offset(15*HWB);
            make.width.offset(56*HWB);
        }];
    }
    return _shareLabel;
}

-(UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn=[UIButton new];
        [_shareBtn setBackgroundColor:LOGOCOLOR];
        [_shareBtn setTitle:@"分享赚" forState:UIControlStateNormal];
        if (THEMECOLOR==2) {
            [_shareBtn setTitleColor:Black51 forState:UIControlStateNormal];
        }
#if DingDangDingDang
        [_shareBtn setTitleColor:COLORWHITE forState:UIControlStateNormal];
#endif
        [_shareBtn addTarget:self action:@selector(shareGoods) forControlEvents:UIControlEventTouchUpInside];
        _shareBtn.titleLabel.font=[UIFont systemFontOfSize:10*HWB];
        _shareBtn.layer.cornerRadius=17*HWB;
        _shareBtn.clipsToBounds=YES;
        _shareBtn.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_shareLabel.mas_centerX);
            make.bottom.equalTo(_shareLabel.mas_top);
            make.height.offset(34*HWB);
            make.width.offset(34*HWB);
        }];
        
        UIView* view=[UIView new];
        view.backgroundColor=[UIColor whiteColor];
        view.layer.cornerRadius=1.5;
        [_shareBtn addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5*HWB);
            make.height.width.offset(3);
            make.centerX.equalTo(_shareBtn.mas_centerX);
        }];
    }
    return _shareBtn;
}

-(UIButton *)smallImaBtn{
    if (!_smallImaBtn) {
        _smallImaBtn=[UIButton new];
        _smallImaBtn.layer.cornerRadius=3;
        _smallImaBtn.layer.borderColor=LOGOCOLOR.CGColor;
#if DingDangDingDang
        _smallImaBtn.layer.borderColor=HRGCOLOR.CGColor;
#endif
        _smallImaBtn.layer.borderWidth=1;
        _smallImaBtn.clipsToBounds=YES;
        [_smallImaBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_smallImaBtn];
        [_smallImaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_fromIma.mas_right).offset(5*HWB);
            make.centerY.equalTo(_fromIma.mas_centerY);
            make.width.offset(63*HWB);
            make.height.offset(16*HWB);
        }];
        
        UILabel* lingquan=[UILabel new];
        lingquan.text=@"领券";
        lingquan.font=[UIFont systemFontOfSize:9*HWB];
        lingquan.textAlignment=NSTextAlignmentCenter;
        lingquan.textColor=[UIColor whiteColor];
        if(THEMECOLOR == 2){
            lingquan.textColor=Black51;
        }
        lingquan.backgroundColor=LOGOCOLOR;
#if DingDangDingDang
        lingquan.backgroundColor=HRGCOLOR;
#endif
        [_smallImaBtn addSubview:lingquan];
        [lingquan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.equalTo(_smallImaBtn);
            make.width.offset(25*HWB);
        }];
        
        _smallQuanLabel = [UILabel new];
        _smallQuanLabel.text=@"券";
        _smallQuanLabel.textAlignment=NSTextAlignmentCenter;
        _smallQuanLabel.font=[UIFont systemFontOfSize:9*HWB];
        _smallQuanLabel.textColor=LOGOCOLOR;
        if (THEMECOLOR==2) {
            _smallQuanLabel.textColor=Black51;
        }
#if DingDangDingDang
        _smallQuanLabel.textColor=HRGCOLOR;
#endif
        _smallQuanLabel.backgroundColor=[UIColor whiteColor];
        [_smallImaBtn addSubview:_smallQuanLabel];
        [_smallQuanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.bottom.equalTo(_smallImaBtn);
            make.width.offset(38*HWB);
        }];
        
    }
    return _smallImaBtn;
}

-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) { // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}


-(UILabel *)fenXiangZhuan{
    if (!_fenXiangZhuan) {
        UIView* view=[UIView new];
        view.tag=666;
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(1);
            make.left.equalTo(_endPriceLab.mas_left);
            make.right.offset(0);
            make.top.equalTo(_endPriceLab.mas_bottom).offset(2*HWB);
        }];
        
        UIView* line=[UIView new];
        line.backgroundColor=[UIColor groupTableViewBackgroundColor];
        [view addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.left.right.top.offset(0);
        }];
        
        UIButton* shareBtn=[UIButton new];
        [shareBtn addTarget:self action:@selector(shareGoods) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:shareBtn];
        [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
        
        _fenXiangZhuan = [UILabel labText:@"" color:Black51 font:11*HWB];
        _fenXiangZhuan.textColor=Black102;
        [view addSubview:_fenXiangZhuan];
        [_fenXiangZhuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5*HWB);
            make.centerY.equalTo(view.mas_centerY);
        }];
        
        UIImageView* ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gl_share_zhuan1"]];
        [view addSubview:ima];
        [ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.top.offset(5*HWB);
            make.bottom.offset(-5*HWB);

            make.width.equalTo(ima.mas_height);
        }];
        
        UILabel* label=[UILabel labText:@"分享" color:Black102 font:11*HWB];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(ima.mas_centerY);
            make.right.equalTo(ima.mas_left).offset(-3*HWB);
        }];
        
    }
    return _fenXiangZhuan ;
}

-(void)takeOrder{
    [PageJump takeOrder:_goodsModel];
}

-(void)shareGoods{
    [PageJump shareGoods:_goodsModel goodsImgAry:nil];
}

-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
//        if ([_isSltGoodsidDic allKeys].count<9) {
//            _goodsModel.isSelect = YES;
//            [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
//            [UIView animateWithDuration:0.26 animations:^{
//                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
//            }];
//            self.goodsBlock(_goodsModel, YES);
//        }else{
//            SHOWMSG(@"一次最多只能分享九个商品哦~")
//        }
    }
}

@end






/*************************************************************************************************************/

#pragma mark ===>>> 第十种样式 《方块、惠得，代理商和消费者有区别》

@implementation GoodsListCltCell10  // 惠得  return  CGSizeMake(ScreenW/2-2,ScreenW/2 + 90*HWB？70*HWB);代理商 消费者

-(void)setIsStartSelect:(BOOL)isStartSelect{
    _isStartSelect = isStartSelect;
    if (_isStartSelect) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
-(void)setIsSltGoodsidDic:(NSMutableDictionary *)isSltGoodsidDic{
    _isSltGoodsidDic = isSltGoodsidDic;
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    _goodsTitle.text = FORMATSTR(@"      %@",_goodsModel.goodsName);
    if ([_goodsModel.platform containsString:@"天猫"]) {
        _platformImgV.image = [UIImage imageNamed:@"tmallicon"];
    }else if([_goodsModel.platform containsString:@"2"]){
        _platformImgV.image = [UIImage imageNamed:@"pddicon"];
    }else{
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
    }
    if (goodsModel.video.length==0) {
        _playVideoBtn.hidden=YES;
    }else{
        _playVideoBtn.hidden=NO;
    }
    _catPLab.text = [NSString stringWithFormat:@"¥%@",_goodsModel.price];
    _salesLab.text = FORMATSTR(@"已售%@",_goodsModel.sales) ;
    _endPriceLab.text = FORMATSTR(@"%@",_goodsModel.endPrice);
    
    [_youHuiQuan setTitle:FORMATSTR(@"¥%@",_goodsModel.couponMoney) forState:UIControlStateNormal];
    if ([_goodsModel.couponMoney floatValue]>0){
        _backView.hidden=NO;
        
    }else{ //无券
        _backView.hidden=YES;
    }
    
    if (role_state==0) {
//        _fenXiangZhuan.hidden=YES;
        _fenXiangZhuan.text=@"分享";
    }else{
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"分享赚￥%@",goodsModel.commission)];
        [str addAttribute:NSForegroundColorAttributeName value:LOGOCOLOR range:NSMakeRange(3,str.length-3)];
//        _fenXiangZhuan.hidden=NO;
        _fenXiangZhuan.attributedText = str;
    }
    
    if (_isStartSelect) { // 开始选择
        _playVideoBtn.hidden=YES;
        @try {
            if (_isSltGoodsidDic) {
                if ([[_isSltGoodsidDic allKeys] containsObject:_goodsModel.goodsId]) {
                    _goodsModel.isSelect = YES;
                }else{ _goodsModel.isSelect = NO; }
            }
            _selectBtn.hidden = NO;
            if (_goodsModel.isSelect==YES) {
                _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
            }else{
                _selectBtn.backgroundColor = [UIColor clearColor];
                [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
            }
        } @catch (NSException *exception) {
            
        } @finally { }
        
    }else{
        _selectBtn.hidden = YES;
        if (goodsModel.video.length==0) {
            _playVideoBtn.hidden=YES;
        }else{
            _playVideoBtn.hidden=NO;
        }
    }
    
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
        [self goodsTitle];
        [self platformImgV];
        [self endPriceLab];
        [self youHuiQuan];
        
        [self fenXiangZhuan];
        [self salesLab];
        [self playVideoBtn];
        [self selectBtn];
    }
    return self;
}

-(UIButton*)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [[UIButton alloc]init];
        _selectBtn.hidden = YES;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [_goodsImgV addSubview:_selectBtn];
        _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _selectBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_selectBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        ADDTARGETBUTTON(_selectBtn, changeSelectState)
        [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }
    return _selectBtn;
}

/** 商品来源图片 **/
-(UIImageView*)platformImgV{
    if (!_platformImgV) {
        _platformImgV = [[UIImageView alloc]init];
        _platformImgV.image = [UIImage imageNamed:@"tbicon"];
        [self.contentView addSubview:_platformImgV];
        [_platformImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsTitle.mas_left);
            make.centerY.equalTo(_goodsTitle.mas_centerY);
            make.height.offset(15*HWB);
            make.width.offset(15*HWB);
        }];
    }
    return _platformImgV ;
}

//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.userInteractionEnabled=YES;
        _goodsImgV.frame = CGRectMake(0, 0, ScreenW/2-2, ScreenW/2-2);
        _goodsImgV.image = [UIImage imageNamed:@"logo"];
        [self.contentView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}

-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        _goodsTitle.numberOfLines=2;
        _goodsTitle.textAlignment =NSTextAlignmentLeft ;
        _goodsTitle.frame = CGRectMake(5*HWB,ScreenW/2+1 ,ScreenW/2-8, 21*HWB );
        [self.contentView addSubview:_goodsTitle];
        
    }
    return _goodsTitle ;
}

//销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"已售00" color:Black102 font:10*HWB];
        [[self.contentView viewWithTag:666] addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5);
            make.centerY.equalTo([self.contentView viewWithTag:666].mas_centerY);
            
        }];
        
    }
    return _salesLab ;
}

//优惠券
-(UIButton *)youHuiQuan{
    if (!_youHuiQuan) {
        _backView = [UIView new];
        _backView.backgroundColor = COLORWHITE ;
        _backView.layer.borderWidth = 1;
        _backView.layer.borderColor = LOGOCOLOR.CGColor;
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_endPriceLab.mas_centerY);
            make.width.offset(54*HWB);
            make.height.offset(16*HWB);
            make.right.offset(-5*HWB);
        }];
        
        UILabel * quan = [UILabel labText:@"券" color:LOGOCOLOR font:11*HWB];
        quan.textAlignment=NSTextAlignmentCenter;
        [_backView addSubview:quan];
        [quan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView.mas_centerY);
            make.width.offset(20*HWB);
            make.height.offset(16*HWB);
            make.left.offset(0);
        }];
        
        UIImageView* line=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"quan_xuxian"]];
        [_backView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView.mas_centerY);
            make.height.offset(16*HWB);
            make.left.equalTo(quan.mas_right).offset(0);
        }];
        
        _youHuiQuan = [UIButton buttonWithType:UIButtonTypeCustom];
        [_youHuiQuan setTitleColor:LOGOCOLOR forState:UIControlStateNormal];

        [_youHuiQuan setBackgroundColor:[UIColor whiteColor]];
        _youHuiQuan.titleLabel.font = [UIFont systemFontOfSize:11*HWB];
        [_backView addSubview:_youHuiQuan];
        [_youHuiQuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(line.mas_right).offset(0);
            make.centerY.equalTo(_backView.mas_centerY);
            make.right.equalTo(_backView.mas_right);
            make.height.offset(16*HWB);
        }];
        
        UIButton* takeOrderBtn=[UIButton new];
        [takeOrderBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        [_backView addSubview:takeOrderBtn];
        [takeOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
    }
    return _youHuiQuan;
}

// 券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        UILabel* lab = [UILabel labText:@"券后￥" color:LOGOCOLOR font:10.5*HWB];
        [self.contentView addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5*HWB);
            make.top.equalTo(_goodsTitle.mas_bottom).offset(10*HWB);
        }];
        
        _endPriceLab = [UILabel labText:@"00" color:LOGOCOLOR font:15*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lab.mas_right).offset(0);
            make.bottom.equalTo(lab.mas_bottom).offset(HWB);
        }];
        
    }
    return _endPriceLab;
}


-(UILabel *)fenXiangZhuan{
    if (!_fenXiangZhuan) {
        UIView* view=[UIView new];
        view.tag=666;
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.left.right.offset(0);
            make.top.equalTo(_endPriceLab.mas_bottom).offset(10*HWB);
        }];
        
        UIImageView* line=[[UIImageView alloc]init];
        line.image=[UIImage imageNamed:@"xuxian"];
        
        [view addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(1);
            make.left.right.top.offset(0);
        }];
        
        _fenXiangZhuan = [UILabel labText:@"" color:Black51 font:10*HWB];
        _fenXiangZhuan.textColor=Black102;
        [view addSubview:_fenXiangZhuan];
        UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareGoods)];
        [view addGestureRecognizer:tap];
        [_fenXiangZhuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5*HWB);
            make.centerY.equalTo(view.mas_centerY);
        }];
        
        UIImageView* ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gl_share_zhuan1"]];
        [view addSubview:ima];
        [ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_fenXiangZhuan.mas_left).offset(-2);
            make.centerY.equalTo(_fenXiangZhuan.mas_centerY);
            make.width.equalTo(ima.mas_height);
        }];

        
    }
    return _fenXiangZhuan ;
}

-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) { // 种草君
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}


-(void)takeOrder{
    [PageJump takeOrder:_goodsModel];
}

-(void)shareGoods{
    [PageJump shareGoods:_goodsModel goodsImgAry:nil];
}

-(void)boFangVideo{
    if (TopNavc!=nil) {
        if (NETSTATE==3) {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
            });
        }
        AVPalyerVC * avpalyer = [[AVPalyerVC alloc]init];
        avpalyer.videoModel = _goodsModel ;
        avpalyer.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:avpalyer animated:YES];
    }
}
// 选中商品
-(void)changeSelectState{
    if (_goodsModel.isSelect) {
        _goodsModel.isSelect = NO;
        [_selectBtn setImage:[UIImage imageNamed:@"goods_select_n"] forState:(UIControlStateNormal)];
        [UIView animateWithDuration:0.26 animations:^{
            _selectBtn.backgroundColor = [UIColor clearColor];
        }];
        self.goodsBlock(_goodsModel, NO);
    }else{
        if (_goodsImgV.image) {
            if ([_isSltGoodsidDic allKeys].count<9) {
                _goodsModel.isSelect = YES;
                
                [UIView animateWithDuration:0.26 animations:^{
                    _selectBtn.backgroundColor = RGBA(0, 0, 0, 0.4);
                    [_selectBtn setImage:[UIImage imageNamed:@"goods_select_h"] forState:(UIControlStateNormal)];
                }];
                self.goodsBlock(_goodsModel, YES);
            }else{
                SHOWMSG(@"一次最多只能分享九个商品哦~")
            }
        }else{
            SHOWMSG(@"商品图片还未下载完成，请稍后再选!")
        }
    }
}

@end



