//
//  UniversalGoodsListVC.m
//  DingDingYang
//
//  Created by ddy on 2017/8/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "UniversalGoodsListVC.h"
#import "ScreeningView.h"
#import "GoodsListCltCell1.h"
#import "DidSelectShareView.h"
#import "GoodsPoster.h"
#import "MoreGoodsSharePopVC.h"

@interface UniversalGoodsListVC () <UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView * universalListCltv ;
@property(nonatomic,strong) ScreeningView * screenView ;
@property(nonatomic,strong) NSMutableDictionary * parameter ;
@property(nonatomic,assign) CGRect  cltvFrame ;
@property(nonatomic,assign) NSInteger   pageNum;
@property(nonatomic,strong) NSMutableArray * classListAry;
@property(nonatomic,strong) UIButton * topBtn ;
@property(nonatomic,assign) NSInteger goodsListStyle ;
@property(nonatomic,strong) NSMutableArray * bannerAry ;

@property(nonatomic,strong) UIButton * changeBtn ;
@property(nonatomic,assign) NSInteger indexBtn ;
// 批量选择分享有关
@property(nonatomic,strong) DidSelectGoodsCltView * didSelectCltView;
@property(nonatomic,strong) NSMutableDictionary * didSelectGoodsDic; // 记录已选商品id和所在的cell的tag(索引)
@property(nonatomic,strong) DidSelectShareView * didShareSView; // 开始选择后的弹窗按钮
@property(nonatomic,strong) NSMutableArray * didSelectGoodsAry; // 用来记录选择的商品(商品model)
@property(nonatomic,strong) UIButton * didShareBtn;

@end

@implementation UniversalGoodsListVC
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (_didShareSView) {
        if (_didShareSView.checkBtn.selected==YES) {
            _didShareSView.checkBtn.selected=NO;
            [_didSelectCltView hiddenSelfAndAnimation];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_mcsModel.title.length>0) {
        self.title = _mcsModel.title ;
    }else if(_mcsModel.name.length>0){
        self.title =_mcsModel.name;
    }else{
        self.title = @"商品列表" ;
    }
    
    self.view.backgroundColor = COLORGROUP ;
    
    if (_mcsModel==nil) {
        _mcsModel = [[MClassModel alloc]initWithMC:@{@"link":url_goods_list2}];
    }
    
    _goodsListStyle = GoodsListStyle ;

    _pageNum = 1 ;
    
    [self parameter];
    
    _classListAry = [[NSMutableArray alloc]init];
    
    
    if (_isHideSelect==NO) { [self screenView]; }
    
    [self universalListCltv];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:LoginSuccess object:nil];
    
    [self topBtn];
    
    if ([NSUDTakeData(ShareStatus) isEqualToString:@"open"]) {
        _didSelectGoodsAry = [NSMutableArray array];
        _didSelectGoodsDic = [NSMutableDictionary dictionary];
        [self didShareBtn];
        [self didShareSView];
    }

    
}

-(UIButton*)didShareBtn{
    if (!_didShareBtn) {
        _didShareBtn = [[UIButton alloc]init];
        _didShareBtn.frame = CGRectMake(0, 0, 52*HWB, 33);
        _didShareBtn.titleLabel.font = [UIFont systemFontOfSize:12*HWB];
        [_didShareBtn setTitle:@"批量分享" forState:(UIControlStateNormal)];
        [_didShareBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        [_didShareBtn addTarget:self action:@selector(startSelectGoods) forControlEvents:UIControlEventTouchUpInside];
        _didShareBtn.selected = NO;
        UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithCustomView:_didShareBtn];
        self.navigationItem.rightBarButtonItem = rightItem;
    }
    return _didShareBtn;
}


-(DidSelectShareView*)didShareSView{
    if (!_didShareSView) {
        _didShareSView = [[DidSelectShareView alloc]init];
        [self.view addSubview:_didShareSView];
        [_didShareSView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.view.mas_centerY).offset(-60*HWB);
            make.height.offset(180*HWB);  make.width.offset(42*HWB);
            make.right.offset(-10*HWB);
        }];
        __weak UniversalGoodsListVC * selfView = self;
        _didShareSView.touchUpType = ^(NSInteger type) {
            switch (type) {
                case 0:{ // 隐藏已选列表
                    [selfView.didSelectCltView hiddenSelfAndAnimation];
                } break;
                    
                case 1:{ // 显示已选列表
                    if (!selfView.didSelectCltView) { [selfView didSelectCltView]; }
                    if (selfView.didSelectGoodsAry.count==0) {
                        SHOWMSG(@"你还没有选择商品哦!")
                    }else{
                        selfView.didSelectCltView.dataAry = selfView.didSelectGoodsAry;
                        [selfView.didSelectCltView showSelfAndAnimation];
                    }
                } break;
                    
                case 2:{ // 微信分享(小程序或web集合页)
                    if (_didSelectGoodsAry.count>=3) {
                        NSArray * allAry = [selfView.didSelectGoodsDic allKeys];
                        [MoreGoodsSharePopVC showShareEditView:selfView shareGoodsIdAry:allAry goodsAry:selfView.didSelectGoodsAry];
                    }else{
                        SHOWMSG(@"至少选择三个商品哦~")
                    }
                    
                } break;
                    
                case 3:{ // 朋友圈分享(合成海报)
                    if (_didSelectGoodsAry.count>=3) {
                        MBShow(@"正在加载...")
                        [GoodsPoster createShareGoodsPosterBy:selfView.didSelectGoodsAry goodsidAry:[selfView.didSelectGoodsDic allKeys] resultPoster:^(id data) {
                            MBHideHUD
                            if ([data isKindOfClass:[UIImage class]]) {
                                if (TARGET_IPHONE_SIMULATOR) { // 模拟器(显示预览)
                                    [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[data]];
                                } else { // 真机打开微信朋友圈分享
                                    CustomShareModel * shareModel = [[CustomShareModel alloc] init];
                                    shareModel.imgDataOrAry = data; //
                                    shareModel.sharePlatform = 2; // 分享朋友圈
                                    shareModel.shareType = 2; // 分享图片
                                    [CustomUMShare shareByCustomModel:shareModel result:^(NSInteger result) {
                                        if (result==1) { // 1: 分享成功
                                        }
                                    }];
                                }
                            }else{
                                SHOWMSG((NSString*)data)
                            }
                        }];
                    }else{ SHOWMSG(@"至少选择三个商品哦~") }
                } break;
                    
                default:  break;
            }
        };
    }
    return _didShareSView;
}


// 点击批量选择执行
-(void)startSelectGoods{
    
    if (_didShareBtn.selected==NO) { // 开始选择
        _didShareBtn.selected = YES;
        _didShareBtn.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_didShareBtn) { _didShareBtn.enabled = YES; }
        });
        [_didShareBtn setTitle:@"取消分享" forState:(UIControlStateNormal)];
        [_didShareSView showSelfAndAnimation];
        
    }else{ // 取消选择
        [_didSelectGoodsAry removeAllObjects];
        [_didSelectGoodsDic removeAllObjects];
        _didShareSView.allCount = 0;
        _didShareBtn.selected = NO;
        _didShareBtn.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_didShareBtn) { _didShareBtn.enabled = YES; }
        });
        [_didShareBtn setTitle:@"批量分享" forState:(UIControlStateNormal)];
        [_didShareSView hiddenSelfAndAnimation];
    }
    
    [self.universalListCltv reloadData];
}

#pragma mark => 展示已选商品的cltView
-(DidSelectGoodsCltView*)didSelectCltView{
    if (!_didSelectCltView) {
        _didSelectCltView = [[DidSelectGoodsCltView alloc]init];
        _didSelectCltView.frame = CGRectMake(0, ScreenH-NAVCBAR_HEIGHT, ScreenW, 160*HWB);
        [self.view addSubview:_didSelectCltView];
        [_didSelectCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.height.offset(160*HWB);
            make.top.equalTo(self.view.mas_bottom).offset(0);
        }];
        __weak UniversalGoodsListVC * selfView = self;
        _didSelectCltView.removeSelectGoodsBlock = ^(GoodsModel * sgModel, NSInteger selectTag) {
            [selfView deleteSelectGoods:sgModel andSelectTag:selectTag];
        };
    }
    return _didSelectCltView;
}
// 从已选商品删除商品
-(void)deleteSelectGoods:(GoodsModel*)gmodel andSelectTag:(NSInteger)tag{
    @try {
        self.didShareSView.allCount = self.didSelectGoodsAry.count;
        // 此处开始遍历整个大字典，根据已选字典存值，确定所在大字典数据位置，修改，并刷新
        if (REPLACENULL(_didSelectGoodsDic[gmodel.goodsId]).length>0) {
            for (GoodsModel * model in _classListAry) {
                if ([model.goodsId isEqual:gmodel.goodsId]&&model.isSelect==YES) {
                    model.isSelect = NO;
                    break;
                }
            }
            [_didSelectGoodsDic removeObjectForKey:gmodel.goodsId];
        }
    } @catch (NSException *exception) {
        if ([[_didSelectGoodsDic allKeys] containsObject:gmodel.goodsId]) {
            [_didSelectGoodsDic removeObjectForKey:gmodel.goodsId];
        }
    } @finally {
        if (_didSelectGoodsAry.count==0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.26 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                _didShareSView.checkBtn.selected = NO;
                [_didSelectCltView hiddenSelfAndAnimation];
            });
        }
        [_universalListCltv reloadData];
    }
    
}

-(void)changeYangShi{
//    _indexBtn++ ;
//    _goodsListStyle = FORMATSTR(@"3-1-0-%ld",(long)_indexBtn);
//    [_changeBtn setTitle:FORMATSTR(@"样式%ld",(long)_indexBtn) forState:(UIControlStateNormal)];
//    [_universalListCltv reloadData];
//    if (_indexBtn==7) { _indexBtn = 0 ; }
    
    
}


-(void)changeState{
    [_universalListCltv.mj_header beginRefreshing];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LoginSuccess object:nil];
}

-(NSMutableDictionary*)parameter{
    if (!_parameter) {
        _parameter = [[NSMutableDictionary alloc]init];
        _parameter[@"st"] = @"1";
        [_parameter addEntriesFromDictionary:_mcsModel.params];
    }
    return _parameter ;
}

//排序条件
-(ScreeningView*)screenView{
    if (!_screenView) {
        _screenView = [[ScreeningView alloc] initWithFrame:CGRectMake(0,0, ScreenW, 38)];
        [self.view addSubview:_screenView];
        [_screenView selectBlock:^(NSString *sortType) {

            _parameter[@"sortType"] = sortType ;
            [_universalListCltv.mj_header beginRefreshing];
        }];
    }
    return _screenView;
}



-(void)loadClassHomeData{
    
    [_universalListCltv.mj_footer resetNoMoreData];
    
    if ([REPLACENULL(_parameter[@"sortType"])  integerValue]==0) {
        _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum);
       
    }else{
        _parameter[@"st"] = @"1" ;
        _pageNum = 2 ;
    }
    
    [NetRequest requestTokenURL:_mcsModel.link parameter:_parameter success:^(NSDictionary *nwdic) {
        
        if (_classListAry.count>0) {
            if ([_parameter[@"st"] integerValue]==1) {
                [_classListAry removeAllObjects];

                [_universalListCltv reloadData];
            }
        }
        
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry= NULLARY( nwdic[@"list"] ) ;
            if (lAry.count>0) {
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_classListAry insertObject:model atIndex:idx];
                }];
                if ( [REPLACENULL(_parameter[@"sortType"]) integerValue]==0 ) {   _pageNum ++ ;  }
            }
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        [_universalListCltv.mj_header endRefreshing];
        [_universalListCltv reloadData];
        
    } failure:^(NSString *failure) {
        [_universalListCltv.mj_header endRefreshing];
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self loadClassHomeData];
        }];
        SHOWMSG(failure);

    }];
    
}

//加载更多
-(void)loadMoreData{
    
    _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum);
    [NetRequest requestTokenURL:_mcsModel.link parameter:_parameter success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry=nwdic[@"list"];
            if (lAry.count>0) {
                _pageNum++;
                [_universalListCltv.mj_footer endRefreshing];
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_classListAry addObject:model];
                }];
            }else{
                [_universalListCltv.mj_footer endRefreshingWithNoMoreData];
            }
            
        }else{
            [_universalListCltv.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        [_universalListCltv reloadData];
    } failure:^(NSString *failure) {
        [_universalListCltv.mj_footer endRefreshing];
        SHOWMSG(failure)
    }];
    
}

//商品列表
-(UICollectionView*)universalListCltv{
    if (!_universalListCltv) {
        
        UICollectionViewFlowLayout * layoutUnvsl = [[UICollectionViewFlowLayout alloc]init];
        _universalListCltv = [[UICollectionView alloc]initWithFrame:_cltvFrame collectionViewLayout:layoutUnvsl];
        _universalListCltv.delegate = self; _universalListCltv.dataSource = self ;
        _universalListCltv.backgroundColor = COLORGROUP ;
        _universalListCltv.clipsToBounds = YES ;
        [self.view addSubview:_universalListCltv];
        [_universalListCltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0);
            make.top.offset(_isHideSelect?0:38);
            make.bottom.offset(0);
        }];
        
        [_universalListCltv registerClass:[AllEmptyHeadOrFooterCltCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"iphonex_foot"];
        [_universalListCltv registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"goods_listclt_cell1"];
        [_universalListCltv registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"goods_listclt_cell2"];
        [_universalListCltv registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"goods_listclt_cell3"];
        [_universalListCltv registerClass:[GoodsListCltCell4   class] forCellWithReuseIdentifier:@"goods_listclt_cell4"];
        [_universalListCltv registerClass:[GoodsListCltCell5   class] forCellWithReuseIdentifier:@"goods_listclt_cell5"];
        [_universalListCltv registerClass:[GoodsListCltCell6   class] forCellWithReuseIdentifier:@"goods_listclt_cell6"];
        [_universalListCltv registerClass:[GoodsListCltCell7   class] forCellWithReuseIdentifier:@"goods_listclt_cell7"];
        [_universalListCltv registerClass:[GoodsListCltCell10   class] forCellWithReuseIdentifier:@"goods_listclt_cell10"];

        //下拉刷新
        __weak UniversalGoodsListVC * SelfView = self ;
        _universalListCltv.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadClassHomeData)];
        _universalListCltv.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreData];
        }];
        [_universalListCltv.mj_header beginRefreshing];
        
    }
    return _universalListCltv ;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _classListAry.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (_goodsListStyle) {
        case 2:{
            GoodsListCltCell3 * goodsListClt3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell3" forIndexPath:indexPath];
            goodsListClt3.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
               goodsListClt3.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt3.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt3.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt3;
        } break;
            
        case 3:{
            GoodsListCltCell2 * goodsListClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell2" forIndexPath:indexPath];
            goodsListClt2.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
                goodsListClt2.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt2.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt2.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt2 ;
        } break;
            
        case 4:{
            GoodsListCltCell4 * goodsListClt4 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell4" forIndexPath:indexPath];
            goodsListClt4.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
                goodsListClt4.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt4.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt4.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt4 ;
        } break;
            
        case 5:{
            GoodsListCltCell5 * goodsListClt5 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell5" forIndexPath:indexPath];
            goodsListClt5.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
                goodsListClt5.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt5.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt5.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt5 ;
        } break;
            
        case 6:{
            GoodsListCltCell6 * goodsListClt6 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell6" forIndexPath:indexPath];
            goodsListClt6.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
                goodsListClt6.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt6.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt6.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt6 ;
        } break;
            
        case 7:{
            GoodsListCltCell7 * goodsListClt7 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell7" forIndexPath:indexPath];
            goodsListClt7.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
                goodsListClt7.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt7.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt7.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt7 ;
        } break;
            
        case 8:{
            GoodsListCltCell10 * goodsListClt10 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell10" forIndexPath:indexPath];
            goodsListClt10.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
                goodsListClt10.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt10.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt10.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt10 ;
        } break;
        
        default:{
            GoodsListCltCell1 * goodsListClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell1" forIndexPath:indexPath];
            goodsListClt1.isStartSelect = _didShareBtn.selected;
            if (_classListAry.count>indexPath.item) {
                goodsListClt1.goodsModel = _classListAry[indexPath.item];
            }else{ [_universalListCltv reloadData]; }
            goodsListClt1.isSltGoodsidDic = _didSelectGoodsDic;
            goodsListClt1.goodsBlock = ^(GoodsModel *goodsModel, BOOL ifSelect) {
                [self selectGoodsModel:goodsModel isSelect:ifSelect];
            };
            return goodsListClt1 ;
        } break;
    }

}

// 从列表点击选择后执行
-(void)selectGoodsModel:(GoodsModel *)model isSelect:(BOOL)select{
    @try {
        if (select) {
            _didSelectGoodsDic[model.goodsId] = model.goodsId;
            [_didSelectGoodsAry addObject:model];
            if (_didShareSView.checkBtn.selected==YES) {
                _didSelectCltView.dataAry = _didSelectGoodsAry;
            }
        }else{
            
            if ([_didSelectGoodsAry containsObject:model]) {
                [_didSelectGoodsAry removeObject:model];
            }else{
                NSMutableArray * ary = [NSMutableArray arrayWithArray:_didSelectGoodsAry];
                for (NSInteger i=0;i<ary.count;i++) {
                    GoodsModel * gmd = ary[i];
                    if ([gmd.goodsId isEqual:model.goodsId]) {
                        [_didSelectGoodsAry removeObjectAtIndex:i];
                        break;
                    }
                }
            }
            
            if ([[_didSelectGoodsDic allKeys] containsObject:model.goodsId]) {
                [_didSelectGoodsDic removeObjectForKey:model.goodsId];
            }
        }
        _didShareSView.allCount = _didSelectGoodsAry.count;
    } @catch (NSException *exception) {
        
    } @finally { }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GoodsDetailsVC*detailVc = [[GoodsDetailsVC alloc]init] ;
    detailVc.hidesBottomBarWhenPushed = YES ;
    detailVc.dmodel = _classListAry[indexPath.row] ;
    PushTo(detailVc, YES)
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    // 部分序号命名和后台不一致
    switch (_goodsListStyle) {
        case 2:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+70*HWB); } break;
        case 4:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
        case 5:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+(role_state==0?70*HWB:90*HWB)); } break;
        case 6:{ return CGSizeMake(ScreenW, 116*HWB); } break;
        case 7:{ return CGSizeMake(ScreenW, 104*HWB); } break;
        case 8:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
        default:{ return CGSizeMake(ScreenW, FloatKeepInt(100*HWB) ); } break;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 5 ; }
    return 1 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 4 ; }
    return 0 ;
}

-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
        float  yy = ScreenH-36*HWB-10 ;
        if (_isOnePage==YES) { yy = ScreenH-36*HWB-59 ; }
//        _topBtn.frame = CGRectMake(ScreenW-36*HWB-10, yy, 36*HWB, 36*HWB);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);  make.bottom.offset(-10);
            make.height.offset(36*HWB); make.width.offset(36*HWB);
        }];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_universalListCltv.contentOffset.y>=0) {
        if (_universalListCltv.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_universalListCltv.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _universalListCltv.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
    }
}
-(void)clickBackTop{
    [_universalListCltv setContentOffset:CGPointZero animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
