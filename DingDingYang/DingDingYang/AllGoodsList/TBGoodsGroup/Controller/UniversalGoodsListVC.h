//
//  UniversalGoodsListVC.h
//  DingDingYang
//
//  Created by ddy on 2017/8/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class MClassModel ;
@interface UniversalGoodsListVC : DDYViewController

@property(nonatomic,assign) BOOL isHideSelect ;  //是否隐藏筛选调件 YES:隐藏  NO或nil:显示

@property(nonatomic,assign) BOOL isOnePage ; //是否是一级界面 YES: 是(collectionView高度-49)  NO或nil:不是

@property(nonatomic,strong) MClassModel * mcsModel ;

@end
