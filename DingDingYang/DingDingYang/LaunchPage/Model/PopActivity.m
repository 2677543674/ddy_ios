
//
//  PopActivity.m
//  DingDingYang
//
//  Created by ddy on 2018/5/3.
//

#import "PopActivity.h"
#import "LaunchAdView.h"
#import "ActivityModel.h"

@implementation PopActivity


+(void)showLaunchAd:(UIView *)supView{
    PopActivity * popAd = [[PopActivity alloc]init];
    [popAd getLaunchAdDataSuccessBlock:^(UIImage *adimg, ActivityModel *adModel) {
        [LaunchAdView showAdView:supView adimg:adimg jumpModel:adModel];
    }];
}


-(void)getLaunchAdDataSuccessBlock:(AdBlock)launchAdBlock{
    self.launchAdBlock = launchAdBlock;
    // 先判空
    if (NSUDTakeData(ad_launch_img)) {
        
        NSInteger nowTime = [[NSDate date] timeIntervalSince1970]*1000;

        if (NSUDTakeData(ad_launch_img)&&NSUDTakeData(ad_launch_dic)) {
            _model = [[ActivityModel alloc]initWithDic:NSUDTakeData(ad_launch_dic)];
            // 判断广告在时间范围内
            if (nowTime>[_model.startTime integerValue]&&nowTime<[_model.endTime integerValue]) {
                UIImage * image = [UIImage imageWithData:NSUDTakeData(ad_launch_img)];
                if (image) { // 有图片展示，存下图片链接
                    if (_model.type==3) { NSUDRemoveData(ad_launch_dic); }
                    if (_model.type==5) {  } // 多次
                    NSUDSaveData(_model.img, ad_launch_imgUrl)
                    self.launchAdBlock(image,_model);
                }
                
            }else{ }
            
        }
    }
    
    [self getActivity];
    
}


// 获取广告并缓存
-(void)getActivity{
    
    [NetRequest requestType:2 url:url_get_activity ptdic:nil success:^(id nwdic) {
        
        @try {
            
            NSArray * activityAry = NULLARY(nwdic[@"activityList"]);
            
            NSMutableArray * actModelAry = [NSMutableArray array];
            
            [activityAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ActivityModel * model = [[ActivityModel alloc]initWithDic:obj];
                [actModelAry addObject:model];
            }];
            
            NSMutableArray * adTypeAry = [NSMutableArray array];
            
            for (NSInteger i=0;i<actModelAry.count;i++) {
                
                ActivityModel * modelType = actModelAry[i];
                [adTypeAry addObject:@(modelType.type)];
                
                NSDictionary * pdic = NULLDIC(activityAry[i]);
                
//                NSLog(@"%ld",(long)modelType.type);
                switch (modelType.type) {
                       
                    case 1:{ [self parsingFloatingAd:pdic]; } break; // 首页悬浮小广告
                    case 2:{ [self parsingPopupWindowType:1 model:pdic]; } break; // 弹框(一次)
                    case 3:{ [self parsingLaunchAdType:1 model:pdic];    } break; // 启动页广告(一次)
                    case 4:{ [self parsingPopupWindowType:0 model:pdic]; } break; // 弹框(弹多次)
                    case 5:{ [self parsingLaunchAdType:0 model:pdic];    } break; // 启动页广告(多次)
                        
                    default: break;
                }
                
            }
            
            //===================  清除已下架的广告缓存  ===================//
            
            // 悬浮小广告为空(本地清除缓存)
            if (![adTypeAry containsObject:@1]) { [self removeAdBuyType:1]; }
            
            // 弹框(一次)为空(本地清除缓存)
            if (![adTypeAry containsObject:@2]&&![adTypeAry containsObject:@4]){ [self removeAdBuyType:2]; }
            
            // 启动页(一次)广告为空(本地清除缓存)
            if (![adTypeAry containsObject:@3]&&![adTypeAry containsObject:@5]) { [self removeAdBuyType:3]; }
            
        } @catch (NSException *exception) {  [self removeAdBuyType:0]; } @finally { }
       
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [self removeAdBuyType:0];
    }];

}


#pragma mark ===>>> 解析启动页广告
-(void)parsingLaunchAdType:(NSInteger)type model:(NSDictionary*)dic{
    
    ActivityModel * model = [[ActivityModel alloc]initWithDic:dic];
    if (type==1) { // 只显示一次广告
        if (NSUDTakeData(ad_launch_img)==nil&&NSUDTakeData(ad_launch_dic)==nil) {
            // 广告未展示过
            if (model.img.length>0&&[model.img hasPrefix:@"http"]) {
                // 下载图片(如果广告数据未变，下载图片会从缓存中读取，无需考虑图片重复下载)
                [NetRequest downloadImageByUrl:model.img success:^(id result) {
                    if (result) {
                        NSData * data = UIImageJPEGRepresentation((UIImage*)result,1);
                        NSUDSaveData(data, ad_launch_img)
                        NSUDSaveData(dic, ad_launch_dic)
                    }
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
            }
        }else if(NSUDTakeData(ad_launch_img)&&NSUDTakeData(ad_launch_dic)==nil){
            NSString * imgurl = REPLACENULL(NSUDTakeData(ad_launch_imgUrl));
            if(![imgurl isEqualToString:model.img]){ // 两次广告不一样
                [NetRequest downloadImageByUrl:model.img success:^(id result) {
                    if (result) {
                        NSData * data = UIImageJPEGRepresentation((UIImage*)result,1);
                        NSUDSaveData(data, ad_launch_img)
                        NSUDSaveData(dic, ad_launch_dic)
                    }
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
            }
        }else{  }
    }else{
        [NetRequest downloadImageByUrl:model.img success:^(id result) {
            if (result) {
                NSData * data = UIImageJPEGRepresentation((UIImage*)result,1);
                NSUDSaveData(data, ad_launch_img)
                NSUDSaveData(dic, ad_launch_dic)
            }
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
    }
}

#pragma mark ===>>> 解析缓存首页弹窗广告
-(void)parsingPopupWindowType:(NSInteger)type model:(NSDictionary*)dic{
    if (type==1) { // 一次弹窗
        ActivityModel * modelAlt = [[ActivityModel alloc]initWithDic:dic];
        if (NSUDTakeData(ad_popView_imgUrl)) {
            // 一次广告，更新了图片链接(缓存数据)
            if (![NSUDTakeData(ad_popView_imgUrl) isEqualToString:modelAlt.img]) {
                NSUDSaveData(dic, ad_popView_dic)
            }
        }else{
            NSUDSaveData(dic, ad_popView_dic)
        }
    }else{ // 多次弹窗(图片在弹出的时候下载)
        NSUDSaveData(dic, ad_popView_dic)
    }
}

#pragma mark ===>>> 解析缓存悬浮广告判断
-(void)parsingFloatingAd:(NSDictionary*)dic{
    NSUDSaveData(dic, ad_floating_dic)
}


-(void)removeAdBuyType:(NSInteger)removeType{
    @try {
        if (removeType==0||removeType==1) {
            if (NSUDTakeData(ad_floating_dic)) { NSUDRemoveData(ad_floating_dic) }
            if (NSUDTakeData(ad_floating_imgUrl)) { NSUDRemoveData(ad_floating_imgUrl) }
        }
        if (removeType==0||removeType==2) {
            if (NSUDTakeData(ad_popView_dic)) { NSUDRemoveData(ad_popView_dic) }
            if (NSUDTakeData(ad_popView_imgUrl)) { NSUDRemoveData(ad_popView_imgUrl) }
        }
        if (removeType==0||removeType==3) {
            if (NSUDTakeData(ad_launch_dic)) { NSUDRemoveData(ad_launch_dic) }
            if (NSUDTakeData(ad_launch_img)) { NSUDRemoveData(ad_launch_img) }
            if (NSUDTakeData(ad_launch_imgUrl)) { NSUDRemoveData(ad_launch_imgUrl) }
        }
       
    } @catch (NSException *exception) {
        
    } @finally { }
}

@end
