//
//  ActivityModel.h
//  DingDingYang
//
//  Created by ddy on 2017/12/22.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModuleModel.h"

@interface ActivityModel : NSObject

@property(nonatomic,copy) NSString * refId; // (商品ID数组)
@property(nonatomic,copy) NSString * des;
@property(nonatomic,copy) NSString * version;
@property(nonatomic,copy) NSString * smallPic;

@property(nonatomic,copy) NSString * startTime; // 开始时间
@property(nonatomic,copy) NSString * endTime;   // 结束时间
@property(nonatomic,copy) NSString * openType;  // 1:webview 2:外部浏览器
@property(nonatomic,copy) NSString * platform;  // 0:本平台.1:淘宝,2:京东.
@property(nonatomic,copy) NSString * sortLink;  // 短链接.
@property(nonatomic,assign) NSInteger todo ;    // 行为：1:商品详情页 2:h5链接
@property(nonatomic,copy) NSString * title;     // 活动标题
@property(nonatomic,copy) NSString * link;      // toDo:2 h5的URL
@property(nonatomic,copy) NSString * img;       // 活动图片.
@property(nonatomic,assign) NSInteger type;     // 活动展示类型( 1:浮动 2:弹框(仅弹一次) 3:闪屏 4:弹框(弹多次) ) 5:闪屏多次

@property(nonatomic,strong) MClassModel * category; // toDo:3 转向二级页面参数.
@property(nonatomic,strong) GoodsModel * goodsMd;   // 商品详情

//-(instancetype)initWithModel:(NSDictionary *)dic;

-(instancetype)initWithDic:(NSDictionary*)dic;

@end
