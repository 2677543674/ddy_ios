//
//  ActivityModel.m
//  DingDingYang
//
//  Created by ddy on 2017/12/22.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ActivityModel.h"

@implementation ActivityModel

//-(instancetype)initWithModel:(NSDictionary *)dic{
//    self = [super init];
//    if (self) {
//        _des = REPLACENULL(dic[@"des"]) ;
//        _endTime = REPLACENULL(dic[@"endTime"]) ;
//        _img = [REPLACENULL(dic[@"img"]) hasPrefix:@"http"]? dic[@"img"]:FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"img"]));
//        _link = REPLACENULL(dic[@"link"]) ;
//        _openType = REPLACENULL(dic[@"openType"]) ;
//        _platform = REPLACENULL(dic[@"platform"]) ;
//        _refId = REPLACENULL(dic[@"refId"]) ;
//        _smallPic = [REPLACENULL(dic[@"smallPic"]) hasPrefix:@"http"]? dic[@"smallPic"] : FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"smallPic"])) ;
//        _sortLink = REPLACENULL(dic[@"sortLink"]) ;
//        _startTime = REPLACENULL(dic[@"startTime"]) ;
//        _title = REPLACENULL(dic[@"title"]) ;
//        _todo = [dic[@"toDo"] integerValue];
//        _type = REPLACENULL(dic[@"type"]) ;
//        _version = REPLACENULL(dic[@"version"]) ;
//        _category = [[MClassModel alloc]initWithMC:dic[@"category"]];
//        _goodsMd = [[GoodsModel alloc]initWithDic:dic[@"goods"]];
//
//    }
//    return self ;
//}


-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        
         if ([REPLACENULL(dic[@"img"]) hasPrefix:@"http"]==NO) {
            _img = FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"img"]));
         }else{ _img = REPLACENULL(dic[@"img"]);  }
        
        if ([REPLACENULL(dic[@"smallPic"]) hasPrefix:@"http"]==NO) {
            _smallPic = FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"smallPic"]));
        }else{ _smallPic = REPLACENULL(dic[@"smallPic"]); }
        
        
        _startTime = REPLACENULL(dic[@"startTime"]);
        _endTime = REPLACENULL(dic[@"endTime"]);
        
        _todo = [REPLACENULL(dic[@"toDo"]) integerValue];
        _type = [REPLACENULL(dic[@"type"]) integerValue];
        _openType = REPLACENULL(dic[@"openType"]);
        _platform = REPLACENULL(dic[@"platform"]);
        _sortLink = REPLACENULL(dic[@"sortLink"]);
        _title = REPLACENULL(dic[@"title"]);
        _link = REPLACENULL(dic[@"link"]);
        
        _category = [[MClassModel alloc]initWithMC:NULLDIC(dic[@"category"])];
        _goodsMd = [[GoodsModel alloc]initWithDic:NULLDIC(dic[@"goods"])];
        
        _des = REPLACENULL(dic[@"des"]) ;
        _refId = REPLACENULL(dic[@"refId"]);
        _version = REPLACENULL(dic[@"version"]);

    }
    return self ;
}

@end
