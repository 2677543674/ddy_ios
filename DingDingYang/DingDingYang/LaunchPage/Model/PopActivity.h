//
//  PopActivity.h
//  DingDingYang
//
//  Created by ddy on 2018/5/3.
//

#import <Foundation/Foundation.h>
#import <YYWebImage.h>

@interface PopActivity : NSObject

//@property(nonatomic,strong) ActivityModel* flashModel;
//@property(nonatomic,strong) ActivityModel* floatModel;
//@property(nonatomic,strong) ActivityModel* PopModel;
//@property(nonatomic,strong) UIView* homeView;
//@property(nonatomic,assign) float height;
//+(PopActivity*)shareActivity;
//-(void)getActivity;
//-(void)firstFlash:(UIView*)view;


#pragma mark ===>>> 新的获取chuang创建广告的方法

@property(nonatomic,assign) NSInteger sec;
@property(nonatomic,strong) NSTimer * timer;
@property(nonatomic,strong) ActivityModel * model;

typedef void (^AdBlock) (UIImage *adimg,ActivityModel*adModel);
@property (copy, nonatomic) AdBlock launchAdBlock;

/**
 显示启动页广告
 
 @param supView 父视图 miantabbar的view
 */
+(void)showLaunchAd:(UIView*)supView;

/**
 获取闪屏页广告
 
 @param launchAdBlock 回调弹框的图片和跳转参数
 */
-(void)getLaunchAdDataSuccessBlock:(AdBlock)launchAdBlock;

-(void)getActivity;

@end
