//
//  PopSearchView.m
//  DingDingYang
//
//  Created by ddy on 2018/7/11.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "PopSearchView.h"
#import "SearchGoodsVC.h"

@implementation PopSearchView

+(PopSearchView*)popSearchViewAndTitle:(NSString*)title{
    static PopSearchView* popSearchView=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        popSearchView =[[PopSearchView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
        [[UIApplication sharedApplication].keyWindow addSubview:popSearchView];
    });
    popSearchView.text.text=title;
    popSearchView.hidden=NO;
    popSearchView.alpha=0;
    [UIView animateWithDuration:0.5 animations:^{
        popSearchView.alpha=1;
    }];
    
    return popSearchView;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.45];
        UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickTap)];
        [self addGestureRecognizer:tap];
        
        [self view];
        [self imageView];
        [self leftBtn];
        [self text];
    }
    
    return self;
}

-(void)clickTap{
//    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

-(UIView *)view{
    if (_view==nil) {
        _view=[UIView new];
        _view.backgroundColor=COLORWHITE;
        _view.layer.cornerRadius=6*HWB;
        _view.clipsToBounds=YES;
        [self addSubview:_view];
        [_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.width.offset(ScreenW-60*HWB);
            make.height.equalTo(_view.mas_width).multipliedBy(0.85);
        }];
    }
    return _view;
}

-(UIImageView *)imageView{
    if (_imageView==nil) {
        _imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pop_search"]];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_view addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
        }];
    }
    return _imageView;
}


-(UIButton *)leftBtn{
    if (!_leftBtn) {
        _leftBtn=[UIButton new];
        [_leftBtn setTitleColor:Black102 forState:(UIControlStateNormal)];
        [_leftBtn setTitle:@"取消" forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(_leftBtn, clickCancel);
        _leftBtn.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
        [_view addSubview:_leftBtn];
        [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.offset(0);
            make.height.offset(40*HWB);
            make.width.equalTo(_view.mas_width).multipliedBy(0.5);
        }];
        
        _rightBtn=[UIButton new];
        [_rightBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        [_rightBtn setTitle:@"搜索" forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(_rightBtn, clickSearch);
        _rightBtn.titleLabel.font=[UIFont systemFontOfSize:13*HWB];
        [_view addSubview:_rightBtn];
        [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.offset(0);
            make.height.offset(40*HWB);
            make.width.equalTo(_view.mas_width).multipliedBy(0.5);
        }];
        
        UIView* centerLine=[UIView new];
        centerLine.backgroundColor=COLORGROUP;
        [_view addSubview:centerLine];
        [centerLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_leftBtn.mas_right);
            make.height.offset(32*HWB);
            make.centerY.equalTo(_leftBtn.mas_centerY);
            make.width.offset(1);
        }];
        
        UIView* topLine=[UIView new];
        topLine.backgroundColor=COLORGROUP;
        [_view addSubview:topLine];
        [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_leftBtn.mas_top);
            make.height.offset(1);
            make.centerX.equalTo(_view.mas_centerX);
            make.width.equalTo(_view.mas_width).multipliedBy(0.9);
        }];
    }
    return _leftBtn;
}


-(UILabel *)text{
    if (!_text) {
        _text=[UILabel new];
        _text.text=@"暂无搜索内容";
        _text.textColor=Black102;
        _text.font=[UIFont systemFontOfSize:12*HWB];
        _text.numberOfLines=0;
        _text.textAlignment=NSTextAlignmentCenter;
        [_view addSubview:_text];
        [_text mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(30*HWB);
            make.right.offset(-30*HWB);
            make.top.equalTo(_imageView.mas_bottom).offset(0);
            make.bottom.equalTo(_leftBtn.mas_top).offset(0);
        }];
    }
    return _text;
}

-(void)clickCancel{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha=0;
    } completion:^(BOOL finished) {
        self.hidden=YES;
    }];
    
    
}


-(void)clickSearch{
    SearchGoodsVC * searchResults = [[SearchGoodsVC alloc]init] ;
    searchResults.hidesBottomBarWhenPushed = YES ;
    searchResults.searchType = 1;
    searchResults.searchStr = _text.text ;
    NSMutableArray* arr=[NSUDTakeData(SearchOld) mutableCopy];
    if (arr.count==0||arr==nil) {
        arr=[NSMutableArray array];
    }
    [arr addObject:_text.text];
    NSUDSaveData(arr, SearchOld);
    [TopNavc pushViewController:searchResults animated:YES];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha=0;
    } completion:^(BOOL finished) {
        self.hidden=YES;
    }];
}








@end
