//
//  ClipboardMonitoring.h
//  DingDingYang
//
//  Created by ddy on 2017/12/4.
//  Copyright © 2017年 ddy.All rights reserved.
//  粘贴板检测

#import <Foundation/Foundation.h>

@interface ClipboardMonitoring : NSObject

+(void)pasteboard:(NSString*)str ;

/**
  请求粘贴板
 */
+(void)requestDataForClipboard ;

/**
 获取粘贴板文字，跟后台判断条件匹配，
 */
+(void)getPasteboardAndDetect ;

@end
