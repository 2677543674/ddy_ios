//
//  ClipboardMonitoring.m
//  DingDingYang
//
//  Created by ddy on 2017/12/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define PasteboardString    @"JF_PasteboardString"
#define ClipboardRules      @"JF_ClipboardRules"
#define TimeClipboardRules  @"Last_time_jf_clipboard_rules"
#import "ClipboardMonitoring.h"
#import "SearchGoodsVC.h"
#import "PopSearchView.h"

@implementation ClipboardMonitoring
+(void)pasteboard:(NSString*)str{
    if (str!=nil) {
        [UIPasteboard generalPasteboard].string = str ;
        LOG(@"复制成功的内容：\n\n%@",[UIPasteboard generalPasteboard].string);
        NSUDSaveData(str, PasteboardString)
    }else{
        NSLog(@"复制的内容为空，无法复制!!!!!!!");
    }
}

+(void)requestDataForClipboard {
    [NetRequest requestTokenURL:url_clipboard_rules parameter:nil success:^(NSDictionary *nwdic) {
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
            NSUDSaveData(nwdic ,ClipboardRules)
            // NSUDSaveData(NowTimeByFormat(@"YYYY-MM-dd"),TimeClipboardRules)
        }
    } failure:^(NSString *failure) { }];
}

+(void)getPasteboardAndDetect{
    LOG(@"\n\n获取到粘贴板的内容:\n\n%@\n",[UIPasteboard generalPasteboard].string)
    if ([UIPasteboard generalPasteboard].string!=nil&&![NSUDTakeData(PasteboardString) isEqualToString:[UIPasteboard generalPasteboard].string]) {
        [self parseData:NSUDTakeData(ClipboardRules)];
    }else{
        NSLog(@"粘贴板的内容为空，或者粘贴板的内容和上次复制的一样");
    }
}


//解析粘贴版判断规则并弹窗
+(void)parseData:(NSDictionary*)dic{
    
    NSArray * listAry = dic[@"list"] ;
    NSString * pasteboardStr = [UIPasteboard generalPasteboard].string;
    if (REPLACENULL(pasteboardStr).length==0) { return;  }
    // 弹框只弹一次 并且复制相同内容不弹
    if ([NSUDTakeData(PasteboardString) isEqualToString:pasteboardStr]) { return; }
    
    @try {
        NSLog(@"开始解析粘贴板规则。。。");
        NSMutableArray * allMutableAry = [NSMutableArray array];
        [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSMutableArray * mutableAryOne = [NSMutableArray array] ;
            
            NSArray * objAry = (NSArray*)obj;
            if (objAry.count>0) {
                NSString * groupId = REPLACENULL(objAry[0][@"groupId"]);
                [objAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSInteger act = [REPLACENULL(obj[@"act"]) integerValue];
                    NSString * valueStr = REPLACENULL(obj[@"val"]) ;
                    NSString * isture = @"0" ;  // 是否成立
                    switch (act) {
                        // 判断条件：包括
                        case 1:{ if ([pasteboardStr containsString:valueStr]) { isture = @"1"; } }break;
                        // 判断条件：以此开头
                        case 2:{ if ([pasteboardStr hasPrefix:valueStr]) { isture = @"1"; } }break;
                        // 判断条件：以此结尾
                        case 3:{ if ([pasteboardStr hasSuffix:valueStr]) { isture = @"1"; } } break;
                        default:  break;
                    }
                    if (valueStr.length==0) { isture = @"1"; }
                    [mutableAryOne addObject:isture];
                }];
                
//                LOG(@"第%lu组对比结果：\n%@",(unsigned long)idx,mutableAryOne);
                //用结果集筛选，如果剩余数组个数为1个并且值为1，则判断成立
                NSSet * set = [NSSet setWithArray:mutableAryOne];
                if ([set allObjects].count>0) {
                    if ([set allObjects].count==1&&[[set allObjects] containsObject:@"1"]) {
                        [allMutableAry addObject:groupId];
                    }
                }
            }
        }];
        
        NSUDSaveData(pasteboardStr, PasteboardString)
        // 粘贴板上存在可执行的条件,
        if (allMutableAry.count>0) {
    
            NSDictionary * dic = @{@"text":pasteboardStr,@"groupId":allMutableAry[0]};
            [NetRequest requestTokenURL:url_clipboard_goods_title parameter:dic success:^(NSDictionary *nwdic) {
                if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
                    NSString * contentStr = REPLACENULL(nwdic[@"content"]);
                    NSString * title  = REPLACENULL(nwdic[@"title"]);
                    if (contentStr.length>0) {
                        if (TopNavc) {
                            
                            ResignFirstResponder
//                            [UIPasteboard generalPasteboard].string = @"";
                            [PopSearchView popSearchViewAndTitle:title];
                            
                        }else{
                            NSLog(@"导航视图为空，不能跳转界面!");
                        }
                    }else{
                        NSLog(@"返回的内容为空，服务器有问题!!!!");
                    }
                }else{
                    NSLog(@"%@",nwdic[@"result"]);
                }
            } failure:^(NSString *failure) { MBHideHUD SHOWMSG(failure)}];
        }else{
            NSLog(@"规则匹配失败,不能搜索!");
        }
    } @catch (NSException *exception) {
        NSLog(@"匹配过程出现bug");
    } @finally {
        
    }
}

@end
















