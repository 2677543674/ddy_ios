//
//  PopSearchView.h
//  DingDingYang
//
//  Created by ddy on 2018/7/11.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopSearchView : UIView

@property(nonatomic,strong)UIView* view;
@property(nonatomic,strong)UIImageView* imageView;
@property(nonatomic,strong)UILabel* text;
@property(nonatomic,strong)UIButton* leftBtn;
@property(nonatomic,strong)UIButton* rightBtn;
+(PopSearchView*)popSearchViewAndTitle:(NSString*)title;
@end
