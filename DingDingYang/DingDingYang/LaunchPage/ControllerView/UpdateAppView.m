//
//  UpdateAppView.m
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2018/5/16.
//

#import "UpdateAppView.h"

@implementation UpdateAppView

+(void)showUpdateType:(NSInteger)showType updateContent:(NSString*)content updateUrl:(NSString*)downLoadUrl newVersion:(NSString*)version{
    UIWindow * keyWindow = [UIApplication sharedApplication].keyWindow;
    if (keyWindow) {
        UpdateAppView * updateView = [[UpdateAppView alloc]initWithType:showType content:content version:version];
        updateView.frame = CGRectMake(0, 0, ScreenW, ScreenH);
        updateView.downLoadUrl = downLoadUrl;
//        updateView.appnNewVersion = version;
        [keyWindow addSubview:updateView];
        [updateView showSelfAnimation];
    }
}

-(instancetype)initWithType:(NSInteger)type content:(NSString*)content version:(NSString*)version{
    self = [super init];
    if (self) {
        
        self.updateType = type;
        self.updateContent = content;
        self.appnNewVersion = version;
        
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:2];
        NSDictionary * attbDic = @{NSFontAttributeName:[UIFont systemFontOfSize:13*HWB],
                                   NSForegroundColorAttributeName:Black102,
                                   NSParagraphStyleAttributeName:paragraphStyle,
                                   NSKernAttributeName:@1};
        NSAttributedString * attrStrContent = [[NSAttributedString alloc]initWithString:content attributes:attbDic];

        float upVhi = AttributedStringRect(content, 210*HWB, attbDic).size.height+100*HWB;
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = COLORWHITE;
        [_bgView cornerRadius:15*HWB];
        _bgView.clipsToBounds = NO;
        [self addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.height.offset(upVhi);
            make.width.offset(240*HWB);
        }];
        
        
        // 顶部图片，标题，版本号等
        UIImageView * headTopImgV = [UIImageView imgName:@"ddy_update_head"];
        [_bgView addSubview:headTopImgV];
        [headTopImgV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_bgView.mas_top).offset(20*HWB);
            make.left.right.offset(0);
            make.height.offset(128*HWB);
        }];
        
        UILabel * titleLab = [UILabel labText:@"发现新版本" color:COLORWHITE font:16*HWB];
        titleLab.font = [UIFont boldSystemFontOfSize:16*HWB];
        [headTopImgV addSubview:titleLab];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.height.offset(20*HWB);
            make.centerY.equalTo(headTopImgV.mas_centerY);
//            .offset(-2*HWB);
        }];
        
        UILabel * versionLab = [UILabel labText:FORMATSTR(@"- %@ -",_appnNewVersion) color:COLORWHITE font:13*HWB];
        [headTopImgV addSubview:versionLab];
        [versionLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(titleLab.mas_left); make.height.offset(18*HWB);
            make.top.equalTo(titleLab.mas_bottom).offset(2*HWB);
        }];
        
        
        // 更新文案
        UILabel * contentLab = [UILabel labText:content color:Black102 font:13*HWB];
        contentLab.textAlignment = NSTextAlignmentLeft;
        contentLab.numberOfLines = 0;
        contentLab.attributedText = attrStrContent;
        [_bgView addSubview:contentLab];
        [contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.right.offset(-15*HWB);
            make.top.offset(30*HWB);
            make.bottom.offset(-65*HWB);
        }];
        
        UIButton * sureBtn = [UIButton titLe:@"立即更新" bgColor:COLORWHITE titColorN:COLORWHITE font:14*HWB];
        ADDTARGETBUTTON(sureBtn, clickUpdate) [sureBtn cornerRadius:16*HWB];
        [sureBtn setBackgroundImage:[UIImage imageNamed:@"home_search_bgimg"] forState:(UIControlStateNormal)];
        [_bgView addSubview:sureBtn];
        if (_updateType==1) { // 强制更新
            [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.offset(100*HWB);
                make.centerX.equalTo(_bgView.mas_centerX);
                make.bottom.offset(-15*HWB);
                make.height.offset(32*HWB);
            }];
        }else{ // 非强制更新，显示取消按钮
            UIButton * cancelBtn = [UIButton titLe:@"取消" bgColor:COLORWHITE titColorN:LOGOCOLOR font:14*HWB];
            [cancelBtn boardWidth:1 boardColor:LOGOCOLOR corner:16*HWB];
            ADDTARGETBUTTON(cancelBtn, clickRemoveSelf)
            [_bgView addSubview:cancelBtn];
            [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(-15*HWB);
                make.left.offset(20*HWB); make.height.offset(32*HWB);
                make.right.equalTo(_bgView.mas_centerX).offset(-10*HWB);
            }];
            
            [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(-15*HWB);
                make.right.offset(-20*HWB); make.height.offset(32*HWB);
                make.left.equalTo(_bgView.mas_centerX).offset(10*HWB);
            }];
            
//            UIImageView * linesBtnCImgv = [[UIImageView alloc]init];
//            linesBtnCImgv.backgroundColor = COLORGROUP;
//            [_bgView addSubview:linesBtnCImgv];
//            [linesBtnCImgv mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.height.offset(20*HWB); make.width.offset(0.5);
//                make.centerX.equalTo(_bgView.mas_centerX);
//                make.centerY.equalTo(cancelBtn.mas_centerY);
//            }];
        }
        
//        UIImageView * linesImgbot = [[UIImageView alloc]init];
//        linesImgbot.backgroundColor = COLORGROUP;
//        [_bgView addSubview:linesImgbot];
//        [linesImgbot mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.offset(-38*HWB);
//            make.left.right.offset(0);
//            make.height.offset(0.5);
//        }];
        
        _bgView.alpha = 0;
        _bgView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        
    }
    return self;
}

-(void)showSelfAnimation{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = RGBA(0, 0, 0,0.4);
        _bgView.alpha = 1;
        _bgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) { }];
}

-(void)clickRemoveSelf{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = COLORCLEAR;
        _bgView.alpha = 0;
//        _bgView.transform = CGAffineTransformMakeScale(0.7, 0.7);
    } completion:^(BOOL finished) { [self removeFromSuperview]; }];
}


-(void)clickUpdate{
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_downLoadUrl]];
}


@end
