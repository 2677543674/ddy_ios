//
//  PopupWindowView.h
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2017/12/5.
//  Copyright © 2018年 哈哈 All rights reserved.
//  首页弹出框广告

#import <UIKit/UIKit.h>
#import <YYWebImage.h>
#import "ActivityModel.h"

@interface PopupWindowView : UIView

+(void)showAlertIfHaveAd;


@property(nonatomic,strong)ActivityModel * model;

- (instancetype)initWithFrame:(CGRect)frame andModel:(ActivityModel*)model;

@property(nonatomic,strong) UIView * backView;
@property(nonatomic,strong) YYAnimatedImageView * alertImgView;
@property(nonatomic,strong) UIButton * clickCloseBtn;

@end
