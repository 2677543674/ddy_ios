//
//  LaunchAdView.h
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2017/12/5.
//  Copyright © 2018年 哈哈 All rights reserved.
//  闪屏页广告

#import <UIKit/UIKit.h>
#import "ActivityModel.h"

@interface LaunchAdView : UIView

+(void)showAdView:(UIView*)supView adimg:(UIImage*)adimg jumpModel:(ActivityModel*)model;

-(instancetype)initWithFrame:(CGRect)frame showImg:(UIImage*)adimg jumpModel:(ActivityModel*)model;

@property(nonatomic,strong) UIImageView * adImageView; // 广告图片
@property(nonatomic,strong) UIButton * jumpBtn;
@property(nonatomic,strong) ActivityModel * adModel; // 广告model

@property(nonatomic,assign) NSInteger sec;
@property(nonatomic,strong) NSTimer * timer;

@end
