//
//  LaunchAdView.m
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2017/12/5.
//  Copyright © 2018年 哈哈 All rights reserved.

//  启动页广告显示

#import "LaunchAdView.h"
#import "PopupWindowView.h"

@interface LaunchAdView ()

@end

@implementation LaunchAdView

+(void)showAdView:(UIView*)supView adimg:(UIImage*)adimg jumpModel:(ActivityModel*)model{
    LaunchAdView * popAdView = [[LaunchAdView alloc]initWithFrame:supView.frame showImg:adimg jumpModel:model];
    [supView addSubview:popAdView];
    [popAdView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.offset(0);
    }];
}


-(instancetype)initWithFrame:(CGRect)frame showImg:(UIImage*)adimg jumpModel:(ActivityModel*)model{
    if (self) {
        self = [super initWithFrame:frame];
        
        self.adModel = model;
        self.sec = 3;
    
        _adImageView = [[UIImageView alloc]init];
        _adImageView.image = adimg;
        _adImageView.backgroundColor = COLORWHITE;
        _adImageView.userInteractionEnabled = YES;
        _adImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_adImageView];
        [_adImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.top.offset(0);
        }];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewTap)];
        [_adImageView addGestureRecognizer:tap];
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
        
        _jumpBtn = [UIButton titLe:FORMATSTR(@"跳过(%lds)",(long)_sec) bgColor:RGBA(0, 0, 0, 0.6) titColorN:COLORWHITE font:12*HWB];
        ADDTARGETBUTTON(_jumpBtn, clickJumpAd)
        [_jumpBtn corner:12*HWB];
        [self addSubview:_jumpBtn];
        [_jumpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-16*HWB);
            make.top.offset(IS_IPHONE_X?40*HWB:30*HWB);
            make.width.offset(60*HWB); make.height.offset(24*HWB);
        }];
    
    }
    return self;
}

// 定时器执行
-(void)timerFired{
    @try {
        if (_sec>0) {
            _sec = _sec-1;
            [_jumpBtn setTitle:FORMATSTR(@"跳过(%lds)",(long)_sec) forState:UIControlStateNormal];
        }else{
            [self removeSelf];
        }
    } @catch (NSException *exception) {
        [self removeSelf];
    } @finally { }
}

// 点击了广告图片
-(void)imageViewTap{
    [self removeSelf];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [PageJump pushToActivity:_adModel];
    });
}
// 点击了跳过广告
-(void)clickJumpAd{
    [self removeSelf];
    [[NSNotificationCenter defaultCenter] postNotificationName:ClickPop object:nil];
}

-(void)removeSelf{
    [_timer invalidate];
    [UIView animateWithDuration:0.2 animations:^{
        _adImageView.alpha = 0;
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end

