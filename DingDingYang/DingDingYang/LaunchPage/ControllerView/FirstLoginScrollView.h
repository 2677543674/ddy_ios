//
//  FirstLoginScrollView.h
//  DingDingYang
//
//  Created by ddy on 13/10/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstLoginScrollView : UIView <UIScrollViewDelegate>

//@property(nonatomic,strong)UIButton *btn;
//@property (strong , nonatomic) NSArray *imageArr;
//@property (strong , nonatomic) UIScrollView *scrollView;
//-(instancetype)initWithFrame:(CGRect)frame withImageArray:(NSArray *)imageArray;


+(void)showFirstLaunch:(UIView*)supView;

@property(nonatomic,strong) UIScrollView * scrollView;
@property(nonatomic,strong) UIPageControl * pageControl;
@property(nonatomic,strong) UIButton * removeBtn; // 移除按钮
@property(nonatomic,strong) UIButton * jumpBtn;   // 跳过按钮
@property(nonatomic,strong) NSArray * imgNameAry;
@property(nonatomic,strong) UILabel * ljtyLab; // 立即体验lab

@end
