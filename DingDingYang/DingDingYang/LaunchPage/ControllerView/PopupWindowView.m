//
//  PopupWindowView.m
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2017/12/5.
//  Copyright © 2018年 哈哈 All rights reserved.
//  首页弹出框广告

#import "PopupWindowView.h"

@interface PopupWindowView ()<UIGestureRecognizerDelegate>

@property(nonatomic,strong)NSMutableArray* dataArr;

@end

@implementation PopupWindowView

+(void)showAlertIfHaveAd{
    // 显示引导页的话，就不显示广告
    if (NSUDTakeData(ad_popView_dic)) {
        NSLog(@"有弹窗广告。。。。。。");
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            ActivityModel * adModel = [[ActivityModel alloc]initWithDic:NSUDTakeData(ad_popView_dic)];
            PopupWindowView * altView = [[PopupWindowView alloc]initWithFrame:CGRectZero andModel:adModel];
            UIWindow * keyWindow = [UIApplication sharedApplication].keyWindow;
            [keyWindow addSubview:altView];
            [altView showImgAndAnimation];
        });
    }else{
        [ClipboardMonitoring getPasteboardAndDetect];
    }
}


- (instancetype)initWithFrame:(CGRect)frame andModel:(ActivityModel*)model{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.model = model;
        
        self.backgroundColor = COLORCLEAR;
    
        [self backView];
        [self alertImgView];
        [self clickCloseBtn];
    
//        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickBackView)];
//        [self addGestureRecognizer:tap];
        
    }
    return self;
}

#pragma mark ===>>> 创建控件
-(UIView*)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.alpha = 0;
        _backView.clipsToBounds = YES;
        _backView.frame = CGRectZero;
        [self addSubview:_backView];
    }
    return _backView;
}

-(YYAnimatedImageView*)alertImgView{
    if (!_alertImgView) {
        _alertImgView = [[YYAnimatedImageView alloc]init];
        _alertImgView.userInteractionEnabled = YES;
        _alertImgView.contentMode = UIViewContentModeScaleAspectFit;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewTap)];
        [_alertImgView addGestureRecognizer:tap];
        [_backView addSubview:_alertImgView];
    }
    return _alertImgView;
}

-(UIButton*)clickCloseBtn{
    if (!_clickCloseBtn) {
        _clickCloseBtn = [UIButton bgImage:@"gdfdelete"];
        [_backView addSubview:_clickCloseBtn];
        ADDTARGETBUTTON(_clickCloseBtn, clickBackView)
        [_backView addSubview:_clickCloseBtn];
    }
    return _clickCloseBtn;
}


#pragma mark ===>>> 显示和移除

// 下载图片并展示
-(void)showImgAndAnimation{
    
    __block PopupWindowView * selfview = self;
    [_alertImgView yy_setImageWithURL:[NSURL URLWithString:_model.img] placeholder:nil options:YYWebImageOptionProgressiveBlur |YYWebImageOptionShowNetworkActivity progress:nil transform:nil completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        @try {
            if (image) {
                
                [selfview mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.bottom.left.right.offset(0);
                }];
                
                UIImage * altImage = image;
                LOG(@"首页弹窗广告图片: \n%@",altImage)
                float imgH = altImage.size.height;
                float imgW = altImage.size.width;
                float hwbl = imgH/imgW; // 宽高比
                if (imgW>(ScreenW-24*HWB)) {
                    imgW = ScreenW-24*HWB;
                    imgH = imgW*hwbl;
                }else{
                    imgW = imgW+24*HWB;
                    imgH = imgH+24*HWB;
                }
                NSLog(@"宽:%f, 高:%f",imgW,imgH);
                float viewH = imgH;
                float viewW = imgW;
                
                [selfview.backView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(selfview);
                    make.height.offset(viewH);
                    make.width.offset(viewW);
                }];
                
                [selfview.alertImgView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(selfview.backView);
                    make.width.offset(viewW-24*HWB);
                    make.height.offset(viewH-24*HWB);
                }];
                
                [_clickCloseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.right.offset(0);
                    make.width.height.offset(24*HWB);
                }];
                
                selfview.backView.transform = CGAffineTransformMakeScale(0.6, 0.6);
                
                if ([TopNavc.topViewController isKindOfClass:[HomeVC class]]) {
                    [UIView animateWithDuration:0.3 animations:^{
                        selfview.backgroundColor = RGBA(0, 0, 0, 0.4);
                        selfview.backView.alpha = 1;
                        selfview.backView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                    } completion:^(BOOL finished) {
                        if (selfview.model.type==2) { // 只弹一次的广告
                            NSUDSaveData(selfview.model.img, ad_popView_imgUrl)
                            NSUDRemoveData(ad_popView_dic)
                        }
                    }];
                }else{
                    [selfview removeFromSuperview];
                }
                
            }else{ [selfview removeFromSuperview]; }
        } @catch (NSException *exception) {
            [selfview removeFromSuperview];
        } @finally { }
    }];
}

-(void)clickBackView{
    [UIView animateWithDuration:0.30 animations:^{
        self.backgroundColor = COLORCLEAR;
        self.backView.alpha = 0;
        self.backView.transform = CGAffineTransformMakeScale(1.1, 1.1);
    } completion:^(BOOL finished) {
        [self.superview removeFromSuperview];
        [self removeFromSuperview];
    }];
}

-(void)imageViewTap{
    [PageJump pushToActivity:_model];
    [self.superview removeFromSuperview];
    [self removeFromSuperview];
}



@end
