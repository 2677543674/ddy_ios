//
//  FirstLoginScrollView.m
//  DingDingYang
//
//  Created by ddy on 13/10/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "FirstLoginScrollView.h"

@implementation FirstLoginScrollView


+(void)showFirstLaunch:(UIView*)supView{
    FirstLoginScrollView * firstView = [[FirstLoginScrollView alloc]init];
    firstView.frame = CGRectMake(0, 0, ScreenW, ScreenH);
    [supView addSubview:firstView];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self) {
        self = [super initWithFrame:frame];
        
        _imgNameAry = @[@"引导页1",@"引导页2",@"引导页3"];
        
        // 省见适配iPhoneX
        if (PID==10025) {
            if (IS_IPHONE_X) {
                _imgNameAry = @[@"iphonex1",@"iphonex2",@"iphonex3"];
            }
        }
       
        // 必转
        if (PID==10108) {  _imgNameAry = @[@"引导1",@"引导2"]; }
        
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(ScreenW*_imgNameAry.count, ScreenH);
        [self addSubview:_scrollView];
        _scrollView.frame = CGRectMake(0, 0, ScreenW, ScreenH);
//        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.left.bottom.right.offset(0);
//        }];
        
        for (int i=0; i<_imgNameAry.count; i++) {
            UIImageView * imageView = [[UIImageView alloc]init];
            imageView.backgroundColor = [UIColor whiteColor];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            [imageView setImage:[UIImage imageNamed:_imgNameAry[i]]];
            imageView.clipsToBounds = YES;
            [_scrollView addSubview:imageView];
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.offset(i*ScreenW); make.top.offset(0);
                make.height.offset(ScreenH); make.width.offset(ScreenW);
            }];
        }
        
        // 立即体验
        _removeBtn = [[UIButton alloc]init];
        _removeBtn.backgroundColor = [UIColor clearColor];
        _removeBtn.frame = CGRectMake((_imgNameAry.count-1)*ScreenW, ScreenH-ScreenW, ScreenW, ScreenW);
        [self.scrollView addSubview:_removeBtn];
        
//        _ljtyLab = [UILabel labText:@"立 即 体 验" color:LOGOCOLOR font:13*HWB];
//        _ljtyLab.textAlignment = NSTextAlignmentCenter;
//        [_ljtyLab boardWidth:1 boardColor:LOGOCOLOR cornerRadius:6*HWB];
//        [_removeBtn addSubview:_ljtyLab];
//        [_ljtyLab mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(_removeBtn.mas_centerX);
//            make.bottom.offset(IS_IPHONE_X?-80:-50*HWB);
//            make.width.offset(80*HWB); make.height.offset(30*HWB);
//        }];
        
        // 跳过
//        _jumpBtn = [UIButton titLe:@"跳过" bgColor:COLORCLEAR titColorN:Black102 font:12*HWB];
//        [_jumpBtn boardWidth:1 boardColor:Black102 corner:12*HWB];
//        [self addSubview:_jumpBtn];
//        [_jumpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.offset(-18*HWB); make.top.offset(30*HWB+NAVCBAR_HEIGHT-64);
//            make.width.offset(52*HWB); make.height.offset(24*HWB);
//        }];
        
        ADDTARGETBUTTON(_removeBtn, removeViewBtn)
//        ADDTARGETBUTTON(_jumpBtn, removeViewBtn)
     
        // 叮叮羊
        if (PID==10107) {
            UIView * bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenH-(IS_IPHONE_X?72:40), ScreenW, 30)];
            bottomView.backgroundColor = [UIColor clearColor];
            _pageControl = [[UIPageControl alloc] initWithFrame:bottomView.bounds];
            _pageControl.currentPage = 0;
            _pageControl.numberOfPages = _imgNameAry.count;
            _pageControl.currentPageIndicatorTintColor = LOGOCOLOR;
            _pageControl.pageIndicatorTintColor = COLORWHITE;
            [bottomView addSubview:_pageControl];
            [self addSubview:bottomView];
        }
       
        
    }
    return self;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x>ScreenW*(_imgNameAry.count-2)) {
        _jumpBtn.hidden = YES;
    }else{ _jumpBtn.hidden=NO; }
    
    if (_pageControl) {
        self.pageControl.currentPage = scrollView.contentOffset.x/ScreenW;
    }
}

-(void)removeViewBtn{
    
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        NSUDSaveData(now_app_version,Launch_last_app_version);
        [[NSNotificationCenter defaultCenter] postNotificationName:ClickPop object:nil];
        [self removeFromSuperview];
    }];
}


/*
-(instancetype)initWithFrame:(CGRect)frame withImageArray:(NSArray *)imageArray{
    if (self) {
        self=[super initWithFrame:frame];
        _imageArr = imageArray;
        [self createScrollViewWithImageArray:(NSArray *)imageArray];
    }
    return self;
}

-(void)createScrollViewWithImageArray:(NSArray *)imageArray{
    
    _scrollView=[[UIScrollView alloc]initWithFrame:self.frame];
    _scrollView.bounces=NO;
    _scrollView.pagingEnabled=YES;
    
    _scrollView.contentSize=CGSizeMake(ScreenW*imageArray.count, ScreenH);
    [self addSubview:_scrollView];
    for (int i=0; i<imageArray.count; i++) {
        UIImageView *imageView=[[UIImageView alloc]init];
        imageView.backgroundColor=[UIColor whiteColor];
        imageView.clipsToBounds = YES;
        imageView.contentMode=UIViewContentModeScaleAspectFill;
        [imageView setImage:[UIImage imageNamed:imageArray[i]]];
        [_scrollView addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.height.offset(ScreenH);
            make.width.offset(ScreenW);
            make.left.offset(i*ScreenW);
        }];
    }
    
    
    
    self.btn=[[UIButton alloc]init];
    self.btn.backgroundColor = [UIColor clearColor];
    [self.btn addTarget:self action:@selector(removeViewBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn setTitleColor:COLORWHITE forState:(UIControlState)UIControlStateNormal];
    [self.scrollView addSubview:self.btn];
    
    self.btn.frame = CGRectMake((imageArray.count-1)*self.frame.size.width, 0, self.frame.size.width, self.frame.size.height);
//    self.btn.center = CGPointMake(self.frame.size.width*(imageArray.count-1)+ self.frame.size.width/2, self.frame.size.height-65*HWB);
//    self.btn.layer.masksToBounds = YES;
//    self.btn.layer.cornerRadius = self.btn.frame.size.height/2;
//    self.btn.layer.borderWidth = 1.0;
//    self.btn.layer.borderColor=COLORWHITE.CGColor;
//    [self.btn setTitle:@"开始体验" forState:UIControlStateNormal];
    
}

-(void)removeViewBtn:(UIButton *)sender{

    [[NSUserDefaults standardUserDefaults]  setValue:TheApp_Version forKey:APP_VersionValue];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha=0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
*/
@end

