//
//  UpdateAppView.h
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2018/5/16.
//

#import <UIKit/UIKit.h>

@interface UpdateAppView : UIView



/**
 显示提示更新界面

 @param showType  0:不强制更新  1:强制更新
 @param content   更新内容
 @param downLoadUrl 下载链接
 @param newVersion 新的版本号
 */
+(void)showUpdateType:(NSInteger)showType updateContent:(NSString*)content updateUrl:(NSString*)downLoadUrl newVersion:(NSString*)version;

@property(nonatomic,copy) NSString * appnNewVersion;
@property(nonatomic,copy) NSString * downLoadUrl;
@property(nonatomic,copy) NSString * updateContent;
@property(nonatomic,assign) NSInteger updateType;
@property(nonatomic,strong) UIView * bgView;

@end
