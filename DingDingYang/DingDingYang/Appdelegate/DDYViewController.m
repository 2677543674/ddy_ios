//
//  DDYViewController.m
//  DingDingYang
//
//  Created by ddy on 26/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface DDYViewController ()

@end

@implementation DDYViewController



// 视图将要出现
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem * item = [UIBarButtonItem appearance];
    NSDictionary * attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.01],
                                  NSForegroundColorAttributeName:[UIColor clearColor]};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [MobClick beginLogPageView:self.title==nil?@"未设置标题":self.title];
}

// 视图已经出现
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

// 视图将要消失
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    MBHideHUD // 隐藏转圈圈控件
    [MobClick endLogPageView:self.title==nil?@"未设置标题":self.title];
    
}

// 视图已经消失
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}

-(void)dealloc{
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLORWHITE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




@end
