//
//  AppDelegate.h
//  DingDingYang
//
//  Created by ddy on 2017/3/1.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end






