//
//  MainTabBarVC.h
//  DingDingYang
//
//  Created by ddy on 2017/8/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeVC.h"
#import "CheckVoucherFirstVC.h"
#import "CouponsLiveVC.h"
#import "VideoSingleVC.h"
#import "UniversalGoodsListVC.h"
#import "ClassificationVC.h"
#import "TBNewsViewController.h"
#import "JingDongGoodsVC.h"
#import "NewOrmdVC.h"
#import "NewRoadVC.h"
#import "MyFirstNewVC.h"
#import "FreeOrderVC.h"
#import "WebVC.h"
#import "MomentsVC.h"
#import "UnGoodsListVC.h"
#import "WPHGoodsListVC.h"
#import "UnGoodsClasslistBigVC.h"


@interface MainTabBarVC : UIViewController <UITabBarDelegate>

@property(nonatomic,strong) UITabBarController * tabbarController;
@property(nonatomic,strong) NSDictionary * launchdic ; //程序启动获取到的推送消息(appdelegate传值进来的)

//首页、 必须有 、 位置: tabbarController索引第一个一个
@property(nonatomic,strong) HomeVC * homeVC ;
@property(nonatomic,strong) UINavigationController * navcHomeVC ;

//我的、 必须有 、 位置: tabbarController索引最后一个
@property(nonatomic,strong) MyFirstNewVC * myfirstNewVC ;
@property(nonatomic,strong) UINavigationController * navcMyfirstNewVC ;


#pragma mark ==>>  动态可供选择的 (首先需要有一个默认的)

@property(nonatomic,strong) UniversalGoodsListVC * brandVC ;
@property(nonatomic,strong) UINavigationController * navcBrandVC ;

@property(nonatomic,strong) UniversalGoodsListVC * findVC ;
@property(nonatomic,strong) UINavigationController * navcFindVC ;

@property(nonatomic,strong) UniversalGoodsListVC * uglVC ;
@property(nonatomic,strong) UINavigationController * navcUglVC ;

@property(nonatomic,strong) UniversalGoodsListVC * todayRec ; //今日推荐
@property(nonatomic,strong) UINavigationController * navcTodayRec ;

@property(nonatomic,strong) UniversalGoodsListVC * todayRec2 ; //今日必推
@property(nonatomic,strong) UINavigationController * navcTodayRec2 ;

@property(nonatomic,strong) UniversalGoodsListVC * bargainVC ;
@property(nonatomic,strong) UINavigationController * navcBargainVC ;

@property(nonatomic,strong) VideoSingleVC * videoVC ;
@property(nonatomic,strong) UINavigationController * navcVideoVC ;

@property(nonatomic,strong) CouponsLiveVC * cliveVC ;
@property(nonatomic,strong) UINavigationController * navcCliveVC ;


@property(nonatomic,strong) ClassificationVC * classVC ;
@property(nonatomic,strong) UINavigationController * navcClassVC ;

@property(nonatomic,strong) TBNewsViewController * tbNewsVC ;
@property(nonatomic,strong) UINavigationController * navcTbNewsVC ;


@property(nonatomic,strong) UnGoodsClasslistBigVC * JDVC ;
@property(nonatomic,strong) UINavigationController * navcJDVC ;

@property(nonatomic,strong) NewOrmdVC * ormdNewVC ;
@property(nonatomic,strong) UINavigationController * navcOrmdNewVC ;


@property(nonatomic,strong) NewRoadVC * onroad; //新手上路
@property(nonatomic,strong) UINavigationController * navcOnroadVC ;


@property(nonatomic,strong) FreeOrderVC * freeOrderVC;
@property(nonatomic,strong) UINavigationController * navcFreeOrderVC ;


@property(nonatomic,strong) WebVC * webVC ; //H5
@property(nonatomic,strong) UINavigationController * navcWebVC ;

@property(nonatomic,strong) MomentsVC *momentsVC ; // 键发圈
@property(nonatomic,strong) UINavigationController * navcMomentsVC ;

@property(nonatomic,strong) UnGoodsListVC *pddListVC ; // 拼多多
@property(nonatomic,strong) UINavigationController * navcPddListVC ;

#pragma mark --> 查券，已废弃
@property(nonatomic,strong) CheckVoucherFirstVC * cvfirstVC ;
@property(nonatomic,strong) UINavigationController * navcCvfirstVC ;

@property(nonatomic,strong) WPHGoodsListVC * wphVC ;
@property(nonatomic,strong) UINavigationController * navcWPHVC ;

@end


