//
//  AppDelegate.m
//  DingDingYang
//
//  Created by ddy on 2017/3/1.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "AppDelegate.h"
#import <AlipaySDK/AlipaySDK.h>

#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
#import <CommonCrypto/CommonDigest.h>
#import "MainTabBarVC.h"

// 融云

#import "RongcloudListViewController.h"

#import <WXApi.h>
#import "BdWechatOrTbVC.h"

// 粘贴板监控
#import "ClipboardMonitoring.h"

#import "RongcloudListViewController.h"
#import "PopSearchView.h"

#import "LZMyCenterInfoServer.h"
#import "APPInfo.h"
#import "LYAgreementActionView.h"

@interface AppDelegate () <UNUserNotificationCenterDelegate,WXApiDelegate,JPUSHRegisterDelegate>

@property(nonatomic,strong) NSDictionary * launchpushDic ;

@end



@implementation AppDelegate{
    // iOS 10通知中心
    UNUserNotificationCenter * _notificationCenter;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window.tintColor = LOGOCOLOR;
    self.window.backgroundColor = COLORWHITE;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];

    // 网络变化监控
    [AFShareManager networkMonitoring];

    [NetRequest getDynamicDNS]; // 动态域名

    if (launchOptions!=nil) {
        // 从通知栏消息启动app 获取推送内容
        _launchpushDic = NULLDIC(launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]) ;
    }

    [self gotoMain];
    
    // 注册融云
    [self registerRongCloud];
    // 友盟分享
    [self configUSharePlatforms];
    
    // 友盟统计
    [self registUMtj];
    
    // 极光推送
    [self registJGPush:launchOptions];
    
    // app每次启动请求一次 app开放的接口
    [self getAppOpenStatus];
    
    // app每次启动时请求一次 (粘贴板检测规则)
    [ClipboardMonitoring requestDataForClipboard];
    
    // 百川电商SDK
    if (TBKEY.length>0) { [self aLiBaiChuanSDK]; }
    
    // 京东SDK
    if (IsNeedJingDong==1) { [self jingDongSDK]; }
    
    [[LZMyCenterInfoServer shareInstance] requestCommission];
    
    return YES;
    
}

#pragma mark ===>>> APP开放的接口接口类型
-(void)getAppOpenStatus{

    // 1:每日利息增长推送 2:批量分享海报 3:新增用户推送(消息及通知) 4:新增订单推送(消息及通知)
    [NetRequest requestType:0 url:url_app_shareStatus ptdic:nil success:^(id nwdic) {
        NSArray * list = NULLARY(nwdic[@"list"]);
        for (int i=0; i<list.count; i++) {
            NSDictionary* dic=list[i];
            if ([REPLACENULL(dic[@"status"]) integerValue]==1) {
                switch ([REPLACENULL(dic[@"type"]) integerValue]) {
                    case 1: { NSUDSaveData(@"open", InterestStatus) } break;
                    case 2: { NSUDSaveData(@"open", ShareStatus) } break;
                    case 3: { NSUDSaveData(@"open", TeamStatus) } break;
                    case 4: { NSUDSaveData(@"open", OrderStatus) } break;
                    default: break;
                }
            }
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        
    }];

}


#pragma mark ===>>>  京东开普勒SDK
-(void)jingDongSDK{
    [[KeplerApiManager sharedKPService]asyncInitSdk:JDKEY secretKey:JDSECRET sucessCallback:^(){
        [KeplerApiManager sharedKPService].isOpenByH5=NO;
        NSLog(@"京东SDK初始化成功");
    }failedCallback:^(NSError *error){
        NSLog(@"京东SDK初始化失败");
    }];
}


#pragma mark ===>>> 阿里百川SDK
-(void)aLiBaiChuanSDK{
    
    [[AlibcTradeSDK sharedInstance] setDebugLogOpen:true];
    
    // 百川平台基础SDK初始化，加载并初始化各个业务能力插件
    [[AlibcTradeSDK sharedInstance] asyncInitWithSuccess:^{
        NSLog(@"阿里百川初始化成功");
    } failure:^(NSError *error) {
        NSLog(@"阿里百川初始化失败: %@",error.description);
    }];
}



#pragma mark ===>>> 融云及时通讯

-(void)registerRongCloud{
    if (rongcloud_APP_Key.length>0&&NSUDTakeData(UserInfo)!=nil) {
        [[RCIM sharedRCIM] initWithAppKey:rongcloud_APP_Key];
        PersonalModel *infoModel = [[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
        if (infoModel.rongcloudToken.length==0) {  return;  }
        
        [RCIM sharedRCIM].enablePersistentUserInfoCache=YES;
        [RCIM sharedRCIM].enableMessageAttachUserInfo=YES;
        
        [[RCIM sharedRCIM] connectWithToken:infoModel.rongcloudToken  success:^(NSString *userId) {
            NSLog(@"登陆成功。当前登录的用户ID：%@", userId);
            
            [[LZMyCenterInfoServer shareInstance] requestCommission];
        } error:^(RCConnectErrorCode status) {
            NSLog(@"登陆的错误码为:%ld", (long)status);
        } tokenIncorrect:^{
            NSLog(@"token错误");
        }];
    }
}

#pragma mark ===>>>  注册极光推送

-(void)registJGPush:(NSDictionary*)launchOptions{
    
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ( SystemVersion >= 8.0) {
        if ( SystemVersion >= 10.0) {
            NSSet<UNNotificationCategory *> *categories;
            entity.categories = categories;
        }else {
            NSSet<UIUserNotificationCategory *> *categories;
            entity.categories = categories;
        }
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    [JPUSHService setupWithOption:launchOptions appKey:JGAPPKEY
                          channel:@"App Store"
                 apsForProduction:YES];
    
    //获取registrationID首次登陆时上报服务器
    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
        if(resCode == 0){
            NSLog(@"\n极光推送ID：%@\n",registrationID);
        }else{
            NSLog(@"\n极光推送ID获取失败：%d\n",resCode);
        }
    }];
    
    //添加通知（极光推送的应用内部消息）
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(networkDidReceiveMessage:) name:kJPFNetworkDidReceiveMessageNotification object:nil];
}



// DeviceToken 注册 并上报极光
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"注册的DeviceToken：%@",deviceToken);
    [JPUSHService registerDeviceToken:deviceToken];

    NSString * token = [[[[deviceToken description]
    stringByReplacingOccurrencesOfString:@"<" withString:@""]
    stringByReplacingOccurrencesOfString:@">" withString:@""]
    stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[RCIMClient sharedRCIMClient] setDeviceToken:token]; // 融云推送
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"苹果推送注册失败: %@", error);
}

// ************************   极光推送代理  *************************

// 支持 iOS 10.0 以上设备
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    
    NSDictionary * userInfo = notification.request.content.userInfo;
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        //前台运行，接收到推送消息后执行 (*暂时不处理*)
        NSLog(@"\n\nIOS10以上系统前台运行接收到消息：\n%@\n\n",userInfo);
        if ([userInfo[@"type"] integerValue]==2) {
            [self showBadgeOnItemIndex:4];
        }
        
    }else {
        //判断为本地通知、 并执行本地通知方法
    }
    // 系统要求执行这个方法
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);
    
}


// iOS 10.0 以上使用
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    NSDictionary * userInfo = response.notification.request.content.userInfo;

    NSLog(@"\n\nIOS10以上系统点击通知栏的推送消息：\n%@\n\n",userInfo);
    if ([NULLDIC(userInfo[@"rc"]) allKeys].count>0) {
        [PageJump pushChat];
        
    }
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"hide_shareview" object:nil];
        //点击通知栏推送内容会执行此方法(前台后台都一样)
        NSInteger type = [REPLACENULL(userInfo[@"type"]) integerValue] ;
        if (type>0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self pushType:type content:REPLACENULL(userInfo[@"content"])];
            });
        }
        
    }else {
        //执行本地通知方法
    }
    
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);
    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler{
    
    SHOWMSG(@"IOS7以上应用程序内部接收到的通知")
    NSLog(@"\n\nIOS7以上应用程序内部接收到的通知：\n%@\n\n",userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hide_shareview" object:nil];
    
    [JPUSHService handleRemoteNotification:userInfo];
    
    completionHandler(UIBackgroundFetchResultNewData);
    
    NSInteger type = [REPLACENULL(userInfo[@"type"]) integerValue] ;
    
    if (type>0) {
        if ([UIApplication sharedApplication].applicationState!=UIApplicationStateActive) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self pushType:type content:REPLACENULL(userInfo[@"content"])];
            });
            
        }else{
            
            id  dicStr = REPLACENULL(userInfo[@"aps"][@"alert"]) ;
            if (TopNavc.topViewController!=nil) {
                NSString * atitle = @"" ;
                if ([dicStr isKindOfClass:[NSDictionary class]]) {
                    atitle =  REPLACENULL(dicStr[@"title"]) ;
                }
                if (atitle.length==0) {
                    switch (type) {
                        case 1: { atitle = @"您有新的系统消息" ; } break;
                        case 2: { atitle = @"您有新的奖励消息" ; } break;
                        case 3: { atitle = @"好商品开抢啦" ; } break;
                        case 4: { atitle = @"0元抢好货" ; } break;
                        case 5: { atitle = @"商品推荐" ; } break;
                        default: { atitle = @"精彩推荐" ; } break;
                    }
                }
                NSString * bodyStr = @"" ;
                if ([dicStr isKindOfClass:[NSDictionary class]]) {
                    bodyStr = REPLACENULL(dicStr[@"body"]) ;
                }else{
                    bodyStr = REPLACENULL(dicStr);
                }
                NSLog(@"%@",bodyStr);
                [UIAlertController showIn:TopNavc.topViewController title:atitle  content:bodyStr ctAlignment:NSTextAlignmentCenter btnAry:@[@"关闭",@"查看"] indexAction:^(NSInteger indexTag) {
                    if (indexTag==1) {
                        [self pushType:type content:REPLACENULL(userInfo[@"content"])];
                    }
                }];
            }
        }
    }
}



//接收自定义消息（不走APNs）
- (void)networkDidReceiveMessage:(NSNotification *)notification {
    NSLog(@"长连接前台消息，暂不处理!");
    //    NSDictionary * userInfo = [notification userInfo];
    //    NSString *content = [userInfo valueForKey:@"content"];
    //    NSDictionary *extras = [userInfo valueForKey:@"extras"];
    //    NSString *customizeField1 = [extras valueForKey:@"customizeField1"]; //服务端传递的Extras附加字段，key是自己定义的
    //    ALERT(FORMATSTR( @"\n%s\n前台运行接收自定义消息类型(非APNS):\n内容content:%@\n extras：%@\ncustomizeField1%@",__func__,content,extras,customizeField1 ))
    
}

#pragma mark ===>>> 注册友盟统计

-(void)registUMtj{
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    UMConfigInstance.appKey = YMAPPKEY ;
    UMConfigInstance.channelId = @"App Store";  //应用发布途径
    [MobClick startWithConfigure:UMConfigInstance]; //配置以上参数后调用此方法初始化SDK！
    [MobClick setCrashReportEnabled:YES];
    [MobClick setLogEnabled:NO];  // 友盟测试调试NO
    
}

#pragma mark  ===>>> 注册友盟的第三方分享登录
- (void)configUSharePlatforms{
    /* 是否打开调试日志 */
    [[UMSocialManager defaultManager] openLog:NO];
    
    /* 设置友盟appkey */
    [[UMSocialManager defaultManager] setUmSocialAppkey:YMAPPKEY];
    
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WXKEY appSecret:WXAS redirectURL:REDIRECTURL];
    
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:QQKEY   appSecret:QQAS redirectURL:REDIRECTURL];
    
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:WBKEY  appSecret:WBAS redirectURL:REDIRECTURL];
    
}

#pragma mark === >>> 第三方应用回调信息 (支付宝、微信分享等等...)
// IOS 9.0之前使用
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:AlipayPayRusult object:self userInfo:resultDic];
        }];
        return YES;
    }
    
    // 百川
    if (![[AlibcTradeSDK sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation]) {

    }
    
    //    /* 微信登录回调 */
    //    if ([[NSString stringWithFormat:@"%@",url] containsString:@"oauth?code"] ) {
    //        [WXApi handleOpenURL:url delegate:self];
    //        return YES;
    //    }
    
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    return result;
    
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options{
    
    // 支付宝回调
    if ([url.host isEqualToString:@"safepay"]) {
        // 跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            LOG(@"支付宝返回结果：%@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:AlipayPayRusult object:self userInfo:resultDic];
        }];
        return YES;
    }
    
    /* 百川新接口写法 */
    if (@available(iOS 9.0, *)) {
        __unused BOOL isHandledByAlibc = [[AlibcTradeSDK sharedInstance] application:app openURL:url options:options];

        if (isHandledByAlibc) {
            return true;
        }
    }
    
    /* 友盟回调 */
    if ([[UMSocialManager defaultManager] handleOpenURL:url options:options]) {
        return YES ;
    }
    
    /* 微信登录回调 */
    //    if ([[NSString stringWithFormat:@"%@",url] containsString:@"oauth?code"] ) {
    //        [WXApi handleOpenURL:url delegate:self];
    //        return YES;
    //    }
    
    return YES ;
}


#pragma mark ===>>>  应用不同状态
//将要退出到后台时执行
- (void)applicationWillResignActive:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter]postNotificationName:ResignActive object:nil];
    NSLog(@"App将要进入后台");
}

//已经进入后台时执行
- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"App已经进入后台");
    [[NSNotificationCenter defaultCenter] removeObserver:EnterBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"App将要进入前台");
    [ClipboardMonitoring getPasteboardAndDetect];

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"App已经进入前台");
    [[NSNotificationCenter defaultCenter]postNotificationName:EnterBackground object:nil];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [JPUSHService resetBadge]; //重置极光角标
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"App进程将要被结束");
}


#pragma mark ===>>> 设置windows的跟视图
-(void)gotoMain {
    MainTabBarVC * tabbarVC = [[MainTabBarVC alloc]init];
    tabbarVC.launchdic = _launchpushDic ;
    self.window.rootViewController = tabbarVC ;
}

#pragma mark === >>> 根据推送类型跳转页面 /** 前提是 window.rootviewcontroller = MianTabbarVC **/
-(void)pushType:(NSInteger)type content:(NSString*)content {
    if (TopNavc!=nil) { [PageJump yunPushType:type content:content]; }
}


- (void)showBadgeOnItemIndex:(int)index{
    
    //移除之前的小红点
    [self removeBadgeOnItemIndex:index];
    //新建小红点
    UIView *badgeView = [[UIView alloc]init];
    badgeView.tag = 888 + index;
    badgeView.backgroundColor = [UIColor redColor];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if ([window.rootViewController isKindOfClass:[MainTabBarVC class]]) {
        MainTabBarVC * mian = (MainTabBarVC*)window.rootViewController ;
        UITabBarController*tabbarVC = mian.tabbarController;
        CGRect tabFrame = tabbarVC.tabBar.frame;
        //确定小红点的位置
        float percentX = (index +0.6) / 5;
        CGFloat x = ceilf(percentX * tabFrame.size.width);
        CGFloat y = ceilf(0.1 * tabFrame.size.height);
        badgeView.frame = CGRectMake(x, y, 8, 8);
        badgeView.layer.cornerRadius = badgeView.frame.size.width/2;
        [tabbarVC.tabBar addSubview:badgeView];
    }
    
}

- (void)removeBadgeOnItemIndex:(int)index{
    
    //按照tag值进行移除
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if ([window.rootViewController isKindOfClass:[MainTabBarVC class]]) {
        MainTabBarVC * mian = (MainTabBarVC*)window.rootViewController ;
        UITabBarController*tabbarVC = mian.tabbarController;
        for (UIView *subView in tabbarVC.tabBar.subviews) {
            if (subView.tag == 888+index) {
                [subView removeFromSuperview];
            }
        }
    }
    
}

@end

