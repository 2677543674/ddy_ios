//
//  ViewController.m
//  TabBarVC
//
//  Created by ddy on 2017/4/21.
//  Copyright © 2017年 ddy.All rights reserved.
//


/* 首页和个人中心不变，中间最少一个，最多三个选项 */


#import "MainTabBarVC.h"
#import "FirstLoginScrollView.h"
#import "SystemMsgModel.h"
#import "PopActivity.h"
//#import <ZipArchive.h>

@interface MainTabBarVC () <UITabBarControllerDelegate,RCIMReceiveMessageDelegate>

@property(nonatomic,assign) NSInteger lastSelectTag; // 记录上一次选中的页面
@property(nonatomic,strong) NSDictionary *markNavcDic , *markvcDic;
@property(nonatomic,strong) UIImageView *IntroImageView;
@property(nonatomic,strong) NSArray *introImageArray;
@property(nonatomic,assign) NSInteger index;

@end

@implementation MainTabBarVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)nextStep{
    _index++;
    if (_index<_introImageArray.count) {
        [_IntroImageView setImage:[UIImage imageNamed:_introImageArray[_index]]];
    }else {
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:IfFirstOpenHomeVC];
        [_IntroImageView removeFromSuperview];
    }
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self changeTabbarBgView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORWHITE;
    
    _tabbarController = [[UITabBarController alloc]init];
    _tabbarController.delegate = self;
    _tabbarController.tabBar.translucent = NO;
    _tabbarController.tabBar.tintColor = LOGOCOLOR ;
    _tabbarController.tabBar.backgroundColor = COLORWHITE ;
    if ( THEMECOLOR == 2 ) {  _tabbarController.tabBar.tintColor = Black51 ;  }
    [self.view addSubview:_tabbarController.view];
    [self addChildViewController:_tabbarController];
    [_tabbarController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0); make.bottom.offset(0);
        make.left.offset(0); make.right.offset(0);
    }];

    // 初始化所有主导航、并设置默认图片
    [self initViewControllerAndNavigationController];
    [self addBarTitleImg];
    

    // 判断有无缓存 (动态导航栏数据)
//    if (NSUDTakeData(app_dynamic_tabbar_dic)!=nil) {
//        NSString * now_time = NowTimeByFormat(@"yyyy-MM-dd") ;
//        NSString * last_time = REPLACENULL(NSUDTakeData(app_dynamic_tabbar_time));
//        NSInteger result = [now_time compare:last_time];
//        [self tabbarViewControllers:NULLDIC(NSUDTakeData(app_dynamic_tabbar_dic))];
//        if (result!=0) { [self huoQuTabbar]; }
//    }else{ [self huoQuTabbar]; }
    [self huoQuTabbar];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSelectIndex:) name:ChangeTabbarSelect object:nil];
    [RCIM sharedRCIM].receiveMessageDelegate = self;

    if (_launchdic==nil) { // 有推送，不执行广告
        
        // 必转:10108、叮当叮当:10102、实惠佳:10050、掌中购:10051、省见:10025、蜜赚:10104、贝贝赞:10033、聚点:10044、宝拉:10106、惠得:10110
        if (PID==10108||PID==10102||PID==10050||PID==10051||PID==10025||PID==10104||PID==10033||PID==10044||PID==10106||PID==10110) {
            [self checkTheFirstLogin]; // 首次进入app引导页
        }else{
            // 闪屏广告
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                [PopActivity showLaunchAd:self.view];
            });
        }
        
    }
    
}

-(void)ifHaveRedPoint{
    //判断小红点
    if(TOKEN.length>0){
        int totalUnreadCount = [[RCIMClient sharedRCIMClient] getTotalUnreadCount];
        if (totalUnreadCount>0) {
            [self showBadgeOnItemIndex:4];
        }else{
            // 从未读中判断（只能判断邮件奖励）
            [NetRequest requestTokenURL:url_no_readMsg parameter:@{@"type":@"1"} success:^(NSDictionary *nwdic) {
                
                if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
                    
                    if ([REPLACENULL(nwdic[@"count"]) integerValue]>0) {
                        [self showBadgeOnItemIndex:4];
                    }else{
                        //判断系统消息
                        [NetRequest requestTokenURL:url_system_msg parameter:@{@"p":@"1"} success:^(NSDictionary *nwdic) {
                            if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
                                
                                NSArray * listAry = NULLARY(nwdic[@"list"]);
                                NSMutableArray* sysdataAry=[NSMutableArray array];
                                [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    SystemMsgModel * model = [[SystemMsgModel alloc]initWithDic:obj];
                                    [sysdataAry addObject:model];
                                }];
                                if (listAry.count>0) {
                                    SystemMsgModel * model = sysdataAry[0];
                                    if (![model.createTime isEqualToString:NSUDTakeData(@"newMesTime")]) {
                                        [self showBadgeOnItemIndex:4];
                                    }else{
                                        [self removeBadgeOnItemIndex:4];
                                    }
                                }else{
                                    [self removeBadgeOnItemIndex:4];
                                }
                            }
                            
                        } failure:^(NSString *failure) {
                            
                        }];
                    }
                }
                
            } failure:^(NSString *failure) {
                
            }];
        }
    }
}

-(void)onRCIMReceiveMessage:(RCMessage *)message left:(int)left{
    dispatch_async(dispatch_get_main_queue(), ^{
        int totalUnreadCount = [[RCIMClient sharedRCIMClient] getTotalUnreadCount];
        if (totalUnreadCount>0) {
            [self showBadgeOnItemIndex:4];
        }else{
            [self removeBadgeOnItemIndex:4];
        }
    });
}



-(void)netWorkChange:(NSNotification*)state{
    NSInteger ns = [state.object integerValue];
    if (ns==1||ns==2) {
        SHOWMSG(@"当前网络不可用!")
    }
    if (ns==3||ns==4) {

        if (NSUDTakeData(app_dynamic_tabbar_dic)==nil) {
            [self huoQuTabbar];
            [NetRequest getDynamicDNS]; // 动态域名
        }
    }
}

#pragma mark ===>>> 获取底部主导航分类(从接口请求)
-(void)huoQuTabbar{
    /*
    NSString *zipUrl     = @"http://ddyqiniuimg.ddyvip.cn/1562656193319_bottom.zip";
    NSURL    *url        = [NSURL URLWithString:zipUrl];
    NSString *md5        = MD5Str(zipUrl);//将下载链接转为md5 后面当做文件夹的名字
    NSArray  *pathes     = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES);
    NSString *path       = [pathes objectAtIndex:0];//大文件放在沙盒下的Library/Caches
    NSString *finishPath = [NSString stringWithFormat:@"%@/zipDownload/TabbarIcon",path];//保存解压后文件的文件夹的路径
    NSString *zipPath    = [NSString stringWithFormat:@"%@/%@.zip",path,md5];//下载的zip包存放路径
    NSLog(@"zip == %@", zipPath);
//    BOOL isExist = [[NSFileManager defaultManager] fileExistsAtPath:finishPath];
//    if (!isExist)
//    {//本地不存在文件 下载
        dispatch_queue_t queue =dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
        dispatch_async(queue, ^{
            NSError *error = nil;
            NSData *data = [NSData dataWithContentsOfURL:url options:0 error:&error];
            if(!error)
            {
                [data writeToFile:zipPath options:0 error:nil];
                //解压zip文件
                ZipArchive *zip= [[ZipArchive alloc]init];
                if([zip UnzipOpenFile:zipPath])
                {//将解压缩的内容写到缓存目录中
                    BOOL ret = [zip UnzipFileTo:finishPath overWrite:YES];
                    if(!ret)
                    {
                        [zip UnzipCloseFile];
                    }
                    //解压完成 删除压缩包
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    [fileManager removeItemAtPath:zipPath error:nil];
                    
                    // 获取样式
                    [NetRequest requestType:0 url:url_getNavc_bottom ptdic:nil success:^(id nwdic) {
                        
                        NSString * time = NowTimeByFormat(@"yyyy-MM-dd") ;
                        NSUDSaveData(time, app_dynamic_tabbar_time)
                        NSArray * list0 = NULLARY(nwdic[@"list0"]);
                        NSArray * list1 = NULLARY(nwdic[@"list1"]);
                        if (list0.count>0||list1.count>0) {
                            NSUDSaveData(NULLDIC(nwdic),app_dynamic_tabbar_dic)
                            [self tabbarViewControllers:NULLDIC(nwdic)];
                        }
                        
                    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                        if (errorType==1) {
                            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(netWorkChange:) name:NetworkChange  object:nil];
                        }
                    }];

                }
            }
        });
//    }
    */
    
    [NetRequest requestType:0 url:url_getNavc_bottom ptdic:nil success:^(id nwdic) {

        NSString * time = NowTimeByFormat(@"yyyy-MM-dd") ;
        NSUDSaveData(time, app_dynamic_tabbar_time)
        NSArray * list0 = NULLARY(nwdic[@"list0"]);
        NSArray * list1 = NULLARY(nwdic[@"list1"]);
        if (list0.count>0||list1.count>0) {
            NSUDSaveData(NULLDIC(nwdic),app_dynamic_tabbar_dic)
            [self tabbarViewControllers:NULLDIC(nwdic)];
        }

    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        if (errorType==1) {
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(netWorkChange:) name:NetworkChange  object:nil];
        }
    }];

}

#pragma mark ===>>> 解析并设置主导航vc
-(void)tabbarViewControllers:(NSDictionary*)dic{

    NSMutableArray * navcAry = [[NSMutableArray alloc]init];
    NSMutableArray * modelAry = [[NSMutableArray alloc]init];
    
    NSArray * ary = @[];
    
#warning 强制list0 默认=0
    if (role_state==0) { ary = NULLARY(dic[@"list0"]); }
    else { ary = NULLARY(dic[@"list1"]); }
    
    
    [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        MClassModel * model = [[MClassModel alloc]initWithMC:obj];
        [modelAry addObject:model];
        
        if (_markNavcDic[model.viewType]!=nil) { // 数组中有分类才会执行，否则不添加
            
            // 将navc添加进数组
            [navcAry addObject:_markNavcDic[model.viewType]];
 
            switch ([model.viewType integerValue]) {
                    
                case 10:{ // 淘抢购
                    
                    [self vc:_cliveVC title:model.title imgN:@"J" imgH:@"JJ"];
                    _cliveVC.isOnePage = YES ;
                    _cliveVC.mcsModel = model ;
                    
                } break;
                    
                case 11:{
                    
                } break;
                    
                case 12:{ // 短视频
                    
                    [self vc:_videoVC title:model.title imgN:@"H" imgH:@"HH"];
                    _videoVC.isOnePage = YES ;
                    _videoVC.mcsModel = model ;
                    
                } break;
                    
                case 13:{ // 品牌团
                    
                    [self vc:_brandVC title:model.title imgN:@"C" imgH:@"CC"];
                    _brandVC.isOnePage = YES ;
                    _brandVC.mcsModel = model ;
                    
                } break;
                    
                case 14:{ // 聚划算
                    
                    [self vc:_bargainVC title:model.title imgN:@"Q" imgH:@"QQ"];
                    _bargainVC.mcsModel = model ;
                    
                } break;
                    
                case 15:{
                    
                } break;
                    
                case 16:{ // 官方推荐
                    
                    [self vc:_ormdNewVC title:@"官方推荐" imgN:@"I" imgH:@"II"];//新官方推荐
                    _ormdNewVC.isOnePage = YES ;
                    _ormdNewVC.mcsModel = model ;
                    
                } break;
                    
                case 17:{ // 超级折扣，超级券，超级划算
                    
                    [self vc:_findVC title:model.title imgN:@"B" imgH:@"BB"];
                    _findVC.isOnePage = YES ;
                    _findVC.mcsModel = model;
                    
                } break;
                    
                case 18:{ // 九块九
                    
                    [self vc:_uglVC title:model.title imgN:@"F" imgH:@"FF"];
                    _uglVC.isOnePage = YES ;
                    _uglVC.mcsModel = model ;
                    
                } break;
                    
                case 19:{ // 分类
                    
                    [self vc:_classVC title:model.title imgN:@"K" imgH:@"KK"];
                    _classVC.mcsModel = model ;
                    
                } break;
                    
                case 20:{
                    
                } break;
                    
                case 21:{
                    
                } break;
                    
                case 22:{ // 淘宝头条
                    
                    [self vc:_tbNewsVC title:model.title imgN:@"G" imgH:@"GG"];
                    
                } break;
                    
                case 23:{ // 网购订单
                    
                } break;
                    
                case 24:{ // 超级钱包
                    
                } break;
                    
                case 25:{ // 新手上路
                    
                    [self vc:_onroad title:model.title imgN:@"P" imgH:@"PP"];
                    _onroad.mcsModel=model;
                    
                } break;
                    
                case 26:{ // 今日推荐
                    
                    [self vc:_todayRec title:model.title imgN:@"M" imgH:@"MM"];
                    _todayRec.isOnePage = YES ;
                    _todayRec.mcsModel = model;
                    
                } break;
                    
                case 27:{  // 今日必推
                    
                    [self vc:_todayRec2 title:model.title imgN:@"O" imgH:@"OO"];
                    _todayRec2.isOnePage = YES ;
                    _todayRec2.mcsModel=model;
                    
                } break;
                    
                case 28:{ // 福利商城
                    
                    [self vc:_freeOrderVC title:@"福利商城" imgN:@"L" imgH:@"LL"];
                    
                } break;
                    
                case 29:{
                    
                } break;
                    
                case 30:{  // h5
                    
                    [self vc:_webVC title:model.title imgN:@"T" imgH:@"TT"];
                    _webVC.ifTab = YES;
                    _webVC.nameTitle = model.title;
                    _webVC.urlWeb = model.link;
                    
                } break;
                    
                case 31:{
                    
                } break;
                    
                case 32:{ // 一键发圈
                    
                    [self vc:_momentsVC title:model.title imgN:@"U" imgH:@"UU"];
                    _momentsVC.mtitle = model.title;
                    
                } break;
                    
                case 33:{ // 拼多多
                    
                    [self vc:_pddListVC title:model.title imgN:@"V" imgH:@"VV"];
                    _pddListVC.mcModel=model;
                    
                } break;
                    
                case 34:{ // 京东
                    
                    [self vc:_JDVC title:model.title imgN:@"W" imgH:@"WW"];
                    _JDVC.mcModel = model;
                    _JDVC.goodsSource = 2;

                } break;
                    
                default:  break;
            }
            
//            [self setNetImageWithVc:_cliveVC title:model.title imgN:model.img imgH:model.nImgs];
        }
    }];
    

    if (navcAry.count==3&&modelAry.count==3) {
        MClassModel * model = modelAry[1];
        UIViewController * vc = (UIViewController*)_markvcDic[model.viewType] ;
        [self vc:vc  title:model.title  imgN:@"ZZ"   imgH:@"ZZ"];
    }

    
    [navcAry insertObject:_navcHomeVC atIndex:0];
    [navcAry addObject:_navcMyfirstNewVC];

    _tabbarController.viewControllers = (NSArray*)navcAry ;
    
}

-(void)changeSelectIndex:(NSNotification*)userinfo{
    NSInteger slindex = [userinfo.object[@"index"] integerValue] ;
    if (slindex+1<=_tabbarController.viewControllers.count) {
        _tabbarController.selectedIndex = slindex ;
    }
}

-(void)initViewControllerAndNavigationController{
    
    _homeVC    = [[HomeVC alloc]init];
    _brandVC   = [[UniversalGoodsListVC alloc]init];
    _findVC    = [[UniversalGoodsListVC alloc]init];
    _videoVC   = [[VideoSingleVC alloc]init];
    _cliveVC   = [[CouponsLiveVC alloc]init];
    _uglVC     = [[UniversalGoodsListVC alloc]init];
    _cvfirstVC = [[CheckVoucherFirstVC alloc]init];
    _classVC   = [[ClassificationVC alloc]init];
    _tbNewsVC  = [[TBNewsViewController alloc]init];
    _bargainVC = [[UniversalGoodsListVC alloc]init];
    _ormdNewVC = [[NewOrmdVC alloc]init]; // 新官方推荐
    _todayRec  = [[UniversalGoodsListVC alloc]init];
    _todayRec2 = [[UniversalGoodsListVC alloc]init];
    _webVC     = [[WebVC alloc]init];
    _onroad    = [[NewRoadVC alloc]init];
    _freeOrderVC   =  [[FreeOrderVC alloc]init];
    _myfirstNewVC  =  [[MyFirstNewVC alloc]init];
    _momentsVC = [[MomentsVC alloc]init];
    _pddListVC = [[UnGoodsListVC alloc]init];
    _JDVC      = [[UnGoodsClasslistBigVC alloc]init];
   
    _navcHomeVC    = [[UINavigationController alloc]initWithRootViewController:_homeVC];
    _navcBrandVC   = [[UINavigationController alloc]initWithRootViewController:_brandVC];
    _navcFindVC    = [[UINavigationController alloc]initWithRootViewController:_findVC];
    _navcVideoVC   = [[UINavigationController alloc]initWithRootViewController:_videoVC];
    _navcCliveVC   = [[UINavigationController alloc]initWithRootViewController:_cliveVC];
    _navcUglVC     = [[UINavigationController alloc]initWithRootViewController:_uglVC];
    _navcCvfirstVC = [[UINavigationController alloc]initWithRootViewController:_cvfirstVC];
    _navcClassVC   = [[UINavigationController alloc]initWithRootViewController:_classVC];
    _navcTbNewsVC  = [[UINavigationController alloc]initWithRootViewController:_tbNewsVC];
    _navcBargainVC = [[UINavigationController alloc]initWithRootViewController:_bargainVC];
    _navcOrmdNewVC = [[UINavigationController alloc]initWithRootViewController:_ormdNewVC];//新官方推荐
    _navcTodayRec  = [[UINavigationController alloc]initWithRootViewController:_todayRec];
    _navcTodayRec2 = [[UINavigationController alloc]initWithRootViewController:_todayRec2];
    _navcWebVC     = [[UINavigationController alloc]initWithRootViewController:_webVC];
    _navcOnroadVC  = [[UINavigationController alloc]initWithRootViewController:_onroad];
    _navcJDVC      = [[UINavigationController alloc]initWithRootViewController:_JDVC];
    _navcMomentsVC = [[UINavigationController alloc]initWithRootViewController:_momentsVC];
    _navcPddListVC = [[UINavigationController alloc]initWithRootViewController:_pddListVC];
    _navcFreeOrderVC   = [[UINavigationController alloc]initWithRootViewController:_freeOrderVC];
    _navcMyfirstNewVC  = [[UINavigationController alloc]initWithRootViewController:_myfirstNewVC];
   
    [self changeNavigationControllerStyle:_navcHomeVC];
    [self changeNavigationControllerStyle:_navcBrandVC];
    [self changeNavigationControllerStyle:_navcFindVC];
    [self changeNavigationControllerStyle:_navcVideoVC];
    [self changeNavigationControllerStyle:_navcCliveVC];
    [self changeNavigationControllerStyle:_navcUglVC];
    [self changeNavigationControllerStyle:_navcCvfirstVC];
    [self changeNavigationControllerStyle:_navcClassVC];
    [self changeNavigationControllerStyle:_navcTbNewsVC];
    [self changeNavigationControllerStyle:_navcBargainVC];
    [self changeNavigationControllerStyle:_navcOrmdNewVC];// 新官方推荐
    [self changeNavigationControllerStyle:_navcTodayRec];
    [self changeNavigationControllerStyle:_navcTodayRec2];
    [self changeNavigationControllerStyle:_navcWebVC];
    [self changeNavigationControllerStyle:_navcOnroadVC];
    [self changeNavigationControllerStyle:_navcFreeOrderVC];
    [self changeNavigationControllerStyle:_navcMyfirstNewVC];
    [self changeNavigationControllerStyle:_navcMomentsVC];
    [self changeNavigationControllerStyle:_navcPddListVC];
    [self changeNavigationControllerStyle:_navcJDVC];
}

// 修改系统导航返回按钮样式
-(void)changeNavigationControllerStyle:(UINavigationController *)navc{

    navc.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:Black102 forKey:NSForegroundColorAttributeName];
    [[UINavigationBar appearance] setTintColor:Black102];
    UIBarButtonItem * item = [UIBarButtonItem appearance];
    NSDictionary * attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.001], NSForegroundColorAttributeName:COLORCLEAR};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    navc.navigationBar.translucent = NO;
    
    // 设置返回按钮样式。这里是自定义的图片
    if ([navc.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
        UIImage * image = [UIImage imageNamed:@"navc_goback"];
        navc.navigationBar.backIndicatorImage = image;
        navc.navigationBar.backIndicatorTransitionMaskImage = image;
    }
    
}

-(void)addBarTitleImg{
    // 此处只有首页和我的修改图片会生效 (如果首次没有请求到，前四个会生效)
    [self vc:_homeVC       title:@"首页"      imgN:@"A"   imgH:@"AA"];
    [self vc:_findVC       title:@"超级划算"   imgN:@"B"   imgH:@"BB"];
    [self vc:_bargainVC    title:@"聚划算"     imgN:@"Q"   imgH:@"QQ"];
    [self vc:_myfirstNewVC title:@"我的"       imgN:@"D"   imgH:@"DD"];
    
    // 默认主导航
    _tabbarController.viewControllers = @[_navcHomeVC,_navcFindVC,_navcBargainVC,_navcMyfirstNewVC];
    
    _markNavcDic = @{@"10":_navcCliveVC,
                     @"12":_navcVideoVC,
                     @"13":_navcBrandVC,
                     @"14":_navcBargainVC,
                     @"16":_navcOrmdNewVC,
                     @"17":_navcFindVC,
                     @"18":_navcUglVC,
                     @"19":_navcClassVC,
                     @"22":_navcTbNewsVC,
                     @"25":_navcOnroadVC,
                     @"26":_navcTodayRec,
                     @"27":_navcTodayRec2,
                     @"28":_navcFreeOrderVC ,
                     @"30":_navcWebVC,
                     @"32":_navcMomentsVC,
                     @"33":_navcPddListVC,
                     @"34":_navcJDVC };
    
    _markvcDic = @{@"10":_cliveVC,
                   @"12":_videoVC,
                   @"13":_brandVC,
                   @"14":_bargainVC,
                   @"16":_ormdNewVC,
                   @"17":_findVC,
                   @"18":_uglVC,
                   @"19":_classVC,
                   @"22":_tbNewsVC,
                   @"25":_onroad ,
                   @"26":_todayRec,
                   @"27":_todayRec2,
                   @"28":_freeOrderVC,
                   @"30":_webVC,
                   @"32":_momentsVC,
                   @"33":_pddListVC,
                   @"34":_JDVC };
}

// 底部itme(网络图片）
-(void)setNetImageWithVc:(UIViewController*)vc title:(NSString*)title imgN:(NSString*)imgn imgH:(NSString*)imgh{
    NSLog(@"%@ == %@", imgn, imgh);
    NSArray  *pathes     = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES);
    NSString *path       = [pathes objectAtIndex:0];//大文件放在沙盒下的Library/Caches
    NSString *finishPath = [NSString stringWithFormat:@"%@/zipDownload/TabbarIcon",path];//保存解压后文件的文件夹的路径
    
    if ([imgn isEqualToString:@"ZZ"]) { //中间放大显示
        UIImage * logoImage = [self bigCenterLogoImg];
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(-10, 0, 10, 0);
        vc.tabBarItem.image = [logoImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [logoImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else{
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        // 获取沙盒文件
        NSString *imageFilePath = [finishPath stringByAppendingPathComponent:imgn];
        NSLog(@"获取沙盒文件 = %@", imageFilePath);
        vc.tabBarItem.image = [[UIImage imageWithContentsOfFile:imageFilePath] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        NSString *imageFilePath2 = [finishPath stringByAppendingPathComponent:imgh];
        vc.tabBarItem.selectedImage = [[UIImage imageWithContentsOfFile:imageFilePath2] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    vc.tabBarItem.title = title;
    vc.title = title ;
}

// 底部itme
-(void)vc:(UIViewController*)vc title:(NSString*)title imgN:(NSString*)imgn imgH:(NSString*)imgh{
    if ([imgn isEqualToString:@"ZZ"]) { //中间放大显示
        UIImage * logoImage = [self bigCenterLogoImg];
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(-10, 0, 10, 0);
        vc.tabBarItem.image = [logoImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [logoImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else{
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        vc.tabBarItem.image = [[UIImage imageNamed:imgn] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:imgh] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    vc.tabBarItem.title = title;
    vc.title = title ;
}

// 生成一个大的底部logo图标
-(UIImage*)bigCenterLogoImg{

    UIView * bgview1 = [[UIView alloc]init];
    bgview1.backgroundColor = COLORWHITE ;
    bgview1.layer.masksToBounds = YES ;
    bgview1.layer.cornerRadius = 25;
    bgview1.frame = CGRectMake(0, 0, 50, 50);
    
    UIView * linesView = [[UIView alloc]init];
    linesView.clipsToBounds = YES;
    linesView.layer.masksToBounds = YES ;
    linesView.layer.cornerRadius = 25;
    linesView.layer.borderWidth = 1;
    linesView.layer.borderColor = Black204.CGColor;
    linesView.backgroundColor = COLORWHITE ;
    linesView.frame = CGRectMake(0, 0, 50, 50);
    [bgview1 addSubview:linesView];
    
    UIView * bottomView = [[UIView alloc]init];
    bottomView.frame = CGRectMake(0, 16, 50, 34);
    bottomView.backgroundColor = COLORWHITE;
    [bgview1 addSubview:bottomView];
    
    UIImageView * imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"ZZ"] ;
    imageView.bounds = CGRectMake(0, 0, 36, 36);
    imageView.layer.masksToBounds = YES ;
    imageView.layer.cornerRadius = 18 ;
    imageView.center = bgview1.center ;
    [bgview1 addSubview:imageView];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, [UIScreen mainScreen].scale);
    [bgview1.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (image) { return image; }
    return [UIImage imageNamed:@"ZZ"] ;

}

#pragma mark ==>> UITabBarDelegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    _lastSelectTag = tabBarController.selectedIndex;
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    if (tabBarController.selectedIndex==_tabbarController.viewControllers.count-1) {
        if (TOKEN.length==0) {
            [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) {
                if (result == 1) {

                }
                if (result == 2) {
                    tabBarController.selectedIndex = 0 ;
                }
            }];
        }
    }
    
}


- (void)showBadgeOnItemIndex:(int)index{
    

    //移除之前的小红点
    [self removeBadgeOnItemIndex:index];
    //新建小红点
    UIView *badgeView = [[UIView alloc]init];
    badgeView.tag = 888 + index;
    badgeView.backgroundColor = [UIColor redColor];
    CGRect tabFrame = _myfirstNewVC.tabBarController.tabBar.frame;
    
    //确定小红点的位置
    float percentX = (index +0.6) / 5;
    CGFloat x = ceilf(percentX * tabFrame.size.width);
    CGFloat y = ceilf(0.1 * tabFrame.size.height);
    badgeView.frame = CGRectMake(x, y, 8, 8);
    badgeView.layer.cornerRadius = badgeView.frame.size.width/2;
    
    [_myfirstNewVC.tabBarController.tabBar addSubview:badgeView];
    
}

- (void)removeBadgeOnItemIndex:(int)index{
    
    //按照tag值进行移除
    for (UIView *subView in _myfirstNewVC.tabBarController.tabBar.subviews) {
        if (subView.tag == 888+index) {
            [subView removeFromSuperview];
        }
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark ==>> 设置引导页

-(void)checkTheFirstLogin{
    
    if (![NSUDTakeData(Launch_last_app_version) isEqualToString:now_app_version]) {
        [[PopActivity new] getActivity];
        [FirstLoginScrollView showFirstLaunch:self.view];
    }else{
        // 闪屏广告
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [PopActivity showLaunchAd:self.view];
        });
    }
    
}


// 可在此修改tabbar一些view 遍历出所有view
-(void)changeTabbarBgView{
    @try {
        if (_tabbarController.tabBar.subviews.count>0) {
            LOG(@"底部导航栏视图:\n\n%@",_tabbarController.tabBar.subviews)
            
            // 有的系统，分割线在tabBar上、有的在tabbar第一个控件上
            for (UIView*bgImgV in _tabbarController.tabBar.subviews) {
                if ([bgImgV isKindOfClass:[UIImageView class]]) {
                    [_tabbarController.tabBar sendSubviewToBack:bgImgV];
                }
            }
            
            UIView * tbBgView = _tabbarController.tabBar.subviews[0];
            for (UIView * tbSubViews in tbBgView.subviews) {
                if ([tbSubViews isKindOfClass:[UIImageView class]] && tbSubViews.bounds.size.height<=1) {
                    UIImageView * linesimg = (UIImageView *)tbSubViews;
                    linesimg.backgroundColor = Black204 ;
                }
            }
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
   
}

@end








