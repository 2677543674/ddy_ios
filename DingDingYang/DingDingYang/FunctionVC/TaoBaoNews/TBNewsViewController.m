//
//  TBNewsViewController.m
//  DingDingYang
//
//  Created by ddy on 08/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "TBNewsViewController.h"
#import "TBNewsCell.h"
#import "TBNewsModel.h"
#import "WebVC.h"

@interface TBNewsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)NSMutableArray *dataArray;
@property(nonatomic,strong)NSString *publishId;
@end

@implementation TBNewsViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated: YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title=@"头条";
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TBNewsCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    __weak typeof(self) weakSelf=self;
    self.tableView.mj_header=[MJRefreshHeader headerWithRefreshingBlock:^{
        weakSelf.publishId=@"";
        [weakSelf downloadNewsList];
        [weakSelf.tableView.mj_header endRefreshing];
    }];
    self.tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (_dataArray.count>0) {
            TBNewsModel *model=[_dataArray lastObject];
            weakSelf.publishId=model.publishId;
        }
        
        [weakSelf downloadNewsList];
        [weakSelf.tableView.mj_footer endRefreshing];
    }];

    self.publishId=@"";
     self.dataArray=[[NSMutableArray alloc]init];
    [self downloadNewsList];
    
}


-(void)downloadNewsList{
    
    MBShow(@"加载中…")
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:_publishId forKey:@"publishId"];
    
    [NetRequest requestType:0 url:url_tbnew_list ptdic:dic success:^(id nwdic) {
        MBHideHUD
        id column = [nwdic objectForKey:@"list"];
        
        if ([column isKindOfClass:[NSArray class]]) {
            NSArray *array=column;
            if ([_publishId isEqualToString:@""]) {
                [self.dataArray removeAllObjects];
            }
            for (int i = 0; i < array.count; i++) {
                NSDictionary *dic=[array objectAtIndex:i];
                TBNewsModel *model = [[TBNewsModel alloc]initWithDic:dic];
                [self.dataArray addObject:model];
            }
            [self.tableView reloadData];
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TBNewsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (!cell) {
        cell=[[TBNewsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    TBNewsModel *model=_dataArray[indexPath.row];
    cell.nameLabel.text=model.name;
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:model.imgUrl] placeholderImage:nil];
    cell.titleLabel.text=model.title;
    
   
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TBNewsModel *model=_dataArray[indexPath.row];

    WebVC*web=[[WebVC alloc]init];
    web.nameTitle=@"头条";
    web.urlWeb=model.detailUrl;
    web.hidesBottomBarWhenPushed = YES ;
    PushTo(web, YES);
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
//    [self.navigationController setNavigationBarHidden:YES animated: YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
