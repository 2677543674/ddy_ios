//
//  TBNewsModel.m
//  DingDingYang
//
//  Created by ddy on 08/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "TBNewsModel.h"

@implementation TBNewsModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        self.name=REPLACENULL(dic[@"bizSourceName"]) ;
        if ([(NSArray *)dic[@"picList"] count]>0) {
            self.imgUrl=REPLACENULL(dic[@"picList"][0]) ;
        }
        self.title=REPLACENULL(dic[@"name"]);
        self.detailUrl=REPLACENULL(dic[@"detailUrl"]);
        self.publishId=REPLACENULL(dic[@"publishId"]);
    }
    return self;
}
@end
