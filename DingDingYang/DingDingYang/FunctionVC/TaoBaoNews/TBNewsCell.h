//
//  TBNewsCell.h
//  DingDingYang
//
//  Created by ddy on 08/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBNewsModel.h"

@interface TBNewsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic,strong)TBNewsModel *model;

@end
