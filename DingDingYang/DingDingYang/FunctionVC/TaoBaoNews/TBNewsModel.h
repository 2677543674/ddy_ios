//
//  TBNewsModel.h
//  DingDingYang
//
//  Created by ddy on 08/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBNewsModel : NSObject
@property(nonatomic,copy)NSString*name;
@property(nonatomic,copy)NSString*imgUrl;
@property(nonatomic,copy)NSString*title;
@property(nonatomic,copy)NSString*detailUrl;
@property(nonatomic,copy)NSString*publishId;
-(instancetype)initWithDic:(NSDictionary*)dic;
@end
