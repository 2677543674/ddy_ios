//
//  TBNewsViewController.h
//  DingDingYang
//
//  Created by ddy on 08/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBNewsViewController : DDYViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
