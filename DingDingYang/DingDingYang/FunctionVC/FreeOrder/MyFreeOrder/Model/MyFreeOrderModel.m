//
//  MyFreeOrderModel.m
//  DingDingYang
//
//  Created by ddy on 18/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "MyFreeOrderModel.h"

@implementation MyFreeOrderModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    if (self) {

        self.freeOrderId    = REPLACENULL(dic[@"freeOrderId"]) ;
        self.orderNo        = REPLACENULL(dic[@"orderNo"]) ;
        self.price          = ROUNDED(REPLACENULL(dic[@"price"]),2) ;
        self.num            = REPLACENULL(dic[@"num"]) ;
        self.goodsName      = REPLACENULL(dic[@"goodsName"]) ;
        self.getTime        = REPLACENULL(dic[@"getTime"]) ;
        self.goodsPic       = REPLACENULL(dic[@"goodsPic"]) ;
        self.goodsFreeId    = REPLACENULL(dic[@"goodsFreeId"]) ;
        self.sales          = REPLACENULL(dic[@"sales"]) ;
        self.goodsId        = REPLACENULL(dic[@"goodsId"]) ;
        self.status         = REPLACENULL(dic[@"status"]) ;
        self.statusType     = REPLACENULL(dic[@"statusType"]) ;
        self.statusText     = REPLACENULL(dic[@"statusText"]) ;
        
        self.restTime       = (long)[REPLACENULL(dic[@"restTime"]) integerValue]/1000 ;
        
        self.awardTime      = REPLACENULL(dic[@"awardTime"]) ;

        self.failReason     = REPLACENULL(dic[@"failReason"]) ;
        self.failReasonText = REPLACENULL(dic[@"failReasonText"]) ;
        
    }
    return self ;
}

@end
