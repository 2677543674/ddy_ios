//
//  MyFreeOrderModel.h
//  DingDingYang
//
//  Created by ddy on 18/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyFreeOrderModel : NSObject

//@property(nonatomic,copy) NSString * goodsName;    // 商品名
//@property(nonatomic,copy) NSString * awardTime;    // 奖励时间
//@property(nonatomic,copy) NSString * price;        // 商品价格(天猫)
//@property(nonatomic,copy) NSString * num;          // 免单数量
//@property(nonatomic,copy) NSString * freeOrderId;  // 免单ID
//@property(nonatomic,copy) NSString * orderNo;      // 订单号
//@property(nonatomic,copy) NSString * goodsPic;     // 商品图


@property(nonatomic,copy) NSString * freeOrderId ;    // 我的福利ID
@property(nonatomic,copy) NSString * orderNo ;        // 订单号
@property(nonatomic,copy) NSString * price ;          // 价格
@property(nonatomic,copy) NSString * num ;            // 免单数量
@property(nonatomic,copy) NSString * goodsName ;      // 商品标题
@property(nonatomic,copy) NSString * getTime ;        //
@property(nonatomic,copy) NSString * goodsPic ;       // 商品主图
@property(nonatomic,copy) NSString * goodsFreeId ;    // 免单商品ID
@property(nonatomic,copy) NSString * sales ;          // 销量
@property(nonatomic,copy) NSString * goodsId ;        // 商品ID
@property(nonatomic,copy) NSString * status ;         // 商品状态
@property(nonatomic,copy) NSString * statusType ;     // 商品状态类型(数字)
@property(nonatomic,copy) NSString * statusText ;     // 商品状态类型(文本)


// 待下单中有的字段
@property(nonatomic,assign) long restTime;       // 剩余时间时间戳

// 待奖励 或已奖励 中有的字段
@property(nonatomic,copy) NSString * awardTime;       // 奖励时间

// 已失效中有的字段
@property(nonatomic,copy) NSString * failReason ;     // 失败原因
@property(nonatomic,copy) NSString * failReasonText ; // 失败原因(文本)


-(instancetype)initWithDic:(NSDictionary*)dic ;

@end







