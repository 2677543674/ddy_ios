//
//  MyFreeOrderfailureCell.h
//  DingDingYang
//
//  Created by ddy on 14/08/2017.
//  Copyright © 2017 ddy.All rights reserved.

//  我的福利已失效 cell

#import <UIKit/UIKit.h>
#import "MyFreeOrderModel.h"
@interface MyFreeOrderfailureCell : UITableViewCell

@property(nonatomic,strong) UIImageView * goodsImgV ;  // 商品主图
@property(nonatomic,strong) UILabel * goodsNameLab ;   // 商品名称
@property(nonatomic,strong) UILabel * priceLab ;       // 商品价格
@property(nonatomic,strong) UILabel * orderNoLab ;     // 订单号
@property(nonatomic,strong) UILabel * failRLab ;       // 失败原因lab

@property(nonatomic,strong) MyFreeOrderModel * model ;


-(void)setCellWithModel:(MyFreeOrderModel *)dmodel;
@end
