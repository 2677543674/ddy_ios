//
//  MyFreeOrderWillCreateOrderCell.h
//  DingDingYang
//
//  Created by ddy on 14/08/2017.
//  Copyright © 2017 ddy.All rights reserved.

//  我的福利待下单

#import <UIKit/UIKit.h>
#import "MyFreeOrderModel.h"

@protocol UpFreeOrderSuccessDeleGate <NSObject>
-(void)upFreeOrderSuccess ;
@end

@interface MyFreeOrderWillCreateOrderCell : UITableViewCell


@property(nonatomic,strong) UIImageView * goodsImgV ;  // 商品主图
@property(nonatomic,strong) UITextField * orderTextF ; // 订单号输入框
@property(nonatomic,strong) UILabel * goodsNameLab ;   // 商品名称
@property(nonatomic,strong) UILabel * priceLab ;       // 商品价格
@property(nonatomic,strong) UILabel * timerLab ;       // 下单剩余时间
@property(nonatomic,strong) UIButton * submitBtn ;     // 提交按钮

@property (nonatomic,assign) NSInteger countTime ;
@property (nonatomic,strong) NSTimer * timer ;

@property(nonatomic,strong) MyFreeOrderModel * model ;

@property(nonatomic,assign) id<UpFreeOrderSuccessDeleGate>delegate ;


@end













