//
//  WelfareNullCell.h
//  DingDingYang
//
//  Created by ddy on 2017/12/26.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelfareNullCell : UITableViewCell
@property(nonatomic,strong) MyCenterNoDataView * nodataView ;
@end
