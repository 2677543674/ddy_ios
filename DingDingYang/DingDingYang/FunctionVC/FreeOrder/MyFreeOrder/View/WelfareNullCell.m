//
//  WelfareNullCell.m
//  DingDingYang
//
//  Created by ddy on 2017/12/26.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "WelfareNullCell.h"

@implementation WelfareNullCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self nodataView];
    }
    return self ;
}
-(MyCenterNoDataView*)nodataView{
    if (!_nodataView) {
        
        _nodataView = [[MyCenterNoDataView alloc]init];
        _nodataView.imgNameStr = @"哭";
        _nodataView.tishiStr = @"你还没有福利订单";
        _nodataView.btnStr = @"立即去推荐好友吧" ;
        ADDTARGETBUTTON(_nodataView.tishiBtn, nodataClickGotoShare)
        [self.contentView addSubview:_nodataView];
        [_nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.centerY.equalTo(self.contentView.mas_centerY).offset(-40*HWB);
            make.height.offset(135*HWB);  make.width.offset(150*HWB);
        }];
    }
    return _nodataView ;
}

-(void)nodataClickGotoShare{
    [MyPageJump pushNextVcByModel:nil byType:51];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
