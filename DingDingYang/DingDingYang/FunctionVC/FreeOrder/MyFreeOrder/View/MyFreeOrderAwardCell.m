//
//  MyFreeOrderAwardCell.m
//  DingDingYang
//
//  Created by ddy on 14/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "MyFreeOrderAwardCell.h"


@implementation MyFreeOrderAwardCell

// 待奖励
-(void)setModel1:(MyFreeOrderModel *)model1{
    _model1 = model1 ;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:model1.goodsPic] placeholderImage:PlaceholderImg];
    _goodsNameLab.text = _model1.goodsName ;
    _priceLab.text = FORMATSTR(@"￥%@",_model1.price) ;
    _orderNoLab.text = FORMATSTR(@"订单号: %@",_model1.orderNo);
//    _timeLab.text = FORMATSTR(@"预估最迟奖励时间: %@",_model1.awardTime);
}

// 已奖励
-(void)setModel2:(MyFreeOrderModel *)model2{
    _model2 = model2 ;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_model2.goodsPic] placeholderImage:PlaceholderImg];
    _goodsNameLab.text = _model2.goodsName ;
    _priceLab.text = FORMATSTR(@"￥%@",_model2.price) ;
    _orderNoLab.text = FORMATSTR(@"订单号: %@",_model2.orderNo);
//    _timeLab.text = FORMATSTR(@"奖励时间: %@",_model2.awardTime);
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self goodsImgV];
        [self goodsNameLab];
        [self priceLab];
        [self orderNoLab];
//        [self timeLab];
        
        UIImageView * linesImg = [[UIImageView alloc]init];
        linesImg.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImg];
        [linesImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.left.offset(0);
            make.right.offset(0); make.height.offset(1);
        }];
        
    }
    return self ;
}

// 商品主图
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.backgroundColor = COLORGROUP ;
        _goodsImgV.layer.masksToBounds = YES ;
        _goodsImgV.layer.cornerRadius = 5 ;
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _goodsImgV.clipsToBounds = YES ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(10);  make.bottom.offset(-10);
            make.left.offset(12); make.width.equalTo(_goodsImgV.mas_height);
        }];
    }
    return _goodsImgV ;
}

// 商品标题
-(UILabel*)goodsNameLab{
    if (!_goodsNameLab) {
        _goodsNameLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _goodsNameLab.numberOfLines = 2 ;
        [self.contentView addSubview:_goodsNameLab];
        [_goodsNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10); make.top.offset(10);
        }];
    }
    return _goodsNameLab ;
}

// 商品价格
-(UILabel*)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥0.0" color:Black51 font:16*HWB];
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
//            make.bottom.equalTo(self.contentView.mas_centerY);
            make.top.equalTo(self.contentView.mas_centerY);
        }];
    }
    return _priceLab ;
}

// 订单编号
-(UILabel*)orderNoLab{
    if (!_orderNoLab) {
        _orderNoLab = [UILabel labText:@"订单号: " color:Black102 font:12*HWB];
        [self.contentView addSubview:_orderNoLab];
        [_orderNoLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
//            make.top.equalTo(self.contentView.mas_centerY).offset(5);
             make.bottom.offset(-10);
        }];
    }
    return _orderNoLab ;
}

// 预估最迟奖励时间
-(UILabel*)timeLab{
    if (!_timeLab) {
        _timeLab = [UILabel labText:@"预估最迟奖励时间: " color:Black102 font:12*HWB] ;
        [self.contentView addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.offset(-10);
        }];
    }
    return _timeLab ;
}


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
