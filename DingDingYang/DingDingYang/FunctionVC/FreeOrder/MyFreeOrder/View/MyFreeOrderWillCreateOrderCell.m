//
//  MyFreeOrderWillCreateOrderCell.m
//  DingDingYang
//
//  Created by ddy on 14/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "MyFreeOrderWillCreateOrderCell.h"

@implementation MyFreeOrderWillCreateOrderCell


-(void)setModel:(MyFreeOrderModel *)model{
    _model = model ;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:model.goodsPic] placeholderImage:PlaceholderImg];
    _goodsNameLab.text = _model.goodsName ;
    _priceLab.text = FORMATSTR(@"￥%@",_model.price) ;

}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self goodsImgV];
        [self goodsNameLab];
        [self priceLab];
        [self timerLab];
        [self submitBtn];
        [self orderTextF];
        
        UIImageView * linesImg = [[UIImageView alloc]init];
        linesImg.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImg];
        [linesImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.left.offset(0);
            make.right.offset(0); make.height.offset(1);
        }];
        
    }
    return self ;
}

// 商品主图
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.backgroundColor = COLORGROUP ;
        _goodsImgV.layer.masksToBounds = YES ;
        _goodsImgV.layer.cornerRadius = 2 ;
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _goodsImgV.clipsToBounds = YES ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(10);  make.bottom.offset(-10);
            make.left.offset(12); make.width.equalTo(_goodsImgV.mas_height);
        }];
    }
    return _goodsImgV ;
}

// 商品标题
-(UILabel*)goodsNameLab{
    if (!_goodsNameLab) {
        _goodsNameLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        [self.contentView addSubview:_goodsNameLab];
        [_goodsNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10); make.top.offset(10);
        }];
    }
    return _goodsNameLab ;
}

// 商品价格
-(UILabel*)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥0.0" color:Black51 font:16*HWB];
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(self.contentView.mas_centerY);
        }];
    }
    return _priceLab ;
}

// 距离下单剩余时间
-(UILabel*)timerLab{
    if (!_timerLab) {
        _timerLab = [UILabel labText:@"剩余下单时间: " color:Black102 font:12*HWB] ;
        [self.contentView addSubview:_timerLab];
        [_timerLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.top.equalTo(self.contentView.mas_centerY).offset(5*HWB);
            
        }];
    }
    return _timerLab ;
}

// 提交订单按钮
-(UIButton*)submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton titLe:@"提交" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:12*HWB];
        _submitBtn.layer.masksToBounds = YES ;
        _submitBtn.layer.cornerRadius = 3 ;
        ADDTARGETBUTTON(_submitBtn, clickSubmit)
        [self.contentView addSubview:_submitBtn];
        [_submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);
            make.height.offset(22*HWB);
            make.width.offset(40*HWB);
            make.bottom.offset(-6);
        }];
    }
    return _submitBtn ;
}

// 订单号出入框
-(UITextField*)orderTextF{
    if (!_orderTextF) {
        UILabel * dingdanhaoLab = [UILabel labText:@"订单号:" color:Black102 font:12*HWB];
        [self.contentView addSubview:dingdanhaoLab];
        [dingdanhaoLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.centerY.equalTo(_submitBtn.mas_centerY);
        }];
        
        //                                                                       此处空格不能删
        _orderTextF = [UITextField textColor:Black51 PlaceHorder:@"请输入订单号                    " font:12*HWB];
        _orderTextF.keyboardType = UIKeyboardTypePhonePad;
        _orderTextF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _orderTextF.textAlignment = NSTextAlignmentLeft ;
        [self.contentView addSubview:_orderTextF];
        [_orderTextF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_submitBtn.mas_left).offset(0);
            make.left.equalTo(dingdanhaoLab.mas_right).offset(-6);
            make.centerY.equalTo(_submitBtn.mas_centerY);
            make.height.offset(20*HWB);
        }];
        
        UIImageView * linesImgv = [[UIImageView alloc]init];
        linesImgv.backgroundColor = Black204 ;
        [_orderTextF addSubview:linesImgv];
        [linesImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(11) ;   make.right.offset(-5);
            make.bottom.offset(0) ; make.height.offset(0.5);
        }];
        
    }
    return _orderTextF ;
}

// 点击提交订单
-(void)clickSubmit{
    
    LOG(@"%@",_orderTextF.text)
    
    if (_orderTextF.text.length==0) {
        SHOWMSG(@"请填写订单号！")
        return;
    }
    
    MBShow(@"正在提交...")
    NSDictionary * parameterDic = @{@"freeOrderId":_model.freeOrderId,
                                    @"orderNo":_orderTextF.text};
    
    [NetRequest requestTokenURL:url_upload_orderNo parameter:parameterDic success:^(NSDictionary * nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"] ) {
            SHOWMSG(@"提交成功！")
            _submitBtn.enabled = NO;
            [self.delegate upFreeOrderSuccess];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];

}



- (void)awakeFromNib {
    [super awakeFromNib];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
