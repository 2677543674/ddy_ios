//
//  MyFreeOrderfailureCell.m
//  DingDingYang
//
//  Created by ddy on 14/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "MyFreeOrderfailureCell.h"
#import "MyFreeOrderModel.h"
@implementation MyFreeOrderfailureCell

-(void)setModel:(MyFreeOrderModel *)model{
    _model = model ;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:model.goodsPic] placeholderImage:PlaceholderImg];
    _goodsNameLab.text = _model.goodsName ;
    _priceLab.text = FORMATSTR(@"￥%@",_model.price) ;
    _orderNoLab.text = FORMATSTR(@"订单号: %@",_model.orderNo.length>0?model.orderNo:@" 未填单");
    _failRLab.text = FORMATSTR(@"----------  %@  ----------",_model.statusText) ;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self goodsImgV];
        [self goodsNameLab];
        [self priceLab];
        [self orderNoLab];
        [self failRLab];
        
        UIImageView * linesImg = [[UIImageView alloc]init];
        linesImg.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImg];
        [linesImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.left.offset(0);
            make.right.offset(0); make.height.offset(1);
        }];
        
    }
    return self ;
}

// 商品主图
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.backgroundColor = COLORGROUP ;
        _goodsImgV.layer.masksToBounds = YES ;
        _goodsImgV.layer.cornerRadius = 5 ;
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _goodsImgV.clipsToBounds = YES ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(10);  make.bottom.offset(-10);
            make.left.offset(12); make.width.equalTo(_goodsImgV.mas_height);
        }];
    }
    return _goodsImgV ;
}

// 商品标题
-(UILabel*)goodsNameLab{
    if (!_goodsNameLab) {
        _goodsNameLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        [self.contentView addSubview:_goodsNameLab];
        [_goodsNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10); make.top.offset(10);
        }];
    }
    return _goodsNameLab ;
}

// 商品价格
-(UILabel*)priceLab{
    if (!_priceLab) {
        _priceLab = [UILabel labText:@"￥0.0" color:Black51 font:16*HWB];
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.bottom.equalTo(self.contentView.mas_centerY);
        }];
    }
    return _priceLab ;
}

// 订单编号
-(UILabel*)orderNoLab{
    if (!_orderNoLab) {
        _orderNoLab = [UILabel labText:@"订单号" color:Black102 font:12*HWB];
        [self.contentView addSubview:_orderNoLab];
        [_orderNoLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.top.equalTo(self.contentView.mas_centerY).offset(5);
        }];
    }
    return _orderNoLab ;
}

// 预估最迟奖励时间
-(UILabel*)failRLab{
    if (!_failRLab) {
        _failRLab = [UILabel labText:@"------ 已失效 ------" color:Black102 font:12*HWB] ;
        _failRLab.textAlignment = NSTextAlignmentCenter ;
        _failRLab.clipsToBounds = YES ;
        [self.contentView addSubview:_failRLab];
        [_failRLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(16);
            make.bottom.offset(-10); make.right.offset(-16);
        }];
    }
    return _failRLab ;
}


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end








