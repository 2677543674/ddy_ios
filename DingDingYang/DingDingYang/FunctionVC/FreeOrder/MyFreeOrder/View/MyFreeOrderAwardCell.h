//
//  MyFreeOrderAwardCell.h
//  DingDingYang
//
//  Created by ddy on 14/08/2017.
//  Copyright © 2017 ddy.All rights reserved.

//  待奖励列表、和已奖励

#import <UIKit/UIKit.h>
#import "MyFreeOrderModel.h"
@interface MyFreeOrderAwardCell : UITableViewCell

@property(nonatomic,strong) UIImageView * goodsImgV ;  // 商品主图
@property(nonatomic,strong) UILabel * goodsNameLab ;   // 商品名称
@property(nonatomic,strong) UILabel * priceLab ;       // 商品价格
@property(nonatomic,strong) UILabel * orderNoLab ;     // 订单号
@property(nonatomic,strong) UILabel * timeLab ;        // 预估最迟奖励时间

@property(nonatomic,strong) MyFreeOrderModel * model1 ;
@property(nonatomic,strong) MyFreeOrderModel * model2 ;


@end
