//
//  MyWelfareVC.m
//  DingDingYang
//
//  Created by ddy on 2017/12/25.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MyWelfareVC.h"
#import "MyFreeOrderWillCreateOrderCell.h"
#import "MyFreeOrderAwardCell.h"
#import "MyFreeOrderfailureCell.h"
#import "MyFreeOrderModel.h"
#import "WelfareNullCell.h"

@interface MyWelfareVC () <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UpFreeOrderSuccessDeleGate>

@property(nonatomic,strong) NSMutableDictionary * countDownTimeDict;
@property(nonatomic,strong) NSMutableArray * timerArr; // 存放定时器

// 数据源
@property(nonatomic,strong) NSMutableArray * dataSource1;
@property(nonatomic,strong) NSMutableArray * dataSource2;
@property(nonatomic,strong) NSMutableArray * dataSource3;
@property(nonatomic,strong) NSMutableArray * dataSource4;

// 页数
@property(nonatomic,copy) NSString * pageCount1;
@property(nonatomic,copy) NSString * pageCount2;
@property(nonatomic,copy) NSString * pageCount3;
@property(nonatomic,copy) NSString * pageCount4;

// 记录当前选中的是哪一个分类
@property(nonatomic,assign) NSInteger selectStatus;

// 控件
@property(nonatomic,strong) UIScrollView * scrollView ;
@property(nonatomic,strong) UITableView * tableView ;
@property(nonatomic,strong) UIView * selectBtnView ;
@property(nonatomic,strong) UIButton * selectBtn ;
@property(nonatomic,strong) UIImageView * cLimgV ; // 选中状态的下划线


@end

@implementation MyWelfareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLORGROUP ;
    
    self.title = @"我的福利" ;
    [self initialization];
    [self creatSelectBtn];
    [self scrollView];
    [self creatTableView];
    
    [self enumerateDatasourceCountDown];
//    [self downloadData];
}

// 初始化
-(void)initialization{
    
    _selectStatus = 2 ;
    _pageCount1 = @"1" ;
    _pageCount2 = @"1" ;
    _pageCount3 = @"1" ;
    _pageCount4 = @"1" ;
    
    self.dataSource1 = [[NSMutableArray alloc]init];
    self.dataSource2 = [[NSMutableArray alloc]init];
    self.dataSource3 = [[NSMutableArray alloc]init];
    self.dataSource4 = [[NSMutableArray alloc]init];
}

#pragma mark ===>>> 请求我的福利数据
-(void)downloadData{
    
//    _selectBtnView.userInteractionEnabled = NO ;
//    _scrollView.scrollEnabled = NO ;
    
    NSMutableDictionary * pdic = [[NSMutableDictionary alloc]init];
    pdic[@"status"] = FORMATSTR(@"%ld",(long)_selectStatus) ;
    pdic[@"st"] = _pageCount1 ;
    
    switch (_selectStatus) {
        case 2: { pdic[@"st"] = _pageCount1 ; }  break;
        case 3: { pdic[@"st"] = _pageCount2 ; }  break;
        case 4: { pdic[@"st"] = _pageCount3 ; }  break;
        case 5: { pdic[@"st"] = _pageCount4 ; }  break;
        default:  break;
    }
    
    [NetRequest requestTokenURL:url_myFree_list parameter:pdic success:^(NSDictionary *nwdic) {
         if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
            
             switch ([pdic[@"status"] integerValue]) {
                 case 2:{ if ([_pageCount1 isEqualToString:@"1"]) { [_dataSource1 removeAllObjects]; } } break;
                 case 3:{ if ([_pageCount2 isEqualToString:@"1"]) { [_dataSource2 removeAllObjects]; } } break;
                 case 4:{ if ([_pageCount3 isEqualToString:@"1"]) { [_dataSource3 removeAllObjects]; } } break;
                 case 5:{ if ([_pageCount4 isEqualToString:@"1"]) { [_dataSource4 removeAllObjects]; } } break;
             }
             
            NSArray * dataAry = NULLARY(nwdic[@"list"]) ;
            
            [dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                MyFreeOrderModel * model = [[MyFreeOrderModel alloc]initWithDic:obj];
                switch ([pdic[@"status"] integerValue]) {
                    case 2:{ [_dataSource1 addObject:model]; } break;
                        
                    case 3:{ [_dataSource2 addObject:model]; } break;
                        
                    case 4:{ [_dataSource3 addObject:model]; } break;
                        
                    case 5:{ [_dataSource4 addObject:model]; } break;
                        
                    default:  break;
                }
            }];
            [self endRefreshing:1 aryCount:dataAry.count];
             
        }else{
            [self endRefreshing:2 aryCount:0];
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
         SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self downloadData];
        }];
        [self endRefreshing:2 aryCount:0];
    }];
}

// 停止刷新  sf: 1:请求成功  2:请求失败    count:获取到的数组个数、当sf为1时count为0，则代表无数据
-(void)endRefreshing:(NSInteger)sf aryCount:(NSInteger)count{
    
//    _selectBtnView.userInteractionEnabled = YES ;
//    _scrollView.scrollEnabled = YES ;
    
    _tableView = (UITableView*)[_scrollView viewWithTag:_selectStatus+99];
    [_tableView.mj_header endRefreshing];
    if (sf==1&&count==0) {
        [_tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [_tableView.mj_footer endRefreshing];
    }
    [_tableView reloadData];
}


#pragma mark ===>>> @"待下单",@"待奖励",@"已奖励",@"失效" 按钮
-(void)creatSelectBtn{
    
    _selectBtnView = [[UIView alloc]init];
    [self.view addSubview:_selectBtnView];
    [_selectBtnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);   make.right.offset(0);
        make.top.offset(0);    make.height.offset(30*HWB+1);
    }];
    
    NSArray * titleAry = @[@"待下单",@"待奖励",@"已奖励",@"已失效"];
    for (NSInteger i=0; i<4; i++) {
        _selectBtn = [UIButton titLe:titleAry[i] bgColor:COLORWHITE titColorN:Black102 titColorH:Black102 font:13*HWB];
        ADDTARGETBUTTON(_selectBtn, clickChangeSelect:)
        _selectBtn.tag = i+10 ; _selectBtn.selected = NO ;
        _selectBtn.frame = CGRectMake(ScreenW/4*i, 0, ScreenW/4, 30*HWB) ;
        [_selectBtnView addSubview:_selectBtn];
        if (i==0) {
            [_selectBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
            _selectBtn.selected = YES ;
        }
    }
    
    // 选中后的下划线
    _cLimgV = [[UIImageView alloc]init];
    _cLimgV.backgroundColor = LOGOCOLOR ;
    _cLimgV.bounds = CGRectMake(0, 0, 40*HWB, 1.5);
    _cLimgV.center = CGPointMake(ScreenW/4/2, 30*HWB-1);
    [self.view addSubview:_cLimgV];
    
}

// @"待下单",@"待奖励",@"已奖励",@"失效" 点击事件
-(void)clickChangeSelect:(UIButton*)btn{
    if (![btn isKindOfClass:[UIButton class]]) {  return ;  }
    if (btn.selected==YES) { return ; }
    
    _selectStatus = btn.tag-8 ;
    
    // 清除之前已选中的btn
    for (UIButton * sbtn in _selectBtnView.subviews) {
        if ([sbtn isKindOfClass:[UIButton class]]) {
            [sbtn setTitleColor:Black102 forState:UIControlStateNormal];
            sbtn.selected = NO ;
        }
    }
    
    _selectBtn = (UIButton*)[_selectBtnView viewWithTag:btn.tag];
    if ([_selectBtn isKindOfClass:[UIButton class]]) {
        _selectBtn.selected = YES ;
        [UIView animateWithDuration:0.2 animations:^{
            [_selectBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
            _cLimgV.center = CGPointMake(_selectBtn.center.x, 30*HWB-1);
        } completion:^(BOOL finished) {
            
            switch (_selectStatus) {
                    
                case 2:{
                    if (_dataSource1.count==0) {
                        _tableView = (UITableView*)[_scrollView viewWithTag:101];
                        if ([_tableView isKindOfClass:[UITableView class]]) {
                             [_tableView.mj_header beginRefreshing];
                        }
                    }
                } break;
                    
                case 3:{
                    if (_dataSource2.count==0) {
                        [self downloadData];
                        _tableView = (UITableView*)[_scrollView viewWithTag:102];
                        if ([_tableView isKindOfClass:[UITableView class]]) {
                            [_tableView.mj_header beginRefreshing];
                        }
                    }
                } break;
                    
                case 4:{
                    if (_dataSource3.count==0) {
                        [self downloadData];
                        _tableView = (UITableView*)[_scrollView viewWithTag:103];
                        if ([_tableView isKindOfClass:[UITableView class]]) {
                            [_tableView.mj_header beginRefreshing];
                        }
                    }
                } break;
                    
                case 5:{
                    if (_dataSource4.count==0) {
                        [self downloadData];
                        _tableView = (UITableView*)[_scrollView viewWithTag:104];
                        if ([_tableView isKindOfClass:[UITableView class]]) {
                            [_tableView.mj_header beginRefreshing];
                        }
                    }
                } break;
                    
                default:  break;
            }
        }];
        
        [_scrollView setContentOffset:CGPointMake(ScreenW*(btn.tag-10), 0) animated:NO] ;
    }
}


#pragma mark ===>>>  创建scrollView ，放四个UITableView
-(UIScrollView*)scrollView{
    if (!_scrollView) {
        _scrollView=[[UIScrollView alloc]init];
        _scrollView.backgroundColor = COLORGROUP ;
        _scrollView.contentSize = CGSizeMake(ScreenW*4,0);
        _scrollView.pagingEnabled = YES;   _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.tag = 1000 ;
        _scrollView.frame = CGRectMake(0, 30*HWB+2, ScreenW, ScreenH-NAVCBAR_HEIGHT-30*HWB-1) ;
        [self.view addSubview:_scrollView];
    }
    return _scrollView ;
}

#pragma mark ===>>> UIScrollView 代理
// 使用代码改变偏移量时执行
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView.tag==1000) { // 滑动的是scrollView
        NSInteger sbtnTag = scrollView.contentOffset.x/ScreenW + 10  ;
        _selectBtn = (UIButton*)[_selectBtnView viewWithTag:sbtnTag];
        [self clickChangeSelect:_selectBtn];
    }
}

// 手动滑动结束后执行
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag==1000) { // 滑动的是scrollView
        NSInteger sbtnTag = scrollView.contentOffset.x/ScreenW + 10 ;
        _selectBtn = (UIButton*)[_selectBtnView viewWithTag:sbtnTag];
        [self clickChangeSelect:_selectBtn];
    }
}

// 开始拖动时 关闭键盘
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


#pragma mark ===>>> 用for循环创建 四个tableView
-(void)creatTableView{
    __weak typeof(self) weakSelf = self;
    for (NSInteger i=0; i<4; i++) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = COLORGROUP ;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tag = i + 101 ;
        _tableView.delegate = self;  _tableView.dataSource = self;
        _tableView.frame = CGRectMake(ScreenW*i,0,ScreenW,HFRAME(_scrollView));
        [_scrollView addSubview:_tableView];
        
        if (i==0) {  [_tableView registerClass:[MyFreeOrderWillCreateOrderCell class] forCellReuseIdentifier:@"cell1"]; }
        if (i==1) {  [_tableView registerClass:[MyFreeOrderAwardCell class] forCellReuseIdentifier:@"cell2"]; }
        if (i==2) {  [_tableView registerClass:[MyFreeOrderAwardCell class] forCellReuseIdentifier:@"cell3"]; }
        if (i==3) {  [_tableView registerClass:[MyFreeOrderfailureCell class] forCellReuseIdentifier:@"cell4"]; }
        [_tableView registerClass:[WelfareNullCell class] forCellReuseIdentifier:@"null_cell"];
        
        //下拉刷新
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            switch (i) {
                case 0:{
                    weakSelf.pageCount1 = @"1" ;
                    [weakSelf.dataSource1 removeAllObjects];
                    [weakSelf.tableView.mj_footer resetNoMoreData];
                } break;
                    
                case 1:{
                    weakSelf.pageCount2 = @"1" ;
                    [weakSelf.dataSource2 removeAllObjects];
                     [weakSelf.tableView.mj_footer resetNoMoreData];
                } break;
                    
                case 2:{
                    weakSelf.pageCount3 = @"1" ;
                    [weakSelf.dataSource3 removeAllObjects];
                    [weakSelf.tableView.mj_footer resetNoMoreData];
                } break;
                    
                case 3:{
                    weakSelf.pageCount4 = @"1" ;
                    [weakSelf.dataSource4 removeAllObjects];
                    [weakSelf.tableView.mj_footer resetNoMoreData];
                } break;
                default:  break;
            }
            [weakSelf downloadData];
        }];
        
        //上拉加载更多
        _tableView.mj_footer = [MJRefreshBackNormalFooter  footerWithRefreshingBlock:^{
            switch (i) {
                case 0:{ weakSelf.pageCount1 = FORMATSTR(@"%d",[weakSelf.pageCount1 intValue]+1); } break;
                    
                case 1:{ weakSelf.pageCount2 = FORMATSTR(@"%d",[weakSelf.pageCount2 intValue]+1); } break;
                    
                case 2:{ weakSelf.pageCount3 = FORMATSTR(@"%d",[weakSelf.pageCount3 intValue]+1); } break;
                    
                case 3:{ weakSelf.pageCount4 = FORMATSTR(@"%d",[weakSelf.pageCount4 intValue]+1);  } break;
               
                default:  break;
            }
            [weakSelf downloadData];
        }];
        
        if (i==0) { [_tableView.mj_header beginRefreshing]; }
        
    }
}


#pragma mark ===>>> TableView 代理方法
// 返回cell高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (tableView.tag) {
        case 101: { if(_dataSource1.count==0) { return HFRAME(_scrollView) ; } else { return 95*HWB ; } } break;
        case 102: { if(_dataSource2.count==0) { return HFRAME(_scrollView) ; } else { return 95*HWB ; }   } break;
        case 103: { if(_dataSource3.count==0) { return HFRAME(_scrollView) ; } else { return 95*HWB ; }   } break;
        case 104: { if(_dataSource4.count==0) { return HFRAME(_scrollView) ; } else { return 95*HWB ; }   } break;
        default:  { return 95*HWB ; } break;
    }
}
// 返回cell个数，当数据源为空时，  默认返回一个cell (用来显示提示信息)
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (tableView.tag) {
        case 101: { if(_dataSource1.count>0){ return _dataSource1.count ;} else { return 1 ; } } break;
        case 102: { if(_dataSource2.count>0){ return _dataSource2.count ;} else { return 1 ; } } break;
        case 103: { if(_dataSource3.count>0){ return _dataSource3.count ;} else { return 1 ; } } break;
        case 104: { if(_dataSource4.count>0){ return _dataSource4.count ;} else { return 1 ; } } break;
        default:  { return 0 ; } break;
    }
}
// 根据不同的tag值，返回不同的cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (tableView.tag) {
        case 101: { // 待下单
            if (_dataSource1.count==0) {
                WelfareNullCell * nullCell = [tableView dequeueReusableCellWithIdentifier:@"null_cell" forIndexPath:indexPath];
                return nullCell ;
            }
            MyFreeOrderWillCreateOrderCell * cell1 = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
            cell1.model = _dataSource1[indexPath.row];
            cell1.delegate = self ;
            return cell1 ;
        } break;
            
        case 102: { // 待奖励
            if (_dataSource2.count==0) {
                WelfareNullCell * nullCell = [tableView dequeueReusableCellWithIdentifier:@"null_cell" forIndexPath:indexPath];
                return nullCell ;
            }
            MyFreeOrderAwardCell * cell2 = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
            cell2.model1 = _dataSource2[indexPath.row];
            return cell2 ;
        } break;
            
        case 103: { // 已奖励
            if (_dataSource3.count==0) {
                WelfareNullCell * nullCell = [tableView dequeueReusableCellWithIdentifier:@"null_cell" forIndexPath:indexPath];
                return nullCell ;
            }
            MyFreeOrderAwardCell * cell3 = [tableView dequeueReusableCellWithIdentifier:@"cell3" forIndexPath:indexPath];
            cell3.model2 = _dataSource3[indexPath.row];
            return cell3 ;
        } break;
            
        case 104: { // 已失效
            if (_dataSource4.count==0) {
                WelfareNullCell * nullCell = [tableView dequeueReusableCellWithIdentifier:@"null_cell" forIndexPath:indexPath];
                return nullCell ;
            }
            MyFreeOrderfailureCell * cell4 = [tableView dequeueReusableCellWithIdentifier:@"cell4" forIndexPath:indexPath];
            cell4.model = _dataSource4[indexPath.row];
            return cell4 ;
        } break;
            
        default:  { return nil ; } break;
    }
}

// 参考来源：https://www.jianshu.com/p/a5834f147ef0
#pragma mark ==>> cell上倒计时有关操作
-(void)enumerateDatasourceCountDown {
    
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(numberCutDown:) userInfo:nil repeats:YES];
    [self.timerArr addObject:timer];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
}

-(void)countDownModel:(MyFreeOrderModel *)model andIndexPath:(NSInteger )indexInteger { //哪一行的数据有倒计时
    NSString *indexKey = [NSString stringWithFormat:@"%ld",(long)indexInteger];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:indexKey forKey:@"indexPath"];
    
    //把模型里的倒计时储存在字典中 以行数index索引为key //添加定时器之前先判断这一行的数据是不是已经添加了定时器
    [dict setObject:@(model.restTime) forKey:indexKey];
    
    NSNumber * number = self.countDownTimeDict[indexKey];
    NSInteger numberInteger = [number integerValue]; //如果在倒计时的字典中取不到 value 说明还没有添加定时器
    NSLog(@"%ld",(long)numberInteger);
    
    [self.countDownTimeDict addEntriesFromDictionary:dict];
}

// 遍历当前数组中的倒计时，并修改cell上显示倒计时的文字
-(void)numberCutDown:(NSTimer *)timer {
    UITableView * tableView = (UITableView*)[_scrollView viewWithTag:101];
    for(int i = 0; i < self.dataSource1.count; ++i) {
        MyFreeOrderModel *model = self.dataSource1[i];
        if (model.restTime) {
            //修改模型时间
            if (model.restTime>0) {
                model.restTime--;
            }else{
                model.restTime=0;
                return;
            }
            
            //刷新界面
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            
            MyFreeOrderWillCreateOrderCell * cell = [tableView cellForRowAtIndexPath:indexPath] ;
            
            cell.timerLab.text=[NSString stringWithFormat:@"有效时间剩余:%02ld:%02ld:%02ld",model.restTime/3600,model.restTime%3600/60,model.restTime%60];
            if (model.restTime==0) {
                cell.orderTextF.enabled = NO ;
            }
        }
    }
}


#pragma mark ==>> 待下单中，订单号上传成功的代理方法
-(void)upFreeOrderSuccess{
    _selectBtn = (UIButton*)[_selectBtnView viewWithTag:11];
    [self clickChangeSelect:_selectBtn];
    
    if (_dataSource2.count>0) { // 如果小于0，此处无需调用刷新
        _tableView = (UITableView*)[_scrollView viewWithTag:102];
        [_tableView.mj_header beginRefreshing];
    }
 
    _tableView = (UITableView*)[_scrollView viewWithTag:101];
    [_tableView.mj_header beginRefreshing];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
