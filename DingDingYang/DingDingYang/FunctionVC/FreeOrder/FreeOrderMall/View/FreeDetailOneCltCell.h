//
//  FreeDetailOneCltCell.h
//  DingDingYang
//
//  Created by ddy on 2017/12/2.
//  Copyright © 2017年 ddy.All rights reserved.

//  免单商品详情页第一分区、显示商品信息

#import <UIKit/UIKit.h>
#import "FreeOrderModel.h"
#import "SDCycleScrollView.h"

@interface FreeDetailOneCltCell : UICollectionViewCell<SDCycleScrollViewDelegate>

@property(nonatomic,strong) SDCycleScrollView * scsView ;

@property(nonatomic,strong) NSArray * urlAry ;        // 详情页(图片)
@property(nonatomic,strong) UIView  * titlrBgView ;   // 放置商品标题等
@property(nonatomic,strong) UILabel * titleLab ;      // 商品标题
@property(nonatomic,strong) UILabel * rmdStrLab ;     // 推荐语
@property(nonatomic,strong) UILabel * oldPriceLab ;   // 原价
@property(nonatomic,strong) UILabel * xlLab ;         // 销量
@property(nonatomic,strong) UIView  * stateBgView ;   // 开团提醒View
@property(nonatomic,strong) UIButton * txStateBtn ;   // 开团提醒状态
@property(nonatomic,strong) UIImageView * tixingImg ; // 开团提醒图片

@property(nonatomic,strong) FreeOrderModel * fgModel ;
@end
