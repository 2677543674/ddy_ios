//
//  FreeOrderCell.h
//  DingDingYang
//
//  Created by ddy on 10/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GoodsModel.h"
#import "FreeOrderModel.h"
@interface FreeOrderCell : UITableViewCell


@property(nonatomic,strong) UIImageView * goodsImgV ;  // 商品主图
@property(nonatomic,strong) UIImageView * bgImgV ;     // 半透明背景遮罩
@property(nonatomic,strong) UILabel     * stateLab ;   // 明日预告，即将开始等显示
@property(nonatomic,strong) UILabel     * titleLab ;   // 商品标题
@property(nonatomic,strong) UILabel     * activityLab; // 活动时间
@property(nonatomic,strong) UIButton    * restNumBtn ; // 剩余份数
@property(nonatomic,strong) UIButton    * kaiTuanBtn ; // 我要福利按钮

@property(nonatomic,strong) FreeOrderModel * freeModel ;
@end









