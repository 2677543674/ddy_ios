//
//  FreeOrderFlowCell.m
//  DingDingYang
//
//  Created by ddy on 25/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "FreeOrderFlowCell.h"
#import "OrderPraiseListVC.h"

@implementation FreeOrderFlowCell


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        [self rulesImgV];
        [self wFRuleBtn];
        [self winnersBtn];
    }
    return self ;
}

-(UIImageView*)rulesImgV{
    if (!_rulesImgV) {
        _rulesImgV = [[UIImageView alloc]init];
        _rulesImgV.image = [UIImage imageNamed:@"福利流程"];
        _rulesImgV.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_rulesImgV];
        [_rulesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);   make.left.offset(0);
            make.right.offset(0); make.height.offset(ScreenW/7.12);
        }];
    }
    return _rulesImgV ;
}

-(UIButton*)wFRuleBtn{
    if (!_wFRuleBtn) {
        _wFRuleBtn = [UIButton titLe:@"福利规则" bgColor:SHAREBTNCOLOR titColorN:COLORWHITE titColorH:Black51 font:13*HWB];
        _wFRuleBtn.layer.masksToBounds = YES ;
        _wFRuleBtn.layer.cornerRadius = 2 ;
        ADDTARGETBUTTON(_wFRuleBtn, fuliRules)
        [self.contentView addSubview:_wFRuleBtn];
        [_wFRuleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.bottom.offset(-10);
            make.top.equalTo(_rulesImgV.mas_bottom).offset(2);
            make.right.equalTo(self.contentView.mas_centerX).offset(-8*HWB);
        }];
    }
    return _wFRuleBtn ;
}

-(UIButton*)winnersBtn{
    if (!_winnersBtn) {
        _winnersBtn = [UIButton titLe:@"获奖名单" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:Black51 font:13*HWB];
        _winnersBtn.layer.masksToBounds = YES ;
        _winnersBtn.layer.cornerRadius = 2 ;
        ADDTARGETBUTTON(_winnersBtn, winnersList)
        [self.contentView addSubview:_winnersBtn];
        [_winnersBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB); make.bottom.offset(-10);
            make.top.equalTo(_rulesImgV.mas_bottom).offset(2);
            make.left.equalTo(self.contentView.mas_centerX).offset(8*HWB);
        }];
    }
    return _winnersBtn ;
}


-(void)fuliRules{
 
    [NetRequest requestTokenURL:url_free_rules parameter:@{@"goodsFreeId":_freeModel.fgid}  success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {

            NSString * rules = REPLACENULL(nwdic[@"freeRole"]) ;
            if (rules.length==0) {
                rules = @"1: 申请[我要福利]--分享给好友--好友在APP给此商品[点赞]\n\n2: 获得规定的点赞数即可获得福利资格\n\n3: 当福利份数为1000份，如已经被领完了，那么你的好友将看到[名额已满] 的不可点赞的状态，你的好友将可在其他商品中点赞。\n\n4: 如你未申请福利，则你的好友不会看到[点赞] 按钮，不会浪费好友名额。\n\n5: 为营造一个公平公正的福利发放让更多的用户受益，防止有几个号的用户自助式的获得福利商品，平台规定，你名下的好友在一定时间段内(暂定为三个月) 只可以帮助你一次，意思是，每个好友在三个月内只能点赞一次。\n\n6: 福利商品不允许申请退款，否则将会受到处罚且不再享受福利资格。" ;
            }
            rules = [rules stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            [UIAlertController showIn:TopNavc.topViewController title:@"福利规则\n" content:rules ctAlignment:NSTextAlignmentLeft btnAry:@[@"知道啦"] indexAction:^(NSInteger indexTag) {
            }];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

-(void)winnersList{
    OrderPraiseListVC *orderPraiseListVC = [[OrderPraiseListVC alloc]init];
    orderPraiseListVC.goodsFreeId = _freeModel.fgid ;
    [TopNavc pushViewController:orderPraiseListVC animated:YES];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

@end




