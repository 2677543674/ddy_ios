//
//  OrderPraiseListCell.m
//  DingDingYang
//
//  Created by ddy on 18/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "OrderPraiseListCell.h"

@implementation OrderPraiseListCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self headImgV];
        [self nicknameLab];
        [self mobileLab];
        
        UIImageView * linesImgV = [[UIImageView alloc]init];
        linesImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImgV];
        [linesImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.right.offset(0);
            make.bottom.offset(0); make.height.offset(1);
        }];
    }
    return self ;
}

// 头像
-(UIImageView*)headImgV{
    if (!_headImgV) {
        _headImgV = [[UIImageView alloc]init];
        _headImgV.layer.masksToBounds = YES ;
        _headImgV.layer.cornerRadius = 20 ;
        [self.contentView addSubview:_headImgV];
        [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB);  make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.offset(40) ;   make.width.offset(40);
        }];
    }
    return _headImgV ;
}

// 昵称
-(UILabel*)nicknameLab{
    if (!_nicknameLab) {
        _nicknameLab = [UILabel labText:@"昵称" color:Black102 font:14*HWB];
        [self.contentView addSubview:_nicknameLab];
        [_nicknameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(10);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
    }
    return _nicknameLab ;
}

// 手机号
-(UILabel*)mobileLab{
    if (!_mobileLab) {
        _mobileLab = [UILabel labText:@"***手机号***" color:Black102 font:14*HWB];
        [self.contentView addSubview:_mobileLab];
        [_mobileLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
    }
    return _mobileLab ;
}





- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
