//
//  FreeOrderFlowCell.h
//  DingDingYang
//
//  Created by ddy on 25/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FreeOrderFlowCell : UICollectionViewCell 

@property(nonatomic,strong) UICollectionView * welfareFlowView ; // 展示福利流程
@property(nonatomic,strong) UIImageView * rulesImgV ; // 福利流程图片
@property(nonatomic,strong) UIButton * wFRuleBtn ;    // 福利规则按钮
@property(nonatomic,strong) UIButton * winnersBtn ;   // 获奖名单按钮

@property(nonatomic,strong) FreeOrderModel * freeModel ;

@end










