//
//  FreeDetailOneCltCell.m
//  DingDingYang
//
//  Created by ddy on 2017/12/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "FreeDetailOneCltCell.h"
@implementation FreeDetailOneCltCell

-(void)setFgModel:(FreeOrderModel *)fgModel{
    _fgModel = fgModel;
    _titleLab.text = _fgModel.goodsName ;
    _rmdStrLab.text = _fgModel.recommendedContent ;
    _oldPriceLab.text = FORMATSTR(@"￥%@",_fgModel.price) ;
    _xlLab.text = FORMATSTR(@"月销%@",_fgModel.sales) ;
    [self changeUi];
}

//已开团不显示推送、未开团显示
-(void)changeUi{
    if (_fgModel.ifStart==1) {
        _stateBgView.hidden = YES ;
        [_titlrBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.top.equalTo(_scsView.mas_bottom) ;
            make.right.offset(0);  make.bottom.offset(0);
        }];
    }else{
        _stateBgView.hidden = NO ;
        [_titlrBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.top.equalTo(_scsView.mas_bottom) ;
            make.right.offset(-76);  make.bottom.offset(0);
        }];
        [self changePushMsgState];
    }
}

//设置商品图片轮播图
-(void)setUrlAry:(NSMutableArray *)urlAry{
    _urlAry = urlAry ;
    _scsView.imageURLStringsGroup = _urlAry ;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self scsView]; //商品主图轮播
        
        // 商品标题有关
        [self titlrBgView];
        [self titleLab];
        [self rmdStrLab]; //推荐语
        [self oldPriceLab];
        [self xlLab];
        
        // 开团提醒有关
        [self stateBgView];
        [self tixingImg];
        [self txStateBtn];
    
    }
    return self ;
}

-(SDCycleScrollView*)scsView{
    if (!_scsView) {
        _scsView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, ScreenW, ScreenW) imageURLStringsGroup:nil];
        _scsView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _scsView.delegate = self;
        _scsView.backgroundColor = COLORGROUP ;
        _scsView.currentPageDotColor = LOGOCOLOR;
        _scsView.pageDotColor = COLORWHITE;
        _scsView.autoScrollTimeInterval=3.0;
        [self.contentView addSubview:_scsView];
    }
    return _scsView ;
}
//商品详情页轮播图代理
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (_urlAry.count>0) {
        [ImgPreviewVC show:TopNavc.topViewController type:0 index:index imagesAry:_urlAry];
    }
}

#pragma mark 商品标题有关

-(UIView*)titlrBgView{
    if (!_titlrBgView) {
        _titlrBgView = [[UIView alloc]init];
        _titlrBgView.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_titlrBgView];
        [_titlrBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.top.equalTo(_scsView.mas_bottom) ;
            make.right.offset(-76);  make.bottom.offset(0);
        }];
    }
    return _titlrBgView ;
}

//商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black51 font:14*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [_titlrBgView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.top.offset(6);
            make.right.offset(-10);
        }];
    }
    return _titleLab;
}

//推荐语
-(UILabel*)rmdStrLab{
    if (!_rmdStrLab) {
        _rmdStrLab = [UILabel labText:@"推荐理由" color:LOGOCOLOR font:12.5*HWB];
        _rmdStrLab.numberOfLines = 0 ;
        [_titlrBgView addSubview:_rmdStrLab];
        [_rmdStrLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.top.equalTo(_titleLab.mas_bottom).offset(6);
            make.left.offset(15);   make.right.offset(-10);
        }];
    }
    return _rmdStrLab ;
}

//原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:LOGOCOLOR font:20*HWB];
        _oldPriceLab.font = [UIFont boldSystemFontOfSize:20*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [_titlrBgView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.bottom.offset(-6);
        }];
    }
    return _oldPriceLab;
}

//销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"月销0万" color:Black102 font:12.5* HWB];
        [_titlrBgView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_oldPriceLab.mas_right).offset(10);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
        }];
    }
    return _xlLab;
}


#pragma mark  //开团提醒按钮,有关
-(UIView*)stateBgView{
    if (!_stateBgView) {
        _stateBgView = [[UIView alloc]init];
        _stateBgView.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_stateBgView];
        [_stateBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_scsView.mas_bottom).offset(0);
            make.right.offset(0);  make.width.offset(75);
            make.bottom.offset(0);
        }];
        
        UITapGestureRecognizer * tapTiXing = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(notificationBtnClick)];
        [_stateBgView addGestureRecognizer:tapTiXing];
        
        UIImageView * lines = [[UIImageView alloc]init];
        lines.backgroundColor = Black204 ;
        [_stateBgView addSubview:lines];
        [lines mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.width.offset(1);
            make.centerY.equalTo(_stateBgView.mas_centerY);
            make.height.offset(50);
        }];
    }
    return _stateBgView ;
}

-(UIImageView*)tixingImg{
    if (!_tixingImg) {
        _tixingImg = [[UIImageView alloc]init];
        _tixingImg.image = [UIImage imageNamed:@"开团提醒1"];
        _tixingImg.contentMode = UIViewContentModeScaleAspectFit ;
        [_stateBgView addSubview:_tixingImg];
        [_tixingImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_stateBgView.mas_centerY).offset(4);
            make.centerX.equalTo(_stateBgView.mas_centerX);
            make.width.offset(30);  make.height.offset(40);
        }];
    }
    return _tixingImg ;
}
-(UIButton*)txStateBtn{
    if (!_txStateBtn) {
        _txStateBtn = [UIButton titLe:@" 开团提醒 " bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13.0];
        _txStateBtn.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
        _txStateBtn.userInteractionEnabled = NO ;
        [_stateBgView addSubview:_txStateBtn];
        [_txStateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_stateBgView.mas_centerX);
            make.top.equalTo(_stateBgView.mas_centerY).offset(8);
            make.height.offset(16);
        }];
    }
    return _txStateBtn ;
}

#pragma mark ==>> 点击开团提醒
-(void)notificationBtnClick{
    [NetRequest requestTokenURL:url_free_setNotice parameter:@{@"goodsFreeId":_fgModel.fgid} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            _fgModel.ifNotice=[nwdic[@"ifNotice"] integerValue];
            [self changePushMsgState];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

-(void)changePushMsgState{
    if (_fgModel.ifNotice==1) {
        [_txStateBtn setTitle:@" 提醒已开启 "  forState:UIControlStateNormal];
        _txStateBtn.backgroundColor = [UIColor grayColor];
        _tixingImg.image = [UIImage imageNamed:@"开团提醒2"];
    }else{
        [_txStateBtn setTitle:@" 开团提醒 "  forState:UIControlStateNormal];
        _txStateBtn.backgroundColor = LOGOCOLOR ;
        _tixingImg.image = [[UIImage imageNamed:@"开团提醒1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
}











@end
