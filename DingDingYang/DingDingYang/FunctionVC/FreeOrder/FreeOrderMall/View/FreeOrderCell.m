//
//  FreeOrderCell.m
//  DingDingYang
//
//  Created by ddy on 10/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "FreeOrderCell.h"


@implementation FreeOrderCell

//设置数据
-(void)setFreeModel:(FreeOrderModel *)freeModel{
  
    _freeModel = freeModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_freeModel.goodsPic] placeholderImage:nil];
    _titleLab.text = _freeModel.goodsName ;
    [_restNumBtn setTitle:FORMATSTR(@"仅剩%@份",_freeModel.restNum) forState:UIControlStateNormal];
    _activityLab.text = FORMATSTR(@"活动时间: %@",_freeModel.activityTime) ;
    switch (_freeModel.ifStart) {
        case 0: { // 将开始
            self.bgImgV.hidden = YES ;
            self.stateLab.text = FORMATSTR(@"即将开始\n%@",TimeByStamp(_freeModel.startTime, @"hh:mm"));
           
        } break;
            
        case 1: { // 正在抢购
            self.bgImgV.hidden = YES ;
            self.stateLab.text = @"正在抢购" ;
        } break;
            
        case 2: { // 明日预告
            self.bgImgV.hidden = NO ;
            self.stateLab.text = FORMATSTR(@"明日预告\n%@",TimeByStamp(_freeModel.startTime, @"hh:mm"));
        } break;
            
        default: break;
    }
    
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self goodsImgV];
        [self bgImgV];
        [self stateLab];
        [self titleLab];
        [self activityLab];
        [self restNumBtn];
        [self kaiTuanBtn];
        
        _restNumBtn.userInteractionEnabled = NO ;
        _kaiTuanBtn.userInteractionEnabled = NO ;
        
        UIImageView * linesImg = [[UIImageView alloc]init];
        linesImg.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:linesImg];
        [linesImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.left.offset(0);
            make.right.offset(0); make.height.offset(5*HWB);
        }];
    }
    return self ;
}

-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _goodsImgV.clipsToBounds = YES ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.top.offset(0);
            make.right.offset(0);  make.height.offset(ScreenW/2);
        }];
    }
    return _goodsImgV ;
}

-(UIImageView*)bgImgV{
    if (!_bgImgV) {
        _bgImgV = [[UIImageView alloc]init];
        _bgImgV.hidden = YES ;
        _bgImgV.backgroundColor = RGBA(0, 0, 0, 0.4) ;
        [self.contentView addSubview:_bgImgV];
        [_bgImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.top.offset(0);
            make.right.offset(0);  make.height.offset(ScreenW/2);
        }];
    }
    return _bgImgV ;
}

-(UILabel*)stateLab{
    if (!_stateLab) {
        _stateLab = [UILabel labText:@"明日预告" color:COLORWHITE font:15*HWB];
        _stateLab.font = [UIFont boldSystemFontOfSize:16*HWB];
        _stateLab.backgroundColor = RGBA(0, 0, 0, 0.6) ;
        _stateLab.layer.masksToBounds = YES ;
        _stateLab.layer.cornerRadius = 43*HWB ;
        _stateLab.numberOfLines = 0 ;
        _stateLab.textAlignment = NSTextAlignmentCenter ;
        [self.contentView addSubview:_stateLab];
        [_stateLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(86*HWB);
            make.width.offset(86*HWB);
        }];
    }
    return _stateLab ;
}

-(UILabel*)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel labText:@"商品标题" color:Black51 font:13.5*HWB];
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12);   make.right.offset(-12);
            make.top.equalTo(_bgImgV.mas_bottom).offset(5*HWB);
            make.height.offset(30);
        }];
    }
    return _titleLab ;
}

-(UILabel*)activityLab{
    if (!_activityLab) {
        _activityLab = [UILabel labText:@"活动时间: " color:LOGOCOLOR font:12.0*HWB];
        [self.contentView addSubview:_activityLab];
        [_activityLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12);  make.top.equalTo(_titleLab.mas_bottom).offset(1);
        }];
    }
    return _activityLab ;
}

-(UIButton*)restNumBtn{
    if (!_restNumBtn) {
        _restNumBtn = [UIButton titLe:@"仅剩0份" bgColor:COLORCLEAR titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:14*HWB];
        [_restNumBtn boardWidth:1 boardColor:LOGOCOLOR corner:15*HWB];
        [self.contentView addSubview:_restNumBtn];
        [_restNumBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);       make.bottom.offset(-20);
            make.height.offset(30*HWB); make.width.offset(95*HWB);
        }];
    }
    return _restNumBtn ;
}

-(UIButton*)kaiTuanBtn{
    if (!_kaiTuanBtn) {
        _kaiTuanBtn = [UIButton titLe:@"我要福利" bgColor:COLORCLEAR titColorN:COLORWHITE titColorH:COLORWHITE font:15*HWB];
        _kaiTuanBtn.layer.masksToBounds = YES ;
        _kaiTuanBtn.layer.cornerRadius =  15 * HWB ;
        [_kaiTuanBtn addShadowLayer];
        
        const CGFloat * component = CGColorGetComponents(LOGOCOLOR.CGColor); // 取出的值是一个数组
        UIColor * newColor1 = [UIColor colorWithRed:component[0] green:component[1] blue:component[2] alpha:0.5];
        UIColor * newColor2 = [UIColor colorWithRed:component[0] green:component[1] blue:component[2] alpha:1];
        UIImage * image = [self buttonImageFromColors:@[newColor1,newColor2] ByGradientType:0];
        [_kaiTuanBtn setBackgroundImage:image forState:UIControlStateNormal];
        
        [self.contentView addSubview:_kaiTuanBtn];
        [_kaiTuanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-12*HWB);  make.height.offset(30*HWB);
            make.centerY.equalTo(_restNumBtn.mas_centerY);
            make.left.equalTo(self.contentView.mas_centerX);
        }];
    }
    return _kaiTuanBtn ;
}

//生成一张渐变色图片(我要开团按钮专用)
- (UIImage*) buttonImageFromColors:(NSArray*)colors ByGradientType:(NSInteger)gradientType{
    NSMutableArray *ar = [NSMutableArray array];
    for(UIColor * c in colors) {
        [ar addObject:(id)c.CGColor];
    }
    CGSize size = CGSizeMake(ScreenW/2-12*HWB, 30*HWB) ;
    UIGraphicsBeginImageContextWithOptions(size, NO, 3);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGColorSpaceRef colorSpace = CGColorGetColorSpace([[colors lastObject] CGColor]);
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)ar, NULL);
    CGPoint start = CGPointZero ;   CGPoint end = CGPointZero ;
    switch (gradientType) {
        case 0  :{
            start = CGPointMake(0.0, 0.0);
            end = CGPointMake(0.0, size.height);
        }  break;
        case 1  :{
            start = CGPointMake(0.0, 0.0);
            end = CGPointMake(size.height, 0.0);
        }  break;
        case 2  :{
            start = CGPointMake(0.0, 0.0);
            end = CGPointMake(size.width, size.height);
        }  break;
        case 3  :{
            start = CGPointMake(size.width, 0.0);
            end = CGPointMake(0.0, size.height);
        }  break;
            
        default:
            break;
    }

    CGContextDrawLinearGradient(context, gradient, start, end, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGContextRestoreGState(context);
    CGColorSpaceRelease(colorSpace);
    UIGraphicsEndImageContext();
    return image;
}





- (void)awakeFromNib {
    [super awakeFromNib];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
