//
//  GiveLikeFrendsCltCell.h
//  DingDingYang
//
//  Created by ddy on 2017/12/21.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

//免单详情页已点赞好友cell
@interface GiveLikeFriendsCltCell : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UILabel * tishiLab1 ;
@property(nonatomic,strong) UILabel * tishiLab2 ;
@property(nonatomic,strong) UICollectionView * glfCltView ;
//@property(nonatomic,strong) NSArray * friendListAry ;
@property(nonatomic,strong) FreeOrderModel * freeModel ;
@end


// 免单详情页已点赞好友（横向cell）
@interface GlfFriendsListCltCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView * headImgV ;
@property(nonatomic,strong) UILabel * nickNameLab ;

@end


