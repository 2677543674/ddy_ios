//
//  GiveLikeFrendsCltCell.m
//  DingDingYang
//
//  Created by ddy on 2017/12/21.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "GiveLikeFriendsCltCell.h"

@implementation GiveLikeFriendsCltCell

-(void)setFreeModel:(FreeOrderModel *)freeModel{
    _freeModel = freeModel ;
    if (_freeModel.glfLiatAry.count>0) {
        _tishiLab1.textColor = Black102 ;
        _tishiLab2.hidden = NO ;
        _tishiLab1.text = @"以下好友已帮你点赞" ;
        _tishiLab2.text = FORMATSTR(@"(需要%@人帮你点赞)",_freeModel.fissionNum);
    }else{
        _tishiLab2.hidden = YES ;
        _tishiLab1.text = FORMATSTR(@"需要%@人帮你点赞哦",_freeModel.fissionNum);
        _tishiLab1.textColor = LOGOCOLOR ;
    }
        
    [_glfCltView reloadData];
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = COLORWHITE ;
        
        _tishiLab1 = [UILabel labText:@"以下好友已帮你点赞" color:Black102 font:12*HWB];
        [self.contentView addSubview:_tishiLab1];
        [_tishiLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12);  make.top.offset(6);
        }];
        
        _tishiLab2 = [UILabel labText:@"(需要N人帮你点赞)" color:LOGOCOLOR font:10.5*HWB];
        [self.contentView addSubview:_tishiLab2];
        [_tishiLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_tishiLab1.mas_right).offset(2);
            make.centerY.equalTo(_tishiLab1.mas_centerY);
        }];
        
        [self glfCltView];
        
    }
    return self ;
}


-(UICollectionView*)glfCltView{
    if (!_glfCltView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(ScreenW/4, 40);
        layout.sectionInset = UIEdgeInsetsMake(2, 2, 2, 2) ;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _glfCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _glfCltView.backgroundColor = COLORWHITE ;
        _glfCltView.delegate = self ;
        _glfCltView.dataSource = self ;
        [self.contentView addSubview:_glfCltView];
        [_glfCltView registerClass:[GlfFriendsListCltCell class] forCellWithReuseIdentifier:@"GlfFriendsListCltCell"];
        [_glfCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(22) ;  make.bottom.offset(0);
            make.left.offset(0) ;  make.right.offset(0);
        }];
        
    }
    return _glfCltView ;
}

// 代理
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _freeModel.glfLiatAry.count ;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GlfFriendsListCltCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlfFriendsListCltCell" forIndexPath:indexPath];
    NSDictionary * dic = _freeModel.glfLiatAry[indexPath.row] ;
    [cell.headImgV sd_setImageWithURL:[NSURL URLWithString:REPLACENULL(dic[@"headImg"])] placeholderImage:PlaceholderImg];
    if (REPLACENULL(dic[@"name"]).length>0) {
        cell.nickNameLab.text = REPLACENULL(dic[@"name"]) ;
    }else{
        cell.nickNameLab.text = REPLACENULL(dic[@"mobile"]) ;
    }
    return cell ;
}


@end






@implementation GlfFriendsListCltCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _headImgV = [[UIImageView alloc]init];
        _headImgV.layer.masksToBounds = YES ;
        _headImgV.layer.cornerRadius = 18 ;
        _headImgV.image = PlaceholderImg ;
        [self.contentView addSubview:_headImgV];
        [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.width.offset(36) ;
            make.height.offset(36);
            make.left.offset(10);
        }];
        
        _nickNameLab = [UILabel labText:@"昵称" color:Black102 font:12*HWB];
        _nickNameLab.textAlignment = NSTextAlignmentLeft ;
        [self.contentView addSubview:_nickNameLab];
        [_nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headImgV.mas_right).offset(2);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.offset(-5);
        }];
        
    }
    return self ;
}

@end












