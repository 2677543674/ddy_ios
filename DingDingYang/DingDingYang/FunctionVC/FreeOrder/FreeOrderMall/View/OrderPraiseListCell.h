//
//  OrderPraiseListCell.h
//  DingDingYang
//
//  Created by ddy on 18/08/2017.
//  Copyright © 2017 ddy.All rights reserved.

//  获奖名单cell

#import <UIKit/UIKit.h>

@interface OrderPraiseListCell : UITableViewCell

@property(nonatomic,strong) UIImageView * headImgV ;
@property(nonatomic,strong) UILabel     * nicknameLab ;
@property(nonatomic,strong) UILabel     * mobileLab ;

@end
