//
//  FreeOrderModel.h
//  DingDingYang
//
//  Created by ddy on 17/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FreeOrderModel : NSObject

/**  免单model **/
@property(nonatomic,copy) NSString * fgid ;               // id
@property(nonatomic,copy) NSString * ugId ;               // id
@property(nonatomic,copy) NSString * price ;              // 商品原价
@property(nonatomic,copy) NSString * num ;                // 免单总份数
@property(nonatomic,copy) NSString * restNum ;            // 免单剩余份数
@property(nonatomic,copy) NSString * status ;             // Publicshed
@property(nonatomic,copy) NSString * createTime ;         // 免单商品创建时间(时间戳，毫秒)
@property(nonatomic,copy) NSString * startTime ;          // 开始时间
@property(nonatomic,copy) NSString * endTime ;            // 结束时间
@property(nonatomic,copy) NSString * partnerId ;          // partnerId
@property(nonatomic,copy) NSString * goodsFreeApplyId ;   // 免单ID
@property(nonatomic,copy) NSString * fissionNum ;         // 免单需要邀请的人数
@property(nonatomic,copy) NSString * sales ;              // 销量
@property(nonatomic,copy) NSString * goodsId ;            // 商品ID
@property(nonatomic,copy) NSString * goodsName ;          // 商品名称
@property(nonatomic,copy) NSString * goodsPic ;           // 商品主图
@property(nonatomic,copy) NSString * itemUrl ;            // 商品详情页链接
@property(nonatomic,copy) NSString * recommendedContent ; // 推荐内容
@property(nonatomic,copy) NSString * activityTime ;       // 活动时间范围(00:00~24:00)
@property(nonatomic,copy) NSString * startHour ;          // 开始时间(小时)
@property(nonatomic,copy) NSString * freeOvertime ;       // 下单超时时间(分钟，在详情页的福利规则中显示)

@property(nonatomic,assign)NSInteger ifStart ;            // 是否开团  0:未开始  1:已开始 2:明日预告
@property(nonatomic,assign)NSInteger restTime ;           // 第一条未开始的免单商品才会有值(离开始剩余时间:毫秒)
@property(nonatomic,assign)NSInteger ifNotice ;           // 是否开启提醒
@property(nonatomic,assign)NSInteger btnType ;            // 详情页底部按钮状态


@property(nonatomic,strong) NSArray * glfLiatAry ;        // 点赞好友列表

-(instancetype)initWithDic:(NSDictionary*)dic ;


//-(void)setValue:(id)value forUndefinedKey:(NSString *)key;
@end







