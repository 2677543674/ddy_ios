//
//  FreeOrderModel.m
//  DingDingYang
//
//  Created by ddy on 17/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "FreeOrderModel.h"

@implementation FreeOrderModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        
        self.fgid               = REPLACENULL(dic[@"id"]) ;
        self.ugId               = REPLACENULL(dic[@"ugId"]) ;
        self.price              = ROUNDED(REPLACENULL(dic[@"price"]),2);
        self.num                = REPLACENULL(dic[@"num"]) ;
        self.restNum            = REPLACENULL(dic[@"restNum"]) ;
        self.status             = REPLACENULL(dic[@"status"]) ;
        self.createTime         = REPLACENULL(dic[@"createTime"]) ;
        self.startTime          = REPLACENULL(dic[@"startTime"]) ;
        self.endTime            = REPLACENULL(dic[@"endTime"]) ;
        self.partnerId          = REPLACENULL(dic[@"partnerId"]) ;
        self.goodsFreeApplyId   = REPLACENULL(dic[@"goodsFreeApplyId"]) ;
        self.fissionNum         = REPLACENULL(dic[@"fissionNum"]) ;
        self.sales              = REPLACENULL(dic[@"sales"]) ;
        self.goodsId            = REPLACENULL(dic[@"goodsId"]) ;
        self.goodsName          = REPLACENULL(dic[@"goodsName"]) ;
        self.goodsPic           = REPLACENULL(dic[@"goodsPic"]) ;
        self.itemUrl            = REPLACENULL(dic[@"itemUrl"]) ;
        self.recommendedContent = REPLACENULL(dic[@"recommendedContent"]) ;
        self.ifStart            = [REPLACENULL(dic[@"ifStart"]) integerValue] ;
        self.freeOvertime       = REPLACENULL(dic[@"freeOvertime"]);
        
        self.activityTime       = REPLACENULL(dic[@"activityTime"]) ;
        self.startHour          = REPLACENULL(dic[@"startHour"]) ;
        
        self.ifNotice           = 0 ;
        self.btnType            = 0 ;
        
        self.glfLiatAry         = @[] ;
    }
    return self ;
}



@end
