//
//  FreeOrderDetailViewController.m
//  DingDingYang
//
//  Created by ddy on 11/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "FreeOrderDetailVC.h"
#import "OneGoodsCell.h"
#import "FreeDetailOneCltCell.h"
#import "HViewOne.h"
#import "GoodsFeedBackVC.h"
#import "GoodsListCltCell1.h"
#import "EmptyCltHFView.h"
#import "FreeOrderFlowCell.h"
#import "FreeShareVC.h"
#import "OrderPraiseListVC.h"

#import "GiveLikeFriendsCltCell.h" //点赞好友

@interface FreeOrderDetailVC ()<UICollectionViewDelegate,UICollectionViewDataSource,DetailsImgDeledate,UIWebViewDelegate>

// 顶部View(颜色渐变)
@property(nonatomic,strong) UIView   * topView ;
@property(nonatomic,strong) UIButton * backBtn ;
@property(nonatomic,strong) UILabel  * titLab;

// 底部功能view和按钮
@property(nonatomic,strong) UIView * bottomView ; // 放置分享反馈按钮
@property(nonatomic,strong) UIButton * allBtn , * surplusBtn , * giveAlikeBtn ; //总份数，剩余份数，点赞好友
@property(nonatomic,strong) UIView * SpecialCodeView;
@property(nonatomic,copy)  NSString * TheSpecialCode ;

//新的商品详情页使用CollectionView实现
@property(nonatomic,strong) UICollectionView * detailCltView;
@property(nonatomic,strong) UICollectionViewFlowLayout * detailLayout ;
@property(nonatomic,strong) NSMutableArray * cnxhGoodsAry ; //猜你喜欢的商品数据
@property(nonatomic,strong) NSMutableArray * detailImgAry2 ; //商品详情
@property(nonatomic,strong) NSMutableArray * sizeImgAry ; //图片高度缓存
@property(nonatomic,strong) NSArray * freePraiseList ; //免单点赞好友列表

@property(nonatomic,strong) NSArray * detailImgAry ;   //商品主图、轮播图
@property(nonatomic,assign) BOOL  isopen; //记录商品详情的展开和折叠;

@property(nonatomic,strong) UIButton * topBtn ;  // 置顶按钮
@property(nonatomic,copy)   NSString * almmpid ; // 阿里妈妈pid
@property(nonatomic,assign) NSInteger btnType ;  // 按钮类型 1:点赞,2:我要开团,3:我要分享,4:我要下单
@property(nonatomic,assign) float titleRedStrH ; // 动态计算到的标题和推荐语的总高度(第一分区总高度)

@end

@implementation FreeOrderDetailVC



- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"福利详情";
    self.view.backgroundColor = COLORGROUP ;
    
    _titleRedStrH = 0.0 ;
    _sizeImgAry = [NSMutableArray array] ;
    _cnxhGoodsAry = [NSMutableArray array] ;
    _detailImgAry2 = [NSMutableArray array] ;
    
    
//    if (_fgModel.goodsName.length==0) {
//        SHOWMSG(@"推荐商品已过期或已下架!")
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            PopTo(YES)
//        });
//    }
//    else{
        [self getFreeGoodsDetails];
//    }
    
   
//    [self feedBackBtn];
//    [self shareBtn];
//    [self buyBtn];

//    if (THEMECOLOR==2) {
//        [_shareBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
//        [_buyBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
//    }
    
    [self detailCltView];
    [self topView];
    [self bottomView];
    [self topBtn];
    
    
}

/*
-(void)createSpecialCodeView{
    _SpecialCodeView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW-60, 200)];
    _SpecialCodeView.center=self.view.center;
    _SpecialCodeView.backgroundColor=[UIColor whiteColor];
    _SpecialCodeView.layer.masksToBounds=YES;
    _SpecialCodeView.layer.cornerRadius=4;
    [self.view addSubview:_SpecialCodeView];
    
    UILabel *label=[[UILabel alloc]init];
    label.numberOfLines=4;
    label.font=[UIFont systemFontOfSize:12];
    label.text=@"提示：请您在下单填写备注的时候，把这段特殊码粘贴进去。\n方便我们跟踪订单，保障双方权益，如没有这样操作，后果自负。";
    label.textAlignment=NSTextAlignmentCenter;
    [_SpecialCodeView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(8);make.left.offset(12);
        make.right.offset(-12);make.height.offset(100);
    }];
    
    UILabel *codeLabel=[[UILabel alloc]init];
    codeLabel.center=CGPointMake(_SpecialCodeView.center.x, _SpecialCodeView.center.y+20);
    codeLabel.layer.borderColor=[UIColor lightGrayColor].CGColor;
    codeLabel.layer.borderWidth=1;
    codeLabel.font=[UIFont systemFontOfSize:12];
    codeLabel.text=_TheSpecialCode;
    [_SpecialCodeView addSubview:codeLabel];
    [codeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(100);make.left.offset(50);
        make.right.offset(-50);make.height.offset(40);
    }];
    
    UILabel *tips=[UILabel labText:@"点击确定自动复制" color:[UIColor lightGrayColor] font:12];
    [_SpecialCodeView addSubview:tips];
    [tips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(codeLabel.mas_bottom);make.left.offset(50);
        make.right.offset(-50);make.height.offset(20);
    }];
    
    UIView *line=[[UIView alloc]init];
    line.backgroundColor=[UIColor darkGrayColor];
    [_SpecialCodeView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-41);make.left.offset(0);
        make.right.offset(0);make.height.offset(1);
    }];
    
    UIButton *button=[UIButton titLe:@"确定" bgColor:[UIColor clearColor] titColorN:[UIColor blueColor] titColorH:nil font:14];
    [button addTarget:self action:@selector(copyCode) forControlEvents:UIControlEventTouchUpInside];
    [_SpecialCodeView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(0);make.left.offset(0);
        make.right.offset(0);make.height.offset(40);
    }];
}
*/

//-(void)copyCode{
//    [MBProgressHUD showSuccess:@"复制成功!"];
//    _SpecialCodeView.hidden=YES;
//    [ClipboardMonitoring pasteboard:_TheSpecialCode] ;
//}

#pragma mark === >>> 获取免单商品详情
-(void)getFreeGoodsDetails{

    [NetRequest requestTokenURL:url_free_details parameter:@{@"goodsFreeId":_fgModel.fgid} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            _detailImgAry = NULLARY( nwdic[@"goodsFreeInfo"][@"images"] ) ;
            if (_detailImgAry.count==0) {
                if ([_fgModel.goodsPic hasPrefix:@"http"]) {
                    _detailImgAry = @[_fgModel.goodsPic];
                }
            }
//            详情页获取到的免单商品信息
//            NSDictionary * rdic = NULLDIC(nwdic[@"goodsFreeInfo"][@"goodsFree"]) ;
//            _fgModel = [[FreeOrderModel alloc]initWithDic:rdic] ;
            
            _fgModel.ifNotice = [REPLACENULL(nwdic[@"goodsFreeInfo"][@"ifNotice"])  integerValue] ;
            _btnType = [REPLACENULL(nwdic[@"goodsFreeInfo"][@"type"])  integerValue] ;
            
//            _fgModel.glfLiatAry = @[@"",@""] ;
            _fgModel.glfLiatAry = NULLARY(nwdic[@"goodsFreeInfo"][@"freePraiseList"]) ;
            
            [self setBtnState]; //设置按钮状态
            
        }else{  SHOWMSG(nwdic[@"result"])  }
        
        [_detailCltView reloadData];
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

//动态修改按钮标题
-(void)setBtnState{
    //1:点赞,2:我要开团,3:我要分享,4:名额已满，5:我要下单,6:已超时，7:已填单，8:免单名额明细
    switch (_btnType) {
        case 1:{
            [_giveAlikeBtn setTitle:@"点赞" forState:UIControlStateNormal];
        }
            break;
        case 2:{
            [_giveAlikeBtn setTitle:@"我要开团" forState:UIControlStateNormal];
        }
            break;
        case 3:{
            [_giveAlikeBtn setTitle:@"我要分享" forState:UIControlStateNormal];
        }
            break;
        case 4:{
            [_giveAlikeBtn setTitle:@"名额已满" forState:UIControlStateNormal];
//            _giveAlikeBtn.enabled = NO ;
            [_giveAlikeBtn setBackgroundColor:[UIColor lightGrayColor]];
        }
            break;
        case 5:{
            // _TheSpecialCode=nwdic[@"goodsFreeInfo"][@"randCode"];
            // [self createSpecialCodeView];
            [_giveAlikeBtn setTitle:@"立即下单" forState:UIControlStateNormal];
        }
            break;
        case 6:{
            [_giveAlikeBtn setTitle:@"已超时" forState:UIControlStateNormal];
//            _giveAlikeBtn.enabled = NO ;
            [_giveAlikeBtn setBackgroundColor:[UIColor lightGrayColor]];
        }
            break;
        case 7:{
            [_giveAlikeBtn setTitle:@"已填单" forState:UIControlStateNormal];
//            _giveAlikeBtn.enabled = NO ;
            //[_buyBtn setBackgroundColor:[UIColor lightGrayColor]];
        }
            break;
        case 8:{
            [_giveAlikeBtn setTitle:@"名额明细" forState:UIControlStateNormal];
        }
            break;
            
        default:   break;
    }
}

#pragma mark ===>>> //获取猜你喜欢
-(void)caiNiXiHuan{
    [NetRequest requestTokenURL:url_goods_list6 parameter:@{@"key":_fgModel.goodsName} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray * lAry = nwdic[@"list"];
            [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                if (![model.goodsName isEqualToString:_fgModel.goodsName]) {
                    [_cnxhGoodsAry addObject:model];
                }
            }];
            NSIndexSet * indexset = [NSIndexSet indexSetWithIndex:2];
            [_detailCltView reloadSections:indexset];
        }
    } failure:^(NSString *failure) {
    }];
}

#pragma mark ===>>> //获取商品详情
-(void)getGoodsDetailsImgs{
    
    if (_isopen==YES) {
        _isopen = NO ;
        [_detailCltView reloadData];
        [_detailCltView setContentOffset:CGPointZero animated:YES];
        return ;
    }
    
    if (_isopen==NO) {
        if (_detailImgAry2.count>0) {
            _isopen = YES ;
            [_detailCltView reloadData];
            [_detailCltView setContentOffset:CGPointMake(0, ScreenW+160) animated:YES];
        }else{
            MBShow(@"正在获取...")
            [NetRequest requestTokenURL:url_goods_detail2 parameter:@{@"goodsId":_fgModel.goodsId} success:^(NSDictionary *nwdic) {
                
                if([nwdic[@"result"] isEqualToString:@"OK"]){
                    
                    
                    if (REPLACENULL(nwdic[@"images"]).length>0) {
                        if ([nwdic[@"images"] isKindOfClass:[NSArray class]]) {
                            NSArray * aryimgs = NULLARY(nwdic[@"images"]) ;
                            [_detailImgAry2 addObjectsFromArray:aryimgs] ;
                            if (_detailImgAry2.count>0) {
                                [self jiaSuanTuPian];
                            }else{
                                MBHideHUD
                                SHOWMSG(@"未获取到商品详情!")
                            }
                        }else{
                            NSString * h5str = nwdic[@"images"];
                            if ([h5str containsString:@"<img"]&&[h5str containsString:@"/>"]) {
                                h5str = [h5str stringByReplacingOccurrencesOfString:@"<img src=\"" withString:@""];
                                h5str = [h5str stringByReplacingOccurrencesOfString:@"\"/>" withString:@","];
                                NSArray * aryimgs = [h5str componentsSeparatedByString:@","];
                                [_detailImgAry2 addObjectsFromArray:aryimgs];
                                if (_detailImgAry2.count>0) {
                                    [self jiaSuanTuPian];
                                }
                            }else{
                                MBHideHUD
                                SHOWMSG(@"未获取到商品详情!")
                            }
                        }
                        
                    }
                }
            } failure:^(NSString *failure) {
                MBHideHUD
                SHOWMSG(failure)
            }];
        }
    }
}

-(void)jiaSuanTuPian{
    SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
    [downloader downloadImageWithURL:[NSURL URLWithString:_detailImgAry2[0]]  options:SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *  targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
        MBHideHUD
        if (image==nil) {  SHOWMSG(error.localizedDescription)
        }else{
            _isopen = YES ;
            float bili = image.size.width/image.size.height ;
            NSString * hi = FORMATSTR(@"%f",ScreenW/bili) ;
            for (NSInteger i=0 ; i<_detailImgAry2.count ; i++ ) {
                [_sizeImgAry addObject:hi];
            }
            [_detailCltView reloadData];
            [_detailCltView setContentOffset:CGPointMake(0, ScreenW+160) animated:YES];
        }
    }];
}

#pragma mark === >>> 创建UICollectionView

-(UICollectionView*)detailCltView{
    if (!_detailCltView) {
        _detailLayout = [[UICollectionViewFlowLayout alloc]init];
        _detailCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:_detailLayout];
        _detailCltView.delegate = self ;
        _detailCltView.dataSource = self ;
        _detailCltView.backgroundColor = COLORGROUP ;
        _detailCltView.showsVerticalScrollIndicator = NO ;
        [self.view addSubview:_detailCltView];
        [_detailCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(-(NAVCBAR_HEIGHT-44));
            make.bottom.offset(-48);  make.left.offset(0); make.right.offset(0);
        }];
        
        [_detailCltView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        
        [_detailCltView registerClass:[FreeOrderFlowCell class] forCellWithReuseIdentifier:@"FreeOrderFlowCell"];
        [_detailCltView registerClass:[FreeDetailOneCltCell class] forCellWithReuseIdentifier:@"detail_cell_one"];
        [_detailCltView registerClass:[GoodsDetailImgCell class] forCellWithReuseIdentifier:@"detail_img_cell"];
        [_detailCltView registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"goods_listclt_cell1"];
        [_detailCltView registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"goods_listclt_cell2"];
        [_detailCltView registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"goods_listclt_cell3"];
        [_detailCltView registerClass:[GiveLikeFriendsCltCell class] forCellWithReuseIdentifier:@"give_like_friend"];
        
        //几个区头
        [_detailCltView registerClass:[HViewOne class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"more_detail_head"];
        [_detailCltView registerClass:[HViewTwo class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"cnxh_head"];
        [_detailCltView registerClass:[EmptyCltHFView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"empty_foot"];
    }
    return _detailCltView ;
}


#pragma mark === >>> UICollectionView 代理

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
//    return 4;
    return 3 ;
}

//分区个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) { return 1 ; }
    if (section==1) { return 2 ; }
    if (section==2) {
        if (_isopen==YES) { return _detailImgAry2.count; }
        if (_isopen==NO)  { return 0 ; }
    }
    return _cnxhGoodsAry.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==0){
        FreeDetailOneCltCell * onecell = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one" forIndexPath:indexPath];
        onecell.fgModel = _fgModel ;
        if (_detailImgAry.count>0) { onecell.urlAry = _detailImgAry ; }
        return onecell ;
    }
    
    if (indexPath.section == 1) {
        if (indexPath.item==0) {
            FreeOrderFlowCell*onecell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FreeOrderFlowCell" forIndexPath:indexPath];
            onecell.freeModel = _fgModel ;
            return onecell;
        }
        GiveLikeFriendsCltCell * glfCltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"give_like_friend" forIndexPath:indexPath];
        glfCltCell.freeModel = _fgModel ;
        return glfCltCell ;
    }
    
    if (indexPath.section==2) {
        GoodsDetailImgCell * detailImgCell =[collectionView dequeueReusableCellWithReuseIdentifier:@"detail_img_cell" forIndexPath:indexPath];
        [detailImgCell.detailImgV sd_setImageWithURL:[NSURL URLWithString:_detailImgAry2[indexPath.item]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (image!=nil) {
                if ([_detailImgAry2 containsObject:FORMATSTR(@"%@",imageURL)]) {
                    NSUInteger imgRow = [_detailImgAry2 indexOfObject:FORMATSTR(@"%@",imageURL)];

                    if (image.size.width>20&&image.size.height>20) {
                          NSIndexPath * srIndex = [NSIndexPath indexPathForItem:imgRow inSection:2];
                        float bili = image.size.width/image.size.height ;
                        NSString * hi = FORMATSTR(@"%.0f",ScreenW/bili) ;
                        if (![hi isEqualToString:_sizeImgAry[imgRow]]) {
                            [_sizeImgAry replaceObjectAtIndex:imgRow withObject:hi];
                        }
                         [_detailCltView reloadItemsAtIndexPaths:@[srIndex]];
                    }else{
                        [_detailImgAry2 removeObjectAtIndex:imgRow];
                        [_sizeImgAry removeObjectAtIndex:imgRow];
                        [_detailCltView reloadSections:[NSIndexSet indexSetWithIndex:2]];
                    }
                }
            }else{
                if (imageURL!=nil) {
                    NSUInteger imgRow = [_detailImgAry2 indexOfObject:FORMATSTR(@"%@",imageURL)];
                    [_detailImgAry2 removeObjectAtIndex:imgRow];
                    [_sizeImgAry removeObjectAtIndex:imgRow];
                    [_detailCltView reloadSections:[NSIndexSet indexSetWithIndex:2]];
                }
            }
            
        }];
        return detailImgCell ;
    }
    
    GoodsListCltCell3 * list3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell3" forIndexPath:indexPath];
    list3.goodsModel = _cnxhGoodsAry[indexPath.row];
    return list3 ;
}

//推荐商品列表选中事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==3) {
        [PageJump pushToGoodsDetail:_cnxhGoodsAry[indexPath.row] pushType:0];
    }
}

#pragma mark === >>> item cell 代理设置
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        if (_titleRedStrH==0) {
            CGRect titleRect = StringRect(_fgModel.goodsName, ScreenW-(_fgModel.ifStart==1?25:100), 14.0*HWB);
            CGRect desRect = StringRect(_fgModel.recommendedContent, ScreenW-(_fgModel.ifStart==1?25:100), 12.5*HWB);
            float hi = titleRect.size.height+desRect.size.height ;
            if (_fgModel.recommendedContent.length==0) { hi = titleRect.size.height -5 ; }
            _titleRedStrH = ScreenW + 55 + hi ;
        }
        return CGSizeMake(ScreenW, _titleRedStrH ) ;
    }
    
    if (indexPath.section==1) {
        if (indexPath.item==0) { return CGSizeMake(ScreenW, ScreenW/7.12 + 46 ) ; }
        if (_fgModel.glfLiatAry.count>0) {  return CGSizeMake(ScreenW, 80) ; }
        return CGSizeMake(ScreenW, 30);
    }
    
    if (indexPath.section==2) { return CGSizeMake(ScreenW-10, [_sizeImgAry[indexPath.row] floatValue]) ; }
    
    return CGSizeMake(ScreenW/2-2,ScreenW/2 + 70*HWB );
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==1) { return 8 ; }
    if (section==3) { return 5 ; }
    return 0 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (section==3) { return 4 ;}
    return 0 ;
}


#pragma mark === >>> 区头区尾代理

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqual:UICollectionElementKindSectionHeader]) {
        if (indexPath.section==2) {
            HViewOne * oneHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"more_detail_head" forIndexPath:indexPath];
            [oneHead.clickBtn addTarget:self action:@selector(getGoodsDetailsImgs) forControlEvents:UIControlEventTouchUpInside];
            oneHead.isTransform = _isopen ;
            return oneHead ;
        }
        if (indexPath.section==3) {
            HViewTwo * twoHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"cnxh_head" forIndexPath:indexPath];
            return twoHead ;
        }
    }
    if ([kind isEqual:UICollectionElementKindSectionFooter]) {
        EmptyCltHFView * empty = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"empty_foot" forIndexPath:indexPath];
        return empty ;
    }
    return  nil ;
}
//区头
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section==0) return CGSizeZero ;
    if (section==1) return CGSizeZero ;
    if (section==2) return CGSizeMake(ScreenW, 40) ;
    return (CGSize){ScreenW,30};
}
//区尾
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return (CGSize){ScreenW,8};
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"contentOffset"]) {
        float   cofsttabY=_detailCltView.contentOffset.y;
        if (cofsttabY<ScreenW) {
            _topView.backgroundColor=RGBA(255.0,255.0,255.0,cofsttabY/(ScreenW-55));
            _titLab.alpha = cofsttabY/ScreenW;
        }else{
            _topView.backgroundColor = COLORWHITE;
            _titLab.alpha = 1 ;
        }
    }
}

#pragma mark === >>> 创建底部免单总数，剩余数量，可以点击的按钮
-(UIView*)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = COLORWHITE ;
        [self.view addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.height.offset(IS_IPHONE_X?70:48);
            make.left.offset(0);  make.right.offset(0);
        }];
        [self allBtn];   [self surplusBtn];  [self giveAlikeBtn];
    }
    return _bottomView ;
}
-(UIButton*)allBtn{
    if (!_allBtn) {
        _allBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _allBtn.backgroundColor = COLORWHITE ;
        [_allBtn setTitle:FORMATSTR(@"共%@份福利",_fgModel.num) forState:UIControlStateNormal];
        _allBtn.titleLabel.font = [UIFont systemFontOfSize:14*HWB] ;
        [_allBtn setTitleColor:COLORGRAY forState:UIControlStateNormal] ;
        _allBtn.enabled = NO ;
        _allBtn.titleLabel.adjustsFontSizeToFitWidth = YES ;
        [_bottomView addSubview:_allBtn];
        [_allBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.top.offset(0);
            make.height.offset(48); make.width.offset(ScreenW/4+10);
        }];
    }
    return _allBtn ;
}
-(UIButton*)surplusBtn{
    if (!_surplusBtn) {
        _surplusBtn = [UIButton titLe:FORMATSTR(@"仅剩%@份",_fgModel.restNum) bgColor:SHAREBTNCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:14*HWB];
        _surplusBtn.titleLabel.adjustsFontSizeToFitWidth = YES ;
        [_bottomView  addSubview:_surplusBtn];
        [_surplusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_allBtn.mas_right).offset(0) ;
            make.top.offset(0); make.height.offset(48);
            make.width.offset(ScreenW/4);
        }];
    }
    return _surplusBtn ;
}

-(UIButton*)giveAlikeBtn{
    if (!_giveAlikeBtn) {
        _giveAlikeBtn = [UIButton titLe:@"点赞好友" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:15*HWB];
        _giveAlikeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14*HWB];
        ADDTARGETBUTTON(_giveAlikeBtn, giveAlike)
        [_bottomView addSubview:_giveAlikeBtn];
        [_giveAlikeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0); make.left.equalTo(_surplusBtn.mas_right).offset(0);
            make.top.offset(0); make.height.offset(48);
        }];
    }
    return _giveAlikeBtn;
}

#pragma mark 我要开团按钮点击
-(void)giveAlike{
    //1:点赞,2:我要开团,3:我要分享,4:名额已满，5:我要下单,6:已超时，7:已填单，8:免单名额明细
    switch (_btnType) {
        case 1: { [self friendsPraise] ;    } break ;
        case 2: { [self joinGroupBuying];   } break ;
        case 3: { [self shareGoods] ;       } break ;
        case 4: { SHOWMSG(@"名额已满!") } break ;
        case 5: { [self arrowStatesAndIsOpen] ; } break ;
        case 6: { SHOWMSG(@"已超时!") } break ;
        case 7: { SHOWMSG(@"已填单!") } break ;
        case 8: { [self pushOrderPraiseList] ; } break ;
        default:  break;
    }
}

#pragma mark ===>>> 按钮相应事件
//助力好友
-(void)friendsPraise{
    NSDictionary * parameterDic = @{@"goodsFreeId":_fgModel.fgid};
    [NetRequest requestTokenURL:url_free_praise parameter:parameterDic success:^(NSDictionary * nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"] ) {
            SHOWMSG(@"点赞成功！")
            _btnType = 2 ;
            [_giveAlikeBtn setTitle:@"我要开团" forState:UIControlStateNormal];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

// 我要开团
-(void)joinGroupBuying{
    NSDictionary * parameterDic = @{@"goodsFreeId":_fgModel.fgid};
    [NetRequest requestTokenURL:url_free_saveOrder parameter:parameterDic success:^(NSDictionary * nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"] ) {
            [_giveAlikeBtn setTitle:@"邀请好友支持" forState:UIControlStateNormal];
            _btnType = 3 ;
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

// 分享海报给好友
-(void)shareGoods{
    FreeShareVC *freeShareVC = [[FreeShareVC alloc]init];
    freeShareVC.dmodel = _fgModel ;
    PushTo(freeShareVC, YES);
}

//获奖名单
-(void)pushOrderPraiseList{
    OrderPraiseListVC *orderPraiseListVC = [[OrderPraiseListVC alloc]init];
    orderPraiseListVC.goodsFreeId = _fgModel.fgid ;
    [self.navigationController pushViewController:orderPraiseListVC animated:YES];
}

//我要下单、进入淘宝购买
-(void)arrowStatesAndIsOpen{
    MBShow(@"正在加载...")
    [NetRequest requestTokenURL:url_free_take_order parameter:@{@"goodsId":_fgModel.goodsId} success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
            NSString * buyUrl = REPLACENULL(nwdic[@"buyUrl"]) ;
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"taobao://uland.taobao.com"]]&&[[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tbopen://"]]) {
                if ([buyUrl hasPrefix:@"https://"]) {
                    buyUrl = [buyUrl substringFromIndex:6];
                    buyUrl = FORMATSTR(@"taobao:%@",buyUrl);
                }
                if ([buyUrl hasPrefix:@"http://"]) {
                    buyUrl = [buyUrl substringFromIndex:5];
                    buyUrl = FORMATSTR(@"taobao:%@",buyUrl);
                }
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:buyUrl]];
            }else{
                if ([buyUrl hasPrefix:@"taobao"]) {
                    buyUrl = [buyUrl substringFromIndex:7];
                    buyUrl = FORMATSTR(@"https:%@",buyUrl);
                }
                WebVC*webv=[[WebVC alloc]init];
                webv.urlWeb = buyUrl ;
                webv.nameTitle = @"领取优惠券";
                webv.hidesBottomBarWhenPushed = YES ;
                PushTo(webv, YES)
            }

        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

#pragma mark ==>> 福利流程中的获奖名单点击代理
-(void)pushToAwardsListVC{
    [self pushOrderPraiseList];
}




//顶部View
-(UIView*)topView{
    if (!_topView) {
        _topView = [[UIView alloc]init];
        _topView.frame = CGRectMake(0, 0, ScreenW, NAVCBAR_HEIGHT);
        [self.view addSubview:_topView];
        [self.view bringSubviewToFront:_topView];
        
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"dback"] forState:UIControlStateNormal];
        [_backBtn setImageEdgeInsets:UIEdgeInsetsMake(7, 7, 7, 7)];
        ADDTARGETBUTTON(_backBtn, clickBack)
        [_topView addSubview:_backBtn];
        [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(6); make.top.offset(NAVCBAR_HEIGHT-50);
            make.height.offset(44); make.width.offset(44);
        }];
        
        _titLab = [UILabel labText:@"商品详情" color:Black51 font:TITLEFONT];
        _titLab.frame=CGRectMake(ScreenW/2-100, _topView.frame.size.height-44, 200, 40);
        _titLab.textAlignment=NSTextAlignmentCenter;
        _titLab.alpha = 0 ;
        [_topView addSubview:_titLab];
        
    }
    return _topView;
}

-(void)clickBack{   PopTo(YES)  }

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated: animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id) self;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated: animated];
}


-(void)dealloc{
    [_detailCltView removeObserver:self forKeyPath:@"contentOffset"];
}

#pragma mark === >>> 置顶按钮
-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_bottomView.mas_top).offset(-10);
            make.right.offset(-10); make.width.offset(36*HWB);
            make.height.offset(36*HWB);
        }];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_detailCltView.contentOffset.y>=0) {
        if (_detailCltView.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_detailCltView.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _detailCltView.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
    }
}

-(void)clickBackTop{
    [_detailCltView setContentOffset:CGPointZero animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
