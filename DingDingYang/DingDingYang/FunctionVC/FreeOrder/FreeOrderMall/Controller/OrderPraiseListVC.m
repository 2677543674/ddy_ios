//
//  OrderPraiseListViewController.m
//  DingDingYang
//
//  Created by ddy on 18/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "OrderPraiseListVC.h"
#import "OrderPraiseListCell.h"

@interface OrderPraiseListVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSMutableArray * dataArray ;
@property(nonatomic,strong) UITableView * tableView ;
@property(nonatomic,assign) NSInteger pageCount ;

@end

@implementation OrderPraiseListVC


- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"获奖名单" ;
    
    _pageCount = 1 ;
    _dataArray = [[NSMutableArray alloc]init];
    
    
    _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = COLORGROUP ;
    _tableView.delegate = self ;  _tableView.dataSource = self ;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);    make.left.offset(0);
        make.right.offset(0);  make.bottom.offset(0);
    }];

    [_tableView registerClass:[OrderPraiseListCell class] forCellReuseIdentifier:@"praise_list_cell"] ;
    
    __weak typeof(self) weakSelf = self ;
    
    _tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageCount = 1 ;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf.tableView.mj_footer resetNoMoreData];
        [weakSelf downloadData];
    }];
    
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf downloadData];
    }];
    
    [_tableView.mj_header beginRefreshing];
    
}

-(void)downloadData{
    if (_goodsFreeId==nil||_goodsFreeId.length==0) {   SHOWMSG(@"商品ID为空!")  return ;  }

    NSDictionary * pdic = @{@"goodsFreeId":_goodsFreeId,@"st":FORMATSTR(@"%ld",(long)_pageCount)};
    
    [NetRequest requestTokenURL:url_free_praiseList  parameter:pdic success:^(NSDictionary *nwdic) {
        if ([REPLACENULL(nwdic[@"result"])  isEqualToString:@"OK"]) {
            NSArray * columnAry = NULLARY( nwdic[@"list"] ) ;
            [columnAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.dataArray addObject:obj];
            }];
            
            _pageCount++ ;
            [_tableView.mj_header endRefreshing];
            if (columnAry.count==0) {  [_tableView.mj_footer endRefreshingWithNoMoreData]; }
            else{   [_tableView.mj_footer endRefreshing];   }
            
        }else{
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        [_tableView reloadData] ;
    } failure:^(NSString *failure) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
         SHOWMSG(failure)
    }];
}


#pragma mark ===>>>  TableView代理

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50*HWB ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderPraiseListCell * cell = [tableView dequeueReusableCellWithIdentifier:@"praise_list_cell" forIndexPath:indexPath];
    NSDictionary *dic = _dataArray[indexPath.row];
    cell.nicknameLab.text = [REPLACENULL(dic[@"name"]) stringByRemovingPercentEncoding] ;
    cell.mobileLab.text = REPLACENULL(dic[@"mobile"]) ;
    NSURL * headurl =  [REPLACENULL(dic[@"headImg"]) hasPrefix:@"http"]?[NSURL URLWithString:dic[@"headImg"]]:[NSURL URLWithString:FORMATSTR(@"%@%@",dns_dynamic_loadimg, dic[@"headImg"])] ;
    [cell.headImgV sd_setImageWithURL:headurl placeholderImage:PlaceholderImg];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
