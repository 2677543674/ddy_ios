//
//  FreeOrderDetailViewController.h
//  DingDingYang
//
//  Created by ddy on 11/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@class FreeOrderModel ;

@interface FreeOrderDetailVC : DDYViewController

@property(nonatomic,assign) NSInteger pushType ; // 进入免单详情页方式 0：商品列表 1：推送消息
@property(nonatomic,strong) FreeOrderModel * fgModel ; // 免单列表model
@end
