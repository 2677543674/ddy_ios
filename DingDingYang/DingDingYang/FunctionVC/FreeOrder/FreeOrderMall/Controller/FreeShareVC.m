//
//  FreeShareViewController.m
//  DingDingYang
//
//  Created by ddy on 19/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "FreeShareVC.h"

@interface FreeShareVC ()

@property(nonatomic,strong) ShareButtonGoods * shareBtn ;  // 分享按钮
@property(nonatomic,strong) UIImageView * posterImgView ;  // 海报显示
@property(nonatomic,strong) UIView      * shareBgView ;  // 分享按钮显示的背景
@property(nonatomic,strong) UIImage     * posterImg ;  // 商品海报图片
@property(nonatomic,strong) UIButton    * backBtn ;  // 返回按钮
@end

@implementation FreeShareVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    
    self.title = @"分享" ;
    
    [self backBtn];
    [self downloadImgData];
    [self creatShareView];
}

-(UIButton*)backBtn{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"dback"] forState:UIControlStateNormal];
        [_backBtn setImageEdgeInsets:UIEdgeInsetsMake(7, 7, 7, 7)];
        ADDTARGETBUTTON(_backBtn, clickBack)
        [self.view addSubview:_backBtn];
        [self.view bringSubviewToFront:_backBtn];
        [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(6); make.top.offset(NAVCBAR_HEIGHT-44);
            make.height.offset(44); make.width.offset(44);
        }];
    }
    return _backBtn ;
}

-(UIImageView*)posterImgView{
    if (!_posterImgView) {
        _posterImgView = [[UIImageView alloc]init];
        _posterImgView.image = _posterImg ;
        _posterImgView.backgroundColor = COLORWHITE ;
        _posterImgView.contentMode = UIViewContentModeScaleAspectFit ;
        if (ScreenH-(60*HWB+60)>_posterImg.size.height/_posterImg.size.width*ScreenW) {
            _posterImgView.frame = CGRectMake(0, 0, ScreenW,_posterImg.size.height/_posterImg.size.width*ScreenW);
        }else{
            _posterImgView.frame = CGRectMake(0, 0, ScreenW,ScreenH-(60*HWB+60));
        }
        
        [self.view addSubview:_posterImgView];
        [self.view bringSubviewToFront:_backBtn];
        [self.view sendSubviewToBack:_posterImgView];
    }
   
    return _posterImgView ;
}

-(void)creatShareView{
    
    _shareBgView = [[UIView alloc]init];
    _shareBgView.backgroundColor = COLORGROUP ;
    [self.view addSubview:_shareBgView];
    [_shareBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);    make.right.offset(0);
        make.bottom.offset(0); make.height.offset(60*HWB+60);
    }];
    
    UIImageView * lines = [[UIImageView alloc]init];
    lines.backgroundColor = [UIColor lightGrayColor];
    lines.frame = CGRectMake(25*HWB,11,ScreenW-50*HWB ,1) ;
    [_shareBgView addSubview:lines];
    
    UILabel * textTWLab = [UILabel labText:@"分 享 到" color:Black102 font:13.0*HWB];
    textTWLab.backgroundColor = COLORGROUP ;
    textTWLab.textAlignment = NSTextAlignmentCenter ;
    textTWLab.center = lines.center;
    textTWLab.bounds = CGRectMake(0, 0, 120, 21);
    [_shareBgView addSubview:textTWLab];
    
    
    NSArray * img = @[@"share_2",@"share_2_1",@"share_1",@"share_1_1",@"share_3"];
    NSArray * tit = @[@"微信",@"朋友圈",@"QQ",@"空间",@"微博",@"更多"];
    if (IsNeedSinaWeb==2) {
        img = @[@"share_2",@"share_2_1",@"share_1",@"share_1_1"];
        tit = @[@"微信",@"朋友圈",@"QQ",@"空间"];
    }
    for (NSInteger i = 0 ; i < img.count ; i++ ) {
        _shareBtn = [[ShareButtonGoods alloc]initWithImgName:img[i] titleName:tit[i]];
        _shareBtn.frame  =CGRectMake(ScreenW/img.count*i, 35 , ScreenW/img.count, 62*HWB);
        _shareBtn.tag = i + 10 ;
        [_shareBtn addTarget:self action:@selector(shareType:) forControlEvents:UIControlEventTouchUpInside];
        [_shareBgView addSubview:_shareBtn];
    }
}



-(void)downloadImgData{
    NSDictionary * parameterDic = @{@"goodsFreeId":_dmodel.fgid};
    
    MBShow(@"正在生成...")
    [NetRequest requestTokenURL:url_free_poster parameter:parameterDic success:^(NSDictionary * nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"] ) {
            SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
            [downloader downloadImageWithURL:[NSURL URLWithString:FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(nwdic[@"poster"]]))  options:SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *  targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                MBHideHUD
                if(image!=nil){
                    _posterImg = image ;
                    [self posterImgView];
                }
            }];
             
        }else{
            MBHideHUD
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
    
}

-(void)shareType:(ShareButtonGoods*)btn{
    switch (btn.tag) {
        case  10:{ // 微信好友
            if (CANOPENWechat) {
                [self shareToPlatformType:UMSocialPlatformType_WechatSession];
            }else{
                SHOWMSG(@"亲，还没有安装微信哦!")
            }
        }
            break;
            
        case  11:{ // 微信朋友圈
            if (CANOPENWechat) {
                [self shareToPlatformType:UMSocialPlatformType_WechatTimeLine];
            }else{
                SHOWMSG(@"亲，还没有安装微信哦!")
            }
        }
            break;
            
        case  12:{ // qq好友
            if (CANOPENQQ) {
                [self shareToPlatformType:UMSocialPlatformType_QQ];
            }else{
                SHOWMSG(@"亲，还没有安装QQ哦!")
            }
        }
            break;
            
        case 13:{ // 空间
            [self shareToPlatformType:UMSocialPlatformType_Qzone];
        }
            break;
            
        case 14:{ // 微博
            if (IsNeedSinaWeb==1) {
                if ([[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Sina]) {
                    [self shareToPlatformType:UMSocialPlatformType_Sina];
                }else{
                    SHOWMSG(@"亲，还没有安装微博哦!")
                }
            }
        }
            break;
            
        default: break;
    }

}

#pragma mark === >>> 调用分享友盟
//分享方法
- (void)shareToPlatformType:(UMSocialPlatformType)platformType{
    
    if(_posterImg==nil) { SHOWMSG(@"海报未下载完成或下载失败,请重试!")  return ; }
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
   
    UMShareImageObject*shObjc=[[UMShareImageObject alloc]init];
    [shObjc setShareImage:_posterImg];
    messageObject.shareObject=shObjc;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        LOG(@"%@，\n\n%@",data,error)
        if (error) {
            switch (error.code) {
                case 2001:{ SHOWMSG(@"不支持的分享格式或软件版本过低!") }break;
                case 2005:{SHOWMSG(@"生成的分享内容为空,请稍后再试!")} break;
                case 2006:{SHOWMSG(@"未能完成操作!")} break;
                case 2008:{
                    if (platformType==1||platformType==2) { SHOWMSG(@"亲，你还没有安装微信哦!") }
                    if (platformType==4||platformType==5) { SHOWMSG(@"亲，你还没有安装QQ哦!") }
                } break;
                case 2009:{ SHOWMSG(@"分享已取消!") }break;
                case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试") }break;
                case 2011:{ SHOWMSG(@"第三方错误") }break;
                default: { SHOWMSG(@"分享出错啦!") } break;
            }
        }else{
            SHOWMSG(@"分享成功!")
        }
    }];
}

-(void)clickBack{ PopTo(YES)}
        
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
