//
//  FreeOrderViewController.m
//  DingDingYang
//
//  Created by ddy on 10/08/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "FreeOrderVC.h"
//#import "GoodsModel.h"
////#import "GoodsModel.h"
#import "FreeOrderModel.h"
#import "FreeOrderCell.h"
#import "FreeOrderDetailVC.h"

@interface FreeOrderVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView * tableView ;
@property(nonatomic,assign) NSInteger  sectionNum ; //应该返回的分区个数
@property(nonatomic,strong) NSMutableArray * dataArray; // 正在抢购
@property(nonatomic,strong) NSMutableArray * waitingBeginDataArray ; //明日预告
@property(nonatomic,assign) int pageCount;
@property(nonatomic,strong) NSTimer * timer;
@property(nonatomic,strong) UIImageView * nowBuyImgV , * nextBuyImgV ;


@end

@implementation FreeOrderVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"福利专区" ;
    
    _dataArray = [[NSMutableArray alloc]init];
    _waitingBeginDataArray = [[NSMutableArray alloc]init];
    _pageCount = 1 ;
    _sectionNum = 2 ;
    
    _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = COLORGROUP ;
    _tableView.delegate=self; _tableView.dataSource=self;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);    make.left.offset(0);
        make.right.offset(0);  make.bottom.offset(0);
    }];
    
    [_tableView registerClass:[FreeOrderCell class] forCellReuseIdentifier:@"free_order_cell"];
    
    __weak typeof(self) weakSelf=self;
    _tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf.tableView.mj_footer resetNoMoreData] ;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf.waitingBeginDataArray removeAllObjects];
        weakSelf.pageCount = 1 ;
        [weakSelf downloadList] ;
    }];
    
    _tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf downloadList];
    }];
   
    
    UIImageView * bannerImgV = [[UIImageView alloc]init];
    bannerImgV.image = [UIImage imageNamed:@"freeorder_banner"];
    bannerImgV.frame = CGRectMake(0, 0, ScreenW, ScreenW/2.88);
    [_tableView addSubview:bannerImgV];
    

    [self downloadList];
    
    
    
    
    
    
}

-(void)downloadList{

    [NetRequest requestTokenURL:url_free_List parameter:@{@"st":FORMATSTR(@"%d",_pageCount),@"versionNo":@"1.1"} success:^(NSDictionary *nwdic) {
        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
            NSArray * array = NULLARY(nwdic[@"list"]) ;
            [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                FreeOrderModel * model = [[FreeOrderModel alloc]initWithDic:obj];
                if (model.ifStart==0||model.ifStart==1) { // 已开团
                    [_dataArray addObject:model];
                }else if (model.ifStart==2){ //未开始
                    [_waitingBeginDataArray addObject:model];
                    _sectionNum = 3 ;
//                    if (!_timer) { //[model.restTime integerValue]/1000
//                        _timer=[NSTimer timerWithTimeInterval:15 target:self selector:@selector(refreshTableView:) userInfo:nil repeats:NO];
//                        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
//                    }
                }
            }];
            
            [self.tableView.mj_header endRefreshing];
            if (array.count==0) {  [self.tableView.mj_footer endRefreshingWithNoMoreData]; }
            else{  _pageCount++ ; [self.tableView.mj_footer endRefreshing];   }
            
        }else{
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        [self.tableView reloadData];

    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self downloadList];
        }];
    }];
    
}

-(void)refreshTableView:(NSTimer *)time{
    
    [self.dataArray removeAllObjects];
    [self.waitingBeginDataArray removeAllObjects];
    self.pageCount=1;
    [_timer invalidate];
    [self downloadList];
    
}

#pragma mark  UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _sectionNum ;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {  return nil ; }
    if (section==1) { [self nowBuyImgV];   return _nowBuyImgV ; }
    [self nextBuyImgV];  return _nextBuyImgV ;
}

-(UIImageView*)nowBuyImgV{
    if (!_nowBuyImgV) {
        _nowBuyImgV = [[UIImageView alloc]init];
        _nowBuyImgV.image = [UIImage imageNamed:@"free_order_nowBuy"];
        _nowBuyImgV.frame = CGRectMake(0, 0, ScreenW, ScreenW/8.9);
    }
    return _nowBuyImgV ;
}
-(UIImageView*)nextBuyImgV{
    if (!_nextBuyImgV) {
        _nextBuyImgV = [[UIImageView alloc]init];
        _nextBuyImgV.backgroundColor = COLORGROUP ;
        _nextBuyImgV.frame = CGRectMake(0, 0, ScreenW, ScreenW/8.9);

        UIImageView * img = [[UIImageView alloc]init];
        img.image = [UIImage imageNamed:@"free_order_nextBuy"];
        img.frame = CGRectMake(0, 15, ScreenW, ScreenW/8.9);
        [_nextBuyImgV addSubview:img];
    }
    return _nextBuyImgV ;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) { return 1 ; }
    if (section==1) { return ScreenW/8.9 ; }
    return  ScreenW/8.9 + 15 ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1 ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) { return ScreenW/2.88 ; }
    return ScreenW/2+60 + 50*HWB ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section==0) {  return 1 ; }
    if (section==1) {  return _dataArray.count; }
    return _waitingBeginDataArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        static NSString * cellResid = @"image_cell" ;
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellResid];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellResid];
            UIImageView * bannerImgV = [[UIImageView alloc]init];
            bannerImgV.image = [UIImage imageNamed:@"freeorder_banner"];
            bannerImgV.frame = CGRectMake(0, 0, ScreenW, ScreenW/2.88);
            [cell.contentView addSubview:bannerImgV];
        }
        return cell ;
    }
    
    FreeOrderCell * goodsCell = [tableView dequeueReusableCellWithIdentifier:@"free_order_cell" forIndexPath:indexPath];
    FreeOrderModel * model ;
    if (indexPath.section==1) { model = _dataArray[indexPath.row] ; }
    if (indexPath.section==2) { model = _waitingBeginDataArray[indexPath.row] ; }
    goodsCell.freeModel = model ;
    return goodsCell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    if (indexPath.section!=0) {
        FreeOrderModel * model ;
        if (indexPath.section==1) {  model=_dataArray[indexPath.row]; }
        if (indexPath.section==2) { model=_waitingBeginDataArray[indexPath.row];  }
        FreeOrderDetailVC * detailVC = [[FreeOrderDetailVC alloc]init] ;
        detailVC.fgModel = model ;
        detailVC.hidesBottomBarWhenPushed = YES ;
        PushTo(detailVC, YES);
    }

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
