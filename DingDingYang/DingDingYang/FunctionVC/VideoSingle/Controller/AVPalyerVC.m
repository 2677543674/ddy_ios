//
//  AVPalyerVC.m
//  Video
//
//  Created by ddy on 2017/6/17.
//  Copyright © 2017年 ddy.All rights reserved.
//



#import "AVPalyerVC.h"

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface AVPalyerVC ()


@property (nonatomic,strong) AVPlayerItem * playerItem ;
@property (nonatomic,strong) AVPlayer * player;//播放器对象
@property (nonatomic,strong) AVPlayerLayer * playerLayer; //播放层
@property (nonatomic,strong) UIProgressView * progress;//播放进度
@property (nonatomic,strong) UIButton * startBtnCenter , * lbBtn;

@property (nonatomic,strong) UILabel * timeLab ;
@property (nonatomic,copy)   NSString * alltime ;
@property (nonatomic,strong) UIView * bottomBgView ;

@property (nonatomic,strong) UIButton * removeBtn ;
@property (nonatomic,assign) NSInteger statePlayer ;
@property (nonatomic,strong) UIView * topView ;
@property (nonatomic,assign) BOOL  isHiden ;

@end

@implementation AVPalyerVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
    if (_player!=nil) {
        [_player play];
        [self clickStop];
        [self lbClickStop];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden=NO;
    if (_player!=nil) {
        [_player pause];
        [self clickStop];
        [self lbClickStop];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    
    _isHiden = NO ;

    [self player];

    [self addProgressObserver]; //修改进度条通知

    [self addObserverToPlayerItem:_playerItem];

    [self startBtnCenter];


    [self addTopTabbarView];
    [self bottomBgView];

    _statePlayer = 1 ;
    

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(stopVideo) name:ResignActive object:nil];
}

-(void)stopVideo{
    if (_player.rate==1) {
        [_player pause];
        [self clickStop];
        [self lbClickStop];
    }
}

-(void)removeSelf{
    [self.player pause];
    [self.navigationController popViewControllerAnimated:NO];
}

//创建播放器
-(AVPlayer *)player{
    if (!_player) {

        NSURL *url=[NSURL URLWithString:[_videoModel.video stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        _playerItem=[AVPlayerItem playerItemWithURL:url];
        
        _player=[AVPlayer playerWithPlayerItem:_playerItem];
        
        //创建播放器层
        _playerLayer=[AVPlayerLayer playerLayerWithPlayer:self.player];
    
//        float videoWdt = ScreenW*9/16 ;
//        _playerLayer.frame=CGRectMake(0, ScreenH/2-videoWdt/2, ScreenW, videoWdt);
         _playerLayer.frame=CGRectMake(0, 0, ScreenW, ScreenH);
        _playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        [self.view.layer addSublayer:_playerLayer];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(isHidenOrShow)];
        
        [self.view addGestureRecognizer:tap];


        //给AVPlayerItem添加播放完成通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.player.currentItem];

    }
    return _player;
}


-(UIView*)bottomBgView{
    if (!_bottomBgView) {
//        float videoWdt = ScreenW*9/16 ;
        
        _bottomBgView = [[UIView alloc]init];
        _bottomBgView.backgroundColor = ColorRGBA(0, 0, 0, 0.8);
        _bottomBgView.frame = CGRectMake(0, ScreenH-40, ScreenW, 40);
        [self.view addSubview:_bottomBgView];
        
        //左下角暂停开始安扭
        _lbBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_lbBtn setImage:[UIImage imageNamed:@"video_stop"] forState:(UIControlStateNormal)];
        _lbBtn.frame = CGRectMake(10, 0 , 40, 40);
        [_lbBtn addTarget:self action:@selector(lbClickStop) forControlEvents:(UIControlEventTouchUpInside)];
        [_bottomBgView addSubview:_lbBtn];
        
        //进度条
        _progress = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progress.trackTintColor = COLORWHITE ;//进度条颜色(底色)
        _progress.progressTintColor = LOGOCOLOR ;//移动的进度条底色
        _progress.progress = 0 ;
        [_bottomBgView addSubview:_progress];
        [_progress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(55);  make.right.offset(-120);
            make.centerY.equalTo(_bottomBgView.mas_centerY);
        }];
        
        
        _timeLab = [[UILabel alloc]init];
        _timeLab.textColor = [UIColor whiteColor];
        _timeLab.text = @"00:00/00:00" ;
        [self.view addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_progress.mas_right).offset(10);
            make.right.offset(-10); make.centerY.equalTo(_bottomBgView.mas_centerY);
        }];

    }
    return _bottomBgView ;
}

-(UIButton*)startBtnCenter{
    if (!_startBtnCenter) {
        _startBtnCenter = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _startBtnCenter.hidden = YES ;
        [_startBtnCenter setImage:[UIImage imageNamed:@"video_stop"] forState:(UIControlStateNormal)];
        _startBtnCenter.frame = CGRectMake(ScreenW/2-25, ScreenH/2-25, 50, 50);
        [_startBtnCenter addTarget:self action:@selector(clickStop) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:_startBtnCenter];
    }
    return _startBtnCenter ;
}

//视频中间的开始按钮
-(void)clickStop{
    
    
    NSLog(@"---> %f",_player.rate);
    
    if(self.player.rate==0){ //说明时暂停
        [_startBtnCenter setImage:[UIImage imageNamed:@"video_stop"] forState:(UIControlStateNormal)];
        _startBtnCenter.hidden = YES ;
        [_lbBtn setImage:[UIImage imageNamed:@"video_stop"] forState:(UIControlStateNormal)];
        if (_statePlayer==2) {
            [_player seekToTime:CMTimeMake(0, 1)];
            [_progress setProgress:0 animated:YES];
        }
        [self.player play];
        _statePlayer = 1 ;
    } else if(self.player.rate==1){//正在播放
        [self.player pause];
        _statePlayer = 0 ;
        [_startBtnCenter setImage:[UIImage imageNamed:@"video_start"] forState:(UIControlStateNormal)];
        [_lbBtn setImage:[UIImage imageNamed:@"video_start"] forState:(UIControlStateNormal)];
    }

}

-(void)lbClickStop{
    if(self.player.rate==0){ //说明时暂停
        _startBtnCenter.hidden = YES ;
        [_startBtnCenter setImage:[UIImage imageNamed:@"video_stop"] forState:(UIControlStateNormal)];
        [_lbBtn setImage:[UIImage imageNamed:@"video_stop"] forState:(UIControlStateNormal)];
        [self.player play];
    } else if(self.player.rate==1){//正在播放
        [self.player pause];
        _startBtnCenter.hidden = NO ;
        [_lbBtn setImage:[UIImage imageNamed:@"video_start"] forState:(UIControlStateNormal)] ;
        [_startBtnCenter setImage:[UIImage imageNamed:@"video_start"] forState:(UIControlStateNormal)];
    }

}


/**
 *  给播放器添加进度更新
 */
-(void)addProgressObserver{
    AVPlayerItem *playerItem=self.player.currentItem;
    __weak AVPalyerVC * selfV = self ;
//    CMTimeMake(a,b)    a当前第几帧, b每秒钟多少帧.当前播放时间a/b
//    CMTimeMakeWithSeconds(a,b)    a当前时间,b每秒钟多少帧.
    //这里设置每秒执行一次
    [_player addPeriodicTimeObserverForInterval:CMTimeMake(1.0,1.0 ) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        
        float current = CMTimeGetSeconds(time);
        float total = CMTimeGetSeconds([playerItem duration]);

        if (current) {
            NSInteger minute = (NSInteger) current/60 ;
            NSInteger second = (NSInteger) current - minute*60 ;
            selfV.timeLab.text = [NSString stringWithFormat:@"%.2ld:%.2ld/%@",(long)minute,(long)second,selfV.alltime];
            
            [selfV.progress setProgress:(current/total) animated:YES];
        }
    }];
}

/**
 *  给AVPlayerItem添加监控
 *
 *  @param playerItem AVPlayerItem对象
 */
-(void)addObserverToPlayerItem:(AVPlayerItem *)playerItem{
    //监控状态属性，注意AVPlayer也有一个status属性，通过监控它的status也可以获得播放状态
    [playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    //监控网络加载情况属性
    [playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
}

/**
 *  通过KVO监控播放器状态
 *
 *  @param keyPath 监控属性
 *  @param object  监视器
 *  @param change  状态改变
 *  @param context 上下文
 */
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    AVPlayerItem *playerItem=object;
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
        if(status==AVPlayerStatusReadyToPlay){
            
            NSInteger time = CMTimeGetSeconds(playerItem.duration) ;
            NSInteger minute = (NSInteger) time/60 ;
            NSInteger second = (NSInteger) time - minute*60 ;
            _alltime = [NSString stringWithFormat:@"%.2ld:%.2ld",(long)minute,(long)second];
            _timeLab.text = [NSString stringWithFormat:@"00:00/%@",_alltime];
            NSLog(@"正在播放...，视频总长度:%.2f",CMTimeGetSeconds(playerItem.duration));
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (_isHiden==NO) {
                     [self animationTopAnbBottomView:YES];
                }
            });
        }
    }else if([keyPath isEqualToString:@"loadedTimeRanges"]){
        
        
        NSArray *array=playerItem.loadedTimeRanges;
        NSLog(@"--> : 数组%@",array) ;
        CMTimeRange timeRange = [array.firstObject CMTimeRangeValue];//本次缓冲时间范围
        float startSeconds = CMTimeGetSeconds(timeRange.start);
        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
        NSTimeInterval totalBuffer = startSeconds + durationSeconds;//缓冲总长度
        NSLog(@"共缓冲：%.2f",totalBuffer);
    }
}

/**
 *  播放完成通知
 *
 *  @param notification 通知对象
 */
-(void)playbackFinished:(NSNotification *)notification{

    _startBtnCenter.hidden = NO ;
    [self animationTopAnbBottomView:NO];
    [_startBtnCenter setImage:[UIImage imageNamed:@"video_again"] forState:(UIControlStateNormal)];
    [_lbBtn setImage:[UIImage imageNamed:@"video_start"] forState:(UIControlStateNormal)];

    _statePlayer = 2;
    
}
//创建顶部view
-(void)addTopTabbarView{
    
    _topView=[[UIView alloc]init];
    _topView.backgroundColor=ColorRGBA(0, 0, 0, 0.8);
    _topView.frame=CGRectMake(0,0, ScreenW,NAVCBAR_HEIGHT);
    [self.view addSubview:_topView];
    
    UILabel*titleLab=[UILabel labText:_videoModel.goodsName color:COLORWHITE font:16];
    titleLab.frame=CGRectMake(60, 20, ScreenW-120, 40);
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.numberOfLines = 2 ;
    [_topView addSubview:titleLab];
    
    UIButton*backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrowl"] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    backBtn.frame=CGRectMake(0, 20, 44, 44);
    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:backBtn];
    
    _removeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [_removeBtn setImage:[UIImage imageNamed:@"error"] forState:(UIControlStateNormal)];
    _removeBtn.frame = CGRectMake(ScreenW-50, 20, 40, 40);
    [_removeBtn addTarget:self action:@selector(removeSelf) forControlEvents:(UIControlEventTouchUpInside)];
    [_topView addSubview:_removeBtn];
}

-(void)isHidenOrShow{
    NSLog(@"点击了点击了。。。。。。。");
    if (_isHiden==NO) {
        [self animationTopAnbBottomView:YES];
    }else{
        [self animationTopAnbBottomView:NO];
    }
}
-(void)animationTopAnbBottomView:(BOOL)hide{
    _isHiden = hide ;
    if (hide) {
        if (_topView!=nil) {
            [UIView animateWithDuration:0.26 animations:^{
                _topView.frame = CGRectMake(0, -70, ScreenW, NAVCBAR_HEIGHT);
                _bottomBgView.frame = CGRectMake(0, ScreenH+10, ScreenW, 40);
            }];
        }
    }else{
        if (_topView!=nil) {
            [UIView animateWithDuration:0.26 animations:^{
                _topView.frame = CGRectMake(0, 0, ScreenW, NAVCBAR_HEIGHT);
                _bottomBgView.frame = CGRectMake(0, ScreenH-40, ScreenW, 40);
            } completion:^(BOOL finished) {
                if (_statePlayer!=2) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        if (_isHiden==NO) {
                            [self animationTopAnbBottomView:YES];
                        }
                    });
                }
            }];

        }
    }
}



-(void)clickBack{    [self.player pause];  PopTo(YES)  }


#pragma mark --> 移除所有通知
-(void)dealloc{
    [self removeObserverFromPlayerItem:self.player.currentItem];
    [[NSNotificationCenter defaultCenter] removeObserver:ResignActive];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)removeObserverFromPlayerItem:(AVPlayerItem *)playerItem{
    [playerItem removeObserver:self forKeyPath:@"status"];
    [playerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
