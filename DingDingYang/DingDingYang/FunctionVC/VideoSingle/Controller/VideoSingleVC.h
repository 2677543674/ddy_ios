//
//  VideoSingleVC.h
//  DingDingYang
//
//  Created by ddy on 2017/6/17.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoSingleVC : DDYViewController

//是否是一级界面 YES: 是  NO或nil:不是
@property(nonatomic,assign) BOOL isOnePage ;
@property(nonatomic,strong) MClassModel * mcsModel ;

@end
