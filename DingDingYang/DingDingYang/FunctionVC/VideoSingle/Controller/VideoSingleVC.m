//
//  VideoSingleVC.m
//  DingDingYang
//
//  Created by ddy on 2017/6/17.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "VideoSingleVC.h"
#import "VideoGoodsCell.h"
#import "GoodsListCltCell1.h"
#import "OrmdCell.h"

@interface VideoSingleVC () <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * collectionCV ;
@property(nonatomic,strong) NSMutableArray * dataAry ;
@property(nonatomic,strong) NSMutableDictionary * parameter ;
@property(nonatomic,assign) NSInteger pageNum ;
@property(nonatomic,strong) UIButton * topBtn ;
@property(nonatomic,assign) NSInteger goodsListStyle ;
//@property(nonatomic,strong) ScreeningView * screenView ;
@end

@implementation VideoSingleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    
    _goodsListStyle = GoodsListStyle ;
    
    self.title=_mcsModel.title.length>0?_mcsModel.title:@"短视频";
    
    _pageNum = 2 ;
    _dataAry = [NSMutableArray array];
    [self parameter];
    
    [self collectionCV];
    [self topBtn];

}


-(NSMutableDictionary*)parameter{
    if (!_parameter) {
        _parameter = [NSMutableDictionary dictionary];
        _parameter[@"st"] = @"1" ;
        _parameter[@"ifVideo"] = @"1" ;
    }
    return _parameter ;
}

#pragma mark --> 请求视频商品
-(void)requestSKData{
    [_collectionCV.mj_footer resetNoMoreData];
    _pageNum = 2 ;    _parameter[@"st"] = @"1" ;
    [NetRequest requestTokenURL:url_goods_list2 parameter:_parameter success:^(NSDictionary *nwdic) {
        
        if (_dataAry.count>0) {
            [_dataAry removeAllObjects];
            [_collectionCV reloadData];
        }else{
            if([nwdic[@"result"] isEqualToString:@"OK"]){
                NSArray*lAry= NULLARY( nwdic[@"list"] ) ;
                if (lAry.count>0) {
                    GOODSNUMRESULTS(REPLACENULL(nwdic[@"totalCount"]));
                }
            }
        }
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray * ary = NULLARY( nwdic[@"list"] ) ;
            if (ary.count>0) {
                [ary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
            }else{
//                SHOWMSG(@"敬请期待！")
            }
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        [_collectionCV.mj_header endRefreshing];
        [_collectionCV reloadData];
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestSKData];
        }];
        [_collectionCV.mj_header endRefreshing];
    }];
}
-(void)loadMoreSKData{
    _parameter [@"st"] =FORMATSTR(@"%ld",(long)_pageNum);
    [NetRequest requestTokenURL:url_goods_list2 parameter:_parameter success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray * ary = nwdic[@"list"];
            if (ary.count>0) {
                _pageNum ++ ;
                [_collectionCV.mj_footer endRefreshing];
                [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
                [_collectionCV reloadData];
            }else{
                [_collectionCV.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
            [_collectionCV.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [_collectionCV.mj_footer endRefreshing];
    }];
}

-(UICollectionView*)collectionCV{
    if (!_collectionCV) {
        //表格布局加载图片
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置单元格的大小
        if (PID==10102){ // 叮当叮当
            switch (_goodsListStyle) {
                case 2:{ layout.itemSize = CGSizeMake(ScreenW/2-2,ScreenW/2+70*HWB); } break;
                case 4:{ layout.itemSize = CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
                case 5:{ layout.itemSize = CGSizeMake(ScreenW/2-2,ScreenW/2+(role_state==0?70*HWB:90*HWB)); } break;
                case 6:{ layout.itemSize = CGSizeMake(ScreenW, 116*HWB); } break;
                case 7:{ layout.itemSize = CGSizeMake(ScreenW, 104*HWB); } break;
                case 8:{ layout.itemSize = CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
                default:{ layout.itemSize = CGSizeMake(ScreenW, FloatKeepInt(100*HWB)); } break;
            }
        }else{
            layout.itemSize = CGSizeMake(ScreenW/2-2,ScreenW/2+50 );
        }
        layout.minimumInteritemSpacing = 4;
        layout.minimumLineSpacing = 5;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//        layout.sectionInset = UIEdgeInsetsMake(4, 4, 4, 4);
        

        _collectionCV = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionCV.delegate = self ;
        _collectionCV.dataSource = self ;
        _collectionCV.backgroundColor = COLORGROUP ;
        [self.view addSubview:_collectionCV];
        [_collectionCV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [_collectionCV registerClass:[VideoGoodsCell class] forCellWithReuseIdentifier:@"VideoGoodsCell"];
        [_collectionCV registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"goods_listclt_cell1"];
        [_collectionCV registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"goods_listclt_cell2"];
        [_collectionCV registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"goods_listclt_cell3"];
        [_collectionCV registerClass:[GoodsListCltCell4   class] forCellWithReuseIdentifier:@"goods_listclt_cell4"];
        [_collectionCV registerClass:[GoodsListCltCell5   class] forCellWithReuseIdentifier:@"goods_listclt_cell5"];
        [_collectionCV registerClass:[GoodsListCltCell6   class] forCellWithReuseIdentifier:@"goods_listclt_cell6"];
        [_collectionCV registerClass:[GoodsListCltCell7   class] forCellWithReuseIdentifier:@"goods_listclt_cell7"];
        [_collectionCV registerClass:[GoodsListCltCell10   class] forCellWithReuseIdentifier:@"goods_listclt_cell10"];
        [_collectionCV registerClass:[OrmdCell class] forCellWithReuseIdentifier:@"ormdcell"];

        __weak VideoSingleVC * SelfView = self;
        _collectionCV.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(requestSKData)];

        _collectionCV.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreSKData];
        }];
        
        [_collectionCV.mj_header beginRefreshing];
    }
    return _collectionCV ;
}


#pragma mark --> UICollectionView代理
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (PID==10102) { // 叮当叮当
        // 部分序号命名和后台不一致，并且有的是某些app单独使用的
        switch (_goodsListStyle) {
            case 2:{
                GoodsListCltCell3 * goodsListClt3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell3" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt3.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt3;
            } break;
                
            case 3:{
                GoodsListCltCell2 * goodsListClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell2" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt2.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt2;
            } break;
                
            case 4:{
                GoodsListCltCell4 * goodsListClt4 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell4" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt4.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt4;
            } break;
                
            case 5:{
                GoodsListCltCell5 * goodsListClt5 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell5" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt5.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt5;
            } break;
                
            case 6:{
                GoodsListCltCell6 * goodsListClt6 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell6" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt6.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt6 ;
            } break;
                
            case 7:{
                GoodsListCltCell7 * goodsListClt7 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell7" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt7.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt7 ;
            } break;
                
            case 8:{
                GoodsListCltCell10 * goodsListClt10 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell10" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt10.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt10 ;
            } break;
                
            default:{
                GoodsListCltCell1 * goodsListClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell1" forIndexPath:indexPath];
                if (_dataAry.count>indexPath.item) { goodsListClt1.goodsModel = _dataAry[indexPath.item]; } else { [_collectionCV reloadData]; }
                return goodsListClt1;
            } break;
        }
    }
    
    OrmdCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ormdcell" forIndexPath:indexPath];
    cell.ormdModel = _dataAry [indexPath.row];
    return cell ;
  
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GoodsDetailsVC*detailVc = [[GoodsDetailsVC alloc]init] ;
    detailVc.hidesBottomBarWhenPushed = YES ;
    detailVc.dmodel = _dataAry[indexPath.row] ;
    PushTo(detailVc, YES)
}



-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);   make.bottom.offset(-10);
            make.height.offset(36*HWB); make.width.offset(36*HWB);
        }];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_collectionCV.contentOffset.y>=0) {
        if (_collectionCV.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_collectionCV.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _collectionCV.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
    }
}
-(void)clickBackTop{
    [_collectionCV setContentOffset:CGPointZero animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//     [self.navigationController setNavigationBarHidden:YES animated: YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UserInfoChange object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LoginSuccess object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
