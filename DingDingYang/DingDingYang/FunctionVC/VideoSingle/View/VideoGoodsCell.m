//
//  VideoGoodsCell.m
//  DingDingYang
//
//  Created by ddy on 2017/6/17.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define SKH (ScreenW-12)/2

#import "VideoGoodsCell.h"
#import "AVPalyerVC.h"

@implementation VideoGoodsCell


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.goodsPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_goodsModel.smallPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    _goodsTitle.text = _goodsModel.goodsName ;
    
    // 券后价
    NSRange rg =  {1,ROUNDED(_goodsModel.endPrice, 0).length} ;
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_goodsModel.endPrice)];
    [dlStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16*HWB]} range:rg];
    _endPriceLab.attributedText = dlStr ;
    
    // 原价(天猫价)
    NSRange titleRange = {0,_goodsModel.price.length+1};
    NSMutableAttributedString * mtitle = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_goodsModel.price)];
    [mtitle addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    _catPLab.attributedText = mtitle ;
    
    _salesLab.text = FORMATSTR(@"已购%@",_goodsModel.sales) ;
    
    [_couponsBtn setTitle:FORMATSTR(@"%@元",_goodsModel.couponMoney)  forState:(UIControlStateNormal)];
    
    
    if (role_state!=0) {  // 代理显示分享赚
        _shareBtn.hidden = NO;
        NSRange sharerg1 = {5,_goodsModel.commission.length};
        NSMutableAttributedString * dlStr21 = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"分享赚:￥%@",_goodsModel.commission)];
        [dlStr21 addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15*HWB]} range:sharerg1];
        [_shareBtn setAttributedTitle:dlStr21 forState:(UIControlStateNormal)];
    }else{  // 消费者隐藏
        _shareBtn.hidden = YES ;
    }
    
    if ([_goodsModel.couponMoney floatValue]>0) { // 有券
        _couponsBtn.hidden = NO ;    _catPLab.hidden = NO ;
    }else{   //无券(可能优惠券失效，或无券)
        _couponsBtn.hidden = YES ;   _catPLab.hidden = YES ;
        
        // 券后价显示成原价
        NSRange rg =  {1,ROUNDED(_goodsModel.price, 0).length} ;
        NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_goodsModel.price)];
        [dlStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16*HWB]} range:rg];
        _endPriceLab.attributedText = dlStr ;
    }
    
    
    if (_goodsModel.video.length>0&&[_goodsModel.video hasPrefix:@"http"]) {
        _playVideoBtn.hidden = NO ;
    }else{
        _playVideoBtn.hidden = YES ;
    }
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
        [self playVideoBtn];
        [self goodsTitle];
        
        [self endPriceLab];
        [self catPLab];
        [self salesLab];
        
        [self couponsBtn];
        [self shareBtn];
        
    }
    return self;
}

//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.frame = CGRectMake(0, 0, ScreenW/2-2, ScreenW/2-2);
        [self.contentView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}
-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        [_playVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_goodsImgV.mas_centerX);
            make.centerY.equalTo(_goodsImgV.mas_centerY);
            make.height.offset(40*HWB); make.width.offset(40*HWB);
        }];
        if (PID!=10088) {  // 种草君不要视频播放
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}
//商品标题
-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _goodsTitle.textAlignment =NSTextAlignmentCenter ;
        _goodsTitle.numberOfLines = 2 ;
        _goodsTitle.frame = CGRectMake(3,ScreenW/2+1 ,ScreenW/2-8, 36*HWB );
        [self.contentView addSubview:_goodsTitle];
    }
    return _goodsTitle ;
}


//券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"00.00元" color:LOGOCOLOR font:11*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsTitle.mas_bottom).offset(6);
            make.left.offset(6) ;
        }];
        if (THEMECOLOR==2) {
            _endPriceLab.textColor=[UIColor colorWithRed:240.f/255.f green:15.f/255.f blue:39.f/255.f alpha:1];
        }
    }
    return _endPriceLab;
}

// 天猫价
-(UILabel*)catPLab{
    if (!_catPLab) {
        _catPLab = [UILabel labText:@"￥0" color:Black102 font:11*HWB];
        [self.contentView addSubview:_catPLab];
        [_catPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(5);
            make.bottom.equalTo(_endPriceLab.mas_bottom).offset(-2);
        }];
    }
    return _catPLab ;
    
}

////暂时无券
//-(UILabel*)noCouponsLab{
//    if (!_noCouponsLab) {
//        _noCouponsLab = [UILabel labText:@"暂时无券哦" color:Black102 font:11*HWB];
//        [self.contentView addSubview:_noCouponsLab];
//        [_noCouponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.offset(5);  make.top.offset(5);
//        }];
//    }
//    return _noCouponsLab;
//}

//销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"月销00" color:Black102 font:11*HWB];
        _salesLab.textAlignment = NSTextAlignmentRight;
        _salesLab.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5); make.centerY.equalTo(_endPriceLab.mas_centerY);
        }];
    }
    return _salesLab ;
}

//优惠券
-(UIButton*)couponsBtn{
    if (!_couponsBtn) {
        _couponsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage*oldImg=[UIImage imageNamed:@"goods_list4_quan"];
        
        //        UIImage*newImg=[oldImg stretchableImageWithLeftCapWidth:115*HWB topCapHeight:0];
        [_couponsBtn setBackgroundImage:oldImg forState:UIControlStateNormal];
        [_couponsBtn setTitle:@"0元" forState:UIControlStateNormal];
        [_couponsBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        _couponsBtn.titleLabel.font = [UIFont systemFontOfSize:11.5*HWB];
        _couponsBtn.titleLabel.adjustsFontSizeToFitWidth = YES ;
        [_couponsBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 22*HWB, 0, 0)];
        [self.contentView addSubview:_couponsBtn];
        ADDTARGETBUTTON(_couponsBtn, takeOrder)
        [_couponsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(3); make.bottom.offset(-6);
            //            make.top.equalTo(_goodsTitle.mas_bottom).offset(3);
            make.height.offset(18*HWB); make.width.offset(63*HWB);
        }];
    }
    return _couponsBtn ;
}


-(UIButton*)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [[UIButton alloc]init];
        [_shareBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        [_shareBtn setTitle:@"分享赚:" forState:(UIControlStateNormal)];
        _shareBtn.titleLabel.font = [UIFont systemFontOfSize:10.0*HWB];
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-2);
            make.bottom.offset(-1);
        }];
        ADDTARGETBUTTON(_shareBtn, takeOrder)
    }
    return _shareBtn ;
}


#pragma mark --> 播放视频
-(void)boFangVideo{
    [PageJump openVideoUrlBySystemVideoPlayer:_goodsModel.video];
}



-(void)takeOrder{
    if (role_state==0) { [PageJump takeOrder:_goodsModel]; }
    else{  [PageJump shareGoods:_goodsModel goodsImgAry:nil]; }
}

@end
