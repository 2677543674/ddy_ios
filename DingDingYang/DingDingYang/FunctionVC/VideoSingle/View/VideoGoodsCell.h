//
//  VideoGoodsCell.h
//  DingDingYang
//
//  Created by ddy on 2017/6/17.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoGoodsCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     //商品图片
@property(nonatomic,strong) UIButton     * playVideoBtn ;  //播放视频
@property(nonatomic,strong) UILabel      * goodsTitle ;    //商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   //券后价
@property(nonatomic,strong) UILabel      * catPLab ;       //天猫价(原价)
@property(nonatomic,strong) UILabel      * salesLab ;      //销量
@property(nonatomic,strong) UIButton     * couponsBtn ;    //优惠券(无券隐藏)
//@property(nonatomic,strong) UILabel      * noCouponsLab ;  //无券提示
@property(nonatomic,strong) UIButton     * shareBtn ;      //分享赚，消费者隐藏
@property(nonatomic,strong) GoodsModel   * goodsModel ;


@end
