//
//  CheckVoucherFirstVC.m
//  DingDingYang
//
//  Created by ddy on 2017/6/19.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "CheckVoucherFirstVC.h"
#import "JiaoBiaoView.h"
#import "CVJiangliCell.h"
#import "VouchersListVC.h"
@interface CheckVoucherFirstVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIView * topBgView ;
@property(nonatomic,strong) UIView * tbView ;// 搜索框和按钮
@property(nonatomic,strong) UITextField * searchField ;
@property(nonatomic,strong) UIButton * searchBtn ;
@property(nonatomic,strong) UILabel * tishiLabTop1 , * tishiLabTop2;

//用户行为轮播
@property(nonatomic,strong) UITableView * broadcastTableV ;
@property(nonatomic,strong) NSTimer * timerScroll ;
@property(nonatomic,copy)   NSString * luoboPage ; //请求参数的开始页数
@property(nonatomic,strong) NSMutableArray * allDataAry ; //所有请求过来的数据
@property(nonatomic,strong) NSMutableArray * lunboDataAry ;//插入表格中的数据

@end

@implementation CheckVoucherFirstVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_timerScroll!=nil) {
        [_timerScroll setFireDate:[NSDate distantPast]];//运行
    }
    if (_searchField!=nil) {
         _searchField.text = @"" ;
        [_searchBtn setTitle:@"粘贴" forState:UIControlStateNormal];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (_timerScroll!=nil) {
        [_timerScroll setFireDate:[NSDate distantFuture]];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addTopTabbarView];
    self.view.backgroundColor = COLORWHITE ;
    

    _allDataAry = [NSMutableArray array];
    _lunboDataAry = [NSMutableArray array];
    
    _topBgView = [[UIView alloc]init];
    _topBgView.frame = CGRectMake(0, 0, ScreenW, 150);
    _topBgView.backgroundColor = ColorRGBA(251.f, 235.f, 225.f, 1);
    [self.view addSubview:_topBgView];
    
    [self tbView];
    
    [self creatTiBuZhouView];
    
    _luoboPage = @"1" ;
    
    [self huoquLuoBoData];

}

-(UIView*)tbView{
    if (!_tbView) {
        
        _tbView = [[UIView alloc]init];
        _tbView.layer.masksToBounds = YES ;
        _tbView.layer.cornerRadius = 8;
        _tbView.layer.borderWidth = 1 ;
        _tbView.layer.borderColor = LOGOCOLOR.CGColor ;
        _tbView.clipsToBounds = YES ;
        _tbView.backgroundColor = COLORWHITE ;
        [_topBgView addSubview:_tbView];
        [_tbView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(25); make.right.offset(-25);
            make.height.offset(38); make.bottom.offset(-20);
        }];
        
        _searchField = [UITextField textColor:Black51 PlaceHorder:@"粘贴商品标题搜索即有奖励" font:13*HWB];
        _searchField.clearButtonMode = UITextFieldViewModeWhileEditing ;
        _searchField.textAlignment = NSTextAlignmentCenter ;
        [_searchField addTarget:self action:@selector(returnKeyBoard) forControlEvents:UIControlEventEditingDidEndOnExit];
        [_searchField addTarget:self action:@selector(textChangeAndChangeBtn) forControlEvents:UIControlEventEditingChanged];
        [_tbView addSubview:_searchField];
        [_searchField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(-60);
            make.top.offset(0); make.bottom.offset(0);
        }];
        
        
        _searchBtn = [UIButton titLe:@"粘贴" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:15*HWB];
        ADDTARGETBUTTON(_searchBtn, clickSearchVoucher)
        [_tbView addSubview:_searchBtn];
        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0); make.top.offset(0);
            make.bottom.offset(0); make.width.offset(60);
        }];
        
        if (THEMECOLOR==2) { //主题色为浅色
             [_searchBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }

        UIImageView*zhaoImg = [[UIImageView alloc]init];
        zhaoImg.image = [UIImage imageNamed:@"wwchaimg"];
        [_topBgView addSubview:zhaoImg];
        [zhaoImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(30); make.bottom.equalTo(_searchField.mas_top);
            make.top.offset(15); make.width.offset((HFRAME(_topBgView)-75)/1.27);
        }];
        
        _tishiLabTop1 = [UILabel labText:@"从手淘复制商品标题粘贴查券" color:Black102 font:13*HWB];
        _tishiLabTop1.textAlignment = NSTextAlignmentCenter ;
         NSRange range1 = {3,6};
        _tishiLabTop1.attributedText = [self attriStr:@"从手淘复制商品标题粘贴查券" range:range1];
        [_topBgView addSubview:_tishiLabTop1];
        [_tishiLabTop1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(zhaoImg.mas_right).offset(15);
            make.right.offset(-30);  make.bottom.offset(-100);
        }];
        
        _tishiLabTop2 = [UILabel labText:@"最高奖励97%" color:Black102 font:13*HWB];
        _tishiLabTop2.numberOfLines = 0 ;
        _tishiLabTop2.textAlignment = NSTextAlignmentCenter ;
        NSRange range2 = {4,3};
        _tishiLabTop2.attributedText = [self attriStr:@"最高奖励97%" range:range2];
        [_topBgView addSubview:_tishiLabTop2];
        [_tishiLabTop2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tishiLabTop1.mas_centerX);
            make.bottom.offset(-80);
        }];        
    }
    return _tbView ;
}
-(NSMutableAttributedString*)attriStr:(NSString*)str range:(NSRange)strrange{
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:strrange];
    return dlStr;
}
//查券
-(void)clickSearchVoucher{
    ResignFirstResponder
    if (_searchField.text.length>0) {
        
        VouchersListVC * listVC = [[VouchersListVC alloc]init];
        listVC.strSearch = _searchField.text ;
        listVC.hidesBottomBarWhenPushed = YES ;
        PushTo(listVC, YES)
    }else{
        
        if ([UIPasteboard generalPasteboard].string.length>0) {
            _searchField.text = [UIPasteboard generalPasteboard].string ;
            [_searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
        }else{
             SHOWMSG(@"亲，复制内容后才能粘贴哦!")
        }
    }
   
}


-(void)creatTiBuZhouView{
    
    UILabel * tieshi = [UILabel labText:@"拿奖励小贴士" color:Black102 font:13*HWB];
    float  wi = StringSize(@"拿奖励小贴士", 13*HWB).width+5;
    tieshi.frame = CGRectMake(10, 225, wi, 20);
    [self.view addSubview:tieshi];
    
    
    float btnw = StringSize(@"详细教程>", 12*HWB).width+10 ;
    UIButton * jiaochengBtn = [UIButton titLe:@"详细教程>" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:COLORGRAY font:12*HWB];
    jiaochengBtn.titleLabel.font = [UIFont systemFontOfSize:12*HWB];
    jiaochengBtn.frame  =CGRectMake(wi+10, 220, btnw, 30);
    ADDTARGETBUTTON(jiaochengBtn, detailJiaoCheng)
    [self.view addSubview:jiaochengBtn];
//    [jiaochengBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(tieshi.mas_right).offset(0);
//        make.centerY.equalTo(jiaochengBtn.mas_centerY);
//        make.height.offset()
//    }];
    
    
    float whi = (ScreenW-40)/3 ;
    NSArray * numAry = @[@"1",@"2",@"3"];
    NSArray * ctAry = @[@"在淘宝复制商品标题",@"粘贴框到搜索框并搜索",@"确认收货后拿奖励"];
    for (NSInteger i=0 ; i<3; i++) {
        JiaoBiaoView * jbView = [[JiaoBiaoView alloc]initWithFrame:CGRectMake(10+(whi+10)*i, 255, whi, 50) num:numAry[i] content:ctAry[i]];

        [self.view addSubview:jbView];
    }
}

-(UITableView*)broadcastTableV{
    if (!_broadcastTableV) {
        _broadcastTableV = [[UITableView alloc]initWithFrame:CGRectMake(10, 265+50*HWB, ScreenW-20, ScreenH-(310+50*HWB)) style:UITableViewStylePlain];
        _broadcastTableV.backgroundColor = COLORGROUP ;
        _broadcastTableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _broadcastTableV.delegate = self;  _broadcastTableV.dataSource = self;
        _broadcastTableV.rowHeight = 36*HWB ;
        _broadcastTableV.layer.masksToBounds = YES ;
        _broadcastTableV.layer.cornerRadius = 8 ;
        [self.view addSubview:_broadcastTableV];
        [_broadcastTableV registerClass:[CVJiangliCell class] forCellReuseIdentifier:@"CVJiangliCell"];
        _broadcastTableV.userInteractionEnabled = NO ;
        _timerScroll = [NSTimer scheduledTimerWithTimeInterval:6.6 target:self selector:@selector(insertCellAndAnimion) userInfo:nil repeats:YES];
        
    }
    return _broadcastTableV;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _lunboDataAry.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CVJiangliCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CVJiangliCell" forIndexPath:indexPath];
    cell.txtctLab.text = _lunboDataAry[indexPath.row];
    return cell ;
}

-(void)insertCellAndAnimion{
    
    if (_lunboDataAry.count==_allDataAry.count-5) {
        [self huoquLuoBoData];
    }
    if (_allDataAry.count-3<_lunboDataAry.count){
        [_lunboDataAry removeAllObjects];
        for (NSInteger i = 0 ; i < 10; i++) {
            [_lunboDataAry addObject:_allDataAry[i]];
        }
    }
    [_lunboDataAry insertObject:_allDataAry[_lunboDataAry.count-1] atIndex:0];
    NSIndexPath * indexOne = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [CATransaction begin];
    [_broadcastTableV beginUpdates];
    [CATransaction setCompletionBlock: ^{
        //动画结束
    }];
    
    [_broadcastTableV insertRowsAtIndexPaths:@[indexOne] withRowAnimation:UITableViewRowAnimationMiddle];
    [_broadcastTableV endUpdates];
    [CATransaction commit];
    
}

-(void)scrollTableView{
    [_broadcastTableV setContentOffset:CGPointMake(0,_broadcastTableV.contentOffset.y+0.5)];
}

-(void)huoquLuoBoData{

    [NetRequest requestTokenURL:url_check_voucher_lbmsg parameter:@{@"st":_luoboPage} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray * ary = NULLARY(nwdic[@"list"]) ;
            [_allDataAry addObjectsFromArray:ary];
            
            if(_allDataAry.count>10&&[_luoboPage integerValue]==1){
                for (NSInteger i = 0 ; i < 10; i++) {
                    [_lunboDataAry addObject:_allDataAry[i]];
                }
                [self broadcastTableV];
            }
            if(ary.count>0){
                _luoboPage = FORMATSTR(@"%ld",(long)[_luoboPage integerValue]+1);
            }
        }

    } failure:^(NSString *failure) {
        
    }];
    
}

#pragma mark --> 奖励教程
-(void)detailJiaoCheng{
    WebVC * web = [[WebVC alloc]init];
    web.hidesBottomBarWhenPushed  = YES ;
    web.urlWeb = FORMATSTR(@"%@searchIntro.api?partnerId=%d",dns_dynamic_main,PID);
    PushTo(web, YES)
}

#pragma mark --> 输入框有关

-(void)textChangeAndChangeBtn{

    NSLog(@"-->: %@",_searchField.text);
    if (_searchField.text.length>0) {
        [_searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    }else{
        [_searchBtn setTitle:@"粘贴" forState:UIControlStateNormal];
    }
    
}

-(void)returnKeyBoard{ }

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


-(void)addTopTabbarView{
    self.title=@"宝贝搜索";
//    UIView*barView=[[UIView alloc]init];
//    barView.backgroundColor= COLORWHITE ;
//    barView.frame=CGRectMake(0, 0, ScreenW, NAVCBAR_HEIGHT);
//    [self.view addSubview:barView];
//   
//    UILabel * titleLab = [UILabel labText:@"宝贝搜索" color:Black51 font:TITLEFONT];
//    titleLab.frame=CGRectMake(ScreenW/2-100, 20, 200, 40);
//    titleLab.textAlignment=NSTextAlignmentCenter;
//    [barView addSubview:titleLab];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
