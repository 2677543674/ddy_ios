//
//  VouchersListVC.m
//  DingDingYang
//
//  Created by ddy on 2017/6/19.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "VouchersListVC.h"

#import "CVSearchListCell.h"

@interface VouchersListVC () <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property(nonatomic,strong) UITableView * tableView ;
@property(nonatomic,strong) NSMutableArray * cvdataAry ;
@property(nonatomic,strong) UISearchBar * searchBarTxt ;
@property(nonatomic,strong) NSMutableDictionary * parameter ;
@property(nonatomic,assign) NSInteger pageNum ;
@property(nonatomic,strong) UIButton * topBtn ;
@end

@implementation VouchersListVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden=YES;
    
    self.view.backgroundColor = COLORGROUP ;
    
    [self addTopTabbarView];
    
    _cvdataAry = [NSMutableArray array];
    
    [self parameter];
        
    [self tableView];
    
    [self topBtn];
}

-(NSMutableDictionary*)parameter{
    if (!_parameter) {
        _parameter = [NSMutableDictionary dictionary];
        _parameter[@"key"] = _strSearch ;
        _parameter[@"st"] = @"1" ;
    }
    return _parameter ;
}

-(void)searchGoodsList{
    [_tableView.mj_footer resetNoMoreData];
    _pageNum = 2 ;    _parameter[@"st"] = @"1" ;
    [NetRequest requestTokenURL:url_check_voucher_goods parameter:_parameter success:^(NSDictionary *nwdic) {
        
        if (_cvdataAry.count > 0) {
            [_cvdataAry removeAllObjects];
            [_tableView reloadData];
        }
        
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            
            NSArray*lAry= NULLARY( nwdic[@"list"] ) ;
            if (lAry.count>0) {

                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_cvdataAry addObject:model];
                }];
               
            }else{
                SHOWMSG(@"商品被抢完啦，下次早点来哦!")
            }
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
        [_tableView.mj_header endRefreshing];
        [_tableView reloadData];
    
    } failure:^(NSString *failure) {
        [_tableView.mj_header endRefreshing];
        SHOWMSG(failure)
    }];
}
-(void)loadMoreSearch{
    _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum);
    [NetRequest requestTokenURL:url_check_voucher_goods parameter:_parameter success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry= NULLARY( nwdic[@"list"] ) ;
            if (lAry.count>0) {
                
                _pageNum ++ ;
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_cvdataAry addObject:model];
                }];
                [_tableView.mj_footer endRefreshing];
                [_tableView reloadData];
            }else{
               [_tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
            [_tableView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        

    } failure:^(NSString *failure) {
        [_tableView.mj_footer endRefreshing];
        SHOWMSG(failure)
    }];

}


-(UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 65, ScreenW, ScreenH-65) style:UITableViewStylePlain];
        _tableView.backgroundColor = COLORGROUP ;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;  _tableView.dataSource = self;
        _tableView.rowHeight = ScreenH/7;
        if (ScreenH==480) { _tableView.rowHeight=ScreenH/6; }
        [self.view addSubview:_tableView];
        [_tableView registerClass:[CVSearchListCell class] forCellReuseIdentifier:@"cvsearch_listcell"];
        [_tableView registerClass:[CVListConsumersHead class] forHeaderFooterViewReuseIdentifier:@"cvlist_head"];
        
        __weak VouchersListVC*SelfView = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(searchGoodsList)];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreSearch];
        }];
        [_tableView.mj_header beginRefreshing];
    }
    return _tableView;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _cvdataAry.count ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (role_state==0&&TOKEN.length>0) {
        return 30 ;
    }
    return 0 ;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return  0 ;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (role_state==0&&TOKEN.length>0) {
        CVListConsumersHead * cvhead = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cvlist_head"];
        return cvhead ;
    }
    return nil ;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CVSearchListCell*  cell = [tableView dequeueReusableCellWithIdentifier:@"cvsearch_listcell" forIndexPath:indexPath];
    cell.listModel = _cvdataAry[indexPath.row];
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GoodsModel * model = _cvdataAry[indexPath.row];
//    if (model.itemUrl.length==0) {
        GoodsDetailsVC*detailVc = [[GoodsDetailsVC alloc]init] ;
        detailVc.dmodel = model ;
        PushTo(detailVc, YES)
//    }else{
//        WebVC * vcweb = [[WebVC alloc]init];
//        vcweb.urlWeb = model.itemUrl ;
//        PushTo(vcweb, YES)
//    }
   
}



-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
        _topBtn.frame = CGRectMake(ScreenW-36*HWB-10, ScreenH-36*HWB-10, 36*HWB, 36*HWB);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_tableView.contentOffset.y>=0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (_tableView.contentOffset.y>ScreenH) {
                _topBtn.alpha = 1 ;
            }else{
                if (_tableView.contentOffset.y>ScreenH/2) {
                    _topBtn.alpha = _tableView.contentOffset.y/ScreenH ;
                }else{
                    _topBtn.alpha = 0 ;
                }
            }
        });
    }
    if (_searchBarTxt.isFirstResponder==YES) {
        [_searchBarTxt resignFirstResponder];
    }
}
-(void)clickBackTop{
    [_tableView setContentOffset:CGPointZero animated:YES];
}

#pragma mark --> 列表页输入框搜索

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self startSearchGoods];
}
-(void)startSearchGoods{
    ResignFirstResponder
    if (_searchBarTxt.text.length==0) {
        SHOWMSG(@"请输入或粘贴商品名称!")
        return ;
    }
    if ([_parameter[@"key"] isEqualToString:_searchBarTxt.text] ) {
        [_searchBarTxt resignFirstResponder];
        return ;
    }
    _parameter[@"key"] = _searchBarTxt.text ;
    [self searchGoodsList];
}

//创建顶部view
-(void)addTopTabbarView{
    
    UIView*barview=[[UIView alloc]init];
    barview.backgroundColor=COLORWHITE;
    barview.frame=CGRectMake(0,0, ScreenW,NAVCBAR_HEIGHT);
    [self.view addSubview:barview];
    
    _searchBarTxt=[[UISearchBar alloc]initWithFrame:CGRectMake(40, 20, ScreenW-110, 44)];
    _searchBarTxt.delegate=self;
    _searchBarTxt.searchBarStyle = UISearchBarStyleMinimal;
    _searchBarTxt.placeholder=@"请输入商品名称";
    _searchBarTxt.text = REPLACENULL(_strSearch);
    [self.view addSubview:_searchBarTxt];
    
    UIButton*searchBtn=[UIButton titLe:@"搜索" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGROUP font:FONT_15];
    searchBtn.layer.cornerRadius=6;
    [searchBtn addTarget:self action:@selector(startSearchGoods) forControlEvents:UIControlEventTouchUpInside];
    [barview addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(26);  make.right.offset(-10);
        make.height.offset(32); make.width.offset(50);
    }];
    
    UIButton*backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"goback"] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(13.5, 12, 13.5, 22)];
    backBtn.frame=CGRectMake(0, 20, 44, 44);
    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    [barview addSubview:backBtn];
    
    if (THEMECOLOR==2) { // 主题色为浅色
        [searchBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
    }

}


-(void)clickBack{   PopTo(YES)  }

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
