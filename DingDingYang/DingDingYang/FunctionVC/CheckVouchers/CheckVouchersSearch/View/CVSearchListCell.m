//
//  CVSearchListCell.m
//  DingDingYang
//
//  Created by ddy on 2017/6/20.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "CVSearchListCell.h"
@implementation CVSearchListCell

-(void)setListModel:(GoodsModel *)listModel{
    
    _listModel = listModel ;
    
    [_goodsImgView sd_setImageWithURL:[NSURL URLWithString:_listModel.smallPic]];
    
    _titLab.text = _listModel.goodsName ;
    
    _priceLab.text = FORMATSTR(@"%@元",_listModel.price) ;
    
    [_xiadanBtn setTitle:FORMATSTR(@"下单奖励\n%@",_listModel.commission) forState:UIControlStateNormal];
    
    _xiaoliangLab.text = FORMATSTR(@"销量:%@",_listModel.sales) ;
    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        
        [self goodsImgView];
        
        [self xiadanBtn];
        
        [self titLab];
        
        [self priceLab];
        
        [self xiaoliangLab];
        
        UIImageView * linesImg = [[UIImageView alloc]init];
        linesImg.backgroundColor = Black204 ;
        [self.contentView addSubview:linesImg];
        [linesImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgView.mas_right).offset(0);
            make.height.offset(1); make.bottom.offset(0);
            make.right.offset(0);
        }];
        
    }
    return self ;
}

-(UIImageView*)goodsImgView{
    if (!_goodsImgView) {
        _goodsImgView = [[UIImageView alloc]init];
        _goodsImgView.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgView];
        [_goodsImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(6); make.left.offset(0);
            make.bottom.offset(0); make.width.equalTo(_goodsImgView.mas_height);
        }];
    }
    return _goodsImgView ;
}

-(UIButton*)xiadanBtn{
    if (!_xiadanBtn) {
        _xiadanBtn = [UIButton titLe:@"下单奖励\n¥0" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:14*HWB];
        _xiadanBtn.titleLabel.numberOfLines = 0 ;
        _xiadanBtn.titleLabel.textAlignment = NSTextAlignmentCenter ;
        [_xiadanBtn boardWidth:1 boardColor:LOGOCOLOR corner:25*HWB];
        [_xiadanBtn addTarget:self action:@selector(clickTakeOrder) forControlEvents:(UIControlEventTouchUpInside)];
        [self .contentView addSubview:_xiadanBtn];
        [_xiadanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.offset(-10);
            make.height.offset(50*HWB);
            make.width.offset(80*HWB);
        }];
    }
    return _xiadanBtn ;
}

-(UILabel*)titLab{
    if (!_titLab) {
        _titLab = [UILabel labText:@"商品标题商品标题商品标题" color:Black51 font:13*HWB];
        _titLab.numberOfLines = 2 ;
        [self.contentView addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgView.mas_right).offset(6);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(3);
            make.right.equalTo(_xiadanBtn.mas_left).offset(-10);
        }];
    }
    return _titLab ;
}

-(UILabel*)priceLab{
    if (!_priceLab) {
        _xianjiaLab = [UILabel labText:@"现价:" color:Black51 font:12.5*HWB];
        [self.contentView addSubview:_xianjiaLab];
        [_xianjiaLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgView.mas_right).offset(6);
            make.top.equalTo(self.contentView.mas_centerY).offset(12) ;
        }];
        
        _priceLab = [UILabel labText:@"888元" color:LOGOCOLOR font:14*HWB] ;
        [self.contentView addSubview:_priceLab] ;
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_xianjiaLab.mas_right).offset(2);
            make.centerY.equalTo(_xianjiaLab.mas_centerY);
        }];
        
    }
    return _priceLab ;
}

-(UILabel*)xiaoliangLab{
    if (!_xiaoliangLab) {
        _xiaoliangLab = [UILabel labText:@"销量:" color:Black204 font:12.5*HWB];
        [self.contentView addSubview:_xiaoliangLab];
        [_xiaoliangLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_xiadanBtn.mas_left).offset(-10);
            make.centerY.equalTo(_xianjiaLab.mas_centerY);
        }];
    }
    return _xianjiaLab ;
}


-(void)clickTakeOrder{
    if (_listModel!=nil) {
        [PageJump takeOrder:_listModel];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end



@implementation CVListConsumersHead

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        
        _xfzUpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _xfzUpBtn.frame = CGRectMake(0, 0, ScreenW, 30);
        [_xfzUpBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        [_xfzUpBtn setTitle:@"你是消费者，暂时无法享受奖励，立即升级会员拿奖励 >" forState:UIControlStateNormal];
        _xfzUpBtn .titleLabel.adjustsFontSizeToFitWidth = YES ;
        [_xfzUpBtn addTarget:self action:@selector(clickGotoZhifu) forControlEvents:(UIControlEventTouchUpInside)];
        [self.contentView addSubview:_xfzUpBtn];
        
    }
    return self ;
}

//进入支付界面
-(void)clickGotoZhifu{
//    UIViewController*topVC ;
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    if ([window.rootViewController isKindOfClass:[MainVC class]]) {
//        MainVC * mian = (MainVC*)window.rootViewController ;
//        UITabBarController*tabbarVC = mian.tabController;
//        UINavigationController*naVC=(UINavigationController*)tabbarVC.selectedViewController;
//        topVC =naVC.topViewController;
//    }
    
    
    
}

@end














