//
//  CVJiangliCell.m
//  DingDingYang
//
//  Created by ddy on 2017/6/19.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "CVJiangliCell.h"

@implementation CVJiangliCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = COLORGROUP ;
        _ldImgV = [[UIImageView alloc]init];
        _ldImgV.image = [UIImage imageNamed:@"ling_dang"];
        [self.contentView addSubview:_ldImgV];
        [_ldImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5); make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.offset(14*HWB); make.width.offset(14*HWB);
        }];
    
        _txtctLab = [UILabel labText:@"恭喜某某**获得奖励恭喜某某**获得奖励恭喜某某励" color:Black102 font:12.5*HWB];
//        _txtctLab.textAlignment = NSTextAlignmentCenter ;
        [self.contentView addSubview:_txtctLab];
        [_txtctLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(8+14*HWB);  make.right.offset(-5);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
        
    }
    return self ;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
