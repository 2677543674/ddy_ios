//
//  CVSelectView.h
//  DingDingYang
//
//  Created by ddy on 2017/6/20.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CVSViewDelegate <NSObject>

@optional
-(void)selectShouldP:(NSString*)str ;

@end

@interface CVSelectView : UIView

@property(nonatomic,strong) UIView * topSelectView ;

@property(nonatomic,strong) UIButton * selectBtn ;



@property(nonatomic,assign) id<CVSViewDelegate>delegate ;

@end



//@interface CvSBtn : UIButton
//
//@property(nonatomic,strong) UILabel * texLab ;
//@property(nonatomic,strong) UIImageView * simgView ;
//
//@end





