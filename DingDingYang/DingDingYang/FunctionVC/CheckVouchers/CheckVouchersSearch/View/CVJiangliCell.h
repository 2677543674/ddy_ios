//
//  CVJiangliCell.h
//  DingDingYang
//
//  Created by ddy on 2017/6/19.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVJiangliCell : UITableViewCell

@property(nonatomic,strong) UIImageView * ldImgV ;
@property(nonatomic,strong) UILabel * txtctLab ;

@end
