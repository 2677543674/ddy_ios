//
//  JiaoBiaoView.m
//  DingDingYang
//
//  Created by ddy on 2017/6/19.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "JiaoBiaoView.h"

@implementation JiaoBiaoView

-(instancetype)initWithFrame:(CGRect)frame num:(NSString*)number content:(NSString*)ct{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORGROUP ;
        self.layer.cornerRadius = 6 ;
        _numberLab = [UILabel labText:number color:LOGOCOLOR font:15];
        _numberLab.frame = CGRectMake(5, 10, 18, 18);
        _numberLab.layer.cornerRadius = 9 ;
        _numberLab.textAlignment = NSTextAlignmentCenter ;
        [_numberLab boardWidth:1 boardColor:LOGOCOLOR];
        [self addSubview:_numberLab];
        
        _contentLab = [UILabel labText:ct color:Black102 font:12*HWB];
        _contentLab.numberOfLines = 0 ;
        [self addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(25) ; make.top.offset(5);
            make.bottom.offset(-5); make.right.offset(-2);
        }];
    }
    return self ;
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
