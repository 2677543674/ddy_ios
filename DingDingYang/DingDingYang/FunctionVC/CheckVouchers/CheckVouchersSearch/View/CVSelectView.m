//
//  CVSelectView.m
//  DingDingYang
//
//  Created by ddy on 2017/6/20.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "CVSelectView.h"

@implementation CVSelectView


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _topSelectView = [[UIView alloc]init];
        _topSelectView.backgroundColor = COLORWHITE ;
        _topSelectView.frame = CGRectMake(0, 0, ScreenW, 38);
        [self addSubview:_topSelectView];
        
        NSArray * titleAry = @[@"综合",@"奖励比率",@"销量"];
        for (NSInteger i =0; i<3; i++) {
            _selectBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
            _selectBtn.frame = CGRectMake(ScreenW/3*i, 0, ScreenW/3, 38);
            [_selectBtn setTitle:titleAry[i] forState:UIControlStateNormal];
            [_selectBtn setTitleColor:Black102 forState:UIControlStateNormal];
            _selectBtn.tag = i + 10 ;
            _selectBtn.titleLabel.font = [UIFont systemFontOfSize:13.5*HWB];
            [_selectBtn addTarget:self action:@selector(clickSelectConditions:) forControlEvents:UIControlEventTouchUpInside];
//            _selectBtn.selected = NO ;
            [_topSelectView addSubview:_selectBtn];
            
            if (i==0) {
                _selectBtn.selected = YES ;
               [_selectBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
            }
//            if (i==2) {
//                [_selectBtn setImage:[UIImage imageNamed:@"cv_no_select"] forState:(UIControlStateNormal)];
//                [_selectBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -40*HWB)];
//                [_selectBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -40*HWB, 0, 0)];
//            }
        }
        
    }
    return self ;
}

-(void)clickSelectConditions:(UIButton*)btn{
    
    for (UIButton * sbtn in _topSelectView.subviews) {
        [sbtn setTitleColor:Black102 forState:UIControlStateNormal];
    }
    
     [btn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    
    if (btn.tag==10) {
        [self.delegate selectShouldP:@""];
    }
    if (btn.tag==11) {
        [self.delegate selectShouldP:@"6"];
    }
    if (btn.tag==12) {
        [self.delegate selectShouldP:@"8"];
    }
    
    /*
    if (btn.tag==10) {
        if (btn.selected==NO) {
            btn.selected = YES ;
            [btn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
            UIButton * btn11 = (UIButton*)[_topSelectView viewWithTag:11];
            btn11.selected = NO ;
            [btn11 setTitleColor:Black102 forState:(UIControlStateNormal)];
            
            UIButton * btn12 = (UIButton*)[_topSelectView viewWithTag:12];
            btn12.selected = NO ;
            [btn12 setImage:[UIImage imageNamed:@"cv_no_select"] forState:(UIControlStateNormal)];
            [btn12 setTitleColor:Black102 forState:(UIControlStateNormal)];
            [self.delegate selectShouldP:@""];
        }else{
            //不执行
        }
        
    }else if (btn.tag==11){
        if (btn.selected == NO) {
            btn.selected = YES ;
            [btn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
            UIButton * btn10 = (UIButton*)[_topSelectView viewWithTag:10];
            btn10.selected = NO ;
            [btn10 setTitleColor:Black102 forState:(UIControlStateNormal)];
            
            UIButton * btn12 = (UIButton*)[_topSelectView viewWithTag:12];
            btn12.selected = NO ;
            [btn12 setImage:[UIImage imageNamed:@"cv_no_select"] forState:(UIControlStateNormal)];
            [btn12 setTitleColor:Black102 forState:(UIControlStateNormal)];
            [self.delegate selectShouldP:@"7"];
        }else{
            //不执行
        }
       
        
    }else{
       
        UIButton * btn10 = (UIButton*)[_topSelectView viewWithTag:10];
        btn10.selected = NO ;
        [btn10 setTitleColor:Black102 forState:(UIControlStateNormal)];
        
        UIButton * btn11 = (UIButton*)[_topSelectView viewWithTag:11];
        btn11.selected  = NO ;
        [btn11 setTitleColor:Black102 forState:(UIControlStateNormal)];
        [btn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        if (btn.selected==NO) {
            [btn setImage:[UIImage imageNamed:@"cv_down_select"] forState:(UIControlStateNormal)];
            btn.selected = YES ;
            [self.delegate selectShouldP:@"4"];
        }else{
            [btn setImage:[UIImage imageNamed:@"cv_up_select"] forState:(UIControlStateNormal)];
            btn.selected = NO ;
            [self.delegate selectShouldP:@"3"];
        }
    }*/
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end


 








