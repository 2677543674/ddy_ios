//
//  CVSearchListCell.h
//  DingDingYang
//
//  Created by ddy on 2017/6/20.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVSearchListCell : UITableViewCell

@property(nonatomic,strong) UIImageView * goodsImgView ; //图片
@property(nonatomic,strong) UILabel * titLab ; //标题
@property(nonatomic,strong) UILabel * xianjiaLab ; //现价
@property(nonatomic,strong) UILabel * priceLab ; //现价价格
@property(nonatomic,strong) UILabel * xiaoliangLab ; //销量

@property(nonatomic,strong) UIButton * xiadanBtn ;

@property(nonatomic,strong) GoodsModel * listModel ;


@end

@interface CVListConsumersHead : UITableViewHeaderFooterView

@property(nonatomic,strong) UIButton * xfzUpBtn ;//有消费者时展示

@end









