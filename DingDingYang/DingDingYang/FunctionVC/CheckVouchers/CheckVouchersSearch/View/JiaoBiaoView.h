//
//  JiaoBiaoView.h
//  DingDingYang
//
//  Created by ddy on 2017/6/19.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JiaoBiaoView : UIView

@property(nonatomic,strong) UILabel * numberLab ;
@property(nonatomic,strong) UILabel * contentLab ;

-(instancetype)initWithFrame:(CGRect)frame num:(NSString*)number content:(NSString*)ct ;

@end
