//
//  NewOrmdVC.h
//  DaRenGouWu
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewOrmdVC : DDYViewController
@property(nonatomic,strong) MClassModel * mcsModel ;
@property(nonatomic,assign)BOOL isOnePage;
@property(nonatomic,copy) NSString* modelTitle;
@end
