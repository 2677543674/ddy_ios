//
//  NewOrmdVC.m
//  DaRenGouWu
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "NewOrmdVC.h"
#import "OrmdSearchView.h"
#import "NewOrmdCell.h"
#import "SharePopoverVC.h"
#import "CompositePosterImg.h"
#import "PictureCell.h"
@interface NewOrmdVC ()<UICollectionViewDelegate,UICollectionViewDataSource,OrmdSearchViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong) OrmdSearchView* searchView;
@property(nonatomic,strong) UICollectionView * collectionCV ;
@property(nonatomic,strong) NSMutableArray* titleArr;
@property(nonatomic,strong) NSMutableArray * dataArray1;
@property(nonatomic,strong) NSMutableArray * dataArray2;
@property(nonatomic,strong) NSMutableArray * dataArray3;
@property(nonatomic,strong) NSMutableArray * listArray ;
@property(nonatomic,strong) NSMutableDictionary * parameter ;
@property(nonatomic,strong) NSMutableDictionary * parameter2 ;
@property(nonatomic,strong) NSMutableDictionary * parameter3 ;
@property(nonatomic,assign) NSInteger pageNum ;
@property(nonatomic,assign) NSInteger pageNum2 ;
@property(nonatomic,assign) NSInteger pageNum3 ;
@property(nonatomic,assign) BOOL isSelect;
@property(nonatomic,assign) BOOL isSelect2;
@property(nonatomic,assign) BOOL isSelect3;
@property(nonatomic,strong) NSMutableArray *goodsModel;
@property(nonatomic,strong) NSMutableArray *goodsImage;
@property(nonatomic,strong) CompositePosterImg * imgPoster ; // 生成海报
@property(nonatomic,copy) NSMutableArray * imgPosterAry ; // 生成的海图片

@property(nonatomic,strong) NSMutableArray *goodsImageBtn;
@property(nonatomic,strong) UIScrollView* scrollerView;
@property(nonatomic,strong) UIButton* numberPicBtn;
@property(nonatomic,strong) UICollectionView* pictureView;
@property(nonatomic,strong) UIView* backView;
@property(nonatomic,strong) UITextField* textField;
@property(nonatomic,strong) UITextView* textView;

@property(nonatomic,strong) UIView * bottomView ;  // 底部显示两个按钮的view
@end

@implementation NewOrmdVC

//代理
-(void)clickBtn:(UIButton*)btn{
    //判断是否是第二个collectview
    if ((btn.tag-10+100==100)&&_isSelect==NO) {
        UICollectionView* collectionView = [self.view viewWithTag:100];
        [collectionView.mj_header beginRefreshing];
        _isSelect=YES;
    }
    
    if ((btn.tag-10+100==101)&&_isSelect2==NO) {
        UICollectionView* collectionView = [self.view viewWithTag:101];
        [collectionView.mj_header beginRefreshing];
        _isSelect2=YES;
    }
    if ((btn.tag-10+100==102)&&_isSelect3==NO) {
        UICollectionView* collectionView = [self.view viewWithTag:102];
        [collectionView.mj_header beginRefreshing];
        _isSelect3=YES;
    }
    
    for (int i=0; i<_titleArr.count; i++) {
        UIButton* btn = [self.searchView viewWithTag:i+10];
        btn.selected=NO;
    }
    btn.selected=YES;
    
    [_scrollerView setContentOffset:CGPointMake((btn.tag-10)*ScreenW,_scrollerView.contentOffset.y) animated:YES];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _mcsModel.title.length==0?@"官方推荐":_mcsModel.title;
    self.view.backgroundColor = COLORGROUP ;
    _pageNum = 1 ;
    _pageNum2 = 1 ;
    _pageNum3 = 1 ;
    _imgPosterAry = [NSMutableArray array];
    _dataArray1 = [NSMutableArray array];
    _dataArray2 = [NSMutableArray array];
    _dataArray3 = [NSMutableArray array];
    _titleArr = [NSMutableArray array];
    _listArray = [NSMutableArray array];
    _goodsModel=[NSMutableArray array];
    _goodsImage=[NSMutableArray array];
    _goodsImageBtn=[NSMutableArray array];
    _isSelect=YES;
    [self getCategory];
    
#ifdef DEBUG
    UIButton * yuLanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [yuLanBtn setTitle:@"海报预览(测试)" forState:UIControlStateNormal];
    [yuLanBtn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    [yuLanBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    yuLanBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13*HWB];
    yuLanBtn.frame=CGRectMake(0, 0, 50, 44);
    [yuLanBtn addTarget:self action:@selector(yunlan) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:yuLanBtn];
#endif
    
}


// 预览测试
-(void)yunlan{
    if (_goodsImage.count==0||_goodsModel.count==0) {
        SHOWMSG(@"请先勾选要分享的商品哦~") return ;
    }
    [CompositePosterImg goodsModeAry:_goodsModel imgAry:_goodsImage posterImg:^(NSMutableArray *posterAry) {
        [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:posterAry];
    } failState:^(NSInteger fresult) {
        SHOWMSG(@"生成失败!")
    }];
}



-(void)getCategory{
    
    MBShow(@"正在加载...")
    [NetRequest requestTokenURL:url_recommmend_list parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSMutableArray* dataArr=[NSMutableArray array];
            NSArray * ary = nwdic[@"list"];
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                MClassModel *model=[[MClassModel alloc]initWithMC:obj];
                [dataArr addObject:model];
                
            }];
            if (dataArr.count>0) {
                for (int i=0; i<dataArr.count; i++) {
                    MClassModel *model=dataArr[i];
                    [_titleArr addObject:model.title];
                    [_listArray addObject:[NSString stringWithFormat:@"%ld",model.listViewType]];
                }
                [self initParameter];
                [self initSearchView];
                [self bottomButton];
                [self initPictureView];
                [self scrollerView];
                
                [self initCollectionCV];
                
            }
            
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        MBHideHUD
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self getCategory];
        }];
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(UIScrollView *)scrollerView{
    if (_scrollerView==nil) {
        _scrollerView=[[UIScrollView alloc]init];
        _scrollerView.showsVerticalScrollIndicator=NO;
        _scrollerView.showsHorizontalScrollIndicator=NO;
        _scrollerView.bounces=NO;
        _scrollerView.pagingEnabled=YES;
        _scrollerView.delegate=self;
        _scrollerView.tag = 999;
        _scrollerView.contentSize=CGSizeMake(ScreenW*_titleArr.count, 0);
        [self.view addSubview:_scrollerView];
        [_scrollerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);
            make.top.equalTo(self.searchView.mas_bottom).offset(0);
            make.bottom.equalTo(self.numberPicBtn.mas_top).offset(-1);
            make.right.offset(0);
        }];
    }
    return _scrollerView;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag==999) {
        CGPoint point=scrollView.contentOffset;
        UIButton* btn=[self.view viewWithTag:point.x/ScreenW+10];
        [self clickBtn:btn];
    }
}

-(void)initCollectionCV{
    for (int i=0; i<_titleArr.count; i++) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        UICollectionView *collectionCV = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        
        collectionCV.delegate = self ;
        collectionCV.dataSource = self ;
        collectionCV.tag=100+i;
        collectionCV.backgroundColor = COLORGROUP ;
        [self.scrollerView addSubview:collectionCV];
        [collectionCV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(i*ScreenW);
            make.top.offset(0);
            make.height.equalTo(self.scrollerView.mas_height);
            make.width.offset(ScreenW);
        }];
        [collectionCV registerClass:[NewOrmdCell class] forCellWithReuseIdentifier:@"NewOrmdCell"];
        
        __weak NewOrmdVC * SelfView = self;
        
        collectionCV.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [SelfView requestSKData:collectionCV.tag];
        }];
        collectionCV.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreSKData:collectionCV.tag];
        }];
        if (i==0) {
            [collectionCV.mj_header beginRefreshing];
        }
    }
    
}

-(void)initParameter{
    _parameter = [NSMutableDictionary dictionary];

    _parameter[@"viewType"]=_listArray[0];
    _parameter[@"st"]=@"1";
    _parameter[@"key"]=@"";
    
    _parameter2 = [NSMutableDictionary dictionary];
    _parameter2[@"viewType"]=_listArray[1];
    _parameter2[@"st"]=@"1";
    _parameter2[@"key"]=@"";
    if (_titleArr.count>2) {
        _parameter3 = [NSMutableDictionary dictionary];
        _parameter3[@"viewType"]=_listArray[2];
        _parameter3[@"st"]=@"1";
        _parameter3[@"key"]=@"";
    }
}

-(void)initPictureView{
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
    _pictureView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, NAVCBAR_HEIGHT, ScreenW, 38) collectionViewLayout:layout];
    [_pictureView registerClass:[PictureCell class] forCellWithReuseIdentifier:@"PictureCell"];
    
    _pictureView.tag=200;
    _pictureView.delegate = self ;
    _pictureView.dataSource = self ;
    _pictureView.backgroundColor = COLORWHITE ;
    _pictureView.showsHorizontalScrollIndicator = NO ;
    [self.view addSubview:_pictureView];
    [_pictureView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.right.offset(0);
        make.height.offset(0);
        make.bottom.equalTo(_bottomView.mas_top).offset(-1);
    }];
    
    _numberPicBtn=[UIButton new];
    [_numberPicBtn setTitle:@"查看已勾选商品（0）" forState:UIControlStateNormal];
    [_numberPicBtn setBackgroundColor:[UIColor whiteColor]];
    _numberPicBtn.titleLabel.font=[UIFont systemFontOfSize:12*HWB];
    [_numberPicBtn setTitleColor:Black51 forState:UIControlStateNormal];
    [_numberPicBtn addTarget:self action:@selector(clickNum) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_numberPicBtn];
    [_numberPicBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.bottom.equalTo(_pictureView.mas_top).offset(-1);
        make.right.offset(0);
        make.height.offset(25*HWB);
    }];
}

-(void)clickNum{
    
    //    [UIView animateWithDuration:0.18 animations:^{
    //        [_pictureView mas_updateConstraints:^(MASConstraintMaker *make) {
    //            make.height.offset(_pictureView.bounds.size.height==0?ScreenW/4.5:0);
    //        }];
    //        [self.view layoutIfNeeded];
    //    }];
    [_pictureView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.offset(_pictureView.bounds.size.height==0?ScreenW/4.5:0);
    }];
    
    //    [self.view layoutIfNeeded];
    [_pictureView reloadData];
    
    
    
}

-(void)initSearchView{
    _searchView=[[OrmdSearchView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 58*HWB+8) andTitle:_titleArr];
    _searchView.backgroundColor=[UIColor whiteColor];
    _searchView.delegate=self;
    [_searchView.searchBtn addTarget:self action:@selector(clickSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_searchView];
    [_searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(58*HWB+8);
    }];
    //偷懒
    UIButton* searchBtn=[_searchView viewWithTag:888];
    [searchBtn addTarget:self action:@selector(clickSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickSearchBtn:(UIButton *)btn{
    _parameter[@"key"] = _searchView.searchText.text;
    _parameter2[@"key"] = _searchView.searchText.text;
    _parameter3[@"key"] = _searchView.searchText.text;
    _isSelect=NO;
    _isSelect2=NO;
    _isSelect3=NO;
    CGPoint point=_scrollerView.contentOffset;
    UIButton* titleBtn=[self.view viewWithTag:point.x/ScreenW+10];
    [self clickBtn:titleBtn];
    [self.view endEditing:YES];

}

#pragma mark ===>>> 底部两个分享按钮
-(void)bottomButton{
    
    _bottomView = [[UIView alloc]init];
    _bottomView.backgroundColor = COLORWHITE ;
    [self.view addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0); make.height.offset(32*HWB+10);
        make.bottom.offset(IS_IPHONE_X?-30:0);
    }];
    
    UIButton * shareBtn = [UIButton titLe:@"海报分享" bgColor:RGBA(252, 56, 50, 1) titColorN:COLORWHITE titColorH:COLORWHITE font:13*HWB];
    ADDTARGETBUTTON(shareBtn, clickShareBtn:)
    shareBtn.layer.cornerRadius = 6;
    shareBtn.clipsToBounds = YES;
    [_bottomView addSubview:shareBtn];
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15); make.bottom.offset(-5); make.height.offset(32*HWB);
        make.right.equalTo(_bottomView.mas_centerX).offset(-8);
    }];
    
    UIButton* webBtn = [UIButton titLe:@"Web集合页" bgColor:HRGCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13*HWB];
    ADDTARGETBUTTON(webBtn, clickWebBtn:)
    webBtn.layer.cornerRadius = 6;
    webBtn.clipsToBounds = YES;
    [_bottomView addSubview:webBtn];
    [webBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-15); make.bottom.offset(-5); make.height.offset(32*HWB);
        make.left.equalTo(_bottomView.mas_centerX).offset(8);
    }];
    
}

-(void)clickShareBtn:(UIButton *)btn{
    
    if (_goodsImage.count==0||_goodsModel.count==0) {
        SHOWMSG(@"请先勾选要分享的商品哦~")
        return ;
    }

    NSString* goodsIds=@"";
    
    for (int i=0; i<_goodsModel.count; i++) {
        GoodsModel * model = _goodsModel[i];
        if ([model.ormdSelect isEqualToString:@"1"]) {
            if ([goodsIds isEqualToString:@""]) {
                goodsIds=FORMATSTR(@"%@",model.goodsId);
            }else{
                goodsIds=FORMATSTR(@"%@,%@",goodsIds,model.goodsId);
            }
        }
    }


    MBShow(@"正在加载...")
    [NetRequest requestTokenURL:url_recommmend_sharePoster parameter:@{@"goodsIds":goodsIds} success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray* list=nwdic[@"list"];
            for (GoodsModel* model in _goodsModel) {
                for (NSDictionary* dic in list) {
                    NSLog(@"%@",dic[@"goodsId"]);
                    NSLog(@"%@",model.goodsId);
                    
                    if ([model.goodsId isEqualToString:[dic[@"goodsId"] stringValue]]) {
                        model.url=dic[@"url"];
                        break;
                    }
                }
            }
            
            [CompositePosterImg goodsModeAry:_goodsModel imgAry:_goodsImage posterImg:^(NSMutableArray *posterAry) {
                NSLog(@"%ld",posterAry.count);
                [self presentSystemShare:posterAry];
//                [ImageBrowserViewController show:TopNavc.topViewController type:0 index:0 imagesBlock:^NSArray *{
//                    return posterAry ;
//                }];
            } failState:^(NSInteger fresult) {
                
            }];
            
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        MBHideHUD
    }];
}

#pragma mark === >>> 调用系统分享
//  调用系统更多分享方式
-(void)presentSystemShare:(NSMutableArray* )arr{
    [SharePopoverVC openSystemShareImage:arr];
}


-(void)clickWebBtn:(UIButton *)btn{
    
    if (_goodsImage.count==0||_goodsModel.count==0) {
        SHOWMSG(@"请先勾选要分享的商品哦~")
        return ;
    }

    if (NSUDTakeData(@"shareTitle")!=nil&&NSUDTakeData(@"shareContent")!=nil) {
        [self popBackView:NSUDTakeData(@"shareTitle") andContent:NSUDTakeData(@"shareContent")];
    }else{
        [NetRequest requestTokenURL:url_recommmend_webContent parameter:nil success:^(NSDictionary *nwdic) {
            MBHideHUD
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSString* title=REPLACENULL(nwdic[@"title"]);
                NSString* content=REPLACENULL(nwdic[@"content"]);
                [self popBackView:title andContent:content];
                
            }else{
                SHOWMSG(nwdic[@"result"])
            }
        } failure:^(NSString *failure) {
            MBHideHUD
            SHOWMSG(failure)
        }];
    }
    
}

-(void)popBackView:(NSString* )title andContent:(NSString*)content{
    if (_backView==nil) {
        
        _backView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
        UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickBackView)];
        tap.delegate=self;
        [_backView addGestureRecognizer:tap];
        _backView.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.3];
        _backView.tag=1000;
        [[UIApplication sharedApplication].keyWindow addSubview:_backView];
        UIView* shareView=[UIView new];
        shareView.backgroundColor=[UIColor whiteColor];
        shareView.tag=2000;
        shareView.layer.cornerRadius=10;
        shareView.clipsToBounds=YES;
        [_backView addSubview:shareView];

        [shareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_backView.mas_centerX);
            make.centerY.equalTo(_backView.mas_centerY).offset(-100*HWB);
            make.width.offset(260);
            make.height.offset(276);
        }];
        UILabel* label=[UILabel new];
        label.font=[UIFont systemFontOfSize:16];
        label.text=@"分享标题:";
        label.textColor=Black102;
        [shareView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.offset(10);
            make.height.offset(25);
        }];
        _textField=[UITextField new];
        _textField.backgroundColor=COLORGROUP;
        _textField.layer.cornerRadius=6;
        _textField.clipsToBounds=YES;
        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 0)];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.text=title;
        _textField.font=[UIFont systemFontOfSize:16];
        [shareView addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(label.mas_bottom).offset(6);
            make.left.offset(10);
            make.right.offset(-10);
            make.height.offset(34);
        }];
        UILabel* label2=[UILabel new];
        label2.font=[UIFont systemFontOfSize:15];
        label2.text=@"分享推荐语:";
        label2.textColor=Black102;
        [shareView addSubview:label2];
        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_textField.mas_bottom).offset(10);
            make.left.offset(10);
            make.height.offset(25);
        }];
        _textView=[[UITextView alloc]init];
        _textView.backgroundColor=COLORGROUP;
        _textView.layer.cornerRadius=6;
        _textView.clipsToBounds=YES;
        _textView.font=[UIFont systemFontOfSize:15];
        _textView.text=content;
        [shareView addSubview:_textView];
        [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(label2.mas_bottom).offset(6);
            make.left.offset(10);
            make.right.offset(-10);
            make.height.offset(100);
        }];
        
        UIButton* leftBtn=[UIButton new];
        [leftBtn setTitle:@"取消" forState:UIControlStateNormal];
        [leftBtn setBackgroundColor:LOGOCOLOR];
        [leftBtn addTarget:self action:@selector(clickCancle) forControlEvents:UIControlEventTouchUpInside];
        [shareView addSubview:leftBtn];
        [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.offset(0);
            make.height.offset(45);
            make.width.offset(130);
        }];
        
        UIButton* rightBtn=[UIButton new];
        [rightBtn setTitle:@"确定" forState:UIControlStateNormal];
        [rightBtn setBackgroundColor:LOGOCOLOR];
        [rightBtn addTarget:self action:@selector(clickCommit) forControlEvents:UIControlEventTouchUpInside];
        [shareView addSubview:rightBtn];
        [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.offset(0);
            make.height.offset(45);
            make.width.offset(130);
        }];
        
        if (THEMECOLOR==2) {
            [leftBtn setTitleColor:Black51 forState:UIControlStateNormal];
            [rightBtn setTitleColor:Black51 forState:UIControlStateNormal];
        }
        //小圆圈
        for (int i=0; i<11; i++) {
            UIView* cornerView=[UIView new];
            cornerView.backgroundColor=[UIColor whiteColor];
            cornerView.layer.cornerRadius=1.2;
            cornerView.clipsToBounds=YES;
            [rightBtn addSubview:cornerView];
            [cornerView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(rightBtn.mas_left);
                make.top.offset((i+1)*1.5+i*2.4);
                make.height.width.offset(2.4);
            }];
        }

    }else{
        _backView.hidden=NO;
    }
    [_textField becomeFirstResponder];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    UIView* shareView=[[UIApplication sharedApplication].keyWindow viewWithTag:2000];
    if ([touch.view isDescendantOfView:shareView] ) {
        return NO;
    }
    return YES;
}

-(void)clickBackView{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

-(void)clickCancle{
    _backView.hidden=YES;
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

-(void)clickCommit{
    _backView.hidden=YES;
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    NSInteger count=0;
    NSString* goodsIds=@"";
    for (int i=0; i<_goodsModel.count; i++) {
        GoodsModel * model = _goodsModel[i];
        if ([model.ormdSelect isEqualToString:@"1"]) {
            if ([goodsIds isEqualToString:@""]) {
                goodsIds=FORMATSTR(@"%@",model.goodsId);
            }else{
                goodsIds=FORMATSTR(@"%@,%@",goodsIds,model.goodsId);
            }
            count++;
        }
    }
    
    [NetRequest requestTokenURL:url_recommmend_shareWeb parameter:@{@"goodsIds":goodsIds} success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSUDSaveData(_textField.text, @"shareTitle")
            NSUDSaveData(_textView.text, @"shareContent")
            NSString* shareUrl=nwdic[@"domain"];
            PersonalModel * model = [[PersonalModel alloc]initWithDic: NSUDTakeData(UserInfo) ];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.headImg]]];
            [SharePopoverVC shareUrl:shareUrl title:_textField.text.length>0?_textField.text:APPNAME des:_textView.text.length>0?_textView.text:APPNAME thubImg:image==nil?[UIImage imageNamed:@"logo"]:image shareType:1];
            
        }else{
            MBHideHUD
            SHOWMSG(nwdic[@"result"])
        }

    } failure:^(NSString *failure) {
        MBHideHUD
    }];
}


#pragma mark --> 请求官方推荐商品
-(void)requestSKData:(NSInteger)tag{
    
    NSMutableDictionary* par=[NSMutableDictionary dictionary];
    if (tag==100) {
        par=_parameter;
        _pageNum=1;
    }
    if (tag==101) {
        par=_parameter2;
        _pageNum2=1;
    }
    if (tag==102) {
        par=_parameter3;
        _pageNum3=1;
    }
    par[@"st"]=@"1";
    UICollectionView* collectionView=[self.scrollerView viewWithTag:tag];
    [collectionView.mj_footer resetNoMoreData];
    [NetRequest requestTokenURL:url_recommmend_category parameter:par success:^(NSDictionary *nwdic) {
        if (tag==100) {
            if (_dataArray1.count>0) {
                [_dataArray1 removeAllObjects];
                
                [collectionView reloadData];
            }
            
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSArray * ary = NULLARY( nwdic[@"list"] ) ;
                if (ary.count>0) {
                    [ary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                        GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                        [_dataArray1 addObject:model];
                    }];
                }else{
//                    SHOWMSG(@"敬请期待!")
                }
            }else{
                SHOWMSG(nwdic[@"result"])
            }
            
            for (int i=0; i<_goodsModel.count; i++) {
                GoodsModel* model=_goodsModel[i];
                for (int j=0; j<_dataArray1.count; j++) {
                    GoodsModel* data=_dataArray1[j];
                    if ([data.goodsId isEqualToString:model.goodsId]) {
                        data.ormdSelect=@"1";
                        break;
                    }
                }
            }
  
        }
        
        if (tag==101) {
            if (_dataArray2.count>0) {
                [_dataArray2 removeAllObjects];
                [collectionView reloadData];
            }
            
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSArray * ary = NULLARY( nwdic[@"list"] ) ;
                if (ary.count>0) {
                    [ary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                        GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                        [_dataArray2 addObject:model];
                    }];
                }else{
//                    SHOWMSG(@"敬请期待!")
                }
                
            }else{
                
                SHOWMSG(nwdic[@"result"])
                
            }
            for (int i=0; i<_goodsModel.count; i++) {
                GoodsModel* model=_goodsModel[i];
                for (int j=0; j<_dataArray2.count; j++) {
                    GoodsModel* data=_dataArray2[j];
                    if ([data.goodsId isEqualToString:model.goodsId]) {
                        data.ormdSelect=@"1";
                        break;
                    }
                }
            }
        }
        if (tag==102) {
            if (_dataArray3.count>0) {
                [_dataArray3 removeAllObjects];
                [collectionView reloadData];
            }
            
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSArray * ary = NULLARY( nwdic[@"list"] ) ;
                if (ary.count>0) {
                    [ary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                        GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                        [_dataArray3 addObject:model];
                    }];
                }else{
//                    SHOWMSG(@"敬请期待!")
                }
            }else{
                SHOWMSG(nwdic[@"result"])
            }
            for (int i=0; i<_goodsModel.count; i++) {
                GoodsModel* model=_goodsModel[i];
                for (int j=0; j<_dataArray3.count; j++) {
                    GoodsModel* data=_dataArray3[j];
                    if ([data.goodsId isEqualToString:model.goodsId]) {
                        data.ormdSelect=@"1";
                        break;
                    }
                }
            }
        }
        
        [collectionView.mj_header endRefreshing];
        
        [collectionView reloadData];
        
    } failure:^(NSString *failure) {
        [collectionView.mj_header endRefreshing];
        SHOWMSG(failure)
    }];
}

-(void)loadMoreSKData:(NSInteger)tag{
    NSMutableDictionary* par=[NSMutableDictionary dictionary];
    if (tag==100) {
        _pageNum++;
        par=_parameter;
        par[@"st"]=FORMATSTR(@"%ld",(long)_pageNum);
    }
    if (tag==101) {
        _pageNum2++;
        par=_parameter2;
        par[@"st"]=FORMATSTR(@"%ld",(long)_pageNum2);
    }
    if (tag==102) {
        _pageNum3++;
        par=_parameter3;
        par[@"st"]=FORMATSTR(@"%ld",(long)_pageNum3);
    }
    UICollectionView* collectionView=[self.scrollerView viewWithTag:tag];
    [NetRequest requestTokenURL:url_recommmend_category parameter:par success:^(NSDictionary *nwdic) {
        if (tag==100) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSArray * ary = nwdic[@"list"];
                if (ary.count>0) {
                    [collectionView.mj_footer endRefreshing];
                    [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                        [_dataArray1 addObject:model];
                    }];
                    [collectionView reloadData];
                }else{
                    [collectionView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                [collectionView.mj_footer endRefreshing];
                SHOWMSG(nwdic[@"result"])
            }
        }
        if (tag==101) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSArray * ary = nwdic[@"list"];
                if (ary.count>0) {
                    [collectionView.mj_footer endRefreshing];
                    [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                        [_dataArray2 addObject:model];
                    }];
                    [collectionView reloadData];
                }else{
                    [collectionView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                [collectionView.mj_footer endRefreshing];
                SHOWMSG(nwdic[@"result"])
            }
        }
        if (tag==102) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                NSArray * ary = nwdic[@"list"];
                if (ary.count>0) {
                    [collectionView.mj_footer endRefreshing];
                    [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                        [_dataArray3 addObject:model];
                    }];
                    [collectionView reloadData];
                }else{
                    [collectionView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                [collectionView.mj_footer endRefreshing];
                SHOWMSG(nwdic[@"result"])
            }
        }
        
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [collectionView.mj_footer endRefreshing];
    }];
}




#pragma mark --> UICollectionView代理

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView.tag==100) { return _dataArray1.count;  }
    if (collectionView.tag==101) { return _dataArray2.count; }
    if (collectionView.tag==102) { return _dataArray3.count; }
    if (collectionView.tag==200) { return _goodsImage.count; }
    return 0;
    
}

-(void)clickDelectPic:(UIButton*)btn{
    
    GoodsModel* model=_goodsModel[btn.tag];
    for (int j=0; j<_dataArray1.count; j++) {
        GoodsModel* data=_dataArray1[j];
        if ([model.goodsId isEqualToString:data.goodsId]) {
            NSLog(@"goodsId = %@ %@",model.goodsId,data.goodsId);
            data.ormdSelect=@"0";
        }
    }
    for (int j=0; j<_dataArray2.count; j++) {
        GoodsModel* data=_dataArray2[j];
        if ([model.goodsId isEqualToString:data.goodsId]) {
            data.ormdSelect=@"0";
        }
    }
    for (int j=0; j<_dataArray3.count; j++) {
        GoodsModel* data=_dataArray3[j];
        if ([model.goodsId isEqualToString:data.goodsId]) {
            data.ormdSelect=@"0";
        }
    }
    
    
    [_goodsImage removeObjectAtIndex:btn.tag];
    [_goodsModel removeObjectAtIndex:btn.tag];
    UICollectionView* collectionView=[self.scrollerView viewWithTag:100];
    UICollectionView* collectionView2=[self.scrollerView viewWithTag:101];
    UICollectionView* collectionView3=[self.scrollerView viewWithTag:102];
    [_pictureView reloadData];
    [collectionView reloadData];
    [collectionView2 reloadData];
    [collectionView3 reloadData];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    [_numberPicBtn setTitle:[NSString stringWithFormat:@"查看已勾选商品（%ld）",(unsigned long)_goodsImage.count] forState:UIControlStateNormal];
    
    if (collectionView==_pictureView) {
        PictureCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PictureCell" forIndexPath:indexPath];
        cell.image=_goodsImage[indexPath.row];
        cell.escBtn.tag=indexPath.row;
        [cell.escBtn addTarget:self action:@selector(clickDelectPic:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    
    NewOrmdCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewOrmdCell" forIndexPath:indexPath];
    cell.checkBtn.tag=indexPath.row;
    cell.shareBtn.tag=indexPath.row;
    if (collectionView.tag==100) {
        cell.ormdModel = _dataArray1[indexPath.row];
        [cell.checkBtn addTarget:self action:@selector(clickCheck:) forControlEvents:UIControlEventTouchUpInside];
        [cell.shareBtn addTarget:self action:@selector(clickExtend:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (collectionView.tag==101) {
        cell.ormdModel = _dataArray2[indexPath.row];
        [cell.checkBtn addTarget:self action:@selector(clickCheck2:) forControlEvents:UIControlEventTouchUpInside];
        [cell.shareBtn addTarget:self action:@selector(clickExtend2:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (collectionView.tag==102) {
        cell.ormdModel = _dataArray3[indexPath.row];
        [cell.checkBtn addTarget:self action:@selector(clickCheck3:) forControlEvents:UIControlEventTouchUpInside];
        [cell.shareBtn addTarget:self action:@selector(clickExtend3:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell ;
}

-(void)clickExtend:(UIButton*)btn{
    GoodsModel * model = _dataArray1[btn.tag];
    [NetRequest requestTokenURL:url_goods_pics parameter:@{@"goodsId":model.goodsId} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray * ary = NULLARY( nwdic[@"images"] ) ;
            NSMutableArray* array=[NSMutableArray array];
            [array addObjectsFromArray:ary];
            [PageJump shareGoods:model goodsImgAry:array];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

-(void)clickExtend2:(UIButton*)btn{
    GoodsModel * model = _dataArray2[btn.tag];
    [NetRequest requestTokenURL:url_goods_pics parameter:@{@"goodsId":model.goodsId} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray * ary = NULLARY( nwdic[@"images"] ) ;
            NSMutableArray* array=[NSMutableArray array];
            [array addObjectsFromArray:ary];
            [PageJump shareGoods:model goodsImgAry:array];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

-(void)clickExtend3:(UIButton*)btn{
    GoodsModel * model = _dataArray3[btn.tag];
    [NetRequest requestTokenURL:url_goods_pics parameter:@{@"goodsId":model.goodsId} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray * ary = NULLARY( nwdic[@"images"] ) ;
            NSMutableArray* array=[NSMutableArray array];
            [array addObjectsFromArray:ary];
            [PageJump shareGoods:model goodsImgAry:array];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    GoodsDetailsVC*detailVc = [[GoodsDetailsVC alloc]init] ;
    detailVc.hidesBottomBarWhenPushed = YES ;
    if(collectionView.tag==100){
        detailVc.dmodel = _dataArray1[indexPath.row] ;
        PushTo(detailVc, YES)
    }
    if(collectionView.tag==101){
        detailVc.dmodel = _dataArray2[indexPath.row] ;
        PushTo(detailVc, YES)
    }
    if(collectionView.tag==102){
        detailVc.dmodel = _dataArray3[indexPath.row] ;
        PushTo(detailVc, YES)
    }
    
}

#pragma mark --> UICollectionViewLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==_pictureView) {
        return CGSizeMake(ScreenW/4.5, ScreenW/4.5);
    }

    return  CGSizeMake(ScreenW, 110*HWB) ;

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, 0, 0, 0) ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}

-(void)clickCheck:(UIButton *)btn{
    UICollectionView* collectionView=[self.scrollerView viewWithTag:100];
    if (_goodsImage.count==9&&btn.selected==NO) {
        SHOWMSG(@"一次最多只能分享9个商品哦~")
        return;
    }
    
    btn.selected=!btn.selected;
    GoodsModel * model = _dataArray1[btn.tag];
    NSIndexPath* indexPath=[NSIndexPath indexPathForRow:btn.tag inSection:0];
    NewOrmdCell * cell = (NewOrmdCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if (btn.selected==YES) {
        model.ormdSelect=@"1";
        if (cell.goodsImgV.image==nil) {
            SHOWMSG(@"当前图片未下载完成或下载失败，请稍后再选!")
        }else{
            [_goodsImage addObject:cell.goodsImgV.image];
            [_goodsModel addObject:cell.ormdModel];
            [_pictureView reloadData];
        }
        
    }else{
        model.ormdSelect=@"0";
        if (cell.goodsImgV.image==nil) {
            SHOWMSG(@"当前图片未下载完成或下载失败，请稍后再选!")
        }else{
            [_goodsImage removeObject:cell.goodsImgV.image];
            [_goodsModel removeObject:cell.ormdModel];
            [_pictureView reloadData];
        }
    }
    
    [UIView performWithoutAnimation:^{
        [collectionView reloadItemsAtIndexPaths:@[indexPath]];
    }];
    NSLog(@"执行完了");
}

-(void)clickCheck2:(UIButton *)btn{
    UICollectionView* collectionView=[self.scrollerView viewWithTag:101];
    if (_goodsImage.count==9&&btn.selected==NO) {
        SHOWMSG(@"一次最多只能分享9个商品哦~")
        return;
    }
    
    btn.selected=!btn.selected;
    GoodsModel * model = _dataArray2[btn.tag];
    
    NSIndexPath* indexPath=[NSIndexPath indexPathForRow:btn.tag inSection:0];
    NewOrmdCell * cell = (NewOrmdCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if (btn.selected==YES) {
        model.ormdSelect=@"1";
        if (cell.goodsImgV.image==nil) {
            SHOWMSG(@"当前图片未下载完成或下载失败，请稍后再选!")
        }else{
            [_goodsImage addObject:cell.goodsImgV.image];
            [_goodsModel addObject:cell.ormdModel];
            [_pictureView reloadData];
        }
        
    }else{
        model.ormdSelect=@"0";
        if (cell.goodsImgV.image==nil) {
            SHOWMSG(@"当前图片未下载完成或下载失败，请稍后再选!")
        }else{
            [_goodsImage removeObject:cell.goodsImgV.image];
            [_goodsModel removeObject:cell.ormdModel];
            [_pictureView reloadData];
        }
    }
    
    [UIView performWithoutAnimation:^{
        [collectionView reloadItemsAtIndexPaths:@[indexPath]];
    }];
    
}

-(void)clickCheck3:(UIButton *)btn{
    UICollectionView* collectionView=[self.scrollerView viewWithTag:102];
    if (_goodsImage.count==9&&btn.selected==NO) {
        SHOWMSG(@"一次最多只能分享9个商品哦~")
        return;
    }
    
    btn.selected=!btn.selected;
    GoodsModel * model = _dataArray3[btn.tag];
    
    NSIndexPath* indexPath=[NSIndexPath indexPathForRow:btn.tag inSection:0];
    NewOrmdCell * cell = (NewOrmdCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if (btn.selected==YES) {
        model.ormdSelect=@"1";
        if (cell.goodsImgV.image==nil) {
            SHOWMSG(@"当前图片未下载完成或下载失败，请稍后再选!")
        }else{
            [_goodsImage addObject:cell.goodsImgV.image];
            [_goodsModel addObject:cell.ormdModel];
            [_pictureView reloadData];
        }
        
    }else{
        model.ormdSelect=@"0";
        if (cell.goodsImgV.image==nil) {
            SHOWMSG(@"当前图片未下载完成或下载失败，请稍后再选!")
        }else{
            [_goodsImage removeObject:cell.goodsImgV.image];
            [_goodsModel removeObject:cell.ormdModel];
            [_pictureView reloadData];
        }
    }
    
    [UIView performWithoutAnimation:^{
        [collectionView reloadItemsAtIndexPaths:@[indexPath]];
    }];
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    ResignFirstResponder
}


@end

