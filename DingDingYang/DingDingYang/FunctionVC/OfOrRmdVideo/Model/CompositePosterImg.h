//
//  CompositePosterImg.h
//  DingDingYang
//
//  Created by ddy on 2017/12/16.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompositePosterImg : NSObject


+(void)goodsModeAry:(NSMutableArray*)goodsModel imgAry:(NSMutableArray*)imgAry posterImg:(void(^)(NSMutableArray * posterAry))posterImgBlock failState:(void(^)(NSInteger fresult))failBlock ;

+(NSMutableArray*)compositePosterByGoodsModel:(NSMutableArray*)aryModel goodsImg:(NSMutableArray*)imgAry ;


+(void)goodsModel:(GoodsModel*)model goodsImg:(UIImage*)img posterImg:(void(^)(UIImage * posterImg))posterImgBlock failState:(void(^)(NSInteger fresult))failBlock;

@end



@interface QuanView : UIView
+(UIImage*)quanImg:(NSString*)money breadth:(float)breadth;
@property(nonatomic,strong) UILabel * quanMoneyLab ;
@end

@interface BaoYouLab : UILabel
+(UIImage*)baoYouImg;
@end


@interface EwmKuangImg : UIImageView
+(UIImage*)ewmKuangImg;
@end




