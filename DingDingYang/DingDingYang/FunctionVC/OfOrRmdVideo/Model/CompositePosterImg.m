//
//  CompositePosterImg.m
//  DingDingYang
//
//  Created by ddy on 2017/12/16.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define quanHi  16

#import "CompositePosterImg.h"


@implementation CompositePosterImg

// 多张
+(void)goodsModeAry:(NSMutableArray*)goodsModel imgAry:(NSMutableArray*)imgAry posterImg:(void(^)(NSMutableArray * posterAry))posterImgBlock failState:(void(^)(NSInteger fresult))failBlock{
    MBShow(@"正在生成...")
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSMutableArray * goodsPosterImgAry = [CompositePosterImg compositePosterByGoodsModel:goodsModel goodsImg:imgAry];
        dispatch_async(dispatch_get_main_queue(), ^{
            MBHideHUD
            posterImgBlock(goodsPosterImgAry);
        });
    });
}
+(NSMutableArray*)compositePosterByGoodsModel:(NSMutableArray*)aryModel goodsImg:(NSMutableArray*)imgAry{
    
    @try {
        @autoreleasepool {
            
            NSMutableArray * goodsPosterImgAry = [NSMutableArray array];
            CGSize imgSize = CGSizeMake(350, 508) ;
            UIImage * imgKuang = [EwmKuangImg ewmKuangImg] ;
            
            for ( NSInteger i=0; i<aryModel.count; i++) {
            
                GoodsModel * goodsModel = aryModel[i] ;
                UIGraphicsPopContext();
                UIGraphicsBeginImageContextWithOptions(imgSize,YES,[UIScreen mainScreen].scale);
                
                // 设置个白色背景
                UIImage * bgImg = [UIImage imageNamed:@"white_bg"];
                [bgImg drawInRect:CGRectMake(0, 0, 350, 508)];
                
                // 商品主图
                UIImage * goodsImg = imgAry[i];
                [goodsImg drawInRect:CGRectMake(0, 0,350, 350)];
            
                // 商品标题
                NSString * goodsName = FORMATSTR(@" %@",goodsModel.goodsName) ; // 商品标题前面加两个空格
                NSRange goodsNameRg =  {0,goodsName.length} ;
                NSMutableAttributedString * attrGoodsName = [[NSMutableAttributedString alloc] initWithString:goodsName];
                [attrGoodsName addAttributes:@{NSForegroundColorAttributeName:Black51} range:goodsNameRg];
                [attrGoodsName addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} range:goodsNameRg];
                NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                [paragraphStyle setLineSpacing:3];
                [attrGoodsName addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:goodsNameRg];
                
                // 淘宝天猫图片
                if ([goodsModel.platform containsString:@"天猫"]||[goodsModel.platform containsString:@"淘宝"]) {
                    NSTextAttachment * baoYoutext = [[NSTextAttachment alloc] init];
                    baoYoutext.bounds = CGRectMake(0, -3, 16, 16);
                    if ([goodsModel.platform containsString:@"天猫"]) {
                        baoYoutext.image = [UIImage imageNamed:@"tmallicon"];
                    }else{
                        baoYoutext.image = [UIImage imageNamed:@"tbicon"]; // 淘宝
                    }
                    NSAttributedString * attrBaoYou = [NSAttributedString attributedStringWithAttachment:baoYoutext];
                    [attrGoodsName insertAttributedString:attrBaoYou atIndex:0];
                }
                [attrGoodsName drawInRect:CGRectMake(15, 370, 200, 66)];
                
                //商品价格(券后价)
                NSRange rg1 =  {0,1} ;
                NSRange rg2 =  {1,goodsModel.endPrice.length} ;
                NSMutableAttributedString * endPrice = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",goodsModel.endPrice)];
                [endPrice addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:rg1];
                [endPrice addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:rg2];
                if (THEMECOLOR==2) {
                    [endPrice addAttributes:@{NSForegroundColorAttributeName:RGBA(255, 80, 0, 1)} range:rg1];
                    [endPrice addAttributes:@{NSForegroundColorAttributeName:RGBA(255, 80, 0, 1)} range:rg2];
                }
                [endPrice addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} range:rg1];
                [endPrice addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20]} range:rg2];
                float endpW = StringSize(FORMATSTR(@"￥%@",goodsModel.endPrice), 20).width + 5 ;
                [endPrice drawInRect:CGRectMake(15, 460, endpW, 20)];
                
                // 有券、画券和原价
                if ([goodsModel.couponMoney floatValue]>0) {
                    float qmW = StringSize(FORMATSTR(@"￥%@",goodsModel.couponMoney), 14).width + 10 ;
                    UIImage * img = [QuanView quanImg:FORMATSTR(@"￥%@",goodsModel.couponMoney) breadth:qmW+quanHi] ;
                    [img drawInRect:CGRectMake(15, 436, quanHi+qmW, quanHi)];

                    NSString * oldPrice = FORMATSTR(@" ￥%@ ",goodsModel.price) ;
                    NSMutableAttributedString * attrOldPrice = [[NSMutableAttributedString alloc] initWithString:oldPrice];
                    [attrOldPrice addAttributes:@{NSForegroundColorAttributeName:Black102} range:NSMakeRange(0, oldPrice.length)];
                    [attrOldPrice addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, oldPrice.length)];
                    [attrOldPrice drawInRect:CGRectMake(endpW+16,467,StringSize(oldPrice,15).width+5, 15)];
                }
                
                // 二维码框
                [imgKuang drawInRect:CGRectMake(225,370,110,110)];
                
                // 二维码
                UIImage * ewmImg = EWMImageWithString(goodsModel.url) ;
                [ewmImg drawInRect:CGRectMake(230, 375,100,100)];
                
                // 长按识别二维码
                NSMutableAttributedString * attrhEwmStr = [[NSMutableAttributedString alloc]initWithString:@"长按识别二维码"] ;
                [attrhEwmStr addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:NSMakeRange(0, attrhEwmStr.length)];
                if (THEMECOLOR==2) {
                    [attrhEwmStr addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} range:NSMakeRange(0, attrhEwmStr.length)];
                }
                [attrhEwmStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10.5]} range:NSMakeRange(0, attrhEwmStr.length)];
                [attrhEwmStr drawInRect:CGRectMake(242, 475, 100, 13)];
                
                //获取
                UIImage * resultImage = UIGraphicsGetImageFromCurrentImageContext();
                NSData *pre = UIImageJPEGRepresentation(resultImage, 0.5);
                UIImage *resultingImage = [UIImage imageWithData:pre];
                [goodsPosterImgAry addObject:resultingImage];
                
                NSData * data = UIImageJPEGRepresentation(resultingImage, 1);
                NSUInteger length = data.length ; float fl = [FORMATSTR(@"%lu",(unsigned long)length) floatValue];
                NSLog(@"合成的图片大小:约为：%.2fkb,%.2fM",fl/1000,fl/1000/1000);

                UIGraphicsEndImageContext();

            }
            
            if (goodsPosterImgAry.count>0) {
                return goodsPosterImgAry ;
            }
        }
    } @catch (NSException * exception) {
        return nil ;
    } @finally {
        
    }
}


#pragma mark ===>>> 单张
+(void)goodsModel:(GoodsModel*)model goodsImg:(UIImage*)img posterImg:(void(^)(UIImage * posterImg))posterImgBlock failState:(void(^)(NSInteger fresult))failBlock{
//    MBShow(@"正在合成...")
   
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        @try {
            @autoreleasepool {
                
                CGSize imgSize = CGSizeMake(350, 508) ;
                UIImage * imgKuang = [EwmKuangImg ewmKuangImg] ;
                GoodsModel * goodsModel = model ;
                UIGraphicsPopContext();
                UIGraphicsBeginImageContextWithOptions(imgSize,YES,[UIScreen mainScreen].scale);
                
                // 设置个白色背景
                UIImage * bgImg = [UIImage imageNamed:@"white_bg"];
                [bgImg drawInRect:CGRectMake(0, 0, 350, 508)];
                
                // 商品主图
                UIImage * goodsImg = img;
                [goodsImg drawInRect:CGRectMake(0, 0,350, 350)];
                
                // 商品标题
                NSString * goodsName = FORMATSTR(@" %@",goodsModel.goodsName) ; // 商品标题前面加两个空格
                NSRange goodsNameRg =  {0,goodsName.length} ;
                NSMutableAttributedString * attrGoodsName = [[NSMutableAttributedString alloc] initWithString:goodsName];
                [attrGoodsName addAttributes:@{NSForegroundColorAttributeName:Black51} range:goodsNameRg];
                [attrGoodsName addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} range:goodsNameRg];
                NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                [paragraphStyle setLineSpacing:3];
                [attrGoodsName addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:goodsNameRg];
                
                // 淘宝天猫图片
                if ([goodsModel.platform containsString:@"天猫"]||[goodsModel.platform containsString:@"淘宝"]||[goodsModel.platform containsString:@"唯品会"]) {
                    NSTextAttachment * baoYoutext = [[NSTextAttachment alloc] init];
                    baoYoutext.bounds = CGRectMake(0, -3, 16, 16);
                    if ([goodsModel.platform containsString:@"天猫"]) {
                        baoYoutext.image = [UIImage imageNamed:@"tmallicon"];
                    }else if([goodsModel.platform containsString:@"唯品会"]){
                        baoYoutext.image = [UIImage imageNamed:@"wphIcon"]; // 唯品会
                    }else{
                        baoYoutext.image = [UIImage imageNamed:@"tbicon"]; // 淘宝
                    }
                    NSAttributedString * attrBaoYou = [NSAttributedString attributedStringWithAttachment:baoYoutext];
                    [attrGoodsName insertAttributedString:attrBaoYou atIndex:0];
                }
                [attrGoodsName drawInRect:CGRectMake(15,  370, 200, 66)];
                
                //商品价格(券后价)
                NSRange rg1 =  {0,1} ;
                NSRange rg2 =  {1,goodsModel.endPrice.length} ;
                NSMutableAttributedString * endPrice = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",goodsModel.endPrice)];
                [endPrice addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:rg1];
                [endPrice addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:rg2];
                if (THEMECOLOR==2) {
                    [endPrice addAttributes:@{NSForegroundColorAttributeName:RGBA(255, 80, 0, 1)} range:rg1];
                    [endPrice addAttributes:@{NSForegroundColorAttributeName:RGBA(255, 80, 0, 1)} range:rg2];
                }
                [endPrice addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} range:rg1];
                [endPrice addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20]} range:rg2];
                float endpW = StringSize(FORMATSTR(@"￥%@",goodsModel.endPrice), 20).width + 5 ;
                [endPrice drawInRect:CGRectMake(15, 460, endpW, 20)];
                
                // 有券、画券和原价
                if ([goodsModel.couponMoney floatValue]>0) {
                    float qmW = StringSize(FORMATSTR(@"￥%@",goodsModel.couponMoney), 14).width + 10 ;
                    UIImage * img = [QuanView quanImg:FORMATSTR(@"￥%@",goodsModel.couponMoney) breadth:qmW+quanHi] ;
                    [img drawInRect:CGRectMake(15, 436, quanHi+qmW, quanHi)];
                    
                    NSString * oldPrice = FORMATSTR(@" ￥%@ ",goodsModel.price) ;
                    NSMutableAttributedString * attrOldPrice = [[NSMutableAttributedString alloc] initWithString:oldPrice];
                    [attrOldPrice addAttributes:@{NSForegroundColorAttributeName:Black102} range:NSMakeRange(0, oldPrice.length)];
                    [attrOldPrice addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, oldPrice.length)];
                    [attrOldPrice drawInRect:CGRectMake(endpW+16,467,StringSize(oldPrice,15).width+5, 15)];
                }
                
                // 二维码框
                [imgKuang drawInRect:CGRectMake(225,370,110,110)];
                
                // 二维码
                UIImage * ewmImg = EWMImageWithString(goodsModel.url) ;
                [ewmImg drawInRect:CGRectMake(230, 375,100,100)];
                
                // 长按识别二维码
                NSMutableAttributedString * attrhEwmStr = [[NSMutableAttributedString alloc]initWithString:@"长按识别二维码"] ;
                [attrhEwmStr addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:NSMakeRange(0, attrhEwmStr.length)];
                if (THEMECOLOR==2) {
                    [attrhEwmStr addAttributes:@{NSForegroundColorAttributeName:RGBA(255, 80, 0, 1)} range:NSMakeRange(0, attrhEwmStr.length)];
                }
                [attrhEwmStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10.5]} range:NSMakeRange(0, attrhEwmStr.length)];
                [attrhEwmStr drawInRect:CGRectMake(242, 475, 100, 13)];
                
//                //商品推荐语
//                if (model.desStr.length>0) {
//                    NSMutableAttributedString * label = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" 商品推荐:  %@",model.desStr]] ;
//                    [label addAttributes:@{NSBackgroundColorAttributeName:RGBA(255, 80, 0, 1)} range:NSMakeRange(0, 7)];
//                    [label addAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} range:NSMakeRange(0, 7)];
//                    [label addAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} range:NSMakeRange(7, attrhEwmStr.length-7)];
//                    [label addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.5]} range:NSMakeRange(7, attrhEwmStr.length-7)];
//                    [label drawInRect:CGRectMake(15, 495, 320, 60)];
//                    [label addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrhEwmStr.length)];
//                }
                
                //获取
                UIImage * resultingImage = UIGraphicsGetImageFromCurrentImageContext();
                //            [goodsPosterImgAry addObject:resultingImage];
                
                NSData * data = UIImageJPEGRepresentation(resultingImage, 1);
                NSUInteger length = data.length ; float fl = [FORMATSTR(@"%lu",(unsigned long)length) floatValue];
                NSLog(@"合成的图片大小:约为：%.2fkb,%.2fM",fl/1000,fl/1000/1000);
                
                UIGraphicsEndImageContext();
                dispatch_async(dispatch_get_main_queue(), ^{
//                    MBHideHUD
                    if (resultingImage) {
                        posterImgBlock(resultingImage);
                    }else{
                        failBlock(1);
                    }
                });
            }
        } @catch (NSException * exception) {
            failBlock(1);
        } @finally {
            
        }
        
        
       
    });

            
}


@end









@implementation QuanView

+(UIImage*)quanImg:(NSString *)money breadth:(float)breadth{
    QuanView * quan = [[QuanView alloc]initWithFrame:CGRectMake(0, 0, breadth, quanHi)];
    quan.quanMoneyLab.text = money ;
    quan.quanMoneyLab.frame = CGRectMake(quanHi, 0, breadth-quanHi-2, quanHi) ;
    UIGraphicsBeginImageContextWithOptions(quan.bounds.size, NO, 3);
    [quan.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image ;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = COLORWHITE  ;
        self.clipsToBounds = YES ;
        self.layer.masksToBounds = YES ;
        self.layer.cornerRadius = 4 ;
        self.layer.borderWidth = 1 ;
        self.layer.borderColor = LOGOCOLOR.CGColor ;
        UILabel * quanLab = [UILabel labText:@"券" color:COLORWHITE font:11];
        quanLab.backgroundColor = LOGOCOLOR ;

        quanLab.textAlignment = NSTextAlignmentCenter ;
        quanLab.frame = CGRectMake(0, 0, quanHi, quanHi);
        [self addSubview:quanLab];
        
        _quanMoneyLab = [UILabel labText:@"￥0" color:LOGOCOLOR font:14];
        if (THEMECOLOR==2) {
            self.layer.borderColor = RGBA(255, 80, 0, 1).CGColor ;
            quanLab.backgroundColor = RGBA(255, 80, 0, 1) ;
            _quanMoneyLab.textColor = RGBA(255, 80, 0, 1) ;
        }
        _quanMoneyLab.textAlignment = NSTextAlignmentCenter ;
        _quanMoneyLab.frame = CGRectMake(quanHi, 0, 36, quanHi);
        _quanMoneyLab.backgroundColor = COLORWHITE ;
        [self addSubview:_quanMoneyLab];
    }
    return self ;
}

@end


@implementation BaoYouLab

+(UIImage*)baoYouImg{
    
    BaoYouLab * baoyou = [[BaoYouLab alloc]initWithFrame:CGRectMake(0, 0, 30, 14)];
    UIGraphicsBeginImageContextWithOptions(baoyou.bounds.size, NO, 3);
    [baoyou.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image ;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = LOGOCOLOR  ;
        self.layer.masksToBounds = YES ;
        self.layer.cornerRadius = 4 ;
        self.text = @"包邮" ;
        self.textColor = COLORWHITE ;
        self.font = [UIFont systemFontOfSize:11];
        self.textAlignment = NSTextAlignmentCenter ;
    }
    return self ;
}

@end



@implementation EwmKuangImg

+(UIImage*)ewmKuangImg{

    EwmKuangImg * ewmK = [[EwmKuangImg alloc]initWithFrame:CGRectMake(0, 0, 110, 110)];
    UIGraphicsBeginImageContextWithOptions(ewmK.bounds.size, NO, 3);
    [ewmK.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image ;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.tintColor = LOGOCOLOR ;
        if (THEMECOLOR==2) {
            self.tintColor = RGBA(255, 80, 0, 1) ;
        }
        self.image =  [[UIImage imageNamed:@"goods_poster_ewmKuang"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)] ;
    }
    return self ;
}

@end

