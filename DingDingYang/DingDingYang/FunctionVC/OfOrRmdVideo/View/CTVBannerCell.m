//
//  CTVBannerCell.m
//  DingDingYang
//
//  Created by ddy on 2017/6/24.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "CTVBannerCell.h"

@implementation CTVBannerCell


-(void)setBannerAry:(NSMutableArray *)bannerAry{
    _bannerAry = bannerAry ;

    NSMutableArray * imgurlAry = [[NSMutableArray alloc]init];
    for (NSInteger i =0; i<_bannerAry.count; i++) {
        BHModel*model = _bannerAry[i] ;
        NSString*url=FORMATSTR(@"%@%@",dns_dynamic_loadimg,model.img);
        [imgurlAry addObject:url];
    }
    _csView.imageURLStringsGroup=imgurlAry;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.layer.masksToBounds = YES ;
        self.contentView.layer.cornerRadius = 6 ;
//        _imgUrlAry = [NSMutableArray array];
        _csView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0,0, ScreenW-8,ScreenW/2.8) imageURLStringsGroup:nil];
        _csView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _csView.delegate = self;
        _csView.currentPageDotColor =LOGOCOLOR;
        _csView.pageDotColor=COLORWHITE;
        _csView.autoScrollTimeInterval=3.0;
        [self.contentView addSubview:_csView];
    }
    return self ;
}

//轮播图代理
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    BHModel * model = _bannerAry[index];
    
    if (TopNavc!=nil) {
        if (model.todo==1) {
            GoodsDetailsVC*detailVc = [[GoodsDetailsVC alloc]init] ;
            detailVc.hidesBottomBarWhenPushed = YES ;
            detailVc.dmodel = model.goodsMd ;
            [TopNavc pushViewController:detailVc animated:YES];
        }
        if (model.todo==2) {
            if ([model.link containsString:@"http"]) {
                WebVC * web = [[WebVC alloc]init];
                web.urlWeb = model.link ;
                web.nameTitle = @"详情页" ;
                web.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:web animated:YES];
            }
        }
    }
    
}


@end
