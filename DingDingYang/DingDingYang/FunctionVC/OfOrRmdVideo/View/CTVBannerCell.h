//
//  CTVBannerCell.h
//  DingDingYang
//
//  Created by ddy on 2017/6/24.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "BHModel.h"
@interface CTVBannerCell : UICollectionViewCell <SDCycleScrollViewDelegate>

@property(nonatomic,strong) SDCycleScrollView * csView ;
@property(nonatomic,strong) NSMutableArray * bannerAry ;
//@property(nonatomic,strong) NSMutableArray * imgUrlAry;

@end
