//
//  PictureCell.m

//
//  Created by ddy on 2017/12/20.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "PictureCell.h"

@implementation PictureCell

-(void)setImage:(UIImage *)image{
    _image=image;
    _goodsImgV.image=image;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self goodsImgV];
        [self escBtn];
        
    }
    return self;
}


-(UIImageView *)goodsImgV{
    if (_goodsImgV==nil) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5);
            make.top.offset(5);
            make.right.offset(-5);
            make.bottom.offset(-5);
            
        }];
    }
    return _goodsImgV;
}

-(UIButton *)escBtn{
    if (_escBtn==nil) {
        _escBtn=[UIButton new];
        [_escBtn setImage:[UIImage imageNamed:@"official_select_delete"] forState:UIControlStateNormal];
        [self.contentView addSubview:_escBtn];
        [_escBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.offset(0);
            make.right.offset(0);
            make.height.width.offset(20);
            
        }];
        
    }
    return _escBtn;
}

@end
