//
//  OrmdCell.m
//  DingDingYang
//
//  Created by ddy on 2017/5/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define SKH (ScreenW-12)/2

#import "OrmdCell.h"
#import "AVPalyerVC.h"
@implementation OrmdCell

-(void)setOrmdModel:(GoodsModel *)ormdModel{
    
    _ormdModel = ormdModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_ormdModel.goodsPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_ormdModel.smallPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
    _goodsTitle.text = _ormdModel.goodsName ;
    _salesLab.text = FORMATSTR(@"已售%@",_ormdModel.sales);
    
    
    if ([_ormdModel.couponMoney floatValue]>0) { // 有券
        _endCouponsLab.hidden = NO ;
        _endPriceLab.text = FORMATSTR(@"¥%@",_ormdModel.endPrice) ;
        [_couponsLab setTitle:FORMATSTR(@"券¥%@",_ormdModel.couponMoney) forState:UIControlStateNormal];
    }else{  // 无券
        _endCouponsLab.hidden = YES ;
        _endPriceLab.text = FORMATSTR(@"¥%@",_ormdModel.price) ;
        [_couponsLab setTitle:@"立即下单" forState:UIControlStateNormal];
    }

    if (_ormdModel.video.length>0&&[_ormdModel.video hasPrefix:@"http"]) {
        _playVideoBtn.hidden = NO ;
    }else{
        _playVideoBtn.hidden = YES ;
    }
    
}


-(NSMutableAttributedString*)attriStr:(NSString*)str range:(NSRange)strrange{
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:strrange];
    return dlStr;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
        
        [self playVideoBtn];
        
        [self couponsLab];
        
        [self goodsTitle];
    
        [self endPriceLab];
        
        [self endCouponsLab];
        
        [self salesLab];
        
    }
    return self;
}

//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.frame = CGRectMake(3, 3, SKH-6, SKH-6);
        _goodsImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}
-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _playVideoBtn.frame = CGRectMake((SKH-6)/2-22, (SKH-6)/2-22, 42*HWB, 42*HWB);
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_playVideoBtn];
        if (PID!=10088) { // 种草君不要视频播放
            [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        }else{
            _playVideoBtn.userInteractionEnabled = NO ;
        }
    }
    return _playVideoBtn ;
}
//商品标题
-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black102 font:13*HWB];
        _goodsTitle.textAlignment =NSTextAlignmentCenter ;
        _goodsTitle.frame = CGRectMake(1, SKH,SKH-2, 20);
        [self.contentView addSubview:_goodsTitle];
    }
    return _goodsTitle ;
}

//优惠券
-(UIButton*)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UIButton buttonWithType:UIButtonTypeCustom];
        [_couponsLab setBackgroundImage:[UIImage imageNamed:@"youhuiquan"] forState:UIControlStateNormal];
        [_couponsLab setTitle:@"券¥0" forState:UIControlStateNormal];
        [_couponsLab setTitleColor:COLORWHITE forState:UIControlStateNormal];
        _couponsLab.titleLabel.font = [UIFont systemFontOfSize:11*HWB];
        [_goodsImgV addSubview:_couponsLab];
        [_couponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0); make.bottom.offset(-2);
            make.height.offset(22); make.width.offset(55*HWB);
        }];
    }
    return _couponsLab ;
}

//券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"¥00.00" color:LOGOCOLOR font:15*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5);
            make.bottom.offset(-6);
        }];
        if (THEMECOLOR==2) {
            _endPriceLab.textColor=[UIColor colorWithRed:240.f/255.f green:15.f/255.f blue:39.f/255.f alpha:1];
        }
    }
    
    
    
    return _endPriceLab;
}

//券后
-(UILabel*)endCouponsLab{
    if (!_endCouponsLab) {
        _endCouponsLab = [UILabel labText:@"券后" color:[UIColor colorWithRed:153.f/255.f green:153.f/255.f blue:153.f/255.f alpha:1] font:11*HWB];
        [self.contentView addSubview:_endCouponsLab];
        [_endCouponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(1);
            make.centerY.equalTo(_endPriceLab.mas_centerY);
        }];
    }
    return _endCouponsLab ;
}

//销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"已售00.00" color:Black102 font:11*HWB];
        _salesLab.textAlignment = NSTextAlignmentLeft;
        _salesLab.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-8);
            make.centerY.equalTo(_endPriceLab.mas_centerY);
        }];
    }
    return _salesLab ;
}

#pragma mark --> 播放视频
-(void)boFangVideo{
    [PageJump openVideoUrlBySystemVideoPlayer:_ormdModel.video];
}

@end
