//
//  NewOrmdCell.h
//  DaRenGouWu
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewOrmdCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     //商品图片
@property(nonatomic,strong) UIImageView  * ima ;          //券1
@property(nonatomic,strong) UIButton     * extendBtn ;     //推广按钮
@property(nonatomic,strong) UIButton     * checkBtn  ;     //选中按钮
@property(nonatomic,strong) UILabel      * goodsTitle ;    //商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   //券后价
@property(nonatomic,strong) UILabel      * priceLab ;      //原价
@property(nonatomic,strong) UILabel      * salesLab ;      //销量
@property(nonatomic,strong) UILabel      * quan;
@property(nonatomic,strong) UILabel      * shareLabel;
@property(nonatomic,strong) UIButton     * shareBtn;


@property(nonatomic,strong) GoodsModel   * ormdModel ;

@end



