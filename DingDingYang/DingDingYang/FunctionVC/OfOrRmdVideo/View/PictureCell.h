//
//  PictureCell.h

//
//  Created by ddy on 2017/12/20.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureCell : UICollectionViewCell
@property(nonatomic,strong) UIImageView  * goodsImgV ;     //商品图片
@property(nonatomic,strong) UIButton* escBtn;

@property(nonatomic,strong) UIImage* image;
@end
