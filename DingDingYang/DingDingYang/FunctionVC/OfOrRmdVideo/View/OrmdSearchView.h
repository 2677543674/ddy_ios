//
//  OrmdSearchView.h
//  DaRenGouWu
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrmdSearchViewDelegate <NSObject>
-(void)clickBtn:(UIButton*)btn;
@end

@interface OrmdSearchView : UIView
@property(nonatomic,strong) UIImageView * searchImgV ;
@property(nonatomic,strong) NSMutableArray * titleArr;  //接口返回标题
@property(nonatomic,strong) UIButton * searchBtn;
@property(nonatomic,strong) UITextField*  searchText;
//@property(nonatomic,strong) UIButton*     searchBtn;
@property(nonatomic,strong) id<OrmdSearchViewDelegate> delegate;
-(instancetype)initWithFrame:(CGRect)frame andTitle:(NSMutableArray*)titleArr;
@end
