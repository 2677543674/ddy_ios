//
//  NewOrmdCell.m
//  DaRenGouWu
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "NewOrmdCell.h"

@implementation NewOrmdCell

-(void)setOrmdModel:(GoodsModel *)ormdModel{
    
    _ormdModel = ormdModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_ormdModel.goodsPic] placeholderImage:PlaceholderImg] ;
    
    _goodsTitle.text = _ormdModel.goodsName ;
    
    _endPriceLab.text = FORMATSTR(@"￥%@",_ormdModel.endPrice) ;
    
    _salesLab.text = FORMATSTR(@"销量:%@",_ormdModel.sales);
    
    _priceLab.text = FORMATSTR(@"原价¥%@",_ormdModel.price) ;
    
    _endPriceLab.text = FORMATSTR(@"¥%@",_ormdModel.endPrice) ;
    
    if (role_state==0) {
        _shareLabel.text=@"立即推广 ";
    }else{
        _shareLabel.text=FORMATSTR(@"赚￥%@",_ormdModel.commission) ;
    }
    
    _quan.text=FORMATSTR(@"¥%@",_ormdModel.couponMoney);
    
    if ([_ormdModel.ormdSelect isEqualToString:@"1"]) {
        _checkBtn.selected = YES;
        _checkBtn.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.4];
    }else{
        _checkBtn.selected = NO;
        _checkBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    }
    
    if ([_ormdModel.couponMoney floatValue]>0) { // 有券
        _ima.hidden=NO;
        _priceLab.hidden=NO;
        [_salesLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_ima.mas_right).offset(10);
            make.centerY.equalTo(_ima.mas_centerY);
            make.width.offset(100);
            make.height.offset(13*HWB);
        }];
    }else{  // 无券
        _ima.hidden=YES;
        _priceLab.hidden=YES;
        [_salesLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.centerY.equalTo(_ima.mas_centerY);
            make.width.offset(100);
            make.height.offset(13*HWB);
        }];
    }
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self goodsImgV];
        [self checkBtn];
        [self goodsTitle];
        
        [self ima];
        
        [self salesLab];
        [self priceLab];
        [self endPriceLab];
        
        [self shareLabel];
        [self shareBtn];
        
    }
    return self;
}


-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.clipsToBounds = YES ;
        _goodsImgV.contentMode = UIViewContentModeScaleAspectFill ;
        _goodsImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);  make.bottom.offset(-10*HWB);
            make.top.offset(10*HWB);  make.width.equalTo(_goodsImgV.mas_height);
        }];
    }
    return _goodsImgV;
}


-(UIButton *)checkBtn{
    if (!_checkBtn) {
        _checkBtn = [UIButton new];
        [_checkBtn setImage:[UIImage imageNamed:@"official_rmd_sltn"] forState:UIControlStateNormal];
        [_checkBtn setImage:[UIImage imageNamed:@"official_rmd_slth"] forState:UIControlStateSelected];
        _checkBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0]; //选中的时候设置
        _checkBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _checkBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_checkBtn setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 0, 0)];
        [self.contentView addSubview:_checkBtn];
        [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10*HWB);  make.bottom.offset(-10*HWB);
            make.top.offset(10*HWB);  make.width.equalTo(_goodsImgV.mas_height);
        }];
    }
    return _checkBtn;
}


-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black102 font:13*HWB];
        _goodsTitle.numberOfLines = 2;
        [self.contentView addSubview:_goodsTitle];
        [_goodsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.right.offset(-10);
            make.top.offset(10*HWB);
            make.height.offset(32*HWB);
        }];
    }
    return _goodsTitle ;
}


-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"销量: 0" color:Black204 font:11*HWB];
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_ima.mas_right).offset(10);
            make.centerY.equalTo(_ima.mas_centerY);
            make.width.offset(100);
            make.height.offset(13*HWB);
        }];
    }
    return _salesLab ;
}



-(UIImageView *)ima{
    if (!_ima) {
        _ima = [[UIImageView alloc]init];
        _ima.image = [UIImage imageNamed:@"official_rmd_vhs_1"] ;
        [self.contentView addSubview:_ima];
        [_ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            make.top.equalTo(_goodsTitle.mas_bottom).offset(5*HWB);
        }];
        UILabel* label=[UILabel labText:@"券" color:COLORWHITE font:11];
        [_ima addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5);
            make.top.bottom.offset(0);
            make.width.offset(10);
        }];
        
        UIImageView* line=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"line"]];
        [_ima addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.offset(0);
            make.left.equalTo(label.mas_right).offset(3);
            make.width.offset(1);
        }];
        
        _quan=[UILabel labText:@"￥100" color:COLORWHITE font:11];
        _quan.textAlignment=NSTextAlignmentCenter;
        [_ima addSubview:_quan];
        [_quan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(line.mas_right).offset(0);
            make.top.bottom.offset(0);
            make.right.offset(-2);
        }];
    }
    return _ima;
}


-(UILabel *)endPriceLab{
    if (!_endPriceLab) {
        
        _endPriceLab = [UILabel labText:@"￥8888.88" color:RGBA(241, 90, 37, 1) font:15*HWB];
        
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(_priceLab.mas_top).offset(-HWB);
            
            make.left.equalTo(_goodsImgV.mas_right).offset(10);
            
            
        }];
    }
    return _endPriceLab ;
}

-(UILabel *)priceLab{
    if (!_priceLab) {
        
        _priceLab = [UILabel labText:@"￥0.0" color:[UIColor grayColor] font:11*HWB];
        
        [self.contentView addSubview:_priceLab];
        [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(15);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
        }];
        UIView* lineView = [UIView new];
        lineView.tag = 555;
        lineView.backgroundColor = [UIColor grayColor];
//        [self.contentView addSubview:lineView];
        [_priceLab addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_priceLab);
            make.width.equalTo(_priceLab.mas_width);
            make.height.offset(1);
        }];
    }
    return _priceLab ;
}


-(UILabel *)shareLabel{
    if (!_shareLabel) {
        _shareLabel = [UILabel labText:@"分享赚" color:RGBA(241, 90, 37, 1) font:10*HWB];
        
        _shareLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:_shareLabel];
        [_shareLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-6*HWB);
            make.bottom.equalTo(_goodsImgV.mas_bottom);
            make.height.offset(15*HWB);
            make.width.offset(64*HWB);
        }];
    }
    return _shareLabel;
}

-(UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn=[UIButton new];
        [_shareBtn setImage:[UIImage imageNamed:@"ormShare"] forState:UIControlStateNormal];
        _shareBtn.layer.cornerRadius=13*HWB;
        _shareBtn.clipsToBounds=YES;
        _shareBtn.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_shareLabel.mas_centerX);
            make.bottom.equalTo(_shareLabel.mas_top);
            make.height.offset(26*HWB);
            make.width.offset(26*HWB);
        }];
    }
    return _shareBtn;
}

@end


