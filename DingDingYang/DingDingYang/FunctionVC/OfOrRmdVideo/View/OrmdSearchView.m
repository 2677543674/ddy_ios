//
//  OrmdSearchView.m
//  DaRenGouWu
//
//  Created by ddy on 2017/12/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "OrmdSearchView.h"

@implementation OrmdSearchView

-(instancetype)initWithFrame:(CGRect)frame andTitle:(NSMutableArray*)titleArr{
    self = [super initWithFrame:frame];
    if (self) {
        _titleArr = [NSMutableArray array];
        _titleArr = titleArr;

        CGFloat space=8;
        CGFloat labelHeight=26*HWB;
        CGFloat btnHeight=32*HWB;
        CGFloat lineViewHeight=28;
        
        UIView* searchView=[UIView new];
        searchView.backgroundColor=RGBA(236, 236, 236, 1);
        searchView.layer.cornerRadius=labelHeight/2.0;
        searchView.clipsToBounds=YES;
        [self addSubview:searchView];
        [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5);
            make.left.offset(30*HWB);
            make.right.offset(-30*HWB);
            make.height.offset(labelHeight);
        }];
        
        _searchBtn=[UIButton new];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        _searchBtn.tag=888;
        [_searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
        [_searchBtn setBackgroundColor:RGBA(241, 90, 37, 1)];
        [searchView addSubview:_searchBtn];
        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.width.offset(55*HWB);
            make.right.offset(0);
            make.bottom.offset(0);
        }];
        
        _searchText=[UITextField new];
        _searchText.placeholder=@"请输入商品名称";
        _searchText.borderStyle=UITextBorderStyleNone;
        _searchText.font=[UIFont systemFontOfSize:13*HWB];
        [searchView addSubview:_searchText];
        [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(4);
            make.left.offset(20);
            make.right.equalTo(_searchBtn.mas_left).offset(-10);
            make.bottom.offset(-2);
        }];
        
//        label.text=@"      请输入商品标题";
//        label.font=[UIFont systemFontOfSize:11*HWB];
//        label.textColor = RGBA(150, 150, 150, 1);
//        label.backgroundColor = RGBA(227, 227, 227, 1);
//        label.layer.cornerRadius=labelHeight/2;
//        label.clipsToBounds=YES;
//        label.tag=800;
//        [self addSubview:label];
//        [label mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(space);
//            make.left.offset(30);
//            make.right.offset(-30);
//            make.height.offset(labelHeight);
//        }];
//        UIImageView* searchIma=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"框搜索"]];
//        [label addSubview:searchIma];
//        [searchIma mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(label);
//            make.right.equalTo(label.mas_right).offset(-20);
//        }];
//        _titleArr=@[@"小编推荐",@"全网好货",@"高佣推荐"];
        for (int i=0; i<_titleArr.count; i++) {
            UIButton * titleBtn=[UIButton new];
            titleBtn.frame=CGRectMake(i*ScreenW/_titleArr.count, labelHeight+space, ScreenW/_titleArr.count, btnHeight);
            [titleBtn setTitle:_titleArr[i] forState:UIControlStateNormal];
            [titleBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal] ;
            [titleBtn setTitleColor:RGBA(241, 90, 37, 1) forState:UIControlStateSelected] ;
            if (i==0) {
                titleBtn.selected=YES;
            }
            titleBtn.titleLabel.font=[UIFont systemFontOfSize:12*HWB];
            titleBtn.tag=10+i;
            [titleBtn addTarget:self action:@selector(clickTitle:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:titleBtn];
            
            if (i!=0) {
                
                UIView* lineView=[[UIView alloc]initWithFrame:CGRectMake(i*ScreenW/_titleArr.count, labelHeight+space+((btnHeight-lineViewHeight)/2), 1, lineViewHeight)];
                lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [self addSubview:lineView];
   
            }
            
        }
        UIView* lineView=[[UIView alloc]initWithFrame:CGRectMake(0, frame.size.height-1, ScreenW , 1)];
        lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:lineView];
        
//        _searchBtn=[UIButton new];
//        [self addSubview:_searchBtn];
//        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(space);
//            make.left.offset(30);
//            make.right.offset(-30);
//            make.height.offset(labelHeight);
//        }];
  
    }
    return self ;
}

-(void)clickTitle:(UIButton*)btn{
    
    [self.delegate clickBtn:btn];
}


@end
