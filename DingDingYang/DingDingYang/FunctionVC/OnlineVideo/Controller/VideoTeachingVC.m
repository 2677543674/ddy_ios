//
//  VideoTeachingVC.m

//
//  Created by ddy on 2017/12/29.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "VideoTeachingVC.h"
#import "VideoTeachingCell.h"
#import "SharePopoverVC.h"
#import "ZFPlayer.h"
#import "VideoTeachModel.h"
#import "Reachability.h"

@interface VideoTeachingVC ()<UICollectionViewDelegate,UICollectionViewDataSource,ZFPlayerDelegate>
@property(nonatomic,strong) UICollectionView * collectionView ;
@property(nonatomic,strong) NSMutableArray* dataArr;
@property(nonatomic,strong) ZFPlayerView    *playerView;
@property(nonatomic,assign) NSInteger page;
@property(nonatomic,assign) NSInteger net;
@end

@implementation VideoTeachingVC

-(void)internetStatus {
    Reachability *reachability   = [Reachability reachabilityWithHostName:@"www.apple.com"];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    switch (internetStatus) {
        case ReachableViaWiFi:
            _net = 1;//wifi
            break;
            
        case ReachableViaWWAN:
            _net = 2;//蜂窝
            break;
            
        case NotReachable:
            _net = 3;//无连接
        default:
            break;
    }
}

// 页面消失时候
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.playerView resetPlayer];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _net=0;
    self.view.backgroundColor=RGBA(245, 245, 245, 1);
    self.title=@"视频教程";
    self.dataArr=[NSMutableArray array];
    [self internetStatus];//判断网络
    [self requestData];
    [self collectionView];
    [self playerView];
    
}


-(void)requestData{
    _page=1;
    [_dataArr removeAllObjects];
    [NetRequest requestTokenURL:url_video_teach parameter:@{@"p":@"1"} success:^(NSDictionary *nwdic) {
        NSArray* list=NULLARY(nwdic[@"list"]);
        [list enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL *  stop) {
            VideoTeachModel*model=[[VideoTeachModel alloc]initWithDic:obj];
            [_dataArr addObject:model];
        }];
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
    } failure:^(NSString *failure) {
        SHOWMSG(@"连接失败")
    }];
}

-(void)requestMoreData{
    _page++;
    NSString* p = [NSString stringWithFormat:@"%ld",_page];
    [NetRequest requestTokenURL:url_video_teach parameter:@{@"p":p} success:^(NSDictionary *nwdic) {
        
        NSArray* list=NULLARY(nwdic[@"list"]);
        if (list.count==0) {
            [self.collectionView.mj_footer endRefreshingWithNoMoreData];
            return;
        }
        [list enumerateObjectsUsingBlock:^(id  obj, NSUInteger idx, BOOL *  stop) {
            VideoTeachModel*model=[[VideoTeachModel alloc]initWithDic:obj];
            [_dataArr addObject:model];
        }];
        [self.collectionView reloadData];
        [self.collectionView.mj_footer endRefreshing];
        
    } failure:^(NSString *failure) {
        SHOWMSG(@"连接失败")
    }];
}


- (ZFPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [ZFPlayerView sharedPlayerView];
        _playerView.delegate = self;
        // 当cell播放视频由全屏变为小屏时候，不回到中间位置
        _playerView.cellPlayerOnCenter = NO;

        // 当cell划出屏幕的时候停止播放
        // _playerView.stopPlayWhileCellNotVisable = YES;
        //（可选设置）可以设置视频的填充模式，默认为（等比例填充，直到一个维度到达区域边界）
        // _playerView.playerLayerGravity = ZFPlayerLayerGravityResizeAspect;
        // 静音
        // _playerView.mute = YES;
        // 移除屏幕移除player
        // _playerView.stopPlayWhileCellNotVisable = YES;

        _playerView.forcePortrait = NO;
//        ZFPlayerShared.isLockScreen = YES;
        ZFPlayerShared.isStatusBarHidden = NO;
        
    }
    return _playerView;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.estimatedItemSize=CGSizeMake(ScreenW, 250);
        layout.minimumLineSpacing = 10*HWB ;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
        _collectionView.delegate = self ;
        _collectionView.dataSource = self ;
        _collectionView.backgroundColor = RGBA(245, 245, 245, 1); ;
        _collectionView.showsVerticalScrollIndicator = NO ;
        [self.view addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.top.offset(5);
            make.bottom.offset(-5);
        }];
        [_collectionView registerClass:[VideoTeachingCell class] forCellWithReuseIdentifier:@"VideoTeachingCell"];
        __weak typeof(self) weakSelf=self;
        _collectionView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf requestData];
        }];
        
        _collectionView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf requestMoreData];
        }];
    }
    return _collectionView;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count ;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    VideoTeachingCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoTeachingCell" forIndexPath:indexPath];
    cell.model=self.dataArr[indexPath.row];
    cell.playBtn.tag=indexPath.row;
    cell.shareBtn.tag=indexPath.row;
    cell.imageView.tag=indexPath.row+1000;
    [cell.playBtn addTarget:self action:@selector(clickPlay:) forControlEvents:UIControlEventTouchUpInside];
    [cell.shareBtn addTarget:self action:@selector(clickShare:) forControlEvents:UIControlEventTouchUpInside];
    return cell ;
}

-(void)clickPlay:(UIButton*)btn{
    if (_net!=1) {
        [UIAlertController showIn:self title:@"温馨提示" content:@"您正在使用非Wi-Fi网络，继续观看会消耗手机流量" ctAlignment:NSTextAlignmentCenter btnAry:@[@"继续",@"取消"] indexAction:^(NSInteger indexTag) {
            NSLog(@"%ld",indexTag);
            if (indexTag==1) {
                return ;
            }else{
                _net=1;
                [self playVideo:btn.tag];
            }
        }];
    }else{
        [self playVideo:btn.tag];
    }
}

-(void)playVideo:(NSInteger )tag{
    NSIndexPath* indexPath=[NSIndexPath indexPathForRow:tag inSection:0];
    VideoTeachingCell* cell=(VideoTeachingCell*)[_collectionView cellForItemAtIndexPath:indexPath];
    
    VideoTeachModel* model=self.dataArr[tag];
    ZFPlayerModel *playerModel = [[ZFPlayerModel alloc] init];
    playerModel.title            = model.title;
    playerModel.videoURL         = model.link;
    playerModel.scrollView       = self.collectionView;
    playerModel.indexPath        = indexPath;
    playerModel.placeholderImageURLString=model.image;
    // player的父视图tag
    playerModel.fatherViewTag    = cell.imageView.tag;
    
    // 设置播放控制层和model
    [_playerView playerControlView:nil playerModel:playerModel];
    // 下载功能
    //    _playerView.hasDownload = YES;
    // 自动播放
    [_playerView autoPlayTheVideo];
}

-(void)clickShare:(UIButton*)btn{
    NSIndexPath* indexPath=[NSIndexPath indexPathForRow:btn.tag inSection:0];
    VideoTeachingCell* cell=(VideoTeachingCell*)[_collectionView cellForItemAtIndexPath:indexPath];
    VideoTeachModel* model=self.dataArr[btn.tag];
    
    [SharePopoverVC shareUrl:[NSString stringWithFormat:@"%@",model.link] title:model.title des:model.ins thubImg:cell.imageView.image shareType:2];
}




#pragma mark - ZFPlayerDelegate

//- (void)zf_playerDownload:(NSString *)url {
//    // 此处是截取的下载地址，可以自己根据服务器的视频名称来赋值
////    NSString *name = [url lastPathComponent];
////    [[ZFDownloadManager sharedDownloadManager] downFileUrl:url filename:name fileimage:nil];
////    // 设置最多同时下载个数（默认是3）
////    [ZFDownloadManager sharedDownloadManager].maxCount = 4;
//}
//
//
//- (BOOL)shouldAutorotate
//{
//    // 调用ZFPlayerSingleton单例记录播放状态是否锁定屏幕方向
//    return !ZFPlayerShared.isLockScreen;
//}
//
//// 支持哪些转屏方向
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskAllButUpsideDown;
//}
//
//// 页面展示的时候默认屏幕方向（当前ViewController必须是通过模态ViewController（模态带导航的无效）方式展现出来的，才会调用这个方法）
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationPortrait;
//}



@end
