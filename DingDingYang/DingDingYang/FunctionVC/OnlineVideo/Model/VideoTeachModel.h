//
//  VideoTeachModel.h
//  DingDingYang
//
//  Created by ddy on 2018/3/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoTeachModel : NSObject
@property (nonatomic, copy  ) NSString *author;
@property (nonatomic, copy  ) NSString *clicks;
@property (nonatomic, copy  ) NSString *createTime;
@property (nonatomic, copy  ) NSString *image;
@property (nonatomic, copy  ) NSString *ins;
@property (nonatomic, copy  ) NSURL *link;
@property (nonatomic, copy  ) NSString *partnerId;
@property (nonatomic, copy  ) NSString *shareLink;
@property (nonatomic, copy  ) NSString *sorts;
@property (nonatomic, copy  ) NSString *title;
@property (nonatomic, copy  ) NSString *type;
@property (nonatomic, copy  ) NSString *updateTime;

-(instancetype)initWithDic:(NSDictionary*)dic;
@end
