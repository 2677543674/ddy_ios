//
//  VideoTeachModel.m
//  DingDingYang
//
//  Created by ddy on 2018/3/9.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "VideoTeachModel.h"

@implementation VideoTeachModel
-(instancetype)initWithDic:(NSDictionary *)dic{
    self=[super init];
    if (self) {
        self.author = REPLACENULL(dic[@"author"]);
        self.clicks = REPLACENULL(dic[@"clicks"]);
        self.createTime = REPLACENULL(dic[@"createTime"]);
        self.image = [REPLACENULL(dic[@"image"]) hasPrefix:@"http"]?dic[@"image"] : FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"image"]));
        self.ins = REPLACENULL(dic[@"ins"]);
        NSString* link = [REPLACENULL(dic[@"link"]) hasPrefix:@"http"]?dic[@"link"] : FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"link"]));
        self.link=[NSURL URLWithString:link];
        self.partnerId = REPLACENULL(dic[@"partnerId"]);
        self.shareLink = [REPLACENULL(dic[@"shareLink"]) hasPrefix:@"http"]?dic[@"shareLink"] : FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"shareLink"]));
        self.sorts = REPLACENULL(dic[@"sorts"]);
        self.title = REPLACENULL(dic[@"title"]);
        self.type = REPLACENULL(dic[@"type"]);
        self.updateTime = REPLACENULL(dic[@"updateTime"]);

    }
    return self;
}
@end
