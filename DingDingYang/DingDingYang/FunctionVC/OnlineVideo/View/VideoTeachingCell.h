//
//  VideoTeachingCell.h

//
//  Created by ddy on 2017/12/29.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoTeachModel.h"

@interface VideoTeachingCell : UICollectionViewCell

@property(nonatomic,strong)UILabel* title;
@property(nonatomic,strong)UILabel* subTitle;
@property(nonatomic,strong)UIButton* shareBtn;
@property(nonatomic,strong)UIImageView* imageView;
@property(nonatomic,strong)UIButton* playBtn;
@property(nonatomic,strong)UILabel* fromName;
@property(nonatomic,strong)UILabel* playNum;
@property(nonatomic,strong)UIView* fatherView;
@property(nonatomic,strong)VideoTeachModel* model;
@end
