//
//  VideoTeachingCell.m

//
//  Created by ddy on 2017/12/29.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "VideoTeachingCell.h"

@implementation VideoTeachingCell

-(void)setModel:(VideoTeachModel *)model{
    self.title.text=model.title;
    self.subTitle.text=model.ins;
    self.fromName.text=model.author;
    self.playNum.text=[NSString stringWithFormat:@"%@次播放",model.clicks];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:PlaceholderImg];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self shareBtn];
        [self title];
        [self subTitle];
        [self imageView];
        [self playBtn];
        [self fromName];
        [self playNum];
        
    }
    return self;
}

-(UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn=[UIButton new];
        [_shareBtn setImage:[UIImage imageNamed:@"分享按钮"] forState:UIControlStateNormal];
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.right.offset(0);
            make.height.width.offset(42*HWB);
        }];
    }
    return _shareBtn;
}

-(UILabel *)title{
    if (!_title) {
        _title=[UILabel labText:@"淘宝领取优惠券后在红人馆购买" color:Black51 font:13*HWB];
        [self.contentView addSubview:_title];
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(4*HWB);
            make.left.offset(10*HWB);
            make.right.equalTo(_shareBtn.mas_left).offset(-10*HWB);
            make.height.offset(18*HWB);
//            make.width.offset(ScreenW-10*HWB-42*HWB-10*HWB);
        }];
    }
    return _title;
}

-(UILabel *)subTitle{
    if (!_subTitle) {
        _subTitle=[UILabel labText:@"省钱购物，分享赚钱" color:Black102 font:11*HWB];
        [self.contentView addSubview:_subTitle];
        [_subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_title.mas_left);
            make.top.equalTo(_title.mas_bottom);
            make.right.equalTo(_shareBtn.mas_left).offset(-15);
            make.height.offset(14*HWB);
        }];
    }
    return _subTitle;
}                  


-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView=[UIImageView new];
        _imageView.image=[UIImage imageNamed:@"背景图"];

        _imageView.backgroundColor=[UIColor blackColor];
        _imageView.contentMode=UIViewContentModeScaleAspectFill;
        _imageView.layer.cornerRadius=4;
        _imageView.clipsToBounds=YES;
        _imageView.userInteractionEnabled=YES;
        [self.contentView addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_shareBtn.mas_bottom).offset(0);
            make.left.offset(10*HWB);
            make.right.offset(-10*HWB);
            make.width.offset(ScreenW-20*HWB);
            make.height.offset((ScreenW-20*HWB)/16*9);
        }];
        
        
        
    }
    return _imageView;
}

-(UIButton *)playBtn{
    if (!_playBtn) {
        _playBtn=[UIButton new];
        [_playBtn setImage:[UIImage imageNamed:@"播放按钮"] forState:UIControlStateNormal];
        [_imageView addSubview:_playBtn];
        [_playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_imageView.mas_centerX);
            make.centerY.equalTo(_imageView.mas_centerY);
        }];
    }
    return _playBtn;
}


-(UILabel *)fromName{
    if (!_fromName) {
        _fromName=[UILabel labText:@"红人馆官方出品" color:Black102 font:11*HWB];
        [self.contentView addSubview:_fromName];
        [_fromName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_title.mas_left);
            make.top.equalTo(_imageView.mas_bottom).offset(3*HWB);
            make.height.offset(16*HWB);
            make.bottom.offset(-3*HWB);
        }];
    }
    return _fromName;
}

-(UILabel *)playNum{
    if (!_playNum) {
        _playNum=[UILabel labText:@"8888次" color:Black102 font:11*HWB];
        [self.contentView addSubview:_playNum];
        [_playNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_imageView.mas_right);
            make.top.equalTo(_imageView.mas_bottom).offset(3*HWB);
            make.height.offset(16*HWB);
        }];
    }
    return _playNum;
}











@end
