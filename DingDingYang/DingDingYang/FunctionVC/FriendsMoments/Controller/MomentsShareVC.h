//
//  MomentsShareVC.h
//  DingDingYang
//
//  Created by ddy on 2018/10/23.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "DDYViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MomentsShareVC : DDYViewController

typedef void (^ClickResultBlock) (NSInteger clickType);
@property (copy, nonatomic) ClickResultBlock clickTypeBlock ;

+(void)showMomentsShareType:(NSInteger)shareContent clickTypeBlock:(ClickResultBlock)clickTypeBlock;

@property(nonatomic,assign) NSInteger shareContent; // 1:花生圈 2:生活圈

@end

NS_ASSUME_NONNULL_END
