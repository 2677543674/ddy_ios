//
//  MomentsShareVC.m
//  DingDingYang
//
//  Created by ddy on 2018/10/23.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "MomentsShareVC.h"


@interface MomentsShareVC () <UIGestureRecognizerDelegate>

@property(nonatomic,strong) UIView * backView;

@property(nonatomic,strong) UIButton * closeBtn;

@end

@implementation MomentsShareVC


+(void)showMomentsShareType:(NSInteger)shareContent clickTypeBlock:(ClickResultBlock)clickTypeBlock{
    MomentsShareVC * momentsVC = [[MomentsShareVC alloc]init];
    momentsVC.clickTypeBlock = clickTypeBlock;
    momentsVC.shareContent = shareContent;
    [TopNavc.topViewController presentViewController:momentsVC animated:NO completion:nil];
}

// 页面初始化方式
-(instancetype)init{
    self = [super init];
    if (self) { self.modalPresentationStyle = UIModalPresentationCustom; }return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor] ;
    self.automaticallyAdjustsScrollViewInsets = NO;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSelf)];
    tap.delegate = self;  [self.view addGestureRecognizer:tap];

    [self backView];
    
    [self performSelector:@selector(showSelf) withObject:self afterDelay:0.1];
    
}

#pragma mark ===>>> 奖励规则弹窗
-(UIView*)backView{
    
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = COLORWHITE ;
        _backView.layer.masksToBounds = YES ;
        _backView.layer.cornerRadius = 8 ;
        _backView.alpha = 0 ;
        [self.view addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.centerY.equalTo(self.view.mas_centerY);
            make.height.offset(_shareContent==1?250:168);
            make.width.offset(280);
        }];
        
        
        UILabel * titleLab = [UILabel labText:@"猜你可能要" color:Black51 font:14*HWB];
        titleLab.textAlignment = NSTextAlignmentCenter;
        [_backView addSubview:titleLab];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.height.offset(60);
        }];
        
        NSArray * titleAry = @[@"合成图分享朋友圈",@"合成图分享好友",@"保存所有图片",@"多图分享朋友圈"];
        NSArray * imgNameAry = @[@"friends_circle",@"friends_wechat",@"friends_save",@"friends_more"];
        for (NSInteger i =(_shareContent==1?0:2); i<4; i++) {
            ShareBtnRegister * shareBtn = [[ShareBtnRegister alloc]initWithImgName:imgNameAry[i] titleName:titleAry[i]];
            shareBtn.titleLab.textColor = Black102;
            shareBtn.frame = CGRectMake(i%2*130+10,_shareContent==1?(i/2*(68+20)+60):60, 130, 68);
            shareBtn.tag = i; ADDTARGETBUTTON(shareBtn, clickShare:)
            [_backView addSubview:shareBtn];
        }
        
        _closeBtn = [UIButton bgImage:@"friends_close"];
        ADDTARGETBUTTON(_closeBtn, removeSelf)
        [self.view addSubview:_closeBtn];
        [_closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView.mas_bottom).offset(30);
            make.centerX.equalTo(_backView.mas_centerX);
            make.width.height.offset(28*HWB);
        }];
        
        _backView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        _backView.alpha = 0;   _closeBtn.alpha = 0;
        
    }
    return _backView;
}

-(void)clickShare:(ShareBtnRegister*)shareBtn{
    self.clickTypeBlock(shareBtn.tag);
    [self removeSelf];
}

-(void)showSelf{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.backgroundColor = RGBA(0, 0, 0,0.4);
        _backView.alpha = 1; _closeBtn.alpha = 1;
        _backView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)removeSelf{
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.backgroundColor = RGBA(0, 0, 0,0);
        _backView.alpha = 0;
         _closeBtn.alpha = 0;
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_backView.frame, point)) {
        return NO;
    }
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
