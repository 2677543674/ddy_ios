//
//  MomentsVC.m
//  DingDingYang
//
//  Created by ddy on 2018/5/15.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MomentsVC.h"
#import "MomentCell.h"
#import "MomentModel.h"
#import "SDCycleScrollView.h"

@interface MomentsVC () <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,SDCycleScrollViewDelegate>
@property(nonatomic,strong) SDCycleScrollView * sdcsView1 ;
@property(nonatomic,strong) SDCycleScrollView * sdcsView2 ;
@property(nonatomic,strong) NSMutableArray * imgUrlAry ; //图片url数组
@property(nonatomic,strong) NSArray * titleArr;
@property(nonatomic,strong) UIScrollView * scrollerView;
@property(nonatomic,strong) NSMutableArray * dataArr1;
@property(nonatomic,strong) NSMutableArray * dataArr2;
@property(nonatomic,strong) UITableView  * tableView1;
@property(nonatomic,strong) UITableView  * tableView2;
@end

@implementation MomentsVC

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setMenuVisible:NO animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setMenuVisible:NO animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if HuaShengLk
    [NetRequest requestTokenURL:url_shareTxt_temple parameter:nil success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            if (REPLACENULL(nwdic[@"selPic2"]).length!=0) {
                NSUDSaveData(REPLACENULL(nwdic[@"selPic2"]), @"selPic2");
            }else{
                NSUDSaveData(@"1", @"selPic2");
            }
        }else{
            NSUDSaveData(@"1", @"selPic2");
        }
    } failure:^(NSString *failure) {
        NSUDSaveData(@"1", @"selPic2");
        SHOWMSG(failure)
    }];
#endif
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=_mtitle.length>0?_mtitle:@"圈子";
    self.view.backgroundColor=COLORWHITE;
    _titleArr=@[@"商品圈",@"生活圈"];
    if (!CANOPENWechat) { _titleArr=@[]; }
    
    if (TOKEN.length==0) {
        [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) {
            if (result == 1) {

            }
            if (result == 2) {
                TopNavc.topViewController.tabBarController.selectedIndex = 0 ;
            }
        }];
        return;
    }
    
    [self creatTopView];
    [self scrollerView];

    [self tableView1];
    [self tableView2];
    
    [_tableView1.mj_header beginRefreshing];
    [_tableView2.mj_header beginRefreshing];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData1) name:@"getMomentsData1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData2) name:@"getMomentsData2" object:nil];

}

#pragma mark ---> 创建轮播图
-(SDCycleScrollView*)sdcsView1{
    if (!_sdcsView1) {
        _sdcsView1 = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero imageURLStringsGroup:nil];
        _sdcsView1.tag=300;
        _sdcsView1.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _sdcsView1.backgroundColor = COLORGROUP ;
        _sdcsView1.delegate = self;
        _sdcsView1.currentPageDotColor = LOGOCOLOR ;
        _sdcsView1.pageDotColor = COLORWHITE;
        _sdcsView1.autoScrollTimeInterval = 3.0;
        [_scrollerView addSubview:_sdcsView1];
        [_sdcsView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.height.offset(0.4*ScreenW);
            make.width.offset(ScreenW);
        }];
    }
    return _sdcsView1 ;
}

-(SDCycleScrollView*)sdcsView2{
    if (!_sdcsView2) {
        _sdcsView2 = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero imageURLStringsGroup:nil];
        _sdcsView2.tag=400;
        _sdcsView2.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _sdcsView2.backgroundColor = COLORGROUP ;
        _sdcsView2.delegate = self;
        _sdcsView2.currentPageDotColor = LOGOCOLOR ;
        _sdcsView2.pageDotColor = COLORWHITE;
        _sdcsView2.autoScrollTimeInterval = 3.0;
        [_scrollerView addSubview:_sdcsView2];
        [_sdcsView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(ScreenW);
            make.height.offset(0.4*ScreenW);
            make.width.offset(ScreenW);
        }];
    }
    return _sdcsView2 ;
}

-(void)getData1{

    [NetRequest requestType:1 url:url_fa_quan_items ptdic:nil success:^(id nwdic) {
        NSArray * data = NULLARY(nwdic[@"data"]);
        self.dataArr1 = [NSMutableArray array];
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MomentModel* model = [[MomentModel alloc]initWithModel:obj];
            model.contentType = 1;
            [_dataArr1 addObject:model];
        }];
        [_tableView1 endRefreshType:self.dataArr1.count isReload:YES];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [_tableView1 endRefreshType:1 isReload:YES]; SHOWMSG(failure)
    }];

}

-(void)getData2{
    [NetRequest requestType:1 url:url_fa_quan_lifes ptdic:nil success:^(id nwdic) {
        NSArray * data = NULLARY(nwdic[@"data"]);
        self.dataArr2 = [NSMutableArray array];
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MomentModel * model=[[MomentModel alloc]initWithModel:obj];
            model.contentType = 2;
            [_dataArr2 addObject:model];
        }];
        [_tableView2 endRefreshType:self.dataArr2.count isReload:YES];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [_tableView2 endRefreshType:1 isReload:YES]; SHOWMSG(failure)
    }];
}

-(void)creatTopView{
    for (int i=0; i<_titleArr.count; i++) {
        UIButton* btn=[UIButton new];
        [btn setTitle:_titleArr[i] forState:(UIControlStateNormal)];
        [btn setTitleColor:Black102 forState:UIControlStateNormal];
        [btn setTitleColor:RGBA(240, 92, 40, 1) forState:UIControlStateSelected];
        btn.titleLabel.font=[UIFont systemFontOfSize:14*HWB];
        btn.tag=10+i;
        [btn addTarget:self action:@selector(clickTopBtn:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(i*ScreenW/_titleArr.count);
            make.height.offset(38);
            make.width.offset(ScreenW/_titleArr.count);
        }];
        if (i==0) {
            UIView* line=[UIView new];
            line.tag=100;
            line.backgroundColor=RGBA(240, 92, 40, 1);
            [self.view addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.offset(36);
                make.height.offset(1);
                make.width.offset(ScreenW/2);
                make.centerX.equalTo(btn.mas_centerX);
            }];
            btn.selected=YES;
        }
    }
}

-(void)clickTopBtn:(UIButton*)btn{
    for (int i=0; i<_titleArr.count; i++) {
        UIButton* button=[self.view viewWithTag:i+10];
        button.selected=NO;
    }
    btn.selected=YES;
    UIView* line=[self.view viewWithTag:100];
    [UIView animateWithDuration:0.35 animations:^{
        [_scrollerView setContentOffset:CGPointMake((btn.tag-10)*ScreenW, 0)];
        [line mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(36);
            make.height.offset(1);
            make.width.offset(ScreenW/2);
            make.centerX.equalTo(btn.mas_centerX);
        }];
        [self.view layoutIfNeeded];
    }];
}

-(UIScrollView *)scrollerView{
    if (!_scrollerView) {
        _scrollerView=[UIScrollView new];
        _scrollerView.delegate=self;
        _scrollerView.tag=999;
        _scrollerView.contentSize=CGSizeMake(_titleArr.count*ScreenW, 0);
        _scrollerView.showsHorizontalScrollIndicator=NO;
        _scrollerView.bounces=NO;
        _scrollerView.pagingEnabled=YES;
        [self.view addSubview:_scrollerView];
        [_scrollerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(38);
            make.left.bottom.offset(0);
            make.width.offset(ScreenW);
        }];
        if (CANOPENWechat==NO) {
            _scrollerView.scrollEnabled=NO;
            [_scrollerView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.offset(0);
                make.left.bottom.offset(0);
                make.width.offset(ScreenW);
            }];
        }
    }
    return _scrollerView;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag==999) {
        UIButton* btn=[self.view viewWithTag:((scrollView.contentOffset.x+ScreenW/2)/ScreenW)+10];
        [self clickTopBtn:btn];
    }
    
}

-(UITableView *)tableView1{
    if (_tableView1==nil) {
        _tableView1=[UITableView new];
        _tableView1.delegate=self;
        _tableView1.dataSource=self;
        _tableView1.tag=100;
        _tableView1.tableFooterView=[UIView new];
        _tableView1.estimatedRowHeight=300;
        _tableView1.separatorInset = UIEdgeInsetsZero;
        [_tableView1 registerClass:[MomentCell class] forCellReuseIdentifier:@"MomentCell"];
        _tableView1.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self getData1];
        }];
        [self.scrollerView addSubview:_tableView1];
        [_tableView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.height.equalTo(_scrollerView.mas_height);
            make.width.offset(ScreenW);
        }];
    }
    return _tableView1;
}

-(UITableView *)tableView2{
    if (_tableView2==nil) {
        _tableView2=[UITableView new];
        _tableView2.delegate=self;
        _tableView2.dataSource=self;
        _tableView2.tag=200;
        _tableView2.tableFooterView=[UIView new];
        _tableView2.estimatedRowHeight=300;
        _tableView2.separatorInset = UIEdgeInsetsZero;
        [_tableView2 registerClass:[MomentCell class] forCellReuseIdentifier:@"MomentCell"];
        _tableView2.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self getData2];
        }];
        [self.scrollerView addSubview:_tableView2];
        [_tableView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(ScreenW);
            make.height.equalTo(_scrollerView.mas_height);
            make.width.offset(ScreenW);
        }];
    }
    return _tableView2;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentCell* cell=[tableView dequeueReusableCellWithIdentifier:@"MomentCell" forIndexPath:indexPath];
    if (indexPath.row==0) {
        cell.ifHot=YES;
    }else{
        cell.ifHot=NO;
    }
    if (tableView.tag==100) {
        cell.type=1;
        cell.model=_dataArr1[indexPath.row];
    }else{
        cell.type=2;
        cell.model=_dataArr2[indexPath.row];
    }

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag==100) {
        return _dataArr1.count;
    }else{
        return _dataArr2.count;
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}




@end
