//
//  MomentImageCell.m
//  DingDingYang
//
//  Created by ddy on 2018/5/16.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MomentImageCell.h"

@implementation MomentImageCell

-(void)setModel:(MomentImageModel *)model{

    _model = model;

    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.largeImgUrl] placeholderImage:nil];
    
    if (_model.sellOff) {
        _allreadyOut.hidden = NO;
    }else{
        _allreadyOut.hidden = YES;
    }
    
    if (model.goodsModel.goodsId.length>0&&model.goodsModel.goodsName.length>0) {
        _bottomView.hidden = NO;
        _topLabel.text = FORMATSTR(@"领%@元券",model.goodsModel.couponMoney);
        _bottomLabel.text = FORMATSTR(@"券后价:￥%@",model.goodsModel.endPrice);
    }else{ _bottomView.hidden=YES; }
    
}


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self imageView];
        [self bottomView];
        [self topLabel];
        [self bottomLabel];
        [self allreadyOut];
    }
    return self;
}


-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [UIImageView new];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        _imageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self.contentView addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
    }
    return _imageView;
}


-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        [self.contentView addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.offset(0);
            make.height.offset(25*HWB);
        }];
    }
    return _bottomView;
}

-(UILabel *)topLabel{
    if (_topLabel==nil) {
        _topLabel = [UILabel new];
        _topLabel.textColor = [UIColor whiteColor];
        _topLabel.font = [UIFont systemFontOfSize:8*HWB];
        [_bottomView addSubview:_topLabel];
        [_topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(1);  make.left.offset(8);
            make.height.equalTo(_bottomView.mas_height).multipliedBy(0.5);
        }];
    }
    return _topLabel;
}

-(UILabel *)bottomLabel{
    if (_bottomLabel==nil) {
        _bottomLabel = [UILabel new];
        _bottomLabel.textColor = [UIColor whiteColor];
        _bottomLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:8*HWB];
        [_bottomView addSubview:_bottomLabel];
        [_bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-1); make.left.offset(8);
            make.height.equalTo(_bottomView.mas_height).multipliedBy(0.5);
        }];
    }
    return _bottomLabel;
}

// 已抢完
-(UIImageView *)allreadyOut{
    if (!_allreadyOut) {
        _allreadyOut = [UIImageView new];
        _allreadyOut.image = [UIImage imageNamed:@"friend_null"];
        _allreadyOut.contentMode = UIViewContentModeCenter;
        _allreadyOut.hidden = YES;
        _allreadyOut.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        [self.contentView addSubview:_allreadyOut];
        [_allreadyOut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
    }
    return _allreadyOut;
}



@end
