//
//  MomentCell.h
//  DingDingYang
//
//  Created by ddy on 2018/5/15.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MomentModel.h"
#import "MomentImageCell.h"

@interface MomentCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UIImageView * headerImgV;
@property(nonatomic,strong) UIImageView * hotImgV;
@property(nonatomic,strong) UILabel * nameLab;
@property(nonatomic,strong) UILabel * contentLab;
@property(nonatomic,strong) UIButton * shareBtn;
@property(nonatomic,strong) UILabel * shareLabel;

@property(nonatomic,strong) UICollectionView * collectionView;
@property(nonatomic,strong) UILabel * timeLab;
@property(nonatomic,strong) UILabel * zhuanLab;
@property(nonatomic,strong) MomentModel * model;
@property(nonatomic,assign) BOOL ifHot; // 是否热门
@property(nonatomic,assign) NSInteger type; // 1:商品圈 2:生活圈
@end
