//
//  MomentImageCell.h
//  DingDingYang
//
//  Created by ddy on 2018/5/16.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MomentImageModel.h"

@interface MomentImageCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView * imageView;
@property(nonatomic,strong) UIImageView * allreadyOut;
@property(nonatomic,strong) UIView * bottomView;
@property(nonatomic,strong) UILabel * topLabel;
@property(nonatomic,strong) UILabel * bottomLabel;
@property(nonatomic,strong) MomentImageModel * model;

@end
