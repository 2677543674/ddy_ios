//
//  MomentCell.m
//  DingDingYang
//
//  Created by ddy on 2018/5/15.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MomentCell.h"
#import "PhotoView.h"
#import "CreateShareImageView.h"
//#import "CreateShareImageView2.h"
//#import "CreateShareImageView3.h"
//#import "CreateShareImageView4.h"
//#import "CreateShareImageView5.h"
#import "GoodsPoster.h"
#import "MomentsShareVC.h"


#define leftRightSpace 12
#define itemWidth (ScreenW-2*leftRightSpace-20)/3


@implementation MomentCell

-(void)setModel:(MomentModel *)model{
    _model = model;
    _nameLab.text = model.authorName;
    _contentLab.text = model.text;
    _zhuanLab.hidden = YES;
    
    _timeLab.text = [self getLocalDateFormateUTCDate:model.startDate];
    NSString * url = [model.headImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [_headerImgV sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:PlaceholderImg];
   
    if (CANOPENWechat==NO) {
        _headerImgV.image = [UIImage imageNamed:@"ZZ"];
        _nameLab.text = APPNAME;
    }
    
    _hotImgV.hidden = !_ifHot;
    _shareLabel.hidden = !model.shared;
    
    [_shareBtn setTitle:FORMATSTR(@"  %@",model.shareCount) forState:(UIControlStateNormal)];
    
    NSInteger height = 0;
    
    switch (model.list.count) {
            // 1 4 2 宽高相等
        case 1:{
            height = (ScreenW-2*leftRightSpace)*2/3;
            [_collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.offset(height);
            }];
        } break;
            
        case 2:{
            height = itemWidth;
            [_collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.offset(itemWidth*2+12);
            }];
        } break;
            
        case 4:{
            height = itemWidth*2+12;
            [_collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.offset(height);
            }];
        } break;
            
        default:{ // 计算collectionview高度
            height = itemWidth*((model.list.count+2)/3)+10*((model.list.count-1)/3);
            [_collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.offset(ScreenW-2*leftRightSpace);
            }];
        } break;
    }
    
    [_collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.offset(height);
    }];
    
    [_collectionView reloadData];
    
}

// 将带有时区的时间转化为正常时间
-(NSString *)getLocalDateFormateUTCDate:(NSString *)utcDate{
    NSArray * array = [utcDate componentsSeparatedByString:@"."];
    if (array.count>0) {
        utcDate = [NSString stringWithFormat:@"%@Z",array[0]];
    }else{
        return utcDate;
    }
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    // 输入格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSTimeZone * localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    
    NSDate * dateFormatted = [dateFormatter dateFromString:utcDate];
    // 输出格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * dateString = [dateFormatter stringFromDate:dateFormatted];
    return dateString;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self headerImgV];
        [self hotImgV];
        [self nameLab];
        [self shareBtn];
        [self shareLabel];
        [self contentLab];
        [self collectionView];
        [self zhuanLab];
        [self timeLab];
    }
    return self;
    
}

-(UIImageView *)headerImgV{
    if (_headerImgV==nil) {
        _headerImgV = [UIImageView new];
        [_headerImgV cornerRadius:3];
        _headerImgV.image = [UIImage imageNamed:@"friend_image1"];
        [self.contentView addSubview:_headerImgV];
        [_headerImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.offset(leftRightSpace);
            make.width.height.offset(36*HWB);
        }];
    }
    return _headerImgV;
}

-(UIImageView *)hotImgV{
    if (_hotImgV==nil) {
        _hotImgV = [UIImageView new];
        _hotImgV.image=[UIImage imageNamed:@"friend_hotImgV"];
        [self.contentView addSubview:_hotImgV];
        [_hotImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.offset(0);
            make.width.height.offset(30);
        }];
    }
    return _hotImgV;
}

-(UILabel *)nameLab{
    if (_nameLab==nil) {
        _nameLab = [UILabel labText:@"" color:RGBA(70, 70, 70, 1) font:13.5*HWB];
        [self.contentView addSubview:_nameLab];
        [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headerImgV.mas_right).offset(8);
            make.centerY.equalTo(_headerImgV.mas_centerY);
            make.width.offset(150*HWB);
        }];
    }
    return _nameLab;
}

-(UIButton *)shareBtn{
    if (_shareBtn==nil) {
        _shareBtn = [UIButton image:@"friend_share" titLe:@" 0" bgColor:COLORWHITE titColorN:RGBA(240, 92, 40, 1) font:9*HWB];
        [_shareBtn boardWidth:0.8 boardColor:RGBA(240,92,40,1) corner:11*HWB];
        ADDTARGETBUTTON(_shareBtn, clickShare)
        [self.contentView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(22*HWB);  make.width.offset(64*HWB);
            make.centerY.equalTo(_headerImgV.mas_centerY);
            make.right.offset(-15);
        }];
    }
    return _shareBtn;
}

-(UILabel *)shareLabel{
    if (_shareLabel==nil) {
        _shareLabel=[UILabel new];
        _shareLabel.text=@"已分享";
        _shareLabel.font=[UIFont systemFontOfSize:10*HWB];
        _shareLabel.textColor=Black153;
        [self.contentView addSubview:_shareLabel];
        [_shareLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_shareBtn.mas_centerY);
            make.right.equalTo(_shareBtn.mas_left).offset(-5);
        }];
    }
    return _contentLab;
}

-(UILabel *)contentLab{
    if (_contentLab==nil) {
        _contentLab = [UILabel labText:@"" color:Black102 font:12*HWB];
        _contentLab.userInteractionEnabled=YES;
        _contentLab.numberOfLines = 0;
        [self.contentView addSubview:_contentLab];
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_headerImgV.mas_bottom).offset(10);
            make.left.equalTo(_headerImgV.mas_left);
            make.width.offset(ScreenW-2*leftRightSpace);
        }];
        
        // 添加长按复制手势
        UILongPressGestureRecognizer *longPressGest = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressView:)];
        longPressGest.allowableMovement = 30; // 长按时候,手指头可以移动的距离
        longPressGest.minimumPressDuration = 0.6;
        [_contentLab addGestureRecognizer:longPressGest];
        
    }
    return _contentLab;
}

-(void)longPressView:(UILongPressGestureRecognizer *)longPressGest{
    
    if (longPressGest.state==UIGestureRecognizerStateBegan) {

        [self becomeFirstResponder];
        UIMenuItem *copyLink = [[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copyContent:)];

        [[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObjects:copyLink, nil]];
        
        [[UIMenuController sharedMenuController] setTargetRect:_contentLab.frame inView:self.contentView];
        
        [[UIMenuController sharedMenuController] setMenuVisible:YES animated: YES];

    } else {
        NSLog(@"长按手势结束");
    }
}


- (BOOL)canBecomeFirstResponder{
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    if (action ==@selector(copyContent:)){
        return YES;
    }
    return NO; // 隐藏系统默认的菜单项
}


-(void)copyContent:(id)sender{
    [ClipboardMonitoring pasteboard:_contentLab.text];
}

-(UILabel *)zhuanLab{
    if (_zhuanLab==nil) {
        _zhuanLab = [UILabel labText:@"" color:RGBA(240, 92, 40, 1) font:11*HWB];
        [self.contentView addSubview:_zhuanLab];
        _zhuanLab.hidden = YES;
        [_zhuanLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_collectionView.mas_bottom).offset(10);
            make.height.offset(10); make.bottom.offset(-10);
            make.left.equalTo(_collectionView.mas_left);
        }];
    }
    return _zhuanLab;
}

-(UILabel *)timeLab{
    if (_timeLab==nil) {
        _timeLab = [UILabel labText:@"2018-08-08" color:Black153 font:10*HWB];
        [self.contentView addSubview:_timeLab];
        [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_zhuanLab.mas_centerY);
            make.left.equalTo(_contentLab.mas_left);
        }];
    }
    return _timeLab;
}

#pragma mark ===>>> UICollectionView(显示图片用的)
-(UICollectionView *)collectionView{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 10; layout.minimumInteritemSpacing = 10;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self; _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        [self.contentView addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_contentLab.mas_bottom).offset(15); make.height.offset(0);
            make.left.equalTo(_contentLab.mas_left); make.width.offset(ScreenW-2*leftRightSpace);
        }];
        
        [_collectionView registerClass:[MomentImageCell class] forCellWithReuseIdentifier:@"moment_image_cell"];
        
    }
    return _collectionView;
}
// 代理
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _model.list.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MomentImageCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"moment_image_cell" forIndexPath:indexPath];
    if (_model.list.count>indexPath.item) {
        cell.model = _model.list[indexPath.item];
    }else{ [_collectionView reloadData]; }
    
    if (_model.list.count==1) {
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        cell.imageView.backgroundColor = COLORWHITE;
    }else{
        cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
        cell.imageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize;
    switch (_model.list.count) {
        case 0:{ itemSize = CGSizeZero; } break;
        case 1:{ itemSize=CGSizeMake((ScreenW-2*leftRightSpace)*2/3,(ScreenW-2*leftRightSpace)*2/3); } break;
        default:{ itemSize=CGSizeMake(itemWidth, itemWidth); } break;
    }
    return itemSize;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MomentImageModel * imageModel = _model.list[indexPath.row];
    // 先判断
    if (imageModel.sellOff) { // 已抢完
        return;
    }else if (imageModel.goodsModel.goodsId.length>0) { // 跳转详情
        MBShow(@"正在加载...")
        NSDictionary * pdic = @{@"goodsId":imageModel.goodsModel.goodsId};
        [NetRequest requestType:0 url:url_goods_info ptdic:pdic success:^(id nwdic) {
            MBHideHUD
            GoodsModel * gmmodel = [[GoodsModel alloc]initWithDic:NULLDIC(nwdic[@"goods"])];
            if (gmmodel.goodsId.length>0&&gmmodel.goodsName.length>0) {
                GoodsDetailsVC * detailvc = [GoodsDetailsVC new];
                detailvc.dmodel = gmmodel ;
                detailvc.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:detailvc animated:YES];
            }else{
                SHOWMSG(@"未查询到商品详情!")
            }
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            MBHideHUD SHOWMSG(failure)
        }];
        
    }else{
        // 放大图片
        NSMutableArray * imageArr = [NSMutableArray array];
        for (int i=0; i<_model.list.count; i++) {
            MomentImageModel * imageModel=_model.list[i];
            [imageArr addObject:imageModel.largeImgUrl];
        }
        [ImgPreviewVC show:TopNavc.topViewController type:0 index:indexPath.item imagesAry:imageArr];
    }
    
}


#pragma mark ===>>> 点击分享，弹出分享选项
-(void)clickShare{

    [MomentsShareVC showMomentsShareType:_model.contentType clickTypeBlock:^(NSInteger clickType) {

        [ClipboardMonitoring pasteboard:_model.text];

        if ([[UIPasteboard generalPasteboard].string isEqualToString:_model.text]) {
            SHOWMSG(@"分享文本内容已复制到粘贴板~")
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            PersonalModel * pmodel = [[PersonalModel alloc]initWithDic:NULLDIC(NSUDTakeData(UserInfo))];

            [NetRequest downloadImageByUrl:pmodel.headImg success:^(id result) {
                 [self creatSharePosters:clickType headImg:(UIImage*)result];
            }serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                [self creatSharePosters:clickType headImg:[UIImage imageNamed:@"logo"]];
            }];
           
        });
    }];
    
}

#pragma mark ===>>> 创建合成海报
-(void)creatSharePosters:(NSInteger)shareType headImg:(UIImage*)userHeadImg{
    
    @try {
        
        MBShow(@"正在加载...")

        NSMutableArray * goodsModelAry = [NSMutableArray array];
        NSMutableArray * goodsIdAry = [NSMutableArray array];
        NSMutableArray * goodsImgAry = [NSMutableArray array];
        
        for (NSInteger i=0; i<_model.list.count; i++) {
            MomentImageModel * imageModel = _model.list[i];
            GoodsModel * goodsModel = imageModel.goodsModel;
            [goodsImgAry addObject:imageModel.largeImgUrl];
            if (goodsModel.goodsId.length>0&&goodsModel.goodsName.length>0) {
                goodsModel.goodsPic = imageModel.largeImgUrl;
                goodsModel.smallPic = imageModel.largeImgUrl;
                [goodsModelAry addObject:goodsModel];
                [goodsIdAry addObject:goodsModel.goodsId];
            }
        }
        
        [NetRequest downloadImageByUrl:goodsImgAry success:^(id result) {
            NSLog(@"已下载所有图片到缓存！");
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            NSLog(@"%@",failure);
        }];
        
        if (goodsIdAry.count>1) { // 多个商品的，
            
            if (shareType==2) { // 合成多张海报保存到本地
                [MethodsClassObjc photoPermissionsNoOpenShowVC:TopNavc.topViewController photoPermissionsType:^(NSInteger type) {
                    if (type==1) {
                        [self shareType:2 goodsMd:goodsModelAry uHImg:userHeadImg];
                    }
                }];
                
            }else if(shareType==3){
                [self shareType:3 goodsMd:goodsModelAry uHImg:userHeadImg];
            }else{
                // 多个商品分享的 合成一张海报
                [GoodsPoster createShareGoodsPosterBy:goodsModelAry goodsidAry:goodsIdAry resultPoster:^(id data) {
                    MBHideHUD
                    if ([data isKindOfClass:[UIImage class]]) {
                        [self shareType:shareType shareImgPosters:(UIImage*)data];
                    }else{ SHOWMSG(FORMATSTR(@"%@",data)) }
                }];
            }
            
        }else if(goodsIdAry.count==1) { // 单个商品的
            
            MomentImageModel * imageModel = _model.list[0];
            GoodsModel * goodsModel = imageModel.goodsModel;
            NSMutableDictionary * parameterDic = [NSMutableDictionary dictionary] ;
            parameterDic[@"couponMoney"] = goodsModel.couponMoney;
            parameterDic[@"goodsPic"] = imageModel.largeImgUrl;
            parameterDic[@"goodsName"] = goodsModel.goodsName;
            parameterDic[@"remnContent"] = goodsModel.desStr;
            parameterDic[@"endPrice"] = goodsModel.endPrice;
            parameterDic[@"goodsId"] = goodsModel.goodsId;
            parameterDic[@"price"] = goodsModel.price;
            parameterDic[@"ifAuth"] = @"1"; // 新接口(淘宝二次授权)
            NSString * reqUrl = FORMATSTR(@"%@%@",dns_dynamic_main,url_goods_share_posterUrl);
            [NetRequest requestType:0 url:reqUrl ptdic:parameterDic success:^(id nwdic) {
                
                NSString * ewmUrl = REPLACENULL(nwdic[@"url"]);
                goodsModel.url = ewmUrl;
                
                [NetRequest downloadImageByUrl:imageModel.largeImgUrl success:^(id result) { MBHideHUD
                    
                    UIImage * posterImg = [self asingleGoodsPosters:(UIImage*)result logo:userHeadImg goodsModel:goodsModel];
                    [self shareType:shareType shareImgPosters:posterImg];
                    
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    if (_model.list.count>1) {
                        MomentImageModel * imageModel1 = _model.list[1];
                        LOG(@"%@\n%@",imageModel.largeImgUrl,imageModel1.largeImgUrl);
                        [NetRequest downloadImageByUrl:imageModel1.largeImgUrl success:^(id results) { MBHideHUD
                            
                            UIImage * posterImg = [self asingleGoodsPosters:(UIImage*)results logo:userHeadImg goodsModel:goodsModel];
                            [self shareType:shareType shareImgPosters:posterImg];
                            
                        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                            MBHideHUD SHOWMSG(failure)
                        }];
                    }else{
                        MBHideHUD SHOWMSG(@"商品主图(大图)下载失败!")
                    }
                }];
                
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD SHOWMSG(failure)
            }];
            
        }else{ // 没有商品的，单图，或多张图的
            MBHideHUD
            if (_model.list.count>0) {
                [self shareType:shareType shareImgPosters:nil];
            }else{
                SHOWMSG(@"分享的文案已复制到粘贴板~")
                [ClipboardMonitoring pasteboard:_model.shareCount];
            }
        }
        
    } @catch (NSException *exception) {
        SHOWMSG(@"海报生成失败!")
    } @finally { }

}

// 只有一个商品的时候，合成海报
-(UIImage*)asingleGoodsPosters:(UIImage*)goodsImage logo:(UIImage*)logoImage goodsModel:(GoodsModel*)goodsModel{
    
    @try {
        
        NSInteger selPic2 = [REPLACENULL(NSUDTakeData(@"selPic2")) integerValue];
        
        UIImage * shareImage;
        
        switch (selPic2) {
                
            case 2:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.68);
                CreateShareImageView2 * view = [[CreateShareImageView2 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.contentView addSubview:view];
                view.goodsImage = goodsImage;   // view.shopHeaderImage = shopImage;
                view.ifRem = (goodsModel.shopModel.picPath.length>0? YES:NO);
                view.ifSale = YES; view.logoImage = logoImage;
                view.model = goodsModel;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            case 3:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.55);
                CreateShareImageView3 * view = [[CreateShareImageView3 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.contentView addSubview:view];
                view.goodsImage = goodsImage;   // view.shopHeaderImage = shopImage;
                view.ifRem = (goodsModel.shopModel.picPath.length>0? YES:NO);
                view.ifSale = YES; view.logoImage = logoImage;
                view.model = goodsModel;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            case 4:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.55);
                CreateShareImageView4 * view = [[CreateShareImageView4 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.contentView addSubview:view];
                view.goodsImage = goodsImage;   // view.shopHeaderImage = shopImage;
                view.ifRem = (goodsModel.shopModel.picPath.length>0? YES:NO);
                view.ifSale = YES; view.logoImage = logoImage;
                view.model = goodsModel;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            case 5:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.55);
                CreateShareImageView5 * view = [[CreateShareImageView5 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.contentView addSubview:view];
                view.goodsImage = goodsImage;   // view.shopHeaderImage = shopImage;
                view.ifRem = (goodsModel.shopModel.picPath.length>0? YES:NO);
                view.ifSale = YES; view.logoImage = logoImage;
                view.model = goodsModel;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            default:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.45);
                CreateShareImageView * view = [[CreateShareImageView alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.contentView addSubview:view];
                view.goodsImage = goodsImage;   // view.shopHeaderImage = shopImage;
                view.ifRem = (goodsModel.shopModel.picPath.length>0? YES:NO);
                view.ifSale = YES; view.logoImage = logoImage;
                view.model = goodsModel;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
        }
        
        if (shareImage) {
            return shareImage;
//            [self shareType:shareType shareImgPosters:shareImage];
        }else{
            return nil;
//            SHOWMSG(@"合成图片为空，请重试!")
        }
    } @catch (NSException *exception) {
        return nil;
//        SHOWMSG(@"海报合成失败！")
    } @finally {
        
    }

    
}

// 将分享的view转化为图片
-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)shareType:(NSInteger)moreType goodsMd:(NSArray*)goodsModelAry uHImg:(UIImage*)userHeadImg{
    
    NSString * reqUrl = FORMATSTR(@"%@%@",dns_dynamic_main,url_goods_share_posterUrl);
    
    NSMutableArray * hchPosterAry = [NSMutableArray array];

    __block NSInteger numCount = 0;
    
    for (GoodsModel * gModel in goodsModelAry) {
        
        NSMutableDictionary * parameterDic = [NSMutableDictionary dictionary] ;
        parameterDic[@"couponMoney"] = gModel.couponMoney;
        parameterDic[@"goodsPic"] = gModel.goodsPic;
        parameterDic[@"goodsName"] = gModel.goodsName;
        parameterDic[@"remnContent"] = gModel.desStr;
        parameterDic[@"endPrice"] = gModel.endPrice;
        parameterDic[@"goodsId"] = gModel.goodsId;
        parameterDic[@"price"] = gModel.price;
        parameterDic[@"ifAuth"] = @"1"; // 新接口(淘宝二次授权)

        [NetRequest requestType:0 url:reqUrl ptdic:parameterDic success:^(id nwdic) {
            NSString * ewmUrl = REPLACENULL(nwdic[@"url"]);
            
            gModel.url = ewmUrl;
            
            [NetRequest downloadImageByUrl:gModel.goodsPic success:^(id result) {
                UIImage * posterImg = [self asingleGoodsPosters:(UIImage*)result logo:userHeadImg goodsModel:gModel];
                [hchPosterAry addObject:posterImg];
               
                numCount++;
                if (goodsModelAry.count==numCount) {
                    [self saveImgOrShareMore:hchPosterAry type:moreType];
                }
                
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                numCount++;
                if (goodsModelAry.count==numCount) {
                    [self saveImgOrShareMore:hchPosterAry type:moreType];
                }
            }];
            
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            numCount++;
            if (goodsModelAry.count==numCount) {
                [self saveImgOrShareMore:hchPosterAry type:moreType];
            }
        }];
        
    }
}

-(void)saveImgOrShareMore:(NSArray*)imgData type:(NSInteger)moreType{
    MBHideHUD
    if (moreType==2) { // 保存多张商品海报到相册
        
        for (UIImage * saveimg in imgData) {
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                // 写入图片到相册
                [PHAssetChangeRequest creationRequestForAssetFromImage:saveimg];
                
            } completionHandler:^(BOOL success, NSError * _Nullable error) {
//                NSLog(@"%d错误信息: %@",success,error);
            }];
        } SHOWMSG(@"图片已保存~")
    
    }else{ // 调用系统分享多张合成海报
        CustomShareModel * model = [[CustomShareModel alloc]init];
        model.currentVC = TopNavc.topViewController;
        model.shareType = 5;
        model.imgDataOrAry = imgData;
        [CustomUMShare shareByCustomModel:model result:^(NSInteger result) {
            if (result==1) { [self shareSuccess]; } // 分享成功
        }];
    }
}

#pragma mark ===>>> 最终分享操作(单个商品或单个商品多张图片)
-(void)shareType:(NSInteger)type shareImgPosters:(UIImage*)posters{
    
    @try {
//        if (TARGET_IPHONE_SIMULATOR) { // 模拟器
//            if (posters!=nil) {
//                [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[posters]];
//            }
//        }else{
            if (type==2) { // 保存到相册
               
                [MethodsClassObjc photoPermissionsNoOpenShowVC:TopNavc.topViewController photoPermissionsType:^(NSInteger ptype) {
                    if (ptype==1) {
                        if (posters!=nil) {
                            UIImageWriteToSavedPhotosAlbum(posters,self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
                        }
                        NSMutableArray * urlImgAry = [NSMutableArray array];
                        for (NSInteger i=0; i<_model.list.count; i++) {
                            MomentImageModel * imageModel = _model.list[i];
                            [urlImgAry addObject:imageModel.largeImgUrl];
                        }
                        
                        [NetRequest downloadImageByUrl:urlImgAry success:^(id result) {
                            NSArray * saveImgAry = (NSArray*)result;
                            for (UIImage * saveimg in saveImgAry) {
                                UIImageWriteToSavedPhotosAlbum(saveimg,self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
                            }
                            SHOWMSG(@"图片已保存~")
                        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                            if (errorType==1) {
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    SHOWMSG(failure)
                                });
                            }else{ SHOWMSG(failure) }
                            
                        }];
                        
                    }
                }];
            }else{
                
                CustomShareModel * model = [[CustomShareModel alloc]init];
                model.shareType = 2; model.currentVC = TopNavc.topViewController;
                
                if (posters!=nil) {
                    model.imgDataOrAry = posters;
                }else{
                    if (_model.list.count>0) {
                        MomentImageModel * imageModel = _model.list[0];
                        [NetRequest downloadImageByUrl:imageModel.largeImgUrl success:^(id result) {
                            model.imgDataOrAry = (UIImage*)result;
                        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
                    }
                }
               
                switch (type) {
                    case 0:{ model.sharePlatform = 2; } break; // 朋友圈
                    case 1:{ model.sharePlatform = 1; } break; // 好友
                    case 3:{ // 调用系统分享
                        model.shareType = 5;
                        NSMutableArray * shareImgAry = [NSMutableArray array];
                        for (NSInteger i=0; i<_model.list.count; i++) {
                            MomentImageModel * imageModel = _model.list[i];
                            [shareImgAry addObject:imageModel.largeImgUrl];
                        }
                        [NetRequest downloadImageByUrl:shareImgAry success:^(id result) {
                            NSMutableArray * sysShareImgAry = [NSMutableArray array];
                            [sysShareImgAry addObjectsFromArray:(NSArray*)result];
                            if (posters) {
                                [sysShareImgAry insertObject:posters atIndex:0];
                            }
                            if (sysShareImgAry.count>9) {
                                [sysShareImgAry removeLastObject];
                            }
                            model.imgDataOrAry = sysShareImgAry;
                            [CustomUMShare shareByCustomModel:model result:^(NSInteger result) {
                                if (result==1) { [self shareSuccess]; } // 分享成功
                            }];
                        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                            if (errorType==2) { SHOWMSG(failure) }
                        }];
                        
                    } break;
                    default: break;
                }
                
                if (type==0||type==1) {
                    if (model.imgDataOrAry==nil) { SHOWMSG(@"还未未获取到要分享的图片！") return; }
                    [CustomUMShare shareByCustomModel:model result:^(NSInteger result) {
                        if (result==1) { [self shareSuccess]; } // 分享成功
                    }];
                }
               
            }
//        }
    } @catch (NSException *exception) {
        SHOWMSG(@"分享过程出现异常!")
    } @finally { }
    
}

//// 保存图片到相册
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    NSLog(@"%@%@",image,error);
}



-(void)shareSuccess{
    NSString * url = @"";
    if (_type==1) { url = FORMATSTR(@"%@Items/%@/increment",dns_dynamic_faquan,_model.momentId); }
    if (_type==2) { url = FORMATSTR(@"%@Lifes/%@/increment",dns_dynamic_faquan,_model.momentId); }
    if(url.length>0){
        [NetRequest requestType:1 url:url ptdic:nil success:^(id nwdic) {
            // 发通知刷新数据
            if (_type==1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"getMomentsData1" object:nil];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"getMomentsData2" object:nil];
            }
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { SHOWMSG(failure) }];
    }
}


@end
