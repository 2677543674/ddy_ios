//
//  MomentModel.m
//  DingDingYang
//
//  Created by ddy on 2018/5/15.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MomentModel.h"

@implementation MomentModel

-(instancetype)initWithModel:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        
        self.text = dic[@"text"];
        self.shareCount = REPLACENULL(dic[@"shareCount"]);
        self.isOn = [REPLACENULL(dic[@"isOn"]) boolValue];
        self.headImage = REPLACENULL(dic[@"headImage"]);
        self.authorName = REPLACENULL(dic[@"authorName"]);
        self.startDate = REPLACENULL(dic[@"startDate"]);
        self.endDate = REPLACENULL(dic[@"endDate"]);
        self.momentId = REPLACENULL(dic[@"id"]);
        self.shared = [dic[@"shared"] boolValue];
        
        NSMutableArray * ary = [NSMutableArray array];
        NSArray * mcary = NULLARY(dic[@"list"]) ;
        [mcary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
            MomentImageModel * model = [[MomentImageModel alloc]initWithModel:obj];
            if (model.goodsModel.goodsId.length > 0) {
                [ary insertObject:model atIndex:0];
            }else{
                [ary addObject:model];
            }
            
            if (ary.count == 9) { *stop = YES; }
        }];
        self.list = [[NSArray alloc]initWithArray:ary];
        
    }
    
    return self;
}

@end
