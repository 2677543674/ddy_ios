//
//  MomentModel.h
//  DingDingYang
//
//  Created by ddy on 2018/5/15.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MomentImageModel.h"

@interface MomentModel : NSObject

@property(nonatomic,copy) NSString * text;
@property(nonatomic,copy) NSString * shareCount;
@property(nonatomic,copy) NSString * headImage;
@property(nonatomic,copy) NSString * authorName;
@property(nonatomic,copy) NSString * startDate;
@property(nonatomic,copy) NSString * endDate;
@property(nonatomic,copy) NSString * momentId;
@property(nonatomic,assign) BOOL isOn;
@property(nonatomic,assign) BOOL shared;
@property(nonatomic,strong) NSArray* list;
@property(nonatomic,assign) NSInteger contentType; // 本地参数，1:花生圈 2:生活圈


-(instancetype)initWithModel:(NSDictionary *)dic;

@end
