//
//  MomentImageModel.h
//  DingDingYang
//
//  Created by ddy on 2018/5/16.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MomentImageModel : NSObject

@property(nonatomic,copy) NSString * smallImgUrl; // 小图
@property(nonatomic,copy) NSString * largeImgUrl; // 大图
@property(nonatomic,assign) BOOL  sellOff;        // 是否已抢完
@property(nonatomic,strong) GoodsModel * goodsModel;

-(instancetype)initWithModel:(NSDictionary *)dic;

@end
