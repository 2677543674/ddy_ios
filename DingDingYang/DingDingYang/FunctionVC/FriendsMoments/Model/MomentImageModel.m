//
//  MomentImageModel.m
//  DingDingYang
//
//  Created by ddy on 2018/5/16.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "MomentImageModel.h"

@implementation MomentImageModel

-(instancetype)initWithModel:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        self.smallImgUrl = [REPLACENULL(dic[@"smallImgUrl"]) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        self.largeImgUrl = [REPLACENULL(dic[@"largeImgUrl"]) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        self.sellOff = [dic[@"sellOff"] boolValue];
        self.goodsModel = [[GoodsModel alloc]initWithDic:NULLDIC(dic[@"goods"])];
    }

    return self;
}
@end
