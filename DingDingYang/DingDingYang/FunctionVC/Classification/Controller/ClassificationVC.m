//
//  ClassificationVC.m
//  DingDingYang
//
//  Created by ddy on 2017/8/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ClassificationVC.h"
#import "ClassASingleCell.h"
#import "UniversalGoodsListVC.h"
#import "SearchOldVC.h"
#import "HSearchView1.h"

@interface ClassificationVC () <UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UIButton * classSearchBtn ;
@property(nonatomic,strong) UICollectionView * classCltView ;

@property(nonatomic,strong) NSMutableArray * dataArray ;
@property(nonatomic,strong) HSearchView1 * searchView1 ;

@end

@implementation ClassificationVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self.navigationController setNavigationBarHidden:YES animated: animated];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
     [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGBA(236, 236, 236, 1) ;
    [self addTopView];
    
    _dataArray = [NSMutableArray array];
    
    if (_mcsModel.subCateInfoList!=nil) {
        [_mcsModel.subCateInfoList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MClassModel * model = [[MClassModel alloc]initWithMC:obj];
            [_dataArray addObject:model];
        }];
    }else{

    }
   
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(ScreenW/2-9, 70*HWB);
    layout.minimumLineSpacing = 6 ;
    layout.minimumInteritemSpacing = 6 ;
    _classCltView = [[UICollectionView alloc]initWithFrame:CGRectMake(6, NAVCBAR_HEIGHT+10, ScreenW-12, ScreenH-125) collectionViewLayout:layout];
    _classCltView.delegate = self ;
    _classCltView.dataSource = self ;
    _classCltView.backgroundColor = COLORGROUP ;
    _classCltView.showsVerticalScrollIndicator = NO ;
    [self.view addSubview:_classCltView];
    [_classCltView registerClass:[ClassASingleCell class] forCellWithReuseIdentifier:@"classA_singlecell"];
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataArray.count ;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ClassASingleCell * asCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"classA_singlecell" forIndexPath:indexPath];
    asCell.mcModel = _dataArray[indexPath.row];
    return asCell ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UniversalGoodsListVC * uglvc = [[UniversalGoodsListVC alloc]init];
    uglvc.mcsModel = _dataArray[indexPath.row] ;
    uglvc.hidesBottomBarWhenPushed = YES ;
    [self.navigationController pushViewController:uglvc animated:YES];
}

-(void)addTopView{
    //创建搜索栏
    _searchView1 = [[HSearchView1 alloc]initWithFrame:CGRectMake(0, 0, ScreenW, NAVCBAR_HEIGHT)];
    [self.view addSubview:_searchView1];

    self.title = _mcsModel.title.length==0?@"分类":_mcsModel.title ;
}

-(void)clickHSearchBtn{
    if (TopNavc!=nil) {
        SearchOldVC * search = [[SearchOldVC alloc]init];
        search.searchType=1;
        search.hidesBottomBarWhenPushed = YES;
        [TopNavc pushViewController:search animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
