//
//  ClassASingleCell.h
//  DingDingYang
//
//  Created by ddy on 2017/8/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassASingleCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView * classImgV ;
@property(nonatomic,strong) UILabel * titLab ;
@property(nonatomic,strong) UILabel * contLab ;
@property(nonatomic,strong) MClassModel * mcModel ;
@end
