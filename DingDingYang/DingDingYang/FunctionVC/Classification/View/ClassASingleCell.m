//
//  ClassASingleCell.m
//  DingDingYang
//
//  Created by ddy on 2017/8/5.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ClassASingleCell.h"

@implementation ClassASingleCell

-(void)setMcModel:(MClassModel *)mcModel{
    _mcModel = mcModel ;
    [_classImgV sd_setImageWithURL:[NSURL URLWithString:mcModel.img] placeholderImage:[UIImage imageNamed:_mcModel.localImgName]];
    _titLab.text = _mcModel.title ;
    _contLab.text = _mcModel.subtitle ;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = COLORWHITE ;
        
        self.contentView.layer.masksToBounds = YES ;
        self.contentView.layer.shouldRasterize = YES;
        self.contentView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.contentView.layer.cornerRadius = 6 ;
        
        _classImgV = [[UIImageView alloc]init];
        _classImgV.image = [UIImage imageNamed:@"tixian"];
        _classImgV.contentMode = UIViewContentModeScaleAspectFit ;
        [self.contentView addSubview:_classImgV];
        [_classImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.equalTo(self.contentView.mas_centerX).offset(-10);
            make.left.offset(10);
            make.height.offset(30*HWB);
        }];
        
        _titLab = [UILabel labText:@"数码" color:Black51 font:13*HWB];
        [self.contentView addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_centerX).offset(-5);
            make.top.equalTo(self.contentView.mas_centerY);
            make.right.offset(0);
        }];
        
        _contLab = [UILabel labText:@"高科技黑科技" color:Black102 font:11*HWB];
        [self.contentView addSubview:_contLab];
        [_contLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_centerX).offset(-5);
            make.bottom.equalTo(self.contentView.mas_centerY).offset(-2.5*HWB);
            make.right.offset(0);
        }];
        
    }
    return self ;
}

@end
