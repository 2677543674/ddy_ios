//
//  HelpCenterVC.m
//  DingDingYang
//
//  Created by ddy on 2018/1/18.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "HelpCenterVC.h"
#import "TxtCell.h"

@interface HelpCenterVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSArray* titleArr;
@property(nonatomic,strong)UITableView* tableView;

@end

@implementation HelpCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    self.title = _modelTitle.length==0?@"帮助说明":_modelTitle;
    [self requestTitle];
}

-(void)requestTitle{
    [NetRequest requestTokenURL:@"http://red.jfapps.top/app/user/getHelpType.api?token=48d6e660-71fc-461f-a626-826f5857ba4c&partnerId=10103" parameter:nil success:^(NSDictionary *nwdic) {
        NSLog(@"%@",nwdic);
        _titleArr=nwdic[@"list"];
        [self setTitleBtn:_titleArr];
        [self tableView];
        
    } failure:^(NSString *failure) {
        
    }];
}

-(void)setTitleBtn:(NSArray* )arr{
    for (int i=0; i<arr.count; i++) {
        NSDictionary* dic=arr[i];
        NSString* img=[dic[@"img"] hasPrefix:@"http"]?dic[@"img"]:FORMATSTR(@"%@%@",dns_dynamic_loadimg,dic[@"img"]);
        TitleBtn* btn=[[TitleBtn alloc]initWithImgName:img titleName:dic[@"name"]];
        btn.backgroundColor=HRGCOLOR;
        btn.tag=i+10;
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.frame=CGRectMake(i*ScreenW/arr.count, 0, ScreenW/arr.count, 40*HWB);
        [self.view addSubview:btn];
    }
}

-(UITableView *)tableView{
    if (_tableView==nil) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = COLORGROUP ;
        _tableView.delegate = self;  _tableView.dataSource = self;
        [_tableView registerClass:[TxtCell class] forCellReuseIdentifier:@"txtcell"];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(40*HWB+1);
            make.right.left.bottom.offset(0);
        }];
    }
    return _tableView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _titleArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44*HWB ;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TxtCell * txtcell = [tableView dequeueReusableCellWithIdentifier:@"txtcell" forIndexPath:indexPath];
    NSDictionary* dic=_titleArr[indexPath.row];
    txtcell.labTitle.text =dic[@"name"];
    txtcell.accessoryType=UITableViewCellAccessoryDisclosureIndicator ;
    return txtcell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)clickBtn:(UIButton*)btn{
    [self.tableView reloadData];
}

@end

@implementation TitleBtn

-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName{
    self=[super init];
    if (self) {
        
        _imgView=[[UIImageView alloc]init];
        [_imgView sd_setImageWithURL:[NSURL URLWithString:ImgName]];
//        _imgView.image=[UIImage imageNamed:ImgName];
//        _imgView.layer.masksToBounds = YES;
        [self addSubview:_imgView];
//        _imgView.clipsToBounds = YES ;
        _imgView.contentMode = UIViewContentModeScaleAspectFill ;
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.offset(-20);
            make.top.offset(2);
            make.width.equalTo(_imgView.mas_height);
        }];
        
        _titleLab=[[UILabel alloc]init];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.text=titleName; _titleLab.font=[UIFont systemFontOfSize:10*HWB];
        _titleLab.textColor=[UIColor whiteColor];
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(13);
            make.bottom.offset(-3);
            make.centerX.equalTo(self.mas_centerX);
        }];
    }  return self ;
    
}
@end
