//
//  HelpCenterVC.h
//  DingDingYang
//
//  Created by ddy on 2018/1/18.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCenterVC : DDYViewController
@property(nonatomic,copy) NSString* modelTitle;
@end


@interface TitleBtn : UIButton
-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName;
@property(nonatomic,strong) UIImageView * imgView;
@property(nonatomic,strong) UILabel * titleLab;
@property(nonatomic,copy) NSString * ImgName,* TitleName ;
@end
