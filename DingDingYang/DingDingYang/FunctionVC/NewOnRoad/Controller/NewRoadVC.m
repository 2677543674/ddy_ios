//
//  NewRoadVC.m
//  DingDingYang
//
//  Created by ddy on 2017/6/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "NewRoadVC.h"
#import "TxtCell.h"
@interface NewRoadVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSMutableArray * dataAry ;
@property(nonatomic,strong) UITableView * tabView ;
@property(nonatomic,copy)   NSString * bannerImg ;
@property(nonatomic,assign) NSInteger   page ;
@property(nonatomic,assign) float  headImgH ; //图片高度

@end

@implementation NewRoadVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = _mcsModel.title.length==0?@"新手上路":_mcsModel.title;
    _headImgH = ScreenW/2.8 ;
    _dataAry = [NSMutableArray array];
    
//    _bannerImg = @"xinshoushanglu" ;

    [self tabView];
    [self requestRoad];
   
}

-(void)requestRoad{
    _page = 2 ;
    [NetRequest requestTokenURL:url_new_road parameter:@{@"p":@"1"} success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            [_dataAry removeAllObjects];
            NSArray * ary =  NULLARY( nwdic[@"list"] ) ;
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [_dataAry addObject:obj];
            }];
            NSString * bannerstr = REPLACENULL(nwdic[@"newUserBanner"]) ;
            if (bannerstr.length>0) {
                _bannerImg = [bannerstr hasPrefix:@"http"] ? bannerstr : FORMATSTR(@"%@%@",dns_dynamic_loadimg,bannerstr) ;
            }
            [_tabView reloadData];
        }else{
           SHOWMSG(nwdic[@"result"])
        }
        [_tabView.mj_header endRefreshing];
    } failure:^(NSString *failure) {
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestRoad];
        }];
        SHOWMSG(failure)
        [_tabView.mj_header endRefreshing];
    }];
}
-(void)loadMoreData{
    [NetRequest requestTokenURL:url_new_road parameter:@{@"p":FORMATSTR(@"%ld",(long)_page)} success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray * ary =  NULLARY( nwdic[@"list"] ) ;
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [_dataAry addObject:obj];
            }];
           
            if (ary.count>0) {
                [_tabView.mj_footer endRefreshing];   _page ++ ;
            }else{
                 [_tabView.mj_footer endRefreshingWithNoMoreData];
            }
            [_tabView reloadData];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        [_tabView.mj_header endRefreshing];
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [_tabView.mj_footer endRefreshing];
    }];
}

-(UITableView*)tabView{
    if (!_tabView) {
        _tabView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tabView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _tabView.delegate=self; _tabView.dataSource=self;
        [self.view addSubview:_tabView];
        [_tabView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [_tabView registerClass:[TxtCell class] forCellReuseIdentifier:@"txtcell"];
        [_tabView registerClass:[ImgCell class] forCellReuseIdentifier:@"imgcell"];
        if (CANOPENWechat==YES||CANOPENAlipay==YES){
            //下拉刷新
            __weak NewRoadVC*SelfView=self;
            _tabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(requestRoad)];
            _tabView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                [SelfView loadMoreData];
            }];
        }
       
    }
    return _tabView ;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {  return 1;  }
    return _dataAry.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {  return _headImgH ; }
    return 42*HWB ;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ImgCell * imgcell = [tableView dequeueReusableCellWithIdentifier:@"imgcell" forIndexPath:indexPath];
        [imgcell.imgView sd_setImageWithURL:[NSURL URLWithString:_bannerImg] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            NSIndexPath * path = [NSIndexPath indexPathForRow:0 inSection:0];
            if (image==nil) {
                if ([_bannerImg isEqualToString:@"xinshoushanglu"]) {
                    imgcell.imgView.image = [UIImage imageNamed:@"xinshoushanglu"] ;
                    _headImgH = ScreenW/2.8 ;
                    [_tabView reloadRowsAtIndexPaths:@[path] withRowAnimation:(UITableViewRowAnimationAutomatic)];
                }else{
                    if (_headImgH!=1.0) {
                        _headImgH = 1.0 ;
                        [_tabView reloadRowsAtIndexPaths:@[path] withRowAnimation:(UITableViewRowAnimationAutomatic)];
                    }
                }
            }else{
                if (_headImgH==ScreenW/2.8||_headImgH==1.0) {
                    _headImgH = ScreenW*image.size.height/image.size.width;
                    [_tabView reloadRowsAtIndexPaths:@[path] withRowAnimation:(UITableViewRowAnimationAutomatic)];
                }
            }
        }];
        return imgcell ;
    }
    
    TxtCell * txtcell = [tableView dequeueReusableCellWithIdentifier:@"txtcell" forIndexPath:indexPath];
    txtcell.labTitle.text =  REPLACENULL( NULLDIC(_dataAry[indexPath.row])[@"title"] ) ;
    txtcell.accessoryType=UITableViewCellAccessoryDisclosureIndicator ;
    return txtcell ;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        NSDictionary * dic = NULLDIC(_dataAry[indexPath.row]) ;
        [WebVC pushWebViewUrlOrStr:REPLACENULL(dic[@"content"]) title:REPLACENULL(dic[@"title"]) isSHowType:1];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
