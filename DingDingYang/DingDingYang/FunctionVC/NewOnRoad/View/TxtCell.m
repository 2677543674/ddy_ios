//
//  TxtCell.m
//  DingDingYang
//
//  Created by ddy on 2017/6/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "TxtCell.h"

@implementation TxtCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        _labTitle=[UILabel labText:@"标题" color:Black51 font:13*HWB];
        [self.contentView addSubview:_labTitle];
        [_labTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);
            make.right.offset(-10);
            make.top.offset(0);
            make.bottom.offset(0);
//            make.centerY.equalTo(self.mas_centerY);
        }];
        
        UILabel*lablines=[[UILabel alloc]init];
        lablines.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:lablines];
        [lablines mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.height.offset(1);
            make.bottom.offset(-1); make.right.offset(10);
        }];
    }
    return self ;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end




@implementation ImgCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.clipsToBounds = YES ;
        _imgView = [[UIImageView alloc]init];
        _imgView.contentMode = UIViewContentModeScaleAspectFill ;
        [self.contentView addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0) ; make.right.offset(0);
            make.bottom.offset(0); make.top.offset(0);
        }];
    }
    return self ;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


