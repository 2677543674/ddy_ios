//
//  LiveTimeModel.h
//  DingDingYang
//
//  Created by ddy on 2017/5/24.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveTimeModel : NSObject

@property(nonatomic,copy) NSString * endTime ;
@property(nonatomic,copy) NSString * ltid ; // 直播ID
@property(nonatomic,copy) NSString * name ; // 名称
@property(nonatomic,copy) NSString * startTime ; //开始时间点
@property(nonatomic,assign) NSInteger  ifLiving ; //正在抢购 0：否（已抢），1：是（正在抢），2：即将
@property(nonatomic,assign) NSInteger  restTime ; //剩余时间(毫秒)
@property(nonatomic,copy) NSString * sort ; //

-(instancetype)initWithDic:(NSDictionary*)dic;

@end
