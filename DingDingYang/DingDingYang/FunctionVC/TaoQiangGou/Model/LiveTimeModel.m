//
//  LiveTimeModel.m
//  DingDingYang
//
//  Created by ddy on 2017/5/24.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "LiveTimeModel.h"

@implementation LiveTimeModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.endTime = REPLACENULL(dic[@"endTime"]) ;
        self.ltid = REPLACENULL(dic[@"id"]) ;
        self.ifLiving = [REPLACENULL(dic[@"ifLiving"]) integerValue] ;
        self.name = REPLACENULL(dic[@"name"]) ;
        self.restTime = [REPLACENULL(dic[@"restTime"]) integerValue] ;
        self.sort = REPLACENULL(dic[@"sort"]);
        self.startTime = REPLACENULL(dic[@"startTime"]) ;
    }
    return self ;
}
@end
