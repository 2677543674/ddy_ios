//
//  LiveTimeView.m
//  DingDingYang
//
//  Created by ddy on 2017/5/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define NOWBGCOLOR [UIColor colorWithRed:44.f/255.f green:56.f/255.f blue:70.f/255.f alpha:1.0]
#define LVCCOLOR [UIColor colorWithRed:246.f/255.f green:90.f/255.f blue:108.f/255.f alpha:1.0]
#import "LiveTimeView.h"

@implementation LiveTimeView

-(void)setLtimeAry:(NSMutableArray *)ltimeAry{

    _ltimeAry = ltimeAry ;
    for (NSInteger i=0; i<_ltimeAry.count ; i++) {
        LiveTimeModel*model = _ltimeAry[i];
        if (model.ifLiving==1) {
            _indexRow = i;
            _nowRow = i ;
            [self hidenTime:NO];
            [self.delegate shouldSelectliveTimeId:model];
            [self startCountDown:model.restTime/1000];
            
        }
    }
    [_collectionTime reloadData];

}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORGROUP ;

        _nowRow = 100 ;
        _indexRow = 0 ;
        [self bgviewTop];
        [self yesterDay];
        [self tomorrow];
        
        [self collectionTime];
        
        [self bgviewBot];
        [self buycarImgV];
        [self tishiLab];
        
        [self creatCountdownLab];
        [self endTimeLab];
    }
    return self;
}

-(UIView*)bgviewTop{
    if (!_bgviewTop) {
        _bgviewTop = [[UIView alloc]init];
        _bgviewTop.backgroundColor = NOWBGCOLOR ;
        _bgviewTop.frame = CGRectMake(0, 0, ScreenW, 56);
        [self addSubview:_bgviewTop];
    }
    return _bgviewTop ;
}

-(UIView*)bgviewBot{
    if (!_bgviewBot) {
        _bgviewBot = [[UIView alloc]init];
        _bgviewBot.backgroundColor = COLORGROUP ;
        _bgviewBot.frame = CGRectMake(0, 56, ScreenW, 30);
        [self addSubview:_bgviewBot];
    }
    return _bgviewBot ;
}
-(UIButton*)yesterDay{
    if (!_yesterDay) {
//        _yesterDay = [UIButton titLe:@"昨日回顾" bgColor:NOWBGCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13.0*HWB];
//        _yesterDay.frame = CGRectMake(5, 15, 68*HWB , 30);
//        [_yesterDay boardWidth:1 boardColor:COLORWHITE corner:15];
        _yesterDay = [UIButton buttonWithType:UIButtonTypeCustom];
        [_yesterDay setImage:[UIImage imageNamed:@"arrowl"] forState:UIControlStateNormal];
        _yesterDay.frame = CGRectMake(0, 0, 30, 56);
        [_yesterDay setImageEdgeInsets:UIEdgeInsetsMake(13, 0, 13, 0)];
        [_bgviewTop addSubview:_yesterDay];
    }
    return _yesterDay ;
}

-(UIButton*)tomorrow{
    if (!_tomorrow) {
//        _tomorrow = [UIButton titLe:@"明日预告" bgColor:NOWBGCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:13.0*HWB];
//        _tomorrow.frame = CGRectMake(ScreenW-68*HWB-5, 15, 68*HWB, 30);
//        [_tomorrow boardWidth:1 boardColor:COLORWHITE corner:15];
        _tomorrow = [UIButton buttonWithType:UIButtonTypeCustom];
        [_tomorrow setImage:[UIImage imageNamed:@"arrowr"] forState:UIControlStateNormal];
        _tomorrow.frame = CGRectMake(ScreenW-30, 0, 30, 56);
        [_tomorrow setImageEdgeInsets:UIEdgeInsetsMake(13, 0, 13, 0)];
        [_bgviewTop addSubview:_tomorrow];
    }
    return _tomorrow ;
}

#pragma mark --> collectionTime 时间处理
-(UICollectionView*)collectionTime{
    if (!_collectionTime) {
        //表格布局加载图片
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        //设置单元格的大小  (ScreenW-(68*HWB+10)*2)/3
        layout.itemSize = CGSizeMake(80, 54);
        layout.minimumInteritemSpacing =0;
        layout.minimumLineSpacing =0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.sectionInset = UIEdgeInsetsMake(1, 1, 1, 1);
        
        _collectionTime = [[UICollectionView alloc]initWithFrame:CGRectMake(30, 0, ScreenW-60,56) collectionViewLayout:layout];
        _collectionTime.delegate = self ;
        _collectionTime.dataSource = self ;
        _collectionTime.showsHorizontalScrollIndicator = NO ;
        _collectionTime.backgroundColor = NOWBGCOLOR ;
        [_bgviewTop addSubview:_collectionTime];
        [_collectionTime registerClass:[CTimeCell class] forCellWithReuseIdentifier:@"ctimecell"];
    }
    return _collectionTime ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    NSLog(@"--->：%lu",(unsigned long)_ltimeAry.count);
    return _ltimeAry.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CTimeCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ctimecell" forIndexPath:indexPath];
    LiveTimeModel*model = _ltimeAry[indexPath.row];
    cell.stimeLab.text = model.name ;
    cell.contentView.backgroundColor = NOWBGCOLOR;
    switch (model.ifLiving) {
        case 0:{ cell.stateLab.text = @"已开抢" ; } break;
        case 1:{ cell.stateLab.text = @"正在抢购" ; } break;
        case 2:{ cell.stateLab.text = @"即将开场" ; } break;
            
        default:  break;
    }
    if (_indexRow==indexPath.row) {
        cell.contentView.backgroundColor = LVCCOLOR ;
    }

    if (_nowRow==_indexRow&&_indexRow==indexPath.row) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.16 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionTime scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            });
        });
    }

    return cell ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _indexRow = indexPath.row ;
    
    LiveTimeModel * model = _ltimeAry[indexPath.row];
    
    [self.delegate shouldSelectliveTimeId:model];
    
    if (indexPath.row==_nowRow) { [self hidenTime:NO]; }
    else{ [self hidenTime:YES]; }
    
    [_collectionTime reloadData];
    
    if (_nowRow!=indexPath.row) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                 [_collectionTime scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            });
        });
    }

}


//购物车小图片
-(UIImageView*)buycarImgV{
    if (!_buycarImgV) {
        _buycarImgV = [[UIImageView alloc]init];
        _buycarImgV.image = [UIImage imageNamed:@"liveshoping"];
        [_bgviewBot addSubview:_buycarImgV];
        [_buycarImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(8); make.centerY.equalTo(_bgviewBot.mas_centerY);
            make.height.offset(16); make.width.offset(18);
        }];
    }
    return _buycarImgV;
}

-(UILabel*)tishiLab{
    if (!_tishiLab) {
        _tishiLab = [UILabel labText:@"限时限量 疯狂抢购" color:LVCCOLOR font:12.5*HWB];
        [_bgviewBot addSubview:_tishiLab];
        [_tishiLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_buycarImgV.mas_centerY);
            make.left.equalTo(_buycarImgV.mas_right).offset(3);
        }];
    }
    return _tishiLab ;
}

-(void)creatCountdownLab{
    [self secondsLab];
    [self colon2];
    [self minutesLab];
    [self colon1];
    [self hoursLab];
}
-(UILabel*)secondsLab{
    if (!_secondsLab) {
        _secondsLab = [UILabel labText:@"00" color:COLORWHITE font:13.0*HWB];
        _secondsLab.backgroundColor = ColorRGBA(0, 0, 0, 1) ;
        _secondsLab.layer.masksToBounds = YES ;
        _secondsLab.layer.cornerRadius = 3*HWB ;
        _secondsLab.textAlignment = NSTextAlignmentCenter ;
        [_bgviewBot addSubview:_secondsLab];
        CGSize hw = StringSize(@"66", 13.0*HWB);
        [_secondsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10); make.centerY.equalTo(_bgviewBot.mas_centerY);
            make.height.offset(hw.height+2); make.width.offset(hw.width+4);
        }];
    }
    return _secondsLab ;
}
-(UILabel*)colon2{
    if (!_colon2) {
        _colon2 = [UILabel labText:@":" color:Black51 font:14.0*HWB];
        _colon2.font = [UIFont boldSystemFontOfSize:14.0*HWB];
        _colon2.textAlignment = NSTextAlignmentCenter ;
        [_bgviewBot addSubview:_colon2];
        [_colon2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_secondsLab.mas_left).offset(-1);
            make.centerY.equalTo(_bgviewBot.mas_centerY);
            make.width.offset(5);
        }];
    }
    return _colon2 ;
}

-(UILabel*)minutesLab{
    if (!_minutesLab) {
        _minutesLab = [UILabel labText:@"00" color:COLORWHITE font:13.0*HWB];
        _minutesLab.backgroundColor = ColorRGBA(0, 0, 0, 1) ;
        _minutesLab.textAlignment = NSTextAlignmentCenter ;
        _minutesLab.layer.masksToBounds = YES ;
        _minutesLab.layer.cornerRadius = 3*HWB ;
        [_bgviewBot addSubview:_minutesLab];
        CGSize hw = StringSize(@"66", 13.0*HWB);
        [_minutesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_colon2.mas_left).offset(-1);
            make.centerY.equalTo(_bgviewBot.mas_centerY);
            make.height.offset(hw.height+2); make.width.offset(hw.width+4);
        }];
    }
    return _minutesLab ;
}

-(UILabel*)colon1{
    if (!_colon1) {
        _colon1 = [UILabel labText:@":" color:Black51 font:14.0*HWB];
        _colon1.textAlignment = NSTextAlignmentCenter ;
        _colon1.font = [UIFont boldSystemFontOfSize:14.0*HWB];
        [_bgviewBot addSubview:_colon1];
        [_colon1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_minutesLab.mas_left).offset(-1);
            make.centerY.equalTo(_bgviewBot.mas_centerY);
            make.width.offset(5);
        }];
    }
    return _colon1 ;
}

-(UILabel*)hoursLab{
    if (!_hoursLab) {
        _hoursLab = [UILabel labText:@"00" color:COLORWHITE font:13.0*HWB];
        _hoursLab.backgroundColor = ColorRGBA(0, 0, 0, 1) ;
        _hoursLab.layer.masksToBounds = YES ;
        _hoursLab.layer.cornerRadius = 3*HWB ;
        _hoursLab.textAlignment = NSTextAlignmentCenter ;
        [_bgviewBot addSubview:_hoursLab];
        CGSize hw = StringSize(@"66", 13.0*HWB);
        [_hoursLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_colon1.mas_left).offset(-1);
            make.centerY.equalTo(_bgviewBot.mas_centerY);
            make.height.offset(hw.height+2); make.width.offset(hw.width+4);
        }];
    }
    return _hoursLab ;
}

-(UILabel*)endTimeLab{
    if (!_endTimeLab) {
        _endTimeLab = [UILabel labText:@"距离本场结束:" color:Black102 font:12.5*HWB];
        [_bgviewBot addSubview:_endTimeLab];
        [_endTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_hoursLab.mas_left).offset(-5);
            make.centerY.equalTo(_bgviewBot.mas_centerY);
        }];
    }
    return _endTimeLab ;
}

#pragma mark -- > 倒计时

-(void)startCountDown:(NSInteger)time{
    
    __block NSInteger timeOut = time ;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _cttimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_cttimer, dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_cttimer, ^{
        if (_cttimer!=nil) {
            if (timeOut <= 0) {
                
                dispatch_source_cancel(_cttimer);
                _cttimer = nil ;
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_hoursLab!=nil) {
                        [[NSNotificationCenter defaultCenter]postNotificationName:RefreshLiveTime object:nil];
                        _hoursLab.text = @"00";
                        _minutesLab.text = @"00";
                        _secondsLab.text = @"00";
                    }
                });
            } else {
                if (_indexRow == _nowRow) {
                    NSInteger days = (NSInteger)(timeOut/(3600*24));
                    NSInteger hours = (NSInteger)((timeOut-days*24*3600)/3600);
                    NSInteger minute = (NSInteger)(timeOut-days*24*3600-hours*3600)/60;
                    NSInteger second = timeOut-days*24*3600-hours*3600-minute*60;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (_hoursLab!=nil) {
                            _hoursLab.text = FORMATSTR(@"%.2ld",(long)hours) ;
                            _minutesLab.text = FORMATSTR(@"%.2ld",(long)minute);
                            _secondsLab.text = FORMATSTR(@"%.2ld",(long)second);
                        }
                    });
                }
                timeOut--;
            }
        }
    });
    dispatch_resume(_cttimer);
}

-(void)hidenTime:(BOOL)yn{
    _endTimeLab.hidden = yn ;
    _colon1.hidden = yn ;
    _colon2.hidden = yn ;
    _hoursLab.hidden = yn ;
    _minutesLab.hidden = yn ;
    _secondsLab.hidden = yn ;
}
//-(NSInteger)shijiancha{
//    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970]; //当前时间戳
//    NSString * str = FORMATSTR(@"%.0f",interval);
//    return [str integerValue] ;
    
//}


//dispatch_source_t CreateDispatchTimer(uint64_t interval,
//                                      uint64_t leeway,
//                                      dispatch_queue_t queue,
//                                      dispatch_block_t block)
//{
//    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER,
//                                                     0, 0, queue);
//    if (timer)
//    {
//        dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), interval, leeway);
//        dispatch_source_set_event_handler(timer, block);
//        dispatch_resume(timer);
//    }
//    return timer;
//}


#pragma mark -- > 昨天和明天
//-(void)clickYesterday{
//    [self.delegate yesterdayOrTomorrow:1];
//}
//-(void)clickTomorrow{
//    [self.delegate yesterdayOrTomorrow:2];
//}

//-(void)dealloc{
//    NSLog(@"定时器倒计时定时器取消。。。");
//    dispatch_source_cancel(_cttimer);
//    _cttimer = nil ;
//}


@end



@implementation CTimeCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = NOWBGCOLOR ;
        self.contentView.layer.masksToBounds = YES ;
        self.contentView.layer.cornerRadius = 3 ;
        [self stimeLab];
        [self stateLab];
    }
    return self;
}

-(UILabel*)stimeLab{
    if (!_stimeLab) {
        _stimeLab = [UILabel labText:@"场次" color:COLORWHITE font:14.0*HWB];
        [self.contentView addSubview:_stimeLab];
        [_stimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.equalTo(self.mas_centerY).offset(-1);
        }];
    }
    return _stimeLab ;
}

-(UILabel*)stateLab{
    if (!_stateLab) {
        _stateLab = [UILabel labText:@"正在抢购" color:COLORWHITE font:12.5*HWB];
        _stateLab.textAlignment = NSTextAlignmentCenter ;
        _stateLab.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_stateLab];
        [_stateLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.mas_centerY).offset(1);
            make.left.offset(0); make.right.offset(0);
        }];
    }
    return _stateLab ;
}
@end















