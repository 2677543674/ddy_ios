//
//  LiveTimeView.h
//  DingDingYang
//
//  Created by ddy on 2017/5/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LiveTimeModel.h"

@protocol LiveTimeDelegate <NSObject>
@optional

//-(void)timeEnd ;

//-(void)yesterdayOrTomorrow:(NSInteger)yt; //昨日回顾和明日预告

-(void)shouldSelectliveTimeId:(LiveTimeModel*)ltmd ;
//-(void)shouldReloadTime ;

@end

@interface LiveTimeView : UIView <UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UIView * bgviewTop , * bgviewBot ;
@property(nonatomic,strong) UICollectionView * collectionTime ;
//@property(nonatomic,strong) UIScrollView * timeScrollV;
@property(nonatomic,strong) UIButton * yesterDay,*tomorrow ;

@property(nonatomic,strong) UIImageView * buycarImgV ;
@property(nonatomic,strong) UILabel * tishiLab ;
@property(nonatomic,strong) UILabel * endTimeLab ;
@property(nonatomic,strong) UILabel * colon1 , *colon2 ;
@property(nonatomic,strong) UILabel * hoursLab , * minutesLab , * secondsLab ;
@property(nonatomic,assign) NSInteger indexRow ; //记录选中行
@property(nonatomic,assign) NSInteger nowRow ; //记录当前时间所在行
@property(nonatomic,strong) dispatch_source_t cttimer;

@property(nonatomic,strong) NSMutableArray * ltimeAry ;

@property(nonatomic,assign) id<LiveTimeDelegate>delegate ;

@end


@interface CTimeCell : UICollectionViewCell
@property(nonatomic,strong) UILabel * stimeLab ;
@property(nonatomic,strong) UILabel * stateLab ;
@end














