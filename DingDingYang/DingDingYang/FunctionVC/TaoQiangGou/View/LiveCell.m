//
//  LiveCell.m
//  DingDingYang
//
//  Created by ddy on 2017/5/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define LEFTWIDTH   [UIScreen mainScreen].bounds.size.width/4
#define LVCCOLOR [UIColor colorWithRed:246.f/255.f green:90.f/255.f blue:108.f/255.f alpha:1.0]

#import "LiveCell.h"

@implementation LiveCell

-(void)setModelDoods:(GoodsModel *)modelDoods{
    _modelDoods = modelDoods ;
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:modelDoods.smallPic]];
    _goodsTitleLab.text = modelDoods.goodsName ;
    _catPLab.text = FORMATSTR(@"原价:%@元",modelDoods.price) ;
    _salesLab.text = FORMATSTR(@"已抢%@件",modelDoods.sales) ;
    
    
    switch (_modelDoods.ifGet) {
        case 0:{ [_takeOrderBtn setTitle:@"已开抢" forState:UIControlStateNormal]; } break;
        case 1:{ [_takeOrderBtn setTitle:@"马上抢" forState:UIControlStateNormal]; } break;
        case 2:{ [_takeOrderBtn setTitle:@"即将开抢" forState:UIControlStateNormal]; } break;
        default:   break;
    }
    
    _vouchersMoneyLab.text = FORMATSTR(@"%@",modelDoods.endPrice) ;
    
    if (role_state!=0) {
        _orderLab.text = @"分享赚" ;
        _couponAmountLab.text = FORMATSTR(@"¥ %@",modelDoods.commission);
    }else{
        if ([_modelDoods.couponMoney floatValue]>0) {
            _couponAmountLab.text = FORMATSTR(@"¥ %@",modelDoods.couponMoney);
            _couponAmountLab.hidden = NO ;
        }else{
            _couponAmountLab.hidden = YES ;
            _orderLab.text = @"立即下单" ;
        }
    }
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORGROUP ;
        _cellH = FloatKeepInt(100*HWB) ;
        [self leftView];
        [self rightView];
        
        [self goodsImgV];
        [self goodsTitleLab];
        [self catPLab];
        [self salesLab];
        [self vouchersPLab];
        [self vouchersMoneyLab];
        [self couponAmountLab];
        [self orderLab];
        [self takeOrderBtn];
        
    }
    return self;
}

//-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        self.contentView.backgroundColor = COLORGROUP ;
//        self.selectionStyle = UITableViewCellSelectionStyleNone ;
//
//        _cellH = FloatKeepInt(100*HWB) ;
//        [self leftView];
//        [self rightView];
//
//        [self goodsImgV];
//        [self goodsTitleLab];
//        [self catPLab];
//        [self salesLab];
//        [self vouchersPLab];
//        [self vouchersMoneyLab];
////        [self baoyouLab];
//
//        [self couponAmountLab];
//        [self orderLab];
//        [self takeOrderBtn];
//
//    }
//    return self;
//}


#pragma mark --> 左侧底部View
-(UIView*)leftView{
    if (!_leftView) {
        _leftView = [[UIView alloc]init];
        _leftView.backgroundColor = COLORWHITE ;
        _leftView.frame = CGRectMake(0, 0 , LEFTWIDTH*3, _cellH-1);
        [self.contentView addSubview:_leftView];
        
    }
    return _leftView;
}

#pragma mark --> 右侧View
-(UIView*)rightView{
    if (!_rightView) {
        _rightView = [[UIView alloc]init];
        _rightView.backgroundColor = COLORWHITE ;
        _rightView.frame = CGRectMake(LEFTWIDTH*3-1, 0, LEFTWIDTH+1 , _cellH-1);
        //        UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takeOrder)];
        //        [_rightView  addGestureRecognizer:tap];
        [self.contentView addSubview:_rightView];
    }
    return _rightView;
}


#pragma mark --> 商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"zbyg"]];
        _goodsImgV.backgroundColor = COLORGROUP;
        _goodsImgV.frame = CGRectMake(8, 10*HWB, _cellH-20*HWB, _cellH-20*HWB);
        [_leftView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}

#pragma mark --> 商品标题
-(UILabel*)goodsTitleLab{
    if (!_goodsTitleLab) {
        _goodsTitleLab = [UILabel labText:@"商品标题" color:Black51 font:12*HWB];
        _goodsTitleLab.numberOfLines = 2 ;
        [_leftView addSubview:_goodsTitleLab];
        [_goodsTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.offset(10*HWB);  make.right.offset(-2);
        }];
    }
    return _goodsTitleLab;
}

#pragma mark --> 天猫价
-(UILabel*)catPLab{
    if (!_catPLab) {
        _catPLab = [UILabel labText:@"原价:00.00元" color:Black102 font:10*HWB];
        [_leftView addSubview:_catPLab];
        [_catPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.top.equalTo(self.mas_centerY).offset(2);
            make.height.offset(14);
        }];
    }
    return _catPLab;
}

#pragma mark --> 销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"已抢1000件" color:LVCCOLOR font:10*HWB];
        [_leftView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5);  make.height.offset(14);
            make.centerY.equalTo(_catPLab.mas_centerY);
        }];
    }
    return _salesLab;
}

#pragma mark --> 券后价
-(UILabel*)vouchersPLab{
    if (!_vouchersPLab) {
        _vouchersPLab = [UILabel labText:@"券后价: " color:LVCCOLOR font:12*HWB];
        [_leftView addSubview:_vouchersPLab];
        [_vouchersPLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_goodsImgV.mas_right).offset(5);
            make.bottom.offset(-10*HWB);
        }];
    }
    return _vouchersPLab;
}

#pragma mark --> 券后价价格
-(UILabel*)vouchersMoneyLab{
    if (!_vouchersMoneyLab) {
        _vouchersMoneyLab = [UILabel labText:@"00.00" color:LVCCOLOR font:13*HWB];
        [_leftView addSubview:_vouchersMoneyLab];
        [_vouchersMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersPLab.mas_right);
            make.centerY.equalTo(_vouchersPLab.mas_centerY);
        }];
        
        UILabel * yuanLab = [UILabel labText:@"元" color:LVCCOLOR font:12*HWB];
        [_leftView addSubview:yuanLab];
        [yuanLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersMoneyLab.mas_right).offset(1);
            make.centerY.equalTo(_vouchersMoneyLab.mas_centerY);
        }];
        
    }
    return _vouchersMoneyLab;
}

#pragma mark --> 包邮
-(UILabel*)baoyouLab{
    if (!_baoyouLab) {
        _baoyouLab = [UILabel labText:@"包邮" color:COLORWHITE font:10*HWB];
        _baoyouLab.backgroundColor = LVCCOLOR;
        _baoyouLab.textAlignment = NSTextAlignmentCenter ;
        _baoyouLab.layer.masksToBounds = YES ;
        _baoyouLab.layer.cornerRadius = 8;
        [_leftView addSubview:_baoyouLab];
        [_baoyouLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_vouchersMoneyLab.mas_right).offset(3);
            make.height.offset(16);
            make.width.offset(33);
            make.centerY.equalTo(_vouchersPLab.mas_centerY);
        }];
    }
    return _baoyouLab;
}

#pragma mark --> 优惠券金额
-(UILabel*)couponAmountLab{
    if (!_couponAmountLab) {
        _couponAmountLab = [UILabel labText:@"¥0" color:LVCCOLOR font:15*HWB];
        [_rightView addSubview:_couponAmountLab];
        [_couponAmountLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_rightView.mas_centerY).offset(3);
            make.centerX.equalTo(_rightView.mas_centerX);
            make.height.offset(20);
        }];
    }
    return _couponAmountLab ;
}

#pragma mark --> 下单立省
-(UILabel*)orderLab{
    if (!_orderLab) {
        _orderLab = [UILabel labText:@"下单立省" color:LVCCOLOR font:13*HWB];
        [_rightView addSubview:_orderLab];
        [_orderLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_rightView.mas_centerX);
            make.bottom.equalTo(_rightView.mas_centerY).offset(-17*HWB);
            make.height.offset(16);
        }];
    }
    return _orderLab;
}

#pragma mark --> 按钮，显示价格
-(UIButton*)takeOrderBtn{
    if (!_takeOrderBtn) {
        _takeOrderBtn = [UIButton titLe:@"马上抢" bgColor:LVCCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:FONT_13*HWB];
        _takeOrderBtn.layer.cornerRadius = 15 ;
        ADDTARGETBUTTON(_takeOrderBtn, takeOrder)
        [_rightView addSubview:_takeOrderBtn];
        [_takeOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_rightView.mas_centerY).offset(6);
            make.height.offset(30);
            make.width.offset(68*HWB);
            make.centerX.equalTo(_rightView.mas_centerX);
        }];
    }
    return _takeOrderBtn;
}
-(void)takeOrder{
    if (TOKEN.length==0&&TopNavc!=nil) {
        [LoginVc showLoginVC:TopNavc.topViewController fromType:2 loginBlock:^(NSInteger result) {
            if (result==1) {  [self takeOrderIslogin]; }
        }];
    }else{
        [self takeOrderIslogin];
    }
   
}

-(void)takeOrderIslogin{
    if (_modelDoods.ifGet==1) {
        [PageJump takeOrder:_modelDoods];
    }
}


@end
