//
//  LiveCell.h
//  DingDingYang
//
//  Created by ddy on 2017/5/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveCell : UICollectionViewCell

@property(nonatomic,strong) UIView      * leftView ;         //最底部(左边View)
@property(nonatomic,strong) UIImageView * goodsImgV ;        //商品图片
@property(nonatomic,strong) UILabel     * goodsTitleLab ;    //商品标题
@property(nonatomic,strong) UILabel     * catPLab ;          //天猫价
@property(nonatomic,strong) UILabel     * salesLab ;         //已抢多少
@property(nonatomic,strong) UILabel     * vouchersPLab ;     //券后价:
@property(nonatomic,strong) UILabel     * vouchersMoneyLab ; //券后价价格
@property(nonatomic,strong) UILabel     * baoyouLab ;        //包邮

@property(nonatomic,strong) UIView      * rightView ;        //右边View
@property(nonatomic,strong) UILabel     * orderLab ;         //下单立省
@property(nonatomic,strong) UILabel     * couponAmountLab ;  //优惠券金额
@property(nonatomic,strong) UIButton    * takeOrderBtn ;     //下单按钮

@property(nonatomic,assign) float         cellH ;            //表格高度

@property(nonatomic,strong) GoodsModel  * modelDoods ;       

@end
