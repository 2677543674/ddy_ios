//
//  CouponsLiveVC.m
//  DingDingYang
//
//  Created by ddy on 2017/5/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "CouponsLiveVC.h"
#import "LiveTimeView.h"
#import "LiveCell.h"
#import "BlankCell.h"
#import "LiveTimeModel.h"
#import "GoodsListCltCell1.h"
@interface CouponsLiveVC ()<UICollectionViewDelegate,UICollectionViewDataSource,LiveTimeDelegate>

@property(nonatomic,strong) NSMutableDictionary * parameterDic;
@property(nonatomic,strong) UICollectionView * universalListCltv;
@property(nonatomic,strong) UIButton * topBtn ;
@property(nonatomic,strong) NSMutableArray * timeAry ; //时间
@property(nonatomic,strong) LiveTimeView * timeView ;
@property(nonatomic,strong) NSMutableArray * listAry ;
@property(nonatomic,assign) NSInteger pageN ;
@property(nonatomic,assign) NSInteger goodsListStyle;
@end

@implementation CouponsLiveVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORWHITE ;
    _timeAry = [NSMutableArray array];
    _listAry = [NSMutableArray array];
    
    _pageN = 2 ;
    self.title=_mcsModel.title.length>0?_mcsModel.title:@"淘抢购";
    [self parameterDic];
    [self timeView];
    [self tabView];

    
    [self requestTime];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestTime) name:EnterBackground object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestTime) name:RefreshLiveTime object:nil];
}

//获取直播时间段
-(void)requestTime{
    
    [NetRequest requestTokenURL:url_tbqg_time parameter:nil success:^(NSDictionary * nwdic) {
        if (_timeAry.count>0) { [_timeAry removeAllObjects]; }
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray * ary = nwdic[@"list"] ;
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                LiveTimeModel * modelL = [[LiveTimeModel alloc]initWithDic:obj];
                [_timeAry addObject:modelL];
            }];
            _timeView.ltimeAry = _timeAry ;
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestTime];
        }];
    }];
    
}


-(NSMutableDictionary*)parameterDic{
    if (!_parameterDic) {
        _parameterDic = [NSMutableDictionary dictionary];
        _parameterDic[@"st"] = @"1" ;
    }
    return _parameterDic ;
}
#pragma mark --> 获取直播商品列表
-(void)requestLiveData{
    [_universalListCltv.mj_footer resetNoMoreData];
    _parameterDic[@"st"] = @"1" ;
    _pageN = 2 ;
    [NetRequest requestTokenURL:url_tbqg_goodsList parameter:_parameterDic success:^(NSDictionary *nwdic) {
        
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            if (_listAry.count>0) {
                [_listAry removeAllObjects];
                [_universalListCltv reloadData];
            }
            NSArray*lAry=nwdic[@"list"];
            if (lAry.count>0) {
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_listAry addObject:model];
                }];
            }
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        [_universalListCltv.mj_header endRefreshing];
        [_universalListCltv reloadData];
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [_universalListCltv.mj_header endRefreshing];
    }];
}
-(void)loadMoreData{
    _parameterDic [@"st"] = FORMATSTR(@"%ld",(long)_pageN);
    [NetRequest requestTokenURL:url_tbqg_goodsList parameter:_parameterDic success:^(NSDictionary *nwdic) {
        MBHideHUD
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry=nwdic[@"list"];
            if (lAry.count>0) {
                _pageN ++ ;
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_listAry addObject:model];
                }];
                 [_universalListCltv.mj_footer endRefreshing];
            }else{
                [_universalListCltv.mj_footer endRefreshingWithNoMoreData];
            }
            [_universalListCltv reloadData];
        }else{
            [_universalListCltv.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        
    } failure:^(NSString *failure) {
         [_universalListCltv.mj_footer endRefreshing];
        MBHideHUD
    }];
   
}
#pragma mark --> 时间控制view
-(LiveTimeView*)timeView{
    if (!_timeView) {
        _timeView = [[LiveTimeView alloc]initWithFrame:CGRectMake(0, 0,ScreenW, 86)];
        _timeView.delegate = self ;
        [self.view addSubview:_timeView];
    }
    return _timeView ;
}
//更换商品(更换参数)
#pragma mark --> LiveTimeView 的 代理
-(void)shouldSelectliveTimeId:(LiveTimeModel*)ltmd{
    _parameterDic[@"liveTimeId"] = ltmd.ltid ;
    _parameterDic[@"st"] = @"1" ;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_universalListCltv.mj_header beginRefreshing];
    });
}


#pragma mark --> UICollectionView
-(UICollectionView*)tabView{
    if (!_universalListCltv) {
        
        float hi = ScreenH-150;
        if (_isOnePage==YES) { hi = ScreenH-150-TABBAR_HEIGHT; }
        UICollectionViewFlowLayout * layoutUnvsl = [[UICollectionViewFlowLayout alloc]init];
        _universalListCltv = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0+86, ScreenW, hi) collectionViewLayout:layoutUnvsl];
        _universalListCltv.delegate = self ;
        _universalListCltv.dataSource = self ;
        _universalListCltv.backgroundColor = COLORGROUP ;
        _universalListCltv.showsVerticalScrollIndicator = NO ;
        [self.view addSubview:_universalListCltv];

        [_universalListCltv registerClass:[LiveCell class] forCellWithReuseIdentifier:@"livecell"];
//        [_universalListCltv registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"goods_listclt_cell1"];
//        [_universalListCltv registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"goods_listclt_cell2"];
//        [_universalListCltv registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"goods_listclt_cell3"];
//        [_universalListCltv registerClass:[GoodsListCltCell4   class] forCellWithReuseIdentifier:@"goods_listclt_cell4"];
//        [_universalListCltv registerClass:[GoodsListCltCell5   class] forCellWithReuseIdentifier:@"goods_listclt_cell5"];
//        [_universalListCltv registerClass:[GoodsListCltCell6   class] forCellWithReuseIdentifier:@"goods_listclt_cell6"];
//        [_universalListCltv registerClass:[GoodsListCltCell7   class] forCellWithReuseIdentifier:@"goods_listclt_cell7"];
//        [_universalListCltv registerClass:[GoodsListCltCell10   class] forCellWithReuseIdentifier:@"goods_listclt_cell10"];
        
        __weak CouponsLiveVC * SelfView = self;
        
        _universalListCltv.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(requestLiveData)];
        
        _universalListCltv.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreData];
        }];
        
    }
    return _universalListCltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _listAry.count ;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    //商品列表样式
//    if(_goodsListStyle == 2)  { return CGSizeMake(ScreenW/2-2,ScreenW/2 + 70*HWB ); }
//    if(_goodsListStyle == 4) { return  CGSizeMake(ScreenW/2-2,ScreenW/2 + 90*HWB ); }
//    if(_goodsListStyle == 5) { return role_state!=0?CGSizeMake(ScreenW/2-2,ScreenW/2 + 90*HWB):CGSizeMake(ScreenW/2-2,ScreenW/2 + 70*HWB); }
//    if(_goodsListStyle == 6) { return  CGSizeMake(ScreenW, 116*HWB) ; }
//    if(_goodsListStyle == 7) { return  CGSizeMake(ScreenW, 104*HWB); }
//    if(_goodsListStyle == 8) { return CGSizeMake(ScreenW/2-2,ScreenW/2 + 90*HWB); }
    return CGSizeMake(ScreenW, FloatKeepInt(100*HWB));
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    if (_goodsListStyle == 1) {
//        GoodsListCltCell1 * goodsListClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell1" forIndexPath:indexPath];
//        goodsListClt1.goodsModel = _listAry[indexPath.row];
//
//        return goodsListClt1 ;
//    }else if (_goodsListStyle ==2){
//        GoodsListCltCell3 * goodsListClt3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell3" forIndexPath:indexPath];
//        goodsListClt3.goodsModel = _listAry[indexPath.row];
//
//        return goodsListClt3 ;
//    }else if (_goodsListStyle == 3){
//        GoodsListCltCell2 * goodsListClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell2" forIndexPath:indexPath];
//        goodsListClt2.goodsModel = _listAry[indexPath.row];
//
//        return goodsListClt2 ;
//    }else if (_goodsListStyle == 4){
//        GoodsListCltCell4 * goodsListClt4 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell4" forIndexPath:indexPath];
//        goodsListClt4.goodsModel = _listAry[indexPath.row];
//
//        return goodsListClt4 ;
//    }else if(_goodsListStyle == 5){//调试用 后台没确定字段
//        GoodsListCltCell5 * goodsListClt5 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell5" forIndexPath:indexPath];
//        goodsListClt5.goodsModel = _listAry[indexPath.row];
//
//        return goodsListClt5 ;
//    }else if(_goodsListStyle == 6){
//        GoodsListCltCell6 * goodsListClt6 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell6" forIndexPath:indexPath];
//        goodsListClt6.goodsModel = _listAry[indexPath.row];
//
//        return goodsListClt6 ;
//    }else if(_goodsListStyle == 7){
//        GoodsListCltCell7 * goodsListClt7 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell7" forIndexPath:indexPath];
//        goodsListClt7.goodsModel = _listAry[indexPath.row];
//
//        return goodsListClt7 ;
//    }else if(_goodsListStyle == 8){
//        GoodsListCltCell10 * goodsListClt10 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell10" forIndexPath:indexPath];
//        goodsListClt10.goodsModel = _listAry[indexPath.row];
//        return goodsListClt10 ;
//    }else{
        LiveCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"livecell" forIndexPath:indexPath];
        cell.modelDoods = _listAry[indexPath.row];
        return cell;
//    }
   
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
//    if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 5 ; }
    return 1 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
//    if(_goodsListStyle ==2||_goodsListStyle == 4||_goodsListStyle == 5) { return 4 ; }
    return 0 ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GoodsDetailsVC * detailVc = [[GoodsDetailsVC alloc]init] ;
    detailVc.pushType = 1 ;
    detailVc.hidesBottomBarWhenPushed = YES ;
    detailVc.dmodel = _listAry[indexPath.row] ;
    PushTo(detailVc, YES)
}


-(void)dealloc{
    //取消定时器
    if (_timeView.cttimer!=nil) {
        dispatch_source_cancel(_timeView.cttimer);
        _timeView.cttimer = nil ;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:EnterBackground object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RefreshLiveTime object:nil];
}

-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
        _topBtn.frame = CGRectMake(ScreenW-36*HWB-10, ScreenH-36*HWB-10-NAVCBAR_HEIGHT, 36*HWB, 36*HWB);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_universalListCltv.contentOffset.y>=0) {
        if (_universalListCltv.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_universalListCltv.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _universalListCltv.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
    }
}
-(void)clickBackTop{
    [_universalListCltv setContentOffset:CGPointZero animated:YES];
}



@end
