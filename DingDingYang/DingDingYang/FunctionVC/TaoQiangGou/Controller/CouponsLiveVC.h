//
//  CouponsLiveVC.h
//  DingDingYang
//
//  Created by ddy on 2017/5/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class MClassModel ;
@interface CouponsLiveVC : DDYViewController

//是否是一级界面 YES: 是(collectionView高度-49)  NO或nil:不是
@property(nonatomic,assign) BOOL isOnePage ;

@property(nonatomic,strong) MClassModel * mcsModel ;

@end
