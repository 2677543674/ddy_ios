//
//  FillOrderRewardVC.m
//  DingDingYang
//
//  Created by ddy on 2018/5/30.
//  Copyright © 2018年 ddy.All rights reserved.
//

#define BGCOLOR RGBA(232, 78, 116, 1)

#import "FillOrderRewardVC.h"
#import "FillOrderListCell.h"

@interface FillOrderRewardVC () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,strong) UIImageView * backImgView;
@property(nonatomic,strong) UIView * textFBackView;
@property(nonatomic,strong) UIButton * addOrderBtn;
@property(nonatomic,strong) UIButton * automaticBtn;
@property(nonatomic,strong) UITextField * orderFillText;

@end

@implementation FillOrderRewardVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP;
    
    self.title = @"填单奖励";
    [self creatTopView];
    [self tableView];
    
}

-(void)creatTopView{

    _backImgView = [[UIImageView alloc]init];
    _backImgView.userInteractionEnabled = YES;
    _backImgView.backgroundColor = BGCOLOR;
    [_backImgView cornerRadius:30*HWB];
    [self.view addSubview:_backImgView];
    [_backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.offset(-30*HWB);
        make.height.offset(120*HWB+NAVCBAR_HEIGHT);
    }];
    
    // 返回按钮、标题、问题按钮
    UIView * backView = [[UIImageView alloc]init];
    backView.backgroundColor = BGCOLOR;
    backView.userInteractionEnabled = YES;
    [_backImgView addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);  make.right.offset(0);
        make.top.offset(30*HWB); make.height.offset(NAVCBAR_HEIGHT);
    }];

    UILabel * titleLab = [UILabel labText:@"填单奖励" color:COLORWHITE font:16*HWB];
    titleLab.frame = CGRectMake(ScreenW/2-100, NAVCBAR_HEIGHT-44, 200, 44);
    titleLab.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:titleLab];

    UIButton * backBtn = [[UIButton alloc]init];
    [backBtn setImage:[UIImage imageNamed:@"arrowl"] forState:(UIControlStateNormal)];
    backBtn.frame = CGRectMake(0, NAVCBAR_HEIGHT-44, 44, 44);
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    ADDTARGETBUTTON(backBtn, clickBack)
    [backView addSubview:backBtn];
    
    UIButton * questionBtn = [[UIButton alloc]init];
    [questionBtn setImage:[UIImage imageNamed:@"white_shuo_ming"] forState:(UIControlStateNormal)];
    questionBtn.frame = CGRectMake(ScreenW-40-5*HWB, NAVCBAR_HEIGHT-42, 40, 40);
    [questionBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    ADDTARGETBUTTON(questionBtn, clickQuestion)
    [backView addSubview:questionBtn];
    
    
    // 填单输入框
    _textFBackView = [[UIView alloc]init];
    _textFBackView.backgroundColor = COLORWHITE;
    [_backImgView addSubview:_textFBackView];
    [_textFBackView boardWidth:1 boardColor:COLORWHITE cornerRadius:15*HWB];
    [_textFBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20*HWB); make.right.offset(-100*HWB);
        make.top.offset(NAVCBAR_HEIGHT+50*HWB);
        make.height.offset(30*HWB);
    }];

    _orderFillText = [UITextField textColor:Black51 PlaceHorder:@"请输入订单号" font:13*HWB];
    [_orderFillText addTarget:self action:@selector(keyboardDown) forControlEvents:(UIControlEventEditingDidEndOnExit)];
    _orderFillText.keyboardType = UIKeyboardTypeNumberPad;
    [_textFBackView addSubview:_orderFillText];
    [_orderFillText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(5*HWB); make.centerY.equalTo(_textFBackView.mas_centerY);
        make.height.offset(30); make.right.offset(-50*HWB);
    }];
    _addOrderBtn = [UIButton titLe:@"添加" bgColor:BGCOLOR titColorN:COLORWHITE font:12*HWB];
    ADDTARGETBUTTON(_addOrderBtn, clickAddOrder)
    [_textFBackView addSubview:_addOrderBtn];
    [_addOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.offset(0);
        make.width.offset(50*HWB);
    }];
    
    _automaticBtn = [UIButton titLe:@"自动添加" bgColor:COLORWHITE titColorN:BGCOLOR font:12*HWB];
    ADDTARGETBUTTON(_automaticBtn, clickAutomatic)
    [_automaticBtn cornerRadius:15*HWB];
    [_backImgView addSubview:_automaticBtn];
    [_automaticBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-20*HWB);  make.height.offset(30*HWB);
        make.centerY.equalTo(_textFBackView.mas_centerY);
        make.width.offset(70*HWB);
    }];

}

-(void)clickBack{ PopTo(YES) }
-(void)keyboardDown{ }
-(void)clickQuestion{ SHOWMSG(@"填单说明!") }


-(void)clickAddOrder{
    if (_orderFillText.text&&_orderFillText.text.length>15) {

        SHOWMSG(@"请求接口，添加订单！")
        
    }else{
        SHOWMSG(@"请输入正确的订单号!")
    }
}
-(void)clickAutomatic{
    NSLog(@"点击了自动获取订单。。。");
}



-(UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self; _tableView.dataSource = self;
        _tableView.rowHeight = 110*HWB;
        _tableView.backgroundColor = COLORCLEAR;
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.offset(0);
            make.top.equalTo(_backImgView.mas_bottom).offset(-15*HWB);
        }];
        [_tableView registerClass:[FillOrderListCell class] forCellReuseIdentifier:@"FillOrderListCell"];
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FillOrderListCell * listCell = [tableView dequeueReusableCellWithIdentifier:@"FillOrderListCell" forIndexPath:indexPath];
    return listCell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_orderFillText.isFirstResponder) {
        [_orderFillText resignFirstResponder];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
