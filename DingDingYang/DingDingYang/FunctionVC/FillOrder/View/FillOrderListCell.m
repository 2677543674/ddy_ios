//
//  FillOrderListCell.m
//  DingDingYang
//
//  Created by ddy on 2018/6/2.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "FillOrderListCell.h"

@implementation FillOrderListCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = COLORCLEAR;
        self.backgroundColor = COLORCLEAR;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self topBgView];
        
        [self goodsImageV];
        [self platform];
        [self nameLab];
        [self orderLab];
        [self stateOrTimeLab];
        [self rewardLab];
      

    }
    return self;
}

-(UIView*)topBgView{
    if (!_topBgView) {
        _topBgView = [[UIView alloc]init];
        _topBgView.backgroundColor = COLORWHITE;
        [_topBgView cornerRadius:10*HWB];
        [self.contentView addSubview:_topBgView];
        [_topBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15*HWB); make.right.offset(-15*HWB);
            make.top.offset(0); make.bottom.offset(-6*HWB);
        }];
    }
    return _topBgView ;
}

-(UIImageView *)goodsImageV{
    if (!_goodsImageV) {
        _goodsImageV = [UIImageView new];
//        _goodsImageV.image = [UIImage imageNamed:@"logo"];
        _goodsImageV.layer.cornerRadius = 5*HWB;
        _goodsImageV.clipsToBounds = YES;
//        _goodsImageV.backgroundColor = COLORRED;
        _goodsImageV.contentMode = UIViewContentModeScaleAspectFill;
        [_topBgView addSubview:_goodsImageV];
        [_goodsImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(15*HWB); make.left.offset(15*HWB);
            make.bottom.offset(-15*HWB);
            make.width.equalTo(_goodsImageV.mas_height);
        }];
    }
    return _goodsImageV;
}

-(UIImageView *)platform{
    if (_platform==nil) {
        _platform=[UIImageView new];
        _platform.image=[UIImage imageNamed:@"tbicon"];
        [_topBgView addSubview:_platform];
        [_platform mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(15*HWB);
            make.width.height.offset(15*HWB);
            make.left.equalTo(_goodsImageV.mas_right).offset(10*HWB);
        }];
    }
    return _platform;
}

-(UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab = [UILabel labText:@"      商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题" color:Black51 font:12.5*HWB];
        _nameLab.numberOfLines = 2;
        [_topBgView addSubview:_nameLab];
        [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(_platform.mas_centerY);
            make.top.offset(15*HWB);
            make.right.offset(-8*HWB);
            make.left.equalTo(_goodsImageV.mas_right).offset(10*HWB);
        }];
    }
    return _nameLab;
}

-(UILabel*)orderLab{
    if (!_orderLab) {
        _orderLab = [UILabel labText:@"订单号: 2018060606061234567" color:Black102 font:12*HWB];
        [_topBgView addSubview:_orderLab];
        [_orderLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsImageV.mas_centerY).offset(4*HWB);
            make.left.equalTo(_goodsImageV.mas_right).offset(10*HWB);
        }];
    }
    return _orderLab;
}

-(UILabel *)stateOrTimeLab{
    if (!_stateOrTimeLab) {
        _stateOrTimeLab = [UILabel labText:@"2018-06-06" color:Black102 font:10*HWB];
        [_topBgView addSubview:_stateOrTimeLab];
        [_stateOrTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_orderLab.mas_bottom).offset(8*HWB);
            make.left.equalTo(_goodsImageV.mas_right).offset(10*HWB);
        }];
    }
    return _stateOrTimeLab;
}

-(UILabel *)rewardLab{
    if (!_rewardLab) {
        _rewardLab = [UILabel labText:@"奖励:￥0.0" color:Black102 font:11*HWB];
        [_topBgView addSubview:_rewardLab];
        [_rewardLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_stateOrTimeLab.mas_centerY);
        }];
    }
    return _rewardLab;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
