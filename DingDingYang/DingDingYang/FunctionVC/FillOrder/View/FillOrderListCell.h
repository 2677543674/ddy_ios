//
//  FillOrderListCell.h
//  DingDingYang
//
//  Created by ddy on 2018/6/2.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FillOrderListCell : UITableViewCell
@property(nonatomic,strong) UIImageView * goodsImageV;
@property(nonatomic,strong) UIImageView * platform;
@property(nonatomic,strong) UIView  * topBgView;
@property(nonatomic,strong) UILabel * nameLab;
@property(nonatomic,strong) UILabel * orderLab;
@property(nonatomic,strong) UILabel * rewardLab;
@property(nonatomic,strong) UILabel * stateOrTimeLab;
@end
