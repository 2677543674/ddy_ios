//
//  SecondsKillVC.m
//  DingDingYang
//
//  Created by ddy on 2017/5/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SecondsKillVC.h"
#import "SkcltCell.h"
#import "ScreeningView.h"
#import "CTVBannerCell.h"
#import "BHModel.h"
@interface SecondsKillVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * collectionCV ;
@property(nonatomic,strong) NSMutableArray * dataAry ;
@property(nonatomic,strong) NSMutableDictionary * parameter ;
@property(nonatomic,assign) NSInteger pageNum ;
@property(nonatomic,strong) UIButton * topBtn ;
@property(nonatomic,strong) ScreeningView * screenView ;
@property(nonatomic,strong) NSMutableArray * berListAry ;

@end

@implementation SecondsKillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.automaticallyAdjustsScrollViewInsets = NO ;
    
    self.view.backgroundColor = COLORGROUP ;

    self.title =_mcsModel.title.length>0?_mcsModel.title:@"限时抢购";
    _pageNum = 2 ;
    _dataAry = [NSMutableArray array];
    _berListAry = [NSMutableArray array];

    [self parameter];
    
    [self screenView];

    [self collectionCV];
    [self topBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestAgain) name:LoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:UserInfoChange object:nil];
}


//-(ScreeningView*)screenView{
//    if (!_screenView) {
//        _screenView = [[ScreeningView alloc]initWithFrame:CGRectMake(0, 65, ScreenW, 38)];
//        [self.view addSubview:_screenView];
//        [_screenView selectBlock:^(NSString *sortType) {
//            _parameter[@"sortType"] = sortType ;
//            [_collectionCV.mj_header beginRefreshing];
//        }];
//    }
//    return _screenView;
//}
-(void)changeState{
//    [_parameter removeObjectForKey:@"sortType"];
//    [self setScreenView:nil];
//    [self screenView];
    [_collectionCV.mj_header beginRefreshing];
}
-(void)requestAgain{
    if (self) {
        if (role_state == 0) {
            [self changeState];
        }else{
            [_collectionCV.mj_header beginRefreshing];
        }
    }
}
-(NSMutableDictionary*)parameter{
    if (!_parameter) {
        _parameter = [NSMutableDictionary dictionary];
        _parameter[@"st"] = @"1" ;
        _parameter[@"ifSeckill"] = @"1";
    }
    return _parameter ;
}

#pragma mark --> 请求秒杀商品
-(void)requestSKData{
    [_collectionCV.mj_footer resetNoMoreData];
    _pageNum = 2 ;    _parameter[@"st"] = @"1" ;
    [NetRequest requestTokenURL:url_goods_list4 parameter:_parameter success:^(NSDictionary *nwdic) {
         [_berListAry removeAllObjects];
        if (_dataAry.count>0) {
            [_dataAry removeAllObjects];
            [_collectionCV reloadData];
        }

        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray * ary = nwdic[@"list"];
            NSArray * banner = NULLARY(nwdic[@"bannerList"]);
            if (banner.count>0 ) {
                [banner enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    BHModel * model = [[BHModel alloc]initWithDic:obj];
                    [_berListAry addObject:model];
                }];
            }

            if (ary.count>0) {
                [ary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
            } 
        }else{
           SHOWMSG(nwdic[@"result"])
        }
        [_collectionCV.mj_header endRefreshing];
        [_collectionCV reloadData];
        
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestSKData];
        }];
        [_collectionCV.mj_header endRefreshing];
    }];
}
-(void)loadMoreSKData{
    _parameter [@"st"] =FORMATSTR(@"%ld",(long)_pageNum);
    [NetRequest requestTokenURL:url_goods_list4 parameter:_parameter success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray * ary = nwdic[@"list"];
            if (ary.count>0) {
                _pageNum ++ ;
                [_collectionCV.mj_footer endRefreshing];
                [ary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_dataAry addObject:model];
                }];
                [_collectionCV reloadData];
            }else{
                [_collectionCV.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
             [_collectionCV.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
         [_collectionCV.mj_footer endRefreshing];
        
    }];
}
-(UICollectionView*)collectionCV{
    if (!_collectionCV) {
        //表格布局加载图片
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        //设置单元格的大小
//        layout.itemSize = CGSizeMake((ScreenW-12)/2, (ScreenW-12)/2+50);
//        layout.minimumInteritemSpacing =3;
//        layout.minimumLineSpacing =6;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//        layout.sectionInset = UIEdgeInsetsMake(4, 4, 4, 4);
        
        _collectionCV = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionCV.delegate = self ;
        _collectionCV.dataSource = self ;
        _collectionCV.backgroundColor = COLORGROUP ;
        [self.view addSubview:_collectionCV];
        [_collectionCV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0) ;  make.right.offset(0);
            make.top.offset(0); make.bottom.offset(0);
        }];
        
        [_collectionCV registerClass:[SkcltCell class] forCellWithReuseIdentifier:@"skcltcell"];
        [_collectionCV registerClass:[CTVBannerCell class] forCellWithReuseIdentifier:@"ctvbannercell"];
        
        __weak SecondsKillVC * SelfView = self;
        _collectionCV.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(requestSKData)];
         [_collectionCV.mj_header beginRefreshing];

        _collectionCV.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreSKData];
        }];
       
    }
    return _collectionCV ;
}


#pragma mark --> UICollectionView代理
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_berListAry.count>0) {
        return _dataAry.count + 1 ;
    }
    return _dataAry.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_berListAry.count>0) {
        if (indexPath.row==0) {
            CTVBannerCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ctvbannercell" forIndexPath:indexPath];
            cell.bannerAry = _berListAry;
            return cell;
            
        }
    }
    
    SkcltCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"skcltcell" forIndexPath:indexPath];
    if (_berListAry.count>0) {
        cell.skgModel = _dataAry[indexPath.row-1];
    }else{
        cell.skgModel = _dataAry[indexPath.row];
    }
    return cell ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row ;
    if (_berListAry.count>0) {
        if (indexPath.row==0) { return ; }
        row = indexPath.row-1 ;
    }
    
    GoodsDetailsVC*detailVc = [[GoodsDetailsVC alloc]init] ;
//    detailVc.hidesBottomBarWhenPushed = YES ;
    detailVc.dmodel = _dataAry[row] ;
    PushTo(detailVc, YES)
}
#pragma mark --> UICollectionViewLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_berListAry.count>0) {
        if (indexPath.row==0) {
            return CGSizeMake(ScreenW-8, ScreenW/2.8);
        }
    }
    return  CGSizeMake((ScreenW-12)/2, (ScreenW-12)/2+50) ;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(4, 4, 4, 4) ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 6;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 3;
}


-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
//        _topBtn.frame = CGRectMake(ScreenW-36*HWB-10, ScreenH-36*HWB-10, 36*HWB, 36*HWB);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10); make.bottom.offset(-10);
            make.height.offset(36*HWB); make.width.offset(36*HWB);
        }];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_collectionCV.contentOffset.y>=0) {
        
        if (_collectionCV.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_collectionCV.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _collectionCV.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
    }
}
-(void)clickBackTop{
    [_collectionCV setContentOffset:CGPointZero animated:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UserInfoChange object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LoginSuccess object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
