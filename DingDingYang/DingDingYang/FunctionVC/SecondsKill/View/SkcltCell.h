//
//  SkcltCell.h
//  DingDingYang
//
//  Created by ddy on 2017/5/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkcltCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView  * goodsImgV ;     //商品图片
@property(nonatomic,strong) UIView       * viewBack  ;     //
@property(nonatomic,strong) UIButton     * couponsLab ;    //优惠券
@property(nonatomic,strong) UILabel      * endTime ;       //结束时间

@property(nonatomic,strong) UILabel      * goodsTitle ;    //商品标题
@property(nonatomic,strong) UILabel      * endPriceLab ;   //券后价
@property(nonatomic,strong) UILabel      * endCouponsLab ; //券后
@property(nonatomic,strong) UILabel      * salesLab ;      //销量

@property(nonatomic,strong) GoodsModel   * skgModel ;       

@end
