//
//  SkcltCell.m
//  DingDingYang
//
//  Created by ddy on 2017/5/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#define SKH (ScreenW-12)/2

#import "SkcltCell.h"
#import "UIView+CornerRadius.h"

@implementation SkcltCell

-(void)setSkgModel:(GoodsModel *)skgModel{
    
    _skgModel = skgModel ;
    
    [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_skgModel.goodsPic] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image==nil) {
            [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_skgModel.smallPic] placeholderImage:PlaceholderImg] ;
        }
    }];
    
   
    _goodsTitle.text = _skgModel.goodsName ;
    _endTime.text = FORMATSTR(@"%@天内结束",_skgModel.endDays);
    
    if ([_skgModel.couponMoney floatValue]>0) {
        _endCouponsLab.hidden = NO ;
         _endPriceLab.text = FORMATSTR(@"¥%@",_skgModel.endPrice) ;
        [_couponsLab setTitle:FORMATSTR(@"券¥%@",_skgModel.couponMoney) forState:UIControlStateNormal];
    }else{
         _endPriceLab.text = FORMATSTR(@"¥%@",_skgModel.endPrice) ;
        _endCouponsLab.hidden = YES ;
        [_couponsLab setTitle:@"立即下单" forState:UIControlStateNormal];
    }

    _salesLab.text = FORMATSTR(@"已售%@",_skgModel.sales);
    
}

-(NSMutableAttributedString*)attriStr:(NSString*)str range:(NSRange)strrange{
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttributes:@{NSForegroundColorAttributeName:LOGOCOLOR} range:strrange];
    return dlStr;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        
        [self goodsImgV];
        
        [self viewBack];
        
        [self endTime];
        
        [self couponsLab];
        [self addShapLayerViewBack];
        
        [self goodsTitle];
        
        [self endPriceLab];
        
        [self endCouponsLab];
        
        [self salesLab];
        
    }
    return self;
}

//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.frame = CGRectMake(3, 3, SKH-6, SKH-6);
        _goodsImgV.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_goodsImgV];
    }
    return _goodsImgV;
}

//商品标题
-(UILabel*)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle = [UILabel labText:@"商品标题" color:Black102 font:13*HWB];
        _goodsTitle.textAlignment =NSTextAlignmentCenter ;
        _goodsTitle.frame = CGRectMake(1, SKH,SKH-2, 20);
        [self.contentView addSubview:_goodsTitle];
    }
    return _goodsTitle ;
}

//展示优惠券和剩余天数
-(UIView*)viewBack{
    if (!_viewBack) {
        _viewBack = [[UIView alloc]init];
        _viewBack.frame = CGRectMake(SKH-56*HWB-3, SKH-45, 56*HWB, 36);
        _viewBack.backgroundColor = ColorRGBA(243.f, 109.f, 110.f, 1) ;
        _viewBack.clipsToBounds  = YES ;
        [self.contentView addSubview:_viewBack];
        [_viewBack cornerSideType:kLQQSideTypeLeft withCornerRadius:6.f];
    }
    return _viewBack ;
}

//结束时间
-(UILabel*)endTime{
    if (!_endTime) {
        _endTime = [UILabel labText:@"1天内结束" color:COLORWHITE font:10*HWB];
        _endTime.textAlignment = NSTextAlignmentCenter ;
        _endTime.adjustsFontSizeToFitWidth = YES ;
        [_viewBack addSubview:_endTime];
        [_endTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0);  make.height.offset(18);
            make.left.offset(0); make.top.offset(0) ;
        }];
    }
    return _endTime ;
}


//优惠券
-(UIButton*)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UIButton buttonWithType:UIButtonTypeCustom];
        [_couponsLab setTitle:@"券¥200" forState:UIControlStateNormal];
        [_couponsLab setTitleColor:COLORWHITE forState:UIControlStateNormal];
        _couponsLab.titleLabel.font = [UIFont systemFontOfSize:11*HWB];
        [_viewBack addSubview:_couponsLab];
        [_couponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(0); make.bottom.offset(0);
            make.height.offset(18); make.left.offset(0);
        }];
    }
    return _couponsLab ;
}
//绘制虚线
-(void)addShapLayerViewBack{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:_viewBack.bounds];
    [shapeLayer setFillColor:LOGOCOLOR.CGColor];
    [shapeLayer setStrokeColor:COLORWHITE.CGColor];
    [shapeLayer setLineWidth:1];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineCap:kCALineCapButt];
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:@4, @2, nil]];
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL,56*HWB/2+5,36);
    CGPathAddLineToPoint(path, NULL,56*HWB+56*HWB/2-2,36);
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    [_viewBack.layer addSublayer:shapeLayer];

}

//券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"¥00.00" color:LOGOCOLOR font:15*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5);
            make.bottom.offset(-6);
        }];
 
        if (THEMECOLOR==2) { //主题颜色为浅色
             _endPriceLab.textColor = OtherColor ;
        }
    }
    return _endPriceLab;
}

//券后
-(UILabel*)endCouponsLab{
    if (!_endCouponsLab) {
        _endCouponsLab = [UILabel labText:@"券后" color:Black204 font:11*HWB];
        [self.contentView addSubview:_endCouponsLab];
        [_endCouponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(1);
            make.centerY.equalTo(_endPriceLab.mas_centerY);
        }];
    }
    return _endCouponsLab ;
}

//销量
-(UILabel*)salesLab{
    if (!_salesLab) {
        _salesLab = [UILabel labText:@"已售00.00" color:Black102 font:11*HWB];
        _salesLab.textAlignment = NSTextAlignmentLeft;
        _salesLab.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_salesLab];
        [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-8);
            make.centerY.equalTo(_endPriceLab.mas_centerY);
        }];
    }
    return _salesLab ;
}


/**
 绘制虚线

 @param lineView 要绘制的父视图
 @param lineLength 线长
 @param lineSpacing 间距
 @param lineColor 颜色
 @param lineWidth 线宽
 @param startPoint 开始起点
 @param endPoint 结束点
 */
//-(void)drawDashLine:(UIView*)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor lineWidth:(int)lineWidth startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint{
//    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//    [shapeLayer setBounds:lineView.bounds];
//    [shapeLayer setPosition:lineView.center];
//    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
//    [shapeLayer setStrokeColor:lineColor.CGColor];
//    [shapeLayer setLineWidth:lineWidth];
//    [shapeLayer setLineJoin:kCALineJoinRound];
//    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]];
//    CGMutablePathRef path = CGPathCreateMutable();
//    CGPathMoveToPoint(path, NULL, startPoint.x, startPoint.y);
//    CGPathAddLineToPoint(path, NULL, endPoint.x, endPoint.y);
//    [shapeLayer setPath:path]; CGPathRelease(path);
//    [lineView.layer addSublayer:shapeLayer];
//}




@end




















