//
//  MethodsClassObjc.m
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "MethodsClassObjc.h"

//使用前需要导入头文件
#import <Accelerate/Accelerate.h>
#import <CommonCrypto/CommonDigest.h>
#import "sys/utsname.h"
@implementation MethodsClassObjc



+(void)photoPermissionsNoOpenShowVC:(UIViewController*)topvc photoPermissionsType:(void(^)(NSInteger type))photoPermissionsType{
    // 判断相册权限
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        NSLog(@"权限状态: %ld",(long)status);
        if (SystemVersion>=11.0) {
            if (status==PHAuthorizationStatusAuthorized||status==PHAuthorizationStatusDenied) {
                photoPermissionsType(1);
            }else{
                [UIAlertController showIn:topvc title:@"提示" content:@"\n您还没有开启app相册权限，无法保存图片，是否开启？" ctAlignment:(NSTextAlignmentCenter) btnAry:@[@"取消",@"开启"] indexAction:^(NSInteger indexTag) {
                    if (indexTag==1) {
                        NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        if ([[UIApplication sharedApplication]canOpenURL:url]) {
                            [[UIApplication sharedApplication]openURL:url];
                        }else{ SHOWMSG(@"无法开启权限，请手动前往系统的app设置") }
                    }
                }];
            }
        }else{
            if (status==PHAuthorizationStatusAuthorized) { photoPermissionsType(1); }
            else{
                [UIAlertController showIn:topvc title:@"提示" content:@"您还没有开启app相册权限，是否开启？" ctAlignment:(NSTextAlignmentCenter) btnAry:@[@"取消",@"开启"] indexAction:^(NSInteger indexTag) {
                    if (indexTag==1) {
                        NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        if ([[UIApplication sharedApplication]canOpenURL:url]) {
                            [[UIApplication sharedApplication]openURL:url];
                        }else{ SHOWMSG(@"无法开启权限，请手动前往系统的app设置") }
                    }
                }];
            }
        }
    }];
}



+(NSInteger)getGoodsListStyle{
    if (NSUDTakeData(cacheS_list_style)) {
        NSString * str = NSUDTakeData(cacheS_list_style);
        return [str integerValue];
    }else{
        return 1; // 默认横条列表
    }
}

+(NSInteger)goodsListStyle{
    NSInteger goodsListStyle = 1 ;
    NSString * style = @"3-1-0-1";
    if (NSUDTakeData(app_dynamic_home_dic)!=nil) {
        if (role_state==0) {
            NSDictionary * dic = (NSDictionary*)NSUDTakeData(app_dynamic_home_dic);
            NSDictionary * homeDic = NULLDIC(dic[@"list0"]) ;
            style = REPLACENULL(homeDic[@"goodsListStyle"]);
        }else{
            NSDictionary * dic = (NSDictionary*)NSUDTakeData(app_dynamic_home_dic);
            NSDictionary * homeDic = NULLDIC(dic[@"list1"]) ;
            style = REPLACENULL(homeDic[@"goodsListStyle"]);
        }
        if ([style isEqual:@"3-1-0-1"]) { goodsListStyle = 1; } else
            if ([style isEqual:@"3-1-0-2"]) { goodsListStyle = 2; } else
            if ([style isEqual:@"3-1-0-3"]) { goodsListStyle = 3; } else
            if ([style isEqual:@"3-1-0-4"]) { goodsListStyle = 4; } else
            if ([style isEqual:@"3-1-0-5"]) { goodsListStyle = 5; } else
            if ([style isEqual:@"3-1-0-6"]) { goodsListStyle = 6; } else
            if ([style isEqual:@"3-1-0-7"]) { goodsListStyle = 7; } else
            if ([style isEqual:@"3-1-0-8"]) { goodsListStyle = 8; } else
            { goodsListStyle = 1; }
#if DingDangDingDang
        //叮当固定列表 样式有修改
        goodsListStyle = 7;
#endif
    }
    return goodsListStyle;
}


#pragma mark === >>> MD5加密
+(NSString*)md5encrypting:(NSString *)mdStr{
    //转换成utf-8
    const char *original_str = [mdStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_LONG cclong = (CC_LONG)strlen(original_str);
    CC_MD5(original_str, cclong , result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}

#pragma mark ===>>>  获取当前时间，日期
+(NSString*)getNowTimeByFormat:(NSString*)format{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    NSLog(@"当前手机本地时间：%@",dateString);
    if (dateString.length>0) { return dateString ; }
    return @"";
}

#pragma mark ===>>>将时间戳转化为时间(时间长度字符串为毫秒)
+(NSString*)timeStamp:(NSString*)strTime timeFormat:(NSString*)format{
    if (REPLACENULL(strTime).length==0||REPLACENULL(format).length==0) { return @"" ; }
    if (strTime.length>5) {
        NSString * timeStampString = strTime;
        NSTimeInterval _interval = [timeStampString doubleValue];
        if (strTime.length>11) { _interval = _interval / 1000.0; } // 毫秒时间戳
        NSDate * date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter * objDateformat = [[NSDateFormatter alloc] init];
        [objDateformat setDateFormat:format]; // 设置时间转化格式
        NSString * timeStr = [objDateformat stringFromDate:date];
        if (timeStr.length==0) { return @"" ; }
        return timeStr ;
    }else{
        return @"";
    }
        
   
    
}

#pragma mark ===>>> 获取当前界面的导航视图
+(UINavigationController*)huoQuNavcTop{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if ([window.rootViewController isKindOfClass:[MainTabBarVC class]]) {
        MainTabBarVC * mian = (MainTabBarVC*)window.rootViewController ;
        UITabBarController*tabbarVC = mian.tabbarController;
        UINavigationController * naVC = (UINavigationController*)tabbarVC.selectedViewController;
        return naVC ;
    }
    return nil;
}

//返回储存的网路状态
+(NSInteger)getNetWorkState{
    return [NSUDTakeData(@"NOW_NETWORK_STATE") integerValue] ;
}

//返回不同屏幕大小宽/320的比例
+(float)ScreenWBiLi{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if (ScreenW==320) { return 1 ; }
        else if (ScreenW==375) { return 1.15; }
        else if (ScreenW==414) { return 1.20; }
        else{  return  [UIScreen mainScreen].bounds.size.width/320 ;  }
    }
    return 1.4 ;
}

+(float)titleFont{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if (ScreenW<400) {  return 16.0 ; }
        if (ScreenW>375) {  return 17.0 ; }
    }
    return 21 ;
}


#pragma mark ===>>> 判断是否可以打开某个应用
+(BOOL)canOpenTaoBao{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"taobao://"]]&&
        [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tbopen://"]] )
    
     {   return YES ;  }
#ifdef DEBUG
    return YES ;
#else
    return NO ;
#endif
   
}

+(BOOL)canOpenWechat{
    
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"wechat://"]]&&
//        [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]] )
    if ([WXApi isWXAppInstalled]) { return YES ; }
#ifdef DEBUG
    return YES ;
#else
    return NO ;
#endif
}

+(BOOL)canOpenAlipay{
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"alipay://"]] )
    
    {   return YES ;  }
    
#ifdef DEBUG
    return YES ;
#else
    return NO ;
#endif
}

+(BOOL)canOpenQQ{

    if ([QQApiInterface isQQInstalled]) { return YES ; }
    
#ifdef DEBUG
    return YES ;
#else
    return NO ;
#endif
}


+(NSString*)huoQuNameAndVersion{
    NSString * version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString * nv = FORMATSTR(@"%@  %@",APPNAME,version);
    return nv;
}

#pragma mark ===>>> 四舍五入
+(NSString *)notRounding:(NSString*)price afterPt:(int)position{
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundBankers scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *ouncesDecimal;
    NSDecimalNumber *roundedOunces;
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:[price floatValue]];
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    return [NSString stringWithFormat:@"%@",roundedOunces];
}
+(float)quInteger:(float)number {
    NSString * str = FORMATSTR(@"%.0f",number);
    return [str floatValue];
}

#pragma mark ===>>> 获取token
+(NSString*)tokenStr{
    NSString*token = REPLACENULL(NSUDTakeData(@"TOKEN"));
    return token;
}

#pragma mark ===>>> 返会用户状态
+(NSInteger)roleInt{
    NSDictionary * dic = NULLDIC(NSUDTakeData(UserInfo));
    NSInteger  role = [REPLACENULL((dic[@"role"])) integerValue];
    return role;
}

+(NSString*)getRoleNameByStatus:(id)state{
    
    NSString * roleName;
    if (NSUDTakeData(cache_role_status)) {
        NSDictionary * dic = NULLDIC(NSUDTakeData(cache_role_status));
        roleName = REPLACENULL(dic[FORMATSTR(@"%@",(NSString*)state)]);
        if (roleName.length>0) { return roleName; }
    }
    switch ([state integerValue]) {
        case 0:{ roleName = @"消费者"; } break;
        case 1:{ roleName = @"代理商"; } break;
        case 2:{ roleName = @"运营商"; } break;
        default:{ roleName = @"消费者"; } break;
    }
    return roleName;
    
}

#pragma mark ===>>> 返回当前个人信息中的手机号
+(NSString*)numberPhone{
    NSDictionary * dic = NULLDIC(NSUDTakeData(UserInfo));
    NSString * numPhone = REPLACENULL(dic[@"mobile"]);
    return numPhone ;
}

#pragma mark ===>>> 替换空
+(NSString*)replaceEmptyStr:(NSString*)str{
    @try {
        NSString * string = [NSString stringWithFormat:@"%@",str];
        if ([string isEqualToString:@"<null>"]||string==nil||[string isEqualToString:@"(null)"]) {
            return @"" ;
        } else { return string; }
    } @catch (NSException *exception) {
        return @"";
    } @finally { }
}

#pragma mark ===>>> 替换空字典
+(NSDictionary*)dicNull:(id)dict{
    
    NSString * str = REPLACENULL(STR(dict));
    if ([str isEqualToString:@"<null>"]||str.length==0) {
        return @{};
    }else{
        NSDictionary * dic = (NSDictionary*)dict;
        if ([dic isKindOfClass:[NSDictionary class]]) {
            return dic;
        }else{
            return @{} ;
        }
    }
}

#pragma mark ===>>> 替换空数组
+(NSArray*)aryNull:(id)ary{
    NSString * str = REPLACENULL(STR(ary)) ;
    if ([str isEqualToString:@"<null>"]||str.length==0) {
        return @[];
    }else{
        NSArray * nsary = (NSArray*)ary;
        if ([nsary isKindOfClass:[NSArray class]]) {
            return nsary;
        }else{
            return @[];
        }
    }
}


#pragma mark ===>>> 正则表达式 1:正则公式  2:需判断的内容
+(BOOL)judgeSelectionExpression:(NSString*)way content:(NSString*)content{
    NSString *regex=way;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isValid = [predicate evaluateWithObject:content];
    return isValid;
}

#pragma mark ===>>> 存值
+(void)saveDataForUesrdefaults:(id)value andKey:(NSString*)valueKey{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:valueKey];
    [userDefaults synchronize];
}

#pragma mark ===>>> 取值
+(id)takeDataForUesrdefaults:(NSString*)key{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id dataTake =[userDefaults objectForKey:key];
    return dataTake;
}

#pragma mark ===>>> 删值
+(void)removeDataForUesrdefaults:(NSString*)key{
    NSUserDefaults*userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:key];
}

#pragma mark ===>>> 类似安卓小提示框
+(void)showMessage:(NSString *)message{
    
     dispatch_async(dispatch_get_main_queue(), ^{
         
         ResignFirstResponder  MBHideHUD
         UIWindow * window = [UIApplication sharedApplication].keyWindow;
         if (window) {
             @try {
                 for (UIView * showView in window.subviews) {
                     if ([showView isKindOfClass:[ShowMsgView class]]) {
                         showView.alpha = 0;
                         [showView removeFromSuperview];
                     }
                 }
                 NSString * msg = REPLACENULL(message) ;
                 
                 // 遇到不需要显示或者要全局修改的提示，在此修改
                 if (msg.length==0) { return ; }
                 if ([msg containsString:@"未能找到使用指定主机名的服务器"]) { msg = @"网络异常!"; }
                 if (([msg containsString:@"似乎已断开与互联网的连接"])) { msg = @"无法连接网络，请检查网络设置!";}
                 if ([msg containsString:@"网络连接已中断"]) { msg = @"网络异常!"; }
                 if ([msg isEqualToString:@"login"]) {  return ;  }
                 
                 float   font ; //提示字体大小
                 if      (ScreenW==320) { font = 14.0 ; }
                 else if (ScreenW==375) { font = 15.0 ; }
                 else if (ScreenW==414) { font = 16.0 ; }
                 else    { font = 17.0 ; }
                 
                 // 计算文字宽高
                 CGRect rect = StringRect(msg, 280,font);
                 CGPoint piont = CGPointMake(ScreenW/2 , ScreenH-ScreenH/4);
                 ShowMsgView * showView = [[ShowMsgView alloc]initWithFrameCenter:piont bounds:rect];
                 showView.transform = CGAffineTransformMakeScale(0,0);
                 showView.tag = 999;
                 [window addSubview:showView];
                 
                 showView.showLab.font = [UIFont systemFontOfSize:font];
                 showView.showLab.text = msg;
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:3.f options:UIViewAnimationOptionCurveEaseOut animations:^{
                         if (showView) {
                             showView.alpha = 1;
                             showView.transform = CGAffineTransformMakeScale(1,1);
                         }
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1 delay:2.5 options:(UIViewAnimationOptionCurveLinear) animations:^{
                             if (showView) {
                                 showView.transform = CGAffineTransformMakeScale(1.1,1.1);
                             }
                         } completion:^(BOOL finished) {
                             [UIView animateWithDuration:0.26 animations:^{
                                 if (showView) {
                                     showView.alpha = 0 ;
                                     showView.transform = CGAffineTransformMakeScale(0.1,0.1);
                                 }
                             } completion:^(BOOL finished) {
                                 if (showView) { [showView removeFromSuperview]; }
                             }];
                         }];
                     }];
                 });
             } @catch (NSException *exception) {
                 MBError(REPLACENULL(message))
             } @finally { }
         }
     });
    
}


+(void)showResult:(NSString*)str{
    if ([str integerValue]==0) { return ; }
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    NSString * showstr = FORMATSTR(@"已为您找到%@件商品",str);
    UILabel*txtLab = [UILabel labText:showstr color:COLORWHITE font:14.0*HWB];
    txtLab.backgroundColor = ColorRGBA(241.f, 103.f, 104.f, 1) ;
    txtLab.textAlignment = NSTextAlignmentCenter ;
    txtLab.center = CGPointMake(ScreenW/2, 150);
    txtLab.bounds = CGRectMake(0, 0, 0, 36) ;
    txtLab.layer.cornerRadius =3;
    [window addSubview:txtLab];
    
    float wh = StringSize(showstr, 14.0*HWB).width+20;
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.26 animations:^{
            txtLab.bounds = CGRectMake(0, 0, wh , 36);
        }completion:^(BOOL finished) {
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC));
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.26 animations:^{
                    txtLab.bounds = CGRectMake(0, 0, 0 , 36);
                } completion:^(BOOL finished) {
                    [txtLab removeFromSuperview];
                }];
           });
        }];
    });
    
}

//将字符串转成二维码
+(UIImage*)changeImageWithStr:(NSString*)str{
    
    // 1.创建过滤器
    CIFilter * filter  =[CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    // 3.给过滤器添加数据
    NSString * dataString = str;
    NSData * data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKeyPath:@"inputMessage"];
    
    // 4.获取输出的二维码
    CIImage * outputImage = [filter outputImage];
    CGRect extent = CGRectIntegral(outputImage.extent);
    CGFloat scale = MIN(1000/CGRectGetWidth(extent), 1000/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:outputImage fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    
    return [UIImage imageWithCGImage:scaledImage] ;
    
}

// 将字符串转成二维码

/**
 生成二维码
 
 @param str 字符串
 @param whi 宽高(一样)
 @param centerImg 中心图片
 @return 二维码图片
 */
+(UIImage*)changeImageWithStr:(NSString*)str whi:(float)whi centerImg:(UIImage*)centerImg{
    
    @try {
        // 1.创建过滤器
        CIFilter * filter  =[CIFilter filterWithName:@"CIQRCodeGenerator"];
        [filter setDefaults];
        // 3.给过滤器添加数据
        NSString * dataString = str;
        NSData * data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
        [filter setValue:data forKeyPath:@"inputMessage"];
        
        // 4.获取输出的二维码
        CIImage * outputImage = [filter outputImage];
        CGRect extent = CGRectIntegral(outputImage.extent);
        CGFloat scale = MIN(whi/CGRectGetWidth(extent), whi/CGRectGetHeight(extent));
        
        // 1.创建bitmap;
        size_t width = CGRectGetWidth(extent) * scale;
        size_t height = CGRectGetHeight(extent) * scale;
        CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
        CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef bitmapImage = [context createCGImage:outputImage fromRect:extent];
        CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
        CGContextScaleCTM(bitmapRef, scale, scale);
        CGContextDrawImage(bitmapRef, extent, bitmapImage);
        
        // 2.保存bitmap到图片
        CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
        CGContextRelease(bitmapRef);
        CGImageRelease(bitmapImage);
        
        // 输出二维码图片
        UIImage * ewmImg = [UIImage imageWithCGImage:scaledImage];
        
        if (centerImg) {
            // 二维码最大分辨率
            CGRect maxRect = CGRectMake(0, 0, whi, whi);
            
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(whi, whi),NO,[UIScreen mainScreen].scale);
            
            float bili = 4;
            // 中心头像的白色背景位置
            CGRect rect = CGRectMake(whi/2-whi/bili/2, whi/2-whi/bili/2, whi/bili, whi/bili);
            
            [ewmImg drawInRect:maxRect];
            UIImage * imageback = [UIImage imageNamed:@"ewm_white_bg"];
            [imageback drawInRect:rect];
            
            bili = 12;
            [centerImg drawInRect:CGRectMake(rect.origin.x+rect.size.width/bili, rect.origin.y+rect.size.height/bili, rect.size.width-rect.size.width/bili*2, rect.size.height-rect.size.height/bili*2)];
            
            // 获取绘制图片
            UIImage * resultingImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return  resultingImage;
        }
        
        return ewmImg;
    } @catch (NSException *exception) {
        SHOWMSG(@"二维码合成失败!")
    } @finally { }
    
}


+ (NSString *)getDeviceType{
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString * deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    // iPhone
    if ([deviceString isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([deviceString isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([deviceString isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([deviceString isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([deviceString isEqualToString:@"iPhone3,2"])    return @"Verizon iPhone 4";
    if ([deviceString isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([deviceString isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([deviceString isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([deviceString isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([deviceString isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([deviceString isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([deviceString isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([deviceString isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([deviceString isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([deviceString isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([deviceString isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([deviceString isEqualToString:@"iPhone10,1"])   return @"iPhone 8";
    if ([deviceString isEqualToString:@"iPhone10,4"])   return @"iPhone 8";
    if ([deviceString isEqualToString:@"iPhone10,2"])   return @"iPhone 8 Plus";
    if ([deviceString isEqualToString:@"iPhone10,5"])   return @"iPhone 8 Plus";
    if ([deviceString isEqualToString:@"iPhone10,3"])   return @"iPhone X";
    if ([deviceString isEqualToString:@"iPhone10,6"])   return @"iPhone X";
    if ([deviceString isEqualToString:@"iPhone11,8"])   return @"iPhone XR";
    if ([deviceString isEqualToString:@"iPhone11,2"])   return @"iPhone XS";
    if ([deviceString isEqualToString:@"iPhone11,4"])   return @"iPhone XS Max";
    if ([deviceString isEqualToString:@"iPhone11,6"])   return @"iPhone XS Max";
    
    // 模拟器
    if ([deviceString isEqualToString:@"i386"])         return @"iPhone Simulator";
    if ([deviceString isEqualToString:@"x86_64"])       return @"iPhone Simulator";
    
    // iPod
    if ([deviceString isEqualToString:@"iPod1,1"])      return@"iPod Touch 1G";
    if ([deviceString isEqualToString:@"iPod2,1"])      return@"iPod Touch 2G";
    if ([deviceString isEqualToString:@"iPod3,1"])      return@"iPod Touch 3G";
    if ([deviceString isEqualToString:@"iPod4,1"])      return@"iPod Touch 4G";
    if ([deviceString isEqualToString:@"iPod5,1"])      return@"iPod Touch (5 Gen)";
    
    // iPad
    if ([deviceString isEqualToString:@"iPad1,1"])      return@"iPad 1G";
    if ([deviceString isEqualToString:@"iPad2,1"])      return@"iPad 2";
    if ([deviceString isEqualToString:@"iPad2,2"])      return@"iPad 2";
    if ([deviceString isEqualToString:@"iPad2,3"])      return@"iPad 2 (CDMA)";
    if ([deviceString isEqualToString:@"iPad2,4"])      return@"iPad 2";
    if ([deviceString isEqualToString:@"iPad2,5"])      return@"iPad Mini";
    if ([deviceString isEqualToString:@"iPad2,6"])      return@"iPad Mini";
    if ([deviceString isEqualToString:@"iPad2,7"])      return@"iPad Mini (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPad3,1"])      return@"iPad 3";
    if ([deviceString isEqualToString:@"iPad3,2"])      return@"iPad 3 (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPad3,3"])      return@"iPad 3";
    if ([deviceString isEqualToString:@"iPad3,4"])      return@"iPad 4";
    if ([deviceString isEqualToString:@"iPad3,5"])      return@"iPad 4";
    if ([deviceString isEqualToString:@"iPad3,6"])      return@"iPad 4 (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPad4,1"])      return@"iPad Air (WiFi)";
    if ([deviceString isEqualToString:@"iPad4,2"])      return@"iPad Air (Cellular)";
    if ([deviceString isEqualToString:@"iPad4,3"])      return@"iPad Air";
    if ([deviceString isEqualToString:@"iPad4,4"])      return@"iPad Mini 2 ";
    if ([deviceString isEqualToString:@"iPad4,5"])      return@"iPad Mini 2 (Cellular)";
    if ([deviceString isEqualToString:@"iPad4,6"])      return@"iPad Mini 2G";
    if ([deviceString isEqualToString:@"iPad4,7"])      return@"iPad Mini 3";
    if ([deviceString isEqualToString:@"iPad4,8"])      return@"iPad Mini 3";
    if ([deviceString isEqualToString:@"iPad4,9"])      return@"iPad Mini 3";
    if ([deviceString isEqualToString:@"iPad5,1"])      return@"iPad Mini 4 (WiFi)";
    if ([deviceString isEqualToString:@"iPad5,2"])      return@"iPad Mini 4 (LTE)";
    if ([deviceString isEqualToString:@"iPad5,3"])      return@"iPad Air 2";
    if ([deviceString isEqualToString:@"iPad5,4"])      return@"iPad Air 2";
    if ([deviceString isEqualToString:@"iPad6,3"])      return@"iPad Pro 9.7英寸";
    if ([deviceString isEqualToString:@"iPad6,4"])      return@"iPad Pro 9.7英寸";
    if ([deviceString isEqualToString:@"iPad6,7"])      return@"iPad Pro 12.9英寸 1代";
    if ([deviceString isEqualToString:@"iPad6,8"])      return@"iPad Pro 12.9英寸 1代 (LTE)";
    if ([deviceString isEqualToString:@"iPad6,11"])     return@"iPad 5";
    if ([deviceString isEqualToString:@"iPad6,12"])     return@"iPad 5 (LTE)";
    if ([deviceString isEqualToString:@"iPad7,1"])      return@"iPad Pro 12.9英寸 2代";
    if ([deviceString isEqualToString:@"iPad7,2"])      return@"iPad Pro 12.9英寸 2代 (LTE)";
    if ([deviceString isEqualToString:@"iPad7,3"])      return@"iPad Pro 10.5英寸";
    if ([deviceString isEqualToString:@"iPad7,4"])      return@"iPad Pro 10.5英寸 (LTE)";
    if ([deviceString isEqualToString:@"iPad7,5"])      return@"iPad 6";
    if ([deviceString isEqualToString:@"iPad7,6"])      return@"iPad 6 (LTE)";
    if ([deviceString isEqualToString:@"iPad8,1"])      return@"iPad Pro 11英寸";
    if ([deviceString isEqualToString:@"iPad8,2"])      return@"iPad Pro 11英寸";
    if ([deviceString isEqualToString:@"iPad8,3"])      return@"iPad Pro 11英寸";
    if ([deviceString isEqualToString:@"iPad8,4"])      return@"iPad Pro 11英寸";
    if ([deviceString isEqualToString:@"iPad8,5"])      return@"iPad Pro 12.9英寸 3代";
    if ([deviceString isEqualToString:@"iPad8,6"])      return@"iPad Pro 12.9英寸 3代";
    if ([deviceString isEqualToString:@"iPad8,7"])      return@"iPad Pro 12.9英寸 3代";
    if ([deviceString isEqualToString:@"iPad8,8"])      return@"iPad Pro 12.9英寸 3代";

    return deviceString;
}





/*
//图片高斯模糊
+ (UIImage *)boxblurImage:(UIImage *)image withBlurNumber:(CGFloat)blur {
    if (blur < 0.f || blur > 1.f) {
        blur = 0.5f;
    }
    int boxSize = (int)(blur * 40);
    boxSize = boxSize - (boxSize % 2) + 1;
    
    CGImageRef img = image.CGImage;
    
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    
    void *pixelBuffer;
    //从CGImage中获取数据
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    //设置从CGImage获取对象的属性
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) *
                         CGImageGetHeight(img));
    
    if(pixelBuffer == NULL)
        NSLog(@"No pixelbuffer");
    
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
    
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(
                                             outBuffer.data,
                                             outBuffer.width,
                                             outBuffer.height,
                                             8,
                                             outBuffer.rowBytes,
                                             colorSpace,
                                             kCGImageAlphaNoneSkipLast);
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    //clean up
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    
    free(pixelBuffer);
    CFRelease(inBitmapData);
    
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(imageRef);
    
    return returnImage;
}
*/



@end








#pragma mark ===>>> 显示提示语
@implementation ShowMsgView

-(instancetype)initWithFrameCenter:(CGPoint)center bounds:(CGRect)rect{
    self = [super init];
    if (self) {
        
        self.alpha = 0;
        self.center = center;
        self.bounds = CGRectMake(0, 0, rect.size.width+18, rect.size.height+15);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.85];
        
        self.layer.masksToBounds = YES ;
        self.layer.cornerRadius = 6 ;
        self.clipsToBounds = YES;
        
        _showLab = [[UILabel alloc]init];
        _showLab.numberOfLines = 0 ;
        _showLab.textColor = COLORWHITE;
        _showLab.textAlignment = NSTextAlignmentCenter;
        _showLab.center = CGPointMake(WFRAME(self)/2, HFRAME(self)/2) ;
        _showLab.bounds = CGRectMake(0, 0, rect.size.width+8, rect.size.height+5);
        [self addSubview:_showLab];
        
    }
    return self;
}

@end



