//
//  MethodsDefine.h
//  DingDingYang
//
//  Created by ddy on 2017/3/17.
//  Copyright © 2017年 ddy.All rights reserved.
//

#ifndef MethodsDefine_h
#define MethodsDefine_h



#define ALERT(str) UIAlertView*aleart=[[UIAlertView alloc]initWithTitle:@"提示" message:str delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];\
      [aleart show];

#define TopNavc  [MethodsClassObjc huoQuNavcTop]

//给button添加点击事件
#define  ADDTARGETBUTTON(btnName,clickWay) [btnName addTarget:self action:@selector(clickWay) forControlEvents:UIControlEventTouchUpInside];

//导航栏的push和pop
#define PushTo(VC,YESORNO)  [self.navigationController pushViewController:VC animated:YESORNO];
#define PopTo(YESORNO) [self.navigationController popViewControllerAnimated:YESORNO];

//不知道第一相应者的情况下取消第一相应
#define ResignFirstResponder [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)to:nil from:nil forEvent:nil];

//RGB颜色
#define ColorRGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

//字符串拼接
#define FORMATSTR(...)    [NSString stringWithFormat:__VA_ARGS__]

//将字符串转为UTF8
#define NSUTF8STR(str) [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]


//MBMBProgressHUD
#define  MBShow(str)        [MBProgressHUD showMessage:str];//正在发送请求显示
#define  MBHideHUD          [MBProgressHUD hideHUD];//隐藏
#define  MBError(errorStr)  [MBProgressHUD showError:errorStr];//提示错误信息
#define  MBSuccess(sucMsg)  [MBProgressHUD showSuccess:sucMsg];//显示成功信息


//判断字符串使用
#define TwoPointNumber @"^([0-9][0-9]*)+(.[1-9]{0,2})?$"  // 输入两位有效数字
#define PHONE @"^1[0-9][0-9]\\d{8}$" // 判断手机号
#define NUMBERPOINT @"^[0-9]*$"      // 只允许输入数字
#define A_Z_0_9  @"^[A-Za-z0-9]{6}$" // 只能有字母数字，六位
#define AZ_az_09 @"^[A-Za-z0-9]+$"   // 可以输入数字和字母
#define JUDGESTR(way,string)  [MethodsClassObjc judgeSelectionExpression:way content:string]

//NSUserDefaults存值
#define  NSUDSaveData(value,Key)  [MethodsClassObjc saveDataForUesrdefaults:value andKey:Key];
#define  NSUDTakeData(Key)  [MethodsClassObjc takeDataForUesrdefaults:Key]
#define  NSUDRemoveData(Key) [MethodsClassObjc removeDataForUesrdefaults:Key];

//提示框(类似安卓)
#define SHOWMSG(MSG)  [MethodsClassObjc showMessage:MSG];

//显示商品数量提示
#define GOODSNUMRESULTS(results) [MethodsClassObjc showResult:results];

//描述对象时转换成纯粹的字符串
#define STR(str) [NSString stringWithFormat:@"%@",str]

//替换空字典
#define NULLDIC(dic) [MethodsClassObjc dicNull:dic]

//替换空数组
#define NULLARY(ary)  [MethodsClassObjc aryNull:ary]

//替换<null>
#define  REPLACENULL(STR)  [MethodsClassObjc replaceEmptyStr:STR]

//四舍五入(1字符串、2小数点位置)
#define ROUNDED(number,point)  [MethodsClassObjc notRounding:number afterPt:point]
//保存为整数
#define FloatKeepInt(numid)   [MethodsClassObjc quInteger:numid]

//判断是否可以打开应用
#define CANOPENTaoBao  [MethodsClassObjc canOpenTaoBao]
#define CANOPENAlipay  [MethodsClassObjc canOpenAlipay]
#define CANOPENWechat  [MethodsClassObjc canOpenWechat]
#define CANOPENQQ      [MethodsClassObjc canOpenQQ]

//MD5加密
#define MD5Str(str)  [MethodsClassObjc md5encrypting:str]

//将字符串转成二维码，返回一张图片
#define EWMImageWithString(str)  [MethodsClassObjc changeImageWithStr:str]

// 商品列表样式
#define GoodsListStyle     [MethodsClassObjc goodsListStyle]
#define GoodsListStylePdd  1

//将字符串转成二维码，返回一张图片
#define EWMImageWithStr(str,whif,img)  [MethodsClassObjc changeImageWithStr:str whi:whif centerImg:img]

//获取设备名

#define GetDeviceName [MethodsClassObjc getDeviceType]

/**
 获取当前时间
 
 @param format 时间格式(1：yyyy-MM-dd HH:mm:ss //24小时制   2：yyyy-MM-dd hh:mm:ss //12小时制)
 @return 时间字符串
 */
#define NowTimeByFormat(format) [MethodsClassObjc getNowTimeByFormat:format]



/**
 将时间戳转化为时间

 @param stamp 时间戳字符串（毫秒）
 @param format 时间转化格式
 @return 返回时间字符串
 */
#define TimeByStamp(stamp,format)  [MethodsClassObjc timeStamp:stamp timeFormat:format]


/**
 计算富文本字符串的宽高
 
 @param string 原字符串
 @param Kuan 最大宽度
 @param attributed 属性字典
 @return rect
 */
#define AttributedStringRect(string,Kuan,attributed)  [string boundingRectWithSize:CGSizeMake(Kuan, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attributed context:nil]

/**
 *  计算字符串的高度(多行)
 *  @param string   输入要计算的字符串
 *  @param Kuan     输入文字需要的宽度
 *  @param FontSize 输入字体大小
 *  @return         返回结果CGRect类型
 */
#define StringRect(string,Kuan,FontSize) [string boundingRectWithSize:CGSizeMake(Kuan, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:[[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:FontSize],NSFontAttributeName,nil] context:nil]

/**
 *  计算字符串的长度或高度(单行)
 *  @param string   输入要计算的字符串
 *  @param FontSize 字体大小
 *  @return          返回结果CGSize类型
 */
#define StringSize(string,FontSize) \
[string sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FontSize]}]


#pragma mark----------------------------键盘遮挡输入框处理
//注册键盘高度变化的通知
#define KeyboardWillChangeFrameNotification  \
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
//键盘高度变化后执行的方法***参数:输入框的Y坐标加上输入框的高度
#define KeyboardWillChangeViewMove(textFieldY) \
-(void)keyboardFrameChange:(NSNotification*)keyboardInfo{\
NSDictionary *KBInfo = [keyboardInfo userInfo];\
NSValue *aValue = [KBInfo objectForKey:UIKeyboardFrameEndUserInfoKey];\
CGRect keyboardRect = [aValue CGRectValue];\
CGFloat  keyBoardY=keyboardRect.origin.y-20;\
if (textFieldY>keyBoardY) {\
[UIView animateWithDuration:0.26 animations:^{\
self.view.frame=CGRectMake(0,-(textFieldY-keyBoardY), self.view.frame.size.width, self.view.frame.size.height);\
}];\
}\
}
//开启一个动画,输入内容结束时回复到原点
#define BackToTheOriginSelfView  \
[UIView animateWithDuration:0.26 animations:^{\
self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);\
}];


#endif /* MethodsDefine_h */











