



#ifndef Define_h
#define Define_h



//#define LOG(...) ;
#define LOG(format, ...)  printf("\n\n%s\n\n",[[NSString stringWithFormat:format, ## __VA_ARGS__] UTF8String]);


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE_X (IS_IPHONE && (SCREEN_MAX_LENGTH==812||SCREEN_MAX_LENGTH==896))

#define NAVCBAR_HEIGHT (IS_IPHONE_X ? 88 : 64)
#define TABBAR_HEIGHT (IS_IPHONE_X ? 83 : 49)

#define SystemVersion [[UIDevice currentDevice].systemVersion floatValue]


#define ScreenW  [UIScreen mainScreen].bounds.size.width
#define ScreenH [UIScreen mainScreen].bounds.size.height
#define PlaceholderImg [UIImage imageNamed:@"place"]

//不同手机屏幕和320的比例
#define HWB  [MethodsClassObjc ScreenWBiLi]
//#define HWB 1

//获取控件宽高XY坐标
#define HFRAME(view)  view.frame.size.height
#define WFRAME(view)  view.frame.size.width
#define XFRAME(view)  view.frame.origin.x
#define YFRAME(view)  view.frame.origin.y
#define YBOTTOM(view) view.frame.origin.y + view.frame.size.height
#define XRIGHT(view)  view.frame.origin.x + view.frame.size.width


#define USERENABLEDNO   [UIApplication sharedApplication].keyWindow.userInteractionEnabled=NO;
#define USERENABLEDYES  [UIApplication sharedApplication].keyWindow.userInteractionEnabled=YES;


//颜色
#pragma mark  ----->  常用颜色

#define LinesColor [UIColor colorWithRed:247.f/255.f green:247.f/255.f blue:247.f/255.f alpha:1]

#define COLORRED   [UIColor redColor]   //红色
#define COLORCLEAR [UIColor clearColor] //透明
#define COLORBLACK [UIColor blackColor] //黑色
#define COLORWHITE [UIColor whiteColor] //白色
#define COLORGRAY  [UIColor grayColor]  //灰色
#define BLACKCOLOR [UIColor blackColor] //黑色
#define COLORGROUP [UIColor colorWithRed:245.f/255.f green:245.f/255.f blue:245.f/255.f alpha:1] //表格背景灰
#define HRGCOLOR   [UIColor colorWithRed:232.f/255.f green:77.f/255.f blue:116.f/255.f alpha:1]

#define OtherColor  [UIColor colorWithRed:240.f/255.f green:15.f/255.f blue:39.f/255.f alpha:1]
//黑色字体颜色
#define  Black51 [UIColor colorWithRed:51.f/255.f green:51.f/255.f blue:51.f/255.f alpha:1]
#define  Black102 [UIColor colorWithRed:102.f/255.f green:102.f/255.f blue:102.f/255.f alpha:1]
#define  Black153 [UIColor colorWithRed:153.f/255.f green:153.f/255.f blue:153.f/255.f alpha:1]
#define  Black204 [UIColor colorWithRed:204.f/255.f green:204.f/255.f blue:204.f/255.f alpha:1]

#define  RGBA(r,g,b,a)  [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

//常用字体
#pragma mark  -------->  字体
#define Font12 [UIFont systemFontOfSize:12]
#define Font13 [UIFont systemFontOfSize:13]
#define Font14 [UIFont systemFontOfSize:14]
#define Font15 [UIFont systemFontOfSize:15]
#define Font16 [UIFont systemFontOfSize:16]
#define Font17 [UIFont systemFontOfSize:17]
#define Font18 [UIFont systemFontOfSize:18]

#define TITLEFONT  [MethodsClassObjc titleFont]  //导航栏标题

#define FONT_17  17.0
#define FONT_16  16.0
#define FONT_15  15.0  //一般文字大小
#define FONT_14  14.0
#define FONT_13  13.0
#define FONT_12  12.0  //最小号字体

//#define HomeLayoutType  2   //2:首页模块化的图片全部占满（去掉文字）



//-1未激活用户、0:消费者、1:代理商、2:运营商 3:运营商 4:试用代理
#define role_state          [MethodsClassObjc roleInt]
#define role_name_by(role)  [MethodsClassObjc getRoleNameByStatus:role]


//当前app登录用户的token
#define TOKEN         [MethodsClassObjc tokenStr]  //

//当前网络状态
#define NETSTATE      [MethodsClassObjc getNetWorkState]

//用户手机号
#define PhoneNumber   [MethodsClassObjc numberPhone]

//判断是否显示分享按钮
#define ifConsumerShowShare          [MethodsClassObjc getIfConsumerShowShare ]


#define IfShowNoLoginBtn   @"IfShowNoLoginBtn"        //是否显示游客登录按钮

#define APP_VersionValue       @"APP_VersionValue"            //本地存储APP版本号
#define TheApp_Version   [[[NSBundle mainBundle] infoDictionary]  objectForKey:@"CFBundleShortVersionString"]      //当前APP的版本号
#define IfFirstOpenUserCenter   @"IfFirstOpenUserCenter"        //是否首次显示个人中心页的引导图
#define IfFirstOpenHomeVC @"IfFirstOpenHomeVC"      //是否首次显示首页

#define SysGoodsShareType 2
/*************************************************************************************************/

#pragma mark --> 发送全局通知  通知名称

#define NetworkChange      @"network_change_state"    // 网络发生变化
#define HidePresentVC      @"hide_shareview"          // 接收到推送，隐藏模态出来的界面
#define AlipayPayRusult    @"pay_result_alipay"       // 支付结果(支付宝支付)
#define ResignActive       @"app_resign_active"       // 将要进入后台
#define EnterBackground    @"enter_foreground"        // 已经进入后台
#define ChangeTabbarSelect @"change_select_index"     // 修改底部导航栏选择索引
#define RefreshLiveTime    @"refresh_live_time"       // 淘抢购倒计时结束，进入下一场
#define LoginSuccess       @"login_success"           // 登录成功
#define UserInfoChange     @"user_info_change"        // 用户支付或使用试用码，身份状态发生改变
#define BDWechatSuccess    @"bang_ding_wechat_success"// 绑定微信成功
#define WechatLoginSuccess @"wechat_login_success"    // 微信登录成功
#define ShouldBDPhone      @"shuould_bang_ding_phone" // 微信授权成功，需要绑定手机号
#define ClickPop           @"ClickPop"                // 点击跳过广告

/*******************************************************************************************/

#pragma mark --> NSUserDefaults 存值取值关键字 (以time开头，表示存的是时间)

#define cache_role_status  @"cache_role_status"       // 缓存用户所有身份名称的数组

#define NumberPhone        @"NUMBERPHONE"             // 用户手机号 (格式：字符串)
#define Wechat_Info        @"wechat_user_info"        // 微信用户信息
#define UserInfo           @"USERINFO"                // 用户基本信息 (格式：字典)
#define UnreadMessages     @"UNREDMESSAGES"           // 未读消息个数 (格式：字符串)
#define SearchOld          @"SEARCHOLD"               // 搜索历史 (格式：数组)
#define HomeOneList        @"HOMEONEDATA"             // 首页前20条商品数据、banner、视频单区（格式：字典）
#define PromotionPoster    @"PROMOTIONPOSTER"         // 推广海报 (格式：数组)
#define ShopCommisDetail   @"SHOPCOMMISSIONDETAIL"    // 消费佣金明细 (格式：数组)
#define ShopCommisYUE      @"SHOPCOMMISSIONYUE"       // 消费佣金余额 (格式：字符串)
#define OneDayRanking      @"ONEDAYRANKINGLIST"       // 今日荣誉榜 (格式：数组)
#define SevenDayRanking    @"SEVENDAYRANKINGLIST"     // 七天荣誉榜 (格式：数组)
#define WalletDetails      @"WALLET_DETAILS"          // 钱包详情 (消费和代理合并的钱包)
#define ShareMuBan         @"SHARE_MU_BAN"            // 分享模板(用户自己编辑的)
#define NewConsumerData    @"NEW_CONSUMER_DATA"       // 消费佣金 (新的，仿淘宝联盟)

#define JD_ConsumerData    @"Jing_dong_Commission"    // 京东消费佣金(格式:字典)
#define TB_ConsumerData    @"Tao_bao_Commission"      // 淘宝消费佣金(格式:字典)

#define Search_onOrOff     @"SEARCH_ON_OR_OFF"        // 搜索商品是否显示券的开关状态
#define Share_goods_TiShi  @"Share_goods_TiShi"       // 判断是否是第一次提示

#define KeFuZhongXin       @"KeFuZhongXin_data"       // 客服中心(字典)
#define ApplyUpgrade       @"ApplyUpgrade_data"       // 升级信息(字典)

#define InterestStatus     @"InterestStatus"          // 每日利息增长推送
#define ShareStatus        @"ShareStatus"             // 批量分享接口开关
#define TeamStatus         @"TeamStatus"              // 新增用户推送
#define OrderStatus        @"OrderStatus"             // 新增订单推送
#define cacheS_list_style  @"goods_list_style"        // 商品列表存值关键字(1:横、2:方块)
#define ChangeGoodsList    @"change_goods_list"       // 修改


// ******  动态模块数据存储  ******//
#define ClassTitle               @"CLASSTITLE"              // 首页标题分类数据 (格式：数组)
#define app_dynamic_my_dic       @"app_dynamic_my_dic"      // 个人中心动态数据 (格式：数组)
#define app_dynamic_my_time      @"app_dynamic_my_time"     // 个人中心动态请求日期 (字符串：yyyy-mm-dd)
#define app_dynamic_home_dic     @"app_dynamic_home_dic"    // 首页动态数据 (格式：数组)
#define app_dynamic_home_time    @"app_dynamic_home_time"   // 首页动态请求日期 (字符串：yyyy-mm-dd)
#define app_dynamic_tabbar_dic   @"app_dynamic_tabbar_dic"  //
#define app_dynamic_tabbar_time  @"app_dynamic_tabbar_time" //


// 广告存取
#define Launch_last_app_version @"launch_last_app_version"    // 储存上一次显示引导页的版本
#define now_app_version [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define ad_launch_dic           @"ad_launch_dic"    // 启动页广告数据 (字典)
#define ad_popView_dic          @"ad_pop_view_dic"  // 弹窗广告数据 (字典)
#define ad_floating_dic         @"ad_floating_dic"  // 悬浮广告数据 (字典)
#define ad_launch_img           @"ad_launch_img_data"    // 启动页广告图片 (img)
#define ad_launch_imgUrl        @"ad_launch_img_url"     // 启动页广告图片 (img)
#define ad_popView_imgUrl       @"ad_pop_view_img_url"   // 弹窗广告图片链接 (字符串)
#define ad_floating_imgUrl      @"ad_floating_img_url"   // 悬浮广告图片链接 (字符串)



#endif /* Define_h */











