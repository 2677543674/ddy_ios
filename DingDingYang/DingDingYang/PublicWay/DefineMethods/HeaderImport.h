
/**
 
 这里导入的头文件，全局可共用
 
 
 **/

#import "TestTheEntranceVC.h"  // 测试功能入口


// *************************************************************************** //

#pragma mark ===>>> 头文件引用 (第三方)

// 友盟
#import "UMMobClick/MobClick.h"
#import <UMSocialCore/UMSocialCore.h>

// 腾讯
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"

// 阿里百川
#import <AlibcTradeSDK/AlibcTradeSDK.h>
#import <AlibabaAuthSDK/ALBBSDK.h>

// 京东开普勒
#import <JDKeplerSDK/JDKeplerSDK.h>

// 融云
#import <RongIMKit/RongIMKit.h>

// 其它
#import <FLAnimatedImageView.h>
#import <FLAnimatedImage.h>
#import <YYWebImage/YYWebImage.h>
#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"
#import "MBProgressHUD+MJ.h"
#import "JPUSHService.h"
#import "MJRefresh.h"
#import "Masonry.h"




// *************************************************************************** //

#pragma mark ===>>> 类别、和一些全局方法

// 类别
#import "UICollectionView+EndRefresh.h"
#import "UIAlertController+Category.h"
#import "UITableView+EndRefresh.h"
#import "UIImageView+Extension.h"
#import "UITextField+Category.h"
#import "UIView+CornerRadius.h"
#import "UIButton+Category.h"
#import "UILabel+Category.h"
#import "UIColor+hexColor.h"
#import "UIColor+extend.h"


#import "MethodsClassObjc.h" // 一些常用的方法
#import "MethodsDefine.h"    // 宏定义调用方法
#import "Define.h"           // 宏定义


#import "AFShareManager.h"   // 网络监测
#import "NoNetworkView.h"    // 请求失败时显示的点击可重试的自定义view
#import "NetRequest.h"       // 网络请求
#import "NetWorkUrl.h"       // 宏定义请求地址


#import "ClipboardMonitoring.h"  // 粘贴板有关
#import "ThirdPartyLogin.h"     // 第三方登录
#import "CustomUMShare.h"      // 封装分享
#import "ImgPreviewVC.h"      // 图片预览


// *************************************************************************** //

#pragma mark ===>>> 登录，和动态化，首页

#import "LoginVc.h"              // 登录
#import "MainTabbarVC.h"         // 主视图
#import "BdWechatOrTbVC.h"       // 绑定手机号
#import "AllEmptyDataCltCell.h"  // 空的cell(啥都没有)
#import "UniversalGoodsListVC.h" // 通用商品列表页

#import "HlayerCvCltCell.h"      // 动态中的子cell
#import "ModuleModel.h"          // 动态模块的model
#import "PageJump.h"             // 页面跳转

#import "GoodsDetailsVC.h"       // 商品详情页
#import "GoodsModel.h"           // 商品model

#import "FreeOrderModel.h"      // 免单商品model
#import "MyWelfareVC.h"         // 我的福利
#import "FreeOrderVC.h"         // 福利商城

#import "TradeWebVC.h"          // 封装的阿里百川web页面调用
#import "WebVC.h"               // web页面

#import "PDDSortingView.h"    // 商品筛选条件
#import "GoodsListCltCell1.h"


// *************************************************************************** //

#pragma mark ===>>> 个人中心的功能

#import "MyCenterNoDataView.h" // 个人中心无数据时显示的界面
#import "MyPageJump.h"         // 个人中心的页面跳转
#import "MyDynamicModel.h"     // 个人中心动态化model
#import "PersonalModel.h"      // 个人信息
#import "SharePopoverVC.h"     // 分享弹窗









