//
//  MethodsClassObjc.h
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MethodsClassObjc : NSObject

//正则表达式判断
+(BOOL)judgeSelectionExpression:(NSString*)way content:(NSString*)content;

//存值
+(void)saveDataForUesrdefaults:(id)value andKey:(NSString*)valueKey;

//取值
+(id)takeDataForUesrdefaults:(NSString*)key;

//删值
+(void)removeDataForUesrdefaults:(NSString*)key;

//类似安卓小提示框
+(void)showMessage:(NSString *)message;

//替换空字典
+(NSDictionary*)dicNull:(id)dict;

//替换空数组
+(NSArray*)aryNull:(id)ary;

//替换服务器<null>值为@""
+(NSString*)replaceEmptyStr:(NSString*)str;

//获取用户级别
+(NSInteger)roleInt;

// 获取相对级别对应的cheng称号
+(NSString*)getRoleNameByStatus:(id)state;

//获取token
+(NSString*)tokenStr;

//获取手机号
+(NSString*)numberPhone;

//四舍五入
+(NSString *)notRounding:(NSString*)price afterPt:(int)position;

//四舍五入取整数
+(float)quInteger:(float)number ;

//获取版本号和应用名称
+(NSString*)huoQuNameAndVersion;

//显示查询到的商品数量
+(void)showResult:(NSString*)str ;


//是否可以打开某个应用
+(BOOL)canOpenTaoBao ;
+(BOOL)canOpenWechat ;
+(BOOL)canOpenAlipay ;
+(BOOL)canOpenQQ ;

//返回标题字体
+(float)titleFont;

//屏幕宽高比例
+(float)ScreenWBiLi ;

//当前网络状态
+(NSInteger)getNetWorkState ;

//获取导航栏最顶层的视图
+(UINavigationController*)huoQuNavcTop ;

//获取时间、
+(NSString*)getNowTimeByFormat:(NSString*)format ;
//将时间戳转化为时间(时间长度字符串为毫秒)
+(NSString*)timeStamp:(NSString*)strTime timeFormat:(NSString*)format ;

//图片高斯模糊
//+ (UIImage *)boxblurImage:(UIImage *)image withBlurNumber:(CGFloat)blur; 

//MD5加密
+(NSString*)md5encrypting:(NSString *)mdStr ;

//将字符串转成二维码
+(UIImage*)changeImageWithStr:(NSString*)str ;

//返回商品列表样式
+(NSInteger)goodsListStyle ;

// 将字符串转成二维码图片
+(UIImage*)changeImageWithStr:(NSString*)str whi:(float)whi centerImg:(UIImage*)centerImg;

// 获取商品列表状态 拼多多
+(NSInteger)getGoodsListStyle;

//获取设备名
+ (NSString *)getDeviceType;

/**
 检测相册权限
 
 @param topvc 要显示弹框的vc
 @param photoPermissionsType 回调 type:1可以保存
 */
+(void)photoPermissionsNoOpenShowVC:(UIViewController*)topvc photoPermissionsType:(void(^)(NSInteger type))photoPermissionsType;

@end



@interface ShowMsgView : UIView
-(instancetype)initWithFrameCenter:(CGPoint)center bounds:(CGRect)rect;
@property(nonatomic,strong) UILabel * showLab;
@end














