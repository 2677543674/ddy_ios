//
//  ManyVersion.h
//  DingDingYang
//
//  Created by ddy on 2017/3/17.
//  Copyright © 2017年 ddy.All rights reserved.


#ifndef ManyVersion_h
#define ManyVersion_h


/* app内配置 */
//http://ddyqiniuimg.ddyvip.cn/
//http://ddlaike.jfapps.top/
#define default_domain_main      @"http://readapp.ddyvip.cn/app/"        // 主域名(没有域名则默认主域名)
#define default_domina_dev       @"http://apptest.times1688.com/app/"    //dev域名

#define default_domain_upimg     @"http://readapp.ddyvip.cn/app/"        // 图片上传前缀(没有域名则默认主域名)一样
#define default_domain_goods     @"http://tb.ddyvip.cn/app/"             // 搜索域名
#define default_domain_wphui     @"http://wph.ddyvip.cn/app/"            // 唯品会
#define default_domain_faquan    @"http://add.ddyvip.cn/api/"            // 一键发圈
#define default_domain_img_h     @"http://ddlaike.jfapps.top/"           // 服务器图片域名(拼接路径用)
#define default_domain_jdong     @"http://jd.ddyvip.cn/"                 // 京东搜索
#define default_domain_pdduo     @"http://pdd.ddyvip.cn/"                // 拼多多商品
#define default_domain_duomai    @"http://dm.ddyvip.cn/"                 // 多麦
#define default_domain_mini      @"https://miniweb.billsys168.com/lk/officialShare/getMinImg"  // 小程序码获取
#define default_domain_meituan   @"http://mt.ddyvip.cn/"
#define default_domain_da_ren    @"http://sh.yihongyuanzk.com/drg/cr/"   // 接口





#define PID              10107
#define APPSECRET        @"e57c463496247ac60bc29cac1b0d18d7"

#define LOGOCOLOR        [UIColor colorWithRed:244.f/255.f green:56.f/255.f blue:106.f/255.f alpha:1]
#define SHAREBTNCOLOR    [UIColor colorWithRed:244.f/255.f green:56.f/255.f blue:106.f/255.f alpha:0.7]

#define REDIRECTURL      @"BaoLaZhenXuan"   // URLScheame

#define APPNAME          @"叮叮羊微集"   // 关于我们应用名 和版本号
#define APPNAMEVERSION   [MethodsClassObjc huoQuNameAndVersion]

#define LOGONAME         @"logo"
#define LOGINBG          @"loginbackground"
#define LOGINLOGO        @"lognlogo"
#define SHARECONTENT     @"你不能不知道的淘宝天猫购物省钱内幕，薅羊毛首席攻略，惊天动地，就在这里"
#define APPLEID          @"1360997901"  // AppStore应用ID 、首页检测更新使用

//不同的app需要判断的值
#define LISTBTNTYPE      2   /* 代理商或运营商状态下  1:列表页为下单立省  2:分享赚钱  */
#define THEMECOLOR       1   /* 主题颜色深浅 1:主题颜色为深色  2:主题颜色为浅色(需要反转颜色) */
#define IsNeedSinaWeb    2   /* 是否需要微博 1:需要  2:不需要  */
#define IsNeedTaoBao     1   /* 是否使用淘宝授权 1:使用 2:不使用 */
#define IsNeedJingDong   1   /* 是否有京东 1:有  2:没有  */


//t42cfx

#pragma mark ===>>>      ***************  第三方 AppKey设置  ***************

/*  分享、统计有关  */

//友盟
#define YMAPPKEY   @"5aec1a7cb27b0a7e090000e6"
//微信
#define WXKEY      @"wx990d99ced4b2e8aa"
#define WXAS       @"390178b52cd6e8ad368c267b621d6924"
//QQ
#define QQKEY      @"1106731272"
#define QQAS       @"l21LGaWtqKgImXHb"
//微博
#define WBKEY      @""
#define WBAS       @""


/*  推送和IM  */

// 极光
#define JGAPPKEY           @"a3e6576207d035ba2f8f654e"
// 融云
#define rongcloud_APP_Key  @""




/*  阿里百川电商Key */
#define TBKEY        @"24855097"

/* 京东 */
#define JDKEY        @"8ea26d94c4674469b8b6054315fd02c1"
#define JDSECRET     @"0ce93677448f463d9fc32c0bc4fcb12c"


#endif /* ManyVersion_h */













