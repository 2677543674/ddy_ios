//
//  UICollectionView+EndRefresh.m
//  DingDingYang
//
//  Created by ddy on 2018/3/22.
//

#import "UICollectionView+EndRefresh.h"

@implementation UICollectionView (EndRefresh)

UIButton * topBtn;

/**
 结束刷新
 
 @param type 0:全部结束,上拉置为无数据状态  1:结束刷新
 */
-(void)endRefreshType:(NSInteger)type isReload:(BOOL)yn{
    if (yn) { [self reloadData]; }
    [self.mj_header endRefreshing];
    if (type>0) { [self.mj_footer endRefreshing]; }
    else{ [self.mj_footer endRefreshingWithNoMoreData]; }
}


-(void)addPlacedAtTheTopBtn:(UIButton*)btn{
    if (self.superview) {
        
        @try {
            [self addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:(__bridge void * _Nullable)(btn)];
            [btn setImage:[UIImage imageNamed:@"zhiding"] forState:(UIControlStateNormal)];
            [btn setImageEdgeInsets:UIEdgeInsetsMake(6*HWB, 6*HWB, 6*HWB, 6*HWB)];
            [btn boardWidth:1 boardColor:COLORGROUP corner:15*HWB]; btn.alpha = 0;
            btn.backgroundColor = RGBA(255.0, 255.0, 255.0,0.9);
            ADDTARGETBUTTON(btn, zhidingTop)
            [self.superview addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.mas_right).offset(-8*HWB);
                make.bottom.equalTo(self.mas_bottom).offset(-8*HWB);
                make.height.width.offset(30*HWB);
            }];
        } @catch (NSException *exception) { } @finally { }
       
    }
}

// 监控滑动事件
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{

    @try {
        UIButton * btn = (__bridge UIButton*)context;
        if ([btn isKindOfClass:[UIButton class]]) {
            if ([keyPath isEqualToString:@"contentOffset"]) {
                if (self.contentOffset.y>=0) {
                    if (self.contentOffset.y>ScreenH) {
                        btn.alpha = 1 ;
                    }else{
                        if (self.contentOffset.y>ScreenH/2) {
                            btn.alpha = self.contentOffset.y/ScreenH;
                        }else{
                            btn.alpha = 0 ;
                        }
                    }
                }
            }
        }
    } @catch (NSException *exception) { } @finally { }

}

-(void)zhidingTop{
    [self setContentOffset:CGPointZero animated:YES];
}

//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (self.contentOffset.y>=0) {
//        if (topBtn) {
//            if (self.contentOffset.y>ScreenH) {
//                topBtn.alpha = 1 ;
//            }else{
//                if (self.contentOffset.y>ScreenH/2) {
//                    topBtn.alpha = self.contentOffset.y/ScreenH;
//                }else{
//                    topBtn.alpha = 0 ;
//                }
//            }
//        }
//    }
//}


@end
