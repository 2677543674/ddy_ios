//
//  UIColor+hexColor.h
//  iCity
//
//  Created by songpei on 13-6-28.
//
//

#import <Foundation/Foundation.h>

@interface UIColor(category)

+(UIColor *)colorWithHexString:(NSString *)stringToConvert;

@end
