//
//  UITextField+Category.m
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "UITextField+Category.h"

@implementation UITextField (Category)

+(UITextField*)textColor:(UIColor*)color PlaceHorder:(NSString*)phStr font:(CGFloat)font{
    UITextField * txF = [[UITextField alloc]init];
    txF.text = @"";
    txF.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 36)];
    txF.leftViewMode = UITextFieldViewModeAlways;
    txF.placeholder = phStr;
    txF.layer.masksToBounds = YES;
    txF.layer.cornerRadius = 6  ;
    txF.font = [UIFont systemFontOfSize:font];
    return txF;
}

-(void)boardWidth:(CGFloat)width boardColor:(UIColor*)color corner:(CGFloat)radius{
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.borderWidth=width;
    self.layer.borderColor=color.CGColor;
    self.layer.cornerRadius=radius;
}

@end
