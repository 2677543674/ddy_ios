//
//  UIButton+Category.m
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//



#import "UIButton+Category.h"

@implementation UIButton (Category)

// 带有图片没有文字的按钮
+(UIButton*)bgImage:(NSString *)imgName{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btn setBackgroundImage:[UIImage imageNamed:imgName] forState:(UIControlStateNormal)];
    [btn setImage:[UIImage imageNamed:imgName] forState:(UIControlStateNormal)];
    return btn ;
}


// 带有标题的按钮
+(UIButton*)titLe:(NSString*)tit bgColor:(UIColor*)bgcolor titColorN:(UIColor*)colorn font:(CGFloat)font{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (bgcolor!=nil) {  btn.backgroundColor = bgcolor;  }
    [btn setTitle:tit forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitleColor:colorn forState:UIControlStateNormal];
    btn.titleLabel.adjustsFontSizeToFitWidth = YES;
    return btn;
}

+(UIButton*)buttonWithTitle:(NSString*)title font:(CGFloat)font backgroundColor:(UIColor*)color cornerRadius:(CGFloat)cornerRadius{
    UIButton* button=[UIButton new];
    button.titleLabel.font=[UIFont systemFontOfSize:font];
    [button setTitle:title forState:UIControlStateNormal];
    [button setBackgroundColor:color];
    button.layer.cornerRadius=cornerRadius;
    button.clipsToBounds=YES;
    //背景是白色
    if (CGColorEqualToColor(color.CGColor, [UIColor whiteColor].CGColor)) {
        [button setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
        button.layer.borderWidth=1;
        button.layer.borderColor=LOGOCOLOR.CGColor;
    }
    return button;
}

//创建带有标题的按钮
+(UIButton*)titLe:(NSString*)tit bgColor:(UIColor*)bgcolor titColorN:(UIColor*)colorn titColorH:(UIColor*)colorH font:(CGFloat)font{
    UIButton*btn=[UIButton buttonWithType:UIButtonTypeCustom];
    if (bgcolor!=nil) {  btn.backgroundColor = bgcolor;  }
    [btn setTitle:tit forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitleColor:colorn forState:UIControlStateNormal];
    [btn setTitleColor:colorH forState:UIControlStateHighlighted];
    btn.titleLabel.adjustsFontSizeToFitWidth=YES;
//    btn.layer.shouldRasterize = YES;
//    btn.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return btn;
}

-(void)addShadowLayer{
    self.layer.shadowColor=COLORGRAY.CGColor;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    self.layer.shadowOpacity=0.7;
}

-(void)setHighlighted:(BOOL)highlighted{
    if (self.layer.shadowOpacity>0) {
        if (highlighted) {
            self.layer.shadowColor=COLORGRAY.CGColor;
            self.layer.shadowOffset = CGSizeMake(1, 3);
            self.layer.shadowOpacity=0.8;
        }else{
            self.layer.shadowColor=COLORGRAY.CGColor;
            self.layer.shadowOffset = CGSizeMake(1, 1);
            self.layer.shadowOpacity=0.7;
        }
    }
}

-(void)startCountDown{
    //倒计时总时间
    __block int timeOut =90;
    //创建子线程队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    // 设置定 时器  每秒执行一次
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        // 倒计时结束，关闭
        if (timeOut <= 0) {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self!=nil) {
                    [self setTitle:@"重新获取" forState:UIControlStateNormal];
                    self.userInteractionEnabled = YES;
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self!=nil) {
                    [self setTitle:[NSString stringWithFormat:@"剩余:%0.2ds",timeOut] forState:UIControlStateNormal];
                    self.userInteractionEnabled = NO;
                }
            });
            timeOut--;
        }
    });
    dispatch_resume(_timer);
}

-(void)startCountDownDetail{
    //倒计时总时间
    __block int timeOut =2;
    //创建子线程队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    // 设置定 时器  每秒执行一次
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        // 倒计时结束，关闭
        if (timeOut <= 0) {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self!=nil) {
//                    [self setTitle:@"重新获取" forState:UIControlStateNormal];
                    self.userInteractionEnabled = YES;
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self!=nil) {
//                    [self setTitle:[NSString stringWithFormat:@"剩余:%0.2ds",timeOut] forState:UIControlStateNormal];
                    self.userInteractionEnabled = NO;
                }
            });
            timeOut--;
        }
    });
    dispatch_resume(_timer);
}

-(void)boardWidth:(CGFloat)width boardColor:(UIColor*)color corner:(CGFloat)radius{
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.masksToBounds=YES;
    self.layer.borderWidth=width;
    self.layer.borderColor=color.CGColor;
    self.layer.cornerRadius=radius;
}


-(void)corner:(float)radius{
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = radius ;
}

// 带有图片和标题的按钮
+(UIButton*)image:(NSString*)imgName titLe:(NSString*)tit bgColor:(UIColor*)bgcolor titColorN:(UIColor*)colorn font:(CGFloat)font{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imgName] forState:(UIControlStateNormal)];
    if (bgcolor!=nil) {  btn.backgroundColor = bgcolor;  }
    [btn setTitle:tit forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitleColor:colorn forState:UIControlStateNormal];
    return btn;
}


@end
