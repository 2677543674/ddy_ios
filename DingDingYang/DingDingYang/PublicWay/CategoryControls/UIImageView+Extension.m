//
//  UIImageView+Extension.m
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2017/3/1.
//  Copyright © 2017年 哈哈.All rights reserved.
//

#import "UIImageView+Extension.h"

@implementation UIImageView (Extension)

+(UIImageView*)imgName:(NSString*)name{
    UIImageView * imgV = [[UIImageView alloc]init];
    imgV.image = [UIImage imageNamed:name];
    return imgV;
}

@end
