//
//  UITableView+EndRefresh.h
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2018/3/20.
//

#import <UIKit/UIKit.h>

@interface UITableView (EndRefresh)

/**
 结束刷新
 
 @param type 0全部结束  1:全部结束,上拉置为无数据状态
 */
-(void)endRefreshType:(NSInteger)type isReload:(BOOL)yn;

@end
