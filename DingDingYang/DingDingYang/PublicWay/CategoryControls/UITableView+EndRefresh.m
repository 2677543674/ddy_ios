//
//  UITableView+EndRefresh.m
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2018/3/20.
//

#import "UITableView+EndRefresh.h"

@implementation UITableView (EndRefresh)


/**
 结束刷新

 @param type 0:全部结束,上拉置为无数据状态  1:结束刷新
 */
-(void)endRefreshType:(NSInteger)type isReload:(BOOL)yn{
    [self.mj_header endRefreshing];
    if (type>0) { [self.mj_footer endRefreshing]; }
    else{ [self.mj_footer endRefreshingWithNoMoreData]; }
    if (yn) { [self reloadData]; }
}

@end
