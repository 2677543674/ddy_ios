//
//  UICollectionView+EndRefresh.h
//  DingDingYang
//
//  Created by ddy on 2018/3/22.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (EndRefresh)


/**
 结束刷新
 
 @param type 0全部结束  1:全部结束,上拉置为无数据状态
 */
-(void)endRefreshType:(NSInteger)type isReload:(BOOL)yn;


-(void)addPlacedAtTheTopBtn:(UIButton*)btn;


@end
