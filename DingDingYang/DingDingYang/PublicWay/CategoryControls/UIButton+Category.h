//
//  UIButton+Category.h
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Category)
//创建按钮
+(UIButton*)titLe:(NSString*)tit bgColor:(UIColor*)bgcolor titColorN:(UIColor*)colorn titColorH:(UIColor*)colorH font:(CGFloat)font;

//倒计时
-(void)startCountDown;

-(void)startCountDownDetail;

-(void)addShadowLayer;

-(void)boardWidth:(CGFloat)width boardColor:(UIColor*)color corner:(CGFloat)radius;

+(UIButton*)buttonWithTitle:(NSString*)title font:(CGFloat)font backgroundColor:(UIColor*)color cornerRadius:(CGFloat)cornerRadius;

/**
 创建一个使用背景背图片的按钮
 
 @param imgName 图片名称
 @return  返回按钮
 */
+(UIButton*)bgImage:(NSString*)imgName;

/**
 添加圆角
 
 @param radius 圆角半径
 */
-(void)corner:(float)radius;

/**
 创建文字按钮
 
 @param tit     标题
 @param bgcolor 背景色
 @param colorn  标题颜色
 @param font    字体大小
 @return        返回按钮
 */
+(UIButton*)titLe:(NSString*)tit bgColor:(UIColor*)bgcolor titColorN:(UIColor*)colorn font:(CGFloat)font;

+(UIButton*)image:(NSString*)imgName titLe:(NSString*)tit bgColor:(UIColor*)bgcolor titColorN:(UIColor*)colorn font:(CGFloat)font;

@end
