//
//  UIImageView+Extension.h
//  Bnln_HuiShengHuo
//
//  Created by 哈哈 on 2017/3/1.
//  Copyright © 2017年 哈哈.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Extension)


/**
 创建图片

 @param name 图片名
 @return  返回图片控件
 */
+(UIImageView*)imgName:(NSString*)name;

@end
