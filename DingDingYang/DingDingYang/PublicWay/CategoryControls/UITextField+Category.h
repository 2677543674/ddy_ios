//
//  UITextField+Category.h
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Category)
+(UITextField*)textColor:(UIColor*)color PlaceHorder:(NSString*)phStr font:(CGFloat)font;
-(void)boardWidth:(CGFloat)width boardColor:(UIColor*)color corner:(CGFloat)radius ;

@end
