//
//  TestTheEntranceVC.h
//  DingDingYang
//
//  Created by ddy on 2018/1/2.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@class PersonalModel ;
@interface TestTheEntranceVC : DDYViewController

@property(nonatomic,strong) PersonalModel * infoModel ;

@end


// 测试用的
@interface TestWebVC : DDYViewController

@property(nonatomic,copy)   NSString * urlWeb ;
@property(nonatomic,strong) WKWebView * wkWebView ;
@property(nonatomic,strong) UIProgressView *progressView;
@property(nonatomic,strong) UIButton * shareWebBtn ; // 右侧分享按钮


@property(nonatomic,strong) PersonalModel * pmodel ;
@property(nonatomic,strong) NSDictionary  * nwdic ;

@end






