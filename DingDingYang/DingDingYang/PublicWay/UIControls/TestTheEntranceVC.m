//
//  TestTheEntranceVC.m
//  DingDingYang
//
//  Created by ddy on 2018/1/2.
//  Copyright © 2018年 ddy.All rights reserved.
//

#define web_mark  @"http://jfkjcodemix/?urlparameters="

#import "TestTheEntranceVC.h"

#import "MyWelfareVC.h"       // 我的福利
#import "FreeOrderVC.h"       // 福利商城
#import "PostersVC.h"         // 分享海报
#import "TradeWebVC.h"        // 封装的阿里百川web页面调用
#import "UpgradeVC.h"         // 加入我们 (新的页面)
#import "NewTeamVC.h"         // 新的团队
#import "NewOrmdVC.h"         // 新的官方推荐
#import "NewShareAppVC.h"     // 新的分享app
#import "MyFirstNewVC.h"      // 新的个人中心
#import "CommissionVC.h"      // 消费佣金(京东和淘宝都有的)
#import "NewConsumer.h"       // 新订单明细
#import "NewCallCeterVC.h"    // 新客服中心
#import "VideoTeachingVC.h"   // 视频教程
#import "NewWithdrawalVC.h"   // 新提现界面
#import "NewAboutVC.h"        // 新关于我们
#import "SharePopoverVC.h"    // 分享弹框
#import "BdWechatOrTbVC.h"    // 绑定手机号
#import "RankingListVC.h"
#import "MomentsVC.h"         // 花生圈
#import "FillOrderRewardVC.h" // 填单奖励
#import "DdyShopWebVC.h"      // 叮叮羊商城webview
#import "JingDongGoodsVC.h"   // 京东
#import "DMGoodsListVC.h"     // 多麦
#import "OperatorTeamListVC.h" // 省见的运营商列表
#import "OperatorEarningsVC.h" // 省见的运营商收益
#import "UnGoodsListVC.h"      // 拼多多商品
#import "ContactUsVC.h"        // 客服中心

#import "UnGoodsClasslistBigVC.h" // 通用的带有大分类的商品列表
#import "SDWebImageDownloader.h"

@interface TestTheEntranceVC () <UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView * tableView ;
@property(nonatomic,strong) NSArray * titleAry ;


@property(nonatomic,strong) NSMutableArray * fontAry ;
@property(nonatomic,strong) NSMutableArray * selectAry;


@end

@implementation TestTheEntranceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORWHITE ;
    
    self.title = @"测试功能" ;
    
    _titleAry = @[@"0: 福利商城",
                  @"1: 我的福利",
                  @"2: 打开淘宝购物车",
                  @"3: 淘宝头条",
                  @"4: 官方推荐",
                  @"5: 视频教程",
                  @"6: 花生圈",
                  @"7: 填单奖励",
                  @"8: web页面",
                  @"9: 多麦web页面",
                  @"10: 营商团队列表",
                  @"11: 运营商收益",
                  @"12: 京东精选",
                  @"13: 拼多多",
                  @"14: 唯品会",
                  @"15: 多麦",
                  @"16: 客服中心"];
    
    [self tableView];

}


//创建表格
-(UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate=self; _tableView.dataSource=self;
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);   make.right.offset(0);
            make.bottom.offset(0); make.top.offset(0);
        }];
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _titleAry.count ;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellID = @"cell_id" ;
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator ;
    }
    cell.textLabel.text = _titleAry[indexPath.row];
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    switch (indexPath.row) {

        case 0:{ // 福利商城
            FreeOrderVC * freeOrderVC = [[FreeOrderVC alloc]init];
            PushTo(freeOrderVC, YES)
        } break;
            
        case 1:{ // 我的福利
            MyWelfareVC * welfareVC = [[MyWelfareVC alloc]init];
            PushTo(welfareVC, YES)
        } break;
            
        case 2:{ // 打开淘宝购物车
            [MyPageJump pushNextVcByModel:nil byType:61];
        } break;
            
        case 3:{ // 淘宝头条
            TBNewsViewController * TBVC = [[TBNewsViewController alloc]init];
            [TopNavc pushViewController:TBVC animated:YES];
        } break;
            
        case 4:{ // 新的官方推荐
            NewOrmdVC * ormdVC = [[NewOrmdVC alloc]init];
            PushTo(ormdVC, YES)
        } break;
        
        case 5:{ // 视频教程
            VideoTeachingVC* teachVC = [VideoTeachingVC new];
            PushTo(teachVC, YES)
        } break;
            
        case 6:{ // 花生圈
            MomentsVC* VC=[MomentsVC new];
            VC.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:VC animated:YES];
        } break;
            
        case 7:{ // 填单奖励
            FillOrderRewardVC * fillOrder = [FillOrderRewardVC new];
            PushTo(fillOrder, YES)
        } break;
            
        case 8:{ // web页面
            TestWebVC * testWeb = [[TestWebVC alloc]init];
            PushTo(testWeb, YES)
        } break;
            
            
        case 9:{ // 叮叮羊web页面
            DdyShopWebVC * ddyWebVc = [[DdyShopWebVC alloc]init];
            ddyWebVc.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:ddyWebVc animated:YES];
        } break;
            
        case 10:{ // 省见运营商列表
            OperatorTeamListVC * otlVC = [[OperatorTeamListVC alloc]init];
            [TopNavc pushViewController:otlVC animated:YES];
        } break;
            
        case 11:{ // 运营商收益
            OperatorEarningsVC * earningVC = [[OperatorEarningsVC alloc]init];
            [TopNavc pushViewController:earningVC animated:YES];
        } break;
            
        case 12:{ // 京东精选
//            JingDongGoodsVC * jdGoodsList = [[JingDongGoodsVC alloc]init];
//            jdGoodsList.hidesBottomBarWhenPushed = YES ;
//            [TopNavc pushViewController:jdGoodsList animated:YES];
            
            UnGoodsClasslistBigVC * goodsClassVC = [[UnGoodsClasslistBigVC alloc]init];
            goodsClassVC.hidesBottomBarWhenPushed = YES; goodsClassVC.goodsSource = 2;
            [self.navigationController pushViewController:goodsClassVC animated:YES];
            
        } break;
            
        case 13:{ // 拼多多
//            UnGoodsListVC * uglvc = [[UnGoodsListVC alloc]init];
//            [TopNavc pushViewController:uglvc animated:YES];
            UnGoodsClasslistBigVC * goodsClassVC = [[UnGoodsClasslistBigVC alloc]init];
            goodsClassVC.hidesBottomBarWhenPushed = YES; goodsClassVC.goodsSource = 3;
            [self.navigationController pushViewController:goodsClassVC animated:YES];
        } break;
            
        case 14:{ // 唯品会
//            WPHGoodsListVC * uglvc = [[WPHGoodsListVC alloc]init];
//            uglvc.hidesBottomBarWhenPushed = YES ;
//            [TopNavc pushViewController:uglvc animated:YES];
           
            UnGoodsClasslistBigVC * goodsClassVC = [[UnGoodsClasslistBigVC alloc]init];
            goodsClassVC.hidesBottomBarWhenPushed = YES; goodsClassVC.goodsSource = 4;
            [self.navigationController pushViewController:goodsClassVC animated:YES];
        } break;
            
        case 15:{ // 多麦
//            DMGoodsListVC * dmGoodsList = [[DMGoodsListVC alloc]init];
//            [TopNavc pushViewController:dmGoodsList animated:YES];
            UnGoodsClasslistBigVC * goodsClassVC = [[UnGoodsClasslistBigVC alloc]init];
            goodsClassVC.hidesBottomBarWhenPushed = YES; goodsClassVC.goodsSource = 5;
            [self.navigationController pushViewController:goodsClassVC animated:YES];
        } break;
            
        case 16:{ // 新的客服中心
    
//            ContactUsVC * cusVc = [[ContactUsVC alloc]init];
//            PushTo(cusVc, YES)
            
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"imeituan://www.meituan.com/food/homepage/?lch=Bcps:x:0:b41daf38634de2d50f42133224a3689e266:6882442"]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tbopen://"]];
            
        } break;
            
            
        default:  break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end






@interface TestWebVC ()<WKUIDelegate,WKNavigationDelegate>

@end

@implementation TestWebVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"测试用的web页面" ;

    [self addTopTabbarView];

    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    WKPreferences *preference = [[WKPreferences alloc]init];
    config.preferences = preference;
    config.allowsInlineMediaPlayback = YES ;

    _urlWeb = @"http://c.duomai.com/track.php?site_id=243484&aid=4549&euid=testeuid&t=http%3A%2F%2Fwww.dusun.com.cn%2F";

    _wkWebView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config] ;
    _wkWebView.UIDelegate = self ;
    _wkWebView.navigationDelegate = self ;

//    NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"html"];
//    NSString *htmlString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
//    NSString * basePath = [[NSBundle mainBundle] bundlePath];
//    NSURL * baseURL = [NSURL fileURLWithPath:basePath];
//    [_wkWebView loadHTMLString:htmlString baseURL:baseURL];
    [_wkWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlWeb]]];
    [self.view addSubview:_wkWebView];
    [_wkWebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);  make.bottom.offset(0);
        make.left.offset(0); make.right.offset(0);
    }];


    //进度条初始化
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0,ScreenW,1.5)];
    _progressView.progressTintColor = RGBA(76, 217, 99, 1.0) ;
    _progressView.trackTintColor = [UIColor clearColor] ;
    _progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    _progressView.progress = 0 ;
    _progressView.hidden = YES ;
    [self.view addSubview:_progressView];

    [_wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [_wkWebView.scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];

}

// 监控web加载进度
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        [_progressView setProgress:self.wkWebView.estimatedProgress animated:YES];
        if (_progressView.progress == 1) {
            _progressView.hidden = YES ;
            _progressView.progress = 0 ;
        }
    }
    if ([keyPath isEqualToString:@"contentOffset"]) {
        NSDictionary * contentDic = (NSDictionary*)change;
        NSLog(@"%@",contentDic[@"new"]);
    }
    else {
//        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

// 将要加载webUrl  返回值：是否允许这个导航加载
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{

    NSString * urlString = navigationAction.request.URL.absoluteString;

    if ([urlString hasPrefix:@"http"]) {
        if ([urlString hasPrefix:@"http://jfkjcodemix/?urlparameters"]) {
            decisionHandler(WKNavigationActionPolicyCancel);
            [self interceptUrl:[urlString substringFromIndex:web_mark.length]];
        }else{
            decisionHandler(WKNavigationActionPolicyAllow);
        }
    }else if([[UIApplication sharedApplication]canOpenURL:navigationAction.request.URL]){
        decisionHandler(WKNavigationActionPolicyAllow);
        [[UIApplication sharedApplication]openURL:navigationAction.request.URL];
    }else{
        decisionHandler(WKNavigationActionPolicyCancel);
    }
}

-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    if (!navigationAction.targetFrame.isMainFrame) {   [webView loadRequest:navigationAction.request]; }
    return nil;
}

// 开始加载webUrl
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    _progressView.hidden = NO;
    _progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view bringSubviewToFront:_progressView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES ;
}

// 加载失败！
-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    LOG(@"加载失败：\n\n%@",error)
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
}

// 加载完成
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    
    if (webView.title.length>0) {
        self.title = webView.title ;
    }
    
    if (!webView.isLoading) {
        _progressView.hidden = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
    }

}


// 接收到需要重定向请求
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
//    LOG(@"接收到需要重定向的要求。。。")
}

// 加载失败时执行
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    LOG(@"ProvisionalNavigation失败:\n\n%@\n\n%@",navigation,error)
}


// 自定义返回按钮，和右侧按钮
-(void)addTopTabbarView{

    UIButton*backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"goback"] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(13, 0, 13, 33)];
    backBtn.frame=CGRectMake(0, 20, 44, 44);
    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem ;

}

// 返回上一级
-(void)clickBack{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
    if (_wkWebView.canGoBack==YES) {
        [_wkWebView goBack];
    }else{
        if (self.navigationController==nil) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }else{
            PopTo(YES)
        }
    }
}

// 解析并操作web页面传过来的数据
-(void)interceptUrl:(NSString*)urlParameter{

    NSString * jsonStr = [urlParameter stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSData * jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    _nwdic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    LOG(@"web页面传过来的内容：\n\n%@\n\n转成后的字典：\n\n%@",jsonStr,_nwdic);

    if (_nwdic==nil) {
        SHOWMSG(@"参数错误，解析失败!")
        return ;
    }

    switch ([REPLACENULL(_nwdic[@"type"]) integerValue]) {
        case 0: { [self openCopyStr]; } break;       // 启用复制功能
        case 1: { [self openWaiBuLianJie]; } break;  // 打开淘宝
        case 2: { [self openWaiBuLianJie]; } break;  // 打开支付宝
        case 3: { [self openTaoBao]; } break;        // 淘宝指定页面
        case 4: { [self openShare]; } break;         // 分享
        case 5: { [self openSharePoster]; } break;   // 分享海报
        case 6: { [self openSecondaryVC]; } break;   // 二级页面
        case 7: { } break; // 保存图片
        default:  break;
    }
}

#pragma mark ===>>> 响应web页面的事件
// 复制到粘贴板
-(void)openCopyStr{
    [ClipboardMonitoring pasteboard:REPLACENULL(_nwdic[@"data"][@"copyContent"])];
    if ([[UIPasteboard generalPasteboard].string isEqual:REPLACENULL(_nwdic[@"data"][@"copyContent"])]) {
        SHOWMSG(@"复制成功!")
    }
}

// 打开外部app链接 ( type为1和2使用 )
-(void)openWaiBuLianJie{
    NSURL * tburlStr = [NSURL URLWithString:REPLACENULL(_nwdic[@"data"][@"jumpUrl"])] ;
    if ([[UIApplication sharedApplication]canOpenURL:tburlStr]) {
        [[UIApplication sharedApplication]openURL:tburlStr];
    }else{  SHOWMSG(@"暂时不能打开页面!")  }
}

// 打开淘宝
-(void)openTaoBao{
    NSString * urlStr = REPLACENULL(_nwdic[@"data"][@"jumpUrl"]) ;
    if (IsNeedTaoBao==1) {
        if (urlStr.length>0) {
            [TradeWebVC showCustomBaiChuan:@"" url:urlStr pagetype:5];
        }
    }else{
        if ([urlStr hasPrefix:@"http"]) {
            WebVC * web = [[WebVC alloc]init];
            web.urlWeb = REPLACENULL(_nwdic[@"data"][@"jumpUrl"]);
            web.title = @"淘宝页面" ;
            PushTo(web, YES)
        }else{
            if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:urlStr]]) {
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlStr]];
            }else{  SHOWMSG(@"暂时不能打开页面!")  }
        }
    }
}

// 分享
-(void)openShare{

    NSInteger shareType = [REPLACENULL(_nwdic[@"data"][@"dataType"]) integerValue];

    NSString * shareTitle = REPLACENULL(_nwdic[@"data"][@"shareTitle"]) ;
    NSString * content    = REPLACENULL(_nwdic[@"data"][@"content"]) ;
    NSString * targetUrl  = REPLACENULL(_nwdic[@"data"][@"targetUrl"]) ;
    NSArray  * imageAry   = NULLARY(_nwdic[@"data"][@"imgUrl"]) ;

    if ([targetUrl hasPrefix:@"http"]) {
        shareTitle = shareTitle.length>0?shareTitle:APPNAME ;
        content    = content.length>0?content:SHARECONTENT ;
    }

    switch (shareType) {
        case 1:{ // 分享图片(可多图)
            if (imageAry.count>0) {
                NSMutableArray * aryImage = [NSMutableArray array];
                for (NSDictionary*dic in aryImage) {
                    NSString * url = REPLACENULL(dic[@"img"]);
                    [aryImage addObject:url];
                }
                [SharePopoverVC openSystemShareImage:aryImage];
            }
        } break;

        case 2:{ // 分享连接
            if ([targetUrl hasPrefix:@"http"]&&imageAry.count>0) {
                NSString * imgurl = REPLACENULL(imageAry[0][@"img"]) ;
                if ([imgurl hasPrefix:@"http"]) {
                    SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
                    [downloader downloadImageWithURL:[NSURL URLWithString:imgurl]  options:SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *  targetURL) {

                    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                        [SharePopoverVC shareUrl:targetUrl title:shareTitle des:content thubImg:image shareType:1];
                    }];
                }
            }else{
                [SharePopoverVC shareUrl:targetUrl title:shareTitle des:content thubImg:[UIImage imageNamed:LOGONAME] shareType:1];
            }
        } break;

        case 18:{ // 分享app
            NewShareAppVC*shareVC = [NewShareAppVC new] ;
            shareVC.hidesBottomBarWhenPushed = YES;
            shareVC.model = _pmodel ;
            PushTo(shareVC, YES);
        } break;

        default:  break;
    }
}

// 分享海报
-(void)openSharePoster{
    PostersVC*poster = [[PostersVC alloc]init];
    poster.hidesBottomBarWhenPushed = YES ;
    PushTo(poster, YES)
}

// 打开二级页面
-(void)openSecondaryVC{
    NSInteger pushvcType = [REPLACENULL(_nwdic[@"data"][@"dataType"]) integerValue];
    switch (pushvcType) {
        case 1: { // 商品详情
            NSDictionary * goodsDic = NULLDIC(_nwdic[@"data"][@"pageBean"]) ;
            GoodsModel * gmodel  = [[GoodsModel alloc]initWithDic:goodsDic] ;
            [PageJump pushToGoodsDetail:gmodel pushType:0];
        }  break;

        case 2: { // 淘抢购
            [PageJump yunPushType:3 content:@""];
        }  break;

        case 3: { // 限时抢购
            [PageJump yunPushType:7 content:@""];
        }  break;

        case 4: { // 官方推荐
            [PageJump yunPushType:16 content:@""];
        }  break;

        case 5: { // 福利商城
            FreeOrderVC * freeOrderVC = [[FreeOrderVC alloc]init];
            freeOrderVC.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:freeOrderVC animated:YES];
        }  break;

        case 6: { // 新手上路
            NewRoadVC * onroad = [[NewRoadVC alloc]init];
            onroad.hidesBottomBarWhenPushed = YES ;
            [TopNavc pushViewController:onroad animated:YES];
        }  break;

        case 7: { // 短视频
            [PageJump yunPushType:12 content:@""];
        }  break;

        case 17: { // 分类

        }  break;

        case 19: { // 商品分享页

        }  break;

        case 20: { // 加入代理
            UpgradeVC * arvc = [[UpgradeVC alloc]init];
            arvc.hidesBottomBarWhenPushed = YES ;
            PushTo(arvc, YES)
        }  break;


        case 21: { // 客服
            NewCallCeterVC * callcenter = [[NewCallCeterVC alloc]init];
            callcenter.hidesBottomBarWhenPushed = YES ;
            PushTo(callcenter, YES)
        }  break;

        case 22: { // 排行榜
            RankingListVC * rklvc = [[RankingListVC alloc]init];
            rklvc.hidesBottomBarWhenPushed = YES ;
            PushTo(rklvc, YES)
        }  break;

        default:{ // 进入商品分类

        } break;
    }
}








- (void)dealloc {
    [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.wkWebView.scrollView removeObserver:self forKeyPath:@"contentOffset"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id) self;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

@end


