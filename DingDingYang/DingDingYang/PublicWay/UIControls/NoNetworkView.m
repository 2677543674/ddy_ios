//
//  NoNetworkView.m
//  DingDingYang
//
//  Created by ddy on 2017/6/7.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "NoNetworkView.h"

@implementation NoNetworkView

+(void)showNoNetView:(UIView*)supView chs:(ChongShi)cs{
    NoNetworkView * nnView = [[NoNetworkView alloc]init];
    nnView.chs = cs ;
    [supView addSubview:nnView];
    [nnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(supView.mas_centerX);
        make.centerY.equalTo(supView.mas_centerY).offset(-30*HWB);
        make.width.offset(ScreenW);
//        make.height.offset(ScreenW);
        make.top.equalTo(supView.mas_top).offset(0);
        make.bottom.equalTo(supView.mas_bottom).offset(0);
    }];
}


-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = COLORGROUP ;
        [self addView];
    }
    return self ;
}

-(void)addView{
    _backView = [[UIView alloc]init];
    [self addSubview:_backView];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.height.offset(300);
        make.width.offset(300);
    }];
    
    _imgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"connectNetFail"]];
    [_backView addSubview:_imgV];
    [_imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_backView.mas_centerY).offset(-30);
        make.centerX.equalTo(_backView.mas_centerX);
        make.height.offset(96*HWB) ;
        make.width.offset(135*HWB) ;
    }];
    
    _tsLab = [UILabel labText:@"网络状态不佳，\n请点击此重新加载!" color:COLORGRAY font:14*HWB];
    _tsLab.numberOfLines = 0 ;
    _tsLab.textAlignment = NSTextAlignmentCenter ;
    [_backView addSubview:_tsLab];
    [_tsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_imgV.mas_bottom).offset(18);
        make.centerX.equalTo(_imgV.mas_centerX);
    }];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSelf)];
    [_backView addGestureRecognizer:tap];
}

-(void)removeSelf{
    self.chs(1) ;
    [self removeFromSuperview];
    
}

@end
