//
//  NoNetworkView.h
//  DingDingYang
//
//  Created by ddy on 2017/6/7.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoNetworkView : UIView



@property(nonatomic,strong) UIView * backView ;
@property(nonatomic,strong) UIImageView * imgV ;
@property(nonatomic,strong) UILabel * tsLab ;

typedef void(^ChongShi)( NSInteger click);

@property(nonatomic,assign) BOOL remove;

@property(nonatomic,strong) ChongShi chs ;

+(void)showNoNetView:(UIView*)supView chs:(ChongShi)cs ;

@end
