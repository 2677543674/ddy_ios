//
//  AFShareManager.h
//  DingDingYang
//
//  Created by ddy on 11/07/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface AFShareManager : AFHTTPSessionManager


+(AFHTTPSessionManager *)shareManager;

/**
 网络监测(监测结果发送全局通知)
 */
+(void)networkMonitoring ;

@property (nonatomic,strong) AFNetworkReachabilityManager * netManager ;





@end
