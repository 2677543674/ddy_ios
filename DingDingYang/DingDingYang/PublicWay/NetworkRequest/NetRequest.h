//
//  NetRequest.h
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFNetworking.h"
@interface NetRequest : NSObject

#pragma mark ===>>> 新的封装接口


/**
 请求数据接口
 
 @param type 请求类型  0:Post需要公共参数  1:Get需要公共参数  2:post不需要公共参数的 3:get不需要公共参数
 @param url 请求链接
 @param pdic 请求的参数
 @param completeS 请求成功的回调(有公共参数，rsult:ok 才回调)
 @param completeErrorResults 请求失败的回调(有公共参数，回调rsult:失败原因和网络错误；没有公共参数:只回调网络错误)
 */
+(void)requestType:(NSInteger)type url:(NSString*)url ptdic:(id)pdic success:(void (^)(id nwdic))completeS serverOrNetWorkError:(void(^)(NSInteger errorType, NSString * failure))completeErrorResults;

+(void)getRequest:(NSString*)url pdic:(id)pdic success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF;
+(void)postRequest:(NSString*)url pdic:(id)pdic success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF;


/**
 上传文件接口
 
 @param url 链接地址
 @param pdic 其他参数
 @param pName 文件名参数
 @param upType 文件类型后缀名(1:txt文档 其它:图片类型),
 @param data 文件( 支持类型: 数组(data或image类型)、单个data、单个image类型)
 @param completeS 成功回调
 @param uploadProgress 返回上传进度
 @param completeErrorResults 失败回调 (0(已请求成功，但服务器查询失败或未达到相关条件) 1:访问服务器失败(超时，网关错误，等等))
 */
+(void)urlUpFile:(NSString*)url ptdic:(id)pdic pName:(NSString*)name upType:(NSInteger)upType  data:(id)data success:(void (^)(id nwdic))completeS progress:(void(^)(NSProgress * uploadProgress))completeUploadProgress serverOrNetWorkError:(void(^)(NSInteger errorType, NSString * failure))completeErrorResults;


/**
 下载图片
 
 @param url 图片链接(字符串或数组)
 @param completeS 下载成功的图片或图片数组
 @param completeErrorResults 失败原因
 */
+(void)downloadImageByUrl:(id)url success:(void (^)(id result))completeS serverOrNetWorkError:(void(^)(NSInteger errorType, NSString * failure))completeErrorResults;



#pragma mark ===>>> 旧的封装接口
/**
 上传单张图片(未拼接token) 接口已加sign签名

 @param urlStr 请求地址     (里面会检测是否拼接有http,如果没有http，则会默认拼接图片上传域名)
 @param dic    请求参数     (NSDictionary格式、可为空)
 @param img    要上传的图片  (UIImage图片类型、不能为空，否则无法上传图片)
 @param name   放图片的字段名称
 @param completeS 请求成功回调  (格式: {"result":"OK"} 或 {"result":"上传失败!"} )
 @param completeF 请求失败回调  (格式:字符串; 内容:失败原因)
 */
//+(void)upFile:(NSString*)urlStr parameter:(id)dic img:(UIImage*)img fileN:(NSString*)name success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF;



/**
 Post请求接口，此接口直接拼接了token，无需再传token 接口已加sign签名

 @param urlstr (里面会检测是否拼接有http,如果没有http，则会默认拼接主域名、以goods开头的会拼接商品域名)
 @param dic    (NSDictionary格式、可为空)
 @param completeS  (格式: {"result":"OK"} 或 {"result":"服务器开小差!"} )
 @param completeF  (格式:字符串; 内容:失败原因)
 */
+(void)requestTokenURL:(NSString*)urlstr parameter:(id)dic success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF;



/**
 多图和参数上传(未拼接token) 接口已加sign签名

 @param urlStr    (里面会检测是否拼接有http,如果没有http，则会默认拼接主域名)
 @param dic       (NSDictionary格式、可为空)
 @param dataAry   (图片数组，UIImage图片类型)
 @param name      (放图片的字段名称)
 @param completeS 请求成功回调  (格式: {"result":"OK"} 或 {"result":"上传失败!"} )
 @param completeF 请求失败回调  (格式:字符串; 内容:失败原因)
 */
//+(void)upMoreFile:(NSString*)urlStr parameter:(id)dic data:(NSMutableArray*)dataAry fileN:(NSString*)name success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF;



/**
 上传文件接口 接口已加sign签名

 @param urlStr  请求地址        (里面会检测是否拼接有http,如果没有http，则会默认拼接主域名)
 @param dic     请求参数        (NSDictionary格式、可为空)
 @param filep   上传文件的参数名  (字符串: 不能为空)
 @param type    文件类型名称     (字符串: 不能为空, 比如txt、html、doc等，)
 @param name    文件名          (字符串: 不能为空)
 @param dataAry 文件数组        (NSData数组，同一种类型，才能同时正确上传多个文件)
 @param completeS 请求成功回调   (格式: {"result":"OK"} 或 {"result":"上传失败!"} )
 @param completeF 请求失败回调   (格式:字符串; 内容:失败原因)
 */
//+(void)upFile:(NSString*)urlStr parameter:(id)dic fileP:(NSString*)filep fileType:(NSString*)type fileName:(NSString*)name fileAry:(NSArray*)dataAry success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF ;



/**
 此接口，未拼接任何域名和参数，也无加密，

 @param urlstr  请求地址  (请填写完整的http请求链接)
 @param dic     请求参数  (NSDictionary格式、可为空)
 @param type    请求类型  (2:get请求; 空或其它都是post请求)
 @param completeS 请求成功回调  (返回id类型)
 @param completeF 请求失败回调  (返回失败原因)
 */
+(void)urlRequest:(NSString*)urlstr parameter:(id)dic getPost:(NSInteger)type success:(void (^)(id nwdic))completeS failure:(void(^)(NSString*failure))completeF;



/**
 根据失败code值，返回对应的提示语

 @param code  http失败错误码
 @return      提示语
 */
+(NSString*)codeStr:(NSInteger)code;



/**
 从服务器获取所有动态域名(接口)
 */
+(void)getDynamicDNS;



/**
 获取动态域名(从本地取值，如果为空，则返回默认的url)

 @param type  3:主域名  4: 图片上传域名  5:商品搜索域名  6:年化模块域名  7:京东域名   8:图片下载主域名 10：拼多多
 @return      域名url
 */
+(NSString*)getUrlByType:(NSInteger)type;



/**
 对比上次请求接口的时间和当前的时间
 
 @param tkey 储存的时间关键字
 @param completeResult 结果回调  0:表示存储的时间为空或两次时间不相等(也就是需要请求网络)  1:不用重新请求网络
 */
+(void)compareRequestTime:(NSString*)tkey result:(void(^)(NSInteger resultType))completeResult;


@end








