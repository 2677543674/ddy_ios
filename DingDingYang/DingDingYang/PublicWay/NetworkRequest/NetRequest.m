//
//  NetRequest.m
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//


#define OutTime 18.0 //请求超时时间
#define whether_log 0  // 是否打印网络请求0:全都打印 1:只打印请求 2:只打印返回json和请求 3都不打印

// 存值关键字
#define dns_main     @"key_url_mian"       // 接口域名(主域名)
#define dns_upimg    @"key_url_upimg"      // 图片上传域名
#define dns_goods    @"key_url_goods"      // 商品搜索域名
#define dns_nianhua  @"key_url_nianhua"    // 年化模块域名
#define dns_jd       @"key_url_jingdong"   // 京东域名
#define dns_loadimg  @"key_url_loadimg"    // 图片主域名
#define dns_pindd    @"key_url_pinduoduo"  // 拼多多商品主域名
#define dns_mini     @"key_url_mini"       // 获取小程序码
#define dns_faquan   @"key_url_faquan"     // 一键发圈
#define dns_weiph    @"key_url_weipinhui"  // 唯品会
#define dns_duomai   @"key_url_duomai"     // 多麦
#define dns_miandan  @"key_url_miandan"    // 福利商城(免单)
#define dns_meituan  @"key_url_meituan"    // 美团


#define IsNeedTimeSign   1    // 是否使用时间戳加密

#define uuid_str  FORMATSTR(@"%@/ios",[[UIDevice currentDevice].identifierForVendor UUIDString])
#define now_time  FORMATSTR(@"%.0f",[[NSDate date] timeIntervalSince1970]*1000)

#import "NetRequest.h"
#import <CommonCrypto/CommonDigest.h>


@implementation NetRequest

#pragma mark ===>>> 动态域名处理
// 获取动态域名
+(void)getDynamicDNS{
    //[self singStr:@{@"partnerId":PID}]
    AFHTTPSessionManager * manager = [AFShareManager shareManager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString * postUrl = FORMATSTR(@"%@%@",dns_dynamic_main,url_dynamic_dns) ;
    
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"partnerId"] = FORMATSTR(@"%d",PID);
    NSString * md5sign = [self singStr:dic];
    dic[@"sign"] = md5sign;
    
    [manager POST:postUrl parameters:dic progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString * str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        LOG(@"动态域名请求链接\n\n%@\n\n动态域名数据 ===>>>\n\n%@\n\n动态域名数据 <<<===",postUrl,str)
        NSDictionary * dicData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if ([REPLACENULL(dicData[@"result"]) isEqualToString:@"OK"]) {
            @try {
                NSArray * urlListAry = NULLARY(dicData[@"list"]);
                [urlListAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj) {
                        NSInteger urlType = [REPLACENULL(obj[@"type"]) integerValue];
                        NSString * url_dns = REPLACENULL(obj[@"domain"]) ;
                        if ([url_dns hasPrefix:@"http"]) {
                            switch (urlType) {
                                case 3:{ NSUDSaveData(url_dns, dns_main) }    break; // 接口域名(主域名)
                                case 4:{ NSUDSaveData(url_dns, dns_upimg) }   break; // 图片上传域名
                                case 5:{ NSUDSaveData(url_dns, dns_goods) }   break; // 商品搜索域名
                                case 6:{ NSUDSaveData(url_dns, dns_nianhua) } break; // 年化模块域名
                                case 7:{ NSUDSaveData(url_dns, dns_jd) }      break; // 京东域名
                                case 8:{ NSUDSaveData(url_dns, dns_loadimg) } break; // 图片下载主域名
                                case 9:{ NSUDSaveData(url_dns, dns_miandan) } break; // 免单
                                case 10:{ NSUDSaveData(url_dns, dns_pindd) }  break; // 拼多多主域名
                                case 11:{ NSUDSaveData(url_dns, dns_mini) }   break; // 小程序码域名
                                case 12:{ NSUDSaveData(url_dns, dns_faquan) } break; // 一键发圈域名
                                case 13:{ NSUDSaveData(url_dns, dns_weiph) }  break; // 唯品会
                                case 14:{ NSUDSaveData(url_dns, dns_duomai) } break; // 多麦
                                case 15:{ NSUDSaveData(url_dns, dns_meituan)} break; // 美团
                                default:  break;
                            }
                        }
                    }
                }];
            } @catch (NSException *exception) { } @finally { }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        LOG(@"动态域名失败错误信息:\n\n%@\n",error)
    }];
}

+(NSString*)getUrlByType:(NSInteger)type{
    @try {
        NSString * typeUrl = @"" ;
        switch (type) {
            case 3:{ typeUrl = NSUDTakeData(dns_main)?NSUDTakeData(dns_main):default_domain_main; } break;    // 接口域名(主域名)
            case 4:{ typeUrl = NSUDTakeData(dns_upimg)?NSUDTakeData(dns_upimg):default_domain_upimg; } break; // 图片上传域名
            case 5:{ typeUrl = NSUDTakeData(dns_goods)?NSUDTakeData(dns_goods):default_domain_goods; } break; // 商品搜索域名
            case 6:{ typeUrl = NSUDTakeData(dns_nianhua)?NSUDTakeData(dns_nianhua):default_domain_main; } break;  // 年化模块域名
            case 7:{ typeUrl = NSUDTakeData(dns_jd)?NSUDTakeData(dns_jd):default_domain_jdong; } break;           // 京东域名
            case 8:{ typeUrl = NSUDTakeData(dns_loadimg)?NSUDTakeData(dns_loadimg):default_domain_img_h; } break; // 图片下载主域名
            case 9:{ typeUrl = NSUDTakeData(dns_miandan)?NSUDTakeData(dns_miandan):default_domain_main; } break;  // 免单福利商城
            case 10:{ typeUrl = NSUDTakeData(dns_pindd)?NSUDTakeData(dns_pindd):default_domain_pdduo; } break;    // 拼多多商品主域名
            case 11:{ typeUrl = NSUDTakeData(dns_mini)?NSUDTakeData(dns_mini):default_domain_mini; }  break;      // 获取小程序码
            case 12:{ typeUrl = NSUDTakeData(dns_faquan)?NSUDTakeData(dns_faquan):default_domain_faquan; } break; // 一键发圈主域名
            case 13:{ typeUrl = NSUDTakeData(dns_weiph)?NSUDTakeData(dns_weiph):default_domain_wphui; } break;    // 唯品会动态域名
            case 14:{ typeUrl = NSUDTakeData(dns_duomai)?NSUDTakeData(dns_duomai):default_domain_duomai; } break; // 多麦动态域名
            case 15:{ typeUrl = NSUDTakeData(dns_meituan)?NSUDTakeData(dns_meituan):default_domain_meituan; } break; // 多麦动态域名
            default:{ typeUrl = default_domain_main; } break;
        }
        if ([typeUrl hasPrefix:@"http"]) { return typeUrl; } else { return default_domain_main; }
    } @catch (NSException *exception) { return default_domain_main; } @finally { }
}

#pragma mark ==========>>>>>>>>> 新的接口请求方法
+(void)requestType:(NSInteger)type url:(NSString*)url ptdic:(id)pdic success:(void (^)(id nwdic))completeS serverOrNetWorkError:(void(^)(NSInteger errorType, NSString * failure))completeErrorResults{
    
    @try {
      
        
        // 另外所需的公共参数， 拼接成 请求体中最终参数
        NSMutableDictionary * allPdic = [NSMutableDictionary dictionaryWithDictionary:pdic];
        
        // 链接
        NSString * urlStr = @"";
        // 有goods的接口跟地址换为read域名 ( 注：个别有goods的地方仍使用主域名,需在请求的地方拼接好 )
        if ([url hasPrefix:@"goods/"]) {
            urlStr = FORMATSTR(@"%@%@?partnerId=%d",dns_dynamic_goods,url,PID);
            if (![[allPdic allKeys] containsObject:@"ifAuth"]) { allPdic[@"ifAuth"] = @"1"; }
        }else if([url hasPrefix:@"http"]){  // 包含http的不需拼接，
            urlStr = FORMATSTR(@"%@?partnerId=%d",url,PID) ;
        }else{ // 没有http默认拼接服务器主域名
            urlStr = FORMATSTR(@"%@%@?partnerId=%d",dns_dynamic_main,url,PID) ;
        }
        
        if (type==0||type==1) { // 需要公共参数
            allPdic[@"deviceId"] = uuid_str; allPdic[@"time"] = now_time;
            if (TOKEN.length>0) { allPdic[@"token"] = TOKEN ; }
            allPdic[@"__rid"] = FORMATSTR(@"%ld",(long)role_state);
            allPdic[@"appVersion"] = FORMATSTR(@"ios/%@",TheApp_Version);
            NSString * md5sign = [self singStr:allPdic];
            allPdic[@"sign"] = md5sign ;
        }
        
        [self http:url pdic:allPdic]; // 打印请求链接
        
        if (type==1||type==3) { // get请求
            
            [self getRequest:urlStr pdic:allPdic success:^(NSDictionary *nwdic) { // 请求成功回调
                if (type==3) {  completeS(nwdic); } else {
                    if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) { completeS(nwdic); } else {
                        completeErrorResults(0,REPLACENULL(nwdic[@"result"]));
                        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"login"]) { [self ifResultIsLogin]; }
                    }
                }
            } failure:^(NSString *failure) { // 请求失败的回调
                // 出现网络错误在重新请求一次
                if ([failure containsString:@"网络连接已中断"]||[failure containsString:@"未能找到使用指定主机名的服务器"]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self getRequest:urlStr pdic:allPdic success:^(NSDictionary *nwdic) {
                            if (type==3) {  completeS(nwdic); } else {
                                if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) { completeS(nwdic); } else {
                                    completeErrorResults(0,REPLACENULL(nwdic[@"result"]));
                                    if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"login"]) { [self ifResultIsLogin]; }
                                }
                            }
                        } failure:^(NSString *failure) { completeErrorResults(1,failure); }];
                    });
                    
                }else{ completeErrorResults(1,failure); }
                
            }];
            
        } else { // post请求
            
            [self postRequest:urlStr pdic:allPdic success:^(NSDictionary *nwdic) { // 请求成功回调
                if (type==2) { completeS(nwdic); } else {
                    if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {  completeS(nwdic); } else {
                        completeErrorResults(0,REPLACENULL(nwdic[@"result"]));
                        if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"login"]) {  [self ifResultIsLogin]; }
                    }
                }
            } failure:^(NSString *failure) { // 请求失败的回调
                // 出现网络错误在重新请求一次
                if ([failure containsString:@"网络连接已中断"]||[failure containsString:@"未能找到使用指定主机名的服务器"]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self postRequest:urlStr pdic:allPdic success:^(NSDictionary *nwdic) {
                            if (type==2) { completeS(nwdic); } else {
                                if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) { completeS(nwdic);} else {
                                    completeErrorResults(0,REPLACENULL(nwdic[@"result"]));
                                    if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"login"]) { [self ifResultIsLogin];}
                                }
                            }
                        } failure:^(NSString *failure) { completeErrorResults(1,failure); }];
                    });
                    
                }else{ completeErrorResults(1,failure); }
                
            }];
        }
    } @catch (NSException *exception) {
        completeErrorResults(1,@"数据异常!");
    } @finally { }
 
    
    
}

+(void)getRequest:(NSString*)url pdic:(id)pdic success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF{
    
    @try {
        AFHTTPSessionManager *manager = [AFShareManager shareManager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        manager.requestSerializer.timeoutInterval = OutTime ;
        [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
        [self http:url pdic:pdic]; //打印请求链接
        [manager GET:url parameters:pdic progress:nil success:^(NSURLSessionDataTask * task, id responseObject) {
            
            [self nalogHttp:url pdic:pdic resdate:responseObject type:2];
            NSDictionary * dicData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (dicData) { completeS(NULLDIC(dicData)); }
            else{ completeF(@"返回数据为空，或数据格式有误!"); }
            
        } failure:^(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
            NSString * codeStr = [self codeStr:responses.statusCode];
            if (codeStr.length > 0) { completeF(codeStr); }
            else{  completeF(error.localizedDescription);  }
            [self nalogHttp:url pdic:pdic error:error type:2];
        }];
    } @catch (NSException *exception) { completeF(@"请求失败!"); } @finally { }
    
}

+(void)postRequest:(NSString*)url pdic:(id)pdic success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF{
    
    @try {
        AFHTTPSessionManager *manager = [AFShareManager shareManager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        manager.requestSerializer.timeoutInterval = OutTime ;
        [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
        
        [self http:url pdic:pdic]; //打印请求链接
        
        [manager POST:url parameters:pdic progress:nil success:^(NSURLSessionDataTask *  task, id   responseObject) {
            
            [self nalogHttp:url pdic:pdic resdate:responseObject type:1];
            NSDictionary * dicData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (dicData) { completeS(NULLDIC(dicData)); }
            else{ completeF(@"返回数据为空，或JSON格式有误!"); }
            
        } failure:^(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
            NSString * codeStr = [self codeStr:responses.statusCode];
            if (codeStr.length > 0) { completeF(codeStr); }
            else{  completeF(error.localizedDescription);  }
            [self nalogHttp:url pdic:pdic error:error type:1];
        }];
    } @catch (NSException *exception) { completeF(@"请求失败!"); } @finally { }

}

#pragma mark ===>>> 上传文件
+(void)urlUpFile:(NSString *)url ptdic:(id)pdic pName:(NSString *)name upType:(NSInteger)upType data:(id)data success:(void (^)(id))completeS progress:(void (^)(NSProgress *))completeUploadProgress serverOrNetWorkError:(void (^)(NSInteger, NSString *))completeErrorResults{
    
    NSString * upUrl = FORMATSTR(@"%@?partnerId=%d",url,PID) ;
    AFHTTPSessionManager * manager = [AFShareManager shareManager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // 请求体中最终参数
    NSMutableDictionary * allPdic = [[NSMutableDictionary alloc]initWithDictionary:pdic];
    if (TOKEN.length>0) { allPdic[@"token"] = TOKEN; }
    allPdic[@"deviceId"] = uuid_str ;
    allPdic[@"time"] = now_time ;
    NSString * md5sign = [self singStr:allPdic]; //md5加密
    allPdic[@"sign"] = md5sign ;
    
    
    NSInteger ftype = 2;  // 记录当前data类型 1:数组 2:image 3:data
    if (data) {
        if ([data isKindOfClass:[NSArray class]]||[data isKindOfClass:[NSMutableArray class]]) {
            ftype = 1 ;
        }
        if ([data isKindOfClass:[UIImage class]]) { ftype = 2; }
        if ([data isKindOfClass:[NSData class]]) { ftype = 3 ; }
    }
    
    // 图片大小不能超过500kb
    // Content-type 对照表:http://tool.oschina.net/commons
    NSString * fileName = @"image/jpeg";
    if (upType==1) { fileName = @"text/plain"; }
    
    [manager POST:upUrl parameters:allPdic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        @try {
            switch (ftype) {
                case 1:{ // 数组类型(二进制或图片)
                    NSArray * dataAry = (NSArray*)data;
                    if (dataAry.count>0) {
                        
                        for (id dt in dataAry) {
                            if ([dt isKindOfClass:[NSData class]]) {
                                [formData appendPartWithFileData:dt name:name fileName:fileName mimeType:fileName];
                                NSData * d = (NSData*)dt;
                                float dataLength = [FORMATSTR(@"%lu",(unsigned long)d.length) floatValue]/1000;
                                NSLog(@"文件大小: %.2fKb",dataLength);
                            }
                            if ([dt isKindOfClass:[UIImage class]]) {
                                NSData * upDt = UIImageJPEGRepresentation(dt, 1);
                                float dataLength = [FORMATSTR(@"%lu",(unsigned long)upDt.length) floatValue]/1000;
                                NSLog(@"文件大小: %.2fKb",dataLength);
                                if (dataLength>500) {
                                    float  bili = 1.0 , fl1 ;
                                    for (int i=9; i<10;i--) {
                                        bili = [FORMATSTR(@"%d",i) floatValue]/10 ;
                                        upDt = UIImageJPEGRepresentation(dt,bili);
                                        fl1 = [FORMATSTR(@"%lu",(unsigned long)upDt.length) floatValue]/1000;
                                        if (fl1<=500) {
                                            NSLog(@"压缩后文件大小约为: %.2fkb",fl1);
                                            [formData appendPartWithFileData:upDt name:name fileName:fileName mimeType:fileName];
                                            break ;
                                        }
                                    }
                                }else{
                                    [formData appendPartWithFileData:upDt name:name fileName:fileName mimeType:fileName];
                                }
                            }
                        }
                        
                    }
                } break;
                    
                case 2:{ // 单张图片
                    NSData * upDt = UIImageJPEGRepresentation(data, 1);
                    float dataLength = [FORMATSTR(@"%lu",(unsigned long)upDt.length) floatValue]/1000;
                    NSLog(@"文件大小: %.2fKb",dataLength);
                    if (dataLength>500) {
                        float  bili = 1.0 , fl1 ;
                        for (int i=9; i<10;i--) {
                            bili = [FORMATSTR(@"%d",i) floatValue]/10 ;
                            upDt = UIImageJPEGRepresentation(data,bili);
                            fl1 = [FORMATSTR(@"%lu",(unsigned long)upDt.length) floatValue]/1000;
                            if (fl1<=500) {
                                NSLog(@"压缩后文件大小约为: %.2fkb",fl1);
                                [formData appendPartWithFileData:upDt name:name fileName:fileName mimeType:fileName];
                                break ;
                            }
                        }
                    }else{
                        [formData appendPartWithFileData:upDt name:name fileName:fileName mimeType:fileName];
                    }
                } break;
                    
                case 3:{ // data为NSData类型
                    NSData * dt = (NSData*)data;
                    [formData appendPartWithFileData:dt name:name fileName:fileName mimeType:fileName];
                    float dataLength = [FORMATSTR(@"%lu",(unsigned long)dt.length) floatValue]/1000;
                    NSLog(@"文件大小: %.2fKb",dataLength);
                } break;
                    
                default:  break;
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"上传进度: %.2f",uploadProgress.fractionCompleted);
        completeUploadProgress(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self nalogHttp:url pdic:allPdic resdate:responseObject type:1];
        NSDictionary * dicData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (dicData) {
            if ([REPLACENULL(dicData[@"result"]) isEqualToString:@"OK"]) {
                completeS(dicData);
            }else{
                completeErrorResults(0,REPLACENULL(dicData[@"result"]));
                if ([REPLACENULL(dicData[@"result"]) isEqualToString:@"login"]) {
                    [self ifResultIsLogin];
                }
            }
        }else{
            completeErrorResults(0,@"返回数据为空，或JSON格式有误!");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSString * codeStr = [self codeStr:responses.statusCode];
        if (codeStr.length > 0) { completeErrorResults(1,codeStr); }
        else{  completeErrorResults(1,error.localizedDescription);  }
        [self nalogHttp:url pdic:pdic error:error type:1];
    }];
    
}




#pragma mark =========>>>>>>>>> 旧的接口请求的方法

// 此接口，不拼接任何参数   type=1:post   type=2:get
+(void)urlRequest:(NSString*)urlstr parameter:(id)dic getPost:(NSInteger)type success:(void (^)(id nwdic))completeS failure:(void(^)(NSString*failure))completeF{
    
    AFHTTPSessionManager *manager = [AFShareManager shareManager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = OutTime ;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    [self http:urlstr pdic:dic]; //打印请求链接
    
    if (type==2) {
        [manager GET:urlstr parameters:dic progress:nil success:^(NSURLSessionDataTask *  task, id   responseObject) {
            [self nalogHttp:urlstr pdic:dic resdate:responseObject type:2];
            NSDictionary*dicData=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (dicData) {  completeS(NULLDIC(dicData));  }
            else { completeS(responseObject) ;}
        } failure:^(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
            NSString * codeStr = [self codeStr:responses.statusCode];
            if (codeStr.length > 0) { completeF(codeStr); }
            else{  completeF(error.localizedDescription);  }
            [self nalogHttp:urlstr pdic:dic error:error type:2];
        }];
    }else{
        [manager POST:urlstr parameters:dic progress:nil success:^(NSURLSessionDataTask *  task, id   responseObject) {
            [self nalogHttp:urlstr pdic:dic resdate:responseObject type:1];
            NSDictionary*dicData=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (dicData) {  completeS(NULLDIC(dicData));  }
            else { completeS(responseObject) ;}
        } failure:^(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
            NSString * codeStr = [self codeStr:responses.statusCode];
            if (codeStr.length > 0) { completeF(codeStr); }
            else{  completeF(error.localizedDescription);  }
            [self nalogHttp:urlstr pdic:dic error:error type:2];
        }];
    }
}


+(void)requestTokenURL:(NSString*)urlstr parameter:(id)dic success:(void (^)(NSDictionary *nwdic))completeS failure:(void(^)(NSString*failure))completeF{
    
    AFHTTPSessionManager *manager = [AFShareManager shareManager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = OutTime ;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    // 请求体中最终参数
    NSMutableDictionary * ptd = [[NSMutableDictionary alloc]initWithDictionary:dic];
    ptd[@"deviceId"] = uuid_str;  ptd[@"time"] = now_time;
    ptd[@"__rid"] = FORMATSTR(@"%ld",(long)role_state);
    ptd[@"appVersion"] = FORMATSTR(@"ios/%@",TheApp_Version);
    if (TOKEN.length>0) { ptd[@"token"] = TOKEN; }
   
    NSString * url = @"" ;
    // 有goods的接口跟地址换为read域名 ( 注：个别有goods的地方仍使用主域名,需在请求的地方拼接好 )
    if ([urlstr hasPrefix:@"goods/"]) {
        url = FORMATSTR(@"%@%@?partnerId=%d",dns_dynamic_goods,urlstr,PID);
        if (![[ptd allKeys] containsObject:@"ifAuth"]) { ptd[@"ifAuth"] = @"1"; }
    }else if([urlstr hasPrefix:@"http"]){  // 包含http的不需拼接，
        url = FORMATSTR(@"%@?partnerId=%d",urlstr,PID) ;
    }else{ // 没有http默认拼接服务器主域名
        url = FORMATSTR(@"%@%@?partnerId=%d",dns_dynamic_main,urlstr,PID) ;
    }
    
    // md5加密
    NSString * md5sign = [self singStr:ptd];
    ptd[@"sign"] = md5sign;
    
    [self http:url pdic:ptd]; // 打印请求链接
    
    [manager POST:url parameters:ptd progress:nil success:^(NSURLSessionDataTask *  task, id   responseObject) {
        
        [self nalogHttp:url pdic:ptd resdate:responseObject type:1];
        
        NSDictionary*dicData=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        completeS(NULLDIC(dicData));
        
        if ([REPLACENULL(dicData[@"result"]) isEqualToString:@"login"]) {  [self ifResultIsLogin]; }

    } failure:^(NSURLSessionDataTask * task, NSError * error) {
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSString * codeStr = [self codeStr:responses.statusCode];
//        NSLog(@"%@",url);
//        NSLog(@"%@",codeStr);
        if (codeStr.length > 0) { completeF(codeStr); }
        else{  completeF(error.localizedDescription);  }
        [self nalogHttp:url pdic:ptd error:error type:1];
    }];
    
}


#pragma mark ===>>> 无验证码登录 (当返回login时才会调用)
// 判断是否返回login
+(void)ifResultIsLogin{
    if (PhoneNumber.length==11&&TOKEN.length>0) {
        NSUDRemoveData(@"TOKEN")
        [self mianMiLogin];
    }else{
        // 遇到需要登录的，但没有登录，服务器返回login，
//        if ([REPLACENULL(dict[@"result"]) isEqualToString:@"login"]) {
            if (TopNavc!=nil) {
                [LoginVc showLoginVC:TopNavc.topViewController fromType:3 loginBlock:^(NSInteger result) { }];
            }else{
                [LoginVc showLoginVC:nil fromType:0 loginBlock:^(NSInteger result) { }];
            }
//        }
    }
}
+(void)mianMiLogin{

    [NetRequest requestType:0 url:url_mian_mi_login ptdic:@{@"mobile":PhoneNumber} success:^(id nwdic) {
        if (REPLACENULL(nwdic[@"token"]).length>0) {
            NSUDSaveData(REPLACENULL(nwdic[@"token"]), @"TOKEN")
            NSUDSaveData(NULLDIC(nwdic[@"user"]), UserInfo);
        }else{
            [LoginVc showLoginVC:nil fromType:0 loginBlock:^(NSInteger result) { }];
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [LoginVc showLoginVC:nil fromType:0 loginBlock:^(NSInteger result) { }];
    }];
   
}
// 首次调用免密登录失败，两秒后再调用一次
+(void)againLogin{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        static dispatch_once_t onceToken;
        if (onceToken==0) {
            dispatch_once(&onceToken, ^{  [self mianMiLogin]; });
        }else{
            [LoginVc showLoginVC:nil fromType:0 loginBlock:^(NSInteger result) { }];
            SHOWMSG(@"登录信息已过期，请重新登录!")
        }
    });
}

#pragma mark ===>>> sign签名加密
+(NSString*)singStr:(NSDictionary*)dic{
    NSArray * arraykey = [dic allKeys];
    NSMutableArray * allAry = [NSMutableArray arrayWithArray:arraykey];
    [allAry addObject:@"partnerId"];
    NSArray *result = [allAry sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2]; //升序: A~Z
    }];
    
    NSString * md5 = @"";
    for (NSString * canshu in result) {
        if ([canshu isEqualToString:@"partnerId"]) {
            md5 = FORMATSTR(@"%@%d",md5,PID);
        }else{
            md5 = FORMATSTR(@"%@%@",md5,dic[canshu]);
        }
    }
    md5 = FORMATSTR(@"%@%@",md5,APPSECRET);
    NSString * md5canshu = [self MD5:md5];
    return md5canshu ;
}

// MD5加密
+(NSString *)MD5:(NSString *)mdStr{
    //转换成utf-8
    const char *original_str = [mdStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_LONG cclong = (CC_LONG)strlen(original_str);
    CC_MD5(original_str, cclong , result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}


#pragma mark ===>>> 打印请求内容
// 打印请求链接和参数
+(void)http:(NSString*)url pdic:(id)dic{
    
    if (whether_log==0||whether_log==1) {
        NSString * pjurl = [self pinJieUrl:url pdic:dic];
        [self nslogRequestUrlJson:pjurl];
    }
    
}

// 打印请求链接和请求成功的参数
+(void)nalogHttp:(NSString*)url pdic:(id)dic resdate:(id)objc type:(NSInteger)type{
    
    @try {
        if (whether_log==0||whether_log==2) {
            NSString * getStr = [self pinJieUrl:url pdic:dic];
            
            //        [self nslogRequestUrlJson:getStr];
            
            NSString * str = [[NSString alloc]initWithData:objc encoding:NSUTF8StringEncoding];
            NSString * postget = @"POST" ; if( type == 2 ) { postget = @"GET" ; }
            LOG(@"%@ 请求成功的链接 : \n\n%@\n\n返回的JSON : \n\n%@\n",postget,getStr,str)
        }else{
            
        }
    } @catch (NSException *exception) { } @finally { }
    
}

// 打印请求失败错误信息
+(void)nalogHttp:(NSString*)url pdic:(id)dic error:(NSError*)error type:(NSInteger)type{
    
    NSString * getStr = [self pinJieUrl:url pdic:dic];
//    [self nslogRequestUrlJson:getStr];
    NSString * postget = @"POST" ; if(type==2) { postget = @"GET" ; }
    LOG(@"%@请求失败的链接:\n\n%@\n\n失败错误信息:\n\n%@\n",postget,getStr,error)
    
}

+(NSString*)pinJieUrl:(NSString*)url pdic:(id)dic{
    @try {
        if ([dic isKindOfClass:[NSDictionary class]]||[dic isKindOfClass:[NSMutableDictionary class]]) {
            NSArray * aryKey = [dic allKeys];
            NSString * getStr = url;
            for (int a = 0; a < aryKey.count; a++) {
                NSString * key = aryKey[a];
                getStr = FORMATSTR(@"%@&%@=%@",getStr,key,dic[key]);
            }
            return getStr;
        }else{ return @""; }
    } @catch (NSException *exception) { return @""; } @finally { }
   
}

+(void)nslogRequestUrlJson:(NSString*)getStr{
    @try {
        if ([getStr containsString:@"?"]) {
            NSArray * fenge = [getStr componentsSeparatedByString:@"?"] ;
            NSString * canshuStr = [fenge lastObject];
            NSArray * arr = [canshuStr componentsSeparatedByString:@"&"];
            NSArray * a0 = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                NSUInteger len0 = [(NSString *)obj1 length];
                NSUInteger len1 = [(NSString *)obj2 length];
                return len0 > len1 ? NSOrderedAscending : NSOrderedDescending;
            }];
            canshuStr = [a0 componentsJoinedByString:@"\",\n   \""];
            canshuStr = [canshuStr stringByReplacingOccurrencesOfString:@"=" withString:@"\":\""];
            canshuStr = FORMATSTR(@" {\n   \"域名和接口名:\":\"%@\",\n\n   \"*** 参数名 ***\":\"---------- ****** 参数值 ****** ----------\",\n\n   \"%@\"\n }",[fenge firstObject],canshuStr);
            LOG(@" ==================== ****** 请求的域名、方法名、参数 ****** ====================\n%@",canshuStr)
        }else{
            //         NSLog(@"请求接口无参数。。。。。。");
        }
    } @catch (NSException *exception) { } @finally { }
}


#pragma mark ===>>> 返回请求失败服务器状态码
+(NSString*)codeStr:(NSInteger)code{
    
    NSString * scode = @"" ;
    switch (code) {
        case 202: { scode = @"正紧急通知服务器处理您的请求!" ;} break;
        case 400: { scode = @"参数有误，您的操作姿势有待改进哦!" ;} break;
        case 401: { scode = @"不好意思，当前用户无法验证!" ;} break;
        case 403: { scode = @"太频繁啦，您先歇息一下!" ;} break;
        case 404: { scode = @"您的请求失败，无法连接到服务器!" ;} break;
        case 408: { scode = @"哎呀，超时啦!" ;} break;
        case 409: { scode = @"请求内容和请求资源存在冲突!" ;} break;
        case 410: { scode = @"请求资源不可用!" ;} break;
        case 413: { scode = @"您的文件太大,请压缩后再试!" ;} break;
        case 414: { scode = @"请求链接过长，服务器无法处理!" ;} break;
        case 415: { scode = @"请求格式不支持，服务器无法处理!" ;} break;
        case 421: { scode = @"服务器支持请求数量已达上限!" ; } break;
        case 422: { scode = @"请求格式正确，但语义错误，无法响应!" ;} break;
        case 500: { scode = @"服务器繁忙，请稍后再试!" ;} break;
        case 501: { scode = @"服务器不支持当前请求!" ;} break;
        case 502: { scode = @"服务器未响应，请稍后再试!" ;} break;
        case 503: { scode = @"服务器内部繁忙，请稍后再试!" ;} break;
        case 504: { scode = @"哎呀，请求服务器超时啦!" ;} break;
        case 505: { scode = @"服务器不支持当前请求!" ;} break;
        case 507: { scode = @"服务器努力过，但依然无法储存当前的数据!" ;} break;
            
        default: break;
    }
    return scode;
}

#pragma mark ===>>> 下载图片 (支持多张)

+(void)downloadImageByUrl:(id)url success:(void (^)(id result))completeS serverOrNetWorkError:(void(^)(NSInteger errorType, NSString * failure))completeErrorResults{
    
    @try {
        LOG(@"下载的图片链接: \n%@",url)
        SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
//        downloader.downloadTimeout = 6;
        
        if ([url isKindOfClass:[NSString class]]) { // 一个链接
            NSString * urlStr = (NSString*)url;
            SDImageCache * cache = [SDImageCache sharedImageCache];
            UIImage * cacheImg = [cache imageFromCacheForKey:urlStr];
            if (cacheImg) {  completeS(cacheImg); } // 有缓存直接拿缓存
            else{
                if ([urlStr hasPrefix:@"http"]) {
                    [downloader downloadImageWithURL:[NSURL URLWithString:urlStr]  options:SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *  targetURL) {
                    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                        if (image!=nil) { completeS(image); }
                        else{ completeErrorResults(2,@"图片下载失败!"); }
                    }];
                }else{ completeErrorResults(2,@"图片链接错误!"); }
            }
            
        }else if ([url isKindOfClass:[NSArray class]]){ // 链接数组
            
            NSArray * urlAry = (NSArray*)url;
            if (urlAry.count>0) {
                NSMutableArray * mtUrlAry = [NSMutableArray arrayWithArray:urlAry];
                NSMutableArray * imageAry = [NSMutableArray array];
                for (NSInteger i=0; i<urlAry.count; i++) {
                    NSString * urlStr = REPLACENULL(urlAry[i]);
                    if ([urlStr hasPrefix:@"http"]) {
                        [downloader downloadImageWithURL:[NSURL URLWithString:urlStr]  options:SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *  targetURL) {
                        } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                            if (image!=nil) { [imageAry addObject:image]; }
                            else{ [mtUrlAry removeObject:urlStr]; }
                            // 所有图片下载完成
                            if (mtUrlAry.count>0&&imageAry.count>0&&mtUrlAry.count==imageAry.count) {
                                completeS(imageAry);
                                if (urlAry.count!=imageAry.count) {
                                    NSInteger errorCount = urlAry.count-imageAry.count;
                                    completeErrorResults(1,FORMATSTR(@"共%lu张图片，其中%ld张获取失败!",(unsigned long)urlAry.count,(long)errorCount));
                                }
                            }
                        }];
                    }else{
                        [mtUrlAry removeObject:urlStr];
                    }
                }
            }
            
        }else{
            completeErrorResults(2,@"参数格式不正确!");
        }
    } @catch (NSException *exception) {
        completeErrorResults(2,@"图片下载失败!");
    } @finally { }
    
}


+(void)removeUsreCache{
    
    @try {
        if (NSUDTakeData(UserInfo)) { NSUDRemoveData(UserInfo) }               // 用户信息
        if (NSUDTakeData(ShareMuBan)) { NSUDRemoveData(ShareMuBan) }           // 用户分享模板
        if (NSUDTakeData(ApplyUpgrade)) { NSUDRemoveData(ApplyUpgrade) }       // 用户升级信息
        if (NSUDTakeData(PromotionPoster)) { NSUDRemoveData(PromotionPoster) } // 用户分享海报
        if (NSUDTakeData(JD_ConsumerData)) { NSUDRemoveData(JD_ConsumerData) } // 用户京东消费佣金
        if (NSUDTakeData(TB_ConsumerData)) { NSUDRemoveData(TB_ConsumerData) } // 用户淘宝消费佣金
        
        // 清除动态模块缓存
        if (NSUDTakeData(app_dynamic_my_dic)) { NSUDRemoveData(app_dynamic_my_dic) }
        if (NSUDTakeData(app_dynamic_my_time)) { NSUDRemoveData(app_dynamic_my_time) }
        if (NSUDTakeData(app_dynamic_home_dic)) { NSUDRemoveData(app_dynamic_home_dic) }
        if (NSUDTakeData(app_dynamic_home_time)) { NSUDRemoveData(app_dynamic_home_time) }
        if (NSUDTakeData(app_dynamic_tabbar_dic)) { NSUDRemoveData(app_dynamic_tabbar_dic) }
        if (NSUDTakeData(app_dynamic_tabbar_time)) { NSUDRemoveData(app_dynamic_tabbar_time) }
        
        if (NSUDTakeData(HomeOneList)!=nil) { NSUDRemoveData(HomeOneList) }         // 首页商品数据缓存
        if (NSUDTakeData(KeFuZhongXin)!=nil) { NSUDRemoveData(KeFuZhongXin) }       // 首页商品数据缓存
        if (NSUDTakeData(OneDayRanking)!=nil) { NSUDRemoveData(OneDayRanking) }     // 首页商品数据缓存
        if (NSUDTakeData(SevenDayRanking)!=nil) { NSUDRemoveData(SevenDayRanking) } // 首页商品数据缓存
    } @catch (NSException *exception) { } @finally { }
  
}


+(void)compareRequestTime:(NSString*)tkey result:(void(^)(NSInteger resultType))completeResult{
    if (tkey) {
        completeResult(0);
//        if (NSUDTakeData(tkey)) {
//            NSString * thisTime = NowTimeByFormat(@"YYYY-MM-dd") ;
//            NSString * lastTime = (NSString*)NSUDTakeData(tkey);
//            // 对比结果result:   0:相等   -1: thisTime<lastTime  1: thisTime>lastTime
//            NSInteger result = [thisTime compare:lastTime];
//            if (result==0) { completeResult(1); }
//            else{ completeResult(0); }
//        }else{ completeResult(0); }
    }else{ completeResult(0); }
}



@end

