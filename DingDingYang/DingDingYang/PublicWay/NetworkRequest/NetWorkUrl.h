//
//  NetWorkUrl.h
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#ifndef NetWorkUrl_h
#define NetWorkUrl_h

#pragma mark  /******  动态域名 ******/


/**  动态域名(获取动态域名接口)  */
#define url_dynamic_dns            @"getApiUrl.api"

/**  动态域名  **/
//#define dns_dynamic_main           [NetRequest getUrlByType:3]   // 接口域名(主域名)
#define dns_dynamic_upimg          [NetRequest getUrlByType:4]   // 图片上传域名
#define dns_dynamic_goods          [NetRequest getUrlByType:5]   // 商品搜索域名
#define dns_dynamic_nianh          [NetRequest getUrlByType:6]   // 年化模块域名
#define dns_dynamic_jingd          [NetRequest getUrlByType:7]   // 京东域名
#define dns_dynamic_loadimg        [NetRequest getUrlByType:8]   // 图片主域名
#define dns_dynamic_miandan        [NetRequest getUrlByType:9]   // 免单域名
#define dns_dynamic_pindd          [NetRequest getUrlByType:10]  // 拼多多商品主域名
#define dns_dynamic_getmini        [NetRequest getUrlByType:11]  // 获取小程序码
#define dns_dynamic_faquan         [NetRequest getUrlByType:12]  // 一键发圈域名
#define dns_dynamic_weiph          [NetRequest getUrlByType:13]  // 唯品会
#define dns_dynamic_duomai         [NetRequest getUrlByType:14]  // 多麦
#define dns_dynamic_meituan        [NetRequest getUrlByType:15]  // 美团(叮叮羊)

// 测试用的域名
#warning lzy 切换域名
#define dns_dynamic_main           default_domain_main // 叮叮羊测试域名


// 非主域名的用以下方法拼接
#define fm_main(path)          FORMATSTR(@"%@%@",dns_dynamic_main,path)    // 某些带有goods的不使用商品域名的
#define fm_fq(path)            FORMATSTR(@"%@%@",dns_dynamic_faquan,path)  // 一键发圈
#define fm_dm(path)            FORMATSTR(@"%@%@",dns_dynamic_duomai,path)  // 多麦(叮叮羊综合平台)
#define fm_jd(path)            FORMATSTR(@"%@%@",dns_dynamic_jingd,path)   // 京东
#define fm_mt(path)            FORMATSTR(@"%@%@",dns_dynamic_meituan,path) // 美团
#define fm_pdd(path)           FORMATSTR(@"%@%@",dns_dynamic_pindd,path)   // 拼多多
#define fm_wph(path)           FORMATSTR(@"%@%@",dns_dynamic_weiph,path)   // 唯品会

#pragma mark  /******  登录有关  ******/

/**  普通登录  **/
#define url_check_mobile           @"user/checkMobile.api"          // 检查手机号是否注册，未注册则需要填邀请码
#define url_send_code_by_phone     @"user/sendLoginCode.api"        // 发送验证码(登录)
#define url_login                  @"user/login.api"                // 登录
/**  微信登录  **/
#define url_wechat_login           @"user/wxLogin.api"              // 微信登录 (已绑定手机，则返回token，并直接登录，否则进入绑定手机页面)
#define url_bang_ding_wechat       @"user/bindWx.api"               // 绑定微信 (已经登录，在个人中心绑定手机号)
#define url_bang_ding_phone        @"user/wxLogin2.api"             // 微信登录后绑定手机号码后调用的登录接口
#define url_bd_invitecode          @"user/wxLogin4InviteCode.api"   // 微信登录绑定邀请码（蜜赚的）
/**  免密登录  **/
#define url_mian_mi_login          @"user/nocheckLogin.api"         // 免密登录接口
/**  蜜赚加的接口，要用到 t:3 加入代理**/
#define url_send_code_type         @"user/sendCode4Login.api"       // 获取验证码(t: 1登录, 2:绑定支付宝 3:加入代理 )


#pragma mark  /******  启动后的 导航和首页 有关接口  ******/

/**  区分消费者和代理商  **/
#define url_getNavc_bottom         @"getModuleNavCateInfoRoleV1.api"      // 获取底部主导航
#define url_home_category          @"getModuleCategoryInfoRole.api"     // 首页模块化数据
//getModuleNavCateInfoRoleV1.api?

#define url_index_home             @"index.api"                     // 获取首页轮播图，和推荐商品
#define url_new_road               @"user/newUserMsg.api"           // 新手上路
#define url_get_hotWords           @"getHotWords.api"               // 获取搜索热词
#define url_update_version         @"vcheck.api"                    // 1 强制更新 0 不强制

#define url_get_activity           @"activity/index.api"            // 获取广告

#define url_home_news_list         fm_main(@"goods/touTiaoGoods.api")   // 首页的头条功能
//#define url_home_news_list         @"http://120.77.226.151:8061/app/goods/getTouTiaoGoods.api"

#pragma mark  /******  商品有关  ******/


/**  商品搜索  **/
#define url_goods_isShow_coupon    @"goods/couponCheck.api"         // 检查是否显示有券无券的开关
#define url_goods_search           @"goods/search.api"              // 搜索(商品搜索独立接口)

/**  商品列表  **/
#define url_goods_list             @"goods/list.api"                // 商品列表
#define url_goods_list2            @"goods/list2.api"               // 商品列表2
#define url_goods_list4            @"goods/list4.api"               // 商品列表:(限时秒杀)
#define url_goods_jiyouojia        @"goods/jiyouojia.api"           // 极有家


/**  商品详情有关  **/
#define url_goods_pics             @"goods/goodsPics.api"           // 商品主图轮播图
#define url_goods_info             @"goods/getInfo.api"             // 获取商品信息
#define url_goods_detail2          @"goods/detail2.api"             // 商品详情图片
#define url_goods_rmdText          @"goods/detail.api"              // 商品推荐语
#define url_goods_list6            @"goods/list6.api"               // 猜你喜欢

/**  商品分享，购买有关，编辑分享模板等  **/
#define url_goods_share_text       @"goods/shareGoods.api"          // 获取分享文案
#define url_goods_new_text         @"goods/shareGoods1.api"         // 获取新分享文案
#define url_goods_share_poster     @"goods/getShareGoodsPoster.api" // 获取商品二维码海报
#define url_goods_share_posterUrl  @"goods/getShareGoodsEwmText.api"// 获取商品分享二维码文本内容
#define url_shareTxt_stIns         @"user/stIns.api"                // 获取分享模板说明
#define url_save_shareTemp         @"user/saveShareTemp.api"        // 上传模板到服务器
#define url_preview_shareTemp      @"user/sharePreview.api"         // 从服务器获取分享文案预览
#define url_buy_goods              @"goods/buy.api"                 // 购买商品
#define url_video_teach            @"media/videoList.api"           // 视频教程
#define url_shareTxt_temple        @"getShareTemple.api"            // 获取美文模板

/**  通过小程序分享商品  **/
#define url_mini_program_check     @"user/checkMini.api"            // 检测
#define url_mini_program_share     @"getShareGoodsByMin.api"        // 小程序分享

/**  商品、用户 反馈  **/
#define url_get_feedback_quest     @"feedback/getQuests.api"        // 获取商品反馈原因
#define url_upload_feedBack        @"feedback/save.api"             // 提交商品或用户反馈

/**  查券(查淘宝联盟的商品、已废弃、不使用了)  **/
#define url_check_voucher_lbmsg    @"getOrderNotice.api"            // 查券功能中的轮播信息获取
#define url_check_voucher_goods    @"goods/queryRebate.api"         // 查券搜索

/**  淘抢购  **/
#define url_tbqg_time              @"liveIndex.api"                 // 淘抢购场次获取
#define url_tbqg_goodsList         @"goods/getLiveGoods.api"        // 淘抢购商品列表

/**  获取粘贴板判断格式 **/
#define url_clipboard_rules        FORMATSTR(@"%@%@",dns_dynamic_goods,@"goodsText/templateGet")  // 获取粘贴板需要判断的格式
#define url_clipboard_goods_title  FORMATSTR(@"%@%@",dns_dynamic_goods,@"goodsText/parse")        // 解析粘贴板格式

/**  新的官方推荐  **/
#define url_recommmend_list        @"category/get.api"                   // 官方推荐 分类
#define url_recommmend_category    @"goods/listByCategory.api"           // 官方推荐 按分类查询
#define url_recommmend_shareWeb    @"officialShare/saveShareInfo.api"    // 官方推荐 web分享
#define url_recommmend_sharePoster  FORMATSTR(@"%@%@",dns_dynamic_main,@"goods/getShareGoodsPosterList.api") // 官方推荐 海报分享

#define url_recommmend_webContent  @"shareWebContent.api"                // 官方推荐 web分享内容

#pragma mark   /******  个人中心  ******/

/**  个人中心动态化接口 **/
#define url_module_personal        @"getModulePersonalRoleInfo.api" // 获取个人中心动态模块

/**  个人信息获取  **/
#define url_get_myInfo_mokuai      @"getMyModuleInfo.api"           // 获取个人中心模块化数据
#define url_user_info              @"user/info.api"                 // 获取用户信息
#define url_user_money_team        @"user/my.api"                   // 获取消费佣金，代理佣金，团队总人数
#define url_no_readMsg             @"user/countNoReadMsg.api"       // 获取未读消息


/**  个人信息修改和设置  **/
#define url_update_nickName        @"user/updateTb.api"             // 修改或设置昵称 (原淘宝账户接口)
#define url_send_code_bdAlipay     @"user/sendCode.api"             // 绑定支付宝，发送验证码
#define url_update_apliay          @"user/updateAli.api"            // 设置支付宝账户 (设置成功不能再修改)
#define url_upload_headimg         @"user/updateHeadImg.api"        // 上传个人头像
#define url_upload_wechatEwm       @"user/updateWxEwm.api"          // 上传微信二维码
#define url_change_wxInfo          @"user/updateWxHeadImg.api"      // 更改微信授权
#define url_check_code             @"user/chkCode.api"              // 更改微信授权确认验证码

/**  消息  **/
#define url_reward_msg             @"user/rewardMsg.api"            // 奖励消息
#define url_system_msg             @"user/sysMsg.api"               // 系统消息
#define url_msg_details            @"message.api"                   // 消息详情

/**  升级代理，支付  **/
#define url_get_alipay_payInfo     @"alipay/getPayInfo.api"         // 获取支付宝支付秘钥签名
//#define url_get_payDes             @"alipay/payDes.api"             // 支付金额获取(旧版)
// 新的升级页面接口(邀请多少人升级，代理码升级，支付升级)
#define url_mian_fei_up            @"proxy/saveFreeUpgrade.api"     // 免费升级代理
#define url_up_daili_buyCode       @"user/updateUserProxyByPromoCode.api" // 通过代理码升级
#define url_get_Upgrade_Ins        @"user/getUpgradeIns.api"        // 获取升级信息(蜜赚只用来取海报)
#define url_tb_shou_quan           @"proxy/saveTbUpgrade.api"       // 获取淘宝授权链接(原:免费升级代理接口)
#define url_tb_shq_up              @"proxy/saveTbInfo.api"          // 上传获取到的信息


// 蜜赚特有的接口 1: (在升级代理页面绑定手机，并提交申请理由)
#define url_check_role_state       @"apply/proxyApplyStatus.api"     // 检测状态
#define url_upload_upins           @"apply/proxy.api"                // 提交申请理由
// 蜜赚特有的接口 2: (检测是否符合条件，符合条件绑定手机成功，即为升级成功)
#define url_check_role_state2      @"apply/checkActivateUser.api"    // 检测用户是否符合升级条件
#define url_upload_upins2          @"apply/upgradeProxy.api"         // 提交申请理由


/**  提现  **/
#define url_check_wd               @"user/wdConsumeCheckOther.api"  // 检测是否可以提现(除淘宝外其他平台)
#define url_bd_phone               @"user/bindMobile.api"           // 绑定手机号(如果没有手机号的)
#define url_withdraw_type          @"user/getWdBy.api"              // 查询提现方式
#define url_check_wd_consumer      @"user/wdConsumeCheck.api"       // 检查消费佣金是否可以提现
#define url_check_wd_daiLi         @"user/wdCheck.api"              // 检查代理佣金是否可以提现
#define url_withdrawal_daiLi       @"user/wd.api"                   // 代理佣金提现(返款提现)
#define url_withdrawal_xiaoFei     @"user/wdConsume.api"            // 淘宝消费佣金提现
//#define url_withdrawal_fankuan     @"user/wd.api"                   // 返款提现
#define url_withdrawal_laxin       @"user/wdOtherWithdraw.api"      // 拉新提现
#define uel_lx_record              @"lxtb/detail.api"               // 拉新记录


/**  分享好友  **/
#define url_get_share_text         @"getShareText.api"              // 获取分享好友中的文案

/** 获取好友海报新的和旧的两个接口 **/
//#define url_share_posters          @"user/newGetPosters.api"        // 分享好友中的海报获取(新的)
#define url_share_posters          @"user/posters.api"              // 分享好友中的海报获取
#define url_poster_frame           @"user/getPostList.api"         // 获取新的分享海报(本地合成，有位置坐标，和内容)



/**  手机通讯录好友分享  **/
//#define url_upload_address_book    @"user/addFriend.api"            // 上传手机通讯录
#define url_get_phoneFriends       @"user/friends.api"              // 获取通讯录好友和好友身份状态
#define url_get_pfShare_txt        @"user/fmsg.api"                 // 获取通讯录好友分享内容
#define url_pfs_success            @"user/addFriendSend.api"        // 通讯录好友短信分享成功上报服务器

/**  账单订单明细、钱包、消费佣金等  **/
#define url_get_consumer_order     @"user/income.api"               // 获取消费订单
#define url_wallet_details         @"user/walletRecord.api"         // 获取账单明细
#define url_new_consumer_detail    @"user/walletNewDetail.api"      // 获取新版消费佣金
#define url_show_consumer          @"partnerInfo.api"               // 是否显示佣金(各个平台的判断)


/**  团队成员和奖励有关  **/
#define url_my_team                @"user/teamPeople.api"           // 查询我的团队
#define url_team_reward            @"user/teamReward.api"           // 团队收益奖励

/**  其它  **/
#define url_kefu_info              @"user/getKfInfo.api"            // 客服信息
#define url_ranking_list           @"ranking.api"                   // 排行榜



#pragma mark  /******  其它  ******/

#define url_tbnew_list             @"getTbTouTiaoList.api"           // 获取淘宝头条
#define url_getbc_catchWay         @"baiChuan/getBaiChuan.api"       // 获取百川订单抓取方式
#define url_upload_bcOrder         @"baiChuan/saveOrderNo.api"       // 上传百川订单
#define url_getName_buyGoodsid     @"goods/getGoodsNameById.api"     // 根据商品ID查询商品标题

#define url_goods_poster_frame     @"sharePaper/getSettings.api"     // 获取商品海报合成位置的信息
#define url_goods_web_share        @"share/webUrl"                   // 批量分享分享web集合页
#define url_app_shareStatus        @"sharePaper/getApiStatus.api"    // APP开放的功能接口类型获取



#pragma mark  /******  免单(福利商城)  ******/

#define url_free_List              @"free/list.api"                 // 免单商品列表
#define url_free_details           @"free/getGoodsFreeInfo.api"     // 免单详情
#define url_free_saveOrder         @"free/saveOrder.api"            // 我要开团
#define url_free_praise            @"free/praise.api"               // 助力好友
#define url_free_praiseList        @"free/orderPraiseList.api"      // 助力详情列表
#define url_myFree_list            @"free/freeOrderList.api"        // 我的免单列表
#define url_upload_orderNo         @"free/updateOrderNo.api"        // 提交订单号
#define url_free_poster            @"free/getFreePoster.api"        // 获取免单分享海报
#define url_free_setNotice         @"free/setNotice.api"            // 设置开团提醒
#define url_free_take_order        @"free/getBuyUrl.api"            // 下单链接
#define url_free_rules             @"free/getFreeRole.api"          // 福利规则


#pragma mark  /****** 订单闭环 ******/

#define url_online_order_list      FORMATSTR(@"%@%@",default_domain_da_ren,@"user/orderList.api")       // 网购商品列表
#define url_superWallet_info       FORMATSTR(@"%@%@",default_domain_da_ren,url_user_info)               // 超级钱包
#define url_principal_record       FORMATSTR(@"%@%@",default_domain_da_ren,@"user/principalRecord.api") // 查看会员本金收入的记录
#define url_income_record          FORMATSTR(@"%@%@",default_domain_da_ren,@"user/incomeRecord.api")    // 查看会员收益的记录
#define url_income_addRecord       FORMATSTR(@"%@%@",default_domain_da_ren,@"user/incomeAddRecord.api") // 累计收益
#define url_seven_earnings         FORMATSTR(@"%@%@",default_domain_da_ren,@"user/seven.api")           // 七天收益图



#pragma mark  /******  京东、拼多多、唯品会、多麦 接口 ******/

//**  获取商品列表，分类，佣金，提现等  **// 京东
#define url_show_coupons_jd         fm_jd(@"goods/jdCouponCheck")     // 检测京东是否需要显示优惠券开关
#define url_goods_class_jd          fm_jd(@"jd/getCategory")          // 获取商品分类
#define url_goods_list_jd           fm_jd(@"r/find")                  // 京东商品列表
#define url_goods_detail_jd         fm_jd(@"goods/goodsDetail")       // 京东详情
#define url_goods_buy_jd            fm_jd(@"goods/link")              // 京东商品购买
#define url_goods_share_jd          fm_jd(@"goods/share.api")         // 京东商品分享
#define url_goods_order_jd          fm_jd(@"r/jdorder/find")          // 京东订单列表
#define url_jd_consumer             fm_jd(@"r/jdorder/stats")         // 京东的消费佣金


//**  获取商品列表，分类，佣金，提现等  **// 拼多多
#define url_goods_list_pdd          fm_pdd(@"pdd/goods/list")         // 获取商品列表
#define url_goods_class_pdd         fm_pdd(@"pdd/goods/catlist")      // 获取商品分类
#define url_goods_search_pdd        fm_pdd(@"pdd/goods/search")       // 商品搜索接口
#define url_goods_details_pdd       fm_pdd(@"pdd/goods/detail")       // 获取商品详情
#define url_share_buy_pdd           fm_pdd(@"share/pddPromotionMade") // 获取下单链接分享文本
#define url_getInfo_pdd             @"user/getPddInfo.api"            // 获取PID等信息
#define url_shareMini_pdd           @"user/getMinImgByGoodsId.api"    // 获取小程序码
#define url_checkMini_pdd           @"user/getMinImgCheck.api"        // 检查是否有拼多多小程序
#define url_pdd_consumer            @"user/pddWalletDetail.api"       // 拼多多消费佣金
#define url_withdrawal_pdd          @"user/wdPdd.api"                 // 拼多多消费佣金提现
#define url_get_order_pdd           @"user/pddOrder.api"              // 获取消费订单拼多多

//**  获取商品列表，分类，佣金等   **// 唯品会
#define url_goods_list_wph          fm_wph(@"wph/goods/list")        // 获取商品列表
#define url_goods_class_wph         fm_wph(@"wph/goods/catagory")    // 获取商品分类
#define url_goods_order_wph         fm_wph(@"wph/order/find")        // 获取订单明细
#define url_goods_wallet_wph        @"walletOtherDetail.api"         // 获取消费佣金
#define url_goods_withdraw_wph      @"wdOtherWithdraw.api"           // 提现其它(唯品会)

//**  获取商品列表，分类，佣金等   **// 多麦
#define url_dm_bili_get             fm_dm(@"duomai/goods/getPlatRateTitle") // 获取佣金比例
#define url_dm_platform_get         fm_dm(@"duomai/goods/platform")  // 获取多麦平台
#define url_goods_class_dm          fm_dm(@"duomai/goods/category")  // 获取商品分类
#define url_goods_list_dm           fm_dm(@"duomai/goods/list")      // 获取商品列表
#define url_goods_detail_dm         fm_dm(@"duomai/goods/detail")    // 多麦商品详情
#define url_goods_order_dm          fm_dm(@"duomai/order/find")      // 多麦订单明细
#define url_dm_platform_detail      fm_dm(@"duomai/goods/plat")      // 多麦更多平台的详情页
#define url_goods_wallet_dm         @"user/walletOtherDetail.api"    // 获取消费佣金
#define url_dm_bill                 @"user/mybill.api"               // 多麦账单

// 美团(叮叮羊的接口)
#define url_goods_wallet_mt        @"commission/getUserCommission"    // 美团消费佣金获取
#define url_goods_order_mt         fm_mt(@"mt/api/getOrderList")      // 美团订单


#pragma mark  /******  一键发圈的接口名 ******/
#define url_fa_quan_items           fm_fq(@"Items/current")           // 商品圈
#define url_fa_quan_lifes           fm_fq(@"Lifes/current")           // 生活圈



#pragma mark  /******  省见的接口 ******/
#define shj_url_operator_list       @"user/uniteGroup.api"            // 省见的运营商列表





#pragma mark  /****** web url ******/

// 登录页用户协议
#define web_url_user_agreement     FORMATSTR(@"%@userAgreement.api?partnerId=%d",default_domain_main,PID)
// 帮助说明和分享赚钱页面：奖励规则
#define web_url_reward_rule        FORMATSTR(@"%@rewardRule.api?partnerId=%d&token=%@",default_domain_main,PID,TOKEN)



#pragma mark  /****** 达人系、特有的 ******/

#define URLOnlineShoppingUpdateOrderNo  FORMATSTR(@"%@%@",default_domain_da_ren,@"user/fillOrder.api")  // 上传订单号
#define url_Withdraw_Deposit            @"wd/saveCrWithdrawal.api"                                  // 消费者提现
#define url_update_nickNameLogin        FORMATSTR(@"%@%@",default_domain_da_ren,@"user/tblogin.api")    // 淘宝登录
#define url_update_nickNameBDPhoneN     FORMATSTR(@"%@%@",default_domain_da_ren,@"user/bindTbAuth.api") // 淘宝绑定手机号码



//#define url_show_yk_login          @"checkShowNoLoginBtn.api"       // 检查是否需要游客登录
//#define url_getNavc_bottom         @"getModuleNavCateInfo.api"      // 获取底部主导航
//#define url_home_category          @"getModuleCategoryInfo.api"     // 首页模块化数据
//#define url_launch_ad              @"getStartAd.api"                // 获取广告图
//#define url_get_goods_class        @"goods/categoryList.api"        // 获取商品分类
//#define url_upload_IdCard          @"user/userAuth.api"             // 上传身份认证
//#define url_IdCard_Info            @"user/getUserAuthInfo.api"      // 查询身份认证信息
//#define url_check_user_role        @"user/ckUserRole.api"           // 升级代理页，检查用户身份信息，并显示于升级按钮上
//#define url_upgradeIns             @"user/upgradeIns.api"           // 获取升级金额可选项、是否可以使用试用码

#endif /* NetWorkUrl_h */











