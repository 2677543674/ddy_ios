//
//  ThirdPartyLogin.m
//  DingDingYang
//
//  Created by ddy on 2017/12/30.
//  Copyright © 2017年 ddy.All rights reserved.

//  第三方登录处理


#import "ThirdPartyLogin.h"
#import "RongcloudListViewController.h"

@implementation ThirdPartyLogin


#pragma mark ===>>> 微信登录有关

/*  微信返回的用户信息格式
    city = Zhengzhou;
    country = CN;
    headimgurl = "http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLS8zaib4RBuZdETmnZjeh8ibFMekeHvfO2mzY1INiaDgjfVs7Wauh2k95l2GY2DG5LPwMlf59rzeJfg/0";
    language = "zh_CN";
    nickname = "\U6d41\U5e74\U4f3c\U6c34\Ud83d\Ude02";
    openid = opKnuwckZqH0py2NNrrr7C5LYMM8;
    privilege = ( );
    province = Henan;
    sex = 1;
    unionid = o1W820RlAW0UeV4mqrczZf0bJHKQ;
*/



/**
 调用微信登录

 @param successBlock 微信返回的数据，调用成功
 ****** state状态: 1:已在平台绑定,有token了直接登录  2:未在平台绑定需要绑定手机号
 ****** sdic:  state为1时为空    state为2时这里返回的是微信的个人信息
 @param fialBlock 失败原因
*/
+(void)wechatLoginSuccess:(void(^)( NSInteger state , NSDictionary *sdic))successBlock fial:(void(^)(NSString *failStr))fialBlock{
    
    // 使用友盟调用微信授权登录
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        
        if (error) { // 授权失败!
            fialBlock(@"授权失败,请重试!");
        } else {
            UMSocialUserInfoResponse * resp = result ;
            NSDictionary * wechatUserDic = NULLDIC(resp.originalResponse);
            NSUDSaveData(wechatUserDic, Wechat_Info)

            LOG(@"微信登录返回的信息: %@",wechatUserDic)

            NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:wechatUserDic];
            // 不移除会验证不通过
            if ([[dic allKeys] containsObject:@"privilege"]) {
                [dic removeObjectForKey:@"privilege"];
            }

            if (resp.openid.length>0) {
                MBShow(@"正在登录...")
                [NetRequest requestTokenURL:url_wechat_login parameter:dic success:^(NSDictionary *nwdic) {
                    MBHideHUD
                    if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                        // token为0，说明还未绑定手机号，需进入绑定手机号界面
                        if ([REPLACENULL(nwdic[@"token"]) isEqualToString:@"0"]) {
                            successBlock(2,wechatUserDic);
                        }else{
                            
                            NSUDSaveData(REPLACENULL(nwdic[@"token"]) , @"TOKEN")
                            NSUDSaveData(nwdic[@"user"],UserInfo);
                            [RongcloudListViewController registerRongCloud];
                            successBlock(1,@{}) ;
                        }
                    }else{
                        fialBlock(REPLACENULL(nwdic[@"result"]));
                    }
                    
                } failure:^(NSString *failure) {
                    MBHideHUD
                    fialBlock(failure);
                }];
            }else{
                fialBlock(@"微信授权成功，个人信息获取失败!");
            }
        }
    }];
    
}



/**
 已经登录， 在个人中心绑定微信
 
 @param successBlock 绑定成功
 @param fialBlock    绑定失败的原因
 */
+(void)bdWechatSuccess:(void(^)(id result))successBlock fial:(void(^)(NSString *failStr))fialBlock{
    
    // 使用友盟调用微信授权登录
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        
        if (error) { // 授权失败!
            fialBlock(@"授权失败,请重试!");
        } else {
            
            UMSocialUserInfoResponse * resp = result ;
            
            NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
            NSString * nickname = [resp.name stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
            pdic[@"nickName"] = nickname ;
            pdic[@"taobaoAccount"] = nickname ;//用taobaoaccount
            pdic[@"headImg"] = REPLACENULL(resp.iconurl) ;
            pdic[@"openid"] = REPLACENULL(resp.openid) ;
            pdic[@"unionId"] = REPLACENULL(resp.unionId) ;
            
            MBShow(@"正在绑定...")
            [NetRequest requestTokenURL:url_bang_ding_wechat parameter:pdic success:^(NSDictionary *nwdic) {
                MBHideHUD
                if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
                    successBlock(@"绑定成功!");
                }else{
                    fialBlock(REPLACENULL(nwdic[@"result"]));
                }
            } failure:^(NSString *failure) {
                MBHideHUD
                fialBlock(failure);
            }];
            
        }
    }];
}

+(void)changeWechat:(void(^)(id result))successBlock fial:(void(^)(NSString *failStr))fialBlock{
    // 使用友盟调用微信授权登录
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        
        if (error) { // 授权失败!
            fialBlock(@"授权失败,请重试!");
        } else {
            
            UMSocialUserInfoResponse * resp = result ;
            
            NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
            NSString * nickname = [resp.name stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
            pdic[@"nickName"] = nickname ;
            pdic[@"taobaoAccount"] = nickname ;//用taobaoaccount
            pdic[@"headImg"] = REPLACENULL(resp.iconurl) ;
            pdic[@"openid"] = REPLACENULL(resp.openid) ;
            MBShow(@"正在绑定...")
            [NetRequest requestTokenURL:url_change_wxInfo parameter:pdic success:^(NSDictionary *nwdic) {
                MBHideHUD
                if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) {
                    successBlock(@"绑定成功!");
                }else{
                    fialBlock(REPLACENULL(nwdic[@"result"]));
                }
            } failure:^(NSString *failure) {
                MBHideHUD
                fialBlock(failure);
            }];
            
        }
    }];
}


+(void)bangding_JPushTags{
    if (NSUDTakeData(UserInfo)) {
        PersonalModel * infoModel = [[PersonalModel alloc]initWithDic:(NSDictionary*)NSUDTakeData(UserInfo)];
        
        [JPUSHService setAlias:infoModel.inviteCode completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
            if (iResCode==0) { NSLog(@"绑定别名成功!"); }
        } seq:666666];
        
        [JPUSHService setTags:[NSSet setWithArray:@[infoModel.role]] completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
            if (iResCode==0) { NSLog(@"绑定标签成功!"); }
        } seq:666666];
    }
   
}


/**
 取消微信授权
 */
+(void)cancelCancelWechatAuthorization{
    [[UMSocialManager defaultManager] cancelAuthWithPlatform:(UMSocialPlatformType_WechatSession) completion:^(id result, NSError *error) {
        LOG(@"%@",result)
    }];
}



@end
