//
//  ThirdPartyLogin.h
//  DingDingYang
//
//  Created by ddy on 2017/12/30.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXApi.h>

@interface ThirdPartyLogin : NSObject


/**
 调用微信登录

 @param successBlock 微信返回的数据，调用成功
 ****** state状态: 1:已在平台绑定,有token了直接登录  2:未在平台绑定需要绑定手机号
 ****** sdic:  state为1时为空    state为2时这里返回的是微信的个人信息
 @param fialBlock 失败原因
 */
+(void)wechatLoginSuccess:(void(^)( NSInteger state , NSDictionary *sdic))successBlock fial:(void(^)(NSString *failStr))fialBlock;



/**
  已经登录， 在个人中心绑定微信
 
 @param successBlock 绑定成功
 @param fialBlock    绑定失败的原因
 */
+(void)bdWechatSuccess:(void(^)(id result))successBlock fial:(void(^)(NSString *failStr))fialBlock ;



/**
 绑定极光标签
 */
+(void)bangding_JPushTags;

/**
 更改微信授权

 @param successBlock
 @param fialBlock
 */
+(void)changeWechat:(void(^)(id result))successBlock fial:(void(^)(NSString *failStr))fialBlock;

@end
