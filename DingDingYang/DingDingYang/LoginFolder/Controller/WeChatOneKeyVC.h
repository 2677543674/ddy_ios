//
//  WeChatOneKeyVC.h
//  DingDingYang
//
//  Created by ddy on 2018/3/27.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeChatOneKeyVC : DDYViewController
@property(nonatomic,copy)NSString* phone;
@property(nonatomic,copy)NSString* code;
@property(nonatomic,copy)NSString* inviteCode;
@end
