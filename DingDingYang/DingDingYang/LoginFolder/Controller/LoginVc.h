//
//  LoginVc.h
//  DingDingYang
//
//  Created by ddy on 2017/3/1.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVc : DDYViewController


@property(nonatomic,strong) UIViewController * beforeVC ; // 记录当前VC前面的vc
@property(nonatomic,strong) UINavigationController * loginNavc ;  // 登录页设置为导航栏

// 0或nil:退出登录  1:个人中心、 2:下单、 3:分享商品、 4:商品反馈  5:淘抢购 后续可能会继续增加
@property(nonatomic,assign) NSInteger numtype ;


typedef void (^LoginResultBlock) (NSInteger result);
@property (copy, nonatomic) LoginResultBlock loginblock ;  // 回调 1:登录成功 2:登录失败或游客登录

/**
 未登录时，展示登录界面，并接收登录结果回调
 
 @param loginVc 登录页面的上级导航视图(用于loginVC中多级页面返回使用)，可为空，为空时将LoginVC设置为window.rootvc
 @param type 调出登录页的来源类型: ( 0或nil:退出登录  1:个人中心、 2:下单、 3:分享商品、 4:商品反馈 5:淘抢购  6:京东 后续可能会继续增加 )
 @param completionBlock  回调结果 (1:登录成功  2:点击了游客登录)
 */
+(void)showLoginVC:(UIViewController*)loginVc fromType:(NSInteger)type loginBlock:(LoginResultBlock)completionBlock ;


@end
