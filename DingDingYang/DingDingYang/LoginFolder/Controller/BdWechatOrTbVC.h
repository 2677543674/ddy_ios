//
//  BdWechatOrTbVC.h
//  DingDingYang
//
//  Created by ddy on 19/07/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BdWechatOrTbVC : DDYViewController

typedef void (^ResultBlock) (NSInteger result);
@property (copy, nonatomic) ResultBlock resultlb;

@property (strong, nonatomic) NSDictionary * userDic; //接收到的微信的数据
@property (strong, nonatomic) ALBBUser * taoBaoDic; //接收到的淘宝的数据
// result 1：绑定成功  2：返回手机登录
+(void)bindingThirdPartyAccount:(UIViewController*)vc userDic:(NSDictionary*)dic loginBlock:(ResultBlock)completionBlock ;

//淘宝登录
+(void)bindingTaoBaoAccount:(UIViewController*)vc userDic:(ALBBUser*)dic loginBlock:(ResultBlock)completionBlock;
@end
