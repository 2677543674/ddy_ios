//
//  LoginVc.m
//  DingDingYang
//
//  Created by ddy on 2017/3/1.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "LoginVc.h"
#import <WXApi.h>
#import "RongcloudListViewController.h"
#import "WeChatOneKeyVC.h"
#import "CustomLBTxtView.h" // 图片、底部分割线、输入框
#import "LZMyCenterInfoServer.h"
#import "LYAgreementActionView.h"
#import <NSAttributedString+YYText.h>

#define TextViewHi  48*HWB  // 输入框所在的view+间距的高
#define TextFHi     32*HWB  // 输入框View的高

@interface LoginVc ()<UITextFieldDelegate,WXApiDelegate>{
    float textFieldY;//标记输入框位置
}

@property(nonatomic,strong) UIImageView * logoImg;
@property(nonatomic,assign) NSInteger yqmState ; //记录邀请码检测状态  0:未检测 1:成功 2:检测失败

@property(nonatomic,strong) UIColor * otherTitleColor ; //其他按钮颜色, 背景较浅:Black102 , 深色:COLORWHITE


@property(nonatomic,strong) UIView * loginBackView ; // 放所有登录输入框，和登录按钮、和验证码按钮
@property(nonatomic,strong) UIView * allBackView ;   // 只放所有输入框
@property(nonatomic,strong) CustomLBTxtView * mphoneTextF;     // 手机号输入框
@property(nonatomic,strong) CustomLBTxtView * verCodeTextF;    // 验证码输入框
@property(nonatomic,strong) CustomLBTxtView * invitationTextF; // 邀请码输入框

@property(nonatomic,strong) UIButton * codeBtn;      // 获取验证码
@property(nonatomic,strong) UIButton * loginBtn;     // 登录
@property(nonatomic,strong) UIButton * cancelBtn;    // 游客或取消登录
@property(nonatomic,strong) UIButton * weChatLogin;  // 微信登录按钮
@property(nonatomic,strong) UIButton * tbLoginBtn;   // 淘宝登录
@property(nonatomic,assign) BOOL ifNeedYQM;          // 超值熊是否需要邀请码

@property (nonatomic, strong) UIButton *agreementBtn;  //协议同意按钮
@property (nonatomic, strong) UILabel *agreementLabel; //富文本协议

@end

@implementation LoginVc

//调出登录界面
+(void)showLoginVC:(UIViewController*)loginVc fromType:(NSInteger)type loginBlock:(LoginResultBlock)completionBlock{
    LoginVc * login = [[LoginVc alloc]init];
    login.numtype = type ;
    login.loginblock = completionBlock ;
    if (loginVc) { login.beforeVC = loginVc ; }
    
    if (type==0) { // 将windows的跟视图设置为loginvc
        login.loginNavc = [[UINavigationController alloc]initWithRootViewController:login] ;
        login.navigationController.navigationBar.hidden = YES ;
        [UIApplication sharedApplication].keyWindow.rootViewController = login.loginNavc ;
    } else {
        login.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:login animated: type==1 ? NO : YES];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.navigationController!=nil) {
        self.navigationController.navigationBar.hidden = YES ;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.navigationController!=nil) {
        self.navigationController.navigationBar.hidden = NO ;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"登录" ;
    
    self.view.backgroundColor = COLORWHITE ;
    
    _otherTitleColor = COLORWHITE;
    
    [self creatLoginTextView]; // 输入框和登录按钮
    
    [self cancelBtn];
    
    [self configureAgreement];
    
    [self cancelBtn];
    
    KeyboardWillChangeFrameNotification
}

KeyboardWillChangeViewMove(textFieldY)


-(void)creatLoginTextView{
    
    UIImageView * backView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"loginbackground"]];
    backView.contentMode = UIViewContentModeScaleAspectFill;
    backView.frame = self.view.bounds;
    [self.view addSubview:backView];
    
    
    _loginBackView = [[UIView alloc]init];
    _loginBackView.backgroundColor = COLORWHITE;
    _loginBackView.layer.masksToBounds = YES ;
    _loginBackView.layer.cornerRadius = 8*HWB ;
    [self.view addSubview:_loginBackView];
    [_loginBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(30*HWB); make.right.offset(-30*HWB);
        make.height.offset(TextViewHi*2+100*HWB);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    
    _allBackView = [[UIView alloc]init];
    [_loginBackView addSubview:_allBackView];
    [_allBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(25*HWB); make.right.offset(-25*HWB);
        make.bottom.offset(-95*HWB);
        make.top.offset(5*HWB+(TextViewHi-TextFHi));
    }];
    
    _mphoneTextF = [[CustomLBTxtView alloc]init];
    _mphoneTextF.leftImgV.image = [UIImage imageNamed:@"shouji"];
    _mphoneTextF.placeHorder = @"请输入手机号";
    _mphoneTextF.txtField.delegate = self;
    _mphoneTextF.txtField.keyboardType = UIKeyboardTypePhonePad;
    [_allBackView addSubview:_mphoneTextF];
    [_mphoneTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0); make.right.offset(0);
        make.top.offset(0); make.height.offset(TextFHi);
    }];
    
    
    _invitationTextF = [[CustomLBTxtView alloc]init];
    _invitationTextF.leftImgV.image = [UIImage imageNamed:@"yaoqingma"];
    _invitationTextF.placeHorder = @"请输入邀请码/邀请人手机号码";
    _invitationTextF.txtField.delegate = self;
    [_allBackView addSubview:_invitationTextF];
    [_invitationTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(TextFHi);
        make.left.offset(0); make.right.offset(0);
        make.centerY.equalTo(_allBackView.mas_centerY);
    }];
    _invitationTextF.hidden = YES ;
    
    
    _verCodeTextF = [[CustomLBTxtView alloc]init];
    _verCodeTextF.leftImgV.image = [UIImage imageNamed:@"yanzhengma"];
    _verCodeTextF.placeHorder = @"请输入验证码";
    _verCodeTextF.txtField.keyboardType = UIKeyboardTypePhonePad;
    _verCodeTextF.txtField.delegate = self;
    [_allBackView addSubview:_verCodeTextF];
    [_verCodeTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0); make.right.offset(0);
        make.bottom.offset(0);  make.height.offset(TextFHi);
    }];
    
    _codeBtn = [UIButton titLe:@"获取验证码" bgColor:[UIColor clearColor] titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:12*HWB];
    [_codeBtn addTarget:self action:@selector(sendValidation) forControlEvents:UIControlEventTouchUpInside];
    [_verCodeTextF addSubview:_codeBtn];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_verCodeTextF.mas_centerY);
        make.right.offset(0); make.width.offset(70*HWB);
        make.height.offset(TextFHi-2);
    }];
    
    [_mphoneTextF.txtField addTarget:self action:@selector(changePhone) forControlEvents:UIControlEventEditingChanged];
    [_invitationTextF.txtField addTarget:self action:@selector(changeInvitationCode) forControlEvents:UIControlEventEditingChanged];
    [_verCodeTextF.txtField addTarget:self action:@selector(changeCode) forControlEvents:UIControlEventEditingChanged];
    
    _loginBtn = [UIButton titLe:@"登录" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGROUP font:15.0*HWB];
    ADDTARGETBUTTON(_loginBtn, login)
    _loginBtn.layer.cornerRadius = 18*HWB ;
    [_loginBtn addShadowLayer];
    [_loginBackView addSubview:_loginBtn];
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(35*HWB); make.right.offset(-35*HWB);
        make.height.offset(36*HWB);
        make.bottom.offset(-25*HWB);
    }];
    
    
    _logoImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:LOGINLOGO]];
    _logoImg.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:_logoImg];
    [_logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(ScreenW/4);
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.offset(NAVCBAR_HEIGHT-44);
        make.bottom.equalTo(_loginBackView.mas_top);
    }];
    
    // 安装了微信，则显示微信登录
    if ([WXApi isWXAppInstalled]){ [self creatWechatBtn]; }
    
}

//微信和游客登录
-(void)creatWechatBtn{
    
    UILabel *label=[UILabel labText:@"快捷登录" color:_otherTitleColor font:14*HWB];
    [self.view addSubview:label];
    label.textAlignment=NSTextAlignmentCenter;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(120); make.height.offset(30);
        make.bottom.offset(IS_IPHONE_X?-150:-130);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    UIImageView *line1=[[UIImageView alloc]init];
    line1.backgroundColor= _otherTitleColor ;
    [self.view addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(label.mas_left).offset(0);
        make.centerY.equalTo(label.mas_centerY);
        make.height.offset(0.5);  make.width.offset(70);
    }];
    UIImageView *line2=[[UIImageView alloc]init];
    line2.backgroundColor= _otherTitleColor ;
    [self.view addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_right).offset(0);
        make.centerY.equalTo(label.mas_centerY);
        make.height.offset(0.5);  make.width.offset(70);
    }];
    
    
    _weChatLogin = [UIButton buttonWithType:(UIButtonTypeCustom)];
    ADDTARGETBUTTON(_weChatLogin, weChatLoginClick)
    [self.view addSubview:_weChatLogin];
    [_weChatLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(70) ;  make.height.offset(70) ;
        make.bottom.offset(IS_IPHONE_X?-75:-55); make.centerX.equalTo(label.mas_centerX);
    }];
    
    UIImageView * wxlogoImgV = [UIImageView imgName:@"ddy_login_wx"];
    wxlogoImgV.contentMode = UIViewContentModeScaleAspectFit;
    [_weChatLogin addSubview:wxlogoImgV];
    [wxlogoImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_weChatLogin.mas_centerX);
        make.top.offset(0); make.width.height.offset(45);
    }];
    
    UILabel * wxLoginLab = [UILabel labText:@"微信登录" color:_otherTitleColor font:12*HWB];
    [_weChatLogin addSubview:wxLoginLab];
    [wxLoginLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(0); make.centerX.equalTo(_weChatLogin.mas_centerX);
    }];
    
    /*
    _cancelBtn = [UIButton titLe:@"游客登录" bgColor:[UIColor clearColor] titColorN:_otherTitleColor titColorH:_otherTitleColor font:13*HWB];
    [_cancelBtn setImage:[UIImage imageNamed:@"游客"] forState:UIControlStateNormal];
    [_cancelBtn setImageEdgeInsets:UIEdgeInsetsMake(-18,14, 0, 0)];
    [_cancelBtn setTitleEdgeInsets:UIEdgeInsetsMake(36, -36,-3,0)];
    ADDTARGETBUTTON(_cancelBtn, cancelLogin)
    
    [self.view addSubview:_cancelBtn];
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(70) ;  make.height.offset(70) ;
        make.bottom.offset(IS_IPHONE_X?-70:-50); make.centerX.equalTo(label.mas_centerX);
    }];
    
    if ([WXApi isWXAppInstalled]){ // 安装了微信，则显示微信登录
        [_cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(70) ;  make.height.offset(70) ;
            make.bottom.offset(IS_IPHONE_X?-70:-50); make.centerX.equalTo(label.mas_centerX).offset(-50);
        }];
        
        _weChatLogin=[UIButton titLe:@"微信登录" bgColor:[UIColor clearColor] titColorN:_otherTitleColor titColorH:_otherTitleColor font:13*HWB];
        [_weChatLogin setImage:[UIImage imageNamed:@"微信登录"] forState:UIControlStateNormal];
        [_weChatLogin setImageEdgeInsets:UIEdgeInsetsMake(-18, 14, 0, 0)];
        [_weChatLogin setTitleEdgeInsets:UIEdgeInsetsMake(36, -36, -3,0)];
        ADDTARGETBUTTON(_weChatLogin, weChatLoginClick)
        [self.view addSubview:_weChatLogin];
        [_weChatLogin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(70) ;  make.height.offset(70) ;
            make.bottom.offset(IS_IPHONE_X?-70:-50); make.centerX.equalTo(label.mas_centerX);
        }];
        if (THEMECOLOR==2) {
            [_weChatLogin setTitleColor:COLORBLACK forState:UIControlStateNormal];
        }
    }
    
    if (THEMECOLOR==2) {
        [_cancelBtn setTitleColor:COLORBLACK forState:UIControlStateNormal];
    }*/
}

-(UIButton*)cancelBtn{
    if (!_cancelBtn) {
        _cancelBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _cancelBtn.tintColor = Black102;
        UIImage * cancelImg = [[UIImage imageNamed:@"close_login"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
        [_cancelBtn setImage:cancelImg forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(_cancelBtn, cancelLogin)
        [self.view addSubview:_cancelBtn];
        [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(NAVCBAR_HEIGHT-40);
            make.right.offset(-12*HWB);
            make.width.height.offset(40*HWB);
        }];
        [self.view addSubview:_cancelBtn];
    }
    return _cancelBtn;
}

- (void)configureAgreement {
    UIView *centerView = [[UIView alloc] init];
    [self.view addSubview:centerView];
    [centerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(IS_IPHONE_X?-40:-20);
        make.height.offset(30);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    self.agreementBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.agreementBtn setImage:[UIImage imageNamed:@"agreeD"] forState:UIControlStateNormal];
    [self.agreementBtn setImage:[UIImage imageNamed:@"agreeh"] forState:UIControlStateSelected];
    [self.agreementBtn addTarget:self action:@selector(clickAgree) forControlEvents:UIControlEventTouchUpInside];
    
    [centerView addSubview:self.agreementBtn];
    
    [self.agreementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.height.mas_equalTo(30);
    }];
    
    
    NSString *agreementStr = @"登录即代表同意《APP用户协议》和《APP隐私协议》";
    NSString *userAg = @"APP用户协议";
    NSString *secretAg = @"APP隐私协议";
    
    self.agreementLabel = [[UILabel alloc] init];
    self.agreementLabel.userInteractionEnabled = true;
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:agreementStr attributes:@{
        NSForegroundColorAttributeName:_otherTitleColor,
        NSFontAttributeName:[UIFont systemFontOfSize:14],
    }];
    [text addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:[agreementStr rangeOfString:userAg]];
    [text addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:[agreementStr rangeOfString:secretAg]];
    [text yy_setTextHighlightRange:[agreementStr rangeOfString:userAg] color:nil backgroundColor:nil tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        NSLog(@"点击了用户协议");
    }];
    [text yy_setTextHighlightRange:[agreementStr rangeOfString:secretAg] color:nil backgroundColor:nil tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        NSLog(@"点击了隐私协议");
    }];
    
    self.agreementLabel.attributedText = text.copy;
    [centerView addSubview:self.agreementLabel];
    
    [self.agreementLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.agreementBtn.mas_right).offset(6);
        make.centerY.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
}

#pragma mark ===>>> 按钮相应事件
// 发送验证码
-(void)sendValidation{
    
    if (_mphoneTextF.txtField.text.length==0) { SHOWMSG(@"请输入手机号!") return; }
    
    BOOL yn = JUDGESTR(PHONE,_mphoneTextF.txtField.text); //判断手机号
    NSString * numberPhone = _mphoneTextF.txtField.text ;
    if (yn) {
        NSDictionary * dic = @{@"mobile":numberPhone} ;
        
        if (_yqmState==1) {
            
            if (_invitationTextF.hidden==NO) {
                if (_invitationTextF.txtField.text.length==0) { SHOWMSG(@"请输入邀请码!") return ; }
                NSString * invitationCodestr = _invitationTextF.txtField.text ;
                dic = @{@"mobile":numberPhone,@"inviteCode":invitationCodestr};
            }
            [self jcyqmSuccessShouldSend:dic]; //发送验证码
        }else{
            // 防止手机号输完检测失败
            MBShow(@"正在加载...")
            [NetRequest requestType:0 url:url_check_mobile ptdic:@{@"mobile":numberPhone} success:^(id nwdic) {
                _yqmState = 1; //检测成功
                if ([nwdic[@"ifInviteCode"]  intValue]==0) {
                    [self hiddenInvitationViewBy:1 hid:YES];
                    [self jcyqmSuccessShouldSend:dic]; // 无需邀请码，直接发送验证码
                }else if ([nwdic[@"ifInviteCode"]  intValue]==1) {
                    [self hiddenInvitationViewBy:1 hid:NO];
                }
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD  SHOWMSG(failure)  _yqmState = 2;
            }];
            
        }
        
    }else{  SHOWMSG(@"请检查手机号是否正确!") }
}

-(void)jcyqmSuccessShouldSend:(NSDictionary*)dic{
    MBShow(@"正在发送...")
    [NetRequest requestType:0 url:url_send_code_by_phone ptdic:dic success:^(id nwdic) {
        MBHideHUD  [_codeBtn startCountDown];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
}

// 调用登录接口
-(void)login{
    
    ResignFirstResponder
    
    if (_mphoneTextF.txtField.text.length==0) { SHOWMSG(@"请输入手机号!") return; }
    if (_mphoneTextF.txtField.text.length!=11) { SHOWMSG(@"请输入正确的手机号!") return; }
    if (_verCodeTextF.txtField.text.length==0) { SHOWMSG(@"请输入验证码!") return; }
    
    
    NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
    pdic[@"mobile"]   = _mphoneTextF.txtField.text ;
    pdic[@"randCode"] = _verCodeTextF.txtField.text ;
    
    if (_invitationTextF.hidden==NO) { // 需要输入邀请码
        if (_invitationTextF.txtField.text.length==0) {  SHOWMSG(@"请输入邀请码") return; }
        pdic[@"inviteCode"] = _invitationTextF.txtField.text;
    }
    if (_ifNeedYQM==YES) {
        pdic[@"inviteCode"] = @"yiasjr";
    }
    
    MBShow(@"正在登录...")
    [NetRequest requestType:0 url:url_login ptdic:pdic success:^(id nwdic) {
        MBHideHUD
        if (REPLACENULL(nwdic[@"token"]).length==0) {
            SHOWMSG(@"登录token为空，请重试!")  return ;
        }
        NSUDSaveData(_mphoneTextF.txtField.text, NumberPhone);
        NSUDSaveData(REPLACENULL(nwdic[@"token"]) , @"TOKEN")
        NSUDSaveData(NULLDIC(nwdic[@"user"]), UserInfo);
        [self successLogin];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD   SHOWMSG(failure)
    }];
    
}

#pragma mark === >>> 登录成功
-(void)successLogin{
    
    [ThirdPartyLogin bangding_JPushTags];
    
    [[LZMyCenterInfoServer shareInstance] requestCommission];
    
    [RongcloudListViewController registerRongCloud];
    [[NSNotificationCenter defaultCenter] postNotificationName:LoginSuccess object:nil];
    if (NSUDTakeData(app_dynamic_home_dic)) { NSUDRemoveData(app_dynamic_home_dic) }
    if (NSUDTakeData(app_dynamic_home_time)) { NSUDRemoveData(app_dynamic_home_time) }
    if (_numtype==0) {
        [self gotoMain];
    }else{
        PopTo(NO)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _loginblock(1);
        });
    }
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"hasShowAgreement"]) {
        LYAgreementActionView *actionView = [[LYAgreementActionView alloc] init];
        [actionView show];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"hasShowAgreement"];
    }
}


-(void)getUserInfo{
    
    PersonalModel * model = [[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo) ];
    [NetRequest requestType:0 url:url_user_info ptdic:@{@"uniteOperationId":model.uniteOperationId} success:^(id nwdic) {
        NSMutableDictionary*dic= [NSMutableDictionary dictionary];
        [dic addEntriesFromDictionary:NULLDIC(nwdic[@"user"])];
        dic[@"roleName"] = REPLACENULL(nwdic[@"roleName"]);
        dic[@"timeinfo"] = REPLACENULL(nwdic[@"timeinfo"]);
        NSUDSaveData(dic, UserInfo)
        [self gotoMain];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        SHOWMSG(failure)
    }];
    
}

//游客、取消登录
-(void)cancelLogin{
    if (_numtype!=0) {
        if (_numtype==1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:ChangeTabbarSelect object:@{@"index":@"0"}];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self gotoMain];
    }
    //    [self gotoMain];
}

//进入主页
-(void)gotoMain {
    MainTabBarVC * tabbarVC = [[MainTabBarVC alloc]init];
    [UIApplication sharedApplication].keyWindow.rootViewController = tabbarVC ;
}

#pragma mark === >>> 微信登录
-(void)weChatLoginClick{
    
    [ThirdPartyLogin wechatLoginSuccess:^(NSInteger state, NSDictionary *sdic) {
        if (state==1) { [self successLogin]; }
        if (state==2) {
            NSDictionary * wechatUserInfo = NULLDIC(sdic);
            [BdWechatOrTbVC bindingThirdPartyAccount:self userDic:wechatUserInfo loginBlock:^(NSInteger result) {
                // 绑定成功
                if (result==1) {  [self successLogin]; }
            }];
        }
    } fial:^(NSString *failStr) {
        SHOWMSG(failStr)
    }];
    
}

//点击用户协议
-(void)clickAgree{
    WebVC * web = [[WebVC alloc]init];
    web.nameTitle = @"用户协议" ;
    web.urlWeb = web_url_user_agreement ;
    web.hidesBottomBarWhenPushed = YES ;
    [self.navigationController pushViewController:web animated:YES];
}

#pragma mark ===>>> 显示和隐藏邀请码输入框
//检查手机号是否已注册
-(void)checkInvitationCode{
    [NetRequest requestType:0 url:url_check_mobile ptdic:@{@"mobile":_mphoneTextF.txtField.text} success:^(id nwdic) {
        _yqmState = 1; // 检测成功
        if ([nwdic[@"ifInviteCode"]  intValue]==0) {
            [self hiddenInvitationViewBy:1 hid:YES];
        } else if ([nwdic[@"ifInviteCode"]  intValue]==1) {
            [self hiddenInvitationViewBy:1 hid:NO];
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        _yqmState = 2;   SHOWMSG(failure)
    }];
    
}

// 控制是否显示和隐藏邀请码输入框
-(void)hiddenInvitationViewBy:(NSInteger)type hid:(BOOL)hidden{
    if (type==1) {
        if (hidden==NO) {
            _invitationTextF.hidden = NO ;
            [UIView animateWithDuration:0.26 animations:^{
                _invitationTextF.alpha = 1 ;
                [_loginBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(30*HWB); make.right.offset(-30*HWB);
                    make.height.offset(TextViewHi*3+100*HWB);
                    make.centerY.equalTo(self.view.mas_centerY);
                    
                }];
                [_invitationTextF.superview layoutIfNeeded];
                [_loginBackView.superview layoutIfNeeded];
                
            }completion:^(BOOL finished) {
                [_verCodeTextF.txtField resignFirstResponder];
                [_invitationTextF.txtField becomeFirstResponder];
            }];
            
        }else{
            [UIView animateWithDuration:0.26 animations:^{
                _invitationTextF.alpha = 0 ;
                [_loginBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(30*HWB); make.right.offset(-30*HWB);
                    make.height.offset(TextViewHi*2+100*HWB);
                    make.centerY.equalTo(self.view.mas_centerY);
                }];
                [_invitationTextF.superview layoutIfNeeded];
                [_loginBackView.superview layoutIfNeeded];
            }completion:^(BOOL finished) {
                _invitationTextF.hidden = YES ;
                [_invitationTextF.txtField resignFirstResponder];
                [_verCodeTextF.txtField becomeFirstResponder];
            }];
        }
    }
    
    if (type==2) {
        if (_invitationTextF.hidden==NO&&hidden==YES) {
            [UIView animateWithDuration:0.26 animations:^{
                _invitationTextF.alpha = 0 ;
                [_loginBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(30*HWB); make.right.offset(-30*HWB);
                    make.height.offset(TextViewHi*2+100*HWB);
                    make.centerY.equalTo(self.view.mas_centerY);
                }];
                [_invitationTextF.superview layoutIfNeeded];
                [_loginBackView.superview layoutIfNeeded];
                
            }completion:^(BOOL finished) {
                _invitationTextF.hidden = YES ;
            }];
        }
    }
}

#pragma mark ===>>> textfield代理、输入框有关
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    BackToTheOriginSelfView
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    BackToTheOriginSelfView
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect rectTY = [textField convertRect:textField.bounds toView:[UIApplication sharedApplication].keyWindow];
    textFieldY = rectTY.origin.y+30 ;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length==0)  { return YES ; }
    if ([textField isEqual:_mphoneTextF.txtField]||[textField isEqual:_verCodeTextF.txtField]) {
        return JUDGESTR(NUMBERPOINT, string);
    }
    if ([textField isEqual:_invitationTextF.txtField]) {
        return JUDGESTR(AZ_az_09, string);
    }
    return YES ;
}

-(void)keyBoardDown{  }

// 手机输入框文字发生变化、
-(void)changePhone{
    // 去除掉空格键
    _mphoneTextF.txtField.text = [_mphoneTextF.txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_mphoneTextF.txtField.text.length==11) {
        if (JUDGESTR(PHONE,_mphoneTextF.txtField.text)) {
            [_mphoneTextF.txtField resignFirstResponder];
            [_verCodeTextF.txtField becomeFirstResponder];
            [self checkInvitationCode];
        }
    }else{
        [self hiddenInvitationViewBy:2 hid:YES];
    }
}

// 验证码框内容变化
-(void)changeCode{
    // 去空格
    _verCodeTextF.txtField.text = [_verCodeTextF.txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_verCodeTextF.txtField.text.length>=6) {
        [_verCodeTextF.txtField resignFirstResponder];
        _verCodeTextF.txtField.text = [_verCodeTextF.txtField.text substringToIndex:6];
        BackToTheOriginSelfView
    }
}

// 去掉邀请码输入框的空格
-(void)changeInvitationCode{
    _invitationTextF.txtField.text = [_invitationTextF.txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
