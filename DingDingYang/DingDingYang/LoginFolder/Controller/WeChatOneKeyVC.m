//
//  WeChatOneKeyVC.m
//  DingDingYang
//
//  Created by ddy on 2018/3/27.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "WeChatOneKeyVC.h"
#import "RongcloudListViewController.h"
@interface WeChatOneKeyVC ()

@end

@implementation WeChatOneKeyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"微信一键授权";
    self.view.backgroundColor=RGBA(245, 245, 245, 1);
    [self creatView];
    
}


-(void)creatView{
    UIImageView* centerImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"mobile_binding_mutual_icon"]];
    [self.view addSubview:centerImageView];
    [centerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(80*HWB+NAVCBAR_HEIGHT);
        make.centerX.equalTo(self.view);
    }];
    
    UIImageView* leftImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"mobile_binding_app_icon"]];
    [self.view addSubview:leftImageView];
    [leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(centerImageView.mas_left).offset(-24*HWB);
        make.centerY.equalTo(centerImageView.mas_centerY);
    }];
    
    UIImageView* rightImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"mobile_binding_wechat_icon"]];
    [self.view addSubview:rightImageView];
    [rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(centerImageView.mas_right).offset(24*HWB);
        make.centerY.equalTo(centerImageView.mas_centerY);
    }];
    
    UIButton* btn=[[UIButton alloc]init];
    [btn setTitle:@"微信一键授权" forState:UIControlStateNormal];
    ADDTARGETBUTTON(btn, clickBtn);
    btn.titleLabel.font=[UIFont systemFontOfSize:14*HWB];
    btn.layer.cornerRadius=18*HWB;
    btn.clipsToBounds=YES;
    btn.backgroundColor=LOGOCOLOR;
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40*HWB);
        make.right.offset(-40*HWB);
        make.height.offset(36*HWB);
        make.top.equalTo(centerImageView.mas_bottom).offset(180*HWB);
    }];
                                  
}

-(void)clickBtn{
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        if (error) { // 授权失败!
            SHOWMSG(@"授权失败，请重新授权")
        } else {
            UMSocialUserInfoResponse * resp = result ;
            NSDictionary * userDic = resp.originalResponse ;
            NSUDSaveData(userDic, Wechat_Info)
            
            LOG(@"微信登录返回的信息: %@",userDic)
            
            
            
            NSMutableDictionary* pdic=[NSMutableDictionary dictionaryWithDictionary:userDic];
            [pdic removeObjectForKey:@"privilege"];
            pdic[@"mobile"]   = _phone ;
            pdic[@"randCode"] = _code ;
            if (_inviteCode.length>0) {
                pdic[@"inviteCode"]=_inviteCode;
            }

            [NetRequest requestTokenURL:url_wechat_login parameter:pdic success:^(NSDictionary *nwdic) {

                if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                    NSUDSaveData(_phone, NumberPhone);
                    NSUDSaveData(REPLACENULL(nwdic[@"token"]) , @" ")
                    NSUDSaveData(NULLDIC(nwdic[@"user"]), UserInfo);
                    [ThirdPartyLogin bangding_JPushTags];
                    [RongcloudListViewController registerRongCloud];
                    [[NSNotificationCenter defaultCenter] postNotificationName:LoginSuccess object:nil];
                    
                    MainTabBarVC * tabbarVC = [[MainTabBarVC alloc]init];
                    [UIApplication sharedApplication].keyWindow.rootViewController = tabbarVC ;
                }else{
                    SHOWMSG(nwdic[@"result"])
                }
            } failure:^(NSString *failure) {  MBHideHUD SHOWMSG(failure) }];
        }
    }];
}

- (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}



@end
