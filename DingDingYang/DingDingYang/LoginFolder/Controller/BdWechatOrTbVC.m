//
//  BdWechatOrTbVC.m
//  DingDingYang
//
//  Created by ddy on 19/07/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "BdWechatOrTbVC.h"
#import <WXApi.h>
#import "RongcloudListViewController.h"

#define TextFHi  42*HWB // 输入框所在的view的高
#define chzhx_yqm    @"yiasjr"  // 超值熊默认邀请码


@interface BdWechatOrTbVC ()<UITextFieldDelegate,WXApiDelegate>{
    UIImageView*lineImg;
    float textFieldY;//标记输入框位置
}
//@property(nonatomic,strong) UIView * backView ;
@property(nonatomic,strong) UIImageView * logoImg;
@property(nonatomic,strong) UIView * backView1, *backView2 ,*backView3 , * allBackView ;
@property(nonatomic,strong) UITextField * phoneTextF , * codeTextF , * invitationCodeTextF ;
@property(nonatomic,strong) UIButton * codeBtn, * loginBtn, * cancelBtn ;
@property(nonatomic,assign) BOOL ifNeedYQM;   // 超值熊是否需要邀请码

@end

@implementation BdWechatOrTbVC

+(void)bindingThirdPartyAccount:(UIViewController*)vc userDic:(NSDictionary*)dic loginBlock:(ResultBlock)completionBlock {
    BdWechatOrTbVC *bangdingVC = [[BdWechatOrTbVC alloc]init];
    bangdingVC.userDic = dic ;
    bangdingVC.resultlb = completionBlock ;
    if (vc.navigationController) {
        [vc.navigationController pushViewController:bangdingVC animated:YES];
    }else{
        [vc presentViewController:bangdingVC animated:NO completion:nil];
    }
    
}

+(void)bindingTaoBaoAccount:(UIViewController*)vc userDic:(ALBBUser*)dic loginBlock:(ResultBlock)completionBlock {
    BdWechatOrTbVC *bangdingVC = [[BdWechatOrTbVC alloc]init];
    bangdingVC.taoBaoDic = dic ;
    bangdingVC.resultlb = completionBlock ;
    if (vc.navigationController) {
        [vc.navigationController pushViewController:bangdingVC animated:YES];
    }else{
        [vc presentViewController:bangdingVC animated:NO completion:nil];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.navigationController!=nil) {
        self.navigationController.navigationBar.hidden = YES ;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.navigationController!=nil) {
        self.navigationController.navigationBar.hidden = NO ;
    }
}

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORWHITE ;
    self.title = @"绑定手机号" ;
    
    [self loadBackImg];
    [self creatTextField];
    [self loginBtn];
    [self cancelBtn];
    KeyboardWillChangeFrameNotification
}

KeyboardWillChangeViewMove(textFieldY)

//加载背景图
-(void)loadBackImg{
    
    UIImageView * backView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bd_phone"]];
    backView.frame = CGRectMake(0,0, ScreenW, ScreenW/1.7);
    [self.view addSubview:backView];
    
    UILabel * label = [UILabel labText:@"绑定手机号" color:COLORWHITE font:14*HWB];
    [backView addSubview:label];
    label.textAlignment=NSTextAlignmentCenter;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(ScreenW);  make.height.offset(30);
        make.bottom.offset(-10);  make.centerX.equalTo(backView.mas_centerX);
    }];
    
    _logoImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:LOGINLOGO]];
    [backView addSubview:_logoImg];
    [_logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        if ([LOGINLOGO isEqualToString:@"logo"]) { // 个别图片用的是logo
            make.width.offset(ScreenW/3.8);
            make.height.offset(ScreenW/3.8);
        }else{
            make.width.offset(ScreenW/3.5);
            make.height.offset(ScreenW/3.5);
        }
        make.centerY.equalTo(backView.mas_centerY);
        make.centerX.equalTo(backView.mas_centerX);
    }];
    
}

#pragma mark ===>>> 输入框有关

-(void)creatTextField{
    //将手机号、邀请码、验证码输入框的View都放到_allBackView上
    _allBackView = [[UIView alloc]init];
    [self.view addSubview:_allBackView];
    [_allBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40*HWB); make.right.offset(-40*HWB);
        make.height.offset(TextFHi*2+10*HWB);
        make.top.offset( ScreenW/1.7+20*HWB);
    }];
    
    [self loadPhoneNumberTextField];
    [self loadInviteCodeTextField];
    [self loadCodeTextField];
}

//手机号输入
-(void)loadPhoneNumberTextField{
    _backView1 = [[UIView alloc]init];
    _backView1.backgroundColor = RGBA(248.f,226.f,205.f,1) ;
    _backView1.layer.masksToBounds = YES;
    _backView1.layer.cornerRadius = TextFHi/2;
    _backView1.layer.borderWidth = 1;
    _backView1.layer.borderColor = COLORGROUP.CGColor;
    [_allBackView addSubview:_backView1];
    [_backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0); make.right.offset(0);
        make.top.offset(0);  make.height.offset(TextFHi);
    }];
    
    UIImageView*phoneImg=[[UIImageView alloc]init];
    phoneImg.image = [UIImage imageNamed:@"shouji"];
    phoneImg.contentMode = UIViewContentModeScaleAspectFit ;
    [_backView1 addSubview:phoneImg];
    [phoneImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(25);
        make.centerY.equalTo(_backView1.mas_centerY);
        make.height.offset(18); make.width.offset(15);
    }];
    
    _phoneTextF=[UITextField textColor:Black51 PlaceHorder:@"请输入手机号" font:14.0*HWB];
    _phoneTextF.delegate = self;
    _phoneTextF.keyboardType = UIKeyboardTypePhonePad;
    _phoneTextF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_phoneTextF addTarget:self action:@selector(changePhone) forControlEvents:UIControlEventEditingChanged];
    [_backView1 addSubview:_phoneTextF];
    [_phoneTextF mas_makeConstraints:^(MASConstraintMaker*make) {
        make.left.offset(45); make.right.offset(-10);
        make.centerY.equalTo(phoneImg.mas_centerY);
        make.height.offset(36);
    }];
}

// 验证码输入框
-(void)loadCodeTextField{
    
    _backView2 = [[UIView alloc]init];
    _backView2.backgroundColor = RGBA(248.f,226.f,205.f,1) ;
    _backView2.layer.masksToBounds = YES;
    _backView2.layer.cornerRadius = TextFHi/2;
    _backView2.layer.borderWidth = 1;
    _backView2.layer.borderColor = COLORGROUP.CGColor ;
    [_allBackView addSubview:_backView2];
    [_backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0); make.right.offset(0);
        make.height.offset(TextFHi); make.bottom.offset(0);
    }];
    
    UIImageView * codeImg = [[UIImageView alloc]init];
    codeImg.image = [UIImage imageNamed:@"yanzhengma"];
    codeImg.contentMode = UIViewContentModeScaleAspectFit ;
    [_backView2 addSubview:codeImg];
    [codeImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(25); make.height.offset(18); make.width.offset(15);
        make.centerY.equalTo(_backView2.mas_centerY);
    }];
    
    _codeBtn = [UIButton titLe:@"获取验证码" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGRAY font:14.0*HWB];
    [_codeBtn addTarget:self action:@selector(sendValidation) forControlEvents:UIControlEventTouchUpInside];
    [_backView2 addSubview:_codeBtn];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);   make.bottom.offset(0);
        make.right.offset(0); make.width.offset(88*HWB);
    }];
    
    _codeTextF = [UITextField textColor:Black51 PlaceHorder:@"请输入验证码" font:14.0*HWB];
    _codeTextF.delegate = self;
    _codeTextF.keyboardType = UIKeyboardTypePhonePad;
    [_codeTextF addTarget:self action:@selector(changeCode) forControlEvents:UIControlEventEditingChanged];
    [_backView2 addSubview:_codeTextF];
    [_codeTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(45); make.height.offset(36);
        make.centerY.equalTo(codeImg.mas_centerY);
        make.right.equalTo(_codeBtn.mas_left).offset(-5);
    }];
    
    if (THEMECOLOR==2) {
        [_codeBtn setTitleColor:COLORBLACK forState:UIControlStateNormal];
    }
}

//邀请码输入框
-(void)loadInviteCodeTextField{
    
    _backView3=[[UIView alloc]init];
    _backView3.backgroundColor=[UIColor clearColor];
    [_allBackView addSubview:_backView3];
    [_backView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0); make.right.offset(0);
        make.height.offset(TextFHi+20*HWB);
        make.centerY.equalTo(_allBackView.mas_centerY);
    }];
    
    UILabel *tipsLabel=[UILabel labText:@"您还不是受邀用户，请输入您的推荐人邀请码" color:COLORBLACK font:13*HWB];
    tipsLabel.textAlignment = NSTextAlignmentCenter ;
    tipsLabel.adjustsFontSizeToFitWidth = YES ;
    [_backView3 addSubview:tipsLabel];
    [tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0); make.height.offset(20*HWB);
        make.left.offset(20); make.right.offset(-20);
    }];
    
    UIView * icbackv = [[UIView alloc]init];
    icbackv.backgroundColor = RGBA(248.f,226.f,205.f,1) ;
    icbackv.layer.masksToBounds = YES;
    icbackv.layer.cornerRadius = TextFHi/2;
    [_backView3 addSubview:icbackv];
    [icbackv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(0);  make.left.offset(0);
        make.bottom.offset(0); make.height.offset(TextFHi);
    }];
    
    UIImageView * phoneImg = [[UIImageView alloc]init];
    phoneImg.image=[UIImage imageNamed:@"yaoqingma"];
    phoneImg.contentMode = UIViewContentModeScaleAspectFit ;
    [icbackv addSubview:phoneImg];
    [phoneImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(25); make.centerY.equalTo(icbackv.mas_centerY);
        make.height.offset(18); make.width.offset(15);
    }];
    
    _invitationCodeTextF = [UITextField textColor:Black51 PlaceHorder:@"请输入邀请码/邀请人手机号码" font:14.0*HWB];
    _invitationCodeTextF.delegate = self;
    _invitationCodeTextF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_invitationCodeTextF addTarget:self action:@selector(changeInvitationCode) forControlEvents:(UIControlEventEditingChanged)];
    [icbackv addSubview:_invitationCodeTextF];
    [_invitationCodeTextF mas_makeConstraints:^(MASConstraintMaker*make) {
        make.left.offset(45);   make.right.offset(-10);
        make.height.offset(36);  make.centerY.equalTo(phoneImg.mas_centerY);
    }];
    
    _backView3.hidden = YES;
}


//发送验证码
-(void)sendValidation{
    
    if (_phoneTextF.text.length==0) { SHOWMSG(@"请输入手机号!") return; }
    
    if (_backView3.hidden==NO) {
        if (_invitationCodeTextF.text.length==0) {
            SHOWMSG(@"请输入邀请码!") return ;
        }
    }
    
    BOOL yn = JUDGESTR(PHONE,_phoneTextF.text);
    
    if (yn) {
        
        NSDictionary * dic = @{@"mobile":_phoneTextF.text} ;
        if (_backView3.hidden==NO) { dic = @{@"mobile":_phoneTextF.text,@"inviteCode":_invitationCodeTextF.text}; }

        // 超值熊
        if (PID==10038) { if (_ifNeedYQM) { dic = @{@"mobile":_phoneTextF.text,@"inviteCode":chzhx_yqm}; } }
        
        MBShow(@"正在发送...")
        [NetRequest requestType:0 url:url_send_code_by_phone ptdic:dic success:^(id nwdic) {
            MBHideHUD [_codeBtn startCountDown] ;
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            MBHideHUD  SHOWMSG(failure)
        }];
    
    }else{ SHOWMSG(@"请检查手机号是否正确!") }

}

#pragma mark ===>>> 按钮和相应事件

-(UIButton*)loginBtn{
    if (!_loginBtn) {
        _loginBtn=[UIButton titLe:@"登录" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGROUP font:14.0*HWB];
        if (_taoBaoDic!=nil) {
            ADDTARGETBUTTON(_loginBtn, TBlogin)
        }else{
            ADDTARGETBUTTON(_loginBtn, login)
        }
        
        _loginBtn.layer.cornerRadius = (TextFHi-5*HWB)/2;
        [_loginBtn addShadowLayer];
        [self.view addSubview:_loginBtn];
        [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(40*HWB); make.right.offset(-40*HWB);
            make.height.offset(TextFHi-5*HWB);
            make.top.equalTo(_allBackView.mas_bottom).offset(20*HWB);
        }];
    }
    return _loginBtn ;
}
//登录
-(void)login{
    
    if (_phoneTextF.text.length==0) { SHOWMSG(@"请输入手机号!") return; }
    if (_phoneTextF.text.length!=11) { SHOWMSG(@"请输入正确的手机号!") return; }
    if (_codeTextF.text.length==0) { SHOWMSG(@"请输入验证码!") return; }
    
    // 请求的参数
    NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
    // 手机号和验证码
    pdic[@"mobile"]   = _phoneTextF.text ;
    pdic[@"randCode"] = _codeTextF.text ;
    // 微信openid、头像地址、昵称
    NSString * nickname = [REPLACENULL(_userDic[@"nickname"]) stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    pdic[@"nickName"] = nickname ;
    pdic[@"taobaoAccount"] = nickname ;
    pdic[@"openid"]   = REPLACENULL(_userDic[@"openid"]);
    pdic[@"headImg"]  = REPLACENULL(_userDic[@"headimgurl"]);
    pdic[@"city"] = REPLACENULL(_userDic[@"city"]);
    pdic[@"country"] = REPLACENULL(_userDic[@"country"]);
    pdic[@"province"] = REPLACENULL(_userDic[@"province"]);
    pdic[@"sex"] = REPLACENULL(_userDic[@"sex"]);
    pdic[@"unionid"] = REPLACENULL(_userDic[@"unionid"]);

#if ChaoZhiXiong
    if (_ifNeedYQM) { pdic[@"inviteCode"] = @"yiasjr"; }
#else
    if (_backView3.hidden==NO) { //  需要输入邀请码
        if (_invitationCodeTextF.text.length==0) {  SHOWMSG(@"请输入邀请码!") return; }
        else{  pdic[@"inviteCode"] = _invitationCodeTextF.text ; }
    }
#endif
    
    ResignFirstResponder
    
    [NetRequest requestTokenURL:url_bang_ding_phone parameter:pdic success:^(NSDictionary *nwdic) {
        
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            if (REPLACENULL(nwdic[@"token"]).length==0) {
                SHOWMSG(@"登录token为空，请重试!")
                return ;
            }
            
            NSUDSaveData(_phoneTextF.text, @"NUMBERPHONE");
            NSUDSaveData(REPLACENULL(nwdic[@"token"]) , @"TOKEN")
            NSUDSaveData(nwdic[@"user"], @"USERINFO");
            
            [ThirdPartyLogin bangding_JPushTags];
            
            if (_resultlb!=nil) {  PopTo(NO) _resultlb(1); }
            
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {  MBHideHUD    SHOWMSG(failure) }];
    
}

-(void)TBlogin{
    if (_phoneTextF.text.length==0) { SHOWMSG(@"请输入手机号!") return; }
    if (_phoneTextF.text.length!=11) { SHOWMSG(@"请输入正确的手机号!") return; }
    if (_codeTextF.text.length==0) { SHOWMSG(@"请输入验证码!") return; }
    NSString * inviteCode = @"";
    if (_backView3.hidden==NO) {
        if (_invitationCodeTextF.text.length==0) {
            SHOWMSG(@"请输入邀请码")  return;
        }else{
            inviteCode=_invitationCodeTextF.text;
        }
    }
    
    ResignFirstResponder
    
    MBShow(@"正在绑定...")
    
    NSDictionary * pdic=@{@"authCode":_taoBaoDic.openId,
                          @"headImg":_taoBaoDic.avatarUrl,
                          @"nickName":_taoBaoDic.nick,
                          @"mobile":_phoneTextF.text,
                          @"randCode":_codeTextF.text,
                          @"inviteCode":inviteCode};
    
    [NetRequest requestType:0 url:url_update_nickNameBDPhoneN ptdic:pdic success:^(id nwdic) {
        MBHideHUD
        if (REPLACENULL(nwdic[@"token"]).length==0) {
            SHOWMSG(@"登录token为空，请重试!")  return ;
        }
        NSUDSaveData(_phoneTextF.text, @"NUMBERPHONE");
        NSUDSaveData(REPLACENULL(nwdic[@"token"]) , @"TOKEN")
        NSUDSaveData(nwdic[@"user"], @"USERINFO");
        
        [ThirdPartyLogin bangding_JPushTags];
        
        if (_resultlb!=nil) {  PopTo(NO) _resultlb(1); }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
    
}


#pragma mark ===>>> 取消登录
-(UIButton*)cancelBtn{
    if (!_cancelBtn) {
        _cancelBtn = [UIButton titLe:@"返回手机登录" bgColor:COLORWHITE titColorN:LOGOCOLOR titColorH:LOGOCOLOR font:14*HWB];
        _cancelBtn.layer.cornerRadius = 22 ;
        ADDTARGETBUTTON(_cancelBtn, cancelLogin)
        [self.view addSubview:_cancelBtn];
        [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(100) ;   make.right.offset(-100) ;
            make.top.equalTo(_loginBtn.mas_bottom).offset(20*HWB);
            make.height.offset(40) ;
        }];
    }
    return _cancelBtn ;
}

-(void)cancelLogin{
    if (self.navigationController) {
        PopTo(YES)
    }else{
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}


#pragma mark ===>>> 显示和隐藏邀请码输入框
//检查手机号是否已注册
-(void)checkInvitationCode{
    
    [NetRequest requestType:0 url:url_check_mobile ptdic:@{@"mobile":_phoneTextF.text} success:^(id nwdic) {
#if ChaoZhiXiong
        [self hiddenInvitationViewBy:1 hid:YES];
        if ([nwdic[@"ifInviteCode"] intValue]==1) { _ifNeedYQM = YES; }
#else
        if ([nwdic[@"ifInviteCode"]  intValue]==0) {
            [self hiddenInvitationViewBy:1 hid:YES];
        }else if ([nwdic[@"ifInviteCode"]  intValue]==1) {
            [self hiddenInvitationViewBy:1 hid:NO];
        }
#endif
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        
    }];

}

-(void)hiddenInvitationViewBy:(NSInteger)type hid:(BOOL)hidden{
    if (type==1) {
        if (hidden==NO) {
            _backView3.hidden = NO ;
            [UIView animateWithDuration:0.26 animations:^{
                _backView3.alpha=1;
                [_allBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(40*HWB); make.right.offset(-40*HWB);
                    make.height.offset(TextFHi*3+40*HWB);
                    make.top.offset( ScreenW/1.7+20*HWB);
                }];
                
                [_allBackView.superview layoutIfNeeded];
            }completion:^(BOOL finished) {
                [_codeTextF resignFirstResponder];
                [_invitationCodeTextF becomeFirstResponder];
            }];
            
        }else{
            [UIView animateWithDuration:0.26 animations:^{
                _backView3.alpha = 0 ;
                [_allBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(40*HWB); make.right.offset(-40*HWB);
                    make.height.offset(TextFHi*2+10*HWB);
                    make.top.offset( ScreenW/1.7+20*HWB);
                }];
                [_allBackView.superview layoutIfNeeded];
            }completion:^(BOOL finished) {
                _backView3.hidden = YES ;
                [_invitationCodeTextF resignFirstResponder];
                [_codeTextF becomeFirstResponder];
            }];
        }
    }
    
    if (type==2) {
        if (_backView3.hidden==NO&&hidden==YES) {
            [UIView animateWithDuration:0.26 animations:^{
                _backView3.alpha = 0 ;
                [_allBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(40*HWB); make.right.offset(-40*HWB);
                    make.height.offset(TextFHi*2+10*HWB);
                    make.top.offset( ScreenW/1.7+20*HWB);
                }];
                [_allBackView.superview layoutIfNeeded];
            }completion:^(BOOL finished) {
                _backView3.hidden = YES ;
            }];
        }
    }
}

#pragma mark ===>>> textfield代理

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    BackToTheOriginSelfView
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    BackToTheOriginSelfView
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length==0)  { return YES ; }
    if ([textField isEqual:_phoneTextF]||[textField isEqual:_codeTextF]) {
        return JUDGESTR(NUMBERPOINT, string);
    }
    if ([textField isEqual:_invitationCodeTextF]) {
        return JUDGESTR(AZ_az_09, string);
    }
    return YES ;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect rectTY=[textField convertRect:textField.bounds toView:[UIApplication sharedApplication].keyWindow];
    textFieldY=rectTY.origin.y+30;
}
-(void)keyBoardDown{}


// 手机输入框文字发生变化、
-(void)changePhone{
    // 去除掉空格键
    _phoneTextF.text = [_phoneTextF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_phoneTextF.text.length==11) {
        if (JUDGESTR(PHONE,_phoneTextF.text)) {
            [_phoneTextF resignFirstResponder];
            [_codeTextF becomeFirstResponder];
            [self checkInvitationCode];
        }
    }else{
        [self hiddenInvitationViewBy:2 hid:YES];
    }
}

// 验证码框内容变化
-(void)changeCode{
    // 去空格
    _codeTextF.text = [_codeTextF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_codeTextF.text.length>=6) {
        [_codeTextF resignFirstResponder];
        _codeTextF.text = [_codeTextF.text substringToIndex:6];
        BackToTheOriginSelfView
    }
}

// 去掉邀请码输入框的空格
-(void)changeInvitationCode{
    _invitationCodeTextF.text = [_invitationCodeTextF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

