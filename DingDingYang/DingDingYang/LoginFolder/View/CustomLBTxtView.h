//
//  CustomLBTxtView.h
//  DingDingYang
//
//  Created by ddy on 2018/1/30.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLBTxtView : UIView
@property(nonatomic,strong) UIImageView * leftImgV;
@property(nonatomic,strong) UIImageView * lineImgV;
@property(nonatomic,strong) UITextField * txtField;

@property(nonatomic,copy) NSString * leftImg;
@property(nonatomic,copy) NSString * placeHorder;

@end
