//
//  CustomLBTxtView.m
//  DingDingYang
//
//  Created by ddy on 2018/1/30.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "CustomLBTxtView.h"

@implementation CustomLBTxtView

-(void)setLeftImg:(NSString *)leftImg{
    _leftImg = leftImg ;
    _leftImgV.image = [[UIImage imageNamed:_leftImg] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

-(void)setPlaceHorder:(NSString *)placeHorder{
    _placeHorder = placeHorder ;
    _txtField.placeholder = _placeHorder ;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        _leftImgV = [[UIImageView alloc]init];
        _leftImgV.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_leftImgV];
        [_leftImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(2*HWB); make.centerY.equalTo(self.mas_centerY);
            make.width.offset(16*HWB); make.height.offset(16*HWB);
        }];
        
        _lineImgV = [[UIImageView alloc]init];
        _lineImgV.backgroundColor = Black204 ;
        [self addSubview:_lineImgV];
        [_lineImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-2*HWB); make.left.offset(22*HWB);
            make.bottom.offset(0); make.height.offset(0.5);
        }];
        
        _txtField = [UITextField textColor:Black51 PlaceHorder:@"请输入相关内容" font:13*HWB];
//        _txtField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:_txtField];
        [_txtField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(20*HWB);  make.centerY.equalTo(self.mas_centerY);
            make.height.offset(30*HWB); make.right.offset(-5*HWB);
        }];
    }
    return self;
}

@end
