//
//  YourTradeWebViewController.h
//  DingDingYang
//
//  Created by ddy on 2017/11/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface TradeWebVC : AlibcWebViewController


/**
 打开百川页面 ( 内呼淘宝web页面，不跳出app )
 
 @param shopOrGoodsId 商品ID或者店铺ID
 @param urlStr url链接
 @param type 类型：
   1:打开店铺(id)  2:商品详情页(id)  3:添加商品到购物车(id)  4:打开购物车(无需参数)
   5:打开任意的淘宝天猫链接(url)   6:打开淘宝手机版订单页面(不抓数据,只做展示)
   7:打开淘宝二次授权界面    100:访问的是抓单
 */
+(void)showCustomBaiChuan:(NSString*)shopOrGoodsId url:(NSString*)urlStr pagetype:(NSInteger)type ;


@property(nonatomic,assign) NSInteger   showOrderType; // 记录当前访问的类型 状态同步(上面打开百川页面的type)


#pragma mark ===>>> 抓单有关

typedef void (^GrabOrderState) (NSInteger result);
@property (nonatomic,copy) GrabOrderState stateBlock ;

typedef void (^UploadOrderResult) (id result);
@property (nonatomic,copy) UploadOrderResult uploadResult ;  //用于服务器需要匹配订单或返回匹配订单而预留的回调

/**
 抓取订单

 @param stateBlock  回调状态  1：成功抓取并上传, 2:服务器返回不抓取, 3:订单web页面加载失败, 4:web加载成功但未解析到订单(上传订单接口不再执行), 5:web解析成功但上传失败或不需要本地解析上传文件失败  6:获取是否抓单和抓单url的接口失败
 @param result      上传成功返回的匹配结果（目前为空，防止服务器有信息回传）
 */
+(void)grabTheOrderState:(GrabOrderState)stateBlock upload:(UploadOrderResult)result ;


// 抓单有关，服务器返回的字段
@property(nonatomic,assign) NSInteger   getOrderType ;  // 是否抓单 ( 1:抓单 2:不抓单 )
@property(nonatomic,assign) NSInteger   parsingOrder ;  // 解析方式 ( 1:服务器解析, 2:前端解析 )
@property(nonatomic,copy)   NSString  * openOrderUrl ;  // 订单页面的链接
@property(nonatomic,copy)   NSString  * regular ;       // 正则公式


#pragma mark ===>>> 打开淘宝二次授权

typedef void (^AuthorizationState)(NSInteger shqState,NSString * errorMsg);
@property(nonatomic,copy) AuthorizationState shqStateBlock;

+(void)tbSecondaryAuthorization:(NSString*)pagrUrl jsCode:(NSString*)jsCode hdUrl:(NSString*)hdUrl shqState:(AuthorizationState)shqStateBlock;
@property(nonatomic,copy) NSString * clickJsCode, *huiDiaoUrl;

@end





