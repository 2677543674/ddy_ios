//
//  DdyShopWebVC.h
//  DingDingYang
//
//  Created by ddy on 2018/8/29.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface DdyShopWebVC : DDYViewController

@property(nonatomic,copy) NSString * webTitle;
@property(nonatomic,copy) NSString * urlWeb ;    // url

@property(nonatomic,assign) NSInteger platform;  // 0:默认  1:美团
@property(nonatomic,strong) NSDictionary * resultDic;

@end


// 登录提示弹框
@interface DdyShopView1 : DDYViewController
@property(nonatomic,copy) NSString * platform;
@property(nonatomic,strong) UIView * backView;
@end


@interface DdyShopView2 : UIView
@property(nonatomic,copy) NSString * aid;
@property(nonatomic,strong) UIView * backView;
@property(nonatomic,strong) UIWebView * webView;
@end





