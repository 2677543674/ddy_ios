//
//  WebVC.h
//  DingDingYang
//
//  Created by ddy on 2017/3/7.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebVC : DDYViewController

@property(nonatomic,copy)NSString * urlWeb ;    //url
@property(nonatomic,copy)NSString * nameTitle ; //标题
//@property(nonatomic,copy)NSString * nameTitle2 ; //标题
@property(nonatomic,assign)BOOL ifTab;
@property(nonatomic,assign) NSInteger  showType; // 0:不显示分享 1:显示分享 2:不显示分享当前页面为拼多多页面或淘宝页面
+(void)pushWebViewUrlOrStr:(NSString*)urlWeb title:(NSString*)title isSHowType:(NSInteger)showType;
@end
