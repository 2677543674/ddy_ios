//
//  DdyShopWebVC.m
//  DingDingYang
//
//  Created by ddy on 2018/8/29.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "DdyShopWebVC.h"

@interface DdyShopWebVC ()<WKUIDelegate,WKNavigationDelegate,UIScrollViewDelegate>

@property(nonatomic,strong) UIImageView * topImgView;
@property(nonatomic,strong) UIImageView * botImgView;
@property(nonatomic,strong) UILabel * webTitleLab;
@property(nonatomic,strong) UIButton * closeBtn,*backBtn;
@property(nonatomic,strong) UIButton * reloadBtn;
@property(nonatomic,strong) UIButton * showBtn,*showBtn2;

@property(nonatomic,strong) UIProgressView * progressView;
@property(nonatomic,strong) WKWebView * wkWebView;
@property(nonatomic,strong) UIButton * reloadWebBtn;  // 刷新按钮

@property(nonatomic,copy) NSString * aid;  // 活动ID
@property(nonatomic,copy) NSString * rate; // 最高返佣比例

@property(nonatomic,strong) DdyShopView2 * shopView2;

@property(nonatomic,assign) float scrollStopY;


@property(nonatomic,copy) NSString * mtCommissionUrl; // 美团佣金url

/** 保存当前请求的URL */
@property(nonatomic, strong) NSURL *currentUrl;

@end

@implementation DdyShopWebVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _scrollStopY = 1;
    
    if (_platform==1) { // 美团
        self.title = REPLACENULL(_resultDic[@"title"]);
        _webTitle = REPLACENULL(_resultDic[@"title"]);
        // 可以打开美团，则页面先展示返佣流程
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"imeituan://"]]){
//            NSString * mtUrl = REPLACENULL(_resultDic[@"urlGo"]);
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mtUrl]];
            _urlWeb = REPLACENULL(_resultDic[@"urlDetail"]);
        }else{
            _urlWeb = REPLACENULL(_resultDic[@"urlWebUrl"]);
        }
        _mtCommissionUrl = REPLACENULL(_resultDic[@"urlDetail"]);
        _rate = REPLACENULL(_resultDic[@"rateStr"]);
        [self creatWkWebView];
    }else{
        
        if (_urlWeb.length==0||[_urlWeb hasPrefix:@"http"]==NO) {
            PopTo(YES) SHOWMSG(@"平台链接错误!") return;
        }
        
        if (_webTitle==nil||_webTitle.length==0) { _webTitle = @"综合平台"; }
        
        @try {
            NSArray * ary = [_urlWeb componentsSeparatedByString:@"&"];
            for (NSString * str in ary) {
                if ([str containsString:@"aid"]) {
                    NSLog(@"截取到的活动ID: %@",str);
                    NSArray * spary = [str componentsSeparatedByString:@"="];
                    if (spary.count>=2) { _aid = spary[1]; } else { _aid = @""; }
                }
                if ([str containsString:@"rate="]) {
                    NSLog(@"截取到的佣金比例: %@",str);
                    NSArray * spary = [str componentsSeparatedByString:@"="];
                    if (spary.count>=2) { _rate = spary[1]; } else { _rate = @""; }
                }
                if ([str containsString:@"title"]) {
                    NSArray * spary = [str componentsSeparatedByString:@"="];
                    if (spary.count>=2) { _webTitle = [spary[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]; }
                }
            }
            
        } @catch (NSException *exception) {
            _aid = @""; _rate = @"";
        } @finally {
            
            self.title = _webTitle;
            
            if (_aid.length>0) {
                
                //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //                MBShow(@"正在加载...")
                [NetRequest requestType:0 url:url_dm_bili_get ptdic:@{@"adId":_aid} success:^(id nwdic) {
                    MBHideHUD
                    NSArray * ary = NULLARY(nwdic[@"list"]);
                    if (ary.count>0) {
                        NSDictionary * dic = NULLDIC(ary[0]);
                        _rate = REPLACENULL(dic[@"rate"]);
                        _urlWeb = REPLACENULL(dic[@"trackUrl"]);
                        [self creatWkWebView];
                    }else{ PopTo(YES) SHOWMSG(@"平台信息获取失败!") return; }
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    MBHideHUD SHOWMSG(failure) PopTo(YES)  return;
                }];
                //            });
                
            }else{ PopTo(YES) SHOWMSG(@"平台ID获取失败!") return; }
            
        }
    }
        

}

-(void)creatWkWebView{
    [self addTopAndBotView];
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    WKPreferences * preference = [[WKPreferences alloc]init];
    config.mediaPlaybackRequiresUserAction = YES;
    config.mediaPlaybackAllowsAirPlay = YES;
    config.allowsInlineMediaPlayback = YES ;
    // 默认认为YES
    config.preferences.javaScriptEnabled = YES;
    // 在iOS上默认为NO，表示不能自动通过窗口打开
    config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
    config.preferences = preference;
    // web内容处理池
    config.processPool = [[WKProcessPool alloc] init];
    
    /*
    // 将所有cookie以document.cookie = 'key=value';形式进行拼接
    NSString * cookieValue = @"document.cookie = 'fromapp=ios';document.cookie = 'channel=appstore';";
    // 加cookie给h5识别，表明在ios端打开该地址
    WKUserContentController * userContentController = WKUserContentController.new;
    WKUserScript * cookieScript = [[WKUserScript alloc]
                                   initWithSource: cookieValue
                                   injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
    [userContentController addUserScript:cookieScript];
    config.userContentController = userContentController;
    */

    _wkWebView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    _wkWebView.UIDelegate = self;  _wkWebView.navigationDelegate = self;
    [self.view addSubview:_wkWebView];
    [_wkWebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topImgView.mas_bottom).offset(0);
        make.bottom.equalTo(_botImgView.mas_top).offset(0);
        make.left.offset(0); make.right.offset(0);
    }];
    
    // 进度条初始化
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0,ScreenW,1.5)];
    _progressView.progressTintColor = RGBA(76, 217, 99, 1.0) ;
    _progressView.trackTintColor = [UIColor clearColor] ;
    _progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    _progressView.progress = 0 ;
    _progressView.hidden = YES ;
    [self.view addSubview:_progressView];
    
    [_wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
//    _wkWebView.scrollView.delegate = self;
    _wkWebView.scrollView.bounces = NO;
//    [self loadRequestWithUrlString];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_urlWeb]];
    [_wkWebView loadRequest:request];

}

- (void)loadRequestWithUrlString{
    
    // 在此处获取返回的cookie
    NSMutableDictionary *cookieDic = [NSMutableDictionary dictionary];
    
    NSMutableString * cookieValue = [NSMutableString stringWithFormat:@""];
    NSHTTPCookieStorage * cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in [cookieJar cookies]) {
        [cookieDic setObject:cookie.value forKey:cookie.name];
    }
    
    // cookie重复，先放到字典进行去重，再进行拼接
    for (NSString *key in cookieDic) {
        NSString *appendString = [NSString stringWithFormat:@"%@=%@;", key, [cookieDic valueForKey:key]];
        [cookieValue appendString:appendString];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_urlWeb]];
    [request addValue:cookieValue forHTTPHeaderField:@"Cookie"];
    LOG(@"%@",request.allHTTPHeaderFields);
    
    [_wkWebView loadRequest:request];

}

-(void)addTopAndBotView{
    _topImgView = [[UIImageView alloc]init];
    _topImgView.userInteractionEnabled = YES;
    _topImgView.image = [UIImage imageNamed:@"ddy_shop_web_top"];
    [self.view addSubview:_topImgView];
    [_topImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(NAVCBAR_HEIGHT);
    }];
    
    _webTitleLab = [UILabel labText:@"商城" color:COLORWHITE font:14*HWB];
    _webTitleLab.textAlignment = NSTextAlignmentCenter;
    [_topImgView addSubview:_webTitleLab];
    [_webTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-10); make.height.offset(24);
        make.centerX.equalTo(_topImgView.mas_centerX);
        make.width.offset(180*HWB);
    }];
    
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _backBtn.tintColor = COLORWHITE;
    [_backBtn setImage:[[UIImage imageNamed:@"goback"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)] forState:UIControlStateNormal];
    [_backBtn setImageEdgeInsets:UIEdgeInsetsMake(13.5, 13, 13.5, 17)];
    _backBtn.frame = CGRectMake(0,NAVCBAR_HEIGHT-44, 40, 44);
    ADDTARGETBUTTON(_backBtn, clickBack)
    [_topImgView addSubview:_backBtn];

    _closeBtn = [UIButton titLe:@"关闭" bgColor:COLORCLEAR titColorN:COLORWHITE font:12*HWB];
    [_topImgView addSubview:_closeBtn];  _closeBtn.alpha = 0;
    ADDTARGETBUTTON(_closeBtn, clickCloseWebView)
    [_closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backBtn.mas_right).offset(0);
        make.centerY.equalTo(_backBtn.mas_centerY);
        make.height.offset(30); make.width.offset(30*HWB);
    }];
    
    _reloadBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [_reloadBtn setImage:[UIImage imageNamed:@"ddy_shop_web_reload"] forState:(UIControlStateNormal)];
    [_reloadBtn setImageEdgeInsets:UIEdgeInsetsMake(12.5, 12, 12.5, 12)];
    _reloadBtn.frame = CGRectMake(ScreenW-50,NAVCBAR_HEIGHT-44, 44, 44);
    ADDTARGETBUTTON(_reloadBtn, clickReload);
    [_topImgView addSubview:_reloadBtn];
    
    
    _botImgView = [[UIImageView alloc]init];
    _botImgView.userInteractionEnabled = YES;
    _botImgView.image = [UIImage imageNamed:@"ddy_shop_web_bot"];
    [self.view addSubview:_botImgView];
    [_botImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.offset(0);
        make.height.offset(IS_IPHONE_X?55*HWB:40*HWB);
    }];
    

    _rate = [_rate stringByReplacingOccurrencesOfString:@"%2525" withString:@"%"];
    _rate = [_rate stringByReplacingOccurrencesOfString:@"%25" withString:@"%"];
    NSString * fanBiLi = FORMATSTR(@"最高返%@ ",_rate);
    _showBtn = [UIButton titLe:fanBiLi bgColor:COLORCLEAR titColorN:COLORWHITE font:13*HWB];
    _showBtn.selected = NO;
    [_botImgView addSubview:_showBtn];
    [_showBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5*HWB); make.height.offset(30*HWB);
        make.centerX.equalTo(_botImgView.mas_centerX).offset(-10*HWB);
    }];
    
    _showBtn2 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [_showBtn2 setImage:[UIImage imageNamed:@"ddy_shop_web_up"] forState:(UIControlStateNormal)];
    [_showBtn2 setImage:[UIImage imageNamed:@"ddy_shop_web_down"] forState:(UIControlStateSelected)];
    [_botImgView addSubview:_showBtn2];
    [_showBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_showBtn.mas_right).offset(0);
        make.centerY.equalTo(_showBtn.mas_centerY);
        make.width.offset(20*HWB); make.height.offset(30*HWB);
    }];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"imeituan://"]]&&_platform==1){
        [_showBtn setTitle:@"立即前往美团" forState:(UIControlStateNormal)];
        [_showBtn2 setImage:[UIImage imageNamed:@"ddy_shop_web_next"] forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(_showBtn, clickOpenMeiTuan)
        ADDTARGETBUTTON(_showBtn2, clickOpenMeiTuan)
    }else{
        [_showBtn setTitle:@"关闭" forState:(UIControlStateSelected)];
        ADDTARGETBUTTON(_showBtn, clickShowOrFolding)
        ADDTARGETBUTTON(_showBtn2, clickShowOrFolding)
    }
    
    _webTitleLab.text = _webTitle;
    
}

-(void)clickBack{
    if (_wkWebView.canGoBack==YES) {
        [_wkWebView goBack];
    }else{
        if (self.navigationController==nil) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }else{ PopTo(YES) }
    }
}
-(void)clickCloseWebView{
    if (self.navigationController==nil) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }else{ PopTo(YES) }
}
-(void)clickReload{ [_wkWebView reload]; }


// 分佣规则的展开或折叠
-(void)clickShowOrFolding{
    _botImgView.userInteractionEnabled = NO;
    [self shopView2];
    if(_showBtn.selected==YES){ // 需要隐藏
        _showBtn.selected = NO;
        _showBtn2.selected = NO;
        [UIView animateWithDuration:0.26 animations:^{
            _shopView2.alpha = 0;
        } completion:^(BOOL finished) {
            _shopView2.hidden = YES;
            _botImgView.userInteractionEnabled = YES;
        }];
    }else{ // 显示
        _showBtn.selected = YES;
        _showBtn2.selected = YES;
        _shopView2.hidden = NO;
        [UIView animateWithDuration:0.26 animations:^{
            _shopView2.alpha = 1;
        } completion:^(BOOL finished) {
            _botImgView.userInteractionEnabled = YES;
        }];
    }
}

#pragma mark ===>>> 打开美团
// 打开美团
-(void)clickOpenMeiTuan{
    NSURL * mtUrl =  [NSURL URLWithString:REPLACENULL(_resultDic[@"urlGo"])];
    if ([[UIApplication sharedApplication]canOpenURL:mtUrl]) {
        [[UIApplication sharedApplication] openURL:mtUrl];
    }else{
//         SHOWMSG(@"无法打开美团app，已为您打开网页版")
        _urlWeb = _resultDic[@"urlWebUrl"];
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_urlWeb]];
        [_wkWebView loadRequest:request];
    }
}


-(DdyShopView2*)shopView2{
    if(!_shopView2){
        _shopView2 = [[DdyShopView2 alloc]init];
        [self.view addSubview:_shopView2];
        [_shopView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_botImgView.mas_top).offset(0);
            make.top.left.right.offset(0);
        }];
    }
    if (_platform==1) {
        [_shopView2.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_mtCommissionUrl]]];
    }else{
        NSString * url = FORMATSTR(@"http://api.times2018.com:8080/dm/h5/activity_detail/%@",_aid);
        [_shopView2.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    }
    [self.view bringSubviewToFront:_shopView2];
    return _shopView2;
}


// 监控web加载进度
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) { // 网页加载进度
        [_progressView setProgress:self.wkWebView.estimatedProgress animated:YES];
        if (_progressView.progress == 1) {
            _progressView.hidden = YES ;
            _progressView.progress = 0 ;
        }
    }
}

/*
// 将要开始拖动
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _scrollStopY = scrollView.contentOffset.y;
}
// 正在拖动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"webview的偏移量: %f",scrollView.contentOffset.y);
    if(scrollView.contentOffset.y>10&&_scrollStopY!=1){
        if(scrollView.contentOffset.y>_scrollStopY){
            [self hideOrShow:YES];
        }else{
            [self hideOrShow:NO];
        }
    }else{
        [_topImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.offset(NAVCBAR_HEIGHT);
        }];
        [_botImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);
        }];
        _backBtn.hidden = NO;
        _closeBtn.hidden = NO;
        _reloadBtn.hidden = NO;
        [self.view bringSubviewToFront:_wkWebView];
    }
}
// yes隐藏
-(void)hideOrShow:(BOOL)ifHide{
    if (ifHide) { // 隐藏
        [UIView animateWithDuration:0.36 animations:^{
            [_topImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.offset((NAVCBAR_HEIGHT-44)+20);
            }];
            
            [_botImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(40*HWB);
            }];
            [self.view layoutIfNeeded];
            
            [_webTitleLab mas_updateConstraints:^(MASConstraintMaker *make) {
                if (IS_IPHONE_X) { make.bottom.offset(-8); }
                else{ make.bottom.offset(-2); }
                make.height.offset(20);
            }];
            _webTitleLab.font = [UIFont systemFontOfSize:12*HWB];
        } completion:^(BOOL finished) {
            _backBtn.hidden = YES;
            _closeBtn.hidden = YES;
            _reloadBtn.hidden = YES;
            [self.view bringSubviewToFront:_wkWebView];
        }];
    }else{
        [UIView animateWithDuration:0.36 animations:^{
            [_topImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                 make.height.offset(NAVCBAR_HEIGHT);
            }];
            
            [_botImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
            }];

            [_webTitleLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(-10); make.height.offset(24);
            }];
            
            [self.view layoutIfNeeded];
            _webTitleLab.font = [UIFont systemFontOfSize:14*HWB];

        } completion:^(BOOL finished) {
            _backBtn.hidden = NO;
            _closeBtn.hidden = NO;
            _reloadBtn.hidden = NO;
            [self.view bringSubviewToFront:_wkWebView];
        }];
    }
}*/

// 将要加载webUrl  返回值：是否允许这个导航加载
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    NSURLRequest * request = navigationAction.request;
    NSString * urlString = request.URL.absoluteString;
    
    LOG(@"将要开始加载的链接或:\n\n%@\n\n%@",urlString,request.allHTTPHeaderFields);
    
    if ([urlString hasPrefix:@"http"]) {
        if (![[NSString stringWithFormat:@"%@",navigationAction.request.URL] isEqualToString:[NSString stringWithFormat:@"%@",self.currentUrl]]) {
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:navigationAction.request.URL];
            [request setValue:TOKEN forHTTPHeaderField:@"token"];
            [webView loadRequest:request];
            self.currentUrl = navigationAction.request.URL;
            decisionHandler(WKNavigationActionPolicyCancel); // 必须实现 取消加载 不然会加载2遍
            return;
        } else {
            self.currentUrl = navigationAction.request.URL;
            decisionHandler(WKNavigationActionPolicyAllow); // 必须实现 加载
            return;
        }
    }else{
        if([[UIApplication sharedApplication]canOpenURL:navigationAction.request.URL]){
            decisionHandler(WKNavigationActionPolicyAllow);
            [[UIApplication sharedApplication]openURL:navigationAction.request.URL];
        }else{
            decisionHandler(WKNavigationActionPolicyCancel);
        }
    }
}
-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    if (!navigationAction.targetFrame.isMainFrame) { [webView loadRequest:navigationAction.request]; }
    return nil;
}

// 开始加载webUrl
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    LOG(@"网页开始加载。。。。");
    _progressView.hidden = NO;
    _progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view bringSubviewToFront:_progressView];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES ;
}

//加载失败！
-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    LOG(@"加载失败：\n\n%@",error)
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
}

//加载完成
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSLog(@"网页标题: %@",webView.title);
    if (webView.title&&webView.title.length>0) {
        _webTitleLab.text = webView.title;
        self.title = webView.title;
    }
    if (!webView.isLoading) {
        _progressView.hidden = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
    }
    if (_wkWebView.canGoBack==YES) {
        [UIView animateWithDuration:0.26 animations:^{ _closeBtn.alpha = 1; }];
    }else{
        [UIView animateWithDuration:0.26 animations:^{ _closeBtn.alpha = 0; } completion:^(BOOL finished) { }];
    }
}

//加载失败时执行
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    LOG(@"ProvisionalNavigation失败:\n\n%@\n\n%@",navigation,error)
}

-(void)dealloc{
    [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end




@implementation DdyShopView1

+(void)ShowDMLoginMsgVC:(UIViewController*)topvc platform:(NSString*)platform{
    DdyShopView1 * shopView1 = [[DdyShopView1 alloc]init];
    shopView1.platform = platform;
    [topvc presentViewController:shopView1 animated:NO completion:nil];
}


-(instancetype)init{
    self = [super init];
    if (self) { self.modalPresentationStyle = UIModalPresentationCustom; }return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creatBackView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showSelfAnimation];
    });
}

-(void)showSelfAnimation{
    [UIView animateWithDuration:0.36 animations:^{
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void)removeSelfAnimation{
    [UIView animateWithDuration:0.26 animations:^{
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void)creatBackView{
    _backView = [[UIView alloc]init];
    _backView.backgroundColor = COLORWHITE;
    [_backView cornerRadius:10*HWB];
    _backView.center = self.view.center;
    _backView.bounds = CGRectMake(0, 0, 260*HWB, 260*HWB);
    [self.view addSubview:_backView];
    
    
    UIImageView * topBackImgv = [[UIImageView alloc]init];
    topBackImgv.image = [UIImage imageNamed:@"ddy_web_login_show_t"];
    [_backView addSubview:topBackImgv];
    [topBackImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
        make.height.offset(100*HWB);
    }];
    
    UIButton * knowBtn = [[UIButton alloc]init];
    [knowBtn setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [knowBtn setTitleColor:COLORWHITE forState:UIControlStateNormal];
    knowBtn.titleLabel.font = [UIFont systemFontOfSize:13*HWB];
    [knowBtn setBackgroundImage:[UIImage imageNamed:@"ddy_web_login_know"] forState:(UIControlStateNormal)];
    ADDTARGETBUTTON(knowBtn, removeSelfAnimation)
    [_backView addSubview:knowBtn];
    [knowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-25*HWB);
        make.width.offset(167.6*HWB);
        make.height.offset(36*HWB);
    }];
    
    NSString * str1 = FORMATSTR(@"需要您登录%@账号",_platform);
    UILabel * titleLab = [UILabel labText:str1 color:COLORWHITE font:14*HWB];
    [topBackImgv addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(topBackImgv.mas_centerX);
        make.centerY.equalTo(topBackImgv.mas_centerY);
    }];
    
    NSString * str2 = FORMATSTR(@"您正在浏览%@网页，如您需要下单或查看账号消息，请使用您的%@账号登录",_platform,_platform);
    UILabel * labIns = [UILabel labText:str2 color:Black102 font:12*HWB];
    [_backView addSubview:labIns];
    [labIns mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topBackImgv.mas_bottom).offset(10);
        make.bottom.equalTo(knowBtn.mas_top).offset(-10);
        make.left.offset(30*HWB); make.right.offset(-30*HWB);
    }];
     
    _backView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    _backView.alpha = 0.3;
   
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end




@implementation DdyShopView2

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        _webView = [[UIWebView alloc]init];
        _webView.backgroundColor = COLORWHITE;
//        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@""]]];
        [self addSubview:_webView];
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.offset(0);
            make.top.equalTo(self.mas_centerY).offset(-60*HWB);
        }];
    }
    return self;
}

@end









