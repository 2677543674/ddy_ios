//
//  YourTradeWebViewController.m
//  DingDingYang
//
//  Created by ddy on 2017/11/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "TradeWebVC.h"
#import "SearchGoodsVC.h"

@interface TradeWebVC () <UIWebViewDelegate>

@property(nonatomic,copy) NSString * nowLoadUrl; // 记录正在加载的链接

@end

@implementation TradeWebVC


/**
 打开百川页面

 @param shopOrGoodsId 商品ID或者店铺ID
 @param urlStr url链接
 @param type 类型： 1:打开店铺(id)  2:商品详情页(id)  3:添加商品到购物车(id)  4:打开购物车(无需参数)  5:打开任意的淘宝天猫链接(url)  6:打开全部订单页面
 */
+(void)showCustomBaiChuan:(NSString*)shopOrGoodsId url:(NSString*)urlStr pagetype:(NSInteger)type{
    
    TradeWebVC * myView = [[TradeWebVC alloc] init] ;
    myView.hidesBottomBarWhenPushed = YES ;
    myView.showOrderType = type ;
    id<AlibcTradePage> page ;
    switch (type) {
        case 1:{  page = [AlibcTradePageFactory shopPage:REPLACENULL(shopOrGoodsId)] ;         } break;
        case 2:{  page = [AlibcTradePageFactory itemDetailPage:REPLACENULL(shopOrGoodsId)] ;   } break;
        case 3:{  page = [AlibcTradePageFactory addCartPage:REPLACENULL(shopOrGoodsId)] ;      } break;
        case 4:{  page = [AlibcTradePageFactory myCartsPage] ;                                 } break;
        case 5:{  page = [AlibcTradePageFactory page:REPLACENULL(urlStr)] ;                    } break;
        case 6:{  page = [AlibcTradePageFactory myOrdersPage:0 isAllOrder:YES] ;               } break;
        case 7:{  page = [AlibcTradePageFactory page:REPLACENULL(urlStr)] ;                    } break;
        default:{ page = [AlibcTradePageFactory page:REPLACENULL(urlStr)] ;                    } break;
    }

    // 设置记录打开和跳转方式
    AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
    showParam.openType = AlibcOpenTypeAuto ;
    showParam.isNeedPush = YES ;
    
    NSInteger res = [[AlibcTradeSDK sharedInstance].tradeService openByBizCode:@"cart" page:page webView:myView.webView parentController:myView showParams:showParam taoKeParams:nil trackParam:nil tradeProcessSuccessCallback:^(AlibcTradeResult * _Nullable result) {
        // 交易结果只有从加入购物车，然后从购物车购物才能产生回调，否则此回调无效
        NSLog(@"交易结果：%lu",(unsigned long)result.result);
    } tradeProcessFailedCallback:^(NSError * _Nullable error) {
        NSLog(@"错误信息：%@",error);
    }];
    
    if (res == 1) {
        [TopNavc pushViewController:myView animated:YES];
    }
    
}

/**filedata 抓取订单，返回结果

 @param stateBlock  回调状态
 @param result   回调服务器返回结果
 */
+(void)grabTheOrderState:(GrabOrderState)stateBlock upload:(UploadOrderResult)result {
    if ([[ALBBSession sharedInstance] isLogin]) {  //打开订单页，必须授权后才可以
        TradeWebVC * myView = [[TradeWebVC alloc] init] ;
        myView.stateBlock = stateBlock ;
        myView.uploadResult = result ;
        myView.showOrderType = 100 ;
        [NetRequest requestTokenURL:url_getbc_catchWay parameter:nil success:^(NSDictionary *nwdic) {
            if ([REPLACENULL(nwdic[@"result"]) isEqualToString:@"OK"]) { // 接口访问成功
                if ([REPLACENULL(nwdic[@"getOrderType"]) integerValue]==1) { // 服务器返回需要抓单
                    myView.getOrderType = [REPLACENULL(nwdic[@"getOrderType"]) integerValue] ;
                    myView.parsingOrder = [REPLACENULL(nwdic[@"parsingOrder"]) integerValue] ;
                    myView.openOrderUrl = REPLACENULL(nwdic[@"openOrderUrl"]) ;
                    myView.regular      = REPLACENULL(nwdic[@"regular"]) ;
                    
                    AlibcTradeShowParams * showParam = [[AlibcTradeShowParams alloc] init];
                    showParam.openType = AlibcOpenTypeAuto ;
                    
                    id<AlibcTradePage> page ;
                    
                    if ([myView.openOrderUrl hasPrefix:@"http"]) {
                        page = [AlibcTradePageFactory page:myView.openOrderUrl] ;
                    }else{
                        page = [AlibcTradePageFactory myOrdersPage:0 isAllOrder:YES] ;
                    }

                    [[AlibcTradeSDK sharedInstance].tradeService openByBizCode:@"detail" page:page webView:myView.webView parentController:myView showParams:showParam taoKeParams:nil trackParam:nil tradeProcessSuccessCallback:^(AlibcTradeResult * _Nullable result) {
                        // 交易结果只有从加入购物车，然后从购物车购物才能产生回调，否则此回调无效
                        NSLog(@"交易结果：%lu",(unsigned long)result.result);
                    } tradeProcessFailedCallback:^(NSError * _Nullable error) {
                        NSLog(@"错误信息：%@",error);
                    }];
                    NSLog(@"将要跳转页面。");
                    myView.view.frame = CGRectZero ;
                    [[UIApplication sharedApplication].keyWindow.rootViewController addChildViewController:myView];
                    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:myView.view];
                }else{
                    myView.stateBlock(2);
                 }
            }
        } failure:^(NSString *failure) {
             myView.stateBlock(6);
        }];
    }
}

+(void)tbSecondaryAuthorization:(NSString*)pagrUrl jsCode:(NSString*)jsCode hdUrl:(NSString*)hdUrl shqState:(AuthorizationState)shqStateBlock{
    TradeWebVC * myView = [[TradeWebVC alloc] init];
    myView.shqStateBlock = shqStateBlock;
    myView.clickJsCode = jsCode;
    myView.huiDiaoUrl = hdUrl;
    myView.showOrderType = 7;
    id<AlibcTradePage> page = [AlibcTradePageFactory page:REPLACENULL(pagrUrl)];
    
    // 设置记录打开和跳转方式
    AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
    showParam.openType = AlibcOpenTypeAuto ;
    showParam.isNeedPush = YES ;
    
    NSInteger res = [[AlibcTradeSDK sharedInstance].tradeService openByUrl:pagrUrl identity:@"trade" webView:myView.webView parentController:myView showParams:showParam taoKeParams:nil trackParam:nil tradeProcessSuccessCallback:nil tradeProcessFailedCallback:nil];

    if (res == 1) {
        myView.hidesBottomBarWhenPushed = YES;
        [TopNavc pushViewController:myView animated:YES];
    }
}



-(instancetype)init{
    self = [super init];
    if (self) { self.webView.delegate = self; }
    return self ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark === >>> 自定义百川页面UIWebView代理

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    LOG(@"开始加载URL：\n\n%@",request.URL);
    _nowLoadUrl = FORMATSTR(@"%@",request.URL);
//    SHOWMSG(FORMATSTR(@"%@",request.URL))
    if (_showOrderType==4) { // 当前页面类型为购物车
        
            @try {
                //使用正则取值
                NSMutableString * str = [NSMutableString stringWithString:request.URL.absoluteString];
                if ([str containsString:@"sellerId"]) {
                    return YES;
                }
                NSString * zhengzeStr = @"\\d{10,13}" ;
                NSRegularExpression * regularE = [[NSRegularExpression alloc]initWithPattern:zhengzeStr options:0 error:nil];
                NSArray * ary = [regularE matchesInString:str options:0 range:NSMakeRange(0, str.length)];
                NSMutableArray * orderNo = [[NSMutableArray alloc]init];
                for (NSInteger i=0; i<ary.count; i++) {
                    NSTextCheckingResult * match = ary[i];
                    NSRange range = [match range] ;
                    [orderNo addObject:REPLACENULL([str substringWithRange:range])];
                }
                NSSet * set = [NSSet setWithArray:orderNo];
                NSArray * noOrderAry = [set allObjects];
                LOG(@"%@\n\n%@",orderNo,noOrderAry);
                if (noOrderAry.count==1) {
                    
                    MBShow(@"正在加载...")
                    
                    NSString * reqUrl = FORMATSTR(@"%@%@",dns_dynamic_main,url_getName_buyGoodsid);
                    NSDictionary * pdic = @{@"goodsId":noOrderAry[0],@"ifSearchMore":@"1"};
                    [NetRequest requestType:0 url:reqUrl ptdic:pdic success:^(id nwdic) {
                        MBHideHUD
                        NSString * goodsName = REPLACENULL(nwdic[@"goodsShortName"]);
                        if(goodsName.length>0){
                            SearchGoodsVC * searchResults = [[SearchGoodsVC alloc]init] ;
                            searchResults.hidesBottomBarWhenPushed = YES ;
                            searchResults.searchType = 1;
                            searchResults.searchStr = goodsName;
                            PushTo(searchResults, YES)
                        }else{
                            [TradeWebVC showCustomBaiChuan:@"" url:FORMATSTR(@"%@",request.URL) pagetype:5];
                        }
                    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                        [TradeWebVC showCustomBaiChuan:@"" url:FORMATSTR(@"%@",request.URL) pagetype:5];
                    }];
                    
                    return NO ;
                    
                }else{ return YES ; }

            } @catch (NSException *exception) {
                return  YES ;
            } @finally {

            }

    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if (![webView isLoading]) {

        self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];

        NSString * lJs = @"document.body.innerHTML" ;
        NSString * htmlStr = [webView stringByEvaluatingJavaScriptFromString:lJs];
        LOG(@"html：\n\n%@",htmlStr);
        
        if (_showOrderType==100) { // 当前页面是抓单的页面
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 [self startParsing:htmlStr];
            });
        }
        
        if (_showOrderType==7) { // 淘宝二次授权
            // (initial-scale是初始缩放比,minimum-scale=1.0最小缩放比,maximum-scale=5.0最大缩放比,user-scalable=yes是否支持缩放)
            NSString * metaJs = @"document.getElementsByName(\"viewport\")[0].content = \"width=self.view.frame.size.width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"";
            [webView stringByEvaluatingJavaScriptFromString:metaJs];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSString * clickShq = @"if(window.parent.document.getElementById(\"J_Submit\")!=null){window.parent.document.getElementById('J_Submit').click();};";
                if (_clickJsCode.length>0) { clickShq = _clickJsCode; }
                [webView stringByEvaluatingJavaScriptFromString:clickShq];
            });
            
            if ([_nowLoadUrl hasPrefix:_huiDiaoUrl]) {
                
                NSData * jsonData = [htmlStr dataUsingEncoding:(NSUTF8StringEncoding)];
                NSDictionary * dicData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
                if (dicData) {
                    if ([REPLACENULL(dicData[@"result"]) isEqualToString:@"OK"]) {
                        self.shqStateBlock(1, @"授权成功~");
                    }else{
                        self.shqStateBlock(0, REPLACENULL(dicData[@"result"]));
                    }
                    PopTo(YES)
//                    [self removeSelf];
                }
                
//                if ([htmlStr containsString:@"重新授权"]||[htmlStr containsString:@"授权淘宝号出错"]) {
//                    self.shqStateBlock(0,@"淘宝号授权出错，请重新授权");
//                }else{ self.shqStateBlock(1,@""); }
                
    
               
            }
            
//                SHOWMSG(@"已自动触发登录事件~")
//                LOG(@"byJs: %@",byJs);
//                [self removeSelf];
            
            
           
        }
        
    }
}

-(void)dealloc{ NSLog(@"继承于百川淘宝的页面释放了。。。。。。"); }


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    LOG(@"---> 百川web加载失败!\n\n%@",error);
    if (_showOrderType==100) { _stateBlock(3);  [self removeSelf]; }
}


-(void)startParsing:(NSString*)htmlStr{
    if (_parsingOrder==2) {
        if (_regular.length==0) { return ; }
        NSArray * noOrderAry = @[] ;
        @try {
            // 用正则表达式来取出订单号
            NSMutableString * str = [NSMutableString stringWithString:htmlStr];
            NSString * zhengzeStr = _regular ;
            NSRegularExpression * regularE = [[NSRegularExpression alloc]initWithPattern:zhengzeStr options:0 error:nil];
            NSArray * ary = [regularE matchesInString:str options:0 range:NSMakeRange(0, str.length)];
            
            NSMutableArray * orderNo = [[NSMutableArray alloc]init];
            for (NSInteger i=0; i<ary.count; i++) {
                NSTextCheckingResult * match = ary[i];
                NSRange range = [match range] ;
                [orderNo addObject:REPLACENULL([str substringWithRange:range]) ];
            }
            NSSet *set = [NSSet setWithArray:orderNo];
            noOrderAry = [set allObjects];
            
        } @catch (NSException *exception) {
            
        } @finally {
            LOG(@"获取取到的订单号：\n\n%@",noOrderAry);
            if (noOrderAry.count>0) {
            
                NSDictionary * dic = @{@"token":TOKEN,@"orderdata":[noOrderAry componentsJoinedByString:@","]} ;
                NSData * data = [[NSData alloc]init];
                [NetRequest urlUpFile:url_upload_bcOrder ptdic:dic pName:@"htmlDate" upType:1 data:@[data] success:^(id nwdic) {
                    _uploadResult(nwdic) ;
                    if (_stateBlock!=nil) { _stateBlock(1) ;  [self removeSelf]; }
                } progress:^(NSProgress *uploadProgress) {
                    
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    if (_stateBlock!=nil) { _stateBlock(5) ;  [self removeSelf];  }
                }];

            }else{  if (_stateBlock!=nil) { _stateBlock(4) ;  [self removeSelf]; } }
        }
    }else{
        NSData * data = [htmlStr dataUsingEncoding:(NSUTF8StringEncoding)];
        [NetRequest urlUpFile:url_upload_bcOrder ptdic:@{@"orderdata":@""} pName:@"htmlDate" upType:1 data:@[data] success:^(id nwdic) {
            if (_stateBlock!=nil) { _stateBlock(1) ;  [self removeSelf]; }
        } progress:^(NSProgress *uploadProgress) {
            
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            if (_stateBlock!=nil) { _stateBlock(5) ;  [self removeSelf];  }
        }];
//        [NetRequest upFile:url_upload_bcOrder parameter:@{@"token":TOKEN,@"orderdata":@""} fileP:@"htmlDate" fileType:@"text/plain" fileName:@"txt" fileAry:@[data] success:^(NSDictionary * nwdic) {
//            _uploadResult(nwdic) ;
//            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
//                _uploadResult(nwdic) ;
//                if (_stateBlock!=nil) { _stateBlock(1) ;  [self removeSelf]; }
//            }else{
//                if (_stateBlock!=nil) { _stateBlock(5) ;  [self removeSelf];  }
//            }
//        } failure:^(NSString *failure) {
//            if (_stateBlock!=nil) { _stateBlock(5) ;  [self removeSelf];  }
//        }];
     }
}


-(void)removeSelf{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        if (_getOrderType==7) {
//            SHOWMSG(@"已关闭自动授权页面~")
        }
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
