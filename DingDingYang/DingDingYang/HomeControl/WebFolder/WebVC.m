


// 需要裁剪拦截的前缀
#define web_mark   @"http://jfkjcodemix/?urlparameters="
#define web_dm_link  @"https://c.duomai.com"

#import "WebVC.h"
#import <WebKit/WebKit.h>
#import "NewShareAppVC.h"
#import "NewCallCeterVC.h"
#import "PostersVC.h"
#import "RankingListVC.h"
#import "UpgradeVC.h"
#import "DdyShopWebVC.h"
#import "LYSavePhotoManager.h"


@interface WebVC ()<WKUIDelegate,WKNavigationDelegate,UIScrollViewDelegate>

@property(nonatomic,strong) WKWebView * wkWebView ;
@property(nonatomic,strong) UIProgressView *progressView;
@property(nonatomic,strong) UIButton * shareWebBtn ; // 右侧分享按钮
@property(nonatomic,strong) NSDictionary  * nwdic ; // 记录web页面传到app的参数
@property(nonatomic,strong) UIButton * colosWebBtn ; // 关闭页面的按钮


@property (strong, nonatomic) WKProcessPool *processPoll;

@property (strong, nonatomic) WKWebViewConfiguration *config;

/** 保存当前请求的URL */
@property(nonatomic, strong) NSURL *currentUrl;

@end

@implementation WebVC



+(void)pushWebViewUrlOrStr:(NSString*)urlWeb title:(NSString*)title isSHowType:(NSInteger)showType{
    
    WebVC * webvc = [[WebVC alloc]init];
    webvc.showType = showType;
    webvc.hidesBottomBarWhenPushed = YES;
    if (urlWeb) { webvc.urlWeb = urlWeb; }
    if (title) { webvc.nameTitle = title; }
    [TopNavc pushViewController:webvc animated:YES];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _nameTitle ;
    
    [self addTopTabbarView];
    
//    self.processPoll = [[WKProcessPool alloc]init];
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    self.config = config;
    WKPreferences *preference = [[WKPreferences alloc]init];
    preference.javaScriptCanOpenWindowsAutomatically = true;
    
//    config.processPool = self.processPoll;
    config.mediaTypesRequiringUserActionForPlayback = YES;
    config.allowsAirPlayForMediaPlayback = YES;
    config.allowsInlineMediaPlayback = YES ;
    config.preferences = preference;

    
    _wkWebView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config] ;
    _wkWebView.UIDelegate = self ;
    _wkWebView.navigationDelegate = self ;
    [self.view addSubview:_wkWebView];
    [_wkWebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);  make.bottom.offset(0);
        make.left.offset(0); make.right.offset(0);
    }];
    
    if (_urlWeb.length>0) {
        
        if ([_urlWeb hasPrefix:@"http"]) {  //加载网页
//            if ([_urlWeb containsString:@".api"]&&TOKEN.length>0) {
//                if (![_urlWeb containsString:@"token"]) {
//                    if ([_urlWeb containsString:@"?"]) {
//                        _urlWeb = FORMATSTR(@"%@&token=%@",dns_dynamic_main,TOKEN);
//                    }else{
//                        _urlWeb = FORMATSTR(@"%@?partnerId=%d&token=%@",dns_dynamic_main,PID,TOKEN);
//                    }
//                }
//            }
            if ([_urlWeb hasPrefix:@"http://"]||[_urlWeb hasPrefix:@"https://"]) {
               [_wkWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlWeb]]];
            }else{
                SHOWMSG(FORMATSTR(@"无效的链接:\n%@",_urlWeb))
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    PopTo(YES)
                });
            }
            
        }else{  //h5字符串
            NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
            [_wkWebView loadHTMLString:[headerString stringByAppendingString:_urlWeb] baseURL:nil];
        }
        LOG(@"上个页面传过来的网页链接：\n\n%@",_urlWeb)
    }else{ SHOWMSG(@"链接为空!") }
    
    //进度条初始化
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0,ScreenW,1.5)];
    _progressView.progressTintColor = RGBA(76, 217, 99, 1.0) ;
    _progressView.trackTintColor = [UIColor clearColor] ;
    _progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    _progressView.progress = 0 ;
    _progressView.hidden = YES ;
    [self.view addSubview:_progressView];
    [_wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
//    _wkWebView.scrollView.delegate = self;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView { return nil; }


// 监控web加载进度
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        [_progressView setProgress:self.wkWebView.estimatedProgress animated:YES];
        if (_progressView.progress == 1) {
            _progressView.hidden = YES ;
            _progressView.progress = 0 ;
        }
    }
//    else{
//        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
//    }
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        
        NSURLCredential *card = [[NSURLCredential alloc]initWithTrust:challenge.protectionSpace.serverTrust];
        
        completionHandler(NSURLSessionAuthChallengeUseCredential,card);
        
    }
}

// 将要加载webUrl  返回值：是否允许这个导航加载
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    NSURL *url = navigationAction.request.URL;
//    [self wkWebViewWillStart:url];
    //为了解决跨域问题，每次跳转url时把cookies拼接上
    NSMutableURLRequest *request = (NSMutableURLRequest *)navigationAction.request;
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    NSDictionary *dict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
    request.allHTTPHeaderFields = dict;
    
    NSString * urlString = navigationAction.request.URL.absoluteString;
    LOG(@"将要开始加载的链接或参数:\n\n%@",urlString)

    if ([urlString hasPrefix:@"http"]) {  //允许跳转
        if ([urlString hasPrefix:web_mark]) {
            decisionHandler(WKNavigationActionPolicyCancel);
            [self interceptUrl:[urlString substringFromIndex:web_mark.length]];
        }else if ([urlString hasPrefix:web_dm_link]){ // 多麦综合平台拦截到另外一个web页面
            decisionHandler(WKNavigationActionPolicyCancel);
            
            DdyShopWebVC * ddydmWeb = [[DdyShopWebVC alloc]init];
            ddydmWeb.urlWeb = urlString;
            PushTo(ddydmWeb, YES)
        }else{
            if (![[NSString stringWithFormat:@"%@",url] isEqualToString:[NSString stringWithFormat:@"%@",self.currentUrl]]) {
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setValue:TOKEN forHTTPHeaderField:@"token"];
                [webView loadRequest:request];
                self.currentUrl = url;
                decisionHandler(WKNavigationActionPolicyCancel); // 必须实现 取消加载 不然会加载2遍
                return;
            } else {
                self.currentUrl = url;
                decisionHandler(WKNavigationActionPolicyAllow); // 必须实现 加载
                return;
            }
        }
    }else if ([[UIApplication sharedApplication]canOpenURL:navigationAction.request.URL]){
        [[UIApplication sharedApplication]openURL:navigationAction.request.URL] ;
        decisionHandler(WKNavigationActionPolicyCancel) ;
    }else{
        decisionHandler(WKNavigationActionPolicyAllow) ;
    }
    
}


-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    if (!navigationAction.targetFrame.isMainFrame) {
        //WKHTTPCookieStore的使用
//        WKHTTPCookieStore *cookieStore = webView.configuration.websiteDataStore.httpCookieStore;
//        //get cookies
//        [cookieStore getAllCookies:^(NSArray<NSHTTPCookie *> * _Nonnull cookies) {
//            NSLog(@"All cookies %@",cookies);
//        }];
//        [cookieStore setCookie:cookieStore completionHandler:nil];
//
//        [webView loadRequest:navigationAction.request];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:navigationAction.request.URL];
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary *dict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
        request.allHTTPHeaderFields = dict;
        
        [self.wkWebView loadRequest:request];
    }
    return nil;
}

// 开始加载webUrl
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    LOG(@"网页开始加载。。。。");
    _progressView.hidden = NO;
    _progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view bringSubviewToFront:_progressView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES ;
}

//加载失败！
-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    LOG(@"加载失败：\n\n%@",error)
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
//    SHOWMSG(@"加载失败，请手动刷新!")
}

//加载完成
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSMutableDictionary *cookiesDictionary = [NSMutableDictionary dictionary];
    NSArray *cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies;
    
    for (NSHTTPCookie *cookie in cookies) {
        [cookiesDictionary setValue:cookie.value forKey:cookie.name];
    }
    
    NSMutableArray *cookieArray = [NSMutableArray array];
    for (NSString *key in cookiesDictionary) {
        [cookieArray addObject:[NSString stringWithFormat:@"%@=%@",key,cookiesDictionary[key]]];
    }
    
    if (cookieArray.count > 0) {
        WKUserScript *userScript = [[WKUserScript alloc] initWithSource:[NSString stringWithFormat:@"document.cookie='%@'",[cookieArray componentsJoinedByString:@"&"]] injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:YES];
        NSLog(@"%@",cookieArray);
        
        [self.config.userContentController addUserScript:userScript];
    }
    
    
    if (webView.title.length>0) {
        NSLog(@"%@",webView.title);
        self.title = webView.title ;
    }
    
    if (!webView.isLoading) {
        _progressView.hidden = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
    }
    
    if (_wkWebView.canGoBack==YES) {
        [UIView animateWithDuration:0.36 animations:^{ _colosWebBtn.alpha = 1; }];
    }else{
        [UIView animateWithDuration:0.36 animations:^{
            _colosWebBtn.alpha = 0;
        } completion:^(BOOL finished) { }];
    }
    
    
    NSString * evjs = @"document.documentElement.innerHTML";
    [webView evaluateJavaScript:evjs completionHandler:^(id htmlStr , NSError *  error) {
//        LOG(@"加载完成获取到的html代码：\n\n%@",htmlStr)
    }];
    
}


// 接收到需要重定向请求
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
    LOG(@"接收到需要重定向的要求。。。")
}

//加载失败时执行
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    LOG(@"ProvisionalNavigation失败:\n\n%@\n\n%@",navigation,error)
}


//自定义返回按钮，和右侧按钮
-(void)addTopTabbarView{
    if (_ifTab==YES) {
        UIButton*shuaxin=[UIButton buttonWithType:UIButtonTypeCustom];
        [shuaxin setImage:[UIImage imageNamed:@"shuaxin"] forState:UIControlStateNormal];
        [shuaxin setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        shuaxin.frame=CGRectMake(0, 20, 44, 44);
        [shuaxin addTarget:self action:@selector(clickShuaxin) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * rightItem = [[UIBarButtonItem alloc]initWithCustomView:shuaxin];
        self.navigationItem.rightBarButtonItem = rightItem ;
    }
    
//    UIButton*backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    [backBtn setImage:[UIImage imageNamed:@"goback"] forState:UIControlStateNormal];
//    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(13, 0, 13, 33)];
//
//    backBtn.frame=CGRectMake(0, 20, 44, 44);
//    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
//
//    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = leftItem ;
    
    UIView * leftView = [[UIView alloc]init];
    leftView.frame = CGRectMake(0, 0, 30*HWB+22, 44);
    
    UIButton*backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"goback2"] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(13, -3, 13, 10)];
    backBtn.frame=CGRectMake(0,0, 20, 44);
    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    [leftView addSubview:backBtn];
    
    _colosWebBtn = [UIButton titLe:@"关闭" bgColor:COLORWHITE titColorN:Black102 font:12.5*HWB];
    _colosWebBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    _colosWebBtn.frame = CGRectMake(20, 12, 30*HWB, 20);
    ADDTARGETBUTTON(_colosWebBtn,clickClose)
    [leftView addSubview:_colosWebBtn];
    _colosWebBtn.alpha = 0;
    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftView];
    self.navigationItem.leftBarButtonItem = leftItem ;
    
    if (_ifTab==YES&&_wkWebView.canGoBack==NO) {
        backBtn.hidden=YES;
    }else{
        backBtn.hidden=NO;
    }
    
    if ([_urlWeb hasPrefix:@"http"]&&_showType==1) {
        _shareWebBtn=[[UIButton alloc]init];
        _shareWebBtn.frame=CGRectMake(0, 0, 44, 44);
        _shareWebBtn.tintColor = Black51 ;
        _shareWebBtn.imageView.contentMode = UIViewContentModeScaleAspectFit ;
        [_shareWebBtn setImage:[[UIImage imageNamed:@"fenxiangZ"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [_shareWebBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 18, 5, -5)];
        [_shareWebBtn addTarget:self action:@selector(shareWebUrl) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithCustomView:_shareWebBtn];
        self.navigationItem.rightBarButtonItem = rightItem;
    }
}

// 分享网页链接
-(void)shareWebUrl{
    [SharePopoverVC shareUrl:_urlWeb title:APPNAME des:self.title thubImg:[UIImage imageNamed:LOGONAME] shareType:2];
}

-(void)clickShuaxin{
    if ([_urlWeb hasPrefix:@"http"]) {  //加载网页
        [_wkWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlWeb]]];
    }else{  //h5字符串
        NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
        [_wkWebView loadHTMLString:[headerString stringByAppendingString:_urlWeb] baseURL:nil];
    }
}

-(void)clickClose{
    if (self.navigationController==nil) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }else{
        PopTo(YES)
    }
}

//返回上一级
-(void)clickBack{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
    if (_wkWebView.canGoBack==YES) {
        [_wkWebView goBack];
    }else{
        if (self.navigationController==nil) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }else{
            PopTo(YES)
        }
    }
}

- (void)dealloc {
    [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id) self;
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}


#pragma mark 根据请求的URL，实现app内页面跳转

// 解析并操作web页面传过来的数据
-(void)interceptUrl:(NSString*)urlParameter{
    //unicode+utf8
//    stringByAddingPercentEscapesUsingEncoding用与将Unicode字符转换成有百分号的形式。
//    stringByReplacingPercentEscapesUsingEncoding:将百分号形式转换成Unicode形式
    NSString * jsonStr = [urlParameter stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSData * jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    _nwdic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];

    LOG(@"web页面传过来的内容：\n%@\nUTF8编码后\n%@\n\n转成后的字典：\n\n%@",urlParameter,jsonStr,_nwdic);

    if (_nwdic==nil) {
        SHOWMSG(@"参数错误，解析失败!")
        return ;
    }
    //http://jfkjcodemix/?urlparameters=
    switch ([REPLACENULL(_nwdic[@"type"]) integerValue]) {
            case 0: { [self openCopyStr]; } break;       // 启用复制功能
            case 1: { [self openWaiBuLianJie]; } break;  // 打开淘宝
            case 2: { [self openWaiBuLianJie]; } break;  // 打开支付宝
            case 3: { [self openTaoBao]; } break;        // 淘宝指定页面
            case 4: { [self openShare]; } break;         // 分享
            case 5: { [self openSharePoster]; } break;   // 分享海报
            case 6: { [self openSecondaryVC]; } break;   // 二级页面
            case 7: { [self downloadImageWithImgUrl:_nwdic[@"data"][@"imgUrl"]];} break; // 保存图片
            case 8: { [self saveVideoWithJumpUrl:_nwdic[@"data"][@"vedioUrl"]];} break; // 保存视频
            case 9: { [self downloadImageWithImgUrl:_nwdic[@"data"][@"imgUrl"]];} break; // 图片直接下载
        default:  break;
    }
}

#pragma mark ===>>> 响应web页面的事件
// 复制到粘贴板
-(void)openCopyStr{
    [ClipboardMonitoring pasteboard:REPLACENULL(_nwdic[@"data"][@"copyContent"])];
    if ([[UIPasteboard generalPasteboard].string isEqual:REPLACENULL(_nwdic[@"data"][@"copyContent"])]) {
        SHOWMSG(@"复制成功!")
    }
}

// 打开外部app链接 ( type为1和2使用 )
-(void)openWaiBuLianJie{
    NSURL * tburlStr = [NSURL URLWithString:REPLACENULL(_nwdic[@"data"][@"jumpUrl"])] ;
    if ([[UIApplication sharedApplication]canOpenURL:tburlStr]) {
        [[UIApplication sharedApplication]openURL:tburlStr];
    }else{  SHOWMSG(@"暂时不能打开页面!")  }
}

// 打开淘宝
-(void)openTaoBao{
    NSString * urlStr = REPLACENULL(_nwdic[@"data"][@"jumpUrl"]) ;
    if (urlStr.length>0) {
        [TradeWebVC showCustomBaiChuan:@"" url:urlStr pagetype:5];
    }else{
        SHOWMSG(@"暂时不能打开页面!")
    }
//    if (IsNeedTaoBao==1) {
//        if (urlStr.length>0) {
//            [TradeWebVC showCustomBaiChuan:@"" url:urlStr pagetype:5];
//        }
//    }else{
//        if ([urlStr hasPrefix:@"http"]) {
//            WebVC * web = [[WebVC alloc]init];
//            web.urlWeb = REPLACENULL(_nwdic[@"data"][@"jumpUrl"]);
//            web.title = @"淘宝页面" ;
//            PushTo(web, YES)
//        }else{
//            if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:urlStr]]) {
//                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlStr]];
//            }else{  SHOWMSG(@"暂时不能打开页面!")  }
//        }
//    }
}

//分享视频
- (void)openShareVideo{
    if (_nwdic) {
        [SharePopoverVC shareVideoWithURL:REPLACENULL(_nwdic[@"DataBean"][@"vedioUrl"])];
    }
}

// 分享
-(void)openShare{
    
    NSInteger shareType = [REPLACENULL(_nwdic[@"data"][@"dataType"]) integerValue];
    
    NSString * shareTitle = REPLACENULL(_nwdic[@"data"][@"shareTitle"]) ;
    NSString * content    = REPLACENULL(_nwdic[@"data"][@"content"]) ;
    NSString * targetUrl  = REPLACENULL(_nwdic[@"data"][@"targetUrl"]) ;
    NSArray  * imageAry   = NULLARY(_nwdic[@"data"][@"imgUrl"]) ;
    
    if ([targetUrl hasPrefix:@"http"]) {
        shareTitle = shareTitle.length>0?shareTitle:APPNAME ;
        content    = content.length>0?content:SHARECONTENT ;
    }
    
    switch (shareType) {
            case 1:{ // 分享图片(可多图)
                if (imageAry.count>0) {
                    NSMutableArray * aryImage = [NSMutableArray array];
                    for (NSDictionary*dic in imageAry) {
                        NSString * url = REPLACENULL(dic[@"img"]);
                        [aryImage addObject:url];
                    }
                    [SharePopoverVC openSystemShareImage:aryImage];
                    
//                    SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
//                    [downloader downloadImageWithURL:[NSURL URLWithString:@"http://spk.huasvip.vip/hs/shareImg618?pwd=\Uffe5QAE70wcWbrU\Uffe5"]  options:SDWebImageDownloaderScaleDownLargeImages progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
//                        NSLog(@"图片:%@",image);
//                        NSLog(@"错误:%@",error);
//                    }];
                    
                }
            } break;
            
            case 2:{ // 分享连接
                if ([targetUrl hasPrefix:@"http"]&&imageAry.count>0) {
                    NSString * imgurl = REPLACENULL(imageAry[0][@"img"]) ;
                    if ([imgurl hasPrefix:@"http"]) {
                        SDWebImageDownloader * downloader = [SDWebImageDownloader sharedDownloader];
                        [downloader downloadImageWithURL:[NSURL URLWithString:imgurl]  options:SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *  targetURL) {
                            
                        } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                            [SharePopoverVC shareUrl:targetUrl title:shareTitle des:content thubImg:image shareType:1];
                        }];
                    }
                }else{
                    [SharePopoverVC shareUrl:targetUrl title:shareTitle des:content thubImg:[UIImage imageNamed:LOGONAME] shareType:1];
                }
            } break;
            
            case 18:{ // 分享app
                [MyPageJump pushNextVcByModel:nil byType:51];
            } break;
            
        default:  break;
    }
}

// 分享海报
-(void)openSharePoster{
    PostersVC*poster = [[PostersVC alloc]init];
    poster.hidesBottomBarWhenPushed = YES ;
    PushTo(poster, YES)
}

// 打开二级页面
-(void)openSecondaryVC{
    NSInteger pushvcType = [REPLACENULL(_nwdic[@"data"][@"dataType"]) integerValue];
    switch (pushvcType) {
            case 1: { // 商品详情
                NSDictionary * goodsDic = NULLDIC(_nwdic[@"data"][@"pageBean"]);
                GoodsModel * gmodel = [[GoodsModel alloc]initWithDic:goodsDic] ;
                [PageJump pushToGoodsDetail:gmodel pushType:0];
            }  break;
            
            case 2: { // 淘抢购
                [PageJump yunPushType:3 content:@""];
            }  break;
            
            case 3: { // 限时抢购
                [PageJump yunPushType:7 content:@""];
            }  break;
            
            case 4: { // 官方推荐
                [PageJump yunPushType:16 content:@""];
            }  break;
            
            case 5: { // 福利商城
                FreeOrderVC * freeOrderVC = [[FreeOrderVC alloc]init];
                freeOrderVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:freeOrderVC animated:YES];
            }  break;
            
            case 6: { // 新手上路
                NewRoadVC * onroad = [[NewRoadVC alloc]init];
                onroad.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:onroad animated:YES];
            }  break;
            
            case 7: { // 短视频
                [PageJump yunPushType:12 content:@""];
            }  break;
            
            case 17: { // 分类
                
            }  break;
            
            case 18: { // 分享好友
                if (TOKEN.length==0) {
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) {
                        
                    }];
                }else{
                    NewShareAppVC * shareVC = [[NewShareAppVC alloc]init];
                    NSDictionary * dic = NSUDTakeData(UserInfo) ;
                    PersonalModel*pmodel = [[PersonalModel alloc]initWithDic:dic];
                    shareVC.modelTitle=@"分享好友";
                    shareVC.model = pmodel;
                    shareVC.hidesBottomBarWhenPushed = YES;
                    [TopNavc pushViewController:shareVC animated:YES];
                }
            }  break;
            
            case 19: { // 商品分享页
                
            }  break;
            
            case 20: { // 加入代理
                if (TOKEN.length==0) {
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) {
                        
                    }];
                }else{
                    UpgradeVC * upvc = [[UpgradeVC alloc]init];
                    upvc.modelTitle = @"申请加入";
                    upvc.hidesBottomBarWhenPushed = YES ;
                    [TopNavc pushViewController:upvc animated:YES];
                }
                
                

            }  break;
            
            
            case 21: { // 客服
                if (TOKEN.length==0) {
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) {
                        
                    }];
                }else{
                    NewCallCeterVC * callcenter = [[NewCallCeterVC alloc]init];
                    callcenter.hidesBottomBarWhenPushed = YES ;
                    PushTo(callcenter, YES)
                }
                
            }  break;
            
            case 22: { // 排行榜
                
                if (TOKEN.length==0) {
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:0 loginBlock:^(NSInteger result) {
                        
                    }];
                }else{
                    RankingListVC * rklvc = [[RankingListVC alloc]init];
                    rklvc.hidesBottomBarWhenPushed = YES ;
                    PushTo(rklvc, YES)
                }
                
            }  break;
            
        default:{ // 进入商品分类
            
        } break;
    }
}

- (void)downloadImageWithImgUrl:(NSArray *)imgUrl{
    LYSavePhotoManager *savePhotoManager = [[LYSavePhotoManager alloc] init];
    
    NSInteger count = imgUrl.count;
    
    if (count == 0) {
        return;
    }
    
    MBShow(@"开始下载图片");
    [imgUrl enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [savePhotoManager saveImageToAlbumWithUrl:obj[@"img"] complete:^(NSString * _Nonnull errorStr) {
            if (errorStr.length > 0) {
                MBError(errorStr);
            } else if (idx == count - 1) {
                MBSuccess(@"保存成功");
            }
            
            if (idx == count - 1) {
                MBHideHUD;
            }
        }];
    }];
}

- (void)saveVideoWithJumpUrl:(NSString *)jumpUrl{
    LYSavePhotoManager *savePhotoManager = [[LYSavePhotoManager alloc] init];
    
    if (!jumpUrl) {
        return;
    }
    
    MBShow(@"开始下载视频");
    [savePhotoManager saveVideoToAlbumWithUrl:jumpUrl complete:^(NSString * _Nonnull errorStr) {
        if (errorStr.length > 0) {
            MBError(errorStr);
        } else {
            MBSuccess(@"保存成功");
        }
        
        MBHideHUD;
    }];
}

#pragma mark wkwebview cookie同步

@end
