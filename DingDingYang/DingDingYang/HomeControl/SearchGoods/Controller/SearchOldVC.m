//
//  SearchOldVC.m
//  DingDingYang
//
//  Created by ddy on 2017/4/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SearchOldVC.h"
#import "SearchGoodsVC.h"
#import "OldCell.h"

@interface SearchOldVC () <UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,HotDelegate,UITextFieldDelegate>

@property(nonatomic,strong) UITextField * searchText;
@property(nonatomic,strong) UITableView * tabView;
@property(nonatomic,strong) NSArray * mtAry, * hotListAry; // 历史记录、热词
@property(nonatomic,strong) UIButton * removeBtn; // 清除历史按钮
@property(nonatomic,assign) float hotcellHi ;

@end

@implementation SearchOldVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES ;
    if (NSUDTakeData(SearchOld)!=nil) {
        _mtAry = NSUDTakeData(SearchOld);
        [_tabView reloadData];
    }
    [_searchText becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (_searchType==2||_searchType==3||_searchType==4||_searchType==5) {
        self.navigationController.navigationBar.hidden = NO ;
//        [self.navigationController setNavigationBarHidden:NO animated: animated];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"商品搜索历史" ;
    
    [self addTopTabbarView];
    
    self.view.backgroundColor = COLORWHITE ;
    
    _hotListAry = @[@"女装",@"短裙",@"连衣裙"];
    // 获取热词
    [NetRequest requestTokenURL:url_get_hotWords parameter:nil success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            _hotListAry = NULLARY(nwdic[@"list"]) ;
        }  [_tabView reloadData];
    } failure:^(NSString *failure) { }];

    [self tabView];
#if DingDangDingDang
    [self shuomingBtn];
#endif
    
}

-(void)shuomingBtn{
    UIButton* btn=[UIButton new];
    [btn setTitle:@"使用说明" forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:14*HWB];
    [btn setTitleColor:LOGOCOLOR forState:UIControlStateNormal];
    ADDTARGETBUTTON(btn, clickShuoMing);
    btn.backgroundColor =[[UIColor blackColor]colorWithAlphaComponent:0.03];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tabView.mas_top).offset(240*HWB);
        make.centerX.equalTo(self.view);
        make.height.offset(26*HWB);
        make.width.offset(64*HWB);
    }];
    
}

-(void)clickShuoMing{
    UIScrollView* scrollerView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, -20, ScreenW, ScreenH+20)];
    scrollerView.tag=500;
    scrollerView.backgroundColor=[UIColor blackColor];
    scrollerView.contentSize=CGSizeMake(ScreenW, ScreenW*CGImageGetHeight([UIImage imageNamed:@"dingdangshuoming"].CGImage)/CGImageGetWidth([UIImage imageNamed:@"dingdangshuoming"].CGImage));
    [self.view addSubview:scrollerView];
    UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dingdangshuoming"]];
    imageView.userInteractionEnabled=YES;
    UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [imageView addGestureRecognizer:tap];
    
    
    imageView.bounds=CGRectMake(0, 0, 0, 0);
    imageView.center=CGPointMake(self.view.center.x, self.view.center.y-100*HWB);
    [UIView animateWithDuration:0.75 animations:^{
        imageView.frame=CGRectMake(0, 0, ScreenW, scrollerView.contentSize.height);
    }];

    [scrollerView addSubview:imageView];
}

-(void)tap{
    UIScrollView* scrollerView=[self.view viewWithTag:500];
    [UIView animateWithDuration:0.75 animations:^{
        scrollerView.alpha=0;
    } completion:^(BOOL finished) {
        [scrollerView removeFromSuperview];
    }];
    
    
}

#pragma mark --> 创建表格
-(UITableView*)tabView{
    if (!_tabView) {
        CGRect rect = CGRectMake(0, NAVCBAR_HEIGHT, ScreenW, ScreenH-NAVCBAR_HEIGHT) ;
        _tabView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
        _tabView.delegate=self;   _tabView.dataSource=self;
        _tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tabView];
        
        [_tabView registerClass:[OldCell class] forCellReuseIdentifier:@"oldcell"];
        [_tabView registerClass:[HotCell class] forCellReuseIdentifier:@"hotcell"];

    }
    return _tabView ;
}

#pragma mark -- > 表格代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_hotListAry.count>0) {  return 2; }
    return 1 ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_hotListAry.count>0) {
        if (section==1) { return _mtAry.count; }
        return 1;
    }
    return _mtAry.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_hotListAry.count>0) {
        if (indexPath.section==0) { return 50; }
        return 36;
    }
    return 36;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_hotListAry.count>0) {
        if (section==1) { return 38; }
        return 0 ;
    }
    return 38 ;
}

// 区尾、清除历史搜索
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIButton*btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, ScreenW, 36);
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (_mtAry.count==0) {
        [btn setTitle:@"暂无搜索记录" forState:UIControlStateNormal];
    }else{
        [btn setTitle:@"清除历史记录" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(removeAllold) forControlEvents:UIControlEventTouchUpInside];
    }
    btn.titleLabel.font = [UIFont systemFontOfSize:14*HWB];
    if (_hotListAry.count>0) { if (section==1) { return btn; }   return nil ;}
    return btn ;
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (_hotListAry.count>0) {
        if (section==0) { return @"热门搜索" ; }
        return @"历史搜索";
    }
    return @"历史搜索";
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_hotListAry.count>0) {
        if (indexPath.section == 0) {
            HotCell*hotcell = [tableView dequeueReusableCellWithIdentifier:@"hotcell" forIndexPath:indexPath];
            hotcell.hotAry = _hotListAry ;
            hotcell.deledate = self;
            return hotcell;
        }
        OldCell*cell=[tableView dequeueReusableCellWithIdentifier:@"oldcell" forIndexPath:indexPath];
        cell.labTitle.text = _mtAry[indexPath.row];
        return cell;
    }else{
        OldCell*cell=[tableView dequeueReusableCellWithIdentifier:@"oldcell" forIndexPath:indexPath];
        cell.labTitle.text = _mtAry[indexPath.row];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (_hotListAry.count>0) {
        if (indexPath.section==1) {
            _searchText.text = _mtAry[indexPath.row];
            [self searchGoods];
        }
    }else{
        _searchText.text = _mtAry[indexPath.row];
        [self searchGoods];
    }
    
}

#pragma mark --> 热搜代理
-(void)selectString:(NSString*)oldStr{
    _searchText.text = oldStr ;
    [self searchGoods];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    ResignFirstResponder
}

-(void)removeAllold{
    NSUDRemoveData(SearchOld)
    _mtAry = @[];
    [_tabView reloadData];
    
}

//键盘上的搜索按钮的点击事件（这里是搜索框的代理方法）
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searchGoods];
}

-(void)searchGoods{
    
    NSMutableArray * oldAry = [NSMutableArray array];
    [oldAry addObjectsFromArray:_mtAry];
    
    if (_searchText.text.length>0) {
        NSString * str = _searchText.text;
        if ([oldAry containsObject:str]==YES) {
            [oldAry removeObject:str];
        }
        [oldAry  insertObject:str atIndex:0];
        NSUDSaveData(oldAry, SearchOld)

        SearchGoodsVC * searchVC = [SearchGoodsVC new];
        searchVC.hidesBottomBarWhenPushed = YES;
        searchVC.searchType = _searchType;
        searchVC.searchStr = str;
        PushTo(searchVC, NO);

    }
}

// 创建顶部view
-(void)addTopTabbarView{

    @try {
        UIView * barview = [[UIView alloc]init];
        barview.backgroundColor = COLORWHITE;
        barview.frame = CGRectMake(0,0, ScreenW,NAVCBAR_HEIGHT);
        [self.view addSubview:barview];
        
//        _searchText = [[UISearchBar alloc]initWithFrame:CGRectMake(35,NAVCBAR_HEIGHT-64+26, ScreenW-100, 32)];
//        _searchText.delegate = self;
//        _searchText.searchBarStyle = UISearchBarStyleMinimal;
//        NSString * title = @"请输入关键词搜索" ;
//        if (PID==10088) { title = @"请输入或粘贴淘宝标题"; } // 种草君
//        _searchText.placeholder = title ;
//        _searchText.text = REPLACENULL(_lastStr);
//        [barview addSubview:_searchText];
//
//        if (_searchText) {
//            UITextField * searchField = [_searchText valueForKey:@"_searchField"];
//            searchField.backgroundColor = COLORCLEAR ;
//            searchField.layer.masksToBounds = YES ;
//            searchField.layer.cornerRadius = 16;
//            searchField.font = [UIFont systemFontOfSize:12.5*HWB] ;
//            [searchField setValue:[UIFont systemFontOfSize:12.5*HWB] forKeyPath:@"_placeholderLabel.font"];
//        }
        _searchText = [UITextField textColor:Black51 PlaceHorder:@"请输入商品名称" font:12.5*HWB];
        [_searchText addTarget:self action:@selector(removeKeyboard) forControlEvents:UIControlEventEditingDidEndOnExit];
        _searchText.frame = CGRectMake(40, NAVCBAR_HEIGHT-64+26, ScreenW-110, 32);
        _searchText.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchText.returnKeyType = UIReturnKeySearch;
        _searchText.text = REPLACENULL(_lastStr);
        _searchText.backgroundColor = LinesColor;
        _searchText.delegate = self;
        [_searchText cornerRadius:16];
        [barview addSubview:_searchText];
        UIImageView * imgS = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"searchGray"]];
        _searchText.leftView.frame = CGRectMake(0,NAVCBAR_HEIGHT-64, 32, 32);
        imgS.frame = CGRectMake(8, 8, 16, 16);
        [_searchText.leftView addSubview:imgS];
        
        
        UIButton * searchBtn = [UIButton titLe:@"搜索" bgColor:LOGOCOLOR titColorN:COLORWHITE font:12*HWB];
        [searchBtn addTarget:self action:@selector(searchGoods) forControlEvents:UIControlEventTouchUpInside];
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"home_search_bgimg"] forState:(UIControlStateNormal)];
        [searchBtn cornerRadius:15];
        [barview addSubview:searchBtn];
        [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);
            make.centerY.equalTo(_searchText.mas_centerY);
            make.height.offset(30); make.width.offset(50);
        }];
        
        UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setImage:[UIImage imageNamed:@"goback"] forState:UIControlStateNormal];
        [backBtn setImageEdgeInsets:UIEdgeInsetsMake(13.5, 12, 13.5, 22)];
        backBtn.frame = CGRectMake(0,NAVCBAR_HEIGHT-64+20, 44, 44);
        [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
        [barview addSubview:backBtn];
        
        if (THEMECOLOR==2) { //主题色为浅色
            [searchBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }
    
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
 
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    if(textField.text==nil||textField.text.length==0){ return NO; }
    [self searchGoods]; return YES;
}
-(void)removeKeyboard{ }

-(void)clickBack{ PopTo(NO) }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
