//
//  SearchGoodsVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SearchGoodsVC.h"
#import "SearchOldVC.h"               // 搜索历史
#import "GoodsListCltCell1.h"         // 淘宝商品列表
#import "SearchScreeningView.h"       // 淘宝商品排序条件
#import "SearchDividingLineCltHead.h" // 淘宝全网商品分割线

#import "PDDSortingView.h"     // 拼多多排序
#import "PDDGoodsCltCellH.h"   // 拼多多商品列表

#import "WPHGoodsCltCell.h"    // 唯品会列表
#import "WPHSortingView.h"     // 唯品会排序

#import "DMGoodsCltCell.h"     // 多麦商品列表
#import "DMSortingView.h"      // 多麦商品排序

#import "JingDongCell.h"       // 京东商品列表
#import "JDSortingView.h"      // 京东排序


#define is_show_coupons_tb @"is_show_coupons_tb" // 服务器返回的默认开关状态
#define is_show_switch_tb  @"is_show_switch_tb"  // 是否显示优惠券开关

// 记录开关状态
#define has_coupons_tb     @"has_coupons_tb"
#define has_coupons_jd     @"has_coupons_jd"
#define has_coupons_pdd    @"has_coupons_pdd"


@interface SearchGoodsVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong) UICollectionView * universalListCltv ;
@property(nonatomic,strong) NSMutableDictionary * parameter; //参数
@property(nonatomic,strong) NSMutableArray * listAry , *listAry2 ; //列表数据
@property(nonatomic,assign) NSInteger sectionNum ;  // 分区个数

@property(nonatomic,strong) UITextField  * searchText ; // 顶部搜索框
@property(nonatomic,assign) NSInteger      pageNum ;    // 页数
@property(nonatomic,strong) UIButton     * nodataBtn ;  // 无数时显示
@property(nonatomic,strong) UIButton     * topBtn ;     // 置顶按钮

@property(nonatomic,copy) NSString * reqUrl; // 搜索接口

//****** 淘宝 ******//
@property(nonatomic,strong) SearchScreeningView * sortingTbView; //排序
@property(nonatomic,assign) NSInteger  ifShowCouponBtn; // 是否显示优惠券开关
@property(nonatomic,assign) NSInteger goodsListStyle; // 商品列表样式类型
@property(nonatomic,strong) UIView   * quanSearchView; // 是否仅显示优惠券商品
@property(nonatomic,strong) UISwitch * quanSwitch;  // 是否显示券的开关
@property(nonatomic,assign) NSInteger  qct;  // 是否有券


//****** 唯品会 ******//
@property(nonatomic,assign) NSInteger act; // 0:浏览器打开  1:APP打开


@end

@implementation SearchGoodsVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES ;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (![[self.navigationController.viewControllers lastObject] isKindOfClass:[GoodsDetailsVC class]]&&![[self.navigationController.viewControllers lastObject] isKindOfClass:[SearchOldVC class]]) {
        self.navigationController.navigationBar.hidden = NO ;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    
    _sectionNum = 1;  _pageNum = 1;
    _listAry = [NSMutableArray array];
    _listAry2 = [NSMutableArray array];
    _parameter = [NSMutableDictionary dictionary];

    [self addTopTabbarView];
 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestAgain) name:LoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:UserInfoChange object:nil];
    
    switch (_searchType) {
        case 2: { [self searchTypeJingDong]; } break; // 京东
        case 3: { [self searchTypePinDuoDuo]; } break; // 拼多多
        case 4: { [self searchTypeWeiPinHui]; } break; // 唯品会
        case 5: { [self searchTypeDuoMai]; } break; // 多麦
        default:{ [self searchTypeTaoBao]; } break; // 0或1都是淘宝
    }
    
}

#pragma mark ===>>> 收到通知时响应
-(void)changeState{
    [_parameter removeObjectForKey:@"sortType"];
    [self setSortingTbView:nil];
    [self sortingTbView];
    [_universalListCltv.mj_header beginRefreshing];
}

-(void)requestAgain{
    if (self) {
        if (role_state == 0) {
            [self changeState];
        }else{
            [_universalListCltv.mj_header beginRefreshing];
        }
    }
}

#pragma mark ====>>>> 淘宝搜索
-(void)searchTypeTaoBao{
    
    _parameter[@"key"] = _searchStr;
    if (_searchType==1) { _parameter[@"ifSearchMore"] = @"1"; }
    _goodsListStyle = GoodsListStyle ;
    _reqUrl = url_goods_search;
    [self isShowCoupon];
    
}

#pragma mark 优惠券开关获取
-(void)isShowCoupon{
    static dispatch_once_t onceToken;
    if (onceToken==0) {
        dispatch_once(&onceToken, ^{
            [NetRequest requestType:0 url:url_goods_isShow_coupon ptdic:nil success:^(id nwdic) {
                NSUDSaveData(REPLACENULL(nwdic[@"ifShowCouponBtn"]), is_show_switch_tb)
                NSUDSaveData(REPLACENULL(nwdic[@"qct"]),is_show_coupons_tb)
                [self readCacheOfShowCouponBtn];
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                 [self readCacheOfShowCouponBtn];
            }];
        });
    }else{ [self readCacheOfShowCouponBtn]; }
}

// 根据有券无券设置开关和修改部分参数,并创建商品列表
-(void)readCacheOfShowCouponBtn{
    @try {
        /*
         ifShowCouponBtn  0:不显示券开关   １: 显示券开关
         qct     0:默认只显示有券(开关：开)  １: 默认有券无券都显示(开关：关)
         上传到服务器的有券无券参数:  ifCoupon  0:显示所有 1:只显示有券
         */
        
        [self sortingTbView];
        
        if (NSUDTakeData(is_show_coupons_tb)&&NSUDTakeData(is_show_switch_tb)) {
            _ifShowCouponBtn = [REPLACENULL(NSUDTakeData(is_show_switch_tb)) integerValue];
            _qct =  [REPLACENULL(NSUDTakeData(is_show_coupons_tb)) integerValue];
        }else{ _ifShowCouponBtn = 0;  _qct = 1; }
        
        // 显示开关
        if(_ifShowCouponBtn==1){ [self quanSearchView]; }
        [self universalListCltv];
        
    } @catch (NSException *exception) {
        _parameter[@"ifCoupon"] = _qct?@"0":@"1";
        [self universalListCltv];
    } @finally { }
    
}

#pragma mark 淘宝排序
-(SearchScreeningView*)sortingTbView{
    if (!_sortingTbView) {
        _sortingTbView = [[SearchScreeningView alloc]initWithFrame:CGRectMake(0, NAVCBAR_HEIGHT+1, ScreenW, 38)];
        [self.view addSubview:_sortingTbView];
        __weak typeof(self) weakSelf = self;
        [_sortingTbView selectBlock:^(NSString *sortType) {
            weakSelf.parameter[@"sortType"] = sortType ;
            [weakSelf.universalListCltv.mj_header beginRefreshing];
        }];
    }
    return _sortingTbView;
}



#pragma mark ====>>>> 京东搜索
-(void)searchTypeJingDong{
    _ifShowCouponBtn = 1;
    _parameter[@"key"] = _searchStr ;
    _parameter[@"sortType"] = @"0";
    _reqUrl = url_goods_list_jd;
    [self quanSearchView];
    [self universalListCltv];
    
    CGRect rect = CGRectMake(0, NAVCBAR_HEIGHT+1, ScreenW, 38);
    [JDSortingView creatIn:self.view frame:rect selectType:^(NSString * _Nonnull stype) {
        _parameter[@"sortType"] = stype;
        [_universalListCltv.mj_header beginRefreshing];
    }];
}

#pragma mark ====>>>> 拼多多搜索
-(void)searchTypePinDuoDuo{
    _ifShowCouponBtn = 1;
    _parameter[@"key"] = _searchStr;
    _parameter[@"sortType"] = @"0";
    _reqUrl = url_goods_search_pdd;
    
    [self quanSearchView];
    [self universalListCltv];
    
    CGRect rect = CGRectMake(0, NAVCBAR_HEIGHT+1, ScreenW, 38);
    [PDDSortingView creatIn:self.view frame:rect selectType:^(NSString *stype) {
        _parameter[@"sortType"] = stype;
        [_universalListCltv.mj_header beginRefreshing];
//        switch ([stype integerValue]) {
//            case 0:{ // 按综合处理
//                _parameter[@"sortType"] = stype;
//                [_universalListCltv.mj_header beginRefreshing];
//            } break;
//
//            default:{
//                _parameter[@"sortType"] = stype;
//                [_universalListCltv.mj_header beginRefreshing];
//            } break;
//        }
    }];
}

#pragma mark ===>>> 唯品会搜索
-(void)searchTypeWeiPinHui{
    
    _parameter[@"key"] = _searchStr ;
    _parameter[@"sortType"] = @"0";
    _reqUrl = url_goods_list_wph;
    CGRect rect = CGRectMake(0, NAVCBAR_HEIGHT+1, ScreenW, 38);
    
    [WPHSortingView creatIn:self.view frame:rect selectType:^(NSString *stype) {
        _parameter[@"sortType"] = stype;
        [_universalListCltv.mj_header beginRefreshing];
    }];
    [self universalListCltv];
}

#pragma mark ===>>> 多麦搜索
-(void)searchTypeDuoMai{
    _parameter[@"goodsName"] = _searchStr;
    _parameter[@"sort"] = @"0";
    _reqUrl = url_goods_list_dm;
    CGRect rect = CGRectMake(0, NAVCBAR_HEIGHT+1, ScreenW, 38);
    
    [DMSortingView creatIn:self.view frame:rect selectType:^(NSString *stype) {
        _parameter[@"sort"] = stype;
        [_universalListCltv.mj_header beginRefreshing];
    }];
    [self universalListCltv];
}

#pragma mark ===>>> 券的开关创建
-(UIView*)quanSearchView{
    if (!_quanSearchView) {
        
        _quanSearchView = [[UIView alloc]init];
        _quanSearchView.backgroundColor = COLORWHITE ;
        _quanSearchView.frame = CGRectMake(0, NAVCBAR_HEIGHT+40, ScreenW, 42);
        [self.view addSubview:_quanSearchView];
        
        UIImageView * img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_quanC"]];
        img.contentMode = UIViewContentModeScaleAspectFill ;
        [_quanSearchView addSubview:img];
        [img mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_quanSearchView.mas_centerY);
            make.width.height.offset(18);
            make.left.offset(15*HWB);
        }];
        
        UILabel * lab = [UILabel labText:@"仅显示优惠券商品" color:Black102 font:12.0*HWB];
        [self.view addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(img.mas_right).offset(8*HWB);
            make.centerY.equalTo(_quanSearchView.mas_centerY);
        }];
        
        _quanSwitch = [[UISwitch alloc]init];
        _quanSwitch.onTintColor = LOGOCOLOR ;
        [_quanSwitch setOn:NO];
        _quanSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
        [_quanSwitch addTarget:self action:@selector(clickOnOrOff) forControlEvents:UIControlEventValueChanged];
        [_quanSearchView addSubview:_quanSwitch];
        [_quanSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB);
            make.centerY.equalTo(_quanSearchView.mas_centerY);
        }];
        
        switch (_searchType) {
                
            case 2:{ // 京东
                NSString * states_jd = NSUDTakeData(has_coupons_jd);
                if (states_jd) {
                    _parameter[@"ifGetNoCoupon"] = states_jd;
                    [_quanSwitch setOn:![states_jd boolValue]];
                }else{ _parameter[@"ifGetNoCoupon"] = @"1"; }
            } break;
                
            case 3:{ // 拼多多
                NSString * states_pdd = NSUDTakeData(has_coupons_pdd);
                if (states_pdd) {
                    _parameter[@"ifOnlyCoupon"] = states_pdd;
                    [_quanSwitch setOn:[states_pdd boolValue]];
                }else{ _parameter[@"ifOnlyCoupon"] = @"0"; }
            } break;
                
            default:{
                NSString * states_tb = NSUDTakeData(has_coupons_tb);
                if (states_tb) {
                    _parameter[@"ifCoupon"] = states_tb;
                    [_quanSwitch setOn:[states_tb boolValue]];
                }else{ _parameter[@"ifCoupon"] = @"0"; }
            } break; // 淘宝
        }
        
    }
    return _quanSearchView ;
}

-(void)clickOnOrOff{
    // 淘宝 仅显示有券参数为：1  有券无券都显示参数为：0
    // 京东 仅显示有券参数为：0  有券无券都显示参数为：1
    if (_quanSwitch.on==YES) {
        switch (_searchType) {
            case 2:{ _parameter[@"ifGetNoCoupon"] = @"0"; NSUDSaveData(@"0", has_coupons_jd) } break; // 京东
            case 3:{ _parameter[@"ifOnlyCoupon"] = @"1"; NSUDSaveData(@"1", has_coupons_pdd) } break; // 拼多多
            default:{ _parameter[@"ifCoupon"] = @"1"; NSUDSaveData(@"1", has_coupons_tb) } break; // 淘宝
        }
    
    }else{
        switch (_searchType) {
            case 2:{ _parameter[@"ifGetNoCoupon"] = @"1"; NSUDSaveData(@"1", has_coupons_jd) } break; // 京东
            case 3:{ _parameter[@"ifOnlyCoupon"] = @"0"; NSUDSaveData(@"0", has_coupons_pdd) } break; // 拼多多
            default:{ _parameter[@"ifCoupon"] = @"0"; NSUDSaveData(@"0", has_coupons_tb) } break; // 淘宝
        }
    }
    [_universalListCltv.mj_header beginRefreshing];
}


#pragma mark ===>>> 搜索商品接口
-(void)searchGoodsData{
    
    switch (_searchType) {
        case 4:{ _parameter[@"pageNum"] = FORMATSTR(@"%ld",(long)_pageNum); } break;  // 唯品会
        case 5:{ _parameter[@"p"] = FORMATSTR(@"%ld",(long)_pageNum); } break;  // 多麦
        default:{ _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum); } break; // 淘宝、拼多多、京东都是st
    }
    
    [NetRequest requestType:0 url:_reqUrl ptdic:_parameter success:^(id nwdic) {

        MBHideHUD
        if (_pageNum==1) {
            if(_searchType==4||_searchType==5){ // 唯品会,多麦
                _act = [REPLACENULL(nwdic[@"act"]) integerValue];
            }
            if (_listAry.count>0) { [_listAry removeAllObjects]; }
            if (_listAry2.count>0) { [_listAry2 removeAllObjects]; }
            [_universalListCltv reloadData];
        }
        
        NSArray * lAry = NULLARY(nwdic[@"list"]);
        
        if(_searchType==5){ lAry = NULLARY(nwdic[@"goodsList"]); } // 多麦
        
        [lAry enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *  stop) {
            
            switch (_searchType) {
                case 4:{ // 唯品会
                    GoodsModel * model = [[GoodsModel alloc]initWithWPH:obj];
                    if (model.ifNet) { // 全网商品
                         [_listAry2 addObject:model];
                    }else{ // 本地商品
                         [_listAry addObject:model];
                    }
                } break;
                    
                case 5:{ // 多麦
                    GoodsModel * model = [[GoodsModel alloc]initWithDM:obj];
                    [_listAry addObject:model];
                } break;
                    
                default:{ // 0或1都是淘宝
                    GoodsModel * model = [[GoodsModel alloc]initWithDic:obj];
                    if (_searchType==2) { model.platform = @"京东"; }
                    if (_searchType==3) { model.platform = @"拼多多"; }
                    if (model.itemUrl.length>0&&[model.itemUrl containsString:@"http"]) { //全网商品
                        [_listAry2 addObject:model];
                    }else{ // 本地库商品
                        [_listAry addObject:model];
                    }
                } break;
            }

        }];
        
        if (_listAry2.count==0) { _sectionNum = 1; } else { _sectionNum = 2; }

        [_universalListCltv endRefreshType:lAry.count isReload:YES];

    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [_universalListCltv endRefreshType:1 isReload:YES];
        MBHideHUD  SHOWMSG(failure)
    }];

}

#pragma mark ===>>> UICollectionView
// 商品列表
-(UICollectionView*)universalListCltv{
    if (!_universalListCltv) {
        
        CGRect listCltvFrame = CGRectMake(0, NAVCBAR_HEIGHT+39, ScreenW, ScreenH-(NAVCBAR_HEIGHT+39)) ;
        if (_ifShowCouponBtn==1) {
            listCltvFrame = CGRectMake(0, NAVCBAR_HEIGHT+80, ScreenW, ScreenH-(NAVCBAR_HEIGHT+80)) ;
        }
        
        UICollectionViewFlowLayout * layoutUnvsl = [[UICollectionViewFlowLayout alloc]init];
        _universalListCltv = [[UICollectionView alloc]initWithFrame:listCltvFrame collectionViewLayout:layoutUnvsl];
        _universalListCltv.delegate = self ; _universalListCltv.dataSource = self ;
        _universalListCltv.backgroundColor = COLORGROUP ;
        [self.view addSubview:_universalListCltv];
        
        // 淘宝搜索分割线区头
        [_universalListCltv registerClass:[SearchDividingLineCltHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"dividing_line_head"];
        
        [_universalListCltv registerClass:[WPHGoodsCltCell class] forCellWithReuseIdentifier:@"un_goods_cell"];

        switch (_searchType) {
            
            case 3:{ // 拼多多
                [_universalListCltv registerClass:[PDDGoodsCltCellH class] forCellWithReuseIdentifier:@"pdd_cellH"];
                [_universalListCltv registerClass:[PDDGoodsCltCellV class] forCellWithReuseIdentifier:@"pdd_cellV"];
            } break;
            
            default:{ // 淘宝商品列表
                [_universalListCltv registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"tb_cell1"];
                [_universalListCltv registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"tb_cell2"];
                [_universalListCltv registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"tb_cell3"];
                [_universalListCltv registerClass:[GoodsListCltCell4   class] forCellWithReuseIdentifier:@"tb_cell4"];
                [_universalListCltv registerClass:[GoodsListCltCell5   class] forCellWithReuseIdentifier:@"tb_cell5"];
                [_universalListCltv registerClass:[GoodsListCltCell6   class] forCellWithReuseIdentifier:@"tb_cell6"];
                [_universalListCltv registerClass:[GoodsListCltCell7   class] forCellWithReuseIdentifier:@"tb_cell7"];
                [_universalListCltv registerClass:[GoodsListCltCell10  class] forCellWithReuseIdentifier:@"tb_cell10"];
              
            } break;
        }
       
        // 下拉刷新
        __weak SearchGoodsVC * selfView = self;
        _universalListCltv.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            selfView.pageNum = 1;
            [selfView searchGoodsData];
        }];
        _universalListCltv.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            selfView.pageNum ++;
            [selfView searchGoodsData];
        }];
        
        [_universalListCltv.mj_header beginRefreshing];
        
    }
    [self creatBackBtn]; // 未搜索到商品时提示
    [self topBtn];
    
    return _universalListCltv ;
}

#pragma mark === >>> cell代理
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _sectionNum;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) { return _listAry.count; }  return _listAry2.count;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_searchType==3) { // 拼多多
        
        if (GoodsListStylePdd==1) { // 列表
            PDDGoodsCltCellH * pddCell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"pdd_cellH" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { pddCell1.goodsModel = _listAry[indexPath.item]; }
            return pddCell1 ;
        }else{ // 方块
            PDDGoodsCltCellV * pddCell2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"pdd_cellV" forIndexPath:indexPath];
            if (_listAry.count>=indexPath.item) { pddCell2.goodsModel = _listAry[indexPath.item]; }
            return pddCell2;
        }
        
    }else if (_searchType==2||_searchType==4||_searchType==5){ // 京东、唯品会、多麦
        
        WPHGoodsCltCell * goodsCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"un_goods_cell" forIndexPath:indexPath];
        goodsCell.goodsSource = _searchType;
        if (indexPath.section==0) { if(_listAry.count>indexPath.item){ goodsCell.goodsModel = _listAry[indexPath.item]; } }
        if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ goodsCell.goodsModel = _listAry2[indexPath.item]; } }
        return goodsCell ;
        
    }else{
        switch (_goodsListStyle) {
            case 2:{
                GoodsListCltCell3 * tbCell3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell3" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell3.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell3.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell3 ;
            } break;
                
            case 3:{
                GoodsListCltCell2 * tbCell2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell2" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell2.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell2.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell2;
            } break;
                
            case 4:{
                GoodsListCltCell4 * tbCell4 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell4" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell4.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell4.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell4;
            } break;
                
            case 5:{
                GoodsListCltCell5 * tbCell5 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell5" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell5.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell5.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell5;
            } break;
                
            case 6:{
                GoodsListCltCell6 * tbCell6 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell6" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell6.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell6.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell6;
            } break;
                
            case 7:{
                GoodsListCltCell7 * tbCell7 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell7" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell7.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell7.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell7 ;
            } break;
                
            case 8:{
                GoodsListCltCell10 * tbCell10 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell10" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell10.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell10.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell10;
            } break;
                
            default:{
                GoodsListCltCell1 * tbCell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"tb_cell1" forIndexPath:indexPath];
                if (indexPath.section==0) { if(_listAry.count>indexPath.item){ tbCell1.goodsModel = _listAry[indexPath.row]; } }
                if (indexPath.section==1) { if(_listAry2.count>indexPath.item){ tbCell1.goodsModel = _listAry2[indexPath.row]; } }
                return tbCell1 ;
            } break;
        }
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GoodsModel * model;
    
    if (indexPath.section==0) { if(_listAry.count>indexPath.row) { model = _listAry[indexPath.item] ; } }
    if (indexPath.section==1) { if(_listAry2.count>indexPath.row) { model = _listAry2[indexPath.item]; } }
    
    if (model) {
    
        if (_searchType<=3) { // 京东，拼多多
            [PageJump pushToGoodsDetail:model pushType:_searchType];
        }else{
            NSString * goodsUrl = model.cpsUrl;
            if (_searchType==5) { goodsUrl = model.goodsUrl; }
            
            if (_act==1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:goodsUrl]];
            }else{
                WebVC * web = [[WebVC alloc]init];
                web.urlWeb = goodsUrl ;
                web.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:web animated:YES];
            }
        }

    }else{ SHOWMSG(@"详情打开失败!") }
   
}

#pragma mark === >>> 区头区尾代理
-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        if ([kind isEqual:UICollectionElementKindSectionHeader]) {
            SearchDividingLineCltHead * head = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"dividing_line_head" forIndexPath:indexPath];
            return head ;
        }
    }
    return  nil ;
}

// 区头
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section==1) { return CGSizeMake(ScreenW, 45*HWB ); }
    return CGSizeZero ;
}

#pragma mark === >>> UICollectionViewLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (_searchType==3) { // 拼多多
        
        if (GoodsListStylePdd==1) {
            return CGSizeMake(ScreenW, FloatKeepInt(115*HWB));
        }else{
            return CGSizeMake(ScreenW/2-3, ScreenW/2-3+90*HWB);
        }
        
    }else if (_searchType==2||_searchType==4||_searchType==5){ // 京东、唯品会、多麦
    
        return CGSizeMake(ScreenW, 116*HWB);
        
    }else{ // 部分序号命名和后台不一致
        switch (_goodsListStyle) {
            case 2:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+70*HWB); } break;
            case 4:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
            case 5:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+(role_state==0?70*HWB:90*HWB)); } break;
            case 6:{ return CGSizeMake(ScreenW, 116*HWB); } break;
            case 7:{ return CGSizeMake(ScreenW, 104*HWB); } break;
            case 8:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
            default:{ return CGSizeMake(ScreenW, FloatKeepInt(100*HWB) ); } break;
        }
    }
    
    return CGSizeZero;

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 5 ; }
    return 1 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 4 ; }
    return 0 ;
}

#pragma mark ===>>> 没有数据时，提示去尝试搜索其他商品
-(void)creatBackBtn{
    
    _nodataBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nodataBtn.backgroundColor = COLORWHITE;
    _nodataBtn.frame = CGRectMake(0, NAVCBAR_HEIGHT, ScreenW, ScreenH-NAVCBAR_HEIGHT);
    _nodataBtn.hidden = YES;
    [self.view addSubview:_nodataBtn];
    UIImageView*jgImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"jinggao"]];
    [_nodataBtn addSubview:jgImg];
    [jgImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_nodataBtn.mas_centerX);
        make.height.offset(112);  make.width.offset(124);
        make.bottom.equalTo(_nodataBtn.mas_centerY);
    }];
    
    UILabel * noFriend = [UILabel labText:@"亲,没有搜索到该商品,试试其它商品吧!" color:Black102 font:15.0];
    noFriend.numberOfLines = 0;
    noFriend.textAlignment = NSTextAlignmentCenter;
    [_nodataBtn addSubview:noFriend];
    [noFriend mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_nodataBtn.mas_centerX);
        make.top.equalTo(_nodataBtn.mas_centerY).offset(20);
    }];
}


#pragma mark === >>> 输入框代理方法
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    ResignFirstResponder
    [self clickSearch];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    ResignFirstResponder
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



//创建顶部view
-(void)addTopTabbarView{
    
    self.title = @"商品搜索结果" ;
    
    UIView * view = [[UIView alloc]init];
    view.backgroundColor=COLORWHITE;
    view.frame=CGRectMake(0,0, ScreenW,NAVCBAR_HEIGHT);
    [self.view addSubview:view];
    
    _searchText = [UITextField textColor:Black51 PlaceHorder:@"请输入商品名称" font:12.5*HWB];
    _searchText.frame = CGRectMake(40, NAVCBAR_HEIGHT-64+26, ScreenW-110, 32);
    _searchText.clearButtonMode = UITextFieldViewModeWhileEditing;
    _searchText.backgroundColor = LinesColor;
    _searchText.delegate = self;
    _searchText.text = _searchStr;
    [_searchText cornerRadius:16];
    [view addSubview:_searchText];
    UIImageView * imgS = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"searchGray"]];
    _searchText.leftView.frame = CGRectMake(0,NAVCBAR_HEIGHT-64, 32, 32);
    imgS.frame = CGRectMake(8, 8, 16, 16);
    [_searchText.leftView addSubview:imgS];
    

    UIButton * searchBtn = [UIButton titLe:@"搜索" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORGROUP font:12*HWB];
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"home_search_bgimg"] forState:(UIControlStateNormal)];
    [searchBtn addTarget:self action:@selector(clickSearch) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn cornerRadius:15];
    [view addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.centerY.equalTo(_searchText.mas_centerY);
        make.height.offset(30); make.width.offset(50);
    }];
    
    UIButton*backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"goback"] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(13.5, 12, 13.5, 22)];
    backBtn.frame=CGRectMake(0, NAVCBAR_HEIGHT-64+20, 44, 44);
    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backBtn];
    
    if (THEMECOLOR==2) { //主题色为浅色
         [searchBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
    }
}

//开始搜索进入搜索历史界面
-(void)clickSearch{
//    if (_searchType==1) {
//        SearchOldVC*oldVC = [SearchOldVC new];
//        oldVC.searchType=1;
//        oldVC.lastStr = _searchText.text ;
//        oldVC.hidesBottomBarWhenPushed = YES ;
//        PushTo(oldVC, YES)
//    }else{
//        PopTo(NO)
//    }
    PopTo(YES)
}

-(void)clickBack{ PopTo(YES);  }


#pragma mark === >>> 置顶按钮
-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
//        _topBtn.frame = CGRectMake(ScreenW-36*HWB-10, ScreenH-36*HWB-10, 36*HWB, 36*HWB);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);  make.bottom.offset(-10);
            make.height.offset(36*HWB); make.width.offset(36*HWB);
        }];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_universalListCltv.contentOffset.y>=0) {
            if (_universalListCltv.contentOffset.y>ScreenH) {
                _topBtn.alpha = 1 ;
            }else{
                if (_universalListCltv.contentOffset.y>ScreenH/2) {
                    _topBtn.alpha = _universalListCltv.contentOffset.y/ScreenH ;
                }else{
                    _topBtn.alpha = 0 ;
                }
            }
    }
}
-(void)clickBackTop{
    [_universalListCltv setContentOffset:CGPointZero animated:YES];
}



-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end




