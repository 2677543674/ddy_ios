//
//  SearchGoodsVC.h
//  DingDingYang
//
//  Created by ddy on 2017/3/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScreeningView.h"
@interface SearchGoodsVC : DDYViewController

@property(nonatomic,copy) NSString * searchStr;

// 0或1淘宝 2:京东 3:拼多多 4:唯品会 5:多麦
@property(nonatomic,assign) NSInteger searchType ; // 搜索类型 0:默认  1:从淘宝的购物车过去的

@end


//#pragma mark ===>>> 淘宝商品搜索商品排序条件
//@interface SearchScreeningView : UIView
//
//
//@end

