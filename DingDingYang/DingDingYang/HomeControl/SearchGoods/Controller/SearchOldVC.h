//
//  SearchOldVC.h
//  DingDingYang
//
//  Created by ddy on 2017/4/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchOldVC : DDYViewController

typedef void (^ResultBk) (NSString* str);

@property (copy, nonatomic) ResultBk resultlb;
@property(nonatomic,copy) NSString * lastStr ;
@property(nonatomic,assign)NSInteger searchType;  //1:淘宝 2:京东 3:拼多多

@end
