//
//  SearchDividingLineCltHead.m
//  DingDingYang
//
//  Created by ddy on 2017/11/28.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SearchDividingLineCltHead.h"

@implementation SearchDividingLineCltHead
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORGROUP ;
        [self dividingLineImg];
    }
    return self;
}

-(UIImageView*)dividingLineImg{
    if (!_dividingLineImg) {
        _dividingLineImg = [[UIImageView alloc]init];
        _dividingLineImg.image = [UIImage imageNamed:@"search_dividingLine"] ;
        _dividingLineImg.contentMode = UIViewContentModeScaleAspectFit ;
        [self addSubview:_dividingLineImg];
        [_dividingLineImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(10*HWB);  make.bottom.offset(-6*HWB);
            make.left.offset(0);      make.right.offset(0);
        }];
    }
    return _dividingLineImg ;
}


@end
