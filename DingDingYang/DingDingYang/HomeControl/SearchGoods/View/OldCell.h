//
//  SearchOldView.h
//  DingDingYang
//
//  Created by ddy on 2017/3/14.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OldCell : UITableViewCell
@property(nonatomic,strong) UILabel * labTitle;
@end


// 热词点击的代理
@protocol HotDelegate <NSObject>
@optional
-(void)selectString:(NSString*)oldStr; //选中的字符串
@end
// 显示热词
@interface HotCell : UITableViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) NSArray * hotAry ;
@property(nonatomic,strong) UICollectionView * cltView ;
@property(nonatomic,assign)id<HotDelegate>deledate;
@end


// 热词lab
@interface HotLabCltCell : UICollectionViewCell
@property(nonatomic,strong) UILabel * hotStrLab;
@end






