//
//  SearchScreeningView.m
//  DingDingYang
//
//  Created by ddy on 2018/9/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "SearchScreeningView.h"

@implementation SearchScreeningView

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        _aryTit = @[@"综合",@"销量",@"价格"];
        [self creatBtn];
    }
    return self;
}


-(void)creatBtn{
    
    for (NSInteger i=0;i<_aryTit.count;i++) {
        CGRect rect = CGRectMake(i*(ScreenW/_aryTit.count), 0, ScreenW/_aryTit.count-1, 38);
        _sbtn = [[ScBtn alloc]initWithFrame:rect];
        _sbtn.tag = 10+i ;
        _sbtn.titLab.text = _aryTit[i];
        _sbtn.selected = NO;
        [_sbtn addTarget:self action:@selector(selectView:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sbtn];
        if (i==0) {
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
        }
    }
}

//按钮事件
-(void)selectView:(ScBtn*)ctr{
    
    //首先清除之前的选中状态（双选按钮不清除）
    if (ctr.tag==12) {
        //价格
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            if (btn.tag!=12) { btn.selected = NO ; }
        }
        if (ctr.selected==YES) {
            ctr.stateType = 2 ;
            ctr.selected = NO ;
            self.select(@"3") ;
        }else{
            ctr.stateType = 1 ;
            ctr.selected = YES ;
            self.select(@"4") ;
        }
        
    }else{
        if (ctr.selected==YES) { return; }
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            btn.selected = NO;
        }
        
        ctr.stateType = 1 ;
        ctr.selected = YES ;
        if (ctr.tag==10) {   self.select(@"") ; } // 综合传空
        if (ctr.tag==11) {  self.select(@"8") ; } // 销量
        
    }
    
}



-(void)selectBlock:(SelectType)type {
    self.select = type ;
}
@end
