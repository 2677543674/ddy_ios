//
//  SearchScreeningView.h
//  DingDingYang
//
//  Created by ddy on 2018/9/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchScreeningView : UIView

@property(nonatomic,strong) NSArray * aryTit; //筛选条件标题
@property(nonatomic,strong) ScBtn * sbtn;

typedef void(^SelectType)(NSString * sortType);
@property(nonatomic,strong) SelectType   select ;

-(void)selectBlock:(SelectType)type ;

@property(nonatomic,copy) NSString * isSelect ;

@end
