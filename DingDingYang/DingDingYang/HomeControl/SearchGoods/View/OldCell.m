//
//  SearchOldView.m
//  DingDingYang
//
//  Created by ddy on 2017/3/14.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "OldCell.h"


#pragma mark ===>>> 显示历史记录的cell
@implementation OldCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        _labTitle=[UILabel labText:@"标题" color:Black51 font:15.0];
        [self addSubview:_labTitle];
        [_labTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(36);  make.height.offset(20); make.right.offset(-30);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        UILabel*lablines=[[UILabel alloc]init];
        lablines.backgroundColor=LinesColor;
        [self addSubview:lablines];
        [lablines mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(20); make.height.offset(1);
            make.bottom.offset(-1); make.right.offset(-20);
        }];
    }
    return self;
}

@end


#pragma mark ===>>> 显示热词tableViewCell
@implementation HotCell

-(void)setHotAry:(NSArray *)hotAry{
    _hotAry = hotAry ;
    [_cltView reloadData];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumInteritemSpacing = 1 ;  layout.minimumLineSpacing = 1 ;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _cltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _cltView.backgroundColor = COLORWHITE ;
        _cltView.dataSource = self ; _cltView.delegate = self ;
        [_cltView registerClass:[HotLabCltCell class] forCellWithReuseIdentifier:@"hotLab_cltcell"] ;
        [self.contentView addSubview:_cltView];
        [_cltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5); make.bottom.offset(-5);
            make.left.offset(15); make.right.offset(-15);
        }];
        
    }
    return self;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float wi = StringSize(_hotAry[indexPath.item], 12*HWB).width+15 ;
    return CGSizeMake(wi, 40);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _hotAry.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HotLabCltCell * hotCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"hotLab_cltcell" forIndexPath:indexPath];
    hotCell.hotStrLab.text = _hotAry[indexPath.item];
    return hotCell ;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     [self.deledate selectString:_hotAry[indexPath.item]];
}

@end

#pragma mark ===>>> 显示热词的子lab
@implementation HotLabCltCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _hotStrLab = [UILabel labText:@"热搜" color:COLORWHITE font:12*HWB];
        _hotStrLab.backgroundColor = Black204 ;
        _hotStrLab.textAlignment = NSTextAlignmentCenter ;
        _hotStrLab.layer.masksToBounds = YES ;
        _hotStrLab.layer.cornerRadius = 5 ;
        [self.contentView addSubview:_hotStrLab];
        [_hotStrLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(2); make.right.offset(-2);
            make.top.offset(8); make.bottom.offset(-8);
        }];
    }
    return self;
}

@end

