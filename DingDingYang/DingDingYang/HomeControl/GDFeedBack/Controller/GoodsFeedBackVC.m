//
//  GoodsFeedBackVC.m
//  DingDingYang
//
//  Created by ddy on 2017/6/9.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "GoodsFeedBackVC.h"
#import "SelectTabViewV.h"
#import "AddImgCell.h"
@interface GoodsFeedBackVC () <UITextViewDelegate,STVDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) UIButton * selectBtn , * upBtn ;
@property(nonatomic,strong) UITextView * fbTxtView ;
@property(nonatomic,strong) UILabel * textLab , * hiddenLab ;
@property(nonatomic,strong) UIImageView * simgView ;
@property(nonatomic,strong) SelectTabViewV * selectV ;
@property(nonatomic,copy) NSString * selectStr ;
@property(nonatomic,assign) NSInteger rowIndex ;
@property(nonatomic,strong) NSMutableArray * biaoqianAry ;
@property(nonatomic,strong) UICollectionView * collectionView ;
@property(nonatomic,strong) NSMutableArray * imgary ;
@property(nonatomic,strong) UIImagePickerController*imgPickerVC;
@property(nonatomic,strong) UILabel * zishuLab ;

@end

@implementation GoodsFeedBackVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.001],
                                 NSForegroundColorAttributeName:[UIColor clearColor]};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    self.automaticallyAdjustsScrollViewInsets = NO ;
    
    self.view.backgroundColor = COLORGROUP ;
    [self addTopTabbarView];
    _selectStr = @"" ;  _rowIndex = 0 ;
    _biaoqianAry = [NSMutableArray array];
    _imgary = [NSMutableArray array];
    
    if (_goodsid.length>0) { [self feedSelect]; } //有商品id代表商品反馈、否则代表用户反馈
    
    [self fbTxtView]; //输入框
    [self collectionView]; //图片选择
    
    _imgPickerVC=[[UIImagePickerController alloc]init];
    _imgPickerVC.delegate = self;
    _imgPickerVC.allowsEditing = YES;
    
    [self upBtn];
    
    //注册键盘高度变化通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

//监控键盘高度变化
-(void)keyboardFrameChange:(NSNotification*)keyboardInfo{
    NSDictionary *KBInfo = [keyboardInfo userInfo];
    NSValue *aValue = [KBInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat  keyBoardY=keyboardRect.origin.y-20;
    
    if (YBOTTOM(_fbTxtView)+NAVCBAR_HEIGHT>keyBoardY) {
        [UIView animateWithDuration:0.26 animations:^{
            self.view.frame=CGRectMake(0,-(YBOTTOM(_fbTxtView)+NAVCBAR_HEIGHT-keyBoardY), ScreenW, ScreenH-NAVCBAR_HEIGHT);
        }];
    }
    
    if (keyboardRect.origin.y > ScreenH-20) {
        [UIView animateWithDuration:0.26 animations:^{
            self.view.frame=CGRectMake(0,NAVCBAR_HEIGHT, ScreenW, ScreenH-NAVCBAR_HEIGHT);
        }];
    }
}


-(void)requestBiaoqian{
    MBShow(@"获取反馈...")
    [NetRequest requestTokenURL:url_get_feedback_quest parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray * ary= NULLARY(nwdic[@"list"]) ;
            [ary enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [_biaoqianAry addObject:obj];
            }];
            [self performSelector:@selector(clickSelect) withObject:self afterDelay:0.6];
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestBiaoqian];
        }];
    }];
    
}

#pragma mark ===>>> 反馈原因选择
-(void)feedSelect{
    
    // 原因筛选点击按钮
    _selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _selectBtn.backgroundColor = COLORWHITE ;
    _selectBtn.layer.cornerRadius = 6 ;
    _selectBtn.frame = CGRectMake(26, 16, ScreenW-52, 42);
    _selectBtn.selected = NO ;
    ADDTARGETBUTTON(_selectBtn, clickSelect)
    [self.view addSubview:_selectBtn];
    
    // 筛选条件显示
    _textLab = [UILabel labText:@" 👉 请选择 👈 " color:Black51 font:14*HWB];
    _textLab.textAlignment = NSTextAlignmentCenter ;
    _textLab.frame = CGRectMake(10, 0, WFRAME(_selectBtn)-40, 42);
    _textLab.userInteractionEnabled = NO ;
    [_selectBtn addSubview:_textLab];
    
    // 筛选条件右侧下拉箭头
    _simgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gdfxiala"]];
    _simgView.frame = CGRectMake(WFRAME(_selectBtn)-30, 17 , 12, 8);
    [_selectBtn addSubview:_simgView];
    
    [self selectV];
    [self requestBiaoqian];
}
#pragma mark ===>>> 反馈条件选择控件
-(SelectTabViewV*)selectV{
    if (!_selectV) {
        if (_goodsid) {
            _selectV = [[SelectTabViewV alloc]initWithFrame:CGRectMake(26, 59, ScreenW-52, 0)];
            _selectV.delegate = self ;
            [self.view addSubview:_selectV];
        }
    }
    return _selectV ;
}
//点击筛选按钮
-(void)clickSelect{
    ResignFirstResponder
    if (_selectV.dataArray.count==0) {  _selectV.dataArray = _biaoqianAry ;  }
    if (_selectStr.length>0) { _selectV.selectRow = _rowIndex ;  }
    
    if (_selectBtn.selected==NO) {
        [UIView animateWithDuration:0.26 animations:^{
            _simgView.transform = CGAffineTransformRotate(_simgView.transform, M_PI);
            _fbTxtView.alpha = 0 ;
            _collectionView.alpha = 0 ;
            _selectV.frame = CGRectMake(26,59, ScreenW-52, ScreenW);
        }];
        _selectBtn.selected = YES ;
    }else{
        [UIView animateWithDuration:0.26 animations:^{
            _simgView.transform = CGAffineTransformIdentity ;
            _fbTxtView.alpha = 1 ;
            _collectionView.alpha = 1 ;
            _selectV.frame = CGRectMake(26,59, ScreenW-52, 0);
        }];
        _selectBtn.selected = NO ;
    }
}

// SelectTabViewV 选择代理
-(void)selectRow:(NSInteger)row str:(NSString*)strings{
    _rowIndex = row ; _selectStr = strings ;
    [UIView animateWithDuration:0.26 animations:^{
        _simgView.transform = CGAffineTransformIdentity ;
        _fbTxtView.alpha = 1 ;
        _collectionView.alpha = 1 ;
        _selectV.frame = CGRectMake(26, 59, ScreenW-52, 0);
    } completion:^(BOOL finished) {
        _textLab.text = _selectStr ;
        _selectBtn.selected = NO ;
    }];
}


#pragma mark ===>>> 反馈原因输入框
-(UITextView*)fbTxtView{
    if (!_fbTxtView) {
        _fbTxtView=[[UITextView alloc]initWithFrame:CGRectMake(26, _goodsid ? 76 : 16 , ScreenW-52, ScreenW/2.5)];
        _fbTxtView.font = [UIFont systemFontOfSize:15*HWB];
        _fbTxtView.layer.masksToBounds=YES ;
        _fbTxtView.layer.cornerRadius = 6 ;
        _fbTxtView.delegate=self;
        [self.view addSubview:_fbTxtView];
        
        _hiddenLab=[UILabel labText:@"请输入反馈意见(最多上传100字)" color:Black204 font:14*HWB];
        _hiddenLab.frame=CGRectMake(5, 9 , WFRAME(_fbTxtView)-10, 15*HWB );
        _hiddenLab.enabled=NO;
        [_fbTxtView addSubview:_hiddenLab];
        
        _zishuLab = [UILabel labText:@"0/100" color:COLORGRAY font:13*HWB];
        _zishuLab.textAlignment = NSTextAlignmentRight ;
        _zishuLab.enabled = NO ;
        _zishuLab.frame = CGRectMake(WFRAME(_fbTxtView)-80, ScreenW/2.5-25, 70, 20);
        [_fbTxtView addSubview:_zishuLab];
    }
    return _fbTxtView ;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length == 0) {
        _hiddenLab.hidden = NO ;
        _zishuLab.text = @"0/100" ;
    }else{
        _hiddenLab.hidden = YES ;
        _zishuLab.text = FORMATSTR(@"%lu/100",(unsigned long)textView.text.length);
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark ===>>> 反馈原因图片选择
-(UICollectionView*)collectionView{
    if (!_collectionView) {
        //表格布局加载图片
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        //设置单元格的大小
        layout.itemSize = CGSizeMake((ScreenW-52)/3,(ScreenW-52)/3);
        layout.minimumInteritemSpacing =0;
        layout.minimumLineSpacing =0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0 );
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = COLORGROUP ;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[AddImgCell class] forCellWithReuseIdentifier:@"addimgcell"];
        [self.view addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_fbTxtView.mas_bottom).offset(15);
            make.left.offset(26); make.right.offset(-26);
            make.height.offset((ScreenW-52)/3) ;
        }];
        
    }
    return _collectionView ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_imgary.count<3) {
        return _imgary.count +1 ;
    }
    return 3 ;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AddImgCell * addimgCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"addimgcell" forIndexPath:indexPath];
    addimgCell.clickRemove.tag = indexPath.row ;
    ADDTARGETBUTTON(addimgCell.clickRemove, clickRemoveImg:)
    
    if (indexPath.row==_imgary.count) {
        addimgCell.isnew = YES ;
    }else{
        addimgCell.dataimg = _imgary[indexPath.row];
    }
    return addimgCell ;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==_imgary.count) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            [self paizhaoORphoto:UIAlertControllerStyleAlert];
        }else{
            [self paizhaoORphoto:UIAlertControllerStyleActionSheet];
        }
    }else{
        [ImgPreviewVC show:self type:0 index:indexPath.item imagesAry:_imgary];
    }
}

#pragma mark ===>>>  调用相册或相机
-(void)paizhaoORphoto:(UIAlertControllerStyle)stylet{
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:15],
                                 NSForegroundColorAttributeName:[UIColor darkGrayColor]};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    UIAlertController*alertVC=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:stylet];
    UIAlertAction*actionCamera=[UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        
        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
        if(!isCamera){
            MBError(@"无法调用相机!");
            return;
        }
        _imgPickerVC.sourceType= UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_imgPickerVC animated:YES completion:nil];
    }];
    
    UIAlertAction*actionPhote=[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        _imgPickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:_imgPickerVC animated:YES completion:nil];
    }];
    UIAlertAction*actionCancle=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *  action) {  }];
    
    [alertVC addAction:actionCamera];
    [alertVC addAction:actionPhote];
    [alertVC addAction:actionCancle];
    [self presentViewController:alertVC animated:YES completion:nil];
    
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        if ([type isEqualToString:@"public.image"]) {
            UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
            UIImageWriteToSavedPhotosAlbum(image, self,@selector(image:didFinishSavingWithError:contextInfo:),nil);
        }
    }else{
        [_imgPickerVC dismissViewControllerAnimated:YES completion:^{
            UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
            [_imgary addObject:image];
            [_collectionView reloadData];
        }];
    }
}
//点击相册页面的取消按钮返回
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [_imgPickerVC dismissViewControllerAnimated:YES completion:nil];
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (!error) {
        [_imgPickerVC dismissViewControllerAnimated:YES completion:^{
            [_imgary addObject:image];
            [_collectionView reloadData];
        }];
    }
}

#pragma mark ===>>> 上传反馈内容
-(UIButton*)upBtn{
    if (!_upBtn) {
        _upBtn = [UIButton titLe:@"提交反馈" bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:15*HWB];
        ADDTARGETBUTTON(_upBtn, upFeedBack)
        [self.view addSubview:_upBtn];
        [_upBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);    make.right.offset(0);
            make.height.offset(44); make.bottom.offset(IS_IPHONE_X?-22:0);
        }];
        
        if (THEMECOLOR==2) { //主题颜色为浅色
            [_upBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        }
        
    }
    return _upBtn ;
}

-(void)upFeedBack{
    
    if ([_textLab.text containsString:@"请选择"]&&_goodsid.length>0) {  SHOWMSG(@"请选择反馈选项!")  return ;  }
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    dic[@"token"] = TOKEN ;
    
    if (_goodsid==nil) {
        if (_fbTxtView.text.length==0) {    SHOWMSG(@"亲，请输入反馈内容后再提交哦!")  return ;  }
        dic[@"type"] = @"1";
    }else{
        dic[@"content"] = _textLab.text ;
        dic[@"goodsId"] = _goodsid ;
    }
    
    if (_fbTxtView.text.length>100) {  SHOWMSG(@"亲，不能大于100字哦!")  return; }
    // 商品反馈和用户反馈都需要此参数
    dic[@"des"] = REPLACENULL(_fbTxtView.text) ;
    
    MBShow(@"正在上传...")
    NSString * url = FORMATSTR(@"%@%@",dns_dynamic_main,url_upload_feedBack);
    [NetRequest urlUpFile:url ptdic:dic pName:@"imgFile" upType:0 data:_imgary success:^(id nwdic) {
        MBHideHUD  MBSuccess(@"反馈成功!")
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            PopTo(YES)
        });
    } progress:^(NSProgress *uploadProgress) {
        
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
    
}

-(void)clickRemoveImg:(UIButton*)removeBtn{
    [_imgary removeObjectAtIndex:removeBtn.tag];
    [_collectionView reloadData];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
//创建顶部view
-(void)addTopTabbarView{
    self.title = _goodsid ? @"商品反馈" : @"用户反馈" ;
}

//-(void)clickBack{   PopTo(YES)  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

