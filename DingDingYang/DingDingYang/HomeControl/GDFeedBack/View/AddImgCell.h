//
//  AddImgCell.h
//  YJZApp
//
//  Created by ddy on 16/8/16.
//  Copyright © 2016年 yons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddImgCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView * ImgV ;
@property(nonatomic,strong) UIButton * clickRemove ;
@property(nonatomic,strong) UIImageView * addImage ;
@property(nonatomic,strong) UILabel * sctpLab ;

@property(nonatomic,strong) UIImage * dataimg ;//设置图片

@property(nonatomic,assign) BOOL isnew ;

@end



