//
//  AddImgCell.m
//  YJZApp
//
//  Created by ddy on 16/8/16.
//  Copyright © 2016年 yons. All rights reserved.
//

#import "AddImgCell.h"

@implementation AddImgCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORGROUP ;
        
        _ImgV=[[UIImageView alloc]init];
        _ImgV.backgroundColor= COLORWHITE;
        _ImgV.contentMode=UIViewContentModeScaleAspectFit;
        _ImgV.layer.masksToBounds = YES ;
        _ImgV.layer.cornerRadius = 4 ;
        [self addSubview:_ImgV];
        [_ImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(10);  make.bottom.offset(0);
            make.left.offset(0);  make.right.offset(-10);
        }];
        
        _addImage=[[UIImageView alloc]init];
        _addImage.image=[UIImage imageNamed:@"gdfaddimg"];
        [_ImgV addSubview:_addImage];
        [_addImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_ImgV.mas_centerX);
            make.bottom.equalTo(_ImgV.mas_centerY).offset(5);
            make.height.offset(20*HWB);
            make.width.offset(20*HWB);
        }];
        
        _sctpLab = [UILabel labText:@"添加图片" color:Black102 font:13*HWB];
        [_ImgV addSubview:_sctpLab];
        [_sctpLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_addImage.mas_bottom).offset(5);
            make.centerX.equalTo(_ImgV.mas_centerX);
        }];
        
        _clickRemove=[UIButton buttonWithType:(UIButtonTypeCustom)];
        _clickRemove.hidden = YES ;
        [_clickRemove setBackgroundImage:[UIImage imageNamed:@"gdfdelete"] forState:(UIControlStateNormal)];
        [self addSubview:_clickRemove];
        [_clickRemove mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(1); make.right.offset(-1);
            make.height.offset(18);make.width.offset(18);
        }];
        
    }return self;
}

-(void)setDataimg:(UIImage *)dataimg{
    _dataimg = dataimg ;
    _ImgV.image = dataimg ;
    _addImage.hidden = YES ;
    _sctpLab.hidden = YES ;
    _clickRemove.hidden = NO ;
}

-(void)setIsnew:(BOOL)isnew{
    _isnew = isnew ;
    if (_isnew) {
        _ImgV.image = nil ;
        _addImage.hidden = NO ;
        _sctpLab.hidden = NO ;
        _clickRemove.hidden = YES ;
    }else{
        _addImage.hidden = YES ;
        _sctpLab.hidden = YES ;
        _clickRemove.hidden = NO ;
    }
}

@end




