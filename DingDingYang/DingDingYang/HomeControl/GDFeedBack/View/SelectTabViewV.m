//
//  SelectTabViewV.m
//  DingDingYang
//
//  Created by ddy on 2017/6/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "SelectTabViewV.h"

@implementation SelectTabViewV

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        self.layer.masksToBounds = YES ;
        self.layer.cornerRadius = 8 ;
        [self slTabV];
        self.clipsToBounds = YES ;
    }
    
    return self ;
}
-(void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray ;
    [_slTabV reloadData];
}

-(UITableView*)slTabV{
    if (!_slTabV) {
        _slTabV = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenW-52, ScreenW) style:UITableViewStylePlain];
        _slTabV.delegate  = self ;
        _slTabV.dataSource = self ;
        _slTabV.backgroundColor = COLORWHITE ;
        _slTabV.rowHeight = 46 ;
        _slTabV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:_slTabV];
        [_slTabV registerClass:[SelectTxtCell class] forCellReuseIdentifier:@"txtcell"];
    }
    return _slTabV ;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count ;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectTxtCell * cell = [tableView dequeueReusableCellWithIdentifier:@"txtcell" forIndexPath:indexPath];
    cell.backgroundColor = COLORWHITE ;
    cell.labTitle.textColor = Black51 ;
    cell.lablines.hidden = NO ;
    cell.labTitle.text = REPLACENULL(_dataArray[indexPath.row]);
    if (_selectRow>0&&indexPath.row == _selectRow - 1 ) {
        cell.backgroundColor = LOGOCOLOR ;
        cell.labTitle.textColor = COLORWHITE ;
        cell.lablines.hidden = YES ;
    }
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate selectRow:indexPath.row+1 str:REPLACENULL(_dataArray[indexPath.row])];
}

-(void)setSelectRow:(NSInteger)selectRow{
    _selectRow = selectRow ;
    [_slTabV reloadData];
}

@end



@implementation SelectTxtCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        self.layer.masksToBounds = YES ;
        self.layer.cornerRadius = 6 ;
        _labTitle=[UILabel labText:@"标题" color:Black51 font:14*HWB];
        [self.contentView addSubview:_labTitle];
        [_labTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10);
            make.right.offset(-10);
            make.top.offset(2);
            make.bottom.offset(-3);
        }];
        
        _lablines=[[UILabel alloc]init];
        _lablines.backgroundColor = COLORGROUP ;
        [self addSubview:_lablines];
        [_lablines mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(10); make.height.offset(1);
            make.bottom.offset(0); make.right.offset(-10);
        }];
    }
    return self ;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}
@end




