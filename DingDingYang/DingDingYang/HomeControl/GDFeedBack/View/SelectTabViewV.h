//
//  SelectTabViewV.h
//  DingDingYang
//
//  Created by ddy on 2017/6/10.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol STVDelegate <NSObject>
-(void)selectRow:(NSInteger)row str:(NSString*)strings ;
@end


@interface SelectTabViewV : UIView <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView * slTabV ;
@property(nonatomic,strong) NSMutableArray * dataArray ;
@property(nonatomic,assign) NSInteger  selectRow ;
@property(nonatomic,assign) id<STVDelegate>delegate ;

@end

@interface SelectTxtCell : UITableViewCell
@property(nonatomic,strong) UILabel * labTitle ;
@property(nonatomic,strong) UILabel * lablines ;
@end






