//
//  HLayerOneCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HLayerOneCltCell1.h"

@implementation HLayerOneCltCell1

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry;
    if (_dataAry.count>0) {
        MClassModel * model = [_dataAry lastObject];
        if (model.styleType==1) {
            [_classImgV sd_setImageWithURL:[NSURL URLWithString:model.nImgs] placeholderImage:[UIImage imageNamed:model.localImgName]];
        }else{
            [_classImgV sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:model.localImgName]];
        }
    }
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _classImgV = [[UIImageView alloc]init];
        _classImgV.backgroundColor=COLORGROUP;
        _classImgV.userInteractionEnabled = YES ;
        _classImgV.contentMode = UIViewContentModeScaleToFill ;
        _classImgV.clipsToBounds = YES ;
        [self.contentView addSubview:_classImgV];
        [_classImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0); make.bottom.offset(0);
        }];
        
        UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOneClass:)];
        [_classImgV  addGestureRecognizer:tap];
    
    }
    return self ;
}

-(void)tapOneClass:(UITapGestureRecognizer*)tabView{
    [PageJump pushAccordingToThe:_dataAry[0]];
}

@end
