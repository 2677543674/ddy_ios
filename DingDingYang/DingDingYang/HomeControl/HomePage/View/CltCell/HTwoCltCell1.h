//
//  HTwoCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

/*首页5个或十个分类*/

#import <UIKit/UIKit.h>

@interface HTwoCltCell1 : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView * classcltView ;

@property(nonatomic,strong) NSArray * dataAry ;

@end


@interface  HTwoClassCell : UICollectionViewCell
@property(nonatomic,strong) UIImageView * imgView ;
@property(nonatomic,strong) UILabel * titLab ;
@property(nonatomic,strong) MClassModel * mcModel ;
@end





