//
//  HLayerTwoCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@class HLayerTwoCltBtn ;

// 两个分类的样式1
@interface HLayerTwoCltCell1 : UICollectionViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
//@property(nonatomic,strong) HLayerTwoCltBtn * hltbtn1 , * hltbtn2 ;
@property(nonatomic,strong) NSArray * dataAry ;
@property(nonatomic,strong) UICollectionView * twoCltv ;
@end


// 两个分类的样式2
@interface HLayerTwoCltCell2 : UICollectionViewCell
@property(nonatomic,strong) HLayerTwoCltBtn * hltbtn1 , * hltbtn2 ;
@property(nonatomic,strong) NSArray * dataAry ;
@end


#pragma mark === >>> 子视图
@interface  HLayerTwoCltBtn : UIButton
@property(nonatomic,strong) UILabel * htitleLab ;
@property(nonatomic,strong) UILabel * ntitleLab ;
@property(nonatomic,strong) UIImageView * gdsImgv ;
@property(nonatomic,strong) MClassModel * mcModel ;
@end


