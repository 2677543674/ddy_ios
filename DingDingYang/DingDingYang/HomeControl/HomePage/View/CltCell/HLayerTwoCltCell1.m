//
//  HLayerTwoCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HLayerTwoCltCell1.h"



#pragma mark === >>> 样式1
@implementation HLayerTwoCltCell1

// 设置数据
-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_twoCltv reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        [self twoCltv];
    }
    return self ;
}

-(UICollectionView*)twoCltv{
    if (!_twoCltv) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        
        layout.itemSize = CGSizeMake(ScreenW/2-1, ScreenW/3) ;
        if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
            layout.itemSize = CGSizeMake(ScreenW/2, ScreenW/3) ;
        }
        layout.minimumLineSpacing = 0 ; layout.minimumInteritemSpacing = 0 ;
        
        _twoCltv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _twoCltv.delegate = self ; _twoCltv.dataSource = self ;
        _twoCltv.backgroundColor = COLORGROUP ;
        _twoCltv.scrollEnabled = NO;
        [self. contentView addSubview:_twoCltv];
        [_twoCltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0);  make.bottom.offset(0);
        }];
        
        [_twoCltv registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];
        
    }
    return _twoCltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    cltCell.styleType = 3 ;
    if (_dataAry.count>indexPath.item) {
        cltCell.mcModel = _dataAry[indexPath.item];
    }else{ [_twoCltv reloadData]; }
   
    return cltCell ;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}


@end




#pragma mark === >>> 样式2  

@implementation HLayerTwoCltCell2

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    if (dataAry.count>=2) {
        _hltbtn1.mcModel = _dataAry[0];
        _hltbtn2.mcModel = _dataAry[1];
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _hltbtn1 = [[HLayerTwoCltBtn alloc]init];
        _hltbtn2 = [[HLayerTwoCltBtn alloc]init];
        
        _hltbtn1.tag = 0 ;
        _hltbtn2.tag = 1 ;
        
        self.contentView.backgroundColor  = RGBA(254, 250, 248,1) ;
        UIButton * imgBtn = [[UIButton alloc]init];
        [imgBtn setTitleColor:LOGOCOLOR forState:(UIControlStateNormal)];
        imgBtn.titleLabel.font = [UIFont systemFontOfSize:14.0*HWB];
        [self.contentView addSubview:imgBtn];
        [imgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(-10);  make.height.offset(40);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [self.contentView addSubview:_hltbtn1];
        [self.contentView addSubview:_hltbtn2];
        
        [_hltbtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(30); make.bottom.offset(0);
            make.left.offset(6); make.right.equalTo(self.contentView.mas_centerX).offset(-3);
        }];
        [_hltbtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(30); make.bottom.offset(0);
            make.right.offset(-6); make.left.equalTo(self.contentView.mas_centerX).offset(3);
        }];
        
        [_hltbtn1 addTarget:self action:@selector(clickPushTo:) forControlEvents:UIControlEventTouchUpInside];
        [_hltbtn2 addTarget:self action:@selector(clickPushTo:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self ;
}

-(void)clickPushTo:(HLayerTwoCltBtn*)btn{
    [PageJump pushAccordingToThe:_dataAry[btn.tag]];
    
}


@end










#pragma mark === >>> 子视图

@implementation HLayerTwoCltBtn

-(void)setMcModel:(MClassModel *)mcModel{
    _mcModel = mcModel ;
    _htitleLab.text = mcModel.title ;
    _ntitleLab.text = mcModel.subtitle;
    [_gdsImgv sd_setImageWithURL:[NSURL URLWithString:mcModel.img] placeholderImage:[UIImage imageNamed:_mcModel.localImgName]];
}

-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        _htitleLab = [UILabel labText:@"官方推荐" color:Black51 font:13.5*HWB];
        [self addSubview:_htitleLab];
       
        _ntitleLab = [UILabel labText:@"高颜值美物" color:Black102 font:10*HWB];
        _ntitleLab.textAlignment = NSTextAlignmentLeft ;
        [self addSubview:_ntitleLab];
        
        _gdsImgv = [[UIImageView alloc]init];
        _gdsImgv.image = PlaceholderImg ;
        _gdsImgv.contentMode = UIViewContentModeScaleAspectFit ;
        _gdsImgv.layer.cornerRadius = 3 ;
        [self addSubview:_gdsImgv];
        [_htitleLab mas_makeConstraints:^(MASConstraintMaker *make) { make.left.offset(8); make.top.offset(6); }];
        [_ntitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_htitleLab.mas_bottom).offset(2);
            make.right.offset(-8); make.left.offset(8);
        }];
        
        [_gdsImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);   make.top.offset(42*HWB);
            make.bottom.offset(-6);   make.left.equalTo(self.mas_centerX).offset(-30);
        }];
        
    }
    return self ;
}

@end















