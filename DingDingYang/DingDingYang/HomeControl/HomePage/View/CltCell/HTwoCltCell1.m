//
//  HTwoCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HTwoCltCell1.h"

@implementation HTwoCltCell1

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_classcltView reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(ScreenW/5, 70*HWB) ;
        layout.minimumInteritemSpacing = 0 ;
        layout.minimumLineSpacing = 0 ;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        _classcltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _classcltView.backgroundColor =COLORWHITE;
        _classcltView.dataSource = self;
        _classcltView.delegate = self;
        [_classcltView registerClass:[HTwoClassCell class] forCellWithReuseIdentifier:@"htwo_classcell"];
        [self.contentView addSubview:_classcltView];
        [_classcltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);   make.left.offset(0);
            make.right.offset(0); make.bottom.offset(0);
        }];
    
    }
    return self ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HTwoClassCell * cltcell = [collectionView dequeueReusableCellWithReuseIdentifier:@"htwo_classcell" forIndexPath:indexPath];
    cltcell.mcModel = _dataAry[indexPath.row];
    return cltcell ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}

@end




@implementation HTwoClassCell

-(void)setMcModel:(MClassModel *)mcModel{
    _mcModel = mcModel ;
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_mcModel.img] placeholderImage:[UIImage imageNamed:_mcModel.localImgName]];
    _titLab.text = _mcModel.title ;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        _imgView=[[UIImageView alloc]init];
        _imgView.contentMode = UIViewContentModeScaleAspectFit ;
        _imgView.clipsToBounds = YES ;
        _imgView.image = [UIImage imageNamed:@"logo"];
        [self.contentView addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.width.offset(ScreenW/5);
            make.top.offset(10);
            make.bottom.offset(-25*HWB);
        }];
        
        _titLab = [UILabel labText:@"官方推荐" color:Black102 font:12.5*HWB];
        _titLab.backgroundColor=COLORWHITE;
        _titLab.adjustsFontSizeToFitWidth=YES;
        [self.contentView addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.offset(-8);
        }];
        
    }
    return self;
}
@end












