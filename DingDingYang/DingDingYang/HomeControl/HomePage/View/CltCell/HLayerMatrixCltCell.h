//
//  HLayerMatrixCltCell.h
//  DingDingYang
//
//  Created by OU on 2019/7/7.
//  Copyright © 2019 jufan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HLayerMatrixCltCell : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,assign) NSInteger rowCount;
@property(nonatomic,strong) NSArray *dataAry ;
@property(nonatomic,strong) UICollectionView *cltv;
@property (nonatomic,strong) UIColor *bgColor; // 背景颜色

@property (assign, nonatomic) NSInteger height;

@end

NS_ASSUME_NONNULL_END
