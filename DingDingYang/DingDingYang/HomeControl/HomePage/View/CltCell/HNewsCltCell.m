//
//  HNewsCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/12/25.
//  Copyright © 2018 ddy.All rights reserved.
//

#import "HNewsCltCell.h"

@implementation HNewsCltCell

// 设置数据
-(void)setMcModel:(MClassModel *)mcModel{
    
    @try {
        _mcModel = mcModel ;
//        if (_labTitle) { _labTitle.text = mcModel.title; }
//        if (_labContent) { _labContent.text = mcModel.subtitle; }
        if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
            [_headImgV sd_setImageWithURL:[NSURL URLWithString:_mcModel.nImgs]];
        }else{
            [_headImgV sd_setImageWithURL:[NSURL URLWithString:_mcModel.img]];
        }
        
    } @catch (NSException *exception) { } @finally { }
    
}

-(void)setMModel:(ModuleModel *)mModel{
    _mModel = mModel;
    [_headImgV sd_setImageWithURL:[NSURL URLWithString:_mModel.img]];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = COLORWHITE;
        
        self.contentView.clipsToBounds = YES;
        
        _bgImgView = [[UIImageView alloc]init];
        _bgImgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_bgImgView];
        [_bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.offset(0);
        }];
        
        
        _lunboDataAry = [NSMutableArray array];
        
//        _headImgV = [UIImageView imgName:@"home_news_headt"];
        _headImgV = [[UIImageView alloc]init];
        _headImgV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_headImgV];
        [_headImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12*HWB);
            make.height.offset(30*HWB);
            make.width.offset(50*HWB);
            make.centerY.equalTo(self.contentView.mas_centerY);
        }];
        
        // 轮播的头条1
        _lunboView1 = [[UIView alloc]init];
        [self.contentView addSubview:_lunboView1];
        [_lunboView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-20*HWB); make.height.offset(30*HWB);
            make.left.equalTo(_headImgV.mas_right).offset(10*HWB);
            make.top.offset(0);
        }];

//        _titleLab1 = [UILabel labText:@" 推荐 " color:LOGOCOLOR font:11*HWB];
//        _titleLab1.backgroundColor = RGBA(252, 239, 229, 1);
//        _titleLab1.textAlignment = NSTextAlignmentCenter;
//        [_titleLab1 cornerRadius:2*HWB];
//        [_lunboView1 addSubview:_titleLab1];
//        [_titleLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.offset(5*HWB); make.height.offset(15*HWB);
//            make.centerY.equalTo(_lunboView1.mas_centerY);
//            make.width.offset(30*HWB);
//        }];
        
        _contentLab1 = [UILabel labText:@"" color:Black102 font:11*HWB];
        _contentLab1.textAlignment = NSTextAlignmentLeft;
        [_lunboView1 addSubview:_contentLab1];
        [_contentLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_titleLab1.mas_right).offset(10*HWB);
            make.left.offset(5*HWB);
            make.centerY.equalTo(_lunboView1.mas_centerY);
            make.right.offset(0);
        }];
        
        
        // 轮播的头条2
        _lunboView2 = [[UIView alloc]init];
        [self.contentView addSubview:_lunboView2];
        [_lunboView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-20*HWB); make.height.offset(30*HWB);
            make.left.equalTo(_headImgV.mas_right).offset(10*HWB);
            make.top.offset(31*HWB);
        }];
        
//        _titleLab2 = [UILabel labText:@" 推荐 " color:LOGOCOLOR font:11*HWB];
//        _titleLab2.backgroundColor = RGBA(252, 239, 229, 1);
//        _titleLab2.textAlignment = NSTextAlignmentCenter;
//        [_titleLab2 cornerRadius:2*HWB];
//        [_lunboView2 addSubview:_titleLab2];
//        [_titleLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.offset(5*HWB); make.height.offset(15*HWB);
//            make.centerY.equalTo(_lunboView2.mas_centerY);
//            make.width.offset(30*HWB);
//        }];
        
        _contentLab2 = [UILabel labText:@"" color:Black102 font:11*HWB];
        [_lunboView2 addSubview:_contentLab2];
        [_contentLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_titleLab2.mas_right).offset(10*HWB);
            make.left.offset(5*HWB);
            make.centerY.equalTo(_lunboView2.mas_centerY);
            make.right.offset(0);
        }];

        _lunboIndex = 0;
        
       
        
        [NetRequest requestType:0 url:url_home_news_list ptdic:nil success:^(id nwdic) {
            NSArray * listAry = NULLARY(nwdic[@"list"]);
            [listAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HNewsModel * model = [[HNewsModel alloc]initWithDic:obj];
                [_lunboDataAry addObject:model];
            }];
            
            if (listAry.count>=2) {
                HNewsModel * model = _lunboDataAry[0];
                _contentLab1.text = model.goodsShowName;
                _timerScroll = [NSTimer scheduledTimerWithTimeInterval:3.8 target:self selector:@selector(changeContent) userInfo:nil repeats:YES];
            }else{
                if (listAry.count==1) {
                    HNewsModel * model = _lunboDataAry[0];
                    _contentLab1.text = model.goodsShowName;
                }else{
                    _contentLab1.text = @"叮叮羊商品头条";
                }
            }
            
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            SHOWMSG(failure)
        }];
        
        UITapGestureRecognizer * clickNewsTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNewsView)];
        [_lunboView1 addGestureRecognizer:clickNewsTap1];
        
        UITapGestureRecognizer * clickNewsTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNewsView)];
        [_lunboView2 addGestureRecognizer:clickNewsTap2];
        
    }
    return self;
}

-(void)changeContent{
    
    @try {
        if (_lunboIndex<_lunboDataAry.count-1) {
            _lunboIndex++;
        }else{
            _lunboIndex = 0;
        }
        
        HNewsModel * model = _lunboDataAry[_lunboIndex];
        if (YFRAME(_lunboView2)==0) {
            _contentLab1.text = model.goodsShowName;
//            _titleLab1.text = model.keyWord;
            [_titleLab1 mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.offset(StringSize(FORMATSTR(@" %@ ",model.goodsShowName), 11*HWB).width);
            }];
            [UIView animateWithDuration:0.8 delay:0.2 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
                [_lunboView1 mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);
                }];
                [_lunboView2 mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(-31*HWB);
                }];
                [self.contentView layoutIfNeeded];
            } completion:^(BOOL finished) {
                [_lunboView2 mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(31*HWB);
                }];
            }];
        
        }else{
            _contentLab2.text = model.goodsShowName;
//            _titleLab2.text = model.goodsShowName;
            [_titleLab2 mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.offset(StringSize(FORMATSTR(@" %@ ",model.goodsShowName), 11*HWB).width);
            }];
            [UIView animateWithDuration:0.8 delay:0.2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [_lunboView2 mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(0);
                }];
                [_lunboView1 mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(-31*HWB);
                }];
                [self.contentView layoutIfNeeded];
            } completion:^(BOOL finished) {
                [_lunboView1 mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(31*HWB);
                }];
            }];
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}


-(void)tapNewsView{
    if (_lunboIndex<_lunboDataAry.count) {
        HNewsModel * model = _lunboDataAry[_lunboIndex];
        if ([model.type isEqualToString:@"1"]) {
            [WebVC pushWebViewUrlOrStr:model.url title:_mcModel.subtitle isSHowType:0];
        }else{
            [PageJump pushToGoodsId:model.goodsId goodsSource:0];
        }
    }
}

@end


@implementation HNewsModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
//        self.des = REPLACENULL(dic[@"des"]);
//        self.keyWord = REPLACENULL(dic[@"keyWord"]);
//        self.goodsId = REPLACENULL(dic[@"goodsId"]);
//        self.platform = REPLACENULL(dic[@"platform"]);
        
       self.goodsId = REPLACENULL(dic[@"goodsId"]);
       self.goodsShowName = REPLACENULL(dic[@"goodsShowName"]);
       self.type = REPLACENULL(dic[@"type"]);
       self.url = REPLACENULL(dic[@"url"]);
        
    }
    return self;
}

@end












