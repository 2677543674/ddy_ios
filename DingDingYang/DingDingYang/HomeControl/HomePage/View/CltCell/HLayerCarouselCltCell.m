//
//  HLayerCarouselCltCell.m
//  DingDingYang
//
//  Created by OU on 2019/7/7.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "HLayerCarouselCltCell.h"

@implementation HLayerCarouselCltCell

// 网络数据调用
-(void)setDataAry:(NSMutableArray *)dataAry{
    
    LOG(@"轮播图model：%@",dataAry);
    
    _dataAry = dataAry ;
    [_imgUrlAry removeAllObjects];
    for (NSInteger i =0; i<dataAry.count; i++) {
        @try {
            MClassModel * model = dataAry[i] ;
            [_imgUrlAry addObject:model.nImgs];
//            [_imgUrlAry addObject:@"http://img95.699pic.com/photo/40141/0737.gif_wh860.gif"];
//            http://img95.699pic.com/photo/40141/0737.gif_wh860.gif
        } @catch (NSException *exception) { } @finally { }
        
    }
    
    if (_imgUrlAry&&_imgUrlAry.count>0) {
        _sdcsView.imageURLStringsGroup = _imgUrlAry;
    }else{
        _sdcsView.imageURLStringsGroup = @[];
    }
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES ;
        _imgUrlAry = [NSMutableArray array];
        self.backgroundColor= COLORGROUP ;
        [self sdcsView];
    }return self;
}

#pragma mark ---> 创建轮播图
-(SDCycleScrollView*)sdcsView{
    if (!_sdcsView) {
        _sdcsView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero imageURLStringsGroup:nil];
        _sdcsView.layer.masksToBounds = YES;
        _sdcsView.layer.cornerRadius = 4;
        _sdcsView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _sdcsView.backgroundColor = COLORGROUP ;
        _sdcsView.delegate = self;
        _sdcsView.currentPageDotColor = LOGOCOLOR ;
        _sdcsView.pageDotColor = COLORWHITE;
        _sdcsView.autoScrollTimeInterval = 3.0;
//        _sdcsView.bannerImageViewContentMode = UIViewContentModeScaleAspectFit;
        _sdcsView.bannerImageViewContentMode = UIViewContentModeScaleToFill;
        
        [self.contentView addSubview:_sdcsView];
        [_sdcsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(10); make.right.offset(-10);
        }];
    }
    return _sdcsView ;
}

//轮播图代理
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    [PageJump pushAccordingToThe:_dataAry[index]];
}

@end
