//
//  HLayerFourCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HLayerFourCltCell1.h"



//*******************************************************************************************************//

#pragma mark ===>>> 四个分类样式1 (从中间平分左边一个，右边上面一个、下面两个)

@implementation HLayerFourCltCell1

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_fourCltv reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        [self fourCltv];
    }
    return self ;
}

-(UICollectionView*)fourCltv{
    if (!_fourCltv) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 1 ;
        layout.minimumInteritemSpacing = 1 ;
        _fourCltv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _fourCltv.delegate = self ; _fourCltv.dataSource = self ;
        _fourCltv.backgroundColor = COLORGROUP ;
        _fourCltv.scrollEnabled=NO;
        [self. contentView addSubview:_fourCltv];
        [_fourCltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0) ; make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [_fourCltv registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];
        
    }
    return _fourCltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
        float bw = FloatKeepInt(ScreenW*0.35), w = FloatKeepInt(ScreenW*0.32), h = FloatKeepInt(ScreenW*0.225);
        switch (indexPath.item) {
            case 1:{ cltCell.frame = CGRectMake(bw, 0, w*2+1, h-1); } break;
            case 2:{ cltCell.frame=CGRectMake(bw, h, w, h); } break;
            case 3:{ cltCell.frame=CGRectMake(bw+w, h, w, h); } break;
            default: { cltCell.frame=CGRectMake(0, 0, bw-1, h*2); } break;
        }
    }else{
        float  wi = ScreenW/5  , hi = ScreenW/1.8 ;
        switch (indexPath.item) {
            case 1:{ cltCell.frame=CGRectMake(wi*2, 0, wi*3-1, hi/2-1); } break;
            case 2:{ cltCell.frame=CGRectMake(wi*2, hi/2, wi*1.5-1, hi/2); } break;
            case 3:{ cltCell.frame=CGRectMake(wi*2+wi*1.5, hi/2, wi*1.5-1, hi/2); } break;
            default: { cltCell.frame=CGRectMake(0, 0, wi*2-1, hi); } break;
        }
    }
   
    if (indexPath.item==1) { cltCell.styleType = 4; } else { cltCell.styleType = 2; }
    if (_dataAry.count>indexPath.item) { cltCell.mcModel = _dataAry[indexPath.row]; }else{ [_fourCltv reloadData]; }
    return cltCell ;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}

@end




//*******************************************************************************************************//

#pragma mark ===>>> 四个分类样式2 (每行两个,平分，等高、等宽)

@implementation HLayerFourCltCell2
-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_fourCltv reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        [self fourCltv];
    }
    return self ;
}

-(UICollectionView*)fourCltv{
    if (!_fourCltv) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        
        layout.itemSize = CGSizeMake(ScreenW/2-0.5, FloatKeepInt(ScreenW/3)) ;
        if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
            layout.itemSize = CGSizeMake(ScreenW/2-0.5, ScreenW*0.2-0.5) ;
        }
        layout.minimumLineSpacing = 1 ; layout.minimumInteritemSpacing = 1 ;
        
        _fourCltv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _fourCltv.delegate = self ; _fourCltv.dataSource = self ;
        _fourCltv.backgroundColor = COLORGROUP ;
        _fourCltv.scrollEnabled=NO;
        [self. contentView addSubview:_fourCltv];
        [_fourCltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0) ; make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [_fourCltv registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];

    }
    return _fourCltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
   
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    cltCell.styleType = 1 ;
    if (_dataAry.count>indexPath.item) { cltCell.mcModel = _dataAry[indexPath.row]; }else{ [_fourCltv reloadData]; }
    return cltCell ;

}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}


@end




//*******************************************************************************************************//

#pragma mark ===>>> 四个分类样式3 (左右各两个，左：上大下小，右：上小下大)

@implementation HLayerFourCltCell3
-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_fourCltv reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORGROUP ;
        [self fourCltv];
    }
    return self ;
}

-(UICollectionView*)fourCltv{
    if (!_fourCltv) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        layout.minimumLineSpacing = 1 ;  layout.minimumInteritemSpacing = 1 ;
        
        _fourCltv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _fourCltv.delegate = self ;
        _fourCltv.dataSource = self ;
        _fourCltv.scrollEnabled=NO;
        _fourCltv.backgroundColor = COLORGROUP ;
        [self. contentView addSubview:_fourCltv];
        [_fourCltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0) ; make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        [_fourCltv registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];

        
    }
    return _fourCltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    if (indexPath.item==0||indexPath.item==3) { cltCell.styleType = 1 ; } else{ cltCell.styleType = 4; }
    if (_dataAry.count>indexPath.item) { cltCell.mcModel = _dataAry[indexPath.item]; } else { [_fourCltv reloadData]; }
    return cltCell ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
        float h1 = FloatKeepInt(ScreenW*0.35) ,h2 = FloatKeepInt(ScreenW*0.19);
        if (indexPath.item==0||indexPath.item==3) {
            return CGSizeMake(ScreenW/2-1, h1);
        }
        return CGSizeMake(ScreenW/2-1, h2);
    }else{
        float hi = FloatKeepInt(ScreenW/4) ;
        if (indexPath.item==0||indexPath.item==3) { // 大的(标题在上、图片在下)
            return CGSizeMake(ScreenW/2-0.5, hi*2-20);
        }
        return CGSizeMake(ScreenW/2-1, hi-10);
    }
}

@end



#pragma mark ===>>> 四个分类的样式，子样式(UIButton)
@implementation FourTwoCltBtn

-(void)setMcModel:(MClassModel *)mcModel{
    _mcModel = mcModel ;
    [_gdsImgv sd_setImageWithURL:[NSURL URLWithString:_mcModel.img] placeholderImage:[UIImage imageNamed:_mcModel.localImgName]];
    _htitleLab.text = _mcModel.title ;
    _ntitleLab.text = _mcModel.subtitle ;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = COLORGROUP ;
        _htitleLab = [UILabel labText:@"官方推荐" color:Black51 font:13.5*HWB];
        [self addSubview:_htitleLab];
        [_htitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(8); make.top.offset(6*HWB);
        }];
        
        _ntitleLab = [UILabel labText:@"高颜值美物" color:Black102 font:11*HWB];
        _ntitleLab.textAlignment = NSTextAlignmentLeft ;
        _ntitleLab.adjustsFontSizeToFitWidth = YES ;
        [self addSubview:_ntitleLab];
        [_ntitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(6*HWB); make.right.offset(-3);
            make.top.equalTo(_htitleLab.mas_bottom).offset(2);
            
        }];
        
        _gdsImgv = [[UIImageView alloc]init];
        _gdsImgv.image = [UIImage imageNamed:@"logo"];
        _gdsImgv.contentMode = UIViewContentModeScaleAspectFit ;
        _gdsImgv.clipsToBounds = YES ;
        [self addSubview:_gdsImgv];
        [_gdsImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-5*HWB);  make.top.offset(38*HWB);
            make.bottom.offset(-6*HWB); make.left.offset(ScreenW/5*3/4-18*HWB);
        }];
    
    }
    return self ;
}

@end



