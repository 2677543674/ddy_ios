//
//  HlayerCvCltCell.h
//  DingDingYang
//
//  Created by ddy on 2018/1/5.
//  Copyright © 2018年 ddy.All rights reserved.
//


#import <UIKit/UIKit.h>

// 子视图所有使用 CollectionView 的子视图 (包括一个正标题，一个副标题，一张主图)

@interface HlayerCvCltCell : UICollectionViewCell

@property(nonatomic,strong) UILabel * labTitle;    // 标题
@property(nonatomic,strong) UILabel * labContent;   // 副标题
@property(nonatomic,strong) UIImageView * imgHead ; // 图片
@property(nonatomic,strong) UIImageView * bigIma ;  // 图片覆盖标题（覆盖整个item）
@property(nonatomic,strong) MClassModel * mcModel ; // model


/**
 布局显示类型:
 1:文字在上图片在下(文字居中、图片居中)     2:文字在上图片在下(文字居左、图片居中)
 3:文字在上图片在下(文字居左上,图片居右下)  4:文字在左图片在右(文字居左、图片居右)

 */
@property(nonatomic,assign) NSInteger styleType ;

@end
