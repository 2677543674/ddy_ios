//
//  TheInterestRateView.m
//  DingDingYang
//
//  Created by ddy on 01/09/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import "TheInterestRateView.h"
#import "PNChart.h"
@implementation TheInterestRateView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        [self addOtherViews];
        [self downloadData];
        self.contentView.backgroundColor = COLORWHITE;
    }
    return self ;
}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        [self addOtherViews];
//        [self downloadData];
//    }
//    return self;
//}

- (void)drawRect:(CGRect)rect {
    //    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(10, 10)];
    self.backgroundColor=[UIColor whiteColor];
    //if ( THEMECOLOR != 2 ) {
    //    CGSize finalSize = CGSizeMake(ScreenW, 100);
    //    CGFloat layerHeight =50;
    //    CAShapeLayer *layer = [[CAShapeLayer alloc]init];
    //    UIBezierPath *bezier = [[UIBezierPath alloc]init];
    //    [bezier  moveToPoint:(CGPointMake(0, -(finalSize.height - layerHeight))) ];
    //    [bezier addLineToPoint:(CGPointMake(0, 0))];
    //    [bezier addLineToPoint:(CGPointMake(finalSize.width, 0))];
    //    [bezier addLineToPoint:(CGPointMake(finalSize.width, -(finalSize.height - layerHeight)))];
    //    [bezier addQuadCurveToPoint:CGPointMake(0,-(finalSize.height - layerHeight)) controlPoint: CGPointMake(finalSize.width / 2, -(finalSize.height - layerHeight) +100)];
    //    layer.path = bezier.CGPath;
    //    layer.fillColor = UIColor.whiteColor.CGColor;
    //
    //    [self.layer addSublayer:layer];
    //}
    
}

-(void)addOtherViews{
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(30, 0, 100, 25)];
    label.text=@"累计收益(元)";
    label.textColor=[UIColor darkGrayColor];
    label.font=[UIFont systemFontOfSize:13];
    [self addSubview:label];
    
    UILabel *label2=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-20, 0, 100,25)];
    label2.text=@"七日收益(元)";
    label2.textColor=[UIColor darkGrayColor];
    label2.font=[UIFont systemFontOfSize:13];
    [self addSubview:label2];
    
    _amountLabel=[[UICountingLabel alloc]initWithFrame:CGRectMake(30, 40, SCREEN_WIDTH/3, 50)];
    _amountLabel.font=[UIFont systemFontOfSize:36];
    _amountLabel.textColor=LOGOCOLOR;
    if ( THEMECOLOR == 2 ) {
        _amountLabel.textColor=[UIColor redColor];
    }
    _amountLabel.format = @"%.2f";
    [self addSubview:_amountLabel];
    
    _lineChart = [[PNChart alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-20, 25, SCREEN_WIDTH/2, 100.0)];
    _lineChart.backgroundColor = [UIColor clearColor];
    
    _lineChart.strokeColor=LOGOCOLOR;
    if ( THEMECOLOR == 2 ) {
        _lineChart.strokeColor=RGBA(235, 35, 50, 1);
    }
    [self addSubview:_lineChart];
}

-(void)downloadData{
    if (TOKEN.length<=0) {
        [_lineChart setXLabels:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7"]];
        [_lineChart setYValues:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
        [_lineChart strokeChart];
        return;
    }
    [NetRequest requestTokenURL:url_seven_earnings parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([[nwdic objectForKey:@"result"] isEqualToString:@"OK"]) {
            
            [_amountLabel countFrom:0.00 to:[nwdic[@"amount"] floatValue] withDuration:1];
            id column= [nwdic objectForKey:@"list"];
            
            NSMutableArray *XLabelArray=[[NSMutableArray alloc]init];
            NSMutableArray *YLabelArray=[[NSMutableArray alloc]init];
            if ([column isKindOfClass:[NSArray class]]) {
                NSArray *array=column;
                for (int i = 0; i < array.count; i++) {
                    NSDictionary *dict=array[i];
                    [XLabelArray addObject:dict[@"day"]];
                    [YLabelArray addObject:[NSString stringWithFormat:@"%@",dict[@"income"]]];
                }
                
                if (YLabelArray.count>0&&XLabelArray.count>0) {
                    [_lineChart setXLabels:XLabelArray];
                    [_lineChart setYValues:YLabelArray];
                    [_lineChart strokeChart];
                }
            }
        }else{
            [_lineChart setXLabels:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7"]];
            [_lineChart setYValues:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
            [_lineChart strokeChart];
        }
        
    } failure:^(NSString *failure) {
        //        SHOWMSG(failure)
        //        [_lineChart setXLabels:@[@"1",@"2",@"3",@"4",@"5"]];
        
    }];
    
    
}

@end

