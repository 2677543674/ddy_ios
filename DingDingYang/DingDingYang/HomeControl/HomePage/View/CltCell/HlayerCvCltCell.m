//
//  HlayerCvCltCell.m
//  DingDingYang
//
//  Created by ddy on 2018/1/5.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "HlayerCvCltCell.h"

@implementation HlayerCvCltCell

// 设置数据
-(void)setMcModel:(MClassModel *)mcModel{
    
    @try {
        _mcModel = mcModel ;
#ifdef DEBUG
        _labTitle.textColor = RGBA(arc4random()%255, arc4random()%255, arc4random()%255, 1);
#else
        if (THEMECOLOR==1) { _labTitle.textColor = LOGOCOLOR;
        }else{ _labTitle.textColor = COLORBLACK; }
#endif
        
        if (_labTitle) { _labTitle.text = mcModel.title; }
        if (_labContent) { _labContent.text = mcModel.subtitle; }
        if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
            [_bigIma sd_setImageWithURL:[NSURL URLWithString:_mcModel.nImgs]];
//            FLAnimatedImage *image=  [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://img95.699pic.com/photo/40141/0737.gif_wh860.gif"]]];
//            FLAnimatedImageView *imageView = [[FLAnimatedImageView alloc]initWithFrame:_bigIma.bounds];
//            imageView.animatedImage = image;
//
//            [_bigIma addSubview:imageView];
            
        }else{
            [_imgHead sd_setImageWithURL:[NSURL URLWithString:_mcModel.img]];
        }
    } @catch (NSException *exception) { } @finally { }

}

- (void)loadAnimatedImageWithURL:(NSURL *const)url completion:(void (^)(FLAnimatedImage *animatedImage))completion
{
    NSString *const filename = url.lastPathComponent;
    NSString *const diskPath = [NSHomeDirectory() stringByAppendingPathComponent:filename];
    
    NSData * __block animatedImageData = [[NSFileManager defaultManager] contentsAtPath:diskPath];
    FLAnimatedImage * __block animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:nil];
    
    if (animatedImage) {
        if (completion) {
            completion(animatedImage);
        }
    } else {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            animatedImageData = data;
            animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData];
            if (animatedImage) {
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(animatedImage);
                    });
                }
                [data writeToFile:diskPath atomically:YES];
            }
        }] resume];
    }
}


// 修改样式布局
-(void)setStyleType:(NSInteger)styleType{
    
    _styleType = styleType ;
    
    @try {
        switch (_styleType) {
            case 1:{ // 文字在上图片在下(文字居中、图片居中)
                
                // 正标题
                [_labTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(6*HWB); make.height.offset(15*HWB);
                    make.centerX.equalTo(self.contentView.mas_centerX);
                }];
                
                // 副标题
                [_labContent mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_labTitle.mas_bottom).offset(2*HWB);
                    make.centerX.equalTo(self.contentView.mas_centerX);
                    make.height.offset(12*HWB);
                }];
                
                // 分类图片
                [_imgHead mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(38*HWB);  make.bottom.offset(-6*HWB);
                    make.left.offset(10);     make.right.offset(-10);
                }];
                
            } break;
                
            case 2:{ // 文字在上图片在下(文字居左、图片居中)
                
                // 正标题
                [_labTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(6*HWB);
                    make.left.offset(6*HWB);
                    make.height.offset(15*HWB);
                }];
                
                // 副标题
                [_labContent mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_labTitle.mas_bottom).offset(2*HWB);
                    make.left.offset(6*HWB);  make.height.offset(12*HWB);
                    make.right.offset(-5);
                }];
                
                // 分类图片
                [_imgHead mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(38*HWB);  make.bottom.offset(-6*HWB);
                    make.left.offset(10);     make.right.offset(-10);
                }];
                
            } break;
                
            case 3:{ // 文字在上图片在下(文字居左上,图片居右下)
                
                // 正标题
                [_labTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(6*HWB);
                    make.left.offset(6*HWB);
                    make.height.offset(15*HWB);
                }];
                
                // 副标题
                [_labContent mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_labTitle.mas_bottom).offset(2*HWB);
                    make.left.offset(6*HWB);  make.height.offset(12*HWB);
                }];
                
                // 分类图片
                [_imgHead mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(38*HWB);  make.bottom.offset(-6*HWB);
                    make.left.equalTo(self.contentView.mas_centerX).offset(0);
                    make.right.offset(-6*HWB);
                }];
                
            } break;
                
            case 4:{ // 文字在左图片在右(文字居左、图片居右)
                
                // 正标题
                [_labTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(6*HWB);  make.left.offset(6*HWB);
                    make.height.offset(15*HWB);
                }];
                
                // 副标题
                [_labContent mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_labTitle.mas_bottom).offset(2*HWB); make.left.offset(6*HWB);
                    //                make.right.equalTo(self.contentView.mas_centerX).offset(0);
                }];
                
                // 分类图片
                [_imgHead mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.offset(6*HWB);  make.bottom.offset(-6*HWB);
                    make.left.equalTo(self.contentView.mas_centerX).offset(0);
                    make.right.offset(-6*HWB);
                }];
                
            } break;
                
            default: break;
        }
    } @catch (NSException *exception) { } @finally { }

}


// 初始化
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.clipsToBounds = YES ;
        self.contentView.backgroundColor = COLORWHITE ;

        if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
            [self bigIma];
        }else{
            [self labTitle];
            [self labContent];
            [self imgHead];
            if (THEMECOLOR==2) { _labTitle.textColor = Black51; }
        }
    }
    return self ;
}

// 主标题
-(UILabel*)labTitle{
    if (!_labTitle) {
        _labTitle = [UILabel labText:@"主题分类" color:LOGOCOLOR font:13*HWB];
        [self.contentView addSubview:_labTitle];
        [_labTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(6*HWB);  make.left.offset(6*HWB);
        }];
    }
    return _labTitle ;
}

// 副标题
-(UILabel*)labContent{
    if (!_labContent) {
        _labContent = [UILabel labText:@"主题分类 主题分类" color:Black102 font:10*HWB];
        _labContent.adjustsFontSizeToFitWidth = YES ;
        [self.contentView addSubview:_labContent];
        [_labContent mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(6*HWB); make.right.offset(-2);
            make.top.equalTo(_labTitle.mas_bottom).offset(1);
        }];
    }
    return _labContent ;
}

// 分类图片
-(UIImageView*)imgHead{
    if (!_imgHead) {
        _imgHead = [[UIImageView alloc]init];
        _imgHead.contentMode = UIViewContentModeScaleAspectFit ;
        _imgHead.clipsToBounds = YES ;
        _imgHead.image = PlaceholderImg ;
        [self.contentView addSubview:_imgHead];
        [_imgHead mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(40*HWB); make.bottom.offset(-6*HWB);
            make.left.offset(10); make.right.offset(-10);
        }];

    }
    return _imgHead ;
}



-(UIImageView *)bigIma{
    if (!_bigIma) {
        _bigIma = [[UIImageView alloc]init];
        _bigIma.clipsToBounds = YES ;
        _bigIma.image = PlaceholderImg ;
        [self.contentView addSubview:_bigIma];
        [_bigIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
    }
    return _bigIma ;
}






@end
