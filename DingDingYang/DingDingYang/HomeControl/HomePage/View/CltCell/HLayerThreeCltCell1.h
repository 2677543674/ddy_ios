//
//  HLayerThreeCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//


/*
   样式1 高为: ScreenW/3*1.1
   样式2 高为: ScreenW/2-20
*/


/*首页三个模块选择*/

#import <UIKit/UIKit.h>

// 三个分类中的样式 1 (三个等高，等宽，标题在上，图片在下)
@interface HLayerThreeCltCell1 : UICollectionViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * threeCltv ;
@property(nonatomic,strong) NSArray * dataAry ;
@end

// 三个分类中的样式 2 (左边一个大的，右边两个小的， 两个小的宽高之和等于大的)
@interface HLayerThreeCltCell2 : UICollectionViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * threeCltv ;
@property(nonatomic,strong) NSArray * dataAry ;
@end




