//
//  HLayerMatrixCltCell.m
//  DingDingYang
//
//  Created by OU on 2019/7/7.
//  Copyright © 2019 jufan. All rights reserved.
//

#import "HLayerMatrixCltCell.h"

@implementation HLayerMatrixCltCell

// 设置数据
-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;

    NSInteger space = 7;

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical ;

    if (self.rowCount == 1) {
        CGFloat width = ScreenW;
        layout.itemSize = CGSizeMake(width, 100) ;
    }else if (self.rowCount == 3){
        CGFloat width = (ScreenW-(self.rowCount+1)*space)/self.rowCount;
        layout.itemSize = CGSizeMake(width, 176) ;
        
        layout.minimumLineSpacing = space ;
        layout.minimumInteritemSpacing = 1 ;
        layout.sectionInset = UIEdgeInsetsMake(7.5, space, 7.5, space);
    }
    else{
        
        CGFloat width = (ScreenW-(self.rowCount+1)*space)/self.rowCount;
        layout.itemSize = CGSizeMake(width, width/2) ;
        
        layout.minimumLineSpacing = space ;
        layout.minimumInteritemSpacing = 1 ;
        layout.sectionInset = UIEdgeInsetsMake(7.5, space, 7.5, space);
    }
        
    [self.cltv reloadData];
    [self.cltv.collectionViewLayout invalidateLayout];
    
    self.cltv.collectionViewLayout = layout;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor colorWithRed:251.0/256.0 green:63.0/256.0 blue:127.0/256.0 alpha:1.0];
        self.rowCount = 2; // 默认两个
        self.height = 0;
        [self cltv];
    }
    return self ;
}

-(UICollectionView*)cltv{
    if (!_cltv) {
        
        NSInteger space = 5;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical ;
        
        CGFloat width = (ScreenW-(self.rowCount+1)*space)/self.rowCount;
        layout.itemSize = CGSizeMake(width, width/2) ;
        
        layout.minimumLineSpacing = space ;
        layout.minimumInteritemSpacing = space ;
        
        layout.sectionInset = UIEdgeInsetsMake(space, space, space, space);
        
        _cltv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _cltv.delegate = self ;
        _cltv.dataSource = self ;
        _cltv.backgroundColor = [UIColor colorWithRed:251.0/256.0 green:63.0/256.0 blue:127.0/256.0 alpha:1.0] ;
        _cltv.scrollEnabled = NO;
        [self. contentView addSubview:_cltv];
        [_cltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);
            make.right.offset(0);
            make.top.offset(0);
            make.bottom.offset(0);
        }];
        
        [_cltv registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];
        
    }
    return _cltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    if (self.rowCount != 1) {
        cltCell.layer.masksToBounds = YES;
        cltCell.layer.cornerRadius = 4;
    }else{
        cltCell.layer.masksToBounds = YES;
        cltCell.layer.cornerRadius = 0;
    }
    cltCell.styleType = 3 ;
    if (_dataAry.count>indexPath.item) {
        cltCell.mcModel = _dataAry[indexPath.item];
    }else{ [_cltv reloadData]; }
    
    return cltCell ;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}

#pragma mark - setters and getters
- (void)setBgColor:(UIColor *)bgColor {
    _bgColor = bgColor;
    _cltv.backgroundColor = _bgColor;
}
@end
