//
//  HLayerOneCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

/*首页单个分类样式*/

#import <UIKit/UIKit.h>

@interface HLayerOneCltCell1 : UICollectionViewCell
@property(nonatomic,strong) UIImageView * classImgV;
@property(nonatomic,strong) NSArray * dataAry ;
@end
