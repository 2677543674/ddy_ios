//
//  HLayerThreeCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HLayerThreeCltCell1.h"

//*******************************************************************************************************//

#pragma mark ===>>> 三个分类中的样式 1 (三个等高，等宽，标题在上，图片在下)

@implementation HLayerThreeCltCell1

// 设置数据
-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_threeCltv reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        [self threeCltv];
    }
    return self ;
}

-(UICollectionView*)threeCltv{
    if (!_threeCltv) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        layout.itemSize = CGSizeMake(ScreenW/3-1, ScreenW/3) ;
        if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
            layout.itemSize = CGSizeMake(FloatKeepInt(ScreenW/3)-1, FloatKeepInt(ScreenW*0.267)) ;
        }
        layout.minimumLineSpacing = 1 ; layout.minimumInteritemSpacing = 1 ;
        
        _threeCltv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _threeCltv.delegate = self ; _threeCltv.dataSource = self ;
        _threeCltv.backgroundColor = COLORGROUP ;
        _threeCltv.scrollEnabled=NO;
        [self. contentView addSubview:_threeCltv];
        [_threeCltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0);  make.bottom.offset(0);
        }];
        
        [_threeCltv registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];
        
    }
    return _threeCltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    cltCell.styleType = 1 ;
    if (_dataAry.count>indexPath.item) {
        cltCell.mcModel = _dataAry[indexPath.row];
    }else{ [_threeCltv reloadData]; }
    return cltCell ;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}


@end



//*******************************************************************************************************//

#pragma mark ===>>> 三个分类中的样式 2 (左边一个大的，右边两个小的， 两个小的宽高之和等于大的)

@implementation HLayerThreeCltCell2
// 设置数据
-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_threeCltv reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        [self threeCltv];
    }
    return self ;
}

-(UICollectionView*)threeCltv{
    if (!_threeCltv) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        layout.minimumLineSpacing = 1 ; layout.minimumInteritemSpacing = 1 ;
        
        _threeCltv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _threeCltv.delegate = self ; _threeCltv.dataSource = self ;
        _threeCltv.backgroundColor = COLORGROUP ;
        _threeCltv.scrollEnabled=NO;
        [self. contentView addSubview:_threeCltv];
        [_threeCltv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0); make.right.offset(0);
            make.top.offset(0);  make.bottom.offset(0);
        }];
        
        [_threeCltv registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];
        
    }
    return _threeCltv ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    if (indexPath.item==0) {  cltCell.styleType = 2 ;  }else{  cltCell.styleType = 4 ; }
    cltCell.mcModel = _dataAry[indexPath.row];
    return cltCell ;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
        if (indexPath.item==0) {
            return CGSizeMake(FloatKeepInt(ScreenW*0.35)-1, FloatKeepInt(ScreenW*0.4));
        }else{
            return CGSizeMake(FloatKeepInt(ScreenW*0.65), FloatKeepInt(ScreenW*0.2)) ;
        }
    }else{
        if (indexPath.item==0) {
            return CGSizeMake(ScreenW/2-0.5, ScreenW/2-20);
        }else{
            return CGSizeMake(ScreenW/2-0.5, (ScreenW/2-20)/2-1) ;
        }
    }
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}


@end



