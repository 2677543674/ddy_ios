//
//  HLyaerFvieCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>


/*
 样式1 高为: FloatKeepInt(ScreenW/1.8))
 样式2 高为: FloatKeepInt(70*HWB)*3+2
 样式3 高为: FloatKeepInt(ScreenW/3-10)*2-10
 */


// 五个选项样式1 (从左到右个数：1、2、2)
@interface HLyaerFvieCltCell1 : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * itmeCltVew ;
@property(nonatomic,strong) NSArray * dataAry ;
@end

// 五个选项样式2
@interface HLyaerFvieCltCell2 : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * itmeCltVew ;
@property(nonatomic,strong) NSArray * dataAry ;
@end

// 五个选项样式3
@interface HLyaerFvieCltCell3 : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * itmeCltVew ;
@property(nonatomic,strong) NSArray * dataAry ;
@end



