//
//  OneLayerCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

/*首页轮播图*/

#import <UIKit/UIKit.h>

#import "SDCycleScrollView.h"
#import "BHModel.h"

@interface HOneCltCell1 : UICollectionViewCell <SDCycleScrollViewDelegate>

@property(nonatomic,strong) SDCycleScrollView * sdcsView ;

@property(nonatomic,strong) NSMutableArray * imgUrlAry ; //图片url数组
@property(nonatomic,strong) NSMutableArray * dataAry ; //所有数据

@property(nonatomic,strong) NSMutableArray * urlAry ; //详情页(图片)
@property(nonatomic,strong) NSArray * imgAry ; //本地图片

@property(nonatomic,copy)void(^changeImgCallBack)(UIColor *bgColor);

@end
