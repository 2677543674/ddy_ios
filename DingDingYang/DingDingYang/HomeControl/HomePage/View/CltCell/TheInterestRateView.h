//
//  TheInterestRateView.h
//  DingDingYang
//
//  Created by ddy on 01/09/2017.
//  Copyright © 2017 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChart.h"
#import "UICountingLabel.h"
@interface TheInterestRateView : UICollectionViewCell
@property(nonatomic,strong) PNChart * lineChart;
@property(nonatomic,strong) UICountingLabel *amountLabel;

-(void)downloadData;
@end
