//
//  HNewsCltCell.h
//  DingDingYang
//
//  Created by ddy on 2018/12/25.
//  Copyright © 2018 ddy.All rights reserved.
//

/*
 2-6-1-0
 2-1-1-1
 6-1-1-1
 */


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HNewsCltCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView * bgImgView;
@property(nonatomic,strong) NSMutableArray * lunboDataAry;
@property(nonatomic,strong) NSTimer * timerScroll;
@property(nonatomic,assign) NSInteger lunboIndex;
@property(nonatomic,copy)   NSString * luoboPage;



@property(nonatomic,strong) UIImageView * headImgV;
@property(nonatomic,strong) UIView * lunboView1, * lunboView2;
@property(nonatomic,strong) UILabel * titleLab1, * titleLab2;
@property(nonatomic,strong) UILabel * contentLab1, * contentLab2;

@property(nonatomic,strong) MClassModel * mcModel ; // model
@property(nonatomic,strong) ModuleModel * mModel;
@end


@interface HNewsModel : NSObject
//@property(nonatomic,copy) NSString * des;
//@property(nonatomic,copy) NSString * goodsId;
//@property(nonatomic,copy) NSString * keyWord;
//@property(nonatomic,copy) NSString * platform;

@property(nonatomic,copy) NSString * goodsId;
@property(nonatomic,copy) NSString * goodsShowName;
@property(nonatomic,copy) NSString * type;
@property(nonatomic,copy) NSString * url;

-(instancetype)initWithDic:(NSDictionary*)dic;
@end



NS_ASSUME_NONNULL_END
