//
//  OneLayerCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/2.
//  Copyright © 2017年 ddy.All rights reserved.
//



#import "HOneCltCell1.h"

@interface HOneCltCell1 ()<UIScrollViewDelegate>

@property (nonatomic,assign) NSInteger currentIndex;

@property (assign, nonatomic) CGFloat currentIndexContentOffsetX;

@end

@implementation HOneCltCell1

// 网络数据调用
-(void)setDataAry:(NSMutableArray *)dataAry{
    
    LOG(@"轮播图model：%@",dataAry);
    
    _dataAry = dataAry ;
    [_imgUrlAry removeAllObjects];
    for (NSInteger i =0; i<dataAry.count; i++) {
        BHModel * model = dataAry[i] ;
        // 宝拉甄选、省见、惠得、叮叮羊微集、花生LK、叮当叮当
        if (PID==10106||PID==10025||PID==10110||PID==10107||PID==888||PID==10102) {
            if (model.bigImg.length==0) {
                [_imgUrlAry addObject:model.img];
            }else{ [_imgUrlAry addObject:model.bigImg]; }
        }else{
            [_imgUrlAry addObject:model.img];
        }
    }
    
    if (_imgUrlAry&&_imgUrlAry.count>0) {
        _sdcsView.imageURLStringsGroup = _imgUrlAry;
    }else{
        _sdcsView.imageURLStringsGroup = @[];
    }
    
    // 显示默认第一张图的背景色
    NSArray *dataArray = NULLARY(_dataAry);
    if (dataArray.count > _currentIndex) {
        BHModel *model = _dataAry[_currentIndex];
        if (model.bgColor) {
            if (self.changeImgCallBack) {
                self.changeImgCallBack(model.bgColor);
            }
        }
    }
    
    self.currentIndexContentOffsetX = (ScreenW-40) * dataAry.count * 100 * 0.5;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES ;
        _imgUrlAry = [NSMutableArray array];
        self.backgroundColor = COLORCLEAR ;
        self.contentView.backgroundColor = COLORCLEAR;
        _currentIndex = 0;
        [self sdcsView];
     
    }return self;
}


#pragma mark ---> 创建轮播图
-(SDCycleScrollView*)sdcsView{
    if (!_sdcsView) {
        _sdcsView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero imageURLStringsGroup:nil];
        _sdcsView.layer.masksToBounds = YES;
        _sdcsView.layer.cornerRadius = 4;
        _sdcsView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _sdcsView.backgroundColor = COLORCLEAR;
        _sdcsView.delegate = self;
        _sdcsView.currentPageDotColor = LOGOCOLOR;
        _sdcsView.pageDotColor = COLORWHITE;
        _sdcsView.autoScrollTimeInterval = 3.0;
        _sdcsView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_sdcsView];
        [_sdcsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(16);
            make.bottom.offset(-11);
            make.left.offset(20);
            make.right.offset(-20);
        }];
    }
    return _sdcsView ;
}

//轮播图代理
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (_dataAry.count>index) {
        if (TopNavc!=nil) {
            BHModel * model = _dataAry[index];
            switch (model.todo) {
                    
                case  1:{
                    GoodsDetailsVC * detailVc = [[GoodsDetailsVC alloc]init] ;
                    detailVc.hidesBottomBarWhenPushed = YES ;
                    detailVc.dmodel = model.goodsMd ;
                    [TopNavc pushViewController:detailVc  animated:YES] ;
                } break;
                    
                case  2:{
                    if ([model.link containsString:@"http"]) {
                        if ([model.link containsString:@"weixin.qq.com"]) {
                            if (CANOPENAlipay==NO&&CANOPENWechat==NO) {  return ;  }
                        }
                        WebVC * web = [[WebVC alloc]init];
                        web.urlWeb = model.link ;
                        web.nameTitle = @"详情页" ;
                        web.hidesBottomBarWhenPushed = YES ;
                        [TopNavc pushViewController:web animated:YES];
                    }
                } break;
                    
                case  3:{
                    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:model.link]]) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.link]];
                    }else{ SHOWMSG(@"无法打开链接!") }
                } break;
                    
                default:{
                    if ([model.link hasPrefix:@"http"]) {
                        WebVC * web = [[WebVC alloc]init];
                        web.urlWeb = model.link ;
                        web.nameTitle = @"详情页" ;
                        web.hidesBottomBarWhenPushed = YES ;
                        [TopNavc pushViewController:web animated:YES];
                    }
                }
                    break;
            }
        }
    }
 
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    _currentIndex = index;
    
    self.currentIndexContentOffsetX = cycleScrollView.mainView.contentOffset.x;
    BHModel *model = _dataAry[index];
    if (self.changeImgCallBack) {
        self.changeImgCallBack(model.bgColor);
    }
}

-(void)cycleScrollviewDidScroll:(UIScrollView *)mainView toIndex:(int)toIndex{
    CGFloat width = CGRectGetWidth(mainView.bounds);
    
    CGFloat p = fabs(mainView.contentOffset.x - self.currentIndexContentOffsetX) / width;
    
    NSInteger count = self.dataAry.count;
    
    NSInteger nextIndex = mainView.contentOffset.x > self.currentIndexContentOffsetX ? _currentIndex + 1 + count : _currentIndex - 1 + count;
    
    if (p > 1 || p < 0) {
        if (p - (int)p == 0) {
            return;
        }
        
        nextIndex = mainView.contentOffset.x > self.currentIndexContentOffsetX ? nextIndex + (int)(p + count): nextIndex + (int)(p + count);
    }
    
    nextIndex = nextIndex % _dataAry.count;
    
    BHModel *currentModel = _dataAry[_currentIndex];
    BHModel *nextModel = _dataAry[nextIndex];
    
    CGFloat startR,startG,startB,startA,endR,endG,endB,endA;
    
    [currentModel.bgColor getRed:&startR green:&startG blue:&startB alpha:&startA];
    [nextModel.bgColor getRed:&endR green:&endG blue:&endB alpha:&endA];
    
    if (p > 1 || p < 0) {
        p = p - (int)p;
    }
    
    CGFloat R = startR + (endR - startR) * p;
    CGFloat G = startG + (endG - startG) * p;
    CGFloat B = startB + (endB - startB) * p;
    CGFloat A = startA + (endA - startA) * p;
    
    if (self.changeImgCallBack) {
        self.changeImgCallBack([UIColor colorWithRed:R green:G blue:B alpha:A]);
    }
}


@end









