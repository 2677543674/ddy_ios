//
//  HLyaerFvieCltCell1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HLyaerFvieCltCell1.h"

@implementation HLyaerFvieCltCell1

#pragma mark ===>>> 五个分类样式1 (从左到右个数：1、2、2)

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_itmeCltVew reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) { [self itmeCltVew]; }
    return self ;
}

-(UICollectionView*)itmeCltVew{
    if (!_itmeCltVew) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 1 ;
        layout.minimumInteritemSpacing = 1 ;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        
        _itmeCltVew = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _itmeCltVew.delegate = self ;
        _itmeCltVew.dataSource = self ;
        _itmeCltVew.scrollEnabled=NO;
        _itmeCltVew.backgroundColor = COLORGROUP ;
        [self.contentView addSubview:_itmeCltVew];
        [_itmeCltVew mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
    
        [_itmeCltVew registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];

    }
    return _itmeCltVew ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    cltCell.styleType = 2 ;
    if (_dataAry.count>indexPath.item) { cltCell.mcModel = _dataAry[indexPath.row]; }else{ [_itmeCltVew reloadData]; }
    return cltCell ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
        float w = FloatKeepInt(ScreenW*0.35); float h = FloatKeepInt(ScreenW*0.45);
        if (indexPath.item==0) { return CGSizeMake(w-1, h); }
        return CGSizeMake((ScreenW-w)/2-1, h/2) ;
    }else{
        float wi = FloatKeepInt(ScreenW/5); float hi = FloatKeepInt(ScreenW/1.8);
        if (indexPath.item==0) {  return CGSizeMake(wi*2-2, hi); }
        return CGSizeMake(wi*3/2-1, hi/2-0.5) ;
    }
}

@end



#pragma mark ===>>> 五个分类样式2 (从左到右: 2、3布局)

@implementation HLyaerFvieCltCell2

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_itmeCltVew reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) { [self itmeCltVew]; }
    return self ;
}

-(UICollectionView*)itmeCltVew{
    if (!_itmeCltVew) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 1 ;
        layout.minimumInteritemSpacing = 1 ;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        
        _itmeCltVew = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _itmeCltVew.delegate = self ;   _itmeCltVew.dataSource = self ;
        _itmeCltVew.backgroundColor = COLORGROUP ;
        _itmeCltVew.scrollEnabled=NO;
        [self.contentView addSubview:_itmeCltVew];
        [_itmeCltVew mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [_itmeCltVew registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];
        

    }
    return _itmeCltVew ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    if (indexPath.item==0) { cltCell.styleType = 2 ; } else { cltCell.styleType = 4; }

    if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
        switch (indexPath.item) {
            case 1:{ cltCell.frame = CGRectMake(0, ScreenW*0.45, ScreenW*0.5-1, ScreenW*0.225); } break;
            case 2:{ cltCell.frame = CGRectMake(ScreenW*0.5, 0, ScreenW*0.5-1, ScreenW*0.225-1); } break;
            case 3:{ cltCell.frame = CGRectMake(ScreenW*0.5, ScreenW*0.225, ScreenW*0.5-1, ScreenW*0.225); } break;
            case 4:{ cltCell.frame = CGRectMake(ScreenW*0.5, ScreenW*0.45, ScreenW*0.5-1, ScreenW*0.225); } break;
            default:{ cltCell.frame = CGRectMake(0, 0, ScreenW*0.5-1, ScreenW*0.45-1); } break;
        }
    }
    if (_dataAry.count>indexPath.item) {
        cltCell.mcModel = _dataAry[indexPath.row];
    }else{ [_itmeCltVew reloadData]; }
    return cltCell ;

}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
        if (indexPath.item==0) {
            return CGSizeMake(FloatKeepInt(ScreenW*0.35)-1, FloatKeepInt(ScreenW*0.4));
        }else if(indexPath.item==1||indexPath.item==2){
            return CGSizeMake(FloatKeepInt(ScreenW*0.65), FloatKeepInt(ScreenW*0.2)-1) ;
        }else{
            return CGSizeMake(FloatKeepInt(ScreenW*0.5)-1, FloatKeepInt(ScreenW*0.2)-1) ;
        }
    }else{
        NSInteger cellh = FloatKeepInt(70*HWB) ;
        if (indexPath.item==0) { return CGSizeMake(ScreenW/2-1, cellh*2+1); }
        return  CGSizeMake(ScreenW/2-1, cellh);
    }
    
}

@end



#pragma mark ===>>> 五个分类样式3 (从上到下：上2、下3)

@implementation HLyaerFvieCltCell3

-(void)setDataAry:(NSArray *)dataAry{
    _dataAry = dataAry ;
    [_itmeCltVew reloadData];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) { [self itmeCltVew]; }
    return self ;
}

-(UICollectionView*)itmeCltVew{
    if (!_itmeCltVew) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0 ;
        layout.minimumInteritemSpacing = 0 ;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical ;
        
        _itmeCltVew = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _itmeCltVew.delegate = self ;   _itmeCltVew.dataSource = self ;
        _itmeCltVew.backgroundColor = COLORGROUP ;
        _itmeCltVew.scrollEnabled=NO;
        [self.contentView addSubview:_itmeCltVew];
        
        [_itmeCltVew registerClass:[HlayerCvCltCell class] forCellWithReuseIdentifier:@"cv_clt_cell"];

        [_itmeCltVew mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];

    }
    return _itmeCltVew ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count ;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HlayerCvCltCell * cltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cv_clt_cell" forIndexPath:indexPath];
    if (indexPath.item<2) { cltCell.styleType = 4 ; } else {  cltCell.styleType = 1 ;  }
    if (_dataAry.count>indexPath.item) { cltCell.mcModel = _dataAry[indexPath.row]; } else { [_itmeCltVew reloadData]; }
    return cltCell ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushAccordingToThe:_dataAry[indexPath.row]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([NSUDTakeData(@"Home_ifNew") integerValue]==1) {
        if (indexPath.item<2) { return CGSizeMake(ScreenW/2, FloatKeepInt(ScreenW/3)); }
        return CGSizeMake(ScreenW/3, FloatKeepInt(ScreenW*0.267)) ;
    }else{
        float wi = FloatKeepInt((ScreenW-3)/3)  ; float hi = FloatKeepInt(ScreenW/3)-10 ;
        if (indexPath.item<2) {  return CGSizeMake(FloatKeepInt(ScreenW/2)-1, hi-10); }
        return CGSizeMake(wi, hi-1) ;
    }
}


@end








