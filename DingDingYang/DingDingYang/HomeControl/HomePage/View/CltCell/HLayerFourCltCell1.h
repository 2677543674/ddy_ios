//
//  HLayerFourCltCell1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>



/*
 样式1 高为: ScreenW/1.8
 样式2 高为: FloatKeepInt(ScreenW/3)*2+2
 样式3 高为: FloatKeepInt(ScreenW/4)*3-29
 */




@class FourTwoCltBtn ;

// 四个分类样式1 (从中间平分左边一个，右边上面一个、下面两个)
@interface HLayerFourCltCell1 : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
//@property(nonatomic,strong) FourTwoCltBtn * ftcltBtn1 ,  * ftcltBtn2 , * ftcltBtn3 , * ftcltBtn4 ;
@property(nonatomic,strong) UICollectionView * fourCltv ;
@property(nonatomic,strong) NSArray * dataAry ;
@end

// 四个分类样式2 (每行两个,平分，等高、等宽)
@interface HLayerFourCltCell2 : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * fourCltv ;
@property(nonatomic,strong) NSArray * dataAry ;
@end

// 四个分类样式3 (交叉对称，交叉对称的两个大小一样)
@interface HLayerFourCltCell3 : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView * fourCltv ;
@property(nonatomic,strong) NSArray * dataAry ;
@end


// 四个样式中的第一种样式，CollectionView用不了，使用自定义btn (已经可以用)
@interface  FourTwoCltBtn : UIButton
@property(nonatomic,strong) UILabel * htitleLab ;
@property(nonatomic,strong) UILabel * ntitleLab ;
@property(nonatomic,strong) UIImageView * gdsImgv ;
@property(nonatomic,strong) MClassModel * mcModel ;
@end








