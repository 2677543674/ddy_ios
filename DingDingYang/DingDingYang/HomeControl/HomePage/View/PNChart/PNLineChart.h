//
//  PNLineChart.h
//  PNChartDemo
//
//  Created by kevin on 11/7/13.
//  Copyright (c) 2013年 kevinzhow. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UICountingLabel.h"

#define chartMargin     10
#define xLabelMargin    10
#define yLabelMargin    10
#define yLabelHeight    11

@interface PNLineChart : UIView

/**
 * This method will call and troke the line in animation
 */

-(void)strokeChart;

@property (strong, nonatomic) NSArray * xLabels;

@property (strong, nonatomic) NSArray * yLabels;

@property (strong, nonatomic) NSArray * yValues;

@property (nonatomic) CGFloat xLabelWidth;

@property (nonatomic) CGFloat yValueMax;

@property (nonatomic,strong) CAShapeLayer * chartLine;

@property (nonatomic, strong) UIColor * strokeColor;


@property (nonatomic, strong)CAShapeLayer *colorLayer;
@property (nonatomic, strong)UIBezierPath *bezierPath;

@property (nonatomic, strong) UICountingLabel * myLabel;


@end
