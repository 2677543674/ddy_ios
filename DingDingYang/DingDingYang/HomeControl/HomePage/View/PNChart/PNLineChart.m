//
//  PNLineChart.m
//  PNChartDemo
//
//  Created by kevin on 11/7/13.
//  Copyright (c) 2013年 kevinzhow. All rights reserved.
//

#import "PNLineChart.h"
#import "PNColor.h"
#import "PNChartLabel.h"

@implementation PNLineChart

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        self.clipsToBounds = YES;
		_chartLine = [CAShapeLayer layer];
		_chartLine.lineCap = kCALineCapRound;
		_chartLine.lineJoin = kCALineJoinBevel;
		_chartLine.fillColor   = [[UIColor whiteColor] CGColor];
		_chartLine.lineWidth   = 2.0;
		_chartLine.strokeEnd   = 0.0;
		[self.layer addSublayer:_chartLine];
        

    }
    
    return self;
}

-(void)setYValues:(NSArray *)yValues
{
    _yValues = yValues;
    [self setYLabels:yValues];
}

-(void)setYLabels:(NSArray *)yLabels
{
    CGFloat max = 0;
    for (NSString * valueString in yLabels) {
        CGFloat value = [valueString floatValue];
        if (value > max) {
            max = value;
        }
        
    }
    
//    Min value for Y label
    if (max <= 0) {
        max = 1;
    }
    
    _yValueMax = (CGFloat)max;
    
//    float level = max /5.0;
//	
//    NSInteger index = 0;
//	NSInteger num = [yLabels count] + 1;
//	while (num > 0) {
//		CGFloat chartCavanHeight = self.frame.size.height - chartMargin - 30.0 ;
//		CGFloat levelHeight = chartCavanHeight /5.0;
//		PNChartLabel * label = [[PNChartLabel alloc] initWithFrame:CGRectMake(0.0,chartCavanHeight - index * levelHeight + (levelHeight - yLabelHeight) , 20.0, yLabelHeight)];
//		[label setTextAlignment:NSTextAlignmentRight];
//		label.text = [NSString stringWithFormat:@"%.3f",level * index];
//		[self addSubview:label];
//        index +=1 ;
//		num -= 1;
//	}

}

-(void)setXLabels:(NSArray *)xLabels
{
    _xLabels = xLabels;
    _xLabelWidth = (self.frame.size.width - chartMargin - 30.0 - ([xLabels count] -1) * xLabelMargin)/7.0;
    
    for (NSString * labelText in xLabels) {
        NSInteger index = [xLabels indexOfObject:labelText];
        PNChartLabel * label = [[PNChartLabel alloc] initWithFrame:CGRectMake(index * (xLabelMargin + _xLabelWidth) + 30.0, self.frame.size.height - 30.0, _xLabelWidth+8, 20.0)];
        [label setTextAlignment:NSTextAlignmentCenter];
        label.text = labelText;
        [self addSubview:label];
        
        UIView *lineView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, self.bounds.size.height)];
        lineView.center=CGPointMake(label.center.x-4, label.center.y-10-self.bounds.size.height/2);
        lineView.backgroundColor=RGBA(200, 200, 200, 0.3);
        [self.layer addSublayer:lineView.layer];
        
    }
    
}

-(void)setStrokeColor:(UIColor *)strokeColor
{
	_strokeColor = strokeColor;
	_chartLine.strokeColor = [strokeColor CGColor];
}

-(void)strokeChart
{
    //        CGSize finalSize = CGSizeMake(CGRectGetWidth(self.frame), 100);
    //        CGFloat layerHeight =50;
    _colorLayer = [[CAShapeLayer alloc]init];
    _bezierPath = [[UIBezierPath alloc]init];
   
    //    [bezier addLineToPoint:(CGPointMake(finalSize.width, 0))];
    //    [bezier addLineToPoint:(CGPointMake(finalSize.width, -(finalSize.height - layerHeight)))];
    //        colorLayer.path = bezierPath.CGPath;
    //        colorLayer.fillColor = UIColor.whiteColor.CGColor;
    //        [self.layer addSublayer:colorLayer];
    
    
    UIBezierPath *progressline = [UIBezierPath bezierPath];
     [progressline addLineToPoint:CGPointMake(30+_xLabelWidth/2,0)];
    CGFloat firstValue = [[_yValues objectAtIndex:0] floatValue];
    
    CGFloat xPosition = (xLabelMargin + _xLabelWidth)   ;
    
    CGFloat chartCavanHeight = self.frame.size.height - chartMargin * 2 - 30.0;
    
    float grade = (float)firstValue / (float)_yValueMax;
    [progressline moveToPoint:CGPointMake( xPosition, chartCavanHeight - grade * chartCavanHeight + 20.0)];
    [_bezierPath  moveToPoint:(CGPointMake(6 * xPosition  + 30.0+ _xLabelWidth /2.0,chartCavanHeight+ 20.0 )) ];
    [_bezierPath addLineToPoint:(CGPointMake( (xLabelMargin + _xLabelWidth), chartCavanHeight+20.0))];
    [_bezierPath addLineToPoint:(CGPointMake( xPosition, chartCavanHeight - grade * chartCavanHeight + 20.0))];
    [progressline setLineWidth:3.0];
    [progressline setLineCapStyle:kCGLineCapRound];
    [progressline setLineJoinStyle:kCGLineJoinRound];
    
    
    CAKeyframeAnimation *frameAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    frameAnimation.calculationMode = kCAAnimationPaced;
    frameAnimation.fillMode = kCAFillModeForwards;
    frameAnimation.removedOnCompletion = NO;
    frameAnimation.duration = 1.75;
    //Lets loop continuously for the demonstration
    frameAnimation.repeatCount = 1;
    

    

    NSMutableArray *valueArray=[[NSMutableArray alloc]init];
    NSValue *v1 = [NSValue valueWithCGPoint:CGPointMake(xPosition, chartCavanHeight - grade * chartCavanHeight+10)];
    [valueArray addObject:v1];
   
    NSInteger index = 0;
    for (NSString * valueString in _yValues) {
        CGFloat value = [valueString floatValue];
        
        float grade = (float)value / (float)_yValueMax;
        if (index != 0) {
            
            [progressline addLineToPoint:CGPointMake(index * xPosition  + 30.0+ _xLabelWidth /2.0, chartCavanHeight - grade * chartCavanHeight + 20.0)];
            [progressline moveToPoint:CGPointMake(index * xPosition + 30.0 + _xLabelWidth /2.0, chartCavanHeight - grade * chartCavanHeight + 20.0 )];
            [_bezierPath addLineToPoint:CGPointMake(index * xPosition  + 30.0+ _xLabelWidth /2.0, chartCavanHeight - grade * chartCavanHeight + 20.0)];
           
            
            NSValue *v1 = [NSValue valueWithCGPoint:CGPointMake(index * xPosition + 30.0 + _xLabelWidth /2.0, chartCavanHeight - grade * chartCavanHeight+10)];
            [valueArray addObject:v1];
            
            
          
            [progressline stroke];
            
        }
        
        index += 1;
    }

    _chartLine.path = progressline.CGPath;
	if (_strokeColor) {
		_chartLine.strokeColor = [_strokeColor CGColor];
	}else{
		_chartLine.strokeColor = [PNGreen CGColor];
	}
    
    // 设置position属性的每一帧要改变的值
    frameAnimation.values = valueArray;
    
//    if (!_myLabel) {
         _myLabel=[[UICountingLabel alloc]initWithFrame:CGRectMake(xPosition,  chartCavanHeight - grade * chartCavanHeight, 25, 15)];
    _myLabel.backgroundColor=LOGOCOLOR;
if ( THEMECOLOR == 2 ) {
    _myLabel.backgroundColor=RGBA(235, 35, 50, 1);
}
    _myLabel.font=[UIFont systemFontOfSize:10];
    _myLabel.textColor=[UIColor whiteColor];
//    _myLabel.text=_yValues[0];
    _myLabel.format=@"%.2f";
         [self addSubview:_myLabel];
//    }
   
    
    [_myLabel countFrom:_yValueMax to:[[_yValues lastObject]  floatValue]withDuration:2];
    
   
//    [_myLabel.layer addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew || NSKeyValueChangeOldKey context:nil];
   
   
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 2.0;
    pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    pathAnimation.autoreverses = NO;
    
        _colorLayer.path = _bezierPath.CGPath;
    _colorLayer.fillColor = RGBA(255, 234, 225, 0.5).CGColor;
    [self.layer addSublayer:_colorLayer];
    [_colorLayer addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
    

    _chartLine.strokeEnd = 1.0;
    _chartLine.fillColor = LOGOCOLOR.CGColor;
    

    [_chartLine addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
    
    [_myLabel.layer addAnimation:frameAnimation forKey:@"moveTheSquare"];
    
    
    CAKeyframeAnimation* widthAnim = [CAKeyframeAnimation animationWithKeyPath:@"width"];
    
    NSArray* widthValues = [NSArray arrayWithObjects:@1.0, @10.0, @5.0, @30.0, @0.5, @15.0, @2.0, @50.0, @10.0, nil];
    
    widthAnim.values = widthValues;
    
    
    //    CABasicAnimation* widthAnim = [CABasicAnimation animationWithKeyPath:@"borderWidth"];
    
    //    widthAnim.toValue=@20.0;
    
    //    widthAnim.fromValue=@0.0;
    
    
    
    // Animation 2
    
    CAKeyframeAnimation* colorAnim = [CAKeyframeAnimation animationWithKeyPath:@"borderColor"];
    
    NSArray* colorValues = [NSArray arrayWithObjects:(id)[UIColor greenColor].CGColor,
                            
                            (id)[UIColor redColor].CGColor, (id)[UIColor blueColor].CGColor,  nil];
    
    colorAnim.values = colorValues;
    
    
    // Animation group
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    
    group.animations = [NSArray arrayWithObjects:colorAnim, widthAnim, nil];
    
    group.duration = 5.0;
    
    group.removedOnCompletion=NO;
    
    group.fillMode=kCAFillModeForwards;
    
    //    group.repeatCount=MAXFLOAT;
    
    group.autoreverses=YES;
    
    [_colorLayer addAnimation:group forKey:@"BorderChanges"];

//    NSMutableArray *keyTimes;
//    NSMutableArray *values;
//    CGFloat duration;
//    CABasicAnimation *gradientAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
//    gradientAnimation.fromValue = [NSNumber numberWithFloat:0.0f] ;
//    gradientAnimation.toValue = [NSNumber numberWithFloat:1.0f];gradientAnimation.duration = 3.0;
////    gradientAnimation.repeatCount = Float.infinity;
//
//
//
//    layer.addAnimation(animation, forKey: "addLayerAnimationMargin")

    
//        CGSize finalSize = self.size;
//        CGFloat layerHeight = finalSize.height;
        CAShapeLayer *layer = [CAShapeLayer layer];
        UIBezierPath *bezier = [UIBezierPath bezierPath];
        [bezier addLineToPoint:CGPointMake(30+_xLabelWidth/2,0)];
    NSInteger index2 = 0;
    for (NSString * valueString in _yValues) {
        CGFloat value = [valueString floatValue];
        
        float grade = (float)value / (float)_yValueMax;
        if (index2 != 0) {
            
            [bezier addLineToPoint:CGPointMake(index2 * xPosition  + 30.0+ _xLabelWidth /2.0, chartCavanHeight - grade * chartCavanHeight + 20.0)];
            
            [bezier moveToPoint:CGPointMake(index2 * xPosition + 30.0 + _xLabelWidth /2.0, chartCavanHeight - grade * chartCavanHeight + 20.0 )];
            
            
            [bezier stroke];
            [bezier fill];
            
        }
        
        index2 += 1;
    }

        layer.path = bezier.CGPath;
        layer.fillColor = LOGOCOLOR.CGColor;
        [self.layer addSublayer:layer];
    
       
    
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
//       if ([keyPath isEqualToString:@"frame"]) {
//        NSLog(@"我的视图frame:%@",_myLabel);
//           
//           
//          
//    }
//}
//- (void)dealloc {
////    [_myLabel.layer removeObserver:self forKeyPath:@"frame"];
//}


@end
