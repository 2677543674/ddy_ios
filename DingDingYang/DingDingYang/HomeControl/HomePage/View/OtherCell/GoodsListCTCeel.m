//
//  GoodsListCTCeel.m
//  DingDingYang
//
//  Created by ddy on 2017/6/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "GoodsListCTCeel.h"

@implementation GoodsListCTCeel


-(void)setAllDic:(NSMutableDictionary *)allDic{
    
    _allDic = allDic ;
    
    NSMutableArray * ary = [NSMutableArray arrayWithArray:_allDic[@"goodslist"]];
    
    _parameter = [NSMutableDictionary dictionaryWithDictionary:_allDic[@"pdic"]];
    
//    _screenViewHead.isSelect = _parameter[@"sortType"];
    if (ary.count>0) {
        _pageNum = [_parameter[@"st"] integerValue]+1;
        _classListAry = [NSMutableArray arrayWithArray:ary];
         [_classTypeCltView reloadData];
    }else{
//        _classListAry = nil ;
        _classListAry = [NSMutableArray array];
        [_classTypeCltView reloadData];
        [self loadClassHomeData];
    }
}

-(void)setScrollY:(NSString *)scrollY{
    _scrollY = scrollY ;
    if (_classListAry.count>0&&[_scrollY floatValue]>=0) {
        [_classTypeCltView setContentOffset:CGPointMake(0,[_scrollY floatValue])];
    }
}

#pragma mark === >>> 初始化
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _pageNum = 1 ;
        self.contentView.backgroundColor = COLORWHITE ;
        
        _goodsListStyle = GoodsListStyle ;
        
        [self classTypeCltView];
        [self topBtn];
    }
    return self ;
}

#pragma mark === >>> 加载数据
-(void)loadClassHomeData{
    [_classTypeCltView.mj_footer resetNoMoreData];
    _pageNum = 2 ;    _parameter[@"st"] = @"1" ;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES ;
    [NetRequest requestTokenURL:url_goods_list2 parameter:_parameter success:^(NSDictionary *nwdic) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
        if (_classListAry.count>0) {
            [_classListAry removeAllObjects];
            [_classTypeCltView reloadData];
        }
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry=nwdic[@"list"];
            if (lAry.count>0) {
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_classListAry addObject:model];
                }];
                
                NSDictionary * pjcs = @{@"pdic":_parameter,@"goodslist":_classListAry};
                [self.delegate gctCell:self nowClassTypeAllData:pjcs];
                
            }
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        [_classTypeCltView.mj_header endRefreshing];
        [_classTypeCltView reloadData];
        
    } failure:^(NSString *failure) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO ;
        [_classTypeCltView.mj_header endRefreshing];
        SHOWMSG(failure)
    }];
    
}

//加载更多
-(void)loadMoreData{
    USERENABLEDNO
    _parameter[@"st"] = FORMATSTR(@"%ld",(long)_pageNum);
    [NetRequest requestTokenURL:url_goods_list2 parameter:_parameter success:^(NSDictionary *nwdic) {
        USERENABLEDYES
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry = NULLARY( nwdic[@"list"] ) ;
            if (lAry.count>0) {
                _pageNum++;
                
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_classListAry addObject:model];
                }];
                [_classTypeCltView.mj_footer endRefreshing];
                
                NSDictionary * pjcs = @{@"pdic":_parameter,@"goodslist":_classListAry};
                [self.delegate gctCell:self nowClassTypeAllData:pjcs];
                
            }else{
                [_classTypeCltView.mj_footer endRefreshingWithNoMoreData];
            }
            
        }else{
            [_classTypeCltView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        
        [_classTypeCltView reloadData];
        
    } failure:^(NSString *failure) {
        USERENABLEDYES
        [_classTypeCltView.mj_footer endRefreshing];
        SHOWMSG(failure)
    }];
    
}

#pragma mark === >>> 排序条件
//-(ScreeningView*)screenViewHead{
//    if (!_screenViewHead) {
//        _screenViewHead = [[ScreeningView alloc]initWithFrame:CGRectMake(0, 0 , ScreenW, 38)];
//        [_screenViewHead selectBlock:^(NSString *sortType) {
//            _parameter[@"sortType"] = sortType ;
//            [_classTypeCltView.mj_header beginRefreshing];
//        }];
//        [self addSubview:_screenViewHead] ;
//    }
//    return _screenViewHead;
//}


#pragma mark === >>> UICollectionView 创建

-(UICollectionView*)classTypeCltView{
    if (!_classTypeCltView) {
        UICollectionViewFlowLayout * layoutUnvsl = [[UICollectionViewFlowLayout alloc]init];
        _classTypeCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layoutUnvsl];
        _classTypeCltView.delegate = self ;
        _classTypeCltView.dataSource = self ;
        _classTypeCltView.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_classTypeCltView];
        [_classTypeCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        [_classTypeCltView registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"goods_listclt_cell1"];
        [_classTypeCltView registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"goods_listclt_cell2"];
        [_classTypeCltView registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"goods_listclt_cell3"];
        [_classTypeCltView registerClass:[GoodsListCltCell4   class] forCellWithReuseIdentifier:@"goods_listclt_cell4"];
        [_classTypeCltView registerClass:[GoodsListCltCell5   class] forCellWithReuseIdentifier:@"goods_listclt_cell5"];
        [_classTypeCltView registerClass:[GoodsListCltCell6   class] forCellWithReuseIdentifier:@"goods_listclt_cell6"];
        [_classTypeCltView registerClass:[GoodsListCltCell7   class] forCellWithReuseIdentifier:@"goods_listclt_cell7"];
        [_classTypeCltView registerClass:[GoodsListCltCell10  class] forCellWithReuseIdentifier:@"goods_listclt_cell10"];

        //下拉刷新
        __weak GoodsListCTCeel*SelfView=self;
        _classTypeCltView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadClassHomeData)];

        _classTypeCltView.mj_footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreData];
        }];

    }
    return _classTypeCltView ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _classListAry.count ;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    // 部分序号命名和后台不一致，并且有的是某些app单独使用的
    switch (_goodsListStyle) {
        case 2:{
            GoodsListCltCell3 * goodsListClt3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell3" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt3.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt3;
        } break;
            
        case 3:{
            GoodsListCltCell2 * goodsListClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell2" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt2.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt2;
        } break;
            
        case 4:{
            GoodsListCltCell4 * goodsListClt4 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell4" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt4.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt4;
        } break;
            
        case 5:{
            GoodsListCltCell5 * goodsListClt5 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell5" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt5.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt5;
        } break;
            
        case 6:{
            GoodsListCltCell6 * goodsListClt6 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell6" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt6.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt6 ;
        } break;
            
        case 7:{
            GoodsListCltCell7 * goodsListClt7 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell7" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt7.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt7 ;
        } break;
            
        case 8:{
            GoodsListCltCell10 * goodsListClt10 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell10" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt10.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt10 ;
        } break;
            
        default:{
            GoodsListCltCell1 * goodsListClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell1" forIndexPath:indexPath];
            if (_classListAry.count>indexPath.item) { goodsListClt1.goodsModel = _classListAry[indexPath.item]; } else { [_classTypeCltView reloadData]; }
            return goodsListClt1;
        } break;
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [PageJump pushToGoodsDetail:_classListAry[indexPath.item] pushType:0];
}

#pragma mark === >>> UICollectionViewLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    // 部分序号命名和后台不一致
    switch (_goodsListStyle) {
        case 2:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+70*HWB); } break;
        case 4:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
        case 5:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+(role_state==0?70*HWB:90*HWB)); } break;
        case 6:{ return CGSizeMake(ScreenW, 116*HWB); } break;
        case 7:{ return CGSizeMake(ScreenW, 104*HWB); } break;
        case 8:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
        default:{ return CGSizeMake(ScreenW, FloatKeepInt(100*HWB) ); } break;
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 5 ; }
    return 1 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 4 ; }
    return 0 ;
}

#pragma mark === >>>  置顶按钮
-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
//        _topBtn.frame = CGRectMake(ScreenW-36*HWB-10, HFRAME(_classTypeCltView)-36*HWB-10, 36*HWB, 36*HWB);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.contentView addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);  make.bottom.offset(-10);
            make.height.offset(36*HWB); make.width.offset(36*HWB);
        }];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (_classTypeCltView.contentOffset.y>=0) {
        if (_classTypeCltView.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_classTypeCltView.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _classTypeCltView.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
        NSString * stry = [NSString stringWithFormat:@"%f",_classTypeCltView.contentOffset.y];
        [self.delegate nowCell:self offsetY:stry];
    }
}

-(void)clickBackTop{
    [_classTypeCltView setContentOffset:CGPointZero animated:YES];
}

@end



@implementation  HomeSTCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _textLab = [UILabel labText:@"标题分类" color:Black51 font:14*HWB];
        _textLab.textAlignment = NSTextAlignmentCenter ;
        _textLab.layer.masksToBounds = YES ;
        _textLab.layer.cornerRadius = 5 ;
        [self.contentView addSubview:_textLab];
        [_textLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(5); make.right.offset(-5);
            make.top.offset(6);  make.bottom.offset(-6);
        }];
    }
    return self ;
}

@end


@implementation  EmptyCtCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORCLEAR ;
    }
    return self ;
}
@end











