//
//  BlankCell.m
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "BlankCell.h"

@implementation BlankCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _imgUrlAry = [NSMutableArray array];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        self.backgroundColor= COLORGROUP ;
        [self addScrollView];
    }return self;
}

#pragma mark ---> 创建轮播图
-(void)addScrollView{
    _csView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0,0, ScreenW,ScreenW/2.8) imageURLStringsGroup:nil];
    _csView.backgroundColor = COLORGROUP ;
    _csView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    _csView.delegate = self;
    _csView.currentPageDotColor = LOGOCOLOR;
    _csView.pageDotColor = COLORWHITE;
    _csView.autoScrollTimeInterval = 3.0;
    [self.contentView addSubview:_csView];
}

//轮播图代理
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    BHModel*model = _dataAry[index];
    [self.deledate didSelectIndexDataModel:model];
}


-(void)setDataAry:(NSMutableArray *)dataAry{
    _dataAry = dataAry ;
    [_imgUrlAry removeAllObjects];
    for (NSInteger i =0; i<dataAry.count; i++) {
        BHModel*model = dataAry[i] ;
        [_imgUrlAry addObject:model.img];
    }
    _csView.imageURLStringsGroup=_imgUrlAry;
}
-(void)setUrlAry:(NSMutableArray *)urlAry{
    _urlAry=urlAry;

    _csView.imageURLStringsGroup=_urlAry;
}

-(void)setImgAry:(NSArray *)imgAry{
    _imgAry = imgAry ;
    _csView.localizationImageNamesGroup = imgAry ;
}



- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
