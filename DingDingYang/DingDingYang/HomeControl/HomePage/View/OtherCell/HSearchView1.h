//
//  HSearchView1.h
//  DingDingYang
//
//  Created by ddy on 2017/8/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSearchView1 : UIView

@property(nonatomic,strong) UIButton * homeBtn ;
@property(nonatomic,strong) UIButton * searchBtn ;
@property(nonatomic,strong) UIImageView * searchImgV ;
@property(nonatomic,copy) NSString * searchBarStyle ;
@property(nonatomic,assign) NSInteger  ZHCJType ; // 种草君首页消息按钮的跳转状态
@property(nonatomic,copy) NSString * ZHCJweburl ; // 种草君首页url
@end
