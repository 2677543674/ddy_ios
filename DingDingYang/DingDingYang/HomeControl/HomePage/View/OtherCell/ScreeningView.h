//
//  ScreeningView.h
//  DingDingYang
//
//  Created by ddy on 2017/3/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class ScBtn;
@interface ScreeningView : UIView

@property(nonatomic,strong) NSArray * aryTit; //筛选条件标题

@property(nonatomic,strong) ScBtn * sbtn;

typedef void(^SelectType)(NSString * sortType);
@property(nonatomic,strong) SelectType   select ;
-(void)selectBlock:(SelectType)type ;

@property(nonatomic,copy) NSString * isSelect ;

@end


@interface ScBtn : UIButton

@property(nonatomic,strong) UIImageView * bgImgV ;  // 主题色为浅色时显示的底部背景
@property(nonatomic,strong) UILabel     * titLab ;  // 标题文字
@property(nonatomic,strong) UIImageView * imgView ; // 箭头图片
@property(nonatomic,strong) UIImageView * linesIV ; // 下划线

// 通过该属性修改按钮显示的状态
@property(nonatomic,assign) NSInteger stateType; // (0:原状态、1:高亮箭头朝下、2:高亮箭头朝上)

@end

















