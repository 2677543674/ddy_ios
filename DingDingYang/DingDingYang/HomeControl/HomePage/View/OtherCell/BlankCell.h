//
//  BlankCell.h
//  DingDingYang
//
//  Created by ddy on 2017/3/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "BHModel.h"
@protocol ScrollSDDeledate <NSObject>
@optional
-(void)didSelectIndexDataModel:(BHModel*)model;
@end

@interface BlankCell : UITableViewCell<SDCycleScrollViewDelegate>

@property(nonatomic,strong)SDCycleScrollView*csView;

@property(nonatomic,assign)id<ScrollSDDeledate>deledate;

@property(nonatomic,strong)NSMutableArray * imgUrlAry;
@property(nonatomic,strong)NSMutableArray * dataAry ;

@property(nonatomic,strong) NSMutableArray * urlAry ; //详情页(图片)
@property(nonatomic,strong)NSArray*imgAry;//本地图片
@end
