//
//  ScreeningView.m
//  DingDingYang
//
//  Created by ddy on 2017/3/6.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ScreeningView.h"

@implementation ScreeningView

-(void)dealloc{
    
}

-(void)setIsSelect:(NSString*)isSelect{
    _isSelect = isSelect ;
    for (ScBtn*btn in self.subviews) {
        btn.selected = NO ;
        btn.stateType = 0;
    }
    switch ([_isSelect integerValue]) {
        case 0 :{
            _sbtn = [self viewWithTag:9];
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
        }break ;
        case 3:{//券后价升序
            _sbtn = (ScBtn*)[self viewWithTag:10];
            _sbtn.selected = YES ;
            _sbtn.stateType = 2 ;
        } break;
        case 4:{//券后价降序
            _sbtn = (ScBtn*)[self viewWithTag:10];
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
            
        } break;
        case 5:{//优惠券金额升序
            _sbtn = (ScBtn*)[self viewWithTag:11];
            _sbtn.selected = YES ;
            _sbtn.stateType = 2 ;
        } break;
        case 6:{//优惠券金额降序
            _sbtn = (ScBtn*)[self viewWithTag:11];
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
        } break;
        case 7:{ //奖励金额排序
            _sbtn = (ScBtn*)[self viewWithTag:12];
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
            
        } break;
        case 8:{//销量排序
            if (role_state==0&&TOKEN.length>0) {
                _sbtn = (ScBtn*)[self viewWithTag:12];
                _sbtn.selected = YES ;
                _sbtn.stateType = 1 ;
            }else{
                _sbtn = (ScBtn*)[self viewWithTag:13];
                _sbtn.selected = YES ;
                _sbtn.stateType = 1 ;
            }
        } break;
            
        default:{
            _sbtn = [self viewWithTag:9];
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
        } break;
    }
}


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        if (role_state==0) {
            _aryTit = @[@"综合",@"券后价",@"优惠券",@"销量"];
        }else{
            _aryTit = @[@"综合",@"券后价",@"优惠券",@"奖励",@"销量"];
        }
        [self creatBtn];
    }
    return self;
}

//三个按钮单选
-(void)creatBtn{
    
    for (NSInteger i=0;i<_aryTit.count;i++) {
        CGRect rect = CGRectMake(i*(ScreenW/_aryTit.count), 0, ScreenW/_aryTit.count-1, 38);
        _sbtn = [[ScBtn alloc]initWithFrame:rect];
        _sbtn.tag = 9+i ;
        _sbtn.titLab.text = _aryTit[i];
        _sbtn.selected = NO;
        [_sbtn addTarget:self action:@selector(selectView:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sbtn];
        if (i==0) {
            _sbtn.selected = YES ;
            _sbtn.stateType = 1 ;
        }
    }
}

//按钮事件
-(void)selectView:(ScBtn*)ctr{
    
    // 券后价格 、优惠券 这两个有升序和降序，其他只有一个排序
    
    //首先清除之前的选中状态 （ 双选按钮清除样式，但不清除按钮的selected事件 ）
    
    if (ctr.tag==10) {
        // 券后价格
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            if (btn.tag!=10) { btn.selected = NO ; }
        }
        if (ctr.selected==YES) {
            ctr.stateType = 2 ;
            ctr.selected = NO ;
            self.select(@"3") ;
        }else{
            ctr.stateType = 1 ;
            ctr.selected = YES ;
            self.select(@"4") ;
        }
        
    }else if(ctr.tag==11){
        //优惠券
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            if (btn.tag!=11) { btn.selected = NO ; }
        }
        if (ctr.selected==YES) {
            ctr.stateType = 2 ;
            ctr.selected = NO ;
            self.select(@"5") ;
        }else{
            ctr.stateType = 1 ;
            ctr.selected = YES ;
            self.select(@"6") ;
        }
        
    }else{
        if (ctr.selected==YES) { return; }
        for (ScBtn*btn in self.subviews) {
            btn.stateType = 0;
            btn.selected = NO;
        }
        ctr.stateType = 1 ;
        ctr.selected = YES ;
        if (role_state==0&&TOKEN.length>0) {
            //消费者展示三个去掉奖励（销量）
            if (ctr.tag==9)  { self.select(@"") ; }
            if (ctr.tag==12) { self.select(@"8") ; }
        }else{
            //代理商或联合运营商（奖励、销量）
            if (ctr.tag==9)  { self.select(@"") ; }
            if (ctr.tag==12) { self.select(@"7") ; }
            if (ctr.tag==13) { self.select(@"8") ; }
        }
    }
    
}




-(void)selectBlock:(SelectType)type {
    self.select = type ;
}


@end


@implementation ScBtn

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        
        if (THEMECOLOR==2) {
            _bgImgV = [[UIImageView alloc]init];
            _bgImgV.backgroundColor = COLORWHITE ;
            _bgImgV.layer.masksToBounds = YES ;
            _bgImgV.layer.cornerRadius = 6 ;
            [self addSubview:_bgImgV];
            [_bgImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.height.offset(26);
                make.width.offset(StringSize(@"综合", 13*HWB).width + 26 );
            }];
        }
        
        _titLab = [UILabel labText:@"条件" color:Black102 font:12.5*HWB];
        _titLab.userInteractionEnabled = NO;
        [self addSubview:_titLab];
        [_titLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX).offset(-6*HWB);
            make.centerY.equalTo(self.mas_centerY).offset(-1);
        }];
        
        _imgView = [[UIImageView alloc]init];
        _imgView.image = [UIImage imageNamed:@"xiala1"];
        [self addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titLab.mas_right).offset(1);
            make.centerY.equalTo(self.mas_centerY).offset(-1);
            make.height.offset(5*HWB); make.width.offset(9*HWB);
        }];
        
        
        _linesIV = [[UIImageView alloc]init];
        _linesIV.backgroundColor = LOGOCOLOR ;
        _linesIV.alpha = 0 ;
        _linesIV.layer.masksToBounds = YES;
        _linesIV.layer.cornerRadius = 1 ;

        [self addSubview:_linesIV];
    
        [_linesIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(-4); make.centerX.equalTo(self.mas_centerX);
            make.height.offset(1); make.width.offset(StringSize(@"综合", 13*HWB).width + 10 );
        }];

        if (THEMECOLOR==2) { //主题色为浅色
            _linesIV.hidden  = YES ;
            self.layer.masksToBounds = YES ;
            self.layer.cornerRadius = 2 ;
        }

    }
    return self;
}
//改变状态
-(void)setStateType:(NSInteger)stateType{
   
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (stateType==0) { // 未选中状态
    
            float wi = StringSize(self.titLab.text, 13*HWB).width + 10 ;
            
            [UIView animateWithDuration:0.2 animations:^{
                
                self.titLab.textColor = Black102 ;
                self.imgView.image = [UIImage imageNamed:@"xiala1"];
                
                [_linesIV.superview layoutIfNeeded];
                [_linesIV mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.offset(-4); make.centerX.equalTo(self.mas_centerX);
                    make.height.offset(1); make.width.offset(wi);
                }];
                _linesIV.alpha = 0;
                self.imgView.transform = CGAffineTransformIdentity;
                
                if (THEMECOLOR==2) { //主题色为浅色
                    _bgImgV.backgroundColor = COLORWHITE ;
                    [_bgImgV.superview layoutIfNeeded];
                    [_bgImgV mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.equalTo(self.mas_centerX);
                        make.centerY.equalTo(self.mas_centerY);
                        make.height.offset(26);  make.width.offset(wi+16);
                    }];
                }
            }];
        }
        
        if (stateType==1) { // 选中状态、箭头朝下
            [UIView animateWithDuration:0.2 animations:^{
                self.titLab.textColor = LOGOCOLOR ;
                if (THEMECOLOR==1) { self.imgView.image = [[UIImage imageNamed:@"xiala2"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
                }else{  self.imgView.image = [UIImage imageNamed:@"xiala2"]; }
    
                _linesIV.alpha = 1;
                self.imgView.transform = CGAffineTransformIdentity;
                
                if (THEMECOLOR==2) { //主题色为浅色
                    self.titLab.textColor = COLORBLACK ;
                    _bgImgV.backgroundColor = LOGOCOLOR ;
                }
            }];
        }
        
        if (stateType==2) { // 选中状态、箭头朝上

            [UIView animateWithDuration:0.2 animations:^{
                _linesIV.alpha = 1;
                self.imgView.transform = CGAffineTransformRotate(_imgView.transform, M_PI);
                
                self.titLab.textColor = LOGOCOLOR ;
                
                if (THEMECOLOR==1) { self.imgView.image = [[UIImage imageNamed:@"xiala2"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
                }else{  self.imgView.image = [UIImage imageNamed:@"xiala2"]; }
                
                if (THEMECOLOR==2) { //主题色为浅色
                    self.titLab.textColor = COLORBLACK ;
                    _bgImgV.backgroundColor = LOGOCOLOR ;
                }
            }];
        }
    });
}

@end







