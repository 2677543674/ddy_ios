//
//  GoodsListCTCeel.h
//  DingDingYang
//
//  Created by ddy on 2017/6/13.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScreeningView.h"
#import "GoodsListCltCell1.h"
@class GoodsListCTCeel;
@protocol ListCTCellDelegate <NSObject>

-(void)gctCell:(GoodsListCTCeel*)ctcell nowClassTypeAllData:(NSDictionary*)datadic;

//-(void)shoudValue:(GoodsModel*)model ;

-(void)nowCell:(GoodsListCTCeel*)ctcell  offsetY:(NSString*)floaty ;

@end



@interface GoodsListCTCeel : UICollectionViewCell <UICollectionViewDelegate,UICollectionViewDataSource>

//@property(nonatomic,strong) UITableView * classTypeTable ;

@property(nonatomic,strong) UICollectionView * classTypeCltView ;

@property(nonatomic,assign) NSInteger   pageNum ;

@property(nonatomic,strong) NSMutableDictionary * parameter ;

@property(nonatomic,strong) NSMutableArray * classListAry ;

@property(nonatomic,strong) NSMutableDictionary * allDic ;

@property(nonatomic,strong) UIButton * topBtn ;

@property(nonatomic,copy) NSString * scrollY ;

@property(nonatomic,strong) ScreeningView * screenViewHead ;

@property(nonatomic,assign) id <ListCTCellDelegate>delegate ;

@property(nonatomic,copy) NSString * titleName ;


@property(nonatomic,assign) NSInteger goodsListStyle ;
/*
 classtype : 类型 (字符串)
 goodslist : 商品列表数组(数组)
 pdic : 请求参数(字典)
 
 */

@end




#pragma mark === >>> 首页分类滑动选项

@interface HomeSTCell : UICollectionViewCell
@property(nonatomic,strong)UILabel * textLab ;
@end

#pragma mark === >>> 空的cell
@interface EmptyCtCell : UICollectionViewCell

@end








