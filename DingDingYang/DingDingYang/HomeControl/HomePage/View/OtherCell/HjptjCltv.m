//
//  HjptjCltv.m
//  DingDingYang
//
//  Created by ddy on 2017/8/16.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HjptjCltv.h"

@implementation HjptjCltv



-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = COLORGROUP;
        UIButton * btn = [UIButton titLe:@" 每日上新·好券不断" bgColor:COLORWHITE titColorN:Black102 titColorH:Black102 font:12*HWB];
        [btn setImage:[[UIImage imageNamed:@"home_jptj"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]forState:UIControlStateNormal];
        btn.frame = CGRectMake(0, HFRAME(self)/2-15*HWB, ScreenW, 30*HWB);
        [self.contentView addSubview:btn];
//        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(self.contentView.mas_centerY);
//            make.left.right.offset(0); make.height.offset(30*HWB);
//        }];
        if (THEMECOLOR==1) {
            [btn setImage:[[UIImage imageNamed:@"home_jptj"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]forState:UIControlStateNormal];
        }else{
            [btn setImage:[UIImage imageNamed:@"home_jptj"] forState:UIControlStateNormal];
        }

    }
    return self ;
}

@end
