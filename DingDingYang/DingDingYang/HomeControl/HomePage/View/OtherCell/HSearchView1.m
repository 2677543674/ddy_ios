//
//  HSearchView1.m
//  DingDingYang
//
//  Created by ddy on 2017/8/4.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HSearchView1.h"
#import "SearchOldVC.h"
#import "MessageVC.h"
@implementation HSearchView1

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        NSString * title = @"请输入关键词搜索" ;
        
        _searchBtn = [UIButton titLe:title bgColor:COLORGROUP titColorN:Black102 titColorH:Black102 font:13.5*HWB];
        _searchBtn.titleLabel.textAlignment=NSTextAlignmentLeft;
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:13.5*HWB];
        _searchBtn.layer.masksToBounds = YES ;
        _searchBtn.layer.cornerRadius = 16 ;
        
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, (ScreenW-66)/2-48)];

        [self addSubview:_searchBtn];
        [_searchBtn addTarget:self action:@selector(clickHSearchBtn) forControlEvents:UIControlEventTouchUpInside];
        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.bottom.offset(-6);
            make.height.offset(30);
            make.right.offset(-51);
        }];
        
        _searchImgV = [[UIImageView alloc]init];
        _searchImgV.image = [UIImage imageNamed:@"sh_imgn"];
        _searchImgV.contentMode = UIViewContentModeScaleAspectFit ;
        [_searchBtn addSubview:_searchImgV];
        [_searchImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(7);  make.bottom.offset(-7);
            make.left.offset(10); make.width.offset(18);
        }];
        
        UIButton * msgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        msgBtn.tag=333;
        [msgBtn setImage:[UIImage imageNamed:@"home_msg"] forState:UIControlStateNormal];
        
        ADDTARGETBUTTON(msgBtn, gotoMsg)
        [self addSubview:msgBtn];
        [msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10);
            make.centerY.equalTo(_searchBtn.mas_centerY);
            make.height.offset(30);
            make.width.offset(30);
        }];
    }
    
    return self ;
}

-(void)gotoMsg{
    if (NSUDTakeData(UserInfo)!=nil) {
        NSDictionary * dic = NSUDTakeData(UserInfo) ;
        MessageVC*msgvc=[[MessageVC alloc]init];
        PersonalModel * infoModel = [[PersonalModel alloc]initWithDic:dic];
        msgvc.modelP =infoModel;
        msgvc.msgtype = 0 ;
        msgvc.hidesBottomBarWhenPushed=YES;
        [TopNavc pushViewController:msgvc animated:YES];
    }
}

// 种草君
-(void)tapMsg{
    if (_ZHCJType==1) {
        WebVC * web = [[WebVC alloc]init];
        web.urlWeb = _ZHCJweburl ;
        web.nameTitle = @"详情页" ;
        web.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:web animated:YES];
        
    }else{
        if (NSUDTakeData(UserInfo)!=nil) {
            NSDictionary * dic = NSUDTakeData(UserInfo) ;
            MessageVC*msgvc=[[MessageVC alloc]init];
            PersonalModel * infoModel = [[PersonalModel alloc]initWithDic:dic];
            msgvc.modelP = infoModel;
            msgvc.msgtype = 1 ;
            msgvc.hidesBottomBarWhenPushed=YES;
            [TopNavc pushViewController:msgvc animated:YES];
        }
    }
}

-(void)setSearchBarStyle:(NSString*)searchBarStyle{
    _searchBarStyle = searchBarStyle ;
    if (searchBarStyle.length>0) {
        NSInteger style = [[[searchBarStyle componentsSeparatedByString:@"-"] lastObject] integerValue];
        if (style==2) { //白色底色
//            self.backgroundColor = COLORWHITE ;
            _searchBtn.backgroundColor = COLORGROUP ;
            _searchImgV.image = [UIImage imageNamed:@"sh_imgn"];
        }else if (style==1) { //logo颜色底色
//            self.backgroundColor = LOGOCOLOR ;
            _searchBtn.backgroundColor = COLORWHITE ;
            _searchImgV.image = [UIImage imageNamed:@"sh_imgn"];
            // 宝拉甄选、省见、惠得、叮叮羊微集、花生LK、叮当叮当
            if (PID==10106||PID==10025||PID==10110||PID==10102||PID==888||PID==10102) {
                UIButton* msgBtn=[self viewWithTag:333];
                [msgBtn setImage:[UIImage imageNamed:@"new_mesgbtn"] forState:UIControlStateNormal];
                [msgBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.right.offset(-8);
                    make.centerY.equalTo(_searchBtn.mas_centerY);
                    make.height.offset(26);
                    make.width.offset(26);
                }];
            }
        }else{
            
        }
    }
}

-(void)clickHSearchBtn{
    if (TopNavc!=nil) {
        SearchOldVC*search = [[SearchOldVC alloc]init];
        search.searchType=1;
        search.hidesBottomBarWhenPushed = YES;
        [TopNavc pushViewController:search animated:NO];
    }
}



@end










