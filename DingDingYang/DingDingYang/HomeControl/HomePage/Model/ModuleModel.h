//
//  ModuleModel.h
//  DingDingYang
//
//  Created by ddy on 2017/8/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModuleModel : NSObject

@property (nonatomic,strong) UIColor *bgColor;       // 新增的背景色
@property(nonatomic,strong) NSArray *moduleClassAry; // 不同样式中的所有数据
@property(nonatomic,copy)   NSString * styleType;    // 模块样式
@property(nonatomic,copy)   NSString * img;          // 
//*** 以下是自定义参数 ***//
@property(nonatomic,assign) CGSize layoutCGSize; // 当前这个样式的宽和高
@property(nonatomic,assign) NSInteger styleTag;  // 模块样式(命名规则:模块个数+第?个样式，小方块分类除外)
@property(nonatomic,assign) NSInteger ifNewType; // 是否是新样式 0:标题、副标题、图片样式  1:图片样式(文字都是图片带的)

-(instancetype)initWith:(NSDictionary*)dic;

@end



@interface MClassModel : NSObject


@property(nonatomic,strong) NSDictionary * params ;     // 其它参数
@property(nonatomic,strong) NSArray * subCateInfoList ; // 主导航有分类时才有这个参数

@property(nonatomic,copy) NSString * title ;       // 标题
@property(nonatomic,copy) NSString * subtitle ;    // 副标题
@property(nonatomic,copy) NSString * img ;         // 网络图片
@property(nonatomic,copy) NSString * nImgs ;       // 网络图片
@property(nonatomic,copy) NSString * link ;        // 请求链接
@property(nonatomic,copy) NSString * viewType ;
@property(nonatomic,copy) NSString * defaultImg ;  // 分类图片
@property(nonatomic,copy) NSString * name ;        // 分类名称

@property(nonatomic,assign) NSInteger  listViewType ; // 跳转类型
@property(nonatomic,assign) NSInteger  ifNav ;
@property(nonatomic,assign) NSInteger  ifSel ;
@property(nonatomic,assign) NSInteger  platform ;     // 0:本平台 1:淘宝 2:京东
@property(nonatomic,assign) NSInteger  openType ;     // 1:webview  2:外部浏览器

// 本地参数
@property(nonatomic,copy) NSString * localImgName ; // 本地图片名称
@property(nonatomic,assign) NSInteger styleType;    // 是否是新样式 0:标题、副标题、图片样式  1:图片样式(文字都是图片带的)

-(instancetype)initWithMC:(NSDictionary*)dic ;

@end

#pragma mark ===>>> 界面写死的，直接从这复制，组成数组，解析。












