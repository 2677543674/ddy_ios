//
//  PageJump.m
//  DingDingYang
//
//  Created by ddy on 2017/8/11.
//  Copyright © 2017年 ddy.All rights reserved.



#import "PageJump.h"
#import "RongcloudListViewController.h"
#import "MomentsVC.h"
#import "UnGoodsListVC.h"
#import "WPHGoodsListVC.h"
#import "DdyShopWebVC.h"   // 叮叮羊多麦web页面
#import "DMGoodsListVC.h"  // 叮叮羊多麦综合平台
#import "UnGoodsClasslistBigVC.h"

@implementation PageJump

+(PageJump*)sharedInstance{
    PageJump * pageJump = [[PageJump alloc]init];
    return pageJump ;
}
-(instancetype)init{
    self = [super init];
    if (self) { }
    return  self ;
}



#pragma mark === >>> 首页普通商品类型跳转
/*
 
 跳转类型(listViewType)
 1:通用界面   2:限时抢购   3:官方推荐、短视频  4:淘抢购
 5:宝贝搜索   6:淘宝头条   7:免单专区         8:H5
 9:新手上路   10:        11:多麦平台链接     12:多麦综合平台
 
 
 21:多麦更多(web页面)
 
 30:京东精选  31:打开一个链接  32:一键发圈
 33:拼多读   34:唯品会       35:多麦

*/
+(void) pushAccordingToThe:(MClassModel*)model{
    
    /*
      页面跳转、下个页面所需参数
     1: listViewType 跳转类型
     2: title    标题
     3: link     请求链接
     4: params   额外参数
    */
    if (TopNavc!=nil) { // 当前导航视图存在
        switch (model.listViewType) {
                
            case 1 :{  // 1:通用界面
          
                if (model.link.length==0) { return ; }

                UniversalGoodsListVC * uglvc = [[UniversalGoodsListVC alloc]init];
                uglvc.mcsModel = model ;
                uglvc.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:uglvc animated:YES];
                
            } break;
                
            case 2 :{ // 2:限时抢购
                
                if (PID==10102) { // 叮当叮当
                    UniversalGoodsListVC * uglvc = [[UniversalGoodsListVC alloc]init];
                    uglvc.mcsModel = model ;
                    uglvc.hidesBottomBarWhenPushed = YES ;
                    [TopNavc pushViewController:uglvc animated:YES];
                }else{
                    SecondsKillVC * skvc = [[SecondsKillVC alloc]init];
                    skvc.mcsModel = model ;
                    skvc.hidesBottomBarWhenPushed = YES ;
                    [TopNavc pushViewController:skvc animated:YES];
                }
                
            } break;
                
            case 3 :{ // 3:官方推荐或短视频
                
                if ([[model.params allKeys] containsObject:@"ifVideo"]){ // 短视频的接口
                    if (PID==10110) { // 惠得
                        UniversalGoodsListVC * uglvc = [[UniversalGoodsListVC alloc]init];
                        uglvc.mcsModel = model ;
                        uglvc.hidesBottomBarWhenPushed = YES ;
                        [TopNavc pushViewController:uglvc animated:YES];
                    }else{
                        VideoSingleVC * videovc = [[VideoSingleVC alloc]init];
                        videovc.mcsModel = model;
                        videovc.hidesBottomBarWhenPushed = YES ;
                        [TopNavc pushViewController:videovc animated:YES];
                    }
                }else{
                    NewOrmdVC * rmdvc = [NewOrmdVC new]; // 新官方推荐
                    rmdvc.mcsModel = model ;
                    rmdvc.hidesBottomBarWhenPushed = YES ;
                    [TopNavc pushViewController:rmdvc animated:YES];
                }
                
            } break;
                
            case 4 :{ // 4:淘抢购
                
                CouponsLiveVC * taoQiangGouVC = [[CouponsLiveVC alloc]init];
                taoQiangGouVC.mcsModel = model;
                taoQiangGouVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:taoQiangGouVC animated:YES];
                
            } break;
                
            case 5 :{ // 宝贝搜索 (已弃用)
                
            } break;
                
            case 6 :{ // 6:淘宝头条
                
                TBNewsViewController * TBVC = [[TBNewsViewController alloc]init];
                TBVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:TBVC animated:YES];
                
            } break;
                
            case 7 :{ // 7:福利商城
                
                FreeOrderVC * freeOrderVC = [[FreeOrderVC alloc]init];
                freeOrderVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:freeOrderVC animated:YES];
                
            } break;
                
            case 8 :{ // 8:H5
                
                [WebVC pushWebViewUrlOrStr:model.link title:model.title isSHowType:0];
//                [self openWebViewBuyUrl:model.link andTitle:model.title];
                
            } break;
                
            case 9 :{ // 新手上路
                
                NewRoadVC * onroad = [[NewRoadVC alloc]init];
                onroad.mcsModel = model;
                onroad.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:onroad animated:YES];
                
            } break;
                
            case 11 :{ // 多麦平台链接
                
                if (TOKEN.length>0) {
                    DdyShopWebVC * ddyWeb = [[DdyShopWebVC alloc]init];
                    ddyWeb.hidesBottomBarWhenPushed = YES;
                    ddyWeb.urlWeb = model.link;
                    [TopNavc pushViewController:ddyWeb animated:YES];
                }else{
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:2 loginBlock:^(NSInteger result) {
                        if (result==1) {
                            DdyShopWebVC * ddyWeb = [[DdyShopWebVC alloc]init];
                            ddyWeb.hidesBottomBarWhenPushed = YES;
                            ddyWeb.urlWeb = model.link;
                            [TopNavc pushViewController:ddyWeb animated:YES];
                        }
                    }];
                }
                
            } break;
                
            case 12:{ // 多麦商品列表
                
                if (TOKEN.length>0) {
                    [self goToHasGoodsClassListType:5 mcModel:model];
                }else{
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:2 loginBlock:^(NSInteger result) {
                        if (result==1) {
                            [self goToHasGoodsClassListType:5 mcModel:model];
                        }
                    }];
                }
                
            } break;
                
            case 14:{ // 美团(叮叮羊)
                if (TOKEN.length>0) {
                    [self goToMeiTuanByModel:model];
                }else{
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:2 loginBlock:^(NSInteger result) {
                        if (result==1) { [self goToMeiTuanByModel:model]; }
                    }];
                }
            } break;
                
            case 21 :{ // 多麦平台个、更多，所有分类
                
                if (TOKEN.length>0) {
                    NSString * requrl = FORMATSTR(@"%@%@",dns_dynamic_duomai,@"duomai/goods/platform");
                    [NetRequest requestType:0 url:requrl ptdic:nil success:^(id nwdic) {
                        NSString * url = FORMATSTR(@"%@%@",dns_dynamic_duomai,REPLACENULL(nwdic[@"url"]));
                        [self openWebViewBuyUrl:url andTitle:model.title];
                    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                        
                    }];
                }else{
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:2 loginBlock:^(NSInteger result) {
                        if (result==1) {
                            NSString * requrl = FORMATSTR(@"%@%@",dns_dynamic_duomai,@"duomai/goods/platform");
                            [NetRequest requestType:0 url:requrl ptdic:nil success:^(id nwdic) {
                                NSString * url = FORMATSTR(@"%@%@",dns_dynamic_duomai,REPLACENULL(nwdic[@"url"]));
                                [self openWebViewBuyUrl:url andTitle:model.title];
                            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                                
                            }];
                        }
                    }];
                }
                
            } break;
                
            case 30 :{ // 京东精选
                
                if (IsNeedJingDong==1) { // 使用了京东的SDK
                    if (TOKEN.length==0) { // 未登录
                        [LoginVc showLoginVC:TopNavc.topViewController fromType:6 loginBlock:^(NSInteger result) {
                            if (result == 1) {
                                [self goToHasGoodsClassListType:2 mcModel:model];
                            }
                        }];
                    }else{ // 已登录
                        [self goToHasGoodsClassListType:2 mcModel:model];
                    }
                }else{ SHOWMSG(@"还未开放京东功能!") }
                
            } break;
                
            case 31 :{ // 其他跳转类型: 京东、淘宝、浏览器、等等
                
                if (model.openType==1) { // 用内部web打开
                    switch (model.platform) {
                        case 0:{ // 本平台链接
                            [self openWebViewBuyUrl:model.link andTitle:@"详情页"] ;
                        } break;
                            
                        case 1:{ // 淘宝
                            if (IsNeedTaoBao==1) { // 使用了淘宝授权
                                [TradeWebVC showCustomBaiChuan:@"" url:model.link pagetype:5];
                            }else{ // 未添加阿里百川SDK、则使用webview加载
                                [self openWebViewBuyUrl:model.link andTitle:@"淘宝详情页"] ;
                            }
                        } break;
                            
                        case 2:{ // 京东
                            if (IsNeedJingDong==1) { // 使用了京东的SDK
                                [[KeplerApiManager sharedKPService] openKeplerPageWithURL:model.link sourceController:TopNavc.topViewController jumpType:2 userInfo:nil];
                            }else{ // 未添加京东开普勒SDK、则使用webview加载
                                [self openWebViewBuyUrl:model.link andTitle:@"京东详情页"] ;
                            }
                        } break;
                            
                        default: break;
                    }
                }
                
                if (model.openType == 2) { // 用浏览器打开
                    NSURL * url = [NSURL URLWithString:model.link];
                    if ([[UIApplication sharedApplication]canOpenURL:url]) {
                         [[UIApplication sharedApplication] openURL:url];
                    }else{
                        SHOWMSG(@"暂时无法打开页面,可能链接有问题!")
                    }
                }
                
              } break;
                
            case 32:{ // 一键发圈
                
                if (TOKEN.length==0) { // 未登录
                    [LoginVc showLoginVC:TopNavc.topViewController fromType:6 loginBlock:^(NSInteger result) {
                        if (result == 1) {
                            MomentsVC * sendShareVC=[MomentsVC new];
                            sendShareVC.mtitle=model.title;
                            sendShareVC.hidesBottomBarWhenPushed = YES ;
                            [TopNavc pushViewController:sendShareVC animated:YES];
                        }
                    }];
                }else{ // 已登录
                    MomentsVC * sendShareVC = [MomentsVC new];
                    sendShareVC.mtitle=model.title;
                    sendShareVC.hidesBottomBarWhenPushed = YES ;
                    [TopNavc pushViewController:sendShareVC animated:YES];
                }
                
            } break;
                
            case 33:{ // 拼多多
                
                [self goToHasGoodsClassListType:3 mcModel:model];
                
            } break;
                
            case 34:{ // 唯品会
                
                [self goToHasGoodsClassListType:4 mcModel:model];
                
            } break;
                
            
            default:  break;
        }
    }
}

// 进入京东、拼多多、多麦、唯品会的商品列表(已封装成通用的)
+(void)goToHasGoodsClassListType:(NSInteger)goodsType mcModel:(MClassModel*)model{
    UnGoodsClasslistBigVC * goodsClassVC = [[UnGoodsClasslistBigVC alloc]init];
    goodsClassVC.hidesBottomBarWhenPushed = YES;
    goodsClassVC.goodsSource = goodsType;
    goodsClassVC.mcModel = model;
    [TopNavc pushViewController:goodsClassVC animated:YES];
}

+(void)goToMeiTuanByModel:(MClassModel*)model{
    
    PersonalModel * pmodel = [[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
    MBShow(@"正在加载..")
    [NetRequest requestType:0 url:model.link ptdic:@{@"userId":pmodel.cid} success:^(id nwdic) {
        MBHideHUD
        // urlGo:美团链接 urlGo:美团链接  urlDetail:返佣链接 rateStr:返佣比例
//        NSDictionary * mtdic = NULLDIC(nwdic);
       
//        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"imeituan://"]]){
//            NSString * mtUrl = REPLACENULL(mtdic[@"urlGo"]);
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mtUrl]];
//        }else{
            DdyShopWebVC * ddyWeb = [[DdyShopWebVC alloc]init];
            ddyWeb.platform = 1;
            ddyWeb.resultDic = NULLDIC(nwdic);
            ddyWeb.hidesBottomBarWhenPushed = YES;
            [TopNavc pushViewController:ddyWeb animated:YES];
//        }
    
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];
}

// 根据链接打开web页面
+(void)openWebViewBuyUrl:(NSString*)webUrl andTitle:(NSString*)title{
    WebVC * webvc = [[WebVC alloc] init];
    webvc.urlWeb = webUrl ;
    webvc.nameTitle = title ;
    webvc.hidesBottomBarWhenPushed = YES ;
    [TopNavc pushViewController:webvc animated:YES];
}


#pragma mark ===>>> 进入商品详情(传值)
+(void)pushToGoodsDetail:(GoodsModel*)model pushType:(NSInteger)type{
    if (TopNavc!=nil) {
        GoodsDetailsVC * detailsVC = [[GoodsDetailsVC alloc]init];
        detailsVC.dmodel = model ;
        detailsVC.pushType = type ;
        detailsVC.hidesBottomBarWhenPushed = YES ;
        [TopNavc pushViewController:detailsVC animated:YES];
    }
}

#pragma mark ===>>> 进入商品详情(传商品id，和商品来源)
+(void)pushToGoodsId:(NSString *)goodsId goodsSource:(NSString*)source{
//    MBShow(@"商品详情...")
    
    switch ([source integerValue]) {
            
        case 2:{ // 京东商品详情
            [NetRequest requestType:0 url:url_goods_detail_jd ptdic:@{@"goodsId":goodsId} success:^(id nwdic) {
                MBHideHUD
                GoodsModel * gmodel = [[GoodsModel alloc]initWithDic:NULLDIC(nwdic[@"bean"])];
                gmodel.platform = @"京东";
                [self pushToGoodsDetail:gmodel pushType:[source integerValue]];

            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD SHOWMSG(failure)
            }];
        } break;
            
        case 3:{ // 拼多多商品详情
            [NetRequest requestType:0 url:url_goods_details_pdd ptdic:@{@"goodsId":goodsId} success:^(id nwdic) {
                MBHideHUD
                GoodsModel * gmodel = [[GoodsModel alloc]initWithDic:NULLDIC(nwdic)];
                gmodel.goodsId = goodsId; gmodel.platform = @"拼多多";
                [self pushToGoodsDetail:gmodel pushType:[source integerValue]];

            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD SHOWMSG(failure)
            }];
        } break;
            
        default:{ // 淘宝商品详情
            [NetRequest requestType:0 url:url_goods_info ptdic:@{@"goodsId":goodsId} success:^(id nwdic) {
                MBHideHUD
                GoodsModel * gmodel = [[GoodsModel alloc]initWithDic:NULLDIC(nwdic[@"goods"])];
                if (gmodel.goodsName.length==0&&gmodel.goodsId.length==0) {
                    SHOWMSG(@"商品详情为空!")
                }else{
                    [self pushToGoodsDetail:gmodel pushType:0];
                }
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD SHOWMSG(failure)
            }];
        } break;
    }
    
}



#pragma mark ===>>> 领券购买

+(void)takeOrder:(GoodsModel *)goodsMd{
    if (TOKEN.length==0) {
        if (TopNavc!=nil) {
            [LoginVc showLoginVC:TopNavc.topViewController fromType:2 loginBlock:^(NSInteger result) {
                if (result==1) {
                    [self takeOrderLoginSuccess:goodsMd];
                }
            }];
        }else{ SHOWMSG(@"操作失败！") }
    }else{
        if (goodsMd!=nil&&TopNavc!=nil) {
            [self takeOrderLoginSuccess:goodsMd];
        }
    }
}

+(void)takeOrderLoginSuccess:(GoodsModel *)goodsMd{
    
    if ([goodsMd.platform containsString:@"3"]||[goodsMd.platform containsString:@"拼多多"]) { // 拼多多
        [self orderForPDD:goodsMd];
    }else if([goodsMd.platform containsString:@"多麦"]) {
        [WebVC pushWebViewUrlOrStr:goodsMd.goodsUrl title:@"下单购买" isSHowType:0];
    }else{ // 淘宝
        if (goodsMd!=nil) {
#warning ce_shi (加了淘宝二次授权)
            if (IsNeedTaoBao==1&&CANOPENWechat) { // 使用淘宝授权，
                MBShow(@"正在授权...")
                [self appUser_TaoBaoShouQuan:^(NSInteger shqState) {
                    MBHideHUD
                    // 已授权，直接去领券
                    if (shqState==1) { [self orderForTB:goodsMd]; }
                }];
                
            }else{ [self orderForTB:goodsMd]; }
        }
    }
}

#pragma mark ==>> 淘宝授权和二次授权
// 淘宝授权(首次，淘宝账号授权)
+(void)aLiBaiChuan_TBLogin:(void(^)(NSInteger tbLoginState))tbCompleteState{
    
    [[ALBBSDK sharedInstance] ALBBSDKInit];

    if ([[ALBBSession sharedInstance] isLogin]) {
        tbCompleteState(1);
        ALBBUser * tbUser = [[ALBBSession sharedInstance] getUser];
        LOG(@"nick: %@\navatarUrl: %@\nopenId: %@\nopenSid: %@\ntopAccessToken: %@\ntopAuthCode: %@",tbUser.nick,tbUser.avatarUrl,tbUser.openId,tbUser.openSid,tbUser.topAccessToken,tbUser.topAuthCode)
    }else{
        ALBBSDK * albbSDK = [ALBBSDK sharedInstance];
        [albbSDK setAppkey:TBKEY];
        [albbSDK setAuthOption:NormalAuth];
        [albbSDK auth:TopNavc.topViewController successCallback:^(ALBBSession *session){
            tbCompleteState(1);
        } failureCallback:^(ALBBSession *session,NSError *error){
            tbCompleteState(0);
            NSLog(@"session == %@,\n error == %@",session,error);
            SHOWMSG(@"授权失败或已取消授权!");
            
#warning ce_shi_ce_shi_ce_shi
            
//            NSString * errorStr = FORMATSTR(@"bindCode:%@\nisCanceledByUser:%d\n\n错误信息：\n%@",session.bindCode,session.isCanceledByUser,error);
//
//            [UIAlertController showIn:TopNavc.topViewController title:@"错误原因" content:errorStr ctAlignment:(NSTextAlignmentLeft) btnAry:@[@"关闭",@"复制"] indexAction:^(NSInteger indexTag) {
//                if (indexTag==1) {
//                    [ClipboardMonitoring pasteboard:errorStr];
//                }
//            }];
            
        }];
        ALBBUser * tbUser = [[ALBBSession sharedInstance] getUser];
        LOG(@"nick: %@\navatarUrl: %@\nopenId: %@\nopenSid: %@\ntopAccessToken: %@\ntopAuthCode: %@",tbUser.nick,tbUser.avatarUrl,tbUser.openId,tbUser.openSid,tbUser.topAccessToken,tbUser.topAuthCode)
    }
  

}

// 先判断该用户是否已授权 获取二次授权的链接
+(void)appUser_TaoBaoShouQuan:(void(^)(NSInteger shqState))completeState{
    
    // 获取授权的链接，判断授权的状态，
//    MBShow(@"正在加载...")
    [NetRequest requestType:0 url:url_tb_shou_quan ptdic:nil success:^(id nwdic) {
//        MBHideHUD
        NSInteger authorizationState = [REPLACENULL(nwdic[@"code"]) integerValue];
        NSString * callbackUrl = REPLACENULL(nwdic[@"callback"]);
        if (authorizationState==1) { // 已授权
            completeState(1);
        }else{ // 未授权则用百川sdk打开授权页面
            
            [self aLiBaiChuan_TBLogin:^(NSInteger tbLoginState) {
                if (tbLoginState==1) {
                    [TradeWebVC tbSecondaryAuthorization:callbackUrl  jsCode:REPLACENULL(nwdic[@"clickAuthJavascript"]) hdUrl:REPLACENULL(nwdic[@"url"]) shqState:^(NSInteger shqState, NSString *errorMsg) {
                        if (shqState==1) {
                            completeState(1); SHOWMSG(@"授权成功!")
                        } else { completeState(0); SHOWMSG(errorMsg) }
                    }];
                }
            }];
        }
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];

}

// 获取淘宝下单链接
+(void)orderForTB:(GoodsModel*)model{
    
    [UIPasteboard generalPasteboard].string = @"" ; // 打开淘宝领券时，先把粘贴板清掉
    
    NSMutableDictionary * parameterDic = [NSMutableDictionary dictionary] ;
    parameterDic[@"catId"] = model.catId ;
    parameterDic[@"goodsId"] = model.goodsId ;
    parameterDic[@"activityId"] = model.activityId ;
    parameterDic[@"endPrice"] = model.endPrice ;
    parameterDic[@"ifAuth"] = @"1"; // 新接口(淘宝二次授权)
    if (model.itemUrl.length>0) {
        parameterDic[@"goodsName"] = model.goodsName ;
        parameterDic[@"itemUrl"] = model.itemUrl ;
        parameterDic[@"price"] = model.price ;
    }
    if ([model.couponMoney floatValue]==0) {
        parameterDic[@"ifHaveCoupon"] = @"1" ; //无优惠券时传1
    }else{
        parameterDic[@"ifHaveCoupon"] = @"0" ; //有优惠券时传0
    }
    
    MBShow(@"正在加载...")
    NSString * tbLingQuan = FORMATSTR(@"%@%@",dns_dynamic_main,url_buy_goods);
    [NetRequest requestType:0 url:tbLingQuan ptdic:parameterDic success:^(id nwdic) {
        MBHideHUD
        NSString * buyUrl = FORMATSTR(@"taobao://uland.taobao.com/coupon/edetail?activityId=%@&pid=%@&itemId=%@&src=%@&dx=%@",model.activityId,REPLACENULL(nwdic[@"pid"]),model.goodsId,REPLACENULL(nwdic[@"src"]),REPLACENULL(nwdic[@"ifDx"]));
        
        if ( REPLACENULL(nwdic[@"marketingLink"]).length>0 ) {
            buyUrl = FORMATSTR(@"%@&pid=%@&src=%@",nwdic[@"marketingLink"],REPLACENULL(nwdic[@"pid"]),REPLACENULL(nwdic[@"src"]));
        }
        
        if (REPLACENULL(nwdic[@"url"]).length>0) {  buyUrl = nwdic[@"url"] ;  }
        
        if (CANOPENTaoBao==YES) {
            if ([buyUrl hasPrefix:@"https://"]) {
                buyUrl = [buyUrl substringFromIndex:6];
                buyUrl = FORMATSTR(@"tbopen:%@",buyUrl);
            }
            if ([buyUrl hasPrefix:@"http://"]) {
                buyUrl = [buyUrl substringFromIndex:5];
                buyUrl = FORMATSTR(@"tbopen:%@",buyUrl);
            }
            if (IsNeedTaoBao==2) { // 不使用淘宝授权(直接打开淘宝)
                if ([buyUrl hasPrefix:@"taobao"]) {
                    buyUrl = [buyUrl substringFromIndex:7];
                    buyUrl = FORMATSTR(@"tbopen:%@",buyUrl);
                }
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:buyUrl]];
            }else{ [self openLingQuan_WebVC_Or_TaoBao:buyUrl]; }
        }else{
            if ([buyUrl hasPrefix:@"taobao"]||[buyUrl hasPrefix:@"tbopen"]) {
                buyUrl = [buyUrl substringFromIndex:7];
                buyUrl = FORMATSTR(@"https:%@",buyUrl);
            }
            if (IsNeedTaoBao==2) { // 不使用淘宝授权(没有安装淘宝时,用web页面打开)
                [self openWebViewBuyUrl:buyUrl andTitle:@"领取优惠券"];
            }else{ [self openLingQuan_WebVC_Or_TaoBao:buyUrl]; }
        }
        
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD  SHOWMSG(failure)
    }];

}
// 使用百川打开淘宝链接
+(void)openLingQuan_WebVC_Or_TaoBao:(NSString*)buyUrl{
    
    id<AlibcTradePage> page = [AlibcTradePageFactory page: buyUrl ];
    
    AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
    showParam.openType = AlibcOpenTypeNative ;
    showParam.backUrl = FORMATSTR(@"tbopen%@://",TBKEY);
    showParam.isNeedPush = YES ;
    showParam.linkKey=@"taobao_scheme";
//    showParam.isNeedCustomNativeFailMode = true;
    
    //淘客参数
    AlibcTradeTaokeParams * taokeParams = [[AlibcTradeTaokeParams alloc] init];
    taokeParams.pid = @"" ; //
    taokeParams.unionId = @"" ; //
    taokeParams.subPid = @"" ; //
    
//    TradeWebVC * myView = [[TradeWebVC alloc] init] ;
//    myView.hidesBottomBarWhenPushed = YES ;
//    myView.showOrderType = 0 ;
    
    //外呼淘宝闭环交易
    NSInteger res = [[AlibcTradeSDK sharedInstance].tradeService openByUrl:buyUrl identity:@"trade" webView:nil parentController:TopNavc.topViewController showParams:showParam taoKeParams:taokeParams trackParam:nil tradeProcessSuccessCallback:nil tradeProcessFailedCallback:nil];
    
    
//    NSInteger res = [[AlibcTradeSDK sharedInstance].tradeService openByBizCode:@"cart" page:page webView:myView.webView parentController:myView showParams:showParam taoKeParams:taokeParams trackParam:nil tradeProcessSuccessCallback:^(AlibcTradeResult * _Nullable result) {
//        LOG(@"状态：%lu\n成功订单：%@\n失败订单：%@",(unsigned long)result.result,result.payResult.paySuccessOrders,result.payResult.payFailedOrders);
//    } tradeProcessFailedCallback:^(NSError * _Nullable error) {
//        NSLog(@"%@",error);
//    }];
    
    if (res == 1) {
//        [TopNavc pushViewController:myView animated:true];
    }
}

#pragma mark ==>> 拼多多下单
+(void)orderForPDD:(GoodsModel*)pddModel{
    NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
    pdic[@"couponMoney"] = pddModel.couponMoney;
    pdic[@"goodsName"] = pddModel.goodsName;
    pdic[@"endPrice"] = pddModel.endPrice;
    pdic[@"goodsImg"] = pddModel.goodsPic;
    pdic[@"goodsId"] = pddModel.goodsId;
    pdic[@"price"] = pddModel.price;
    pdic[@"ifBuy"] = @"1";
    [NetRequest requestType:0 url:url_getInfo_pdd ptdic:nil success:^(id nwdic) {
        
        pdic[@"pid"] = REPLACENULL(nwdic[@"pddPid"]);
        pdic[@"clientId"] = REPLACENULL(nwdic[@"clientId"]);
        pdic[@"parameter"] = REPLACENULL(nwdic[@"parameter"]);
        pdic[@"clientSecret"] = REPLACENULL(nwdic[@"clientSecret"]);
        
        if (REPLACENULL(nwdic[@"pddPid"]).length>0) {
            NSString * urlScheme = @"pinduoduo://com.xunmeng.pinduoduo/duo_coupon_landing.html?";
            NSString* urlString = FORMATSTR(@"%@goods_id=%@&pid=%@",urlScheme,pddModel.goodsId,REPLACENULL(nwdic[@"pddPid"]));
            NSLog(@"%@",urlString);
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"pinduoduo://"]]){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }else{
                NSString * pddBuyUrl = url_share_buy_pdd;
                [NetRequest requestType:0 url:pddBuyUrl ptdic:pdic success:^(id nwdic) {
                    NSString * takekOrderUrl = REPLACENULL(nwdic[@"url"]);
                    if (takekOrderUrl.length>0) {
                        // 用浏览器打开
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:takekOrderUrl]];
//                        if ([takekOrderUrl hasPrefix:@"http"]) {
//                            [WebVC pushWebViewUrlOrStr:takekOrderUrl title:@"领券购买" isSHowType:2];
//                        }else{ SHOWMSG(@"领券失败!") }
                    }else{ SHOWMSG(@"领券失败!") }
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    MBHideHUD SHOWMSG(failure)
                }];
            }
        }else{ SHOWMSG(@"领券失败!") }
        
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD SHOWMSG(failure)
    }];
    
}


#pragma mark ===>>> 进入分享编辑页面、分享赚钱
// 先判断是否登录
+(void)shareGoods:(GoodsModel*)goodsMd goodsImgAry:(NSMutableArray*)ary {
    if (goodsMd!=nil&&TopNavc!=nil) {
        if (TOKEN.length==0) {
            [LoginVc showLoginVC:TopNavc.topViewController fromType:3 loginBlock:^(NSInteger result) {
                if (result==1) {
                    if (goodsMd!=nil) {
                        [self pushToEditor:goodsMd urlary:ary];
                    }
                }
            }];
        }else{ [self pushToEditor:goodsMd urlary:ary]; }
    }else{ SHOWMSG(@"数据错误！") }
}


// 判断是否有商品主图，此处只有拼多多和淘宝商品
+(void)pushToEditor:(GoodsModel*)model urlary:(NSMutableArray*)urlAry{
    
    if (urlAry==nil||urlAry.count==0) {
        
        MBShow(@"获取图片...")
        // 拼多多
        if ([model.platform isEqualToString:@"3"]||[model.platform isEqualToString:@"拼多多"]) {
            
            NSString * pddUrl = url_goods_details_pdd;
            NSDictionary * pdic = @{@"goodsId":model.goodsId};
            
            [NetRequest requestType:0 url:pddUrl ptdic:pdic success:^(id nwdic) {
                MBHideHUD  [model addDetailDic:nwdic];
                if (model.goodsGalleryUrls.count==0) {
                    model.goodsGalleryUrls = @[model.smallPic];
                }
                [self pushToShareVcBy:model imgAry:(NSMutableArray*)model.goodsGalleryUrls];
                
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD  SHOWMSG(failure)
            }];
            
        } else if ([model.platform isEqualToString:@"京东"]){
            [self pushToShareVcBy:model imgAry:(NSMutableArray*)model.goodsPic];
        } else { // 淘宝商品
            // 获取商品轮播图
            [NetRequest requestType:0 url:url_goods_pics ptdic:@{@"goodsId":model.goodsId} success:^(id nwdic) {
                NSMutableArray * aryImg = [NSMutableArray array];
                NSDictionary * picDic = nwdic;
                NSDictionary * par = @{@"goodsId":model.goodsId,@"ifShop":@"1"}; // 叮当加店铺
                // 获取推荐语
                [NetRequest requestType:0 url:url_goods_rmdText ptdic:par success:^(id nwdic) {
                    MBHideHUD
                    NSArray * ary = NULLARY(picDic[@"images"]);
                    [aryImg addObjectsFromArray:ary];
                    if (aryImg.count==0) {
                        if ([model.goodsPic hasPrefix:@"http"]) {
                            [aryImg addObject:model.goodsPic];
                        }
                    }
                    [model setValue:REPLACENULL(nwdic[@"des"]) forKey:@"desStr"];
                    ShopModel * shop = [[ShopModel alloc]initWithDic:nwdic[@"shop"]];
                    model.shopModel = shop;
                    if (aryImg.count>0) {
#warning ce_shi ( 加了授权判断 )
                        if (IsNeedTaoBao==1) {
                            MBShow(@"正在加载...")
                            [self appUser_TaoBaoShouQuan:^(NSInteger shqState) {
                                MBHideHUD
                                if (shqState==1) { [self pushToShareVcBy:model imgAry:aryImg]; }
                            }];
                        }else{ [self pushToShareVcBy:model imgAry:aryImg]; }
                    }else{ SHOWMSG(@"未获取到商品主图，请稍后重试!") }
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    MBHideHUD  SHOWMSG(failure)
                }];
                
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD  SHOWMSG(failure)
            }];
        }
        
    }else{
        if ([model.platform isEqualToString:@"3"]&&[model.platform isEqualToString:@"拼多多"]) {
            [self pushToShareVcBy:model imgAry:urlAry];
        }else if ([model.platform isEqualToString:@"京东"]){
            [self pushToShareVcBy:model imgAry:urlAry];
        }else{
#warning ce_shi ( 加了授权判断 )
            if (IsNeedTaoBao==1) {
                MBShow(@"正在加载...")
                [self appUser_TaoBaoShouQuan:^(NSInteger shqState) {
                    MBHideHUD
                    if (shqState==1) { [self pushToShareVcBy:model imgAry:urlAry]; }
                }];
            }else{ [self pushToShareVcBy:model imgAry:urlAry]; }
        }
    }
    
}

// 进入编辑分享页面
+(void)pushToShareVcBy:(GoodsModel*)model imgAry:(NSMutableArray*)urlAry{

    NewCreateShareVC * sharevc = [[NewCreateShareVC alloc]init];
    sharevc.gimgUrlAry = [urlAry mutableCopy];
    sharevc.model = model ;
    sharevc.hidesBottomBarWhenPushed = YES ;
    if (model.goodsGalleryUrls.count>0) { sharevc.shareType=3; }
    if ([model.platform isEqualToString:@"京东"]) { sharevc.shareType = 2; }
    [TopNavc pushViewController:sharevc animated:YES];
}

+(void)pushChat{
    if (TopNavc!=nil&&[TopNavc isKindOfClass:[UINavigationController class]]) {
        RongcloudListViewController* list=[RongcloudListViewController new];
        list.hidesBottomBarWhenPushed=YES;
        [TopNavc pushViewController:list animated:YES];
    }
    else{
        NSLog(@"topVC==nil");
    }
}

#pragma mark ===>>> 云推送界面跳转处理


/*
 1:  系统邮件详情  content: url
 2:  奖励消息列表
 3:  淘抢购
 4:  0元购
 5:  商品详情页 content:商品id

 7:  限时抢购
 8:  今日主题 (已废弃)
 9:  美食汇
 10: 淘抢购 (跟3重复)
 11: 新手上路
 12: 短视频
 13: 品牌团
 14: 聚划算
 15: 极有家
 16: 官方推荐
 
 
 20: 免单商品详情页 content:商品id
 
 
 /////////////////////////////////////////////////////////
 页面跳转、下个页面所需参数 （通用类型需拼接参数）
 1: listViewType 跳转类型
 2: title    标题
 3: link     请求链接
 4: params   额外参数

 */
+(void)yunPushType:(NSInteger)type content:(NSString*)content{
    if (TopNavc!=nil&&[TopNavc isKindOfClass:[UINavigationController class]]) {
        switch (type) {
            case 1:{  // 系统邮件详情
                WebVC * web = [[WebVC alloc]init];
                web.urlWeb = content ;
                web.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:web animated:YES];
            }  break;
                
            case 2:{ // 奖励消息列表
                if (NSUDTakeData(UserInfo)!=nil) {
                    PersonalModel*infoModel=[[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
                    MessageVC*message=[[MessageVC alloc]init];
                    message.hidesBottomBarWhenPushed = YES;
                    message.msgtype = 0 ;
                    message.modelP=infoModel;
                    [TopNavc pushViewController:message animated:YES];
                }
            }  break;
                
            case 3:{ // 淘抢购
                CouponsLiveVC * taoQiangGouVC = [[CouponsLiveVC alloc]init];
                taoQiangGouVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:taoQiangGouVC animated:YES];
            }  break;
                
            case 4:{ // 0元购
                
            }  break;
                
            case 5:{ // 商品详情
                if (content.length>0) {
                    MBShow(@"商品详情...")
                    [NetRequest requestTokenURL:url_goods_info parameter:@{@"goodsId":content} success:^(NSDictionary *nwdic) {
                        MBHideHUD
                        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                            GoodsModel * gmmodel = [[GoodsModel alloc]initWithDic:NULLDIC(nwdic[@"goods"])];
                            GoodsDetailsVC*detailvc= [GoodsDetailsVC new];
                            detailvc.dmodel = gmmodel ;
                            detailvc.hidesBottomBarWhenPushed = YES ;
                            [TopNavc pushViewController:detailvc animated:YES];
                        }else{
                            SHOWMSG(nwdic[@"result"])
                        }
                    } failure:^(NSString *failure) {
                        MBHideHUD
                        SHOWMSG(failure)
                    }];
                }else{
                   SHOWMSG(@"推送的商品ID为空!")
                }
                
            }  break;
                
            case 7:{ // 限时抢购
                SecondsKillVC * skvc = [[SecondsKillVC alloc]init];
                skvc.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:skvc animated:YES];
            }  break;
                
            case 8:{ // 今日主题 (已废弃)
                
            }  break;
                
            case 9:{ // 美食汇
                NSDictionary * dic = @{@"listViewType":@"1",@"title":@"美食汇",@"link":url_goods_list2,@"params":@{@"catId":@"6"}};
                [self jieXi:dic];
            }  break;
                
            case 10:{ // 淘抢购
                CouponsLiveVC * taoQiangGouVC = [[CouponsLiveVC alloc]init];
                taoQiangGouVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:taoQiangGouVC animated:YES];
            }  break;
                
            case 11:{// 新手上路
                NewRoadVC * onroad = [[NewRoadVC alloc]init];
                onroad.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:onroad animated:YES];
            }  break;
                
            case 12:{ // 短视频
                VideoSingleVC * videovc = [[VideoSingleVC alloc]init];
                videovc.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:videovc animated:YES];
            }  break;
                
            case 13:{ // 品牌团
                NSDictionary * dic = @{@"listViewType":@"1",@"title":@"品牌团",@"link":url_goods_list2,@"params":@{@"ifBrand":@"1"}};
                [self jieXi:dic];
            }  break;
                
            case 14:{ // 聚划算
                NSDictionary * dic = @{@"listViewType":@"1",@"title":@"聚划算",@"link":url_goods_list2,@"params":@{@"activateType":@"1"}};
                [self jieXi:dic];
            }  break;
                
            case 15:{ // 极有家
                NSDictionary * dic = @{@"listViewType":@"1",@"title":@"极有家",@"link":url_goods_jiyouojia,@"params":@{}};
                [self jieXi:dic];
            }  break;
                
            case 16:{ // 官方推荐
                NSDictionary * dic = @{@"listViewType":@"3",@"title":@"官方推荐",@"link":url_goods_list2,@"params":@{@"ifRecomn":@"1"}};
                [self jieXi:dic];
            }  break;
                
            case 20:{ // 免单商品详情页
                [NetRequest requestTokenURL:url_free_details parameter:@{@"goodsFreeId":content} success:^(NSDictionary *nwdic) {
                    if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                        FreeOrderModel * fgModel = [[FreeOrderModel alloc]initWithDic:NULLDIC(nwdic[@"goods"])];
                        FreeOrderDetailVC * detailvc= [FreeOrderDetailVC new];
                        detailvc.fgModel = fgModel ;
                        detailvc.hidesBottomBarWhenPushed = YES ;
                        [TopNavc pushViewController:detailvc animated:YES];
                    }else{
                        SHOWMSG(nwdic[@"result"])
                    }
                } failure:^(NSString *failure) {
                    SHOWMSG(failure)
                }];
            }  break;
                
            case 21:{ // 团队
                NewTeamVC * teamVC = [[NewTeamVC alloc]init];
                teamVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:teamVC animated:YES];
            }  break;
                
            case 22:{ // 订单
                NewConsumer * consumerVC = [[NewConsumer alloc]init];
                consumerVC.hidesBottomBarWhenPushed = YES ;
                [TopNavc pushViewController:consumerVC animated:YES];
            }  break;
                
            default: break;
                
        }
    }
}

//解析
+(void)jieXi:(NSDictionary*)dic{
    MClassModel * modelMC = [[MClassModel alloc] initWithMC:dic];
    [self pushAccordingToThe:modelMC];
}



+(void)pushToActivity:(ActivityModel*)model{
    switch (model.todo) {
        case  1:{
            GoodsDetailsVC*detailVc = [[GoodsDetailsVC alloc]init] ;
            detailVc.hidesBottomBarWhenPushed = YES ;
            detailVc.dmodel = model.goodsMd ;
            [TopNavc pushViewController:detailVc  animated:YES] ;
        } break;
            
        case  2:{
            if ([model.openType isEqualToString:@"1"]) { // 用内部web打开
                switch ([model.platform integerValue]) {
                    case 0:{ // 本平台链接
                        if (TOKEN.length > 0 && ![model.link containsString:@"token="]) {
                            model.link = [NSString stringWithFormat:@"%@&token=%@",model.link,TOKEN];
                        }
                        
                        [self openWebViewBuyUrl:model.link andTitle:@"详情页"] ;
                    } break;
                        
                    case 1:{ // 淘宝
                        if (IsNeedTaoBao==1) { // 使用了淘宝授权
                            [TradeWebVC showCustomBaiChuan:@"" url:model.link pagetype:5];
                        }else{ // 未添加阿里百川SDK、则使用webview加载
                            [self openWebViewBuyUrl:model.link andTitle:@"淘宝详情页"] ;
                        }
                    } break;
                        
                    case 2:{ // 京东
                        if (IsNeedJingDong==1) { // 使用了京东的SDK
                            [[KeplerApiManager sharedKPService] openKeplerPageWithURL:model.link sourceController:TopNavc.topViewController jumpType:2 userInfo:nil];
                        }else{ // 未添加京东开普勒SDK、则使用webview加载
                            [self openWebViewBuyUrl:model.link andTitle:@"京东详情页"] ;
                        }
                    } break;
                        
                    default: break;
                }
            }
            
            if ([model.openType isEqualToString:@"2"]) { // 用浏览器打开
                NSURL * url = [NSURL URLWithString:model.link];
                if ([[UIApplication sharedApplication]canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }else{
                    SHOWMSG(@"暂时无法打开页面,可能链接有问题!")
                }
            }
        }break;
            
        case  3:{ // 二级页面
            [self pushAccordingToThe:model.category];
        } break;
            
        default:  break;
    }
}

/**
 打开系统视频播放器
 
 @param url 视频地址
 */
+(void)openVideoUrlBySystemVideoPlayer:(NSString*)url{
    if (TopNavc.topViewController!=nil) {
        if ([url hasPrefix:@"http"]) {
            if (NETSTATE==3) {
                static dispatch_once_t onceToken;
                dispatch_once(&onceToken, ^{
                    SHOWMSG(@"当前使用的是移动数据，请注意您的流量!")
                });
            }
            AVPlayerViewController * playerView = [[AVPlayerViewController alloc]init];
            playerView.player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:url]];
            [TopNavc.topViewController presentViewController:playerView animated:YES completion:^{
                [playerView.player play];
                NSLog(@"视频开始播放了。。。。。。。。");
            }];
        }else{
            SHOWMSG(@"视频链接错误，无法播放!")
        }
    }else{
        SHOWMSG(@"打开失败!")
    }
}

@end






