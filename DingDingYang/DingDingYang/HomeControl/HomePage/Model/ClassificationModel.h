//
//  ClassificationModel.h
//  DingDingYang
//
//  Created by ddy on 2017/3/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>
// 首页分类入口model
@interface ClassificationModel : NSObject
@property(nonatomic,copy)NSString*cid;
@property(nonatomic,copy)NSString*imgUrl;
@property(nonatomic,copy)NSString*title;
-(instancetype)initWithDic:(NSDictionary*)dic;
@end
