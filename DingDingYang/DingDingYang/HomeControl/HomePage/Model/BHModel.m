//
//  BHModel.m
//  DingDingYang
//
//  Created by ddy on 2017/5/15.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "BHModel.h"

@implementation BHModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.refid = REPLACENULL(dic[@"refId"]) ;
        self.link = REPLACENULL(dic[@"link"]) ;
        
        NSString *color = REPLACENULL(dic[@"bgColor"]);
        if (color.length>0) {
            NSLog(@"%@,%@", dic, color);
            self.bgColor = [self colorFromHexCode:color];
        }else{
            self.bgColor = COLORWHITE;
        }
        
        NSString * img = REPLACENULL(dic[@"img"]);
        if (img.length>0) {
           self.img = [img hasPrefix:@"http"]?img:FORMATSTR(@"%@%@",dns_dynamic_loadimg,img);
        }else{ self.img = @""; }
        
        NSString * bigImg = REPLACENULL(dic[@"newImg"]);
        if (bigImg.length>0) {
           self.bigImg = [bigImg hasPrefix:@"http"]?bigImg:FORMATSTR(@"%@%@",dns_dynamic_loadimg,bigImg);
        }else{ self.bigImg = @""; }
        
        self.ifNew = REPLACENULL(dic[@"ifNew"]);
        self.todo = [dic[@"toDo"] integerValue];
        self.goodsMd = [[GoodsModel alloc]initWithDic:NULLDIC(dic[@"goods"])];
        self.mClassModel = [[MClassModel alloc]initWithMC:NULLDIC(dic[@"category"])];
        self.des = REPLACENULL(dic[@"des"]);
    }
    return self ;

}

-(instancetype)initWithWph:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.img = [REPLACENULL(dic[@"img_url"]) hasPrefix:@"http"]? dic[@"img_url"] : FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"img_url"])) ;
        self.link = REPLACENULL(dic[@"url"]) ;
        self.refid = REPLACENULL(dic[@"id"]) ;
        
        NSString *color = REPLACENULL(dic[@"bgColor"]);
        if (color.length>0) {
            NSLog(@"%@,%@", dic, color);
            self.bgColor = [self colorFromHexCode:color];
        }else{
            self.bgColor = COLORWHITE;
        }
    }
    return self ;
    
}

- (UIColor *) colorFromHexCode:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
