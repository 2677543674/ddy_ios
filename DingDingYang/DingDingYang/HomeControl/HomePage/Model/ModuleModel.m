//
//  ModuleModel.m
//  DingDingYang
//
//  Created by ddy on 2017/8/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ModuleModel.h"

// RGB颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation ModuleModel

-(instancetype)initWith:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        
        _styleType = REPLACENULL(dic[@"styleType"]);
        _ifNewType = [NSUDTakeData(@"Home_ifNew") integerValue];
        NSString * img = REPLACENULL(dic[@"img"]);
        if (img.length>0) {
           _img = [img hasPrefix:@"http"]?img:FORMATSTR(@"%@%@",dns_dynamic_loadimg,img);
        }else{
           _img = @"";
        }
        NSString *color = REPLACENULL(dic[@"bgColor"]);
        if (color.length>0) {
            NSLog(@"%@,%@", dic, color);
            _bgColor = [self colorFromHexCode:color];
        }else{
            _bgColor = COLORWHITE;
        }
        
        
        NSMutableArray * ary = [NSMutableArray array];
        NSArray * mcary = NULLARY(dic[@"cateInfoList"]) ;
        [mcary enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
            MClassModel * classmodel = [[MClassModel alloc]initWithMC:obj];
            classmodel.styleType = _ifNewType;
            [ary addObject:classmodel];
        }];
        _moduleClassAry = [[NSArray alloc]initWithArray:ary];
        
        NSLog(@"_styleType == %@", _styleType);
        // OU
        // 轮播
        if ([_styleType isEqualToString:@"2-1-1-20"]) { // 1行2列
            _styleTag = 0;
            _layoutCGSize = CGSizeMake(ScreenW, (ScreenW - 40) * 130.0 / 375 - 27);
        }else if ([_styleType isEqualToString:@"2-1-1-21"]) { // 按列排，个数不定  // 1行2列
            _styleTag = 1;
            _layoutCGSize = CGSizeMake(ScreenW, 100);
        }else if ([_styleType isEqualToString:@"2-1-1-22"]) { // 1行2列
            _styleTag = 2;
            CGFloat space = 7;
            CGFloat offset = 15;
            _layoutCGSize = CGSizeMake(ScreenW, (ScreenW-(2+1)*space)/2/2*((_moduleClassAry.count-1)/2+1)+(_moduleClassAry.count/2-1)*space+offset);
        }else if ([_styleType isEqualToString:@"2-1-1-23"]) {
            _styleTag = 3;
            CGFloat space = 7;
            CGFloat offset = 15;
            _layoutCGSize = CGSizeMake(ScreenW, (ScreenW-(3+1)*space)/3/2*((_moduleClassAry.count-1)/3+1)+(_moduleClassAry.count/3-1)*space+offset);
        }else if ([_styleType isEqualToString:@"2-1-1-24"]) {
            _styleTag = 4;
            CGFloat space = 7;
            CGFloat offset = 15;
            _layoutCGSize = CGSizeMake(ScreenW, (ScreenW-(4+1)*space)/4/2*((_moduleClassAry.count-1)/4+1)+(_moduleClassAry.count/4-1)*space+offset);
        }else{
            
            // 不是按列排，个数固定
            // 先根据数组个数判断，再判断类型，可以减少判断次数
            switch (_moduleClassAry.count) {
                    
                case 1:{
                    
                    _styleTag = 10;
                    _layoutCGSize = CGSizeMake(ScreenW,120);
                    
                    if ([_styleType isEqualToString:@"2-1-1-1"]) { // 头条
                        _styleTag = 60;
                        _layoutCGSize = CGSizeMake(ScreenW,30*HWB);
                    }
                    
                } break;
                    
                case 2:{
                    
                    _styleTag = 20;
                    _layoutCGSize = CGSizeMake(ScreenW, ScreenW/3);
                    
                } break;
                    
                case 3:{ // 默认: 2-3-1
                    
                    if ([_styleType isEqualToString:@"2-3-1-1"]) {
                        _styleTag = 31;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.4)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW,ScreenW/2-20);
                        }
                    }else{
                        _styleTag = 30;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.267)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW, ScreenW/3);
                        }
                    }
                    
                } break;
                    
                case 4:{ // 默认 @"2-4-1"
                    
                    if ([_styleType isEqualToString:@"2-4-1-1"]) {
                        _styleTag = 41;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.54)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW/4)*3-29);
                        }
                    }else if ([_styleType isEqualToString:@"2-4-1-2"]){
                        _styleTag = 42;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.4)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW/3)*2);
                        }
                    }else{
                        _styleTag = 40;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.45)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW/1.8));
                        }
                    }
                    
                } break;
                    
                case 5:{ // 默认 @"2-5-1"
                    
                    if ([_styleType isEqualToString:@"2-5-0"]) {
                        _styleTag = 105; // 五个或十个的小图标分类
                        _layoutCGSize =  CGSizeMake(ScreenW,70*HWB);
                    }else if ([_styleType isEqualToString:@"2-5-1-1"]){
                        _styleTag = 51;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.675)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(70*HWB)*3);
                        }
                    }else if ([_styleType isEqualToString:@"2-5-1-2"]){
                        _styleTag = 52;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.6)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW/3-10)*2-10);
                        }
                    }else{
                        _styleTag = 50;
                        if (_ifNewType==1) {
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW*0.45)+2);
                        }else{
                            _layoutCGSize = CGSizeMake(ScreenW, FloatKeepInt(ScreenW/1.8));
                        }
                    }
                    
                } break;
                    
                case 10:{ // 小方块分类
                    
                    _styleTag = 110; // @"2-10-0"
                    _layoutCGSize = CGSizeMake(ScreenW, 140*HWB);
                    
                } break;
                    
                default: {
                    
                    if ([_styleType isEqualToString:@"7-6-1-0"]) { // 七天收益图
                        _styleTag = 70;
                        _layoutCGSize = CANOPENWechat?CGSizeMake(ScreenW, 128):CGSizeZero;
                    } else if ([_styleType isEqualToString:@"2-1-1-1"]) { // 头条
                        _styleTag = 60;
                        _layoutCGSize = CGSizeMake(ScreenW,30*HWB);
                    } else { // 判断是否有未知类型
                        if (_moduleClassAry.count>5) {
                            _styleTag = 100; // 小分类不限个数，
                            NSInteger beishu = _moduleClassAry.count/5;
                            if (_moduleClassAry.count%5>0) { beishu = beishu+1; }
                            _layoutCGSize = CGSizeMake(ScreenW, 70*HWB*beishu);
                        }else{
                            _styleTag = 0;
                            _layoutCGSize = CGSizeZero;
                        }
                    }
                }break;
            }
            
        }

        
        
        
    }
    return self ;
}

- (UIColor *) colorFromHexCode:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end



@implementation MClassModel

-(instancetype)initWithMC:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        
        _name = REPLACENULL(dic[@"name"]) ;
        _title = REPLACENULL(dic[@"title"]);
        _subtitle = REPLACENULL(dic[@"subtitle"]);

        _link = REPLACENULL(dic[@"link"]) ;
        _params = NULLDIC(dic[@"params"]) ;
        _subCateInfoList = NULLARY(dic[@"subCateInfoList"]);
        
        _viewType = REPLACENULL(dic[@"viewType"]) ;
        _ifNav = [REPLACENULL(dic[@"ifNav"]) integerValue];
        _ifSel = [REPLACENULL(dic[@"ifSel"]) integerValue];
        _listViewType = [REPLACENULL(dic[@"listViewType"]) integerValue];
        
        _platform = [REPLACENULL(dic[@"platform"]) integerValue];
        _openType = [REPLACENULL(dic[@"openType"]) integerValue];

        
        NSString * smallImg = REPLACENULL(dic[@"img"]);
        if (smallImg.length>0) {
            _img = [smallImg hasPrefix:@"http"]?smallImg:FORMATSTR(@"%@%@",dns_dynamic_loadimg,smallImg);
        }else{ _img = @""; }
        
        NSString * bigImg = REPLACENULL(dic[@"newImgs"]);
        if (bigImg.length>0) {
           _nImgs = [bigImg hasPrefix:@"http"]?bigImg:FORMATSTR(@"%@%@",dns_dynamic_loadimg,bigImg);
        }else{ bigImg = @""; }
        
        _defaultImg = REPLACENULL(dic[@"defaultImg"])?dic[@"defaultImg"]:FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"defaultImg"]));
        
        _localImgName = @"zhan_wei_tu" ;
        if (REPLACENULL(dic[@"localImgName"]).length>0) { _localImgName = REPLACENULL(dic[@"localImgName"]) ; }
        
    }
    return self ;
}

@end




