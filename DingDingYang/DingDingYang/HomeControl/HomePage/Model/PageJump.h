//
//  PageJump.h
//  DingDingYang
//
//  Created by ddy on 2017/8/11.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CheckVoucherFirstVC.h"
#import "CouponsLiveVC.h"
#import "VideoSingleVC.h"
#import "SecondsKillVC.h"
#import "UniversalGoodsListVC.h"
#import "ClassificationVC.h"
//#import "EditorShareVC.h"
#import "NewCreateShareVC.h"
#import "TBNewsViewController.h"
#import "CheckVoucherFirstVC.h"
#import "GoodsDetailsVC.h"
#import "MessageVC.h"
#import "NewRoadVC.h"
#import "FreeOrderDetailVC.h"

#import "TBNewsViewController.h"
#import "FreeOrderVC.h"
#import "JingDongGoodsVC.h"
#import "NewTeamVC.h"
#import "ActivityModel.h"
#import "NewOrmdVC.h"
#import "VideoTeachingVC.h"
#import "NewConsumer.h"



@interface PageJump : NSObject


+(PageJump*)sharedInstance ;

/**
 首页模块化跳转
 
 @param model 模块化的需往另外 model
 */
+(void)pushAccordingToThe:(MClassModel*)model ;


/**
 进入商品详情

 @param model 商品model
 @param type   0：一般商品页面  1：直播  2：视频单
 */
+(void)pushToGoodsDetail:(GoodsModel*)model pushType:(NSInteger)type ;


/**
 进入商品详情(传商品id和平台)

 @param goodsId 商品id
 @param source 商品来源
 */
+(void)pushToGoodsId:(NSString *)goodsId goodsSource:(NSString*)source;


/**
 点击去领券

 @param goodsMd 商品model
 */
+(void)takeOrder:(GoodsModel*)goodsMd ;


/**
 分享赚钱

 @param goodsMd 商品model
 @param ary 商品图片数组(列表页可为空)
 */
+(void)shareGoods:(GoodsModel*)goodsMd goodsImgAry:(NSMutableArray*)ary; //分享


/**
 判断用户是否已二次授权

 @param completeState 1:已授权 0:授权失败
 */
+(void)appUser_TaoBaoShouQuan:(void(^)(NSInteger shqState))completeState;


/**
 服务器推送事件跳转

 @param type 推送类型
 @param content 额外参数()
 */
+(void)yunPushType:(NSInteger)type content:(NSString*)content ;


//活动跳转
+(void)pushToActivity:(ActivityModel*)model;

+(void)pushChat;

/**
 打开系统视频播放器

 @param url 视频地址
 */
+(void)openVideoUrlBySystemVideoPlayer:(NSString*)url ;

@end
























