//
//  ClassificationModel.m
//  DingDingYang
//
//  Created by ddy on 2017/3/3.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ClassificationModel.h"

@implementation ClassificationModel
-(instancetype)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self) {
        self.cid=REPLACENULL(dic[@"id"]) ;
        self.imgUrl=FORMATSTR(@"%@%@",dns_dynamic_loadimg,REPLACENULL(dic[@"img"])) ;
        self.title=REPLACENULL(dic[@"name"]);
    }
    return self;
}
@end
