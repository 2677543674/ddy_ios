//
//  BHModel.h
//  DingDingYang
//
//  Created by ddy on 2017/5/15.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BHModel : NSObject

@property (nonatomic,strong) UIColor *bgColor;
@property(nonatomic,copy) NSString * refid;  // 商品ID
@property(nonatomic,copy) NSString * link;   // 链接
@property(nonatomic,copy) NSString * img;    // 广告图片链接
@property(nonatomic,copy) NSString * bigImg; // 广告图片链接
@property(nonatomic,copy) NSString * ifNew;  // 广告图片链接
@property(nonatomic,assign) NSInteger todo;  // 行为：1、商品详情页 2、h5链接
@property(nonatomic,copy) NSString * des;
@property(nonatomic,strong) GoodsModel * goodsMd; // 商品信息
@property(nonatomic,strong) MClassModel * mClassModel; // 跳转信息

-(instancetype)initWithDic:(NSDictionary*)dic;
-(instancetype)initWithWph:(NSDictionary*)dic;
@end




