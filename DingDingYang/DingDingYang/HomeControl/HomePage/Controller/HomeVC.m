//
//  HomeVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/1.
//  Copyright © 2017年 ddy.All rights reserved.
//


#import "HomeVC.h"
#import "ClassificationModel.h"
#import "SearchGoodsVC.h"
#import "MessageVC.h"
#import "CouponsLiveVC.h"
#import "GoodsListCTCeel.h"
#import "NewRoadVC.h"
#import "VideoSingleVC.h"

//模块化
#import "ASingleHomeOneVC.h"
#import "HSearchView1.h"
#import "TheInterestRateView.h"
#import <YYWebImage.h>
#import "ActivityModel.h"
#import "PopActivity.h"

#import "PopupWindowView.h"
#import "UpdateAppView.h"

@interface HomeVC ()<UIScrollViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,ListCTCellDelegate>{
    NSInteger pageNum;//标记页码
}

@property(nonatomic,strong) UITableView * tableView;
@property(nonatomic,strong) NSMutableArray * classAry;//分类数据
@property(nonatomic,strong) UITextField * searchField;
@property(nonatomic,strong) NSMutableArray * classTitleAry;
@property(nonatomic,strong) UIImageView * lineImg;//底部线条

@property(nonatomic,strong) UICollectionView * titleCollectionV, * glCollectionV ;
@property(nonatomic,strong) NSMutableArray * allDataAry ;

@property(nonatomic,strong) NSIndexPath * pathIndexS ; //记录当前应该选中行
@property(nonatomic,strong) NSMutableArray * offsetYAry ;//记录缓存上次滑动的位置
@property(nonatomic,strong) ActivityModel * model; // 活动的model

//模块化
@property(nonatomic,strong) ASingleHomeOneVC * aSingleHomeVC ;
@property(nonatomic,copy) NSString * searchStyle ; //搜索栏样式
@property(nonatomic,assign) NSInteger ifSortOn ; //是否显示顶部分类样式



@property(nonatomic,strong) UIView * blackView;
@property(nonatomic,strong) YYAnimatedImageView* floatImageView;

@property(nonatomic,strong)UIImageView*IntroImageView;
@property(nonatomic,strong)NSArray *IntroImageArray;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,assign)float height;

@property(nonatomic,assign) NSInteger goodsListStyle ;

//判断HomeSTCell字体颜色
@property (nonatomic,assign) NSInteger stCellColorStyle; // 0 白色 1 黑色
@end

@implementation HomeVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    NSLog(@"%@",self.navigationController.viewControllers);
    if (![[self.navigationController.viewControllers lastObject] isKindOfClass:[GoodsDetailsVC class]] ) {
        [self.navigationController setNavigationBarHidden:NO animated: animated];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    self.title = @"首页" ;
    
    
    // 初始化数据
    _classAry = [[NSMutableArray alloc]init];
    _classTitleAry = [[NSMutableArray alloc]init];
    _allDataAry = [NSMutableArray array];
    _offsetYAry = [NSMutableArray array];
    
    // 创建首页动态模块view
    _aSingleHomeVC = [[ASingleHomeOneVC alloc]init];
    _aSingleHomeVC.superVC = self;
    [self addChildViewController:_aSingleHomeVC];
    
    
    // 判断有无缓存
    if (NSUDTakeData(app_dynamic_home_dic)!=nil) {
        NSString * now_time = NowTimeByFormat(@"yyyy-MM-dd") ;
        NSString * last_time = REPLACENULL(NSUDTakeData(app_dynamic_home_time));
        NSInteger result = [now_time compare:last_time]; // 0为相同 !0为不同
        [self toDealWithData:NSUDTakeData(app_dynamic_home_dic)];
        if (result!=0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestClassType:NO];
            });
        }
    }else{ [self requestClassType:YES]; }
    
#warning ce_shi
//#if DEBUG
//    [self requestClassType:YES];
//#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessAndReqAgain) name:LoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:UserInfoChange object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(launchAdClickPop) name:ClickPop object:nil];

    
    if (_launchDic!=nil) {
        // 获取推送数据
        NSInteger type = [REPLACENULL(_launchDic[@"type"]) integerValue] ;
        if (type>0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self pushType:type content:REPLACENULL(_launchDic[@"content"])];
            });
        }
    }else{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateApp];
        });
        [self popActivity];
    }
    
}


#pragma mark === >>> 接收本地通知
-(void)netWorkChange:(NSNotification*)state{
    NSInteger ns = [state.object integerValue];
    if (ns==1||ns==2) { SHOWMSG(@"当前网络不可用!") }
    if (ns==3||ns==4) {
        if (NSUDTakeData(app_dynamic_home_dic)==nil) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestClassType:YES];
            });
        }
    }
}

-(void)changeState{
    if (NSUDTakeData(app_dynamic_home_dic)!=nil) {
        [self toDealWithData:NSUDTakeData(app_dynamic_home_dic)];
    }else{
        [_allDataAry removeAllObjects];
        [_offsetYAry removeAllObjects];
        for (NSInteger i=0; i<_classAry.count; i++) {
            ClassificationModel*model = _classAry[i];
            NSDictionary *dic = @{ @"pdic":@{@"st":@"1",@"catId":model.cid},
                                   @"goodslist":@[] };
            [_allDataAry addObject:dic];
            [_offsetYAry addObject:@""];
        }
        [_glCollectionV reloadData];
    }
}

// 登录成功，重新获取首页动态模块
-(void)loginSuccessAndReqAgain{
    [self requestClassType:NO];
}

#pragma mark ===>>> 请求首页数据
-(void)requestClassType:(BOOL)show{
    if (show) { MBShow(@"正在加载...") }else{ if(NSUDTakeData(app_dynamic_home_dic)==nil){ MBShow(@"正在加载...") } }
    // 获取模块化布局样式 ， 一天获取一次， 获取失败，先找缓存，无缓存，展示无网络界面,点击可重新调用此方法
    NSLog(@"OU == %@", url_home_category);
    [NetRequest requestType:0 url:url_home_category ptdic:nil success:^(id nwdic) {
        MBHideHUD
        NSString * time = NowTimeByFormat(@"yyyy-MM-dd") ;
        NSUDSaveData(time, app_dynamic_home_time)
        NSDictionary * dic = NULLDIC(nwdic);
        NSLog(@"OU == %@", dic);
        if ([[dic allKeys] containsObject:@"list0"]||[[dic allKeys] containsObject:@"list1"]) {
            NSUDSaveData(NULLDIC(nwdic), app_dynamic_home_dic)
        }
        [self toDealWithData:NULLDIC(nwdic)];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD
        [self requestClassTypeError:failure];
    }];
    
}

-(void)requestClassTypeError:(NSString*)str{
    
    if (NSUDTakeData(app_dynamic_home_dic)!=nil) {
        [self toDealWithData:NSUDTakeData(app_dynamic_home_dic)];
    }else{
        SHOWMSG(str)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(netWorkChange:) name:NetworkChange  object:nil];
        [NoNetworkView showNoNetView:self.view chs:^(NSInteger click) {
            [self requestClassType:YES];
        }];
    }
    
}

// 解析模块数据
-(void)toDealWithData:(NSDictionary*)dic{
    
    [_classAry removeAllObjects];
    [_classTitleAry removeAllObjects];
    [_allDataAry removeAllObjects];
    [_offsetYAry removeAllObjects];
    
    NSDictionary * homeDic = [NSDictionary dictionary];
    
#warning 强制list0 默认=0
    if (role_state==0) {
        homeDic = NULLDIC(dic[@"list0"]);
    }else{
        homeDic = NULLDIC(dic[@"list1"]);
    }
    
    NSUDSaveData(REPLACENULL(homeDic[@"ifNew"]), @"Home_ifNew")
    
    #pragma mark ==>> 背景图
    if (_liningView == nil) {
        _liningView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, NAVCBAR_HEIGHT+150)];
        _liningView.backgroundColor = COLORWHITE;
        [self.view addSubview:_liningView];
        // 画多边形
        CGSize finalSize = CGSizeMake(ScreenW, NAVCBAR_HEIGHT+150);
        CAShapeLayer *layer = [[CAShapeLayer alloc]init];
        UIBezierPath *bezier = [[UIBezierPath alloc]init];
        [bezier moveToPoint:CGPointMake(finalSize.width, 0)];
        [bezier addLineToPoint:CGPointMake(0, 0)];
        [bezier addLineToPoint:CGPointMake(0, NAVCBAR_HEIGHT+150-50)];
        [bezier addQuadCurveToPoint:CGPointMake(ScreenW/2, NAVCBAR_HEIGHT+150) controlPoint:CGPointMake(ScreenW/4, NAVCBAR_HEIGHT+150)];
        [bezier addQuadCurveToPoint:CGPointMake(ScreenW, NAVCBAR_HEIGHT+150-50) controlPoint:CGPointMake(ScreenW*3/4, NAVCBAR_HEIGHT+150)];
        [bezier addLineToPoint:CGPointMake(ScreenW, 0)];

        layer.path = bezier.CGPath;
        _liningView.layer.mask = layer;
    }
    
    #pragma mark ==>> 开始设置首页
    // 创建搜索栏
    if (_searchView1==nil) {
        _searchView1 = [[HSearchView1 alloc]initWithFrame:CGRectMake(0, 0, ScreenW, NAVCBAR_HEIGHT)];
        [self.view addSubview:_searchView1];
    }
    
    // 搜索栏样式
    _searchView1.searchBarStyle = REPLACENULL(homeDic[@"searchStyle"]) ;
    // 是否在首页显示分类
    _ifSortOn = [REPLACENULL(homeDic[@"ifSortOn"]) integerValue];
    
    // 首页分块样式数组 、商品列表样式、是否使用新的yangshi
    _aSingleHomeVC.stytleMAry = NULLARY(homeDic[@"list"]) ;
    
    NSArray * styleAry = NULLARY(homeDic[@"list"]);
    _aSingleHomeVC.styleModelAry = [NSMutableArray array];
    [styleAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ModuleModel * model = [[ModuleModel alloc]initWith:obj];
        [_aSingleHomeVC.styleModelAry addObject:model];
    }];
    
    NSString * goodsListStyle = REPLACENULL(homeDic[@"goodsListStyle"]);
    if ([goodsListStyle isEqual:@"3-1-0-1"]) {
        _aSingleHomeVC.goodsListStyle = 1;
    }else if ([goodsListStyle isEqual:@"3-1-0-2"]){
        _aSingleHomeVC.goodsListStyle = 2;
    }else if ([goodsListStyle isEqual:@"3-1-0-3"]){
        _aSingleHomeVC.goodsListStyle = 3;
    }else if ([goodsListStyle isEqual:@"3-1-0-4"]){
        _aSingleHomeVC.goodsListStyle = 4;
    }else if([goodsListStyle isEqual:@"3-1-0-5"]){
        _aSingleHomeVC.goodsListStyle = 5;
    }else if([goodsListStyle isEqual:@"3-1-0-6"]){
        _aSingleHomeVC.goodsListStyle = 6;
    }else if([goodsListStyle isEqual:@"3-1-0-7"]){
        _aSingleHomeVC.goodsListStyle = 7;
    }else if([goodsListStyle isEqual:@"3-1-0-8"]){
        _aSingleHomeVC.goodsListStyle = 8;
    }else{
        _aSingleHomeVC.goodsListStyle = 1;
    }
    
    _aSingleHomeVC.ifNew = [REPLACENULL(homeDic[@"ifNew"]) integerValue];
    
    if (PID==10102) { _aSingleHomeVC.goodsListStyle = 7; } // 叮当首页固定样式
   
    if (_ifSortOn==1) { // 展示分类，则解析分类数据，
        if (_titleCollectionV) { _titleCollectionV.hidden = NO; }
        if (_glCollectionV) { _glCollectionV.hidden = NO; }
        if (PID==10107) { // 叮叮羊
            [_classTitleAry addObject:@"今日精选"];
        }else{ [_classTitleAry addObject:@"首页"]; }
        
        // 首页分类
        NSArray * ary = NULLARY(homeDic[@"sortList"]) ;
        [ary enumerateObjectsUsingBlock:^(id    obj, NSUInteger idx, BOOL *   stop) {
            ClassificationModel*model=[[ClassificationModel alloc]initWithDic:obj];
            [_classAry addObject:model];
        }];
        // 首页展示分类时，将记录所有分类列表
        [_classAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ClassificationModel*model = obj ;
            [_classTitleAry addObject:model.title];
            NSDictionary *dic = @{ @"pdic":@{@"st":@"1",@"catId":model.cid},
                                   @"goodslist":@[] } ;
            [_allDataAry addObject:dic];
            [_offsetYAry addObject:@""];
        }];
        // 设置动态view的大小
        _aSingleHomeVC.cltFrame = CGRectMake(0, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT-TABBAR_HEIGHT-38) ;
        [self titleCollectionV];
        [self glCollectionV];
//        if (_aSingleHomeVC.homeCltView) { [_aSingleHomeVC.homeCltView reloadData]; }
    }else{
        if (_titleCollectionV) { _titleCollectionV.hidden = YES ; }
        if (_glCollectionV) { _glCollectionV.hidden = YES ; }
        
        // 宝拉甄选、省见、惠得、叮叮羊微集、花生LK、叮当叮当
        if (PID==10106||PID==10025||PID==10110||PID==10107||PID==888||PID==10102) {
            // 首页顶部有透明，banner加高
            if (IS_IPHONE_X) {
                if(NSUDTakeData(HomeOneList)){
                    _aSingleHomeVC.cltFrame = CGRectMake(0, -44, ScreenW, ScreenH-TABBAR_HEIGHT+44) ;
                }else{
                    _aSingleHomeVC.cltFrame = CGRectMake(0, 0, ScreenW, ScreenH-TABBAR_HEIGHT) ;
                }
            }else{
                if(NSUDTakeData(HomeOneList)){
                    _aSingleHomeVC.cltFrame = CGRectMake(0, -20, ScreenW, ScreenH-TABBAR_HEIGHT+20) ;
                }else{
                    _aSingleHomeVC.cltFrame = CGRectMake(0, 0, ScreenW, ScreenH-TABBAR_HEIGHT) ;
                }
            }
            _aSingleHomeVC.view.frame = CGRectMake(0, 0, ScreenW, ScreenH);
            _searchView1.backgroundColor=[UIColor clearColor];
            [self.view addSubview:_aSingleHomeVC.view];
            [self.view bringSubviewToFront:_searchView1];
//            [_aSingleHomeVC.homeCltView.mj_header beginRefreshing];
        }else{
            _aSingleHomeVC.cltFrame = CGRectMake(0, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT-TABBAR_HEIGHT) ;
            _aSingleHomeVC.view.frame = CGRectMake(0, NAVCBAR_HEIGHT, ScreenW, ScreenH-NAVCBAR_HEIGHT);
            [self.view addSubview:_aSingleHomeVC.view];
//            [_aSingleHomeVC.homeCltView.mj_header beginRefreshing];
        }
//        if (_aSingleHomeVC.homeCltView) { [_aSingleHomeVC.homeCltView reloadData]; }
        
    }
}



#pragma mark //** 若首页不在顶部显示分类选项，以下有关界面的代码都不会被调用 **//

#pragma mark === >>> 分类标题 UICollectionView
-(UICollectionView*)titleCollectionV{
    if (!_titleCollectionV) {
        // 默认标题颜色为白色 0
        _stCellColorStyle = 0;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _titleCollectionV = [[UICollectionView alloc]initWithFrame:CGRectMake(0, NAVCBAR_HEIGHT, ScreenW, 38) collectionViewLayout:layout];
        _titleCollectionV.delegate = self ;
        _titleCollectionV.dataSource = self ;
//        _titleCollectionV.backgroundColor = COLORWHITE ;
        _titleCollectionV.backgroundColor = COLORCLEAR;
        _titleCollectionV.showsHorizontalScrollIndicator = NO ;
        [self.view addSubview:_titleCollectionV];
        [_titleCollectionV registerClass:[HomeSTCell class] forCellWithReuseIdentifier:@"homestcell"];
        
        [_titleCollectionV addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        
        _lineImg = [[UIImageView alloc]init];
        _lineImg.backgroundColor = LOGOCOLOR ;
        float wd = StringSize(@"首页", 15*HWB).width + 20 ;
        _lineImg.center = CGPointMake(wd/2, 35);
        _lineImg.bounds = CGRectMake(0, 0, 35, 1);
        [_titleCollectionV addSubview:_lineImg];
        
        if (THEMECOLOR==2) {  _lineImg.hidden = YES ; }
        
    }else{
        [_titleCollectionV reloadData];
    }
    
    return _titleCollectionV ;
    
}

#pragma mark === >>>  首页商品列表(横向切换、类似今日头条)
-(UICollectionView*)glCollectionV{
    if (!_glCollectionV) {
        
        // 表格布局加载图片
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal ;
        _glCollectionV = [[UICollectionView alloc]initWithFrame:CGRectMake(0, NAVCBAR_HEIGHT+38, ScreenW, ScreenH-(NAVCBAR_HEIGHT+38)-TABBAR_HEIGHT) collectionViewLayout:layout];
        _glCollectionV.delegate = self ;
        _glCollectionV.dataSource = self ;
        _glCollectionV.backgroundColor = COLORCLEAR ;
        _glCollectionV.pagingEnabled = YES ;
        _glCollectionV.showsHorizontalScrollIndicator = NO ;
        [self.view addSubview:_glCollectionV];
        [_glCollectionV registerClass:[EmptyCtCell class] forCellWithReuseIdentifier:@"emptyctcell"];
        [_glCollectionV registerClass:[GoodsListCTCeel class] forCellWithReuseIdentifier:@"goodslistctcell"];
        
        [_glCollectionV addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        
    }else{
        [_glCollectionV reloadData];
    }

    return _glCollectionV ;
}


#pragma mark === >>> UICollectionViewCell 代理
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _classTitleAry.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==_glCollectionV) {
        if (indexPath.row==0) {
            EmptyCtCell * emptycell = [collectionView dequeueReusableCellWithReuseIdentifier:@"emptyctcell" forIndexPath:indexPath];
#pragma mark == >> 若首页显示分类、在此更换首页
            [emptycell.contentView addSubview:_aSingleHomeVC.view];
            return emptycell ;
        }
        GoodsListCTCeel * listCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"goodslistctcell" forIndexPath:indexPath];
        listCell.delegate = self ;
        listCell.tag = indexPath.row + 9 ;
        listCell.allDic = _allDataAry[indexPath.row-1] ;
        listCell.scrollY = _offsetYAry[indexPath.row-1] ;
        return listCell ;
    }
    
    HomeSTCell * stcell = [collectionView dequeueReusableCellWithReuseIdentifier:@"homestcell" forIndexPath:indexPath];
    stcell.textLab.text = _classTitleAry[indexPath.row];
    switch (_stCellColorStyle) {
        case 0:
            stcell.textLab.textColor = COLORWHITE;
            break;
        case 1:
            stcell.textLab.textColor = COLORBLACK;
            break;
        default:
            break;
    }
//    stcell.textLab.backgroundColor = COLORWHITE ;
    stcell.textLab.font = [UIFont systemFontOfSize:14*HWB];
    if (indexPath.row==_pathIndexS.row) {
        [UIView animateWithDuration:0.26 animations:^{
            switch (_stCellColorStyle) {
                case 0:
                    stcell.textLab.textColor = COLORWHITE;
                    break;
                case 1:
                    stcell.textLab.textColor = COLORBLACK;
                    break;
                default:
                    break;
            }
            stcell.textLab.font = [UIFont boldSystemFontOfSize:16*HWB];
            _lineImg.center = CGPointMake(stcell.center.x,35);
            
            if (THEMECOLOR==2) {//主题色为浅色
                stcell.textLab.textColor = Black51 ;
                stcell.textLab.backgroundColor = LOGOCOLOR ;
            }
            
        }completion:^(BOOL finished) {
            _titleCollectionV.userInteractionEnabled = YES ;
        }];
    }
    return stcell ;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==_titleCollectionV) {
        if (indexPath==_pathIndexS) { return ; }
        _pathIndexS = indexPath ;
        [self changeFrame];
    }
}

#pragma mark === >>> UIScrollView 滑动事件代理
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (scrollView==_glCollectionV) {
        _titleCollectionV.userInteractionEnabled = NO ;
    }else{
        _titleCollectionV.userInteractionEnabled = YES ;
    }
}

//手指滑动结束
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == _glCollectionV) {
        NSInteger row1 =  _glCollectionV.contentOffset.x/ScreenW ;
        _pathIndexS = [NSIndexPath indexPathForItem:row1 inSection:0];
        [self changeFrame];
    }
}

-(void)changeFrame{
    [_titleCollectionV reloadData];
    [_titleCollectionV scrollToItemAtIndexPath:_pathIndexS atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [_glCollectionV scrollToItemAtIndexPath:_pathIndexS atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary*)change context:(void *)context {
    if (object == _glCollectionV) {
        // 如果是这个对象就可以获得contentOffset的值然后判断是正或者负，来判断上拉下拉。
        CGPoint point = [((NSValue *)[_glCollectionV valueForKey:@"contentOffset"]) CGPointValue];
        NSLog(@"_glCollectionV：x=%f\ny=%f", point.x, point.y);
        
        //banner 高度
        CGFloat h = 0;
        if (PID==10107) {// 叮叮羊
            h = ScreenW/2.273;
        }else if (PID==10106||PID==10025||PID==10110||PID==10107||PID==888||PID==10102) {
            // 宝拉甄选、省见、惠得、叮叮羊微集、花生LK、叮当叮当
            h = ScreenW*215/375;
        }else{
            h = ScreenW/2.5;
        }
        if (self.aSingleHomeVC.homeCltView.contentOffset.y >= h/3*1) {
        }else{
            self.liningView.alpha = (ScreenW-point.x)/ScreenW;
        }
        
        if (point.x >= ScreenW/3*1) {
            // 字体变黑
            [self changeHomeSTCellTitleColor:1];
        }else{
            //banner 高度
            CGFloat h = 0;
            if (PID==10107) {// 叮叮羊
                h = ScreenW/2.273;
            }else if (PID==10106||PID==10025||PID==10110||PID==10107||PID==888||PID==10102) {
                // 宝拉甄选、省见、惠得、叮叮羊微集、花生LK、叮当叮当
                h = ScreenW*215/375;
            }else{
                h = ScreenW/2.5;
            }
            if (self.aSingleHomeVC.homeCltView.contentOffset.y >= h/3*1) {
            }else{
                // 字体变白
                [self changeHomeSTCellTitleColor:0];
            }
        }
        
    }else if (object == _titleCollectionV) {
        // 如果是这个对象就可以获得contentOffset的值然后判断是正或者负，来判断上拉下拉。
        CGPoint point = [((NSValue *)[_titleCollectionV valueForKey:@"contentOffset"]) CGPointValue];
        NSLog(@"_titleCollectionV：x=%f\ny=%f", point.x, point.y);
    }else if (object == _aSingleHomeVC.homeCltView) {
        // 如果是这个对象就可以获得contentOffset的值然后判断是正或者负，来判断上拉下拉。
        CGPoint point = [((NSValue *)[_aSingleHomeVC.homeCltView valueForKey:@"contentOffset"]) CGPointValue];
        NSLog(@"_aSingleHomeVC.homeCltView：x=%f\ny=%f", point.x, point.y);
    }
    
}

//分类商品数据回调，缓存到数组
-(void)gctCell:(GoodsListCTCeel *)ctcell nowClassTypeAllData:(NSDictionary *)datadic{
    [_allDataAry replaceObjectAtIndex:ctcell.tag-10 withObject:datadic];
}

-(void)nowCell:(GoodsListCTCeel *)ctcell offsetY:(NSString *)floaty{
    [_offsetYAry replaceObjectAtIndex:ctcell.tag-10 withObject:floaty];
}


#pragma mark === >>> UICollectionViewLayout 布局代理

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==_titleCollectionV) {
        float w = StringSize(_classTitleAry[indexPath.row], 14*HWB).width + 28 ;
        return CGSizeMake(w, 38) ;
    }
    return CGSizeMake(ScreenW, ScreenH-(NAVCBAR_HEIGHT+38)-TABBAR_HEIGHT);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}


#pragma mark === >>> 根据不同的推送类型跳转不同的界面
-(void)pushType:(NSInteger)type content:(NSString*)content {
    if (TopNavc!=nil) { [PageJump yunPushType:type content:content]; }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma mark ===>>> 首页弹窗广告和浮动广告有关
// 版本更新点击了引导页的跳过
-(void)launchAdClickPop{
    NSLog(@"点击了跳过。。。");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showHomeAd];
    });
}

-(void)popActivity{
    // 必转:10108、叮当叮当:10102、实惠佳:10050、掌中购:10051、省见:10025、蜜赚:10104、贝贝赞:10033、聚点:10044、宝拉:10106、惠得:10110
    if (PID==10108||PID==10102||PID==10050||PID==10051||PID==10025||PID==10104||PID==10033||PID==10044||PID==10106||PID==10110) {
        if ([NSUDTakeData(Launch_last_app_version) isEqualToString:now_app_version]) {
            float showTimes = 1.0 ;
            if (NSUDTakeData(ad_launch_imgUrl)) { showTimes = 3.8; }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(showTimes * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showHomeAd];
            });
        }
    }else{
        float showTimes = 0.8 ;
        if (NSUDTakeData(ad_launch_imgUrl)) { showTimes = 3.8; }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(showTimes * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showHomeAd];
        });
    }
}

-(void)showHomeAd{
    @try {
        if (CANOPENWechat) {
            [PopupWindowView showAlertIfHaveAd];
            if (NSUDTakeData(ad_floating_dic)) {
                NSDictionary * dic = NULLDIC(NSUDTakeData(ad_floating_dic));
                _model = [[ActivityModel alloc]initWithDic:dic];
                [self floatImageView];
            }
        }
    } @catch (NSException *exception) { } @finally {  }
}

-(YYAnimatedImageView*)floatImageView{
    if (!_floatImageView) {
        @try {
            _floatImageView = [[YYAnimatedImageView alloc]init];
            _floatImageView.userInteractionEnabled = YES;
            _floatImageView.backgroundColor = [UIColor clearColor];
            _floatImageView.contentMode = UIViewContentModeScaleAspectFit;
            UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewTap)];
            UIPanGestureRecognizer * pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(pan:)];
            [_floatImageView addGestureRecognizer:tap];
            [_floatImageView addGestureRecognizer:pan];
            
            __block HomeVC * selfView = self;
            [_floatImageView yy_setImageWithURL:[NSURL URLWithString:_model.img] placeholder:nil options:YYWebImageOptionProgressiveBlur |YYWebImageOptionShowNetworkActivity progress:nil transform:nil completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
                if (image) {
                    NSLog(@"%@",image);
                    float imageHeight = image.size.height;
                    float imageWidth = image.size.width;
                    if (imageHeight>10&&imageWidth>10) {
                        float width = 42*HWB;
                        float height = width*imageHeight/imageWidth;
                        selfView.height = height;
                        [selfView.view addSubview:selfView.floatImageView];
                        [selfView.view bringSubviewToFront:selfView.floatImageView];
                        selfView.floatImageView.frame=CGRectMake((ScreenW-8*HWB-width), ScreenH-TABBAR_HEIGHT-height-42*HWB, width, height);
                    }
                }else{
                    _floatImageView = nil;
                }
            }];
        } @catch (NSException *exception) { } @finally { }
    }
    return _floatImageView;
}

-(void)pan:(UIPanGestureRecognizer *)rec{
    @try {
        CGPoint point = [rec translationInView:self.view];
        rec.view.center = CGPointMake(rec.view.center.x + point.x, rec.view.center.y + point.y);
        [rec setTranslation:CGPointMake(0, 0) inView:self.view];
        
        if (rec.state == UIGestureRecognizerStateEnded ||rec.state == UIGestureRecognizerStateCancelled) {
            [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0.7 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                if (rec.view.center.y>ScreenH-TABBAR_HEIGHT||rec.view.center.y<0) {
                    rec.view.center=CGPointMake(ScreenW-28*HWB,ScreenH-TABBAR_HEIGHT-_height/2-10);
                }else{
                    if (rec.view.center.x>ScreenW/2) {
                        rec.view.center=CGPointMake(ScreenW-28*HWB,rec.view.center.y);
                    }else{
                        rec.view.center=CGPointMake(28*HWB,rec.view.center.y);
                    }
                }
            } completion:nil];
        }
    } @catch (NSException *exception) { } @finally { }
    
}

-(void)imageViewTap{
    [PageJump pushToActivity:_model];
}


#pragma mark === >>> 检查更新
- (void)updateApp{
    
    [MyPageJump newcheckAppUpdateBlock:^(NSInteger stateCode, NSString *downLoadUrl, NSString *updateContent, NSString *newVersion) {
        switch (stateCode) { // 0:有更新  1:强制更新
            case 0:{ [UpdateAppView showUpdateType:0 updateContent:updateContent updateUrl:downLoadUrl newVersion:newVersion]; } break;
            case 1:{ [UpdateAppView showUpdateType:1 updateContent:updateContent updateUrl:downLoadUrl newVersion:newVersion]; } break;
            default: break;
        }
    }];
    
}

// 改变homestcell的文字颜色
- (void)changeHomeSTCellTitleColor:(NSInteger)colorType {
    _stCellColorStyle = colorType;
    [_titleCollectionV reloadData];
}

// 改变liningView的背景颜色
- (void)changeLiningViewBgColor:(UIColor *)bgColor {
    [UIView animateWithDuration:0.25 animations:^{
        self.liningView.backgroundColor = bgColor;
    }];
}

@end
