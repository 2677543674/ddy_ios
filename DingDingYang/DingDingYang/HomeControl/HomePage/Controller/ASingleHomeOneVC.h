//
//  ASingleHomeOneVC.h
//  DingDingYang
//
//  Created by ddy on 2017/8/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class HomeVC;

@interface ASingleHomeOneVC : DDYViewController

@property(nonatomic,assign) CGRect cltFrame;
@property(nonatomic,strong) NSArray * stytleMAry ; // 第1分区样式
@property(nonatomic,assign) NSInteger goodsListStyle; // 列表样式
@property(nonatomic,assign) NSInteger  ifNew;

@property(nonatomic,strong) NSMutableArray * classAty , * fourAry ;

@property(nonatomic,strong) UICollectionView * homeCltView ;
@property(nonatomic,strong) NSMutableArray * styleModelAry; // 分层的样式model

@property (nonatomic,strong) HomeVC *superVC;
@end




