//
//  HomeVC.h
//  DingDingYang
//
//  Created by ddy on 2017/3/1.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSearchView1.h"
@interface HomeVC : DDYViewController

@property(nonatomic,strong) NSDictionary * launchDic ;
@property(nonatomic,strong) HSearchView1 * searchView1 ;
@property (nonatomic,strong) UIView *liningView;

// 改变homestcell的文字颜色
- (void)changeHomeSTCellTitleColor:(NSInteger)colorType;

// 改变liningView的背景颜色
- (void)changeLiningViewBgColor:(UIColor *)bgColor;

@end
