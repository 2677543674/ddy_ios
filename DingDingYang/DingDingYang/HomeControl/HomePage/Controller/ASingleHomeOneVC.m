//
//  ASingleHomeOneVC.m
//  DingDingYang
//
//  Created by ddy on 2017/8/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ASingleHomeOneVC.h"

#import "HOneCltCell1.h"  // banner
#import "HTwoCltCell1.h"  // 五个或十个的小图标分类
#import "HNewsCltCell.h"  // 首页头条功能

// 矩阵
#import "HLayerMatrixCltCell.h"
#import "HLayerCarouselCltCell.h"

// 模块(1~5个的)
#import "HLayerOneCltCell1.h"
#import "HLayerTwoCltCell1.h"
#import "HLayerThreeCltCell1.h"
#import "HLayerFourCltCell1.h"
#import "HLyaerFvieCltCell1.h"

#import "GoodsListCltCell1.h" // 商品列表

#import "HjptjCltv.h"
#import <YYWebImage.h>
#import "TheInterestRateView.h" // 七天收益图view

@interface ASingleHomeOneVC () <UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionViewFlowLayout * homeLayout ;
@property(nonatomic,strong) NSMutableArray * listAry ; //商品列表数据
@property(nonatomic,strong) NSMutableArray * bannerAry ; //轮播图数据
@property(nonatomic,assign) NSInteger  pageNum ;
@property(nonatomic,strong) UIButton * topBtn ;
@property(nonatomic,strong)TheInterestRateView *interestRateView;
//可变样式控制参数
//@property(nonatomic,copy) NSString * searchStyle ; //搜索栏样式
//@property(nonatomic,assign) NSInteger ifSortOn ; //是否开启顶部分类样式

@property(nonatomic,assign) float oneViewHWBili ; // 一层视图的



@end

@implementation ASingleHomeOneVC


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"首页";
    _pageNum = 2 ;
    _bannerAry = [[NSMutableArray alloc]init];
    _listAry = [[NSMutableArray alloc]init];
    _oneViewHWBili = 2.5 ;
    _classAty = [[NSMutableArray alloc]init];
    _fourAry = [[NSMutableArray alloc]init];
    _styleModelAry = [NSMutableArray array];
    
    [_stytleMAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ModuleModel * model = [[ModuleModel alloc]initWith:obj];
        [_styleModelAry addObject:model];
    }];
    
    self.view.backgroundColor = COLORCLEAR ;

    [self homeCltView];
    [self topBtn];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState) name:LoginSuccess object:nil];


}

-(UICollectionView*)homeCltView{
    if (!_homeCltView) {
        
        _homeLayout = [[UICollectionViewFlowLayout alloc]init];
        _homeCltView = [[UICollectionView alloc]initWithFrame:_cltFrame collectionViewLayout:_homeLayout];
        _homeCltView.delegate = self ;
        _homeCltView.dataSource = self ;
        _homeCltView.backgroundColor = COLORCLEAR;
        _homeCltView.showsVerticalScrollIndicator = NO ;
        [self.view addSubview:_homeCltView];
        
        [_homeCltView registerClass:[HOneCltCell1 class] forCellWithReuseIdentifier:@"hone_clt_cell1"];
        [_homeCltView registerClass:[HTwoCltCell1 class] forCellWithReuseIdentifier:@"htwo_clt_cell1"];
        [_homeCltView registerClass:[HNewsCltCell class] forCellWithReuseIdentifier:@"h_news_clt_cell"];
        
        [_homeCltView registerClass:[AllEmptyDataCltCell class] forCellWithReuseIdentifier:@"empty_clt_cell"];
        
        [_homeCltView registerClass:[HLayerMatrixCltCell   class] forCellWithReuseIdentifier:@"hlmatrix_cell"];
        [_homeCltView registerClass:[HLayerCarouselCltCell   class] forCellWithReuseIdentifier:@"hlcarousel_cell"];
        
        [_homeCltView registerClass:[HLayerOneCltCell1   class] forCellWithReuseIdentifier:@"hlone_cell1"];
        
        [_homeCltView registerClass:[HLayerTwoCltCell1   class] forCellWithReuseIdentifier:@"hltwo_cell1"];
        [_homeCltView registerClass:[HLayerTwoCltCell2   class] forCellWithReuseIdentifier:@"hltwo_cell2"];
        
        [_homeCltView registerClass:[HLayerThreeCltCell1 class] forCellWithReuseIdentifier:@"hlthree_cell1"];
        [_homeCltView registerClass:[HLayerThreeCltCell2 class] forCellWithReuseIdentifier:@"hlthree_cell2"];
        
        [_homeCltView registerClass:[HLayerFourCltCell1  class] forCellWithReuseIdentifier:@"hlfour_cell1"];
        [_homeCltView registerClass:[HLayerFourCltCell2  class] forCellWithReuseIdentifier:@"hlfour_cell2"];
        [_homeCltView registerClass:[HLayerFourCltCell3  class] forCellWithReuseIdentifier:@"hlfour_cell3"];
        
        [_homeCltView registerClass:[HLyaerFvieCltCell1  class] forCellWithReuseIdentifier:@"hlfive_cell1"];
        [_homeCltView registerClass:[HLyaerFvieCltCell2  class] forCellWithReuseIdentifier:@"hlfive_cell2"];
        [_homeCltView registerClass:[HLyaerFvieCltCell3  class] forCellWithReuseIdentifier:@"hlfive_cell3"];
        
        [_homeCltView registerClass:[HjptjCltv           class] forCellWithReuseIdentifier:@"h_jptj_cell"];
        [_homeCltView registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"goods_listclt_cell1"];
        [_homeCltView registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"goods_listclt_cell2"];
        [_homeCltView registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"goods_listclt_cell3"];
        [_homeCltView registerClass:[GoodsListCltCell4   class] forCellWithReuseIdentifier:@"goods_listclt_cell4"];
        [_homeCltView registerClass:[GoodsListCltCell5   class] forCellWithReuseIdentifier:@"goods_listclt_cell5"];
        [_homeCltView registerClass:[GoodsListCltCell6   class] forCellWithReuseIdentifier:@"goods_listclt_cell6"];
        [_homeCltView registerClass:[GoodsListCltCell7   class] forCellWithReuseIdentifier:@"goods_listclt_cell7"];
        [_homeCltView registerClass:[GoodsListCltCell10  class] forCellWithReuseIdentifier:@"goods_listclt_cell10"];
        
        [_homeCltView registerClass:[TheInterestRateView class] forCellWithReuseIdentifier:@"theInterestRateView"];
        
        [_homeCltView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        
        if (NSUDTakeData(HomeOneList)!=nil) {
            [self parsingOngPageDic:NSUDTakeData(HomeOneList)];
        }
        
        // 下拉刷新
        __weak ASingleHomeOneVC * SelfView = self;
        
        _homeCltView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [SelfView loadOneHomeData];
        }];
        _homeCltView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [SelfView loadMoreData];
        }];
        

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_homeCltView) { [_homeCltView.mj_header beginRefreshing]; }
        });
        
    }else{ [_homeCltView reloadData]; }
    return _homeCltView;
    
}

-(void)changeState{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [_homeCltView reloadData];
        [_homeCltView.mj_header beginRefreshing];
    });
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LoginSuccess object:nil];
}

#pragma mark === >>> 加载首页商品数据
-(void)loadOneHomeData{

    [_homeCltView.mj_footer resetNoMoreData];
    [NetRequest requestType:0 url:url_index_home ptdic:nil success:^(id nwdic) {
    
        NSUDSaveData(nwdic,HomeOneList);
        [self parsingOngPageDic:nwdic];

    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        [_homeCltView.mj_header endRefreshing];
        [self loadMoreData];
    }];

}

-(void)parsingOngPageDic:(NSDictionary*)nwdic{
    
    if (_bannerAry.count>0) {
        [_bannerAry removeAllObjects];
        [_homeCltView reloadData];
    }
    if (_listAry.count>0) {
        [_listAry removeAllObjects];
        [_homeCltView reloadData];
    }
    
    // 轮播图数据
    NSArray * banner = NULLARY(nwdic[@"bannerList"]) ;
    if (banner.count>0) {
        [banner enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            BHModel * bmodel = [[BHModel alloc]initWithDic:obj];
            [_bannerAry addObject:bmodel]; 
        }];
        [_homeCltView reloadData];
    }
    
    // 列表数据
    NSArray * lAry = NULLARY(nwdic[@"list"]);
    if (lAry.count>0) {
        [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
            GoodsModel * model = [[GoodsModel alloc]initWithDic:obj];
            [_listAry addObject:model] ;
        }];
    }else{  [self loadMoreData]; }
    
    [_homeCltView.mj_header endRefreshing];

    [_homeCltView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (_homeCltView) { [_homeCltView reloadData]; }
    });
    
}

// 加载更多
-(void)loadMoreData{
    NSString * page = FORMATSTR(@"%ld",(long)_pageNum);
    [NetRequest requestTokenURL:url_goods_list2 parameter:@{@"st":page} success:^(NSDictionary *nwdic) {
        if([nwdic[@"result"] isEqualToString:@"OK"]){
            NSArray*lAry = NULLARY( nwdic[@"list"] ) ;
            if (lAry.count>0) {
                _pageNum++;
                
                [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
                    GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
                    [_listAry addObject:model];
                }];
                [_homeCltView.mj_footer endRefreshing];
            }else{
                [_homeCltView.mj_footer endRefreshingWithNoMoreData];
            }
            
        }else{
            [_homeCltView.mj_footer endRefreshing];
            SHOWMSG(nwdic[@"result"])
        }
        
        [_homeCltView reloadData];
        
    } failure:^(NSString *failure) {
        [_homeCltView.mj_footer endRefreshing];
        SHOWMSG(failure)
    }];
}

#pragma mark - observe
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary*)change context:(void *)context {
    if (object == _homeCltView) {
        // 如果是这个对象就可以获得contentOffset的值然后判断是正或者负，来判断上拉下拉。
        CGPoint point = [((NSValue *)[_homeCltView valueForKey:@"contentOffset"]) CGPointValue];
//        NSLog(@"x=%f\ny=%f", point.x, point.y);
        
        //banner 高度
        CGFloat h = 0;
        if (PID==10107) {// 叮叮羊
            h = ScreenW/2.273;
        }else if (PID==10106||PID==10025||PID==10110||PID==10107||PID==888||PID==10102) {
            // 宝拉甄选、省见、惠得、叮叮羊微集、花生LK、叮当叮当
            h = ScreenW*215/375;
        }else if (_bannerAry.count==0) {
            h = 1;
        }else{
            h = ScreenW/2.5;
        }
        
        self.superVC.liningView.alpha = (h-point.y)/h;
        
        if (point.y >= h/3*1) {
            // 字体变黑
            [self.superVC changeHomeSTCellTitleColor:1];
        }else{
            // 字体变白
            [self.superVC changeHomeSTCellTitleColor:0];
        }
    }
}

#pragma mark ===>>> UICollectionView代理

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 4 ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0||section==2) {  return 1 ;  }
    if (section==1) {  return _styleModelAry.count ;  }
    return  _listAry.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section==0) { // 首页banner
        HOneCltCell1 * oneclt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hone_clt_cell1" forIndexPath:indexPath];
        oneclt1.changeImgCallBack = ^(UIColor *bgColor) {
            // 如果超过范围就不改变
            [self.superVC changeLiningViewBgColor:bgColor];
        };
        oneclt1.dataAry = _bannerAry;  return oneclt1;
    }
    
    if (indexPath.section==1) {
        
        if (_styleModelAry.count>indexPath.item) {
            ModuleModel * model = _styleModelAry[indexPath.item];
            switch (model.styleTag) {
                    
                case 0:{ // @"2-1-1-20"
                    HLayerCarouselCltCell * hltwoClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlcarousel_cell" forIndexPath:indexPath];
                    hltwoClt1.dataAry = [model.moduleClassAry mutableCopy];
                    return hltwoClt1;
                } break;
                   
                case 1:{ // @"2-1-1-21"
                    HLayerMatrixCltCell * hltwoClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlmatrix_cell" forIndexPath:indexPath];
                    hltwoClt1.rowCount = 1;
                    hltwoClt1.bgColor = model.bgColor;
                    hltwoClt1.dataAry = model.moduleClassAry;
                    return hltwoClt1;
                } break;
                    
                case 2:{ // @"2-1-1-22"
                    HLayerMatrixCltCell * hltwoClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlmatrix_cell" forIndexPath:indexPath];
                    hltwoClt1.rowCount = 2;
                    hltwoClt1.bgColor = model.bgColor;
                    hltwoClt1.dataAry = model.moduleClassAry;
                    return hltwoClt1;
                } break;
                    
                case 3:{ // @"2-1-1-23"
                    HLayerMatrixCltCell * hltwoClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlmatrix_cell" forIndexPath:indexPath];
                    hltwoClt1.rowCount = 3;
                    hltwoClt1.bgColor = model.bgColor;
                    
                    hltwoClt1.dataAry = model.moduleClassAry;
                    return hltwoClt1;
                } break;
                    
                case 4:{ // @"2-1-1-24"
                    HLayerMatrixCltCell * hltwoClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlmatrix_cell" forIndexPath:indexPath];
                    hltwoClt1.rowCount = 4;
                    hltwoClt1.bgColor = model.bgColor;
                    hltwoClt1.dataAry = model.moduleClassAry;
                    return hltwoClt1;
                } break;
                    
                case 10:{ // @"2-1-2"
                    
                    HLayerOneCltCell1 * hloneClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlone_cell1" forIndexPath:indexPath];
                    hloneClt1.dataAry = model.moduleClassAry;
                    if (hloneClt1.classImgV.image!=nil) {
                        if (model.layoutCGSize.height==120) {
                            @try {
                                float onebl = hloneClt1.classImgV.image.size.width/hloneClt1.classImgV.image.size.height;
                                [_homeCltView reloadItemsAtIndexPaths:@[indexPath]];
                                
                                model.layoutCGSize = CGSizeMake(ScreenW,ScreenW/onebl);
                            } @catch (NSException *exception) {
                                [_homeCltView reloadData];
//                                [_homeCltView reloadSections:[NSIndexSet indexSetWithIndex:1]];
                            } @finally { }
                        }
                    }else{
                        @try {
                            MClassModel * classmodel = (MClassModel*)model.moduleClassAry[0];
                            NSString * imgUrl = _ifNew?classmodel.nImgs:classmodel.img;
                            [NetRequest downloadImageByUrl:imgUrl success:^(id result) {
                                UIImage * image = (UIImage*)result;
                                if (image!=nil) {
                                    // iPhoneX刷新偶儿会闪退
                                    @try {
                                        float onebl = image.size.width/image.size.height;
                                        [_homeCltView reloadItemsAtIndexPaths:@[indexPath]];
                                        
                                        model.layoutCGSize = CGSizeMake(ScreenW,ScreenW/onebl);
                                    } @catch (NSException *exception) {
                                        [_homeCltView reloadData];
                                    } @finally { }
                                }
                            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { }];
                        } @catch (NSException *exception) {
                            [_homeCltView reloadSections:[NSIndexSet indexSetWithIndex:1]];
                        } @finally { }
                        
                    }
                    
                    return hloneClt1;
                    
                } break;
                    
                case 20:{ // @"2-2-1"
                    
                    HLayerTwoCltCell1 * hltwoClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hltwo_cell1" forIndexPath:indexPath];
                    hltwoClt1.dataAry = model.moduleClassAry;  return hltwoClt1;
                    
                } break;
                    
                    //******  三个主题的  ******//
                    
                case 30:{ // @"2-3-1"
                    
                    HLayerThreeCltCell1 * hlthreeClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlthree_cell1" forIndexPath:indexPath];
                    hlthreeClt1.dataAry = model.moduleClassAry;  return hlthreeClt1;
                    
                } break;
                    
                case 31:{ // @"2-3-1-1"
                    
                    HLayerThreeCltCell2 * hlthreeClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlthree_cell2" forIndexPath:indexPath];
                    hlthreeClt2.dataAry = model.moduleClassAry;  return hlthreeClt2;
                    
                } break;
                    
                    //******  四个主题的  ******//
                
                case 40:{ // @"2-4-1"
                    
                    HLayerFourCltCell1 * hlfourClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlfour_cell1" forIndexPath:indexPath];
                    hlfourClt1.dataAry = model.moduleClassAry;  return hlfourClt1;
                    
                } break;
                    
                case 41:{ // @"2-4-1-1"
                    
                    HLayerFourCltCell3 * hlfourClt3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlfour_cell3" forIndexPath:indexPath];
                    hlfourClt3.dataAry = model.moduleClassAry;  return hlfourClt3;
                    
                } break;
                    
                case 42:{ // @"2-4-1-2"
                    
                    HLayerFourCltCell2 * hlfourClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlfour_cell2" forIndexPath:indexPath];
                    hlfourClt2.dataAry = model.moduleClassAry;  return hlfourClt2;
                    
                } break;
                    
                    //******  五个主题的  ******//
                    
                case 50:{ //  @"2-5-1"
                    
                    HLyaerFvieCltCell1 * hlfiveClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlfive_cell1" forIndexPath:indexPath];
                    hlfiveClt1.dataAry = model.moduleClassAry;  return hlfiveClt1;
                    
                } break;
                    
                case 51:{ // @"2-5-1-1"
                    
                    HLyaerFvieCltCell2 * hlfiveClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlfive_cell2" forIndexPath:indexPath];
                    hlfiveClt2.dataAry = model.moduleClassAry;  return hlfiveClt2;
                    
                } break;
                    
                case 52:{ // @"2-5-1-2"
                    
                    HLyaerFvieCltCell3 * hlfiveClt3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"hlfive_cell3" forIndexPath:indexPath];
                    hlfiveClt3.dataAry = model.moduleClassAry;  return hlfiveClt3;
                    
                } break;
                    
//#warning ce_shi
                    //******  头条推荐  ******//

                case 60:{ // @"2-1-1-1"

                    HNewsCltCell * newsCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"h_news_clt_cell" forIndexPath:indexPath];
                    if (model.moduleClassAry.count>0) {
                        newsCell.mcModel = model.moduleClassAry[0];
                    }else{
                        newsCell.mModel = model;
                    }
                    
                    return newsCell;

                } break;
                    
                    //******  达人系的七天收益  ******//
                    
                case 70:{ // @"7-6-1-0"
                    
                    TheInterestRateView * interestRateView = [collectionView dequeueReusableCellWithReuseIdentifier:@"theInterestRateView" forIndexPath:indexPath];
                    return interestRateView;
                    
                } break;
                    
                    //******  小方块分类的  ******//
                    
                case 105:{ // @"2-5-0"
                    
                    HTwoCltCell1 * twoClassClt = [collectionView dequeueReusableCellWithReuseIdentifier:@"htwo_clt_cell1" forIndexPath:indexPath];
                    twoClassClt.dataAry = model.moduleClassAry;  return twoClassClt;
                    
                } break;
                    
                case 110:{ // @"2-10-0"
                    
                    HTwoCltCell1 * twoClassClt = [collectionView dequeueReusableCellWithReuseIdentifier:@"htwo_clt_cell1" forIndexPath:indexPath];
                    twoClassClt.dataAry = model.moduleClassAry;  return twoClassClt;
                    
                } break;
                    
                default:{ // 防止其他类型的
                    
                    AllEmptyDataCltCell * emptyCltCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"empty_clt_cell" forIndexPath:indexPath];
                    return emptyCltCell;
                    
                } break;
            }
            
        }else {
            [_homeCltView reloadData];
        }
    }
    
    
    if (indexPath.section==2) {
        HjptjCltv * jptjClt = [collectionView dequeueReusableCellWithReuseIdentifier:@"h_jptj_cell" forIndexPath:indexPath];
        return jptjClt ;
    }
    
    // 部分序号命名和后台不一致，并且有的是某些app单独使用的
    switch (_goodsListStyle) {
        case 2:{
            GoodsListCltCell3 * goodsListClt3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell3" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt3.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt3;
        } break;
            
        case 3:{
            GoodsListCltCell2 * goodsListClt2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell2" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt2.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt2;
        } break;
            
        case 4:{
            GoodsListCltCell4 * goodsListClt4 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell4" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt4.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt4;
        } break;
            
        case 5:{
            GoodsListCltCell5 * goodsListClt5 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell5" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt5.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt5;
        } break;
            
        case 6:{
            GoodsListCltCell6 * goodsListClt6 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell6" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt6.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt6 ;
        } break;
            
        case 7:{
            GoodsListCltCell7 * goodsListClt7 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell7" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt7.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt7 ;
        } break;
            
        case 8:{
            GoodsListCltCell10 * goodsListClt10 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell10" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt10.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt10 ;
        } break;
    
        default:{
            GoodsListCltCell1 * goodsListClt1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell1" forIndexPath:indexPath];
            if (_listAry.count>indexPath.item) { goodsListClt1.goodsModel = _listAry[indexPath.item]; } else { [_homeCltView reloadData]; }
            return goodsListClt1;
        } break;
    }
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==3){
        [PageJump pushToGoodsDetail:_listAry[indexPath.row] pushType:0];
    }
}

#pragma mark === >>> UICollectionViewLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section==0) {
        
        if (PID==10107) { return  CGSizeMake(ScreenW, ScreenW/2.273); } // 叮叮羊
        
        // 宝拉甄选、省见、惠得、叮叮羊微集、花生LK、叮当叮当
        if (PID==10106||PID==10025||PID==10110||PID==10107||PID==888||PID==10102) {
            return  CGSizeMake(ScreenW, ScreenW*215/375);
        }
        
        if (_bannerAry.count==0) { return  CGSizeMake(ScreenW, 1); }

        return  CGSizeMake(ScreenW, ScreenW/2.5);
    }
    
    if (indexPath.section==1) {
        ModuleModel * model = _styleModelAry[indexPath.item];
        if (model.styleTag == 3) {
            return CGSizeMake(ScreenW, 176);
        }
        
        if (model.styleTag == 0) {
            return  CGSizeMake(ScreenW, model.moduleClassAry.count > 1 ? (ScreenW - 20) * 130.0 / 375 : 100);
        }
        
        if (indexPath.item<_styleModelAry.count) {
            ModuleModel * model = _styleModelAry[indexPath.item];
            return model.layoutCGSize;
        }else{
            return CGSizeZero;
        }
    }
    
    if (indexPath.section==2) { return CGSizeMake(ScreenW, 35*HWB); }
    
    // 部分序号命名和后台不一致
    switch (_goodsListStyle) {
        case 2:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+70*HWB); } break;
        case 4:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
        case 5:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+(role_state==0?70*HWB:90*HWB)); } break;
        case 6:{ return CGSizeMake(ScreenW, 116*HWB); } break;
        case 7:{ return CGSizeMake(ScreenW, 104*HWB); } break;
        case 8:{ return CGSizeMake(ScreenW/2-2,ScreenW/2+90*HWB); } break;
        default:{ return CGSizeMake(ScreenW, FloatKeepInt(100*HWB) ); } break;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==1) { return 5*HWB; }
    if (section==3) { if(_goodsListStyle==2||_goodsListStyle==4||_goodsListStyle==5) { return 5 ; } }
    return 1 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (section==3) { if(_goodsListStyle == 2||_goodsListStyle == 4||_goodsListStyle == 5) { return 4 ; } }
    return 0;
}

-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
        _topBtn.frame = CGRectMake(ScreenW-36*HWB-10,YBOTTOM(_homeCltView)-36*HWB-10 , 36*HWB, 36*HWB);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];

    }
    return _topBtn ;
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    // 宝拉甄选、省见、惠得、花生LK、叮当叮当
    if (PID==10106||PID==10025||PID==10110||PID==888||PID==10102) {
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        if ([window.rootViewController isKindOfClass:[MainTabBarVC class]]) {
            MainTabBarVC * mian = (MainTabBarVC*)window.rootViewController ;
            if (_homeCltView.frame.origin.y==0) {
                if (_homeCltView.contentOffset.y>0) {
                    NSInteger style = [[[mian.homeVC.searchView1.searchBarStyle componentsSeparatedByString:@"-"] lastObject] integerValue];
                    if (style==2) { // 白色底色
                        mian.homeVC.searchView1.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:(_homeCltView.contentOffset.y)/(ScreenH*0.3)];
                    }else{
                        mian.homeVC.searchView1.backgroundColor=[LOGOCOLOR colorWithAlphaComponent:(_homeCltView.contentOffset.y)/(ScreenH*0.3)];
                    }
                }else{
                    mian.homeVC.searchView1.backgroundColor=[UIColor clearColor];
                }
            }else{
                if (_homeCltView.contentOffset.y+20>0) {
                    NSInteger style = [[[mian.homeVC.searchView1.searchBarStyle componentsSeparatedByString:@"-"] lastObject] integerValue];
                    if (style==2) { //白色底色
                        mian.homeVC.searchView1.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:(_homeCltView.contentOffset.y+20)/(ScreenH*0.3)];
                    }else{
                        mian.homeVC.searchView1.backgroundColor=[LOGOCOLOR colorWithAlphaComponent:(_homeCltView.contentOffset.y+20)/(ScreenH*0.3)];
                    }
                }else{
                    mian.homeVC.searchView1.backgroundColor=[UIColor clearColor];
                }
            }
        }
    }

    if (_homeCltView.contentOffset.y>=0) {
        if (_homeCltView.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_homeCltView.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _homeCltView.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
    }
}
-(void)clickBackTop{
    [_homeCltView setContentOffset:CGPointZero animated:YES];
}


@end
