//
//  NewCreateShareVC.h
//  DingDingYang
//
//  Created by ddy on 2018/7/23.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoodsModel;
@interface NewCreateShareVC : DDYViewController

@property(nonatomic,strong) GoodsModel * model;
@property(nonatomic,assign) NSInteger shareType; // 2:京东 3:拼多多

@property(nonatomic,strong) NSMutableArray * gimgUrlAry;

@end



//带有图片和文字的按钮
@interface ShareButtonGoods : UIButton
-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName;
@property(nonatomic,strong)UIImageView*imgView;
@property(nonatomic,strong)UILabel*titleLab;
@property(nonatomic,copy)NSString*ImgName,*TitleName;

@end


