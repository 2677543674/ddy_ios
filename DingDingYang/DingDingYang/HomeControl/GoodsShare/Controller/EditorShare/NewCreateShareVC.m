//
//  NewCreateShareVC.m
//  DingDingYang
//
//  Created by ddy on 2018/7/23.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "NewCreateShareVC.h"
#import "EditorSelectImgCell.h"
#import "SGSImgModel.h"
#import "GoodsRewardRulesVC.h"
#import <Social/Social.h>
#import "ShareInstructionsVC.h"
#import "CompositePosterImg.h"
#import "CreateShareImageView.h"
#import "SelectPosterVC.h"
#import "SelectContentVC.h"
#import "ShareTheTemplateVC.h"
#import "CreateShareImageView6.h"

@interface NewCreateShareVC () <UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate>

// 美文和美图选择仅花生LK会有

@property(nonatomic,strong) UIView * tbgView; // 顶部view
@property(nonatomic,strong) UILabel * jlyjMoneyLab; // 预计奖励
@property(nonatomic,strong) UILabel * yxcountLab;   // 已选图片个数
@property(nonatomic,strong) UICollectionView * selectImgCltV; // 图片选择

@property(nonatomic,strong) UIView * centerBtnView;
@property(nonatomic,strong) UIButton * remdBtn; // 合成图是否包含推荐语
@property(nonatomic,strong) UIButton * saleBtn; // 合成图是否包含销量
@property(nonatomic,strong) UIButton * minipBtn;  // 生成小程序码
@property(nonatomic,strong) UIButton * posterBtn; // 生成普通海报

@property(nonatomic,strong) UITextView * shareContentView2; // 显示分享文案
@property(nonatomic,strong) ShareButtonGoods * shareBtn;
@property(nonatomic,strong) UIView * shareBgView;

@property(nonatomic,strong) NSMutableArray * shareImgAry; // 存放model(包含图片，二维码和已选状态)
@property(nonatomic,strong) NSMutableArray * dataImgAry; // 遍历并存放最终已选择的图片
@property(nonatomic,assign) BOOL ifRem;  // 是否有推荐语
@property(nonatomic,assign) BOOL ifSale; // 是否包含销量
@property(nonatomic,assign) NSInteger posterNum; // 生成的海报数量
@property(nonatomic,strong) UIImage * image;
@property(nonatomic,assign) NSInteger sharePosterType; // 海报模板
@property(nonatomic,strong) NSMutableDictionary * parameterDic;

@property(nonatomic,strong) UIImage * headImg; // 个人头像
@property(nonatomic,strong) UIImage * shopImg; // 店铺头像
@property(nonatomic,strong) UIImage * miniCodeImg; // 小程序码

@property(nonatomic,strong) UIActivityIndicatorView * activityView;


@end

@implementation NewCreateShareVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//#if HuaShengLk
//    [NetRequest requestType:0 url:url_shareTxt_temple ptdic:nil success:^(id nwdic) {
//        if (REPLACENULL(nwdic[@"selPic"]).length!=0) {
//            _sharePosterType = [REPLACENULL(nwdic[@"selPic"]) integerValue];
//        }else{ _sharePosterType = 1; }
//    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
//         _sharePosterType = 1;
//    }];
//#endif
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORWHITE;
     self.title = @"创建分享";
    
    _posterNum = 0; _ifRem = YES; _ifSale = YES;
    
    _dataImgAry = [NSMutableArray array];
    _shareImgAry = [NSMutableArray array];
    _parameterDic = [NSMutableDictionary dictionary];
 
//    if (PID==888) { [self rightBtn]; }
    
    // 获取图片，组合成model，标记或记录已选项
    for (NSInteger i=0; i<_gimgUrlAry.count ; i++) {
        SGSImgModel * model = [[SGSImgModel alloc]init];
        [model setValue:_gimgUrlAry[i] forKey:@"urlImg"];
        if (i==0) { [model setValue:@"1" forKey:@"isSelect"]; }
        [_shareImgAry addObject:model];
    }
    
    if (role_state!=0) {  [self tbgView];  }
    
    [self selectImgCltV];  // 分享图片选择
    [self centerBtnView];  //
    [self shareBgView];    // 分享按钮
    [self createShareContentView];  // 分享内容输入框
   
    [self huoquShareData]; // 分享数据

    //注册键盘高度变化通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

// 键盘高度监测
-(void)keyboardFrameChange:(NSNotification*)keyboardInfo{
    NSDictionary * KBInfo = [keyboardInfo userInfo];
    NSValue *aValue = [KBInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat  keyBoardY=keyboardRect.origin.y-20;
    if (YBOTTOM([self.view viewWithTag:666])+NAVCBAR_HEIGHT>keyBoardY) {
        [UIView animateWithDuration:0.26 animations:^{
            self.view.frame=CGRectMake(0,-(YBOTTOM([self.view viewWithTag:666])-keyBoardY), ScreenW, ScreenH-NAVCBAR_HEIGHT);
        }];
    }
    if (keyboardRect.origin.y >= ScreenH) {
        [UIView animateWithDuration:0.26 animations:^{
            self.view.frame=CGRectMake(0,NAVCBAR_HEIGHT, ScreenW, ScreenH-NAVCBAR_HEIGHT);
        }];
    }
}


#pragma mark ===>>> 美文，海报样式选择 (仅花生LK)

-(void)rightBtn{
    UIBarButtonItem * rightItem1 = [self creatBtnItemWithImage:[UIImage imageNamed:@"shareRight1"] andTag:100];
    UIBarButtonItem * rightItem2 = [self creatBtnItemWithImage:[UIImage imageNamed:@"shareRight2"] andTag:200];
    if (_shareType==3) { // 拼多多
        self.navigationItem.rightBarButtonItems=@[rightItem1];
    }else{
        self.navigationItem.rightBarButtonItems=@[rightItem1,rightItem2];
    }
}

-(UIBarButtonItem*)creatBtnItemWithImage:(UIImage*)image andTag:(NSInteger)tag{
    UIButton * rightBtn = [[UIButton alloc]init];
    rightBtn.tag = tag;
    rightBtn.frame = CGRectMake(0, 0, 30, 30);
    [rightBtn setImage:image forState:UIControlStateNormal];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, -5)];
    rightBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [rightBtn addTarget:self action:@selector(clickRight:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    return rightItem;
}

-(void)clickRight:(UIButton*)btn{
    if (btn.tag==100) {
        SelectPosterVC * posterVC = [SelectPosterVC new];
        PushTo(posterVC, YES);
    }else{
        SelectContentVC * contentVC = [SelectContentVC new];
        contentVC.block = ^{ [self huoquShareData]; };
        PushTo(contentVC, YES);
    }
}


#pragma mark ===>>> 获取分享口令或文案

-(void)huoquShareData{
    
//    MBShow(@"获取文案...")
    _activityView.hidden = NO;
    [_activityView startAnimating];

    _parameterDic[@"couponMoney"] = _model.couponMoney;
    _parameterDic[@"activityId"] = _model.activityId;
    _parameterDic[@"goodsName"] = _model.goodsName;
    _parameterDic[@"endPrice"] = _model.endPrice;
    _parameterDic[@"goodsImg"] = _model.goodsPic;
    _parameterDic[@"goodsId"] = _model.goodsId;
    _parameterDic[@"price"] = _model.price;
    _parameterDic[@"catId"] = _model.catId;
    
    if (_model.itemUrl.length>0) {
        _parameterDic[@"itemUrl"] = _model.itemUrl;
    }
    if ([_model.couponMoney floatValue]==0) {
        _parameterDic[@"ifHaveCoupon"] = @"1"; // 无优惠券时传1
    }else{
        _parameterDic[@"ifHaveCoupon"] = @"0"; // 有优惠券时传0
    }
    
    if (_shareType==3) { // 拼多多文案
        
        // 先获取拼多多的 PID 等信息
        [NetRequest requestType:0 url:url_getInfo_pdd ptdic:nil success:^(id nwdic) {
            
            _parameterDic[@"pid"] = REPLACENULL(nwdic[@"pddPid"]);
            _parameterDic[@"clientId"] = REPLACENULL(nwdic[@"clientId"]);
            _parameterDic[@"parameter"] = REPLACENULL(nwdic[@"parameter"]);
            _parameterDic[@"clientSecret"] = REPLACENULL(nwdic[@"clientSecret"]);
            
            // 获取分享文案
            NSString * pddUrl = url_share_buy_pdd;
            [NetRequest requestType:0 url:pddUrl ptdic:_parameterDic success:^(id nwdic) {
                [_activityView stopAnimating];
                _activityView.hidden = YES;
                _shareContentView2.text = REPLACENULL(nwdic[@"pwd"]);
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                [_activityView stopAnimating];
                _activityView.hidden = YES;
                SHOWMSG(failure)
            }];
            
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            [_activityView stopAnimating];
            _activityView.hidden = YES;
            MBHideHUD SHOWMSG(failure)
        }];

    } else if (_shareType==2){ // 京东
        [NetRequest requestType:0 url:url_goods_share_jd ptdic:@{@"goodsId":_model.goodsId} success:^(id nwdic) {
            MBHideHUD [_activityView stopAnimating];
            _activityView.hidden = YES;
            
            NSString * shareText = REPLACENULL(nwdic[@"shareContent"]);
            if (shareText.length==0) { shareText = SHARECONTENT; }
            _shareContentView2.text = shareText;

        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            [_activityView stopAnimating];
            _activityView.hidden = YES;
            MBHideHUD  SHOWMSG(failure)
        }];
    } else{ // 获取淘宝商品的分享文案
        _parameterDic[@"ifAuth"] = @"1"; // 新接口(淘宝二次授权)
        NSString * url = FORMATSTR(@"%@%@",dns_dynamic_main,url_goods_share_text);
        if (PID==888) { url = FORMATSTR(@"%@%@",dns_dynamic_main,url_goods_new_text); }
        
        [NetRequest requestType:0 url:url ptdic:_parameterDic success:^(id nwdic) {
            
            [_activityView stopAnimating];
            _activityView.hidden = YES;
            
            _shareContentView2.text = REPLACENULL(nwdic[@"pwd"]);
            if ([REPLACENULL(nwdic[@"commission"]) floatValue]>0&&role_state!=0) {
                _jlyjMoneyLab.text = FORMATSTR(@"您的奖励预计为¥%@", ROUNDED(REPLACENULL(nwdic[@"commission"]),2));
            }
            
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            SHOWMSG(failure)
            [_activityView stopAnimating];
            _activityView.hidden = YES;
        }];

    }
}

#pragma mark ===>>> 顶部佣金显示 (消费者不显示)

-(UIView*)tbgView{
    if (!_tbgView) {
        _tbgView = [[UIView alloc]init];
        _tbgView.backgroundColor = ColorRGBA(243.f, 249.f, 234.f, 1.0) ;
        [self.view addSubview:_tbgView];
        [_tbgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.height.offset(26*HWB);
        }];
        
        UIImageView * qianImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"jinqian"]];
        [_tbgView addSubview:qianImg];
        [qianImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_tbgView.mas_centerY);
            make.height.width.offset(14);
            make.left.offset(20);
        }];
        
        _jlyjMoneyLab = [UILabel labText:FORMATSTR(@"您的奖励预计为¥%@",_model.commission) color:RGBA(6.f, 85.f, 0, 1.0) font:11.5*HWB];
        [_tbgView addSubview:_jlyjMoneyLab];
        [_jlyjMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(qianImg.mas_right).offset(3);
            make.centerY.equalTo(_tbgView.mas_centerY);
            make.height.offset(26*HWB);
            make.width.offset(200*HWB);
        }];
        
        UIButton * rulesBtn = [UIButton titLe:@"规则 ›" bgColor:COLORWHITE titColorN:RGBA(6.f, 85.f, 0, 1.0) titColorH:RGBA(6.f, 85.f, 0, 1.0) font:11.5*HWB];
        rulesBtn.backgroundColor = ColorRGBA(243.f, 249.f, 234.f, 1.0);
        rulesBtn.frame = CGRectMake(ScreenW-60, 0, 50, 26*HWB);
        ADDTARGETBUTTON(rulesBtn, showRulesView)
        [_tbgView addSubview:rulesBtn];
    }
    return _tbgView ;
}

-(void)showRulesView{
    GoodsRewardRulesVC * vc = [[GoodsRewardRulesVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark ===>>> 分享图片选择(可能会插入海报)

-(UICollectionView*)selectImgCltV{
    if (!_selectImgCltV) {
        
        float yyy = 30*HWB ;
        if (role_state==0)  { yyy = 5*HWB ; }
        
        UILabel * tishiLab1 = [UILabel labText:@"选择图片" color:Black102 font:12*HWB];
        tishiLab1.frame = CGRectMake(15, yyy , 200, 20);
        [self.view addSubview:tishiLab1];
        
        _yxcountLab = [UILabel labText:@"已选 1 张" color:Black102 font:12*HWB];
        NSString * str = @"已选 1 张";  NSRange rg = {3,1} ;
        _yxcountLab.attributedText = [self attriStr:str range:rg];
        _yxcountLab.frame = CGRectMake(ScreenW-100, yyy, 80, 20);
        _yxcountLab.textAlignment = NSTextAlignmentRight ;
        [self.view addSubview:_yxcountLab];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = CGSizeMake(ScreenW/4, ScreenW/4);
        layout.minimumInteritemSpacing = 10 ;
        
        CGRect sicRect = CGRectMake(15, YBOTTOM(_yxcountLab)+5 , ScreenW-30, ScreenW/4+2);
        _selectImgCltV = [[UICollectionView alloc]initWithFrame:sicRect collectionViewLayout:layout];
        _selectImgCltV.delegate = self;  _selectImgCltV.dataSource = self ;
        _selectImgCltV.showsHorizontalScrollIndicator = NO ;
        _selectImgCltV.backgroundColor = COLORGROUP ;
        [self.view addSubview:_selectImgCltV];
        
        [_selectImgCltV registerClass:[EditorSelectImgCell class] forCellWithReuseIdentifier:@"selectimgcell"];
        
    }
    return _selectImgCltV ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _shareImgAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EditorSelectImgCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"selectimgcell" forIndexPath:indexPath];
    cell.model = _shareImgAry[indexPath.item];
    cell.isSelectbtn.tag = indexPath.item;
    [cell.isSelectbtn addTarget:self action:@selector(clickImgSelect:) forControlEvents:(UIControlEventTouchUpInside)];
    return cell ;
}

// 商品图片选择
-(void)clickImgSelect:(UIButton*)btn{
    
    NSIndexPath * indexP = [NSIndexPath indexPathForItem:btn.tag inSection:0];
    EditorSelectImgCell * cell = (EditorSelectImgCell*)[_selectImgCltV cellForItemAtIndexPath:indexP];
    
    if ([cell isKindOfClass:[EditorSelectImgCell class]]) {
        SGSImgModel * model = _shareImgAry[btn.tag];
        if ([model.isSelect integerValue]==1) {
            [model setValue:@"0" forKey:@"isSelect"];
        }else{
            if (cell.goodsImgV.image==nil) {
                SHOWMSG(@"当前图片未下载完成或下载失败，请稍后再选!")
            }else{
                [model setValue:@"1" forKey:@"isSelect"];
            }
        }
        
        [_selectImgCltV reloadData];
        
        NSInteger isSelectNum = 0;
        for (SGSImgModel * alM in _shareImgAry) {
            if ([alM.isSelect integerValue]==1) {
                isSelectNum ++;
            }
        }
        NSString * str = FORMATSTR(@"已选 %lu 张",(unsigned long)isSelectNum);
        NSRange rg = {3,1} ;
        _yxcountLab.attributedText = [self attriStr:str range:rg];
    }else{ SHOWMSG(@"选择失败!") }

}

-(NSMutableAttributedString*)attriStr:(NSString*)str range:(NSRange)strrange{
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttributes:@{NSForegroundColorAttributeName:THEMECOLOR==2?OtherColor:LOGOCOLOR , NSFontAttributeName:[UIFont systemFontOfSize:16]} range:strrange];
    return dlStr;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [ImgPreviewVC show:TopNavc.topViewController type:0 index:indexPath.item imagesAry:_gimgUrlAry];
}

#pragma mark ===>>> 几个按钮(推荐语、销量、小程序和二维码海报)
-(UIView*)centerBtnView{
    if (!_centerBtnView) {
        
        _centerBtnView = [[UIView alloc]init];
        [self.view addSubview:_centerBtnView];
        _centerBtnView.frame = CGRectMake(0,YBOTTOM(_selectImgCltV),ScreenW, 50*HWB);

        UILabel * shareLabel = [UILabel labText:@"分享文案" color:Black102 font:13*HWB];
        shareLabel.frame = CGRectMake(15, 25*HWB, 60*HWB, 20*HWB);
        [_centerBtnView addSubview:shareLabel];
       
        // 海报上需要的信息
        _remdBtn = [UIButton titLe:@" App推荐" bgColor:COLORWHITE titColorN:Black102 font:10*HWB];
        [_remdBtn setImage:[UIImage imageNamed:@"share_img_sn"] forState:UIControlStateNormal];
        [_remdBtn setImage:[UIImage imageNamed:@"share_img_sh"] forState:UIControlStateSelected];
        _remdBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_centerBtnView addSubview:_remdBtn];
        [_remdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(8*HWB); make.left.offset(15); make.height.offset(15*HWB);
        }];
        
        _saleBtn = [UIButton titLe:@" 销量信息" bgColor:COLORWHITE titColorN:Black102 font:10*HWB];
        [_saleBtn setImage:[UIImage imageNamed:@"share_img_sn"] forState:UIControlStateNormal];
        [_saleBtn setImage:[UIImage imageNamed:@"share_img_sh"] forState:UIControlStateSelected];
        _saleBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_centerBtnView addSubview:_saleBtn];
        [_saleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(15);
            make.centerY.equalTo(_remdBtn.mas_centerY);
            make.left.equalTo(_remdBtn.mas_right).offset(10);
        }];
        
        _remdBtn.selected = YES;  _saleBtn.selected = YES;
        
        // 合成海报的按钮
        _posterBtn = [UIButton titLe:@"二维码海报" bgColor:RGBA(219, 20, 71, 1) titColorN:COLORWHITE font:10*HWB];
        [_posterBtn cornerRadius:13*HWB];
        [_centerBtnView addSubview:_posterBtn];
        [_posterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15);
            make.centerY.equalTo(_centerBtnView.mas_centerY);
            make.height.offset(26*HWB);  make.width.offset(70*HWB);
        }];
        
        _minipBtn = [UIButton titLe:@"小程序码海报" bgColor:COLORWHITE titColorN:RGBA(219, 20, 71, 1) font:10*HWB];
        [_minipBtn boardWidth:1 boardColor:RGBA(219, 20, 71, 1) corner:13*HWB];
        [_centerBtnView addSubview:_minipBtn];
        [_minipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-(70*HWB+25));
            make.centerY.equalTo(_centerBtnView.mas_centerY);
            make.height.offset(26*HWB);  make.width.offset(76*HWB);
        }];
        
        // 给几个按钮添加事件
        _remdBtn.tag = 100; _saleBtn.tag = 200; _posterBtn.tag = 300; _minipBtn.tag = 400;
        
        ADDTARGETBUTTON(_remdBtn,clickGoodsInfoBtn:)
        ADDTARGETBUTTON(_saleBtn,clickGoodsInfoBtn:)
        ADDTARGETBUTTON(_minipBtn,syntheticPosters:)
        ADDTARGETBUTTON(_posterBtn,syntheticPosters:)
        
        _minipBtn.hidden = YES;
        
        if (_shareType==3) { // 拼多多商品, 先检查是否有拼多多小程序
            
            [NetRequest requestType:0 url:url_checkMini_pdd ptdic:nil success:^(id nwdic) {
                if ([REPLACENULL(nwdic[@"ifHave"]) isEqualToString:@"1"]) { // 有
                    _minipBtn.hidden = NO;
                }
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                
            }];
            
            // 省见:10025、叮当:10102、惠得:10110 扫货圈:10035 自动生成二维码海报
            if (PID==10025||PID==10102||PID==10110||PID==10035) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self syntheticPosters:_posterBtn];
                });
            }
            
        } else if(_shareType==2) { // 京东
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self syntheticPosters:_posterBtn];
            });
        }  else{ // 淘宝商品
            
            // 今日买手:10105、贝贝赞:10033、一秒券:10022、超值熊:10038、种草君:10088、豆皮:10020 淘宝商品 要中转小程序码
            if (PID==10105||PID==10033||PID==10022||PID==10038||PID==10088||PID==10020) {
                _minipBtn.hidden = NO;
            }
            
            // 省见:10025、叮当:10102、惠得:10110 扫货圈:10035 自动生成二维码海报
            if (PID==10025||PID==10102||PID==10110||PID==10035) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self syntheticPosters:_posterBtn];
                });
            }
            
        }
    }
    return _centerBtnView;
}

-(void)clickGoodsInfoBtn:(UIButton*)btn{
    btn.selected = !btn.selected;
}

#pragma mark ===>>> 合成海报(二维码、小程序码)
// 合成海报
-(void)syntheticPosters:(UIButton*)posterBtn{
    
    if (posterBtn.selected==YES) { SHOWMSG(@"已经存在一张海报了~") return; }
    
//    posterBtn.userInteractionEnabled = NO;
    
    NSMutableArray * indexAry = [NSMutableArray array];
    for (int i=0; i<_shareImgAry.count; i++) {
        SGSImgModel * model = _shareImgAry[i];
        if ([model.isSelect isEqualToString:@"1"]) {
            // 选中的情况下判断图片是否已生成海报
            if ([model.urlImg isKindOfClass:[UIImage class]]) {
                SHOWMSG(@"请勾选一张没有二维码的图片!") return;
            }
            [indexAry addObject:@(i)];
        }
    }
    
    if (indexAry.count==0) { SHOWMSG(@"请选择图片") return; }
    
    if (_posterNum+indexAry.count>2) { SHOWMSG(@"最多可生成2张海报") return; }
    
    NSString * url = FORMATSTR(@"%@%@",dns_dynamic_main,url_goods_share_posterUrl);
    
    MBShow(@"生成海报...")
    // 需传到服务器的公共参数
    NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
    pdic[@"couponMoney"] = _model.couponMoney;
    pdic[@"goodsName"] = _model.goodsName;
    pdic[@"endPrice"] = _model.endPrice;
    pdic[@"goodsId"] = _model.goodsId;
    pdic[@"price"] = _model.price;
    
     // 小程序码
    if (posterBtn.tag==400) {
        
        if (_shareType==3) {
            url = url_shareMini_pdd;
            [pdic removeAllObjects];
            // 拼多多小程序码只需要一个参数
            pdic[@"goodsId"] = _model.goodsId ;
            
        }else{ // 淘宝中转码
            pdic[@"ifAuth"] = @"1"; // 新接口(淘宝二次授权)
            pdic[@"goodsurl"] = dns_dynamic_goods;
            pdic[@"remnContent"] = _model.desStr;
            pdic[@"goodsPic"] = _model.goodsPic;
            url = dns_dynamic_getmini;
        }
        
        // 获取小程序码
        [NetRequest requestType:0 url:url ptdic:pdic success:^(id nwdic) {
            NSString * miniUrl = REPLACENULL(nwdic[@"imgUrl"]);
            if (miniUrl.length>0) {
                NSString * imgUrl = [miniUrl hasPrefix:@"http"]?miniUrl:FORMATSTR(@"%@%@",dns_dynamic_loadimg,miniUrl);
                [NetRequest downloadImageByUrl:imgUrl success:^(id result) {
                    
                    _miniCodeImg = (UIImage*)result;
                    
                    [self downLoadHeadAndShopImgPosterType:_shareType==3?4:2];
                    
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    MBHideHUD SHOWMSG(failure)
                    _minipBtn.userInteractionEnabled = YES;

                }];
            }else{
                MBHideHUD SHOWMSG(@"小程序码链接获取失败!")
                _minipBtn.userInteractionEnabled = YES;
            }
        
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
             MBHideHUD SHOWMSG(failure)
            _minipBtn.userInteractionEnabled = YES;

        }];
        
    }
    
    // 二维码
    if (posterBtn.tag==300) {
        if (_shareType==3) { // 拼多多商品
            
            [NetRequest requestType:0 url:url_getInfo_pdd ptdic:nil success:^(id nwdic) {
                
                pdic[@"ifBuy"] = @"1";
                pdic[@"goodsImg"] = _model.goodsPic;

                // 其它跟踪用户订单信息有关参数
                pdic[@"pid"] = REPLACENULL(nwdic[@"pddPid"]);
                pdic[@"clientId"] = REPLACENULL(nwdic[@"clientId"]);
                pdic[@"parameter"] = REPLACENULL(nwdic[@"parameter"]);
                pdic[@"clientSecret"] = REPLACENULL(nwdic[@"clientSecret"]);
                
                NSString * reqUrl = url_share_buy_pdd;
                [NetRequest requestType:0 url:reqUrl ptdic:pdic success:^(id nwdic) {
                    
                    // 拼多多二维码
                    _model.url = REPLACENULL(nwdic[@"url"]);
                    [self downLoadHeadAndShopImgPosterType:3];
                    
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    MBHideHUD SHOWMSG(failure)
                    _posterBtn.userInteractionEnabled = YES;
                }];
                
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD SHOWMSG(failure)
                _posterBtn.userInteractionEnabled = YES;
            }];
            
        }else if (_shareType==2) { // 京东商品
            [NetRequest requestType:0 url:url_goods_share_jd ptdic:@{@"goodsId":_model.goodsId} success:^(id nwdic) {
                MBHideHUD
                NSString * shareText = REPLACENULL(nwdic[@"shareContent"]);
                if (shareText.length==0) { shareText = SHARECONTENT; }
                _shareContentView2.text = shareText;
                
                // [a-zA-z]+://[^\s]*
                NSString * str = _shareContentView2.text;
                NSString * zhengzeStr = @"[a-zA-z]+://[^\\s]*";
                NSRegularExpression * regularE = [[NSRegularExpression alloc]initWithPattern:zhengzeStr options:0 error:nil];
                NSArray * ary = [regularE matchesInString:str options:0 range:NSMakeRange(0, str.length)];
                NSMutableArray * orderNo = [[NSMutableArray alloc]init];
                for (NSInteger i=0; i<ary.count; i++) {
                    NSTextCheckingResult * match = ary[i];
                    NSRange range = [match range] ;
                    [orderNo addObject:REPLACENULL([str substringWithRange:range])];
                }
                NSSet * set = [NSSet setWithArray:orderNo];
                NSArray * noOrderAry = [set allObjects];
                NSLog(@"%@",noOrderAry);
                if (noOrderAry.count>0) {
                    _model.url = [noOrderAry lastObject];
//                    _sharePosterType = 6;
                    [self downLoadHeadAndShopImgPosterType:5];
                }else{
                    SHOWMSG(@"海报生成失败!")
                }
                
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD  SHOWMSG(failure)
            }];
            
        }else{ // 淘宝商品
            
            pdic[@"goodsurl"] = dns_dynamic_goods;
            pdic[@"remnContent"] = _model.desStr;
            pdic[@"goodsPic"] = _model.goodsPic;
            pdic[@"ifAuth"] = @"1"; // 新接口(淘宝二次授权)
            [NetRequest requestType:0 url:url ptdic:pdic success:^(id nwdic) {
                NSString * posterUrl = REPLACENULL(nwdic[@"url"]);
                if ([posterUrl hasPrefix:@"http"]) {
                    _model.url = posterUrl;
                    [self downLoadHeadAndShopImgPosterType:1];
                }else{
                    MBHideHUD SHOWMSG(@"商品二维码获取失败!")
                    _posterBtn.userInteractionEnabled = YES;
                }
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD SHOWMSG(failure)
                _posterBtn.userInteractionEnabled = YES;
            }];
           
        }
    }
    
}

// 生成海报类型  1:淘宝二维码海报 2:淘宝商品中转小程序码 3:拼多多二维码海报 4:拼多多小程序码海报 5:京东二维码
-(void)downLoadHeadAndShopImgPosterType:(NSInteger)posterType{
    
    // 先下载个人头像，再下载商铺头像，然后获取商品图，最后合成海报 (图片下载失败，则用默认logo)
    if (NSUDTakeData(UserInfo)) {
        PersonalModel * pmodel = [[PersonalModel alloc]initWithDic:NULLDIC(NSUDTakeData(UserInfo))];
        
        _headImg = [UIImage imageNamed:@"ZZ"];
        _shopImg = [UIImage imageNamed:@"ZZ"];
        
        [NetRequest downloadImageByUrl:pmodel.headImg success:^(id result) {
            
            if (result) {
                _headImg = (UIImage*)result;
                if (_model.shopModel.picPath.length>0) {
                    [NetRequest downloadImageByUrl:_model.shopModel.picPath success:^(id shopResult) {
                        if (shopResult) {
                            _shopImg = (UIImage*)shopResult;
                            [self startSyntheticPosterType:posterType];
                        }else{
                            [self startSyntheticPosterType:posterType];
                        }
                    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                        [self startSyntheticPosterType:posterType];
                    }];
                }else{ [self startSyntheticPosterType:posterType]; }
            }else{ [self startSyntheticPosterType:posterType]; }
            
        }serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            [self startSyntheticPosterType:posterType];
        }];
        
    }else{ [self startSyntheticPosterType:posterType]; }
    
}

-(void)startSyntheticPosterType:(NSInteger)posterType{
    
    NSMutableArray * indexAry = [NSMutableArray array]; // 选中的图片
    for (int i=0; i<_shareImgAry.count; i++) {
        SGSImgModel * model = _shareImgAry[i];
        if ([model.isSelect isEqualToString:@"1"]) {
            [indexAry addObject:@(i)];
        }
    }
    
    if (indexAry.count>0) {
        NSInteger index = [indexAry[0] integerValue];
        if (_gimgUrlAry.count>index) {
            id imgUrl = _gimgUrlAry[index];
            if ([imgUrl isKindOfClass:[NSString class]]) {
                
                [NetRequest downloadImageByUrl:imgUrl success:^(id result) {
                    
                    UIImage * posterImg ;
                    
                    if (result) {
                        switch (posterType) {
                            case 1:{ // 淘宝二维码海报
                                if (_model.url.length>0) {
                                   posterImg = [self asingleGoodsPosters:(UIImage*)result miniCodeImg:nil];
                                }
                            } break;
                                
                            case 2:{ // 淘宝中转小程序码
                                posterImg = [self asingleGoodsPosters:(UIImage*)result miniCodeImg:_miniCodeImg];
                            } break;
                                
                            case 3:{ // 拼多多二维码
                                if (_model.url.length>0) {
                                   posterImg = [self asingleGoodsPosters:(UIImage*)result miniCodeImg:nil];
                                }
                            } break;
                                
                            case 4:{ // 拼多多小程序码
                                posterImg = [self asingleGoodsPosters:(UIImage*)result miniCodeImg:_miniCodeImg];
                            } break;
                                
                            case 5:{ // 京东二维码
                                posterImg = [self asingleGoodsPosters:(UIImage*)result miniCodeImg:nil];
                            } break;
                                
                            default: break;
                        }
                        
                        MBHideHUD
                        if (posterImg) {

                            for (SGSImgModel * oldModel in _shareImgAry) {
                                oldModel.isSelect = @"0";
                            }
                            
                            SGSImgModel * model = [[SGSImgModel alloc]init];
                            model.isSelect = @"1" ;  model.type = @"1";
                            model.urlImg = posterImg;

                            [_shareImgAry insertObject:model atIndex:0];
                            [_gimgUrlAry insertObject:model.urlImg atIndex:0];
                            [_selectImgCltV reloadData];
                            
                            NSString * str = @"已选 1 张";
                            NSRange rg = {3,1}  ;
                            _yxcountLab.attributedText = [self attriStr:str range:rg];
                            
                            if (posterType==1||posterType==3||posterType==5) {
                                _posterBtn.selected = YES;
                                _posterBtn.userInteractionEnabled = YES;
                            }
                            if (posterType==2||posterType==4) {
                                _minipBtn.selected = YES;
                                _minipBtn.userInteractionEnabled = YES;
                            }
                        }
                    }
                    
                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                    _posterBtn.userInteractionEnabled = YES;
                    _minipBtn.userInteractionEnabled = YES;
                    MBHideHUD SHOWMSG(failure)
                }];
                
            }
        }else{
            _posterBtn.userInteractionEnabled = YES;
            _minipBtn.userInteractionEnabled = YES;
            MBHideHUD SHOWMSG(@"获取选择图片失败!")
        }
    }else{
        _posterBtn.userInteractionEnabled = YES;
        _minipBtn.userInteractionEnabled = YES;
        MBHideHUD SHOWMSG(@"未获取到已选图片!")
    }

}


// 合成海报
-(UIImage*)asingleGoodsPosters:(UIImage*)goodsImage miniCodeImg:(UIImage*)miniImg{
    
    @try {
        
        UIImage * shareImage;
        
        switch (_sharePosterType) {
                
            case 2:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.68);
                CreateShareImageView2 * view = [[CreateShareImageView2 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.view addSubview:view];
                view.ifRem = _remdBtn.selected; view.ifSale = _saleBtn.selected;
                view.goodsImage = goodsImage; view.shopHeaderImage = _shopImg;
                if (miniImg) { view.ewmImage = miniImg; }
                view.logoImage = _headImg;
                view.model = _model;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            case 3:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.55);
                CreateShareImageView3 * view = [[CreateShareImageView3 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.view addSubview:view];
                view.ifRem = _remdBtn.selected; view.ifSale = _saleBtn.selected;
                view.goodsImage = goodsImage; view.shopHeaderImage = _shopImg;
                if (miniImg) { view.ewmImage = miniImg; }
                view.logoImage = _headImg;
                view.model = _model;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            case 4:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.55);
                CreateShareImageView4 * view = [[CreateShareImageView4 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.view addSubview:view];
                view.ifRem = _remdBtn.selected; view.ifSale = _saleBtn.selected;
                view.goodsImage = goodsImage; view.shopHeaderImage = _shopImg;
                if (miniImg) { view.ewmImage = miniImg; }
                view.logoImage = _headImg;
                view.model = _model;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            case 5:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.55);
                CreateShareImageView5 * view = [[CreateShareImageView5 alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.view addSubview:view];
                view.ifRem = _remdBtn.selected; view.ifSale = _saleBtn.selected;
                view.goodsImage = goodsImage; view.shopHeaderImage = _shopImg;
                if (miniImg) { view.ewmImage = miniImg; }
                view.logoImage = _headImg;
                view.model = _model;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
            case 6:{
                
                CreateShareImageView6 * view = [[CreateShareImageView6 alloc]initWithFrame:CGRectMake(-2000, -2000, ScreenW, ScreenW*1.42)];
                view.backgroundColor = COLORWHITE;
                [self.view addSubview:view];
                view.goodsImage = goodsImage;
                view.model = _model;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
                
                
            default:{
                
                CGRect rect = CGRectMake(-2000, -2000, ScreenW, ScreenW*1.45);
                CreateShareImageView * view = [[CreateShareImageView alloc]initWithFrame:rect];
                view.backgroundColor = COLORWHITE;  [self.view addSubview:view];
                view.ifRem = _remdBtn.selected; view.ifSale = _saleBtn.selected;
                view.goodsImage = goodsImage; view.shopHeaderImage = _shopImg;
                if (miniImg) { view.ewmImage = miniImg; }
                view.logoImage = _headImg;
                view.model = _model;
                shareImage = [self convertViewToImage:view];
                [view removeFromSuperview];
                
            } break;
        }
        
        if (shareImage) { return shareImage; }else{ return nil; }
        
    } @catch (NSException *exception) {  return nil; } @finally { }
    
}

-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark ===>>> 分享文案有关
-(void)createShareContentView{
    
    _shareContentView2 = [[UITextView alloc]init];
    _shareContentView2.editable = NO;
    _shareContentView2.delegate = self ;
    _shareContentView2.textColor = Black102;
    _shareContentView2.backgroundColor = COLORWHITE ;
    _shareContentView2.font = [UIFont systemFontOfSize:13*HWB];
    _shareContentView2.textContainerInset = UIEdgeInsetsMake(10, 3, 10, 3);
    if (role_state==0) {
        _shareContentView2.frame = CGRectMake(0, 0, ScreenW-40, 190*HWB);
        if (ScreenW>320) { _shareContentView2.frame = CGRectMake(0, 0, ScreenW-40, 220*HWB); }
    }else{
        _shareContentView2.frame = CGRectMake(0, 0, ScreenW-40, 165*HWB);
        if (ScreenW>320) { _shareContentView2.frame = CGRectMake(0, 0, ScreenW-40, 190*HWB); }
    }
    
    CGRect xxRect = CGRectMake(20,YBOTTOM(_centerBtnView),ScreenW-40,HFRAME(_shareContentView2)+1);
    UIView * xuXianView = [self createXuXianViewRect:xxRect];
    [self.view addSubview:xuXianView];
    [xuXianView addSubview:_shareContentView2];
    
    if(_shareType!=3&&_shareType!=2){
        UIButton * editorBtn = [UIButton titLe:@"编辑模板" bgColor:RGBA(255,255,255,0.8) titColorN:RGBA(219, 20, 71, 1) font:13*HWB];
        editorBtn.frame = CGRectMake(XRIGHT(_shareContentView2)-80, YBOTTOM(_shareContentView2)-36, 80, 36);
        ADDTARGETBUTTON(editorBtn, clickEdotor)
        [xuXianView addSubview:editorBtn];
    }
    
    UIButton * copybtn = [UIButton titLe:@"复制分享文案" bgColor:COLORWHITE titColorN:[UIColor redColor] font:12*HWB];
    copybtn.frame = CGRectMake(ScreenW-110, YBOTTOM(xuXianView)+6, 100, 20);
    ADDTARGETBUTTON(copybtn, copyWenAn)
    [self.view addSubview:copybtn];
    
    UILabel * tishiLab = [UILabel labText:@"分享朋友圈时，将复制的文案粘贴在评论里，避免封号" color:Black102 font:11.5*HWB];
    tishiLab.numberOfLines = 2; tishiLab.frame = CGRectMake(20, YBOTTOM(xuXianView)+5, 160*HWB, 30*HWB);
    [self.view addSubview:tishiLab];
    
    _activityView = [[UIActivityIndicatorView alloc]init];
    _activityView.color = COLORGRAY;
    _activityView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    [_shareContentView2 addSubview:_activityView];
    [_activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_shareContentView2.mas_centerX);
        make.centerY.equalTo(_shareContentView2.mas_centerY);
        make.height.with.offset(20*HWB);
    }];
    
}

// 返回一个四周边框都是虚线的view
-(UIView*)createXuXianViewRect:(CGRect)rect{
    UIView* view = [UIView new];
    view.backgroundColor=COLORWHITE;
    view.frame = rect;
    CAShapeLayer * border = [CAShapeLayer layer];
    border.strokeColor = Black204.CGColor;
    border.fillColor = [UIColor clearColor].CGColor;
    border.path = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
    border.frame = view.bounds;
    border.lineWidth = 1.f;
    border.lineDashPattern = @[@8, @4];
    [view.layer addSublayer:border];
    return view;
}

// 进入模板编辑
-(void)clickEdotor{
    ResignFirstResponder
    ShareTheTemplateVC * templateVC = [[ShareTheTemplateVC alloc]init];
    templateVC.changeText = ^(NSInteger result) {
        if (result==1) { [self huoquShareData]; }
    } ;
    PushTo(templateVC, YES)
}
// 复制文案
-(void)copyWenAn{
    if (REPLACENULL(_shareContentView2.text).length>0) {
        [ClipboardMonitoring pasteboard:REPLACENULL(_shareContentView2.text)] ;
        SHOWMSG(@"复制成功!")
    }else{ SHOWMSG(@"复制失败!") }
}

#pragma mark ===>>> 创建分享帮助
-(void)creatShareBangZhu{
    if (NSUDTakeData(Share_goods_TiShi)==nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [UIAlertController showIn:self title:@"分享帮助\n" content:@"查看分享帮助，能帮你快速了解如何使用分享" ctAlignment:NSTextAlignmentCenter btnAry:@[@"不再提示",@"关闭",@"查看"] indexAction:^(NSInteger indexTag) {
                if (indexTag == 0) {
                    NSUDSaveData(Share_goods_TiShi, Share_goods_TiShi)
                }
                if (indexTag==2) {
                    [self clickBangzhu];
                }
            }];
        });
    }
}
-(void)clickBangzhu{
    ShareInstructionsVC * instructionsVC = [[ShareInstructionsVC alloc]init];
    PushTo(instructionsVC, YES)
}

#pragma mark ===>>> 分享按钮创建
-(UIView*)shareBgView{
    if (!_shareBgView) {
        _shareBgView = [[UIView alloc]init];
        [self.view addSubview:_shareBgView];
        [_shareBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);    make.right.offset(0);
            make.bottom.offset(IS_IPHONE_X?-40:0); make.height.offset(60*HWB+40);
        }];
        
        UIImageView * lines = [[UIImageView alloc]init];
        lines.backgroundColor = [UIColor lightGrayColor];
        lines.frame = CGRectMake(20, 11,ScreenW-40 ,1) ;
        [_shareBgView addSubview:lines];
        
        UILabel * textTWLab = [UILabel labText:@"图文分享到" color:Black102 font:12*HWB];
        textTWLab.backgroundColor = COLORWHITE ;
        textTWLab.textAlignment = NSTextAlignmentCenter ;
        textTWLab.center = lines.center;
        textTWLab.bounds = CGRectMake(0, 0, 80, 21);
        [_shareBgView addSubview:textTWLab];
        
        
        NSArray * img = @[@"new1_share_wx",@"new1_share_pyq",@"new1_share_qq",@"new1_share_qqkj",@"new1_share_wb",@"share_more"];
        NSArray * tit = @[@"微信",@"朋友圈",@"QQ",@"空间",@"微博",@"更多"];
        if (IsNeedSinaWeb==2) {
            img = @[@"new1_share_wx",@"new1_share_pyq",@"new1_share_qq",@"new1_share_qqkj",@"share_more"];
            tit = @[@"微信",@"朋友圈",@"QQ",@"空间",@"更多"];
        }
        
        for (NSInteger i = 0 ; i < img.count ; i++ ) {
            _shareBtn = [[ShareButtonGoods alloc]initWithImgName:img[i] titleName:tit[i]];
            _shareBtn.frame  =CGRectMake(ScreenW/img.count*i, 28 , ScreenW/img.count, 60*HWB);
            _shareBtn.tag = i + 10 ;
            [_shareBtn addTarget:self action:@selector(shareType:) forControlEvents:UIControlEventTouchUpInside];
            [_shareBgView addSubview:_shareBtn];
        }
        
    }
    return _shareBgView ;
}

-(void)shareType:(ShareButtonGoods*)btn{
    
    if (btn.tag!=12||btn.tag!=13) {
        [self zhengLiImg];
        if (_dataImgAry.count==0) { SHOWMSG(@"未发现选择图片或图片无效!") return ; }
    }
    
    [self tag:btn.tag];
    
}


-(void)tag:(NSInteger)btg{
    
    if (_shareContentView2.text.length>0) {
        [ClipboardMonitoring pasteboard:REPLACENULL(_shareContentView2.text)];
        SHOWMSG(@"分享文案已复制到粘贴板~")
    }
    
    switch (btg) {
        case  10:{ //微信好友
            if (CANOPENWechat) {
                [self shareToPlatformType:UMSocialPlatformType_WechatSession];
            }else{
                SHOWMSG(@"亲，还没有安装微信哦!")
            }
        }
            break;
            
        case  11:{ //微信朋友圈
            if (CANOPENWechat) {
                [self shareToPlatformType:UMSocialPlatformType_WechatTimeLine];
            }else{
                SHOWMSG(@"亲，还没有安装微信哦!")
            }
        }
            break;
            
        case  12:{//qq好友
            if (CANOPENQQ) {
                [self shareToPlatformType:UMSocialPlatformType_QQ];
            }else{
                SHOWMSG(@"亲，还没有安装QQ哦!")
            }
        }
            break;
            
        case 13:{//空间
            [self shareToPlatformType:UMSocialPlatformType_Qzone];
        }
            break;
            
        case 14:{//微博
            if (IsNeedSinaWeb==1) {
                if ([[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Sina]) {
                    for (int i=0; i<_shareImgAry.count; i++) {
                        SGSImgModel * model = _shareImgAry[i];
                        if ([model.isSelect isEqualToString:@"1"]&&[model.type isEqualToString:@"1"]) {
                            SHOWMSG(@"微博分享请选择二维码海报")
                            return;
                        }
                    }
                    [self shareToPlatformType:UMSocialPlatformType_Sina];
                }else{
                    SHOWMSG(@"亲，还没有安装微博哦!")
                }
            }else{
                [self presentSystemShare];
            }
            
        }
            break;
            
        default:{ [self presentSystemShare]; }
            break;
    }
    
}


-(void)zhengLiImg{
    [_dataImgAry removeAllObjects];
    SDImageCache * cacheImg = [SDImageCache sharedImageCache];
    for (SGSImgModel * model in  _shareImgAry) {
        if ([model.isSelect integerValue]==YES) {
            UIImage * img ;
            if ([model.urlImg isKindOfClass:[UIImage class]]) {
                img = model.urlImg ;
            }else{
                img = [cacheImg imageFromCacheForKey:model.urlImg];
            }
            if (img!=nil) {
                [_dataImgAry addObject:img];
            }
        }
    }
}

#pragma mark === >>> 调用分享友盟
//分享方法
- (void)shareToPlatformType:(UMSocialPlatformType)platformType{
    [ClipboardMonitoring pasteboard:REPLACENULL(_shareContentView2.text)] ;
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    if (platformType==UMSocialPlatformType_QQ) {//qq 只能分享文案或者图片 选择文案
        messageObject.text=_shareContentView2.text;
    }else{
        if (platformType!=UMSocialPlatformType_Qzone) {//不是QQ空间
            messageObject.text=_shareContentView2.text;
        }
        UMShareImageObject*shObjc=[[UMShareImageObject alloc]init];
        [shObjc setShareImage:_dataImgAry[0]];
        messageObject.shareObject=shObjc;
    }
    
    // 调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        LOG(@"%@，\n\n%@",data,error)
        if (error) {
            switch (error.code) {
                case 2001:{ SHOWMSG(@"不支持的分享格式或软件版本过低!") }break;
                case 2005:{SHOWMSG(@"生成的分享内容为空,请稍后再试!")} break;
                case 2006:{SHOWMSG(@"未能完成操作!")} break;
                case 2008:{
                    if (platformType==1||platformType==2) { SHOWMSG(@"亲，你还没有安装微信哦!") }
                    if (platformType==4||platformType==5) { SHOWMSG(@"亲，你还没有安装QQ哦!") }
                } break;
                case 2009:{ SHOWMSG(@"分享已取消!") }break;
                case 2010:{ SHOWMSG(@"网络异常，请检查网络后重试") }break;
                case 2011:{ SHOWMSG(@"第三方错误") }break;
                default: { SHOWMSG(@"分享出错啦!") } break;
            }
        }else{
            SHOWMSG(@"分享成功!")
        }
    }];
}

#pragma mark === >>> 调用系统分享
//  调用系统更多分享方式
-(void)presentSystemShare{
    
    
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSForegroundColorAttributeName:[UIColor darkGrayColor]};
    
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:_dataImgAry applicationActivities:nil];
    
    //不显示系统默认的分享方式
    activity.excludedActivityTypes = @[UIActivityTypeAirDrop,
                                       UIActivityTypePostToFacebook,
                                       UIActivityTypePostToTwitter,
                                       UIActivityTypePostToWeibo,
                                       UIActivityTypeMessage,
                                       UIActivityTypeMail,
                                       UIActivityTypePrint,
                                       UIActivityTypeCopyToPasteboard,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo,
                                       UIActivityTypePostToTencentWeibo];
    
    UIActivityViewControllerCompletionWithItemsHandler shareBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
        // 导航栏标题
        UIBarButtonItem *item = [UIBarButtonItem appearance];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.001],
                                     NSForegroundColorAttributeName:[UIColor clearColor]};
        [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
        
        if (completed) {
            NSLog(@"\n\n\nactivityType: %@\n\nreturnedItems: %@\n\n",activityType,returnedItems);
        }else{
            NSLog(@"\n\n分享取消，取消或错误信息：\n%@\n\n",activityError);
        }
        
    };
    
    activity.completionWithItemsHandler = shareBlock;
    
    // 判断设备进行页面弹出
    if ([[UIDevice currentDevice].model isEqualToString:@"iPhone"]) {
        
        [self presentViewController:activity animated:YES completion:nil];
        
    } else if([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
        
        UIPopoverPresentationController *popover = activity.popoverPresentationController;
        if (popover) {
            popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
        }
        [self presentViewController:activity animated:YES completion:nil];
        
    } else {
        // 未知设备
    }
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}



@end








#pragma mark --> 分享按钮

@implementation ShareButtonGoods

-(instancetype)initWithImgName:(NSString*)ImgName titleName:(NSString*)titleName{
    self=[super init];
    if (self) {
        _imgView=[[UIImageView alloc]init];
        _imgView.image=[UIImage imageNamed:ImgName];
        _imgView.layer.masksToBounds=YES;
        _imgView.layer.cornerRadius = 18*HWB ;
        [self addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.offset(0);
            make.width.offset(36*HWB);
            make.height.offset(36*HWB);
            //            make.width.offset(50) ;
            //            make.height.offset(50) ;
        }];
        
        _titleLab=[[UILabel alloc]init];
        _titleLab.textAlignment=NSTextAlignmentCenter;
        _titleLab.text=titleName; _titleLab.font=[UIFont systemFontOfSize:13.0*HWB];
        _titleLab.textColor = Black102;
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.height.offset(15);
            make.bottom.offset(0);
            make.centerX.equalTo(self.mas_centerX);
        }];
    }return self;
}
@end




