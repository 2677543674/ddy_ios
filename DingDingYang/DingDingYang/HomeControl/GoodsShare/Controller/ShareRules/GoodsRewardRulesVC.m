//
//  GoodsRewardRulesVC.m
//  DingDingYang
//
//  Created by ddy on 2017/3/12.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "GoodsRewardRulesVC.h"

@interface GoodsRewardRulesVC ()<UIGestureRecognizerDelegate>
@property(nonatomic,strong) UIView * backView ;

@end

@implementation GoodsRewardRulesVC

//页面初始化方式
-(instancetype)init{
    self=[super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor] ;
    self.title = @"奖励计算规则" ;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSelf)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideself) name:HidePresentVC object:nil];
    
    [self backView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showViewAnima];
    });
}

-(UIView*)backView{
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = COLORWHITE ;
        _backView.center = self.view.center ;
        _backView.bounds = CGRectMake(0, 0, 0, 0);
        _backView.layer.masksToBounds = YES ;
        _backView.layer.cornerRadius = 8 ;
        _backView.alpha = 0 ;
        [self.view addSubview:_backView];
        
        UIImageView * topImgV =[[UIImageView alloc]init];
        topImgV.image = [UIImage imageNamed:@"share_jlgzjs"];
        [_backView addSubview:topImgV];
        [topImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.left.offset(0);
            make.right.offset(0); make.height.offset(60);
        }];

        UIButton * zzdlBtn = [UIButton titLe:@"朕知道了" bgColor:COLORWHITE titColorN:Black51 titColorH:Black51 font:18];
         ADDTARGETBUTTON(zzdlBtn, removeSelf)
        [_backView addSubview:zzdlBtn];
        [zzdlBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0); make.left.offset(0);
            make.right.offset(0); make.height.offset(40);
        }];

        
        UILabel * contentLab = [UILabel labText:@"此处展示高佣为卖家设置的佣金\n不同用户申请到的佣金不同\n最终以实际结算结果为准" color:Black102 font:16];
        contentLab.numberOfLines = 0 ;
        [UILabel changeLineSpaceForLabel:contentLab WithSpace:10];
        contentLab.textAlignment = NSTextAlignmentCenter ;
        [_backView addSubview:contentLab];
        [contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_backView.mas_centerX);
            make.centerY.equalTo(_backView.mas_centerY).offset(10);
        }];
    
        UIImageView * linesImg = [[UIImageView alloc]init];
        linesImg.backgroundColor = Black204 ;
        linesImg.frame = CGRectMake(0, 160, 260, 1);
        [_backView addSubview:linesImg];
    
    }
    return _backView ;
}

-(void)showViewAnima{
   _backView.bounds = CGRectMake(0, 0, 260, 200);
   [UIView animateWithDuration:0.1 animations:^{
       self.view.backgroundColor = ColorRGBA(0, 0, 0, 0.5) ;
       _backView.alpha = 1 ;
       
   } completion:^(BOOL finished) {
       
   }];
}



-(void)removeSelf{
     _backView.bounds = CGRectMake(0, 0, 0, 0);
    [UIView animateWithDuration:0.1 animations:^{
        self.view.backgroundColor = ColorRGBA(0, 0, 0, 0) ;
        _backView.alpha = 0 ;
       
    } completion:^(BOOL finished) {
         [self dismissViewControllerAnimated:NO completion:nil];
    }];
   
}

-(void)hideself{
    NSLog(@"收到通知隐藏界面");
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HidePresentVC object:nil];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_backView.frame, point)) {
        return NO;
    }
    return YES;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
