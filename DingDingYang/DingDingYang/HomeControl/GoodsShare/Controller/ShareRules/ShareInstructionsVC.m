//
//  ShareInstructionsVC.m
//  DingDingYang
//
//  Created by ddy on 2017/10/30.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ShareInstructionsVC.h"
#import "LevelView.h"
@interface ShareInstructionsVC ()

@property(nonatomic,strong) UIScrollView * bscrollView ;

@property(nonatomic,strong) NSArray * titleAry1 , * textAry1, * imgAry1 ;
@property(nonatomic,strong) LevelView * shareType1 ,  *shareType2 ;

@end

@implementation ShareInstructionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    
    _bscrollView = [[UIScrollView alloc]init];
    _bscrollView.backgroundColor = COLORGROUP;
    _bscrollView.frame=CGRectMake(0, 0, ScreenW, ScreenH-NAVCBAR_HEIGHT);
    [self.view addSubview:_bscrollView];
    
#pragma mark == >> 介绍友盟分享
    
    UILabel * lab1 = [UILabel labText:@"创建分享中直接分享 (单图)" color:COLORBLACK font:16*HWB];
    [_bscrollView addSubview:lab1];
    [lab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.offset(15);
    }];
    
    self.title = @"分享帮助" ;
    _titleAry1 = @[@"分享路径",@"QQ好友",@"QQ空间",@"微信好友",@"微信朋友圈",@"微博"];
    _textAry1 = @[@"分享文案",@"✔️",@"✔️",@"✔️",@"✔️",@"✔️"];
    _imgAry1 = @[@"分享图片",@"✖️",@"✔️",@"✔️",@"✔️",@"✔️"];
    
    for (NSInteger i = 0; i<_titleAry1.count ; i++ ) {
        
        _shareType1 = [[LevelView alloc]initWithFrame:CGRectMake(30, 50 + 43*i , ScreenW-60, 42)];
        [_bscrollView addSubview:_shareType1];
        _shareType1.titColor = Black51 ;
        _shareType1.oneStr = _titleAry1[i];
        _shareType1.twoStr = _textAry1[i];
        _shareType1.threeStr = _imgAry1[i];
        
        if (i==0) { _shareType1.bgColor = LOGOCOLOR;   _shareType1.titColor = COLORWHITE ; }
    }
    
    UILabel * tishiLab1 = [UILabel labText:@"注: 以上分享都是单张图片 (如果勾选多张,默认按顺序分享选中的第一张),\n文案已附带到粘贴板，分享时需手动粘贴哦" color:COLORBLACK font:11.5*HWB];
    tishiLab1.numberOfLines = 0 ;
    tishiLab1.textAlignment = NSTextAlignmentCenter ;
    tishiLab1.frame = CGRectMake(25*HWB, YBOTTOM(_shareType1)+10, ScreenW-50*HWB, 60) ;
    [_bscrollView addSubview:tishiLab1];
    
    
#pragma mark == >> 介绍更多分享
    UILabel * lab2 = [UILabel labText:@"使用更多分享 (多图)" color:COLORBLACK font:16*HWB];
    [_bscrollView addSubview:lab2];
    [lab2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(tishiLab1.mas_bottom).offset(15);
    }];
    
    _titleAry1 = @[@"分享路径",@"QQ好友",@"QQ空间",@"微信好友",@"微信朋友圈"];
    _textAry1 = @[@"分享文案",@"✖️",@"✔️",@"✖️",@"✔️"];
    _imgAry1 = @[@"分享图片",@"✔️",@"✔️",@"✔️",@"✔️"];
    
    for (NSInteger i = 0; i<_titleAry1.count ; i++ ) {
        
        _shareType2 = [[LevelView alloc]initWithFrame:CGRectMake(20*HWB, YBOTTOM(tishiLab1)+55 + 43*i , ScreenW-40*HWB, 42)];
        [_bscrollView addSubview:_shareType2];
        _shareType2.titColor = Black51 ;
        _shareType2.oneStr = _titleAry1[i];
        _shareType2.twoStr = _textAry1[i];
        _shareType2.threeStr = _imgAry1[i];
        
        if (i==0) { _shareType2.bgColor = LOGOCOLOR;   _shareType2.titColor = COLORWHITE ; }
    }
    
    UILabel * tishiLab2 = [UILabel labText:@"注: 更多分享可分享多图，文案已自动附带在粘贴板上，好友列表不能分享文案\n\n( 如果没有在更多中找到微信或QQ，\n需要手动添加分享选项哦 )\n\n使用更多分享时请保持第三方平台已在后台运行" color:COLORBLACK font:11.5*HWB];
    tishiLab2.numberOfLines = 0 ;
    tishiLab2.textAlignment = NSTextAlignmentCenter ;
    tishiLab2.frame = CGRectMake(25*HWB, YBOTTOM(_shareType2)+10, ScreenW-50*HWB, 120*HWB) ;
    [_bscrollView addSubview:tishiLab2];
    
    _bscrollView.contentSize = CGSizeMake(0,YBOTTOM(tishiLab2)+20) ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
