//
//  ShareTheTemplateVC.m
//  DingDingYang
//
//  Created by ddy on 2017/8/28.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "ShareTheTemplateVC.h"
#import "PopSearchView.h"

@interface ShareTheTemplateVC () <UITextViewDelegate>

@property (strong, nonatomic) UITextView *contentTextView;

@property (strong, nonatomic) UILabel *tipLabel, *introTitleLabel, *introLabel;

@property(nonatomic,strong) UIButton *previewBtn , *saveBtn , *recoverBtn;

@end

@implementation ShareTheTemplateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLORGROUP ;
    self.title = @"编辑模板" ;
    
    self.contentTextView = ({
        UITextView *textView = [[UITextView alloc]init];
        
        textView.layer.cornerRadius = 8 ;
        textView.layer.masksToBounds = YES;
        
        textView.delegate = self;
        textView.font = [UIFont systemFontOfSize:15];
        
        textView.backgroundColor = COLORWHITE;
        textView.text = @"{title}\n【在售价】{price}\n【券后价】{endPrice}\n{desc}\n------------------------\n复制这条信息,{tkl},\n打开【手机淘宝】即可查看" ;
        
        if (NSUDTakeData(ShareMuBan)!=nil) {
            textView.text = (NSString*)NSUDTakeData(ShareMuBan) ;
        }
        
        textView;
    });
    
    [self.view addSubview:self.contentTextView];
    
    [self.contentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(15);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(ScreenW/1.8);
    }];
    
    
    self.saveBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundImage:[UIImage imageNamed:@"fb_save_bg"] forState:UIControlStateNormal];
        
        [btn setTitle:@"保存" forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        
        [btn addTarget:self action:@selector(saveShareText) forControlEvents:UIControlEventTouchUpInside];
        
        btn;
    });
    
    [self.view addSubview:self.saveBtn];
    
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.equalTo(self.contentTextView.mas_bottom);
        make.width.height.mas_equalTo(58);
    }];
    
    self.recoverBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [btn setImage:[UIImage imageNamed:@"fb_recover"] forState:UIControlStateNormal];
        [btn setTitle:@"恢复系统模板" forState:UIControlStateNormal];
        [btn setTitleColor:Black153 forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [btn addTarget:self action:@selector(recoverText) forControlEvents:UIControlEventTouchUpInside];
        
        [btn sizeToFit];
        
        btn;
    });
    
    [self.view addSubview:self.recoverBtn];
    
    [self.recoverBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.equalTo(self.saveBtn.mas_bottom);
    }];
    
    self.previewBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"fb_preview"] forState:UIControlStateNormal];
        
        [btn setTitle:@"预览" forState:UIControlStateNormal];
        [btn setTitleColor:Black153 forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [btn addTarget:self action:@selector(previewShareText) forControlEvents:UIControlEventTouchUpInside];
        
        [btn sizeToFit];
        
        btn;
    });
    
    [self.view addSubview:self.previewBtn];
    
    [self.previewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-68);
        make.top.equalTo(self.saveBtn.mas_bottom);
    }];
    
    self.tipLabel = ({
        UILabel *aLabel = [[UILabel alloc] init];
        aLabel.text = @"请不要修改\"{}\"之间的文字";
        
        aLabel.font = [UIFont systemFontOfSize:14];
        aLabel.textColor = Black153;
        
        aLabel;
    });
    
    [self.view addSubview:self.tipLabel];
    
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.previewBtn.mas_bottom).offset(20);
    }];
    
    self.introTitleLabel = ({
        UILabel *aLabel = [[UILabel alloc] init];
        aLabel.text = @"说明";
        
        aLabel.font = [UIFont systemFontOfSize:16];
        aLabel.textColor = Black102;
        
        aLabel;
    });
    
    [self.view addSubview:self.introTitleLabel];
    
    [self.introTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.tipLabel.mas_bottom).offset(20);
    }];
    
    self.introLabel = ({
        UILabel *aLabel = [[UILabel alloc] init];
        aLabel.text = @"正在加载。。。";
        
        aLabel.font = [UIFont systemFontOfSize:16];
        aLabel.textColor = Black102;
        aLabel.numberOfLines = 0;
        
        aLabel;
    });
    
    [self.view addSubview:self.introLabel];
    
    [self.introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.introTitleLabel.mas_bottom).offset(10);
    }];
    
    
    [NetRequest requestTokenURL:url_shareTxt_stIns parameter:nil success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            
            NSArray * ary = NULLARY(nwdic[@"list"]) ;
            NSMutableArray * mtary = [[NSMutableArray alloc]initWithArray:ary] ;
            [mtary removeObjectAtIndex:0];
            
            NSString * str = [mtary componentsJoinedByString:@"\n"];
            self.introLabel.text = str ;
            
            //设置带属性的字符串
            NSMutableAttributedString * attributeStr = [[NSMutableAttributedString alloc]initWithString:self.introLabel.text];
            
            NSArray * rangeValueAry = [self rangeOfSubString:@"必填" inString:str];
            for (NSInteger i=0 ; i<rangeValueAry.count ; i++) {
                NSValue * valueRange = (NSValue*)rangeValueAry[i];
                NSRange rangeBT = [valueRange rangeValue];
                [attributeStr addAttribute:NSForegroundColorAttributeName value:LOGOCOLOR range:rangeBT];
            }
            
            //行间距
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:5];
            [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
            //字间距
            [attributeStr addAttribute:NSKernAttributeName value:@(1) range:NSMakeRange(0, [str length])];
            self.introLabel.attributedText = attributeStr ;

        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
    
}


#pragma mark === >>> 预览效果
-(void)previewShareText{
    ResignFirstResponder
    NSString * txt = [self.contentTextView.text stringByReplacingOccurrencesOfString:@"\n" withString:@"##"];
    
    MBShow(@"正在加载...")
    [NetRequest requestTokenURL:url_save_shareTemp parameter:@{@"content":txt,@"ifValid":@"0"} success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            [NetRequest requestTokenURL:url_preview_shareTemp parameter:nil success:^(NSDictionary *nwdic) {
                MBHideHUD
                if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                    [UIAlertController showIn:self title:@"效果预览\n" content: REPLACENULL(nwdic[@"content"]) ctAlignment:NSTextAlignmentLeft btnAry:@[@"关闭预览",@"保存模板"] indexAction:^(NSInteger indexTag) {
                        if (indexTag==1) {
                            [self saveShareText];
                        }
                    }];
                }else{
                    SHOWMSG(nwdic[@"result"])
                }
            } failure:^(NSString *failure) {
                SHOWMSG(failure)
            }];
        }else{
            MBHideHUD
           SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

#pragma mark === >>> 保存模板
-(void)saveShareText{
    ResignFirstResponder
    
    NSString * txt = [self.contentTextView.text stringByReplacingOccurrencesOfString:@"\n" withString:@"##"];
    [NetRequest requestTokenURL:url_save_shareTemp parameter:@{@"content":txt , @"ifValid":@"1"} success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            MBSuccess(@"保存成功!")
            NSUDSaveData(self.contentTextView.text, ShareMuBan)
            self.changeText(1) ;
            PopTo(YES);
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

- (void)recoverText{
    ResignFirstResponder
    
//    self.contentTextView.text = @"{title}\n【在售价】{price}\n【券后价】{endPrice}\n{desc}\n------------------------\n复制这条信息,{tkl},\n打开【手机淘宝】即可查看";

    MBShow(@"正在恢复默认...");
    [NetRequest requestTokenURL:@"getTemplateTxt.api" parameter:nil success:^(NSDictionary *nwdic) {
        MBHideHUD
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            self.contentTextView.text = [nwdic[@"data"] stringByReplacingOccurrencesOfString:@"##" withString:@"\n"];

            SHOWMSG(@"恢复成功，请点击保存按钮");
        }else{
            SHOWMSG(nwdic[@"result"])
        }
        NSLog(@"%@",nwdic);
    } failure:^(NSString *failure) {
        MBHideHUD
        SHOWMSG(failure)
    }];
}

//返回一个 range 组 处理带属性的字符串
- (NSArray*)rangeOfSubString:(NSString*)subStr inString:(NSString*)string {
    NSMutableArray *rangeArray = [NSMutableArray array];
    NSString*string1 = [string stringByAppendingString:subStr];
    NSString *temp;
    for(int i =0; i < string.length; i ++) {
        
        temp = [string1 substringWithRange:NSMakeRange(i, subStr.length)];
            
        if ([temp isEqualToString:subStr]) {
            NSRange range = {i,subStr.length};
            [rangeArray addObject: [NSValue valueWithRange:range]];
       }
    }
    return rangeArray;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
