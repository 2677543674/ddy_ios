//
//  SelectPosterVC.m
//  DingDingYang
//
//  Created by ddy on 2018/6/7.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "SelectPosterVC.h"
#import "DCCycleScrollView.h"
@interface SelectPosterVC ()
@property(nonatomic,strong)NSArray* titleArr;
@property(nonatomic,strong)UIScrollView* scrollerView;
@property(nonatomic,strong)DCCycleScrollView* leftView;
@property(nonatomic,strong)DCCycleScrollView* rightView;
@property(nonatomic,strong)UIButton* leftBtn;
@property(nonatomic,strong)UIButton* rightBtn;
@property(nonatomic,assign)NSInteger contentFlag;//美文
@property(nonatomic,assign)NSInteger selectFlag;//美图1
@property(nonatomic,assign)NSInteger selectFlag2;//美图2

@end

@implementation SelectPosterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"选择美报";
    self.view.backgroundColor=COLORGROUP;
    _titleArr=@[@"创建分享",@"圈子广告"];
    _contentFlag=1;
    _selectFlag=1;
    _selectFlag2=1;
    [NetRequest requestTokenURL:url_shareTxt_temple parameter:nil success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            if (REPLACENULL(nwdic[@"sel"]).length!=0) {
                _contentFlag=[REPLACENULL(nwdic[@"sel"]) integerValue];
            }
            if (REPLACENULL(nwdic[@"selPic"]).length!=0) {
                _selectFlag=[REPLACENULL(nwdic[@"selPic"]) integerValue];
            }
            if (REPLACENULL(nwdic[@"selPic2"]).length!=0) {
                _selectFlag2=[REPLACENULL(nwdic[@"selPic2"]) integerValue];
            }
            [self creatTopView];
            [self scrollerView];
            [self createCycleView];
            [self creatBtn];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

-(void)creatTopView{
    for (int i=0; i<_titleArr.count; i++) {
        UIButton* btn=[UIButton new];
        btn.backgroundColor=[UIColor whiteColor];
        [btn setTitle:_titleArr[i] forState:(UIControlStateNormal)];
        [btn setTitleColor:Black102 forState:UIControlStateNormal];
        [btn setTitleColor:RGBA(214, 20, 71, 1) forState:UIControlStateSelected];
        btn.titleLabel.font=[UIFont systemFontOfSize:14*HWB];
        btn.tag=10+i;
        [btn addTarget:self action:@selector(clickTopBtn:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(i*ScreenW/_titleArr.count);
            make.height.offset(38);
            make.width.offset(ScreenW/_titleArr.count);
        }];
        if (i==0) {
            btn.selected=YES;
            UIView* line=[UIView new];
            line.backgroundColor=Black204;
            [self.view addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.view.mas_centerX);
                make.height.offset(22);
                make.top.offset(8);
                make.width.offset(1);
            }];
        }
    }
}

-(void)clickTopBtn:(UIButton*)btn{
    for (int i=0; i<_titleArr.count; i++) {
        UIButton* button=[self.view viewWithTag:i+10];
        button.selected=NO;
    }
    btn.selected=YES;
    [UIView animateWithDuration:0.35 animations:^{
        [_scrollerView setContentOffset:CGPointMake((btn.tag-10)*ScreenW, 0)];
    }];
}

-(UIScrollView *)scrollerView{
    if (!_scrollerView) {
        _scrollerView=[UIScrollView new];
        _scrollerView.tag=999;
        _scrollerView.backgroundColor=COLORGROUP;
        _scrollerView.contentSize=CGSizeMake(_titleArr.count*ScreenW, 0);
        _scrollerView.showsHorizontalScrollIndicator=NO;
        _scrollerView.bounces=NO;
        _scrollerView.pagingEnabled=YES;
        [self.view addSubview:_scrollerView];
        [_scrollerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(38);
            make.left.bottom.offset(0);
            make.width.offset(ScreenW);
        }];
        _scrollerView.scrollEnabled=NO;
    }
    return _scrollerView;
}



-(void)createCycleView{
    NSArray *imageArr = @[@"poster1",@"poster2",@"poster3",@"poster4",@"poster5"];
    _leftView=[DCCycleScrollView cycleScrollViewWithFrame:CGRectZero shouldInfiniteLoop:YES imageGroups:imageArr];
    _leftView.selectFlag=_selectFlag;
    _leftView.tag=100;
    _leftView.isZoom = YES;
    _leftView.itemSpace = 0;
    _leftView.autoScroll = NO;
    _leftView.itemWidth = 225*HWB;
    _leftView.backgroundColor=COLORGROUP;
    [_scrollerView addSubview:_leftView];
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(20*HWB);
        make.left.offset(0);
        make.height.offset(_leftView.itemWidth*1.58);
        make.width.offset(ScreenW);
    }];
    
    UIButton* leftScrollBtn=[UIButton new];
    leftScrollBtn.tag=101;
    ADDTARGETBUTTON(leftScrollBtn, clickScroll:);
    [leftScrollBtn setImage:[UIImage imageNamed:@"left"] forState:(UIControlStateNormal)];
    [leftScrollBtn setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5]];
    leftScrollBtn.layer.cornerRadius=18*HWB;
    leftScrollBtn.clipsToBounds=YES;
    [_leftView addSubview:leftScrollBtn];
    [leftScrollBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);
        make.centerY.equalTo(_leftView.mas_centerY);
        make.height.width.offset(36*HWB);
    }];
    
    UIButton* rightScrollBtn=[UIButton new];
    rightScrollBtn.tag=102;
    ADDTARGETBUTTON(rightScrollBtn, clickScroll:);
    [rightScrollBtn setImage:[UIImage imageNamed:@"right"] forState:(UIControlStateNormal)];
    [rightScrollBtn setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5]];
    rightScrollBtn.layer.cornerRadius=18*HWB;
    rightScrollBtn.clipsToBounds=YES;
    [_leftView addSubview:rightScrollBtn];
    [rightScrollBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-20);
        make.centerY.equalTo(_leftView.mas_centerY);
        make.height.width.offset(36*HWB);
    }];
    
    _rightView=[DCCycleScrollView cycleScrollViewWithFrame:CGRectZero shouldInfiniteLoop:YES imageGroups:imageArr];
    _rightView.selectFlag=_selectFlag2;
    _rightView.tag=200;
    _rightView.isZoom = YES;
    _rightView.itemSpace = 0;
    _rightView.autoScroll = NO;
    _rightView.itemWidth = FloatKeepInt(225*HWB);
    _rightView.backgroundColor=COLORGROUP;
    [_scrollerView addSubview:_rightView];
    [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(20*HWB);
        make.left.offset(ScreenW);
        make.height.offset(_leftView.itemWidth*1.58);
        make.width.offset(ScreenW);
    }];
    
    UIButton* leftScrollBtn2=[UIButton new];
    leftScrollBtn2.tag=103;
    ADDTARGETBUTTON(leftScrollBtn2, clickScroll:);
    [leftScrollBtn2 setImage:[UIImage imageNamed:@"left"] forState:(UIControlStateNormal)];
    [leftScrollBtn2 setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5]];
    leftScrollBtn2.layer.cornerRadius=18*HWB;
    leftScrollBtn2.clipsToBounds=YES;
    [_rightView addSubview:leftScrollBtn2];
    [leftScrollBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(20);
        make.centerY.equalTo(_rightView.mas_centerY);
        make.height.width.offset(36*HWB);
    }];
    
    UIButton* rightScrollBtn2=[UIButton new];
    rightScrollBtn2.tag=104;
    ADDTARGETBUTTON(rightScrollBtn2, clickScroll:);
    [rightScrollBtn2 setImage:[UIImage imageNamed:@"right"] forState:(UIControlStateNormal)];
    [rightScrollBtn2 setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5]];
    rightScrollBtn2.layer.cornerRadius=18*HWB;
    rightScrollBtn2.clipsToBounds=YES;
    [_rightView addSubview:rightScrollBtn2];
    [rightScrollBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-20);
        make.centerY.equalTo(_rightView.mas_centerY);
        make.height.width.offset(36*HWB);
    }];
}

-(void)clickScroll:(UIButton* )btn{
    if (btn.tag==101) {
        NSInteger index = (_leftView.collectionView.contentOffset.x + (_leftView.itemWidth + _leftView.itemSpace) * 0.5) / (_leftView.itemSpace + _leftView.itemWidth);
        [_leftView scrollToIndex:MAX(0,index)-1];
        NSLog(@"%ld",(index-1)%5+1);
        if (_selectFlag==(index-1)%5+1) {
            _leftBtn.selected=YES;
        }else{
            _leftBtn.selected=NO;
        }

    }else if (btn.tag==102){
        NSInteger index = (_leftView.collectionView.contentOffset.x + (_leftView.itemWidth + _leftView.itemSpace) * 0.5) / (_leftView.itemSpace + _leftView.itemWidth);
        [_leftView scrollToIndex:MAX(0,index)+1];
        NSLog(@"%ld",(index+1)%5+1);
        if (_selectFlag==(index+1)%5+1) {
            _leftBtn.selected=YES;
        }else{
            _leftBtn.selected=NO;
        }
    }else if (btn.tag==103){
        NSInteger index = (_rightView.collectionView.contentOffset.x + (_rightView.itemWidth + _rightView.itemSpace) * 0.5) / (_rightView.itemSpace + _rightView.itemWidth);
        [_rightView scrollToIndex:MAX(0,index)-1];
        if (_selectFlag2==(index-1)%5+1) {
            _rightBtn.selected=YES;
        }else{
            _rightBtn.selected=NO;
        }
    }else{
        NSInteger index = (_rightView.collectionView.contentOffset.x + (_rightView.itemWidth + _rightView.itemSpace) * 0.5) / (_rightView.itemSpace + _rightView.itemWidth);
        [_rightView scrollToIndex:MAX(0,index)+1];
        if (_selectFlag2==(index+1)%5+1) {
            _rightBtn.selected=YES;
        }else{
            _rightBtn.selected=NO;
        }
    }
}

-(void)creatBtn{
    _leftBtn=[UIButton new];
    _leftBtn.tag=300;
    [_leftBtn setImage:[UIImage imageNamed:@"poster_select"] forState:(UIControlStateSelected)];
    [_leftBtn setImage:[UIImage imageNamed:@"poster_normal"] forState:(UIControlStateNormal)];
    if (_selectFlag==1) {
        _leftBtn.selected=YES;
    }
    ADDTARGETBUTTON(_leftBtn, clickBtn:);
    [_scrollerView addSubview:_leftBtn];
    [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-40*HWB);
        make.centerX.equalTo([_scrollerView viewWithTag:100].mas_centerX);
        make.height.width.offset(34*HWB);
    }];

    _rightBtn=[UIButton new];
    _rightBtn.tag=400;
    [_rightBtn setImage:[UIImage imageNamed:@"poster_select"] forState:(UIControlStateSelected)];
    [_rightBtn setImage:[UIImage imageNamed:@"poster_normal"] forState:(UIControlStateNormal)];
    if (_selectFlag2==1) {
        _rightBtn.selected=YES;
    }
    ADDTARGETBUTTON(_rightBtn, clickBtn:);
    [_scrollerView addSubview:_rightBtn];
    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-40*HWB);
        make.centerX.equalTo([_scrollerView viewWithTag:200].mas_centerX);
        make.height.width.offset(34*HWB);
    }];
}

-(void)clickBtn:(UIButton*)btn{
    btn.selected=!btn.selected;
    if (btn.tag==300) {
        NSInteger index = (_leftView.collectionView.contentOffset.x + (_leftView.itemWidth + _leftView.itemSpace) * 0.5) / (_leftView.itemSpace + _leftView.itemWidth);
        NSLog(@"%ld",index%5+1);
        NSMutableDictionary* dic=[NSMutableDictionary dictionary];
        dic[@"p"]=[NSString stringWithFormat:@"%ld",index%5+1];
        dic[@"p2"]=[NSString stringWithFormat:@"%ld",_selectFlag2];
        dic[@"t"]=[NSString stringWithFormat:@"%ld",_contentFlag];
        [NetRequest requestTokenURL:@"saveNewShareTemp.api" parameter:dic success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                SHOWMSG(@"保存成功")
                _selectFlag=index%5+1;
                _leftView.selectFlag=index%5+1;
            }else{
                SHOWMSG(nwdic[@"result"])
            }
        } failure:^(NSString *failure) {
            SHOWMSG(failure)
        }];
 
    }else{
        NSInteger index = (_rightView.collectionView.contentOffset.x + (_rightView.itemWidth + _rightView.itemSpace) * 0.5) / (_rightView.itemSpace + _rightView.itemWidth);
        NSLog(@"%ld",index%5);
        NSMutableDictionary* dic=[NSMutableDictionary dictionary];
        dic[@"p"]=[NSString stringWithFormat:@"%ld",_selectFlag];
        dic[@"p2"]=[NSString stringWithFormat:@"%ld",index%5+1];
        dic[@"t"]=[NSString stringWithFormat:@"%ld",_contentFlag];
        [NetRequest requestTokenURL:@"saveNewShareTemp.api" parameter:dic success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                SHOWMSG(@"保存成功")
                _selectFlag2=index%5+1;
                _rightView.selectFlag=index%5+1;
            }else{
                SHOWMSG(nwdic[@"result"])
            }
        } failure:^(NSString *failure) {
            SHOWMSG(failure)
        }];
    }
}



//点击图片的代理
//-(void)cycleScrollView:(DCCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
//{
//    NSLog(@"index = %ld",index);
//}




@end
