//
//  ShareTheTemplateVC.h
//  DingDingYang
//
//  Created by ddy on 2017/8/28.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "DDYViewController.h"

@interface ShareTheTemplateVC : DDYViewController

typedef void (^ChangeShareText) (NSInteger result);

@property (copy, nonatomic) ChangeShareText changeText;

@end
