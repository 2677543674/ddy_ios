//
//  SelectContentVC.m
//  DingDingYang
//
//  Created by ddy on 2018/6/13.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "SelectContentVC.h"
#import "ContentCell.h"
#import "ContentModel.h"

@interface SelectContentVC ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong)NSMutableArray* dataArr;
@property(nonatomic,assign)NSInteger sel;//美文
@property(nonatomic,assign)NSInteger selectFlag;//美图1
@property(nonatomic,assign)NSInteger selectFlag2;//美图2
@end


@implementation SelectContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArr=[NSMutableArray array];
    self.view.backgroundColor=COLORGROUP;
    self.title=@"选择美文";
    [self createBottomBtn];
    [self getData];
}

-(void)getData{
    [NetRequest requestTokenURL:url_shareTxt_temple parameter:nil success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            NSArray* list=nwdic[@"list"];
            [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ContentModel* model=[[ContentModel alloc]initWithModel:obj];
                [_dataArr addObject:model];
            }];
            _sel=[nwdic[@"sel"] integerValue];
            _selectFlag=[nwdic[@"selPic"] integerValue];
            _selectFlag2=[nwdic[@"selPic2"] integerValue];
            [self tableView];
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

-(void)createBottomBtn{
    UIButton* bottomBtn=[UIButton buttonWithTitle:@"确定" font:12*HWB backgroundColor:RGBA(217, 20, 71, 1) cornerRadius:17*HWB];
    ADDTARGETBUTTON(bottomBtn, clickBtn);
    [self.view addSubview:bottomBtn];
    [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-40*HWB);
        make.centerX.equalTo(self.view.mas_centerX);
        make.height.offset(34*HWB);
        make.width.offset(ScreenW*0.5);
    }];
}

-(void)clickBtn{
    NSMutableDictionary* dic=[NSMutableDictionary dictionary];
    dic[@"p"]=[NSString stringWithFormat:@"%ld",_selectFlag];
    dic[@"p2"]=[NSString stringWithFormat:@"%ld",_selectFlag2];
    dic[@"t"]=[NSString stringWithFormat:@"%ld",_sel];
    [NetRequest requestTokenURL:@"saveNewShareTemp.api" parameter:dic success:^(NSDictionary *nwdic) {
        if ([nwdic[@"result"] isEqualToString:@"OK"]) {
            SHOWMSG(@"保存成功")
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.block();
                PopTo(YES)
            });
        }else{
            SHOWMSG(nwdic[@"result"])
        }
    } failure:^(NSString *failure) {
        SHOWMSG(failure)
    }];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.separatorStyle=0;
        _tableView.estimatedRowHeight=100*HWB;
        _tableView.backgroundColor=[UIColor clearColor];
        [_tableView registerClass:[ContentCell class] forCellReuseIdentifier:@"ContentCell"];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.height.offset(ScreenH-150*HWB);
            
        }];
    }
    return _tableView;
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContentCell* cell=[tableView dequeueReusableCellWithIdentifier:@"ContentCell"];
    ContentModel* model=_dataArr[indexPath.row];
    cell.selectionStyle=0;
    cell.model=_dataArr[indexPath.row];
    cell.checkBtn.tag=100+[model.sorts integerValue];
    if (_sel==[model.sorts integerValue]) {
        cell.checkBtn.selected=YES;
    }else{
        cell.checkBtn.selected=NO;
    }
    ADDTARGETBUTTON(cell.checkBtn, clickBtn:);
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ContentModel* model=_dataArr[indexPath.row];
    [self clickBtn:[self.view viewWithTag:100+[model.sorts integerValue]]];
}

-(void)clickBtn:(UIButton*)btn{
    for (int i=0; i<_dataArr.count; i++) {
        ContentModel* model=_dataArr[i];
        UIButton* b=[self.view viewWithTag:100+[model.sorts integerValue]];
        b.selected=NO;
    }
    _sel=btn.tag-100;
    btn.selected=YES;
}








@end
