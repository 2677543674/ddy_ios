//
//  EditorSelectImgCell.m
//  DingDingYang
//
//  Created by ddy on 2017/6/17.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "EditorSelectImgCell.h"
#import "SGSImgModel.h"
@implementation EditorSelectImgCell

-(void)setModel:(SGSImgModel *)model{
    _model = model ;
    if ([model.urlImg isKindOfClass:[UIImage class]]) {
        _goodsImgV.image = _model.urlImg;
    }else{
        [_goodsImgV sd_setImageWithURL:[NSURL URLWithString:_model.urlImg]];
    }
    
    
    if ([_model.isSelect integerValue]==1) {
        [_isSelectbtn setImage:[UIImage imageNamed:@"share_img_sh"] forState:(UIControlStateNormal)];
    }else{
        [_isSelectbtn setImage:[UIImage imageNamed:@"share_img_sn"] forState:(UIControlStateNormal)];
    }
        
    
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self goodsImgV];
        [self isSelectbtn];
    }
    return self;
}
//商品图片
-(UIImageView*)goodsImgV{
    if (!_goodsImgV) {
        _goodsImgV = [[UIImageView alloc]init];
        _goodsImgV.contentMode=UIViewContentModeScaleAspectFill;
        _goodsImgV.clipsToBounds=YES;
        _goodsImgV.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_goodsImgV];
        [_goodsImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.left.offset(0);
            make.bottom.offset(0); make.right.offset(0);
        }];
    }
    return _goodsImgV;
}

//-(UIImageView*)isSelectImgV{
//    if (!_isSelectImgV) {
//        _isSelectImgV = [[UIImageView alloc]init];
//        _isSelectImgV.image = [UIImage imageNamed:@"share_img_sn"];
//        [self.contentView addSubview:_isSelectImgV] ;
//        [_isSelectImgV mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.offset(5) ; make.right.offset(-5) ;
//            make.height.offset(20); make.width.offset(20);
//        }];
//    }
//    return _isSelectImgV ;
//}

-(UIButton*)isSelectbtn{
    if (!_isSelectbtn) {
        _isSelectbtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_isSelectbtn setImage:[UIImage imageNamed:@"share_img_sn"] forState:UIControlStateNormal];
        [_isSelectbtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 5, 0)];
        [self.contentView addSubview:_isSelectbtn];
        [_isSelectbtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0) ; make.right.offset(0) ;
            make.height.offset(25*HWB); make.width.offset(25*HWB);
        }];
        
    }
    
    return _isSelectbtn ;
}



@end








