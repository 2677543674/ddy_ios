//
//  EditorSelectImgCell.h
//  DingDingYang
//
//  Created by ddy on 2017/6/17.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class SGSImgModel ;
@interface EditorSelectImgCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView * goodsImgV ;
//@property(nonatomic,strong) UIImageView * isSelectImgV ;
@property(nonatomic,strong) UIButton * isSelectbtn ;

@property(nonatomic,strong) SGSImgModel * model ;
//@property(nonatomic,copy) NSString * isselect ;
@end
