//
//  ContentCell.h
//  DingDingYang
//
//  Created by ddy on 2018/6/13.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentModel.h"

@interface ContentCell : UITableViewCell
@property(nonatomic,strong)UIView* backView;
@property(nonatomic,strong)UILabel* contentLabel;
@property(nonatomic,strong)UIButton* checkBtn;
@property(nonatomic,strong)ContentModel* model;
@end
