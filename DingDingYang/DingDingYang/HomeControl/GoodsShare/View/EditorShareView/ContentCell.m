//
//  ContentCell.m
//  DingDingYang
//
//  Created by ddy on 2018/6/13.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "ContentCell.h"

@implementation ContentCell

-(void)setModel:(ContentModel *)model{
    _model=model;
    _contentLabel.text=model.value;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor=COLORGROUP;
        [self contentLabel];
        [self backView];
        [self checkBtn];
        
    }
    return self;
}

-(UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel=[UILabel new];
        _contentLabel.backgroundColor=[UIColor whiteColor];
        _contentLabel.numberOfLines=0;
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"我"]];
        NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle  setLineSpacing:5];
        [str addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
        _contentLabel.attributedText = str;

        _contentLabel.font=[UIFont systemFontOfSize:10*HWB];
        _contentLabel.textColor=Black102;
        [self.contentView addSubview:_contentLabel];
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(15*HWB);
            make.left.offset(30*HWB);
            make.right.offset(-40*HWB);
            make.bottom.offset(-15*HWB);
        }];
    }
    return _contentLabel;
}



-(UIView *)backView{
    if (!_backView) {
        _backView=[UIView new];
        _backView.backgroundColor=[UIColor whiteColor];
        _backView.layer.cornerRadius=5;
        _backView.layer.shadowColor = [UIColor blackColor].CGColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 0);
        _backView.layer.shadowOpacity = 0.1;
        _backView.layer.shadowRadius = 4;
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5*HWB);
            make.left.offset(20*HWB);
            make.right.offset(-20*HWB);
            make.bottom.equalTo(_contentLabel.mas_bottom).offset(10*HWB);
        }];
        [self.contentView sendSubviewToBack:_backView];
    }
    return _backView;
}

-(UIButton *)checkBtn{
    if (!_checkBtn) {
        _checkBtn=[UIButton new];
        [_checkBtn setImage:[UIImage imageNamed:@"share_img_sn"] forState:UIControlStateNormal];
        [_checkBtn setImage:[UIImage imageNamed:@"share_img_sh"] forState:UIControlStateSelected];
        [self.contentView addSubview:_checkBtn];
        [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView.mas_top).offset(8);
            make.right.equalTo(_backView.mas_right).offset(-8);
            make.height.offset(22*HWB);
            make.width.offset(22*HWB);
        }];
//        [self.contentView bringSubviewToFront:_checkBtn];
    }
    return _checkBtn;
}


@end
