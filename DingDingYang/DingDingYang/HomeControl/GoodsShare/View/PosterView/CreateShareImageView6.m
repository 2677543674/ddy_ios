//
//  CreateShareImageView6.m
//  DingDingYang
//
//  Created by ddy on 2018/7/24.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "CreateShareImageView6.h"
#import "CompositePosterImg.h"

@implementation CreateShareImageView6

-(void)setModel:(GoodsModel *)model{
    _model=model;
    _goodsImageView.image=_goodsImage;
    _goodsTitle.text=[NSString stringWithFormat:@"      %@",model.goodsName];
    _endPrice.text=[NSString stringWithFormat:@"   唯品会￥%@    ",model.endPrice];
    _price.text=[NSString stringWithFormat:@"  市场价￥%@  ",model.price];
    _ewm.image=EWMImageWithString(model.cpsUrl);
}

-(void)setDmModel:(GoodsModel *)dmModel{
    _dmModel = dmModel;
    _goodsImageView.image = _goodsImage;
    [_fromIma mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.offset(40*HWB);
    }];
    [_fromIma sd_setImageWithURL:[NSURL URLWithString:_dmModel.gfromUrl] placeholderImage:[UIImage imageNamed:@"logo"]];
    _goodsTitle.text=[NSString stringWithFormat:@"             %@",_dmModel.goodsName];
    _endPrice.text=[NSString stringWithFormat:@"   平台价￥%@    ",_dmModel.endPrice];
    _price.text=[NSString stringWithFormat:@"  市场价￥%@  ",_dmModel.price];

    _ewm.image=EWMImageWithString(_dmModel.trackUrl);
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self backImageView];
        [self goodsImageView];
        [self goodsTitle];
        [self fromIma];
        [self price];
        [self endPrice];
        [self ewm];
    }
    return self;
}

-(UIImageView *)backImageView{
    if (!_backImageView) {
        _backImageView=[[UIImageView alloc]init];
        _backImageView.backgroundColor=[UIColor whiteColor];
        [self addSubview:_backImageView];
        [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.height.offset(ScreenW*1.42);
        }];
    }
    return _backImageView;
}

-(UIImageView *)goodsImageView{
    if (!_goodsImageView) {
        _goodsImageView=[UIImageView new];
        [_backImageView addSubview:_goodsImageView];
        [_goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.height.equalTo(_goodsImageView.mas_width);
        }];
    }
    return _goodsImageView;
}


-(UILabel *)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle=[UILabel new];
        _goodsTitle.textColor=Black51;
        _goodsTitle.font=[UIFont systemFontOfSize:14*HWB];
        _goodsTitle.numberOfLines=2;
        
        [_backImageView addSubview:_goodsTitle];
        [_goodsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsImageView.mas_bottom).offset(12*HWB);
            make.left.offset(12*HWB);
            make.width.offset(0.6*ScreenW);
//            make.height.offset(34*HWB);
        }];
    }
    return _goodsTitle;
}

-(UIImageView *)fromIma{
    if (!_fromIma) {
        _fromIma=[UIImageView new];
        _fromIma.image=[UIImage imageNamed:@"wphIcon"];
        _fromIma.contentMode = UIViewContentModeScaleAspectFit;
        [_backImageView addSubview:_fromIma];
        [_fromIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsImageView.mas_bottom).offset(12*HWB);
            make.left.offset(12*HWB);
            make.height.width.offset(16*HWB);
        }];
    }
    return _fromIma;
}

-(UILabel *)price{
    if (!_price) {
        _price=[UILabel new];
        _price.backgroundColor=RGBA(250, 229, 228, 1);
        _price.textColor=RGBA(248, 72, 72, 1);
        _price.font=[UIFont systemFontOfSize:14*HWB];
        [_backImageView addSubview:_price];
        [_price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsTitle.mas_bottom).offset(8*HWB);
            make.left.equalTo(_goodsTitle.mas_left);
            make.height.offset(22*HWB);
        }];
    }
    return _price;
}


-(UILabel *)endPrice{
    if (!_endPrice) {
        _endPrice=[UILabel new];
        _endPrice.font=[UIFont systemFontOfSize:15*HWB];
        _endPrice.textColor=COLORWHITE;
        _endPrice.backgroundColor=RGBA(248, 72, 72, 1);
        [_backImageView addSubview:_endPrice];
        [_endPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_price.mas_bottom).offset(12*HWB);
            make.left.equalTo(_goodsTitle.mas_left);
            make.height.offset(26*HWB);
        }];
    }
    return _endPrice;
}


-(UIImageView *)ewm{
    if (!_ewm) {
        UIImageView* kuang=[[UIImageView alloc]initWithImage:[EwmKuangImg ewmKuangImg]];
        [_backImageView addSubview:kuang];
        [kuang mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsImageView.mas_bottom).offset(10*HWB);
            make.right.offset(-10*HWB);
            make.height.width.offset(0.315*ScreenW);
        }];
        
        _ewm=[UIImageView new];
        [_backImageView addSubview:_ewm];
        [_ewm mas_makeConstraints:^(MASConstraintMaker *make) {

            make.height.width.offset(0.3*ScreenW);
            make.centerX.equalTo(kuang.mas_centerX);
            make.centerY.equalTo(kuang.mas_centerY);
        }];
        
        UILabel* label=[UILabel new];
        label.text=@"长按识别二维码";
        label.textColor=LOGOCOLOR;
        label.font=[UIFont systemFontOfSize:10*HWB];
        [_backImageView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(kuang.mas_bottom).offset(2*HWB);
            make.centerX.equalTo(kuang.mas_centerX);
        }];
    }
    return _ewm;
}






@end
