//
//  CreateShareImageView5.m
//  DingDingYang
//
//  Created by ddy on 2018/6/12.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "CreateShareImageView5.h"

@implementation CreateShareImageView5

-(void)setModel:(GoodsModel *)model{
    _model=model;
    ShopModel* shopModel=model.shopModel;
    
    _goodsImageView.image=_goodsImage;
    _shopHeader.image=_shopHeaderImage;
    _shopName.text=shopModel.shopTitle;
    _wuliu.text=[NSString stringWithFormat:@"物流服务 %@",shopModel.deliveryScore];
    _shangpin.text=[NSString stringWithFormat:@"商品描述 %@",shopModel.itemScore];
    _maijia.text=[NSString stringWithFormat:@"卖家服务 %@",shopModel.serviceScore];
    _goodsTitle.text=model.goodsName;
    if ([model.platform isEqualToString:@"天猫"]) {
        _fromIma.image=[UIImage imageNamed:@"tmallicon"];
    }else if([model.platform isEqualToString:@"2"]){
        _fromIma.image=[UIImage imageNamed:@"pddicon"];
    }else{
        _fromIma.image=[UIImage imageNamed:@"tbicon"];
    }
    [_quan setTitle:[NSString stringWithFormat:@"券￥%@",model.couponMoney] forState:(UIControlStateNormal)];
    _sale.text=[NSString stringWithFormat:@"销量 %@",model.sales];
    if (_ewmImage!=nil) {
        //小程序码
        _ewm.image = _ewmImage ;
        UIImageView* centerImage=[[UIImageView alloc]initWithImage:_logoImage];
        centerImage.backgroundColor=[UIColor whiteColor];
        centerImage.layer.cornerRadius=0.43*0.18*ScreenW/2;
        centerImage.clipsToBounds=YES;
        [_ewm addSubview:centerImage];
        [centerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(_ewm.mas_height).multipliedBy(0.43);
            make.centerY.equalTo(_ewm.mas_centerY);
            make.centerX.equalTo(_ewm.mas_centerX);
        }];
    }else{
        //二维码
        _ewm.image = EWMImageWithString(model.url) ;
//        UIImageView* centerImage=[[UIImageView alloc]initWithImage:_logoImage];
//        centerImage.backgroundColor=[UIColor whiteColor];
//        centerImage.layer.cornerRadius=0.28*0.18*ScreenW/2;
//        centerImage.clipsToBounds=YES;
//        [_ewm addSubview:centerImage];
//        [centerImage mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.height.equalTo(_ewm.mas_height).multipliedBy(0.28);
//            make.centerY.equalTo(_ewm.mas_centerY);
//            make.centerX.equalTo(_ewm.mas_centerX);
//        }];
    }

    [_endPrice setTitle:[NSString stringWithFormat:@"￥%@",model.endPrice] forState:(UIControlStateNormal)];
    _price.text=[NSString stringWithFormat:@"原价￥%@",model.price];
    if (_model.shopModel.nick.length==0) {
        _backView.hidden=YES;
    }else{
        _backView.hidden=NO;
    }
    if (_ifSale==NO) {
        _sale.hidden=YES;
    }else{
        _sale.hidden=NO;
    }
    if (_ifRem==NO) {
        _nameLabel.hidden=YES;
    }else{
        _nameLabel.hidden=NO;
    }

}


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self backImageView];
        [self nameLabel];
        [self goodsTitle];
        [self fromIma];
        [self quan];
        [self sale];
        [self price];
        [self ewm];
        [self goodsImageView];
        [self endPrice];
        [self backView];
        
    }
    return self;
}

-(UIImageView *)backImageView{
    if (!_backImageView) {
        _backImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"shareBack"]];
        _backImageView.backgroundColor=[UIColor whiteColor];
        [self addSubview:_backImageView];
        [_backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.offset(0);
            make.height.offset(ScreenW*1.52);
        }];
    }
    return _backImageView;
}

-(UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel=[UILabel new];
        _nameLabel.textColor=[UIColor redColor];
        _nameLabel.font=[UIFont systemFontOfSize:12*HWB];
        //        _nameLabel.textAlignment=NSTextAlignmentCenter;
        _nameLabel.textColor=[UIColor blackColor];
        PersonalModel* p=[[PersonalModel alloc]initWithDic:NSUDTakeData(UserInfo)];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"我是%@   加入%@随时随地领券喲",p.taobaoAccount,APPNAME]];
        //需要改变的文字
        NSRange range1 = [[str string] rangeOfString:p.taobaoAccount];
        NSRange range2 = [[str string] rangeOfString:APPNAME];
        //字体变红
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range1];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range2];
        //字体变大
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13*HWB] range:range1];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13*HWB] range:range2];
        _nameLabel.attributedText = str;
        [_backImageView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(20*HWB);
            make.left.offset(30*HWB);
            make.right.offset(-30*HWB);
        }];
    }
    return _nameLabel;
}

-(UILabel *)goodsTitle{
    if (!_goodsTitle) {
        _goodsTitle=[UILabel new];
        _goodsTitle.textColor=Black102;
        _goodsTitle.font=[UIFont systemFontOfSize:13*HWB];
        _goodsTitle.numberOfLines=2;
        [_backImageView addSubview:_goodsTitle];
        [_goodsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_left);
            make.right.equalTo(_nameLabel.mas_right);
            make.top.equalTo(_nameLabel.mas_bottom).offset(15*HWB);
            make.height.offset(33*HWB);
        }];
    }
    return _goodsTitle;
}


-(UIImageView *)fromIma{
    if (!_fromIma) {
        _fromIma=[UIImageView new];
        [_backImageView addSubview:_fromIma];
        [_fromIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsTitle.mas_bottom).offset(10*HWB);
            make.left.equalTo(_nameLabel.mas_left);
            make.height.width.offset(16*HWB);
        }];
    }
    return _fromIma;
}

-(UIButton *)quan{
    if (!_quan) {
        _quan=[UIButton new];
        _quan.titleLabel.font=[UIFont systemFontOfSize:10*HWB];
        [_quan setBackgroundImage:[UIImage imageNamed:@"shareQuan"] forState:(UIControlStateNormal)];
        [_backImageView addSubview:_quan];
        [_quan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_fromIma.mas_centerY);
            make.left.equalTo(_fromIma.mas_right).offset(10);
            make.height.offset(16*HWB);
        }];
    }
    return _quan;
}

-(UILabel *)sale{
    if (!_sale) {
        _sale=[UILabel new];
        _sale.textColor=Black153;
        _sale.font=[UIFont systemFontOfSize:11*HWB];
        [_backImageView addSubview:_sale];
        [_sale mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_quan.mas_centerY);
            make.left.equalTo(_quan.mas_right).offset(10);
        }];
    }
    return _sale;
}

-(UIButton *)endPrice{
    if (!_endPrice) {
        _endPrice=[UIButton new];
        _endPrice.titleLabel.font=[UIFont systemFontOfSize:18*HWB];
        [_endPrice setBackgroundImage:[UIImage imageNamed:@"rotate_quan"] forState:(UIControlStateNormal)];
        [_backImageView addSubview:_endPrice];
        [_endPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            if (ScreenW==414) {
                make.right.equalTo(_goodsImageView.mas_right).offset(44*HWB+1);
                make.bottom.equalTo(_goodsImageView.mas_bottom).offset(-0.32*43*HWB);
            }else{
                make.right.equalTo(_goodsImageView.mas_right).offset(42*HWB);
                make.bottom.equalTo(_goodsImageView.mas_bottom).offset(-0.32*43*HWB);
            }
 
            make.width.offset(ScreenW/2);
            make.height.equalTo(_endPrice.mas_width).multipliedBy(0.32);
        }];
        _endPrice.transform=CGAffineTransformMakeRotation(-M_PI/4);
        _endPrice.titleEdgeInsets=UIEdgeInsetsMake(10*HWB, 0, 0, 0);
        UILabel* label=[UILabel new];
        label.textColor=COLORWHITE;
        label.font=[UIFont systemFontOfSize:9*HWB];
        label.text=@"券后只要";
        [_endPrice addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_endPrice.mas_top).offset(8*HWB);
            make.left.equalTo(_endPrice.mas_left).offset(35*HWB);
            
        }];
    }
    return _endPrice;
}

-(UILabel *)price{
    if (!_price) {
        _price=[UILabel new];
        _price.textColor=Black153;
        _price.font=[UIFont systemFontOfSize:14*HWB];
        [_backImageView addSubview:_price];
        [_price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_fromIma.mas_bottom).offset(8*HWB);
            make.left.equalTo(_nameLabel.mas_left);
        }];

    }
    return _price;
}

-(UIImageView *)ewm{
    if (!_ewm) {
        _ewm=[UIImageView new];
        [_backImageView addSubview:_ewm];
        [_ewm mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_goodsTitle.mas_bottom);
            make.right.equalTo(_goodsTitle.mas_right);
            make.height.width.offset(0.18*ScreenW);
        }];
        
    }
    return _ewm;
}


-(UIImageView *)goodsImageView{
    if (!_goodsImageView) {
        _goodsImageView=[UIImageView new];
        _goodsImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        _goodsImageView.layer.shadowOffset = CGSizeMake(0, 0);
        _goodsImageView.layer.shadowOpacity = 0.5;
        _goodsImageView.layer.shadowRadius = 10;
        [_backImageView addSubview:_goodsImageView];
        [_goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_ewm.mas_bottom).offset(10*HWB);
            make.left.equalTo(_nameLabel.mas_left);
            make.right.equalTo(_nameLabel.mas_right);
            make.height.equalTo(_goodsImageView.mas_width);
        }];
    }
    return _goodsImageView;
}

-(UIView *)backView{
    if (!_backView) {
        _backView=[UIView new];
        _backView.backgroundColor=RGBA(221, 222, 223, 1);
        [_backImageView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.bottom.offset(-16*HWB);
            make.height.offset(42*HWB);
        }];
        
        _shopHeader=[UIImageView new];
        _shopHeader.layer.cornerRadius=13*HWB;
        _shopHeader.clipsToBounds=YES;
        [_backView addSubview:_shopHeader];
        [_shopHeader mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView.mas_centerY);
            make.left.offset(15*HWB);
            make.height.width.offset(26*HWB);
        }];
        
        _shopName=[UILabel new];
        _shopName.textColor=Black102;
        _shopName.font=[UIFont systemFontOfSize:13*HWB];
        [_backView addSubview:_shopName];
        [_shopName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_shopHeader.mas_top).offset(-2);
            make.left.equalTo(_shopHeader.mas_right).offset(10);
        }];
        
        _wuliu=[UILabel new];
        _wuliu.textColor=Black102;
        _wuliu.font=[UIFont systemFontOfSize:10*HWB];
        [_backView addSubview:_wuliu];
        [_wuliu mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_shopHeader.mas_bottom).offset(2);
            make.left.equalTo(_shopName.mas_left);
        }];
        
        _shangpin=[UILabel new];
        _shangpin.textColor=Black102;
        _shangpin.font=[UIFont systemFontOfSize:10*HWB];
        [_backView addSubview:_shangpin];
        [_shangpin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_wuliu.mas_centerY);
            make.centerX.equalTo(_backView.mas_centerX).offset(18*HWB);
        }];
        
        _maijia=[UILabel new];
        _maijia.textColor=Black102;
        _maijia.font=[UIFont systemFontOfSize:10*HWB];
        [_backView addSubview:_maijia];
        [_maijia mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_wuliu.mas_centerY);
            make.right.offset(-20);
        }];
        
    }
    return _backView;
}

@end
