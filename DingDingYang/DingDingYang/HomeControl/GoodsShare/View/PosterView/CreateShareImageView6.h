//
//  CreateShareImageView6.h
//  DingDingYang
//
//  Created by ddy on 2018/7/24.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateShareImageView6 : UIView
@property(nonatomic,strong)UIImageView* backImageView;
@property(nonatomic,strong)UIImageView* goodsImageView;
@property(nonatomic,strong)UILabel* goodsTitle;
@property(nonatomic,strong)UIImageView* fromIma;
@property(nonatomic,strong)UILabel* endPrice;
@property(nonatomic,strong)UILabel* price;
@property(nonatomic,strong)UIImageView* ewm;

@property(nonatomic,strong)UIImage* goodsImage;
@property(nonatomic,strong)UIImage* ewmImage;
@property(nonatomic,strong)GoodsModel *model;
@property(nonatomic,strong) GoodsModel * dmModel;
@end
