//
//  CreateShareImageView3.h
//  DingDingYang
//
//  Created by ddy on 2018/6/7.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateShareImageView3 : UIView
@property(nonatomic,strong)UIImageView* backImageView;
@property(nonatomic,strong)UIImageView* goodsImageView;
@property(nonatomic,strong)UIView* backView;
@property(nonatomic,strong)UIImageView* shopHeader;
@property(nonatomic,strong)UILabel* shopName;
@property(nonatomic,strong)UILabel* wuliu;
@property(nonatomic,strong)UILabel* shangpin;
@property(nonatomic,strong)UILabel* maijia;
@property(nonatomic,strong)UILabel* goodsTitle;
@property(nonatomic,strong)UIImageView* fromIma;
@property(nonatomic,strong)UIButton* quan;
@property(nonatomic,strong)UILabel* sale;
@property(nonatomic,strong)UILabel* endPrice;
@property(nonatomic,strong)UILabel* price;
@property(nonatomic,strong)UIImageView* ewm;
@property(nonatomic,strong)UIImage* goodsImage;
@property(nonatomic,strong)UIImage* ewmImage;
@property(nonatomic,strong)UIImage* logoImage;
@property(nonatomic,strong)UIImage* shopHeaderImage;
@property(nonatomic,strong)GoodsModel *model;
@property(nonatomic,assign)BOOL ifRem;
@property(nonatomic,assign)BOOL ifSale;
@property(nonatomic,strong)UILabel* nameLabel;
@end
