//
//  ContentModel.h
//  DingDingYang
//
//  Created by ddy on 2018/6/13.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentModel : NSObject

@property(nonatomic,copy)NSString* des;
@property(nonatomic,copy)NSString* valueId;
@property(nonatomic,copy)NSString* ifShow;
@property(nonatomic,copy)NSString* keyName;
@property(nonatomic,copy)NSString* sorts;
@property(nonatomic,copy)NSString* value;

-(instancetype)initWithModel:(NSDictionary *)dic;
@end
