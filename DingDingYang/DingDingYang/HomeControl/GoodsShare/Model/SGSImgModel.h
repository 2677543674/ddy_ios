//
//  SGSImgModel.h
//  DingDingYang
//
//  Created by ddy on 2017/6/22.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SGSImgModel : NSObject

@property(nonatomic,strong) id  urlImg ;
@property(nonatomic,copy) NSString * isSelect ;
@property(nonatomic,copy) NSString * type;  // 1 小程序码 2 二维码  @"" 未生成
@end
