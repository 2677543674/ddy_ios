//
//  ContentModel.m
//  DingDingYang
//
//  Created by ddy on 2018/6/13.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "ContentModel.h"

@implementation ContentModel

-(instancetype)initWithModel:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        self.des=REPLACENULL(dic[@"des"]);
        self.valueId=REPLACENULL(dic[@"id"]);
        self.ifShow=REPLACENULL(dic[@"ifShow"]);
        self.keyName=REPLACENULL(dic[@"keyName"]);
        self.sorts=REPLACENULL(dic[@"sorts"]);
        self.value=REPLACENULL(dic[@"value"]);
    }
    
    return self;
}


@end
