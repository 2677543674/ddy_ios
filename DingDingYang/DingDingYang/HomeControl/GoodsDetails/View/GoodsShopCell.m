//
//  GoodsShopCell.m
//  DingDingYang
//
//  Created by ddy on 2018/5/14.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "GoodsShopCell.h"

@implementation GoodsShopCell

-(void)setModel:(ShopModel *)model{
    _model=model;
    _name.text=model.shopTitle;
    NSMutableAttributedString *wuLiuScore = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"物流服务 %@",model.deliveryScore]];
    NSRange range = [[wuLiuScore string] rangeOfString:@"物流服务 "];
    [wuLiuScore addAttribute:NSForegroundColorAttributeName value:Black102 range:range];
    _wuLiuScore.attributedText=wuLiuScore;
    
    NSMutableAttributedString *shangPinScore = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"商品描述 %@",model.itemScore]];
    NSRange range2 = [[shangPinScore string] rangeOfString:@"商品描述 "];
    [shangPinScore addAttribute:NSForegroundColorAttributeName value:Black102 range:range2];
    _shangPinScore.attributedText=shangPinScore;
    
    NSMutableAttributedString *maiJiaScore = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"卖家服务 %@",model.serviceScore]];
    NSRange range3 = [[maiJiaScore string] rangeOfString:@"卖家服务 "];
    [maiJiaScore addAttribute:NSForegroundColorAttributeName value:Black102 range:range3];
    _maiJiaScore.attributedText=maiJiaScore;
    
    [_header sd_setImageWithURL:[NSURL URLWithString:model.picPath] placeholderImage:[UIImage imageNamed:@"logo"]];
    if ([model.platform containsString:@"天猫"]) {
        _formIma.image=[UIImage imageNamed:@"tmallicon"];
        _formLabel.text=@"天猫";
    }else {
        _formIma.image=[UIImage imageNamed:@"tbicon"];
        _formLabel.text=@"淘宝";
    }
}


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor=[UIColor whiteColor];
        [self header];
        [self name];
        [self formIma];
        [self formLabel];
        [self wuLiuScore];
        [self shangPinScore];
        [self maiJiaScore];
    }
    return self;
}

-(UIImageView *)header{
    if (_header==nil) {
        _header=[UIImageView new];
        _header.image=[UIImage imageNamed:@"logo"];
        _header.layer.masksToBounds = YES;
        _header.layer.cornerRadius = 15*HWB;
        [self.contentView addSubview:_header];
        [_header mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(5*HWB);
            make.left.offset(12*HWB);
            make.height.width.offset(30*HWB);
            
        }];
        UIView* line=[UIView new];
        line.backgroundColor=[UIColor groupTableViewBackgroundColor];
        [self.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_header.mas_bottom).offset(5*HWB);
            make.left.right.offset(0);
            make.height.offset(1);
        }];
        
//        UIImageView* arrow=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_arrow"]];
//        [self.contentView addSubview:arrow];
//        [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(_header.mas_centerY);
//            make.right.offset(-10*HWB);
//        }];
 
    }
    return _header;
}


-(UILabel *)name{
    if (_name==nil) {
        _name=[UILabel new];
        _name.text=@"店铺名称";
        _name.font=[UIFont systemFontOfSize:12*HWB];
        _name.textColor=Black102;
        [self.contentView addSubview:_name];
        [_name mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_header.mas_right).offset(10*HWB);
            make.top.equalTo(_header.mas_top).offset(2*HWB);
        }];
    }
    return _name;
}


-(UIImageView *)formIma{
    if (_formIma==nil) {
        _formIma=[UIImageView new];
        _formIma.image=[UIImage imageNamed:@"tbicon"];
        [self.contentView addSubview:_formIma];
        [_formIma mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_name.mas_left);
            make.bottom.equalTo(_header.mas_bottom).offset(-2*HWB);
            make.height.width.offset(10*HWB);
        }];
    }
    return _formIma;
}

-(UILabel *)formLabel{
    if (_formLabel==nil) {
        _formLabel=[UILabel new];
        _formLabel.font=[UIFont systemFontOfSize:9.5*HWB];
        _formLabel.text=@"淘宝";
        _formLabel.textColor=Black153;
        [self.contentView addSubview:_formLabel];
        [_formLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_formIma.mas_centerY);
            make.left.equalTo(_formIma.mas_right).offset(4);
        }];
    }
    return _formLabel;
}


-(UILabel *)wuLiuScore{
    if (_wuLiuScore==nil) {
        _wuLiuScore=[UILabel new];
        _wuLiuScore.font=[UIFont systemFontOfSize:11*HWB];
        _wuLiuScore.textColor=[UIColor redColor];
        _wuLiuScore.text=@"物流服务 4.0";
        [self.contentView addSubview:_wuLiuScore];
        [_wuLiuScore mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_header.mas_bottom).offset(10*HWB);
            make.left.equalTo(_header.mas_left);
            make.height.offset(18*HWB);
            
        }];
    }
    return _wuLiuScore;
}

-(UILabel *)shangPinScore{
    if (_shangPinScore==nil) {
        _shangPinScore=[UILabel new];
        _shangPinScore.font=[UIFont systemFontOfSize:11*HWB];
        _shangPinScore.textColor=[UIColor redColor];;
        _shangPinScore.text=@"商品描述 4.0";
        [self.contentView addSubview:_shangPinScore];
        [_shangPinScore mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(_wuLiuScore.mas_centerY);
            make.height.offset(18*HWB);
        }];
    }
    return _shangPinScore;
}

-(UILabel *)maiJiaScore{
    if (_maiJiaScore==nil) {
        _maiJiaScore=[UILabel new];
        _maiJiaScore.font=[UIFont systemFontOfSize:11*HWB];
        _maiJiaScore.textColor=[UIColor redColor];;
        _maiJiaScore.text=@"卖家服务 4.0";
        [self.contentView addSubview:_maiJiaScore];
        [_maiJiaScore mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_wuLiuScore.mas_centerY);
            make.right.offset(-15*HWB);
            make.height.offset(18*HWB);
        }];
    }
    return _maiJiaScore;
}











@end
