//
//  GoodsShopCell.h
//  DingDingYang
//
//  Created by ddy on 2018/5/14.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopModel.h"

@interface GoodsShopCell : UICollectionViewCell

@property(nonatomic,strong)UIImageView* header;     //店铺头像
@property(nonatomic,strong)UILabel* name;           //名称
@property(nonatomic,strong)UIImageView* formIma;
@property(nonatomic,strong)UILabel* formLabel;
@property(nonatomic,strong)UILabel* wuLiuScore;
@property(nonatomic,strong)UILabel* shangPinScore;
@property(nonatomic,strong)UILabel* maiJiaScore;
@property(nonatomic,strong)ShopModel* model;

@end
