//
//  HViewOne.h
//  DingDingYang
//
//  Created by ddy on 2017/5/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"


//商品详情页 轮播图
@interface BannerHeadView : UICollectionReusableView <SDCycleScrollViewDelegate>
@property(nonatomic,strong) SDCycleScrollView * scsView ; // 商品轮播图
@property(nonatomic,strong) UIButton * playVideoBtn; // 视频播放按钮
@property(nonatomic,strong) NSArray * urlAry ;       // 详情页(图片)
@property(nonatomic,strong) GoodsModel * gModel ;
@property(nonatomic,strong) UIActivityIndicatorView * activityView;
@end


// 显示推荐语view
@interface RmdStrFootView : UICollectionReusableView
@property(nonatomic,strong) UIView * rmdStrView ;    // 显示推荐语的view
@property(nonatomic,strong) UILabel * rmdStrLab ;    // 推荐语
@end




//商品详情页 第一分区区头 ( 查看商品详情、按钮 )
@interface HViewOne : UICollectionReusableView
@property(nonatomic,strong) UIButton * clickBtn ;
@property(nonatomic,strong) UILabel * textLab;
@property(nonatomic,strong) UIImageView * arrowImgV;
@property(nonatomic,assign) BOOL isTransform;
@end


//商品详情页 第二分区区头 ( 猜你喜欢的标示 )
@interface HViewTwo : UICollectionReusableView
@property(nonatomic,strong) UIImageView * zanImgV;
@property(nonatomic,strong) UILabel *  recommendLab;
@end




















