//
//  OneGoodsCell.h
//  DingDingYang
//
//  Created by ddy on 2017/5/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"


#pragma mark ===>>> 第一版默认样式

@interface OneGoodsCell : UICollectionViewCell 

@property(nonatomic,strong) UILabel * titleLab ;     // 商品标题
@property(nonatomic,strong) UILabel * rmdStrLab ;    // 推荐语
@property(nonatomic,strong) UILabel * endPriceLab ;  // 券后价
@property(nonatomic,strong) UILabel * oldPriceLab ;  // 原价
@property(nonatomic,strong) UILabel * couponsLab ;   // 优惠券
@property(nonatomic,strong) UILabel * xlLab ;        // 销量
@property(nonatomic,strong) GoodsModel * modeld ;

@end


#pragma mark ===>>> 2 一个大的优惠券显示、占据整个屏幕的宽

@interface OneGoodsCell2 : UICollectionViewCell

@property(nonatomic,strong) UILabel * titleLab ;     // 商品标题
@property(nonatomic,strong) UILabel * oldPriceLab ;  // 原价
@property(nonatomic,strong) UILabel * endPriceLab ;  // 券后价
@property(nonatomic,strong) UILabel * xlLab ;        // 销量
@property(nonatomic,strong) UIButton * leftCpBtn ;   // 左侧显示券的图片按钮
@property(nonatomic,strong) UILabel  * couponsLab ;  // 优惠券
@property(nonatomic,strong) UIButton * rightLedBtn;  // 立即领取按钮
@property(nonatomic,strong) GoodsModel * modeld ;

@end



#pragma mark ===>>> 3 券和领券显示在右侧，渐变色的底色 (销量在下显示)
@interface OneGoodsCell3 : UICollectionViewCell

@property(nonatomic,strong) UILabel * titleLab ;     // 商品标题
@property(nonatomic,strong) UILabel * oldPriceLab ;  // 原价
@property(nonatomic,strong) UILabel * endPriceLab ;  // 券后价
@property(nonatomic,strong) UILabel * xlLab ;        // 销量
@property(nonatomic,strong) UIButton * leftCpBtn ;   // 左侧显示券的图片按钮
@property(nonatomic,strong) UILabel  * couponsLab ;  // 优惠券
@property(nonatomic,strong) UIButton * rightLedBtn;  // 立即领取按钮

@property(nonatomic,strong) GoodsModel * modeld ;

@end


#pragma mark ===>>> 4 券和领券显示在右侧，券底部为纯色 (销量在上显示)
@interface OneGoodsCell4 : UICollectionViewCell

@property(nonatomic,strong) UILabel * titleLab ;     // 商品标题
@property(nonatomic,strong) UILabel * oldPriceLab ;  // 原价
@property(nonatomic,strong) UILabel * endPriceLab ;  // 券后价
@property(nonatomic,strong) UILabel * xlLab ;        // 销量
@property(nonatomic,strong) UIButton * leftCpBtn ;   // 左侧显示券的图片按钮
@property(nonatomic,strong) UILabel  * couponsLab ;  // 优惠券
@property(nonatomic,strong) UIButton * rightLedBtn;  // 立即领取按钮

@property(nonatomic,strong) GoodsModel * modeld ;

@end


#pragma mark ===>>> 5 原价 券后价 券 销量 在同一行 叮当叮当需要（券较小）
@interface OneGoodsCell5 : UICollectionViewCell
    
@property(nonatomic,strong) UILabel * titleLab ;     // 商品标题
    
@property(nonatomic,strong) UILabel * oldPriceLab ;  // 原价
@property(nonatomic,strong) UILabel * endPriceLab ;  // 券后价
@property(nonatomic,strong) UILabel * xlLab ;        // 销量
    
@property(nonatomic,strong) UIButton * leftCpBtn ;   // 左侧显示券的图片按钮
@property(nonatomic,strong) UILabel  * couponsLab ;  // 优惠券
@property(nonatomic,strong) UIButton * rightLedBtn;  // 立即领取按钮
@property(nonatomic,strong) UIView   * backView;
@property(nonatomic,strong) UIButton * youHuiQuan;     //优惠券
@property(nonatomic,strong) GoodsModel * modeld ;
    
@end


#pragma mark ===>>> 6 惠得App（券较小）
@interface OneGoodsCell6 : UICollectionViewCell

@property(nonatomic,strong) UILabel * titleLab ;     // 商品标题
@property(nonatomic,strong) UILabel * oldPriceLab ;  // 原价
@property(nonatomic,strong) UILabel * endPriceLab ;  // 券后价
@property(nonatomic,strong) UILabel * xlLab ;        // 销量
@property(nonatomic,strong) UIButton * leftCpBtn ;   // 左侧显示券的图片按钮
@property(nonatomic,strong) UILabel * quanLabel;     // 优惠券
@property(nonatomic,strong) GoodsModel * modeld ;

@end

#pragma mark ===>>> 6 京东的（券较小）
@interface OneGoodsCell7 : UICollectionViewCell

@property(nonatomic,strong) UILabel * titleLab ;     // 商品标题
@property(nonatomic,strong) UILabel * oldPriceLab ;  // 原价
@property(nonatomic,strong) UILabel * endPriceLab ;  // 券后价
@property(nonatomic,strong) UILabel * xlLab ;        // 销量
@property(nonatomic,strong) UIButton * leftCpBtn ;   // 左侧显示券的图片按钮
@property(nonatomic,strong) UILabel * quanLabel;     // 优惠券
@property(nonatomic,strong) GoodsModel * modeld ;

@end



#pragma mark ===>>> 显示推荐语的cell
@interface RmdStrCltCell : UICollectionViewCell
@property(nonatomic,strong) UIView * rmdStrView ;    // 显示推荐语的view
@property(nonatomic,strong) UILabel * rmdStrLab ;    // 推荐语
@end


#pragma mark ===>>> 商品详情页图片加载
@protocol DetailsImgDeledate <NSObject>
@optional
-(void)cellHeight:(float)hi ; //返回高度
@end

@interface GoodsDetailImgCell : UICollectionViewCell //<UIWebViewDelegate>
@property(nonatomic,assign) id<DetailsImgDeledate>delegate ;
@property(nonatomic,strong) UIImageView * detailImgV;
@property(nonatomic,strong) UIWebView * imgWeb ;
@property(nonatomic,copy) NSString * h5Str ; //

@property(nonatomic,copy) NSString * imgUrl; //
typedef void (^ImgHiBlock)(NSString * hi,NSInteger itemRow);
@property(nonatomic,copy)ImgHiBlock blockHi;
@end








