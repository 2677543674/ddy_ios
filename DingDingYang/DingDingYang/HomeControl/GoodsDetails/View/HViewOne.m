//
//  HViewOne.m
//  DingDingYang
//
//  Created by ddy on 2017/5/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "HViewOne.h"
#import "AVPalyerVC.h"


@implementation BannerHeadView


#pragma mark ===>>> 商品详情页 轮播图

//设置商品图片轮播图
-(void)setUrlAry:(NSMutableArray *)urlAry{
    _urlAry = urlAry ;
    _scsView.imageURLStringsGroup = _urlAry;

    if (_activityView&&_urlAry.count>0) {
        _activityView.hidden = YES;
        [_activityView stopAnimating];
    }

}
-(void)setGModel:(GoodsModel *)gModel{
    _gModel = gModel ;
    if ([_gModel.video hasPrefix:@"http"]) {
        _playVideoBtn.hidden = NO ;
    }else{
        _playVideoBtn.hidden = YES ;
    }
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;
        [self scsView];
        [self playVideoBtn];
        
        _activityView = [[UIActivityIndicatorView alloc]init];
        _activityView.color = COLORGRAY;
        _activityView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        [self addSubview:_activityView];
        [_activityView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.height.with.offset(20*HWB);
        }];
        
        [_activityView startAnimating];
    }
    return self ;
}

-(SDCycleScrollView*)scsView{
    if (!_scsView) {
        _scsView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, ScreenW, ScreenW) imageURLStringsGroup:nil];
        _scsView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _scsView.delegate = self;
        _scsView.backgroundColor = COLORGROUP ;
        _scsView.currentPageDotColor = LOGOCOLOR;
        _scsView.pageDotColor = COLORWHITE;
        _scsView.autoScrollTimeInterval = 3.0;
        _scsView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_scsView];
    }
    return _scsView ;
}

//商品详情页轮播图代理
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (_urlAry.count>0) {
        [ImgPreviewVC show:TopNavc.topViewController type:0 index:index imagesAry:_urlAry];
    }
}

//播放视频按钮
-(UIButton*)playVideoBtn{
    if (!_playVideoBtn) {
        _playVideoBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _playVideoBtn.hidden = YES ;
        _playVideoBtn.frame = CGRectMake(ScreenW-42*HWB-5, ScreenW-42*HWB-5, 42*HWB, 42*HWB);
        [_playVideoBtn setImage:[UIImage imageNamed:@"video_list_start"] forState:(UIControlStateNormal)];
        [_playVideoBtn addTarget:self action:@selector(boFangVideo) forControlEvents:(UIControlEventTouchUpInside)];
        [self addSubview:_playVideoBtn];
    }
    return _playVideoBtn ;
}

-(void)boFangVideo{
    [PageJump openVideoUrlBySystemVideoPlayer:_gModel.video];
}

@end



#pragma mark ===>>> 显示推荐语view

@implementation RmdStrFootView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

        _rmdStrView = [[UIView alloc]init];
        _rmdStrView.backgroundColor = COLORWHITE ;
        [self addSubview:_rmdStrView];
        [_rmdStrView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0);
            make.top.offset(0);  make.bottom.offset(0);
        }];
        
        UIImageView * linesT = [[UIImageView alloc]init];
        linesT.backgroundColor = COLORGROUP ;
        [_rmdStrView addSubview:linesT];
        [linesT mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.height.offset(1);
            make.left.offset(0); make.right.offset(0);
        }];
        
        UIImageView * linesB = [[UIImageView alloc]init];
        linesB.backgroundColor = COLORGROUP ;
        [_rmdStrView addSubview:linesB];
        [linesB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.height.offset(1);
            make.left.offset(0);    make.right.offset(0);
        }];
        
        UILabel * tuiJianCiLab = [UILabel labText:@"推荐词" color:LOGOCOLOR font:11*HWB];
        
        tuiJianCiLab.layer.masksToBounds = YES ;
        tuiJianCiLab.layer.cornerRadius = 3 ;
        tuiJianCiLab.textAlignment = NSTextAlignmentCenter ;
        [tuiJianCiLab boardWidth:1 boardColor:LOGOCOLOR];
        if (THEMECOLOR==2) {
            tuiJianCiLab.textColor=[UIColor redColor];
            [tuiJianCiLab boardWidth:1 boardColor:[UIColor redColor]];
        }
        [_rmdStrView addSubview:tuiJianCiLab];
        [tuiJianCiLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12);  make.top.offset(15);
            make.width.offset(38*HWB);
            make.height.offset(14*HWB);
        }];
        
        _rmdStrLab = [UILabel labText:@"推荐理由" color:Black102 font:12*HWB];
        _rmdStrLab.numberOfLines = 0 ;
        [_rmdStrView addSubview:_rmdStrLab];
        [_rmdStrLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.left.offset(15+40*HWB);  make.right.offset(-15);
            make.top.offset(15);
        }];
    }
    return self;
}

@end



#pragma mark ===>>> 商品详情页 第一分区区头 ( 查看商品详情、按钮 )

@implementation HViewOne

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = COLORWHITE ;
        _textLab = [UILabel labText:@"查看更多宝贝详情" color:Black102 font:13.0*HWB];
        [self addSubview:_textLab];
        [_textLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.centerY.equalTo(self.mas_centerY);
        }];
        
        _clickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_clickBtn];
        [_clickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
        
        _arrowImgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"detail_zhankai"]];
        [self addSubview:_arrowImgV];
        [_arrowImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15); make.centerY.equalTo(self.mas_centerY);
            make.height.offset(25); make.width.offset(25);
        }];
        
    }
    return self;
}

-(void)setIsTransform:(BOOL)isTransform{
    _isTransform = isTransform ;
    if (isTransform==YES) {
        _arrowImgV.image = [UIImage imageNamed:@"detail_shouqi"] ;
    }else{
        _arrowImgV.image = [UIImage imageNamed:@"detail_zhankai"] ;
    }
//    _arrowImgV.transform = CGAffineTransformIdentity ;
//    if (isTransform) {
//        [UIView animateWithDuration:0.16 animations:^{
//            _arrowImgV.transform = CGAffineTransformRotate(_arrowImgV.transform, M_PI);
//        }];
//    }
}

@end



#pragma mark ===>>> 商品详情页 第二分区区头 ( 猜你喜欢的标示 )

@implementation  HViewTwo

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLORWHITE ;

        [self recommendLab];
        [self zanImgV];
        
        UIImageView*linea = [[UIImageView alloc]init];
        linea.backgroundColor = Black204 ;
        [self addSubview:linea];
        [linea mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_centerX).offset(-50);
            make.centerY.equalTo(self.mas_centerY);
            make.height.offset(2); make.width.offset(40);
        }];
        
        UIImageView*lineb = [[UIImageView alloc]init];
        lineb.backgroundColor = Black204 ;
        [self addSubview:lineb];
        [lineb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_centerX).offset(50);
            make.centerY.equalTo(self.mas_centerY);
            make.height.offset(2); make.width.offset(40);
        }];

    }
    return self;
}

//推荐
-(UILabel*)recommendLab{
    if (!_recommendLab) {
        _recommendLab = [UILabel labText:@"猜你喜欢" color:Black102 font:14*HWB];
        [self addSubview:_recommendLab];
        [_recommendLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX).offset(12);
            make.top.offset(0);  make.bottom.offset(0);
        }];
    }
    return _recommendLab;
}

-(UIImageView*)zanImgV{
    if (!_zanImgV) {
        _zanImgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dzan"]];
        [self addSubview:_zanImgV];
        [_zanImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_recommendLab.mas_left).offset(-4);
            make.height.offset(20); make.width.offset(20);
            make.centerY.equalTo(self.mas_centerY);
        }];

    }
    return _zanImgV;
    
}





@end





