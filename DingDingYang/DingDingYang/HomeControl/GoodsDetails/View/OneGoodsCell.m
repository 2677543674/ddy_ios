//
//  OneGoodsCell.m
//  DingDingYang
//
//  Created by ddy on 2017/5/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "OneGoodsCell.h"
#import "AVPalyerVC.h"
@implementation OneGoodsCell

// 商品详情页数据
-(void)setModeld:(GoodsModel *)modeld{
    _modeld = modeld ;
    
//    if (_modeld.attributedGoodsName) {
//        _titleLab.attributedText = _modeld.attributedGoodsName;
//    }else{
//        _titleLab.text = _modeld.goodsName;
//    }
    _titleLab.attributedText = _modeld.attributedGoodsName;
  
    _endPriceLab.text = FORMATSTR(@"¥%@",_modeld.endPrice) ;
    _oldPriceLab.attributedText = [self deleteLine:FORMATSTR(@"¥%@",_modeld.price)];
    _couponsLab.text = FORMATSTR(@"优惠券:%@元",_modeld.couponMoney);
    _xlLab.text = FORMATSTR(@"月销%@",_modeld.sales);
    
    if ([_modeld.couponMoney floatValue]==0) { // 无优惠券
        _couponsLab.hidden = YES ;
        _oldPriceLab.hidden = YES ;
         _endPriceLab.text = FORMATSTR(@"¥%@",_modeld.price) ;
    }
    
    _rmdStrLab.text = modeld.desStr;
    
}

//返回一个删除线风格的字符串
-(NSMutableAttributedString*)deleteLine:(NSString*)str{
    NSRange titleRange = {0,str.length};
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    return dlStr;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self titleLab];
        [self rmdStrLab]; //推荐语
        [self endPriceLab];
        [self oldPriceLab];
        [self couponsLab];
        [self xlLab];

        if (THEMECOLOR==2) {
            _endPriceLab.textColor = COLORBLACK ;
            _couponsLab.textColor = Black51 ;
        }
        
    }
    return self ;
}

//商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);  make.right.offset(-15);
            make.top.offset(6);
        }];
    }
    return _titleLab;
}

//推荐语
-(UILabel*)rmdStrLab{
    if (!_rmdStrLab) {
        _rmdStrLab = [UILabel labText:@"推荐理由" color:LOGOCOLOR font:12*HWB];
        _rmdStrLab.numberOfLines = 0 ;
        [self.contentView addSubview:_rmdStrLab];
        [_rmdStrLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.left.offset(15);  make.right.offset(-15);
            make.top.equalTo(_titleLab.mas_bottom).offset(6);
        }];
    }
    return _rmdStrLab ;
}

//券后价
-(UILabel*)endPriceLab{
    if (!_endPriceLab) {
        _endPriceLab = [UILabel labText:@"¥0.0" color:LOGOCOLOR font:20.0*HWB];
        _endPriceLab.font = [UIFont boldSystemFontOfSize:20.0*HWB];
        [self.contentView addSubview:_endPriceLab];
        [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(20);  make.height.offset(20);
            make.bottom.offset(-10);
        }];
    }
    return _endPriceLab;
}

//原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:Black102 font:12*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(5);
            make.bottom.offset(-10); make.height.offset(15);
        }];
    }
    return _oldPriceLab;
}

//优惠券
-(UILabel*)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UILabel labText:@"优惠券:00.0元" color:COLORWHITE font:FONT_13];
        _couponsLab.backgroundColor = LOGOCOLOR ;
        _couponsLab.textAlignment = NSTextAlignmentCenter ;
        _couponsLab.layer.cornerRadius = 3 ;
        [self.contentView addSubview:_couponsLab];
        [_couponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_oldPriceLab.mas_right).offset(5);
            make.bottom.offset(-10);
            make.width.offset(86);
            make.height.offset(20);
        }];
    }
    return _couponsLab;
}

//销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"月销0万" color:Black102 font:12* HWB];
        [self.contentView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-10); make.bottom.offset(-10);
        }];
    }
    return _xlLab;
}

/*
 //收藏按钮
 -(UIButton*)collectBtn{
     if (!_collectBtn) {
         _collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
         _collectBtn.backgroundColor = COLORWHITE ;
         [_collectBtn setTitle:@"已收藏" forState:UIControlStateNormal];
         _collectBtn.titleLabel.font = Font14 ;
         [_collectBtn setTitleColor:COLORGRAY forState:UIControlStateNormal];
         [_collectBtn setImage:[UIImage imageNamed:@"h_collectbtn"] forState:UIControlStateNormal];
         ADDTARGETBUTTON(_collectBtn, collectGoods:)
         _collectBtn.selected = NO ;
         [_collectBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 21, 25, 21)];
         [_collectBtn setTitleEdgeInsets:UIEdgeInsetsMake(21, -50, 0, 0)];
         [self.contentView addSubview:_collectBtn];
         [_collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
         make.centerY.equalTo(_titleLab.mas_centerY);
         make.height.offset(48); make.width.offset(60);
         make.right.offset(0);
         }];
     }
     return _collectBtn ;
 }
 */


- (void)awakeFromNib {
    [super awakeFromNib];
}




/*
#pragma mark --> 收藏
-(void)collectGoods:(UIButton*)btn{
    if (btn.selected==NO) {
        [_collectBtn setTitle:@"已收藏" forState:UIControlStateNormal];
        [_collectBtn setImage:[UIImage imageNamed:@"h_collectbtn"] forState:UIControlStateNormal];
        _collectBtn.selected = YES;

    }else{
        [_collectBtn setTitle:@"收藏" forState:UIControlStateNormal];
        [_collectBtn setImage:[UIImage imageNamed:@"n_collectbtn"] forState:UIControlStateNormal];
         _collectBtn.selected = NO ;
    }
}
*/

@end




#pragma mark ===>>> 2 一个大的优惠券显示、占据整个屏幕的宽

@implementation OneGoodsCell2

//商品普通数据
-(void)setModeld:(GoodsModel *)modeld{
    _modeld = modeld ;
    
    _titleLab.attributedText = _modeld.attributedGoodsName;

    _endPriceLab.text = _modeld.endPrice ;
    _oldPriceLab.attributedText = [self deleteLine:FORMATSTR(@"原价: ¥%@",_modeld.price)];
    _couponsLab.text = FORMATSTR(@"%@元",_modeld.couponMoney);

    _xlLab.text = FORMATSTR(@"销量%@件",_modeld.sales);
    
//    if ([_modeld.couponMoney floatValue]==0) {
//        _couponsLab.hidden = YES ;
//        _oldPriceLab.hidden = YES ;
//        _endPriceLab.text = FORMATSTR(@"¥%@",_modeld.price) ;
//    }
//
//    _rmdStrLab.text = modeld.desStr ;
    
}

//返回一个删除线风格的字符串
-(NSMutableAttributedString*)deleteLine:(NSString*)str{
    NSRange titleRange = {0,str.length};
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    return dlStr;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self titleLab];
        
        [self oldPriceLab];

        [self creatEndPriceLab]; // 券后价
       
        [self xlLab];
        
        [self creatCouponsView]; // 优惠券
        
    }
    return self ;
}

//商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.right.offset(-15);
            make.top.offset(6*HWB);
        }];
    }
    return _titleLab;
}

//原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:Black102 font:11*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);   make.height.offset(15*HWB);
            make.top.equalTo(_titleLab.mas_bottom).offset(10*HWB);
        }];
    }
    return _oldPriceLab;
}

//券后价
-(void)creatEndPriceLab{
    
    UIButton * qhjImgBtn = [[UIButton alloc]init];
    [qhjImgBtn setTitle:@"券后价 " forState:(UIControlStateNormal)];
    [qhjImgBtn setTitleColor:COLORWHITE forState:UIControlStateNormal];
    UIImage * qhjImg = [[UIImage imageNamed:@"gd_quan_hou_jia"] imageWithRenderingMode:(UIImageRenderingModeAlwaysTemplate)];
    if (THEMECOLOR==2) { qhjImg = [UIImage imageNamed:@"gd_quan_hou_jia"] ; }
    [qhjImgBtn setBackgroundImage:qhjImg forState:(UIControlStateNormal)];
    qhjImgBtn.titleLabel.font = [UIFont systemFontOfSize:11*HWB];
    qhjImgBtn.titleLabel.textAlignment = NSTextAlignmentLeft ;
    [self.contentView addSubview:qhjImgBtn];
    [qhjImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15);  make.height.offset(14*HWB);
        make.width.offset(40*HWB);
        make.top.equalTo(_oldPriceLab.mas_bottom).offset(6*HWB);
    }];
    
    UILabel * moneyIconLab = [UILabel labText:@"￥" color:LOGOCOLOR font:12*HWB];
    [self.contentView addSubview:moneyIconLab];
    [moneyIconLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(qhjImgBtn.mas_right).offset(5);
        make.centerY.equalTo(qhjImgBtn.mas_centerY);
    }];
    
    _endPriceLab = [UILabel labText:@"0.0" color:LOGOCOLOR font:20.0*HWB];
    [self.contentView addSubview:_endPriceLab];
    [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyIconLab.mas_right).offset(0);
        make.bottom.equalTo(qhjImgBtn.mas_bottom).offset(3*HWB);
    }];
}

//销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"销量__万件" color:Black102 font:12*HWB];
        [self.contentView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.right.offset(-15);
            make.centerY.equalTo(_endPriceLab.mas_centerY);
        }];
    }
    return _xlLab;
}

// 创建优惠券
-(void)creatCouponsView{
    [self leftCpBtn];   // 左侧
    [self rightLedBtn]; // 右侧
    [self couponsLab];  // 券额
    
    
    UILabel * yhqLab = [UILabel labText:@"优惠券" color:COLORWHITE font:14*HWB];
    yhqLab.textAlignment = NSTextAlignmentCenter ;
    [_leftCpBtn addSubview:yhqLab];
    [yhqLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_leftCpBtn.mas_centerY).offset(2*HWB);
        make.height.offset(20*HWB);
        make.centerX.equalTo(_leftCpBtn.mas_centerX);
        make.width.offset(60*HWB);
    }];

    UIImageView * linesLeft = [[UIImageView alloc]init];
    linesLeft.backgroundColor = COLORWHITE ;
    [_leftCpBtn addSubview:linesLeft];
    [linesLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(yhqLab.mas_left).offset(0);
        make.left.offset(15*HWB); make.height.offset(1);
        make.centerY.equalTo(yhqLab.mas_centerY);
    }];

    UIImageView * linesRight = [[UIImageView alloc]init];
    linesRight.backgroundColor = COLORWHITE ;
    [_leftCpBtn addSubview:linesRight];
    [linesRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(yhqLab.mas_right).offset(0);
        make.right.offset(-15*HWB); make.height.offset(1);
        make.centerY.equalTo(yhqLab.mas_centerY);
    }];
}

// 左侧
-(UIButton*)leftCpBtn{
    if (!_leftCpBtn) {
        _leftCpBtn = [[UIButton alloc]init];
//        _leftCpBtn.tintColor = SHAREBTNCOLOR ;
        [_leftCpBtn setBackgroundImage:[UIImage imageNamed:@"gd_left_quan1"]  forState:(UIControlStateNormal)];
        [self.contentView addSubview:_leftCpBtn];
        [_leftCpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.height.offset(70*HWB);
            make.width.offset((ScreenW-30)/1.55);
            make.top.equalTo(_endPriceLab.mas_bottom).offset(8*HWB);
        }];
    }
    return _leftCpBtn ;
}

// 右侧
-(UIButton*)rightLedBtn{
    if (!_rightLedBtn) {
        _rightLedBtn = [[UIButton alloc]init];
        [_rightLedBtn setBackgroundImage:[UIImage imageNamed:@"gd_right_quan1"]  forState:(UIControlStateNormal)];
        [_rightLedBtn setTitle:@"立即领券" forState:(UIControlStateNormal)];
        ADDTARGETBUTTON(_rightLedBtn, takeOrder)
        [self.contentView addSubview:_rightLedBtn];
        [_rightLedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15);
            make.left.equalTo(_leftCpBtn.mas_right);
            make.top.equalTo(_leftCpBtn.mas_top);
            make.bottom.equalTo(_leftCpBtn.mas_bottom);
        }];
    }
    return _rightLedBtn ;
}

//优惠券
-(UILabel*)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UILabel labText:@"0.0元" color:COLORWHITE font:22*HWB];
        _couponsLab.font = [UIFont boldSystemFontOfSize:22*HWB];
        [_leftCpBtn addSubview:_couponsLab];
        [_couponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_leftCpBtn.mas_centerX);
            make.bottom.equalTo(_leftCpBtn.mas_centerY);
        }];
    }
    return _couponsLab;
}


-(void)takeOrder{
    if (_modeld.goodsGalleryUrls.count>0) {
        NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
        pdic[@"couponMoney"] = _modeld.couponMoney;
        pdic[@"goodsName"] = _modeld.goodsName;
        pdic[@"endPrice"] = _modeld.endPrice;
        pdic[@"goodsId"] = _modeld.goodsId;
        pdic[@"goodsImg"] = _modeld.goodsPic;
        pdic[@"price"] = _modeld.price;
        pdic[@"ifBuy"] = @"1";
        [NetRequest requestTokenURL:url_getInfo_pdd parameter:nil success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                pdic[@"pid"]=REPLACENULL(nwdic[@"pddPid"]);
                pdic[@"clientId"]=REPLACENULL(nwdic[@"clientId"]);
                pdic[@"clientSecret"]=REPLACENULL(nwdic[@"clientSecret"]);
                pdic[@"parameter"]=REPLACENULL(nwdic[@"parameter"]);
                if (REPLACENULL(nwdic[@"pddPid"]).length>0) {
                    NSString * urlScheme = @"pinduoduo://com.xunmeng.pinduoduo/duo_coupon_landing.html?";
                    NSString* urlString = FORMATSTR(@"%@goods_id=%@&pid=%@",urlScheme,_modeld.goodsId,REPLACENULL(nwdic[@"pddPid"]));
                    NSLog(@"%@",urlString);
                    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"pinduoduo://"]]){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                    }else{
                        [NetRequest requestTokenURL:url_share_buy_pdd parameter:pdic success:^(NSDictionary *nwdic) {
                            NSString * takekOrderUrl = REPLACENULL(nwdic[@"url"]);
                            if (takekOrderUrl.length>0) {
                                if ([takekOrderUrl hasPrefix:@"http"]) {
                                    [WebVC pushWebViewUrlOrStr:takekOrderUrl title:@"领券购买" isSHowType:2];
                                }else{
                                    SHOWMSG(@"领券失败!")
                                }
                            }else{
                                SHOWMSG(@"领券失败!")
                            }
                        } failure:^(NSString *failure) {
                            MBHideHUD SHOWMSG(failure)
                        }];
                    }
                }else{
                    SHOWMSG(@"领券失败!")
                }
            }else{
                SHOWMSG(@"领券失败!");
            }
        } failure:^(NSString *failure) {
            SHOWMSG(@"领券失败!")
        }];
    }else{
        [PageJump takeOrder:_modeld];
    }
}



- (void)awakeFromNib {
    [super awakeFromNib];
}


@end




#pragma mark ===>>> 3 券和领券显示在右侧，渐变色的底色 (销量在下显示)

@implementation OneGoodsCell3

-(void)setModeld:(GoodsModel *)modeld{
    _modeld = modeld ;

    _titleLab.attributedText = _modeld.attributedGoodsName;
    
    _endPriceLab.text = _modeld.endPrice ;
    _oldPriceLab.attributedText = [self deleteLine:FORMATSTR(@"¥%@",_modeld.price)];
    
    _xlLab.text = FORMATSTR(@"销量%@件",_modeld.sales);
    
    // 优惠券
    NSRange rg =  {1,_modeld.couponMoney.length} ;
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_modeld.couponMoney)];
    [dlStr addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:22*HWB]} range:rg];
    _couponsLab.attributedText = dlStr ;
}

//返回一个删除线风格的字符串
-(NSMutableAttributedString*)deleteLine:(NSString*)str{
    NSRange titleRange = {0,str.length};
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    return dlStr;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self titleLab];
        
        [self creatCouponsView]; // 优惠券
        
        [self creatEndPriceLab]; // 券后价
        
        [self oldPriceLab];
        
        [self xlLab];
    
    }
    return self ;
}

//商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.right.offset(-15);
            make.top.offset(6*HWB);
        }];
    }
    return _titleLab;
}

// 创建优惠券
-(void)creatCouponsView{
    
    [self rightLedBtn]; // 右侧
    [self leftCpBtn];   // 左侧
    [self couponsLab];  // 券额
    
    UILabel * yhqLab = [UILabel labText:@"优惠券\nDISCOUNT COUPON" color:COLORWHITE font:9*HWB];
    yhqLab.numberOfLines = 0 ;
    [UILabel changeLineSpaceForLabel:yhqLab WithSpace:2];
    yhqLab.textAlignment = NSTextAlignmentCenter ;
    [_leftCpBtn addSubview:yhqLab];
    [yhqLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_leftCpBtn.mas_centerY);
        make.centerX.equalTo(_leftCpBtn.mas_centerX);
    }];
}

// 右侧
-(UIButton*)rightLedBtn{
    if (!_rightLedBtn) {
        _rightLedBtn = [[UIButton alloc]init];
        [_rightLedBtn setBackgroundImage:[UIImage imageNamed:@"gd_right_quan2"] forState:(UIControlStateNormal)];
        [_rightLedBtn setTitle:@"立\n即\n领\n取" forState:(UIControlStateNormal)];
        _rightLedBtn.titleLabel.font = [UIFont systemFontOfSize:10*HWB];
        _rightLedBtn.titleLabel.numberOfLines = 0 ;
        ADDTARGETBUTTON(_rightLedBtn, takeOrder)
        [self.contentView addSubview:_rightLedBtn];
        [_rightLedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15); make.height.offset(55*HWB);
//            make.bottom.offset(-12*HWB);
            make.top.equalTo(_titleLab.mas_bottom).offset(10*HWB);
            make.width.offset(40*HWB);
        }];
    }
    return _rightLedBtn ;
}

// 左侧
-(UIButton*)leftCpBtn{
    if (!_leftCpBtn) {
        _leftCpBtn = [[UIButton alloc]init];
        [_leftCpBtn setBackgroundImage:[UIImage imageNamed:@"gd_left_quan2"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_leftCpBtn];
        [_leftCpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(110*HWB);
            make.right.equalTo(_rightLedBtn.mas_left);
            make.top.equalTo(_rightLedBtn.mas_top);
            make.bottom.equalTo(_rightLedBtn.mas_bottom);
        }];
    }
    return _leftCpBtn ;
}

//优惠券
-(UILabel*)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UILabel labText:@"￥0.0" color:COLORWHITE font:15*HWB];
        _couponsLab.font = [UIFont boldSystemFontOfSize:15*HWB];
        [_leftCpBtn addSubview:_couponsLab];
        [_couponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_leftCpBtn.mas_centerX);
            make.bottom.equalTo(_leftCpBtn.mas_centerY);
        }];
    }
    return _couponsLab;
}

//券后价
-(void)creatEndPriceLab{
    
    UILabel * moneyIconLab = [UILabel labText:@"券后￥" color:LOGOCOLOR font:12*HWB];
    [self.contentView addSubview:moneyIconLab];
    [moneyIconLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15); make.top.equalTo(_leftCpBtn.mas_top).offset(10*HWB);
    }];
    
    _endPriceLab = [UILabel labText:@"0.0" color:LOGOCOLOR font:20.0*HWB];
    if (THEMECOLOR==2) {
        moneyIconLab.textColor=[UIColor redColor];
        _endPriceLab.textColor=[UIColor redColor];
    }
    [self.contentView addSubview:_endPriceLab];
    [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyIconLab.mas_right).offset(0);
        make.bottom.equalTo(moneyIconLab.mas_bottom).offset(3*HWB);
    }];
}

//原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:Black102 font:11*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(5);
            make.bottom.equalTo(_endPriceLab.mas_bottom).offset(-3*HWB);
        }];
    }
    return _oldPriceLab;
}

//销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"销量__万件" color:Black102 font:12*HWB];
        [self.contentView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.left.offset(15);
            make.bottom.equalTo(_leftCpBtn.mas_bottom).offset(-5*HWB);
        }];
    }
    return _xlLab;
}

-(void)takeOrder{
    if (_modeld.goodsGalleryUrls.count>0) {
        NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
        pdic[@"couponMoney"] = _modeld.couponMoney;
        pdic[@"goodsName"] = _modeld.goodsName;
        pdic[@"endPrice"] = _modeld.endPrice;
        pdic[@"goodsId"] = _modeld.goodsId;
        pdic[@"goodsImg"] = _modeld.goodsPic;
        pdic[@"price"] = _modeld.price;
        pdic[@"ifBuy"] = @"1";
        [NetRequest requestTokenURL:url_getInfo_pdd parameter:nil success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                pdic[@"pid"]=REPLACENULL(nwdic[@"pddPid"]);
                pdic[@"clientId"]=REPLACENULL(nwdic[@"clientId"]);
                pdic[@"clientSecret"]=REPLACENULL(nwdic[@"clientSecret"]);
                pdic[@"parameter"]=REPLACENULL(nwdic[@"parameter"]);
                if (REPLACENULL(nwdic[@"pddPid"]).length>0) {
                    NSString * urlScheme = @"pinduoduo://com.xunmeng.pinduoduo/duo_coupon_landing.html?";
                    NSString* urlString = FORMATSTR(@"%@goods_id=%@&pid=%@",urlScheme,_modeld.goodsId,REPLACENULL(nwdic[@"pddPid"]));
                    NSLog(@"%@",urlString);
                    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"pinduoduo://"]]){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                    }else{
                        [NetRequest requestTokenURL:url_share_buy_pdd parameter:pdic success:^(NSDictionary *nwdic) {
                            NSString * takekOrderUrl = REPLACENULL(nwdic[@"url"]);
                            if (takekOrderUrl.length>0) {
                                if ([takekOrderUrl hasPrefix:@"http"]) {
                                    [WebVC pushWebViewUrlOrStr:takekOrderUrl title:@"领券购买" isSHowType:2];
                                }else{
                                    SHOWMSG(@"领券失败!")
                                }
                            }else{
                                SHOWMSG(@"领券失败!")
                            }
                        } failure:^(NSString *failure) {
                            MBHideHUD SHOWMSG(failure)
                        }];
                    }
                }else{
                    SHOWMSG(@"领券失败!")
                }
            }else{
                SHOWMSG(@"领券失败!");
            }
        } failure:^(NSString *failure) {
            SHOWMSG(@"领券失败!")
        }];
    }else{
        [PageJump takeOrder:_modeld];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}


@end






#pragma mark ===>>> 4 券和领券显示在右侧，券底部为纯色 (销量在上显示)

@implementation OneGoodsCell4

-(void)setModeld:(GoodsModel *)modeld{
    _modeld = modeld ;

    _titleLab.attributedText = _modeld.attributedGoodsName;

    _endPriceLab.text = _modeld.endPrice ;
    
    _oldPriceLab.attributedText = [self deleteLine:FORMATSTR(@"¥%@",_modeld.price)];
    
    _xlLab.text = FORMATSTR(@"月销%@笔",_modeld.sales);

    // 优惠券
    NSRange rg =  {1,_modeld.couponMoney.length} ;
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:FORMATSTR(@"￥%@",_modeld.couponMoney)];
    [dlStr addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:22*HWB]} range:rg];
    _couponsLab.attributedText = dlStr ;
}

//返回一个删除线风格的字符串
-(NSMutableAttributedString*)deleteLine:(NSString*)str{
    NSRange titleRange = {0,str.length};
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    return dlStr;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self titleLab];
        
        [self creatCouponsView]; // 优惠券
        
        [self xlLab]; // 销量

        [self creatEndPriceLab]; // 券后价

        [self oldPriceLab];
        
    }
    return self ;
}

//商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(6*HWB); make.left.offset(15);
            make.right.offset(-15);
        }];
    }
    return _titleLab;
}

// 创建优惠券
-(void)creatCouponsView{
    
    [self rightLedBtn]; // 右侧
    [self leftCpBtn];   // 左侧
    [self couponsLab];  // 券额

    UILabel * yhqLab = [UILabel labText:@"优惠券" color:RGBA(199,13,16,1) font:10*HWB];
    yhqLab.textAlignment = NSTextAlignmentCenter ;
    [_leftCpBtn addSubview:yhqLab];
    [yhqLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_couponsLab.mas_bottom);
//        make.bottom.equalTo(_leftCpBtn.mas_bottom);
        make.centerX.equalTo(_leftCpBtn.mas_centerX);
    }];

    UIImageView * linesLeft = [[UIImageView alloc]init];
    linesLeft.backgroundColor = RGBA(199,13,16,1) ;
    [_leftCpBtn addSubview:linesLeft];
    [linesLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(yhqLab.mas_left).offset(-5);
        make.left.offset(15*HWB); make.height.offset(1);
        make.centerY.equalTo(yhqLab.mas_centerY);
    }];

    UIImageView * linesRight = [[UIImageView alloc]init];
    linesRight.backgroundColor = RGBA(199,13,16,1) ;
    [_leftCpBtn addSubview:linesRight];
    [linesRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(yhqLab.mas_right).offset(5);
        make.right.offset(-15*HWB); make.height.offset(1);
        make.centerY.equalTo(yhqLab.mas_centerY);
    }];
    
}

// 右侧
-(UIButton*)rightLedBtn{
    if (!_rightLedBtn) {
        _rightLedBtn = [[UIButton alloc]init];
        [_rightLedBtn setBackgroundImage:[UIImage imageNamed:@"gd_right_quan3"] forState:(UIControlStateNormal)];
        [_rightLedBtn setTitle:@"立\n即\n领\n取" forState:(UIControlStateNormal)];
        _rightLedBtn.titleLabel.font = [UIFont systemFontOfSize:10*HWB];
        _rightLedBtn.titleLabel.numberOfLines = 0 ;
        ADDTARGETBUTTON(_rightLedBtn, takeOrder)
        [self.contentView addSubview:_rightLedBtn];
        [_rightLedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(55*HWB);  make.width.offset(30*HWB);
            make.top.equalTo(_titleLab.mas_bottom).offset(10*HWB);
            make.right.offset(-15);
        }];
    }
    return _rightLedBtn ;
}

// 左侧
-(UIButton*)leftCpBtn{
    if (!_leftCpBtn) {
        _leftCpBtn = [[UIButton alloc]init];
        [_leftCpBtn setBackgroundImage:[UIImage imageNamed:@"gd_left_quan3"] forState:(UIControlStateNormal)];
        [self.contentView addSubview:_leftCpBtn];
        [_leftCpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.offset(110*HWB);
            make.right.equalTo(_rightLedBtn.mas_left);
            make.top.equalTo(_rightLedBtn.mas_top);
            make.bottom.equalTo(_rightLedBtn.mas_bottom);
        }];
    }
    return _leftCpBtn ;
}

//优惠券
-(UILabel*)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UILabel labText:@"￥0.0" color:RGBA(199,13,16,1) font:15*HWB];
        _couponsLab.font = [UIFont boldSystemFontOfSize:15*HWB];
        [_leftCpBtn addSubview:_couponsLab];
        [_couponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_leftCpBtn.mas_centerX);
            make.bottom.equalTo(_leftCpBtn.mas_centerY).offset(5*HWB);
        }];
    }
    return _couponsLab;
}


//销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"销量__万件" color:Black102 font:12*HWB];
        [self.contentView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.left.offset(15);
            make.top.equalTo(_leftCpBtn.mas_top).offset(5*HWB);
//            make.bottom.equalTo(_leftCpBtn.mas_bottom).offset(-5*HWB);
        }];
    }
    return _xlLab;
}


//券后价
-(void)creatEndPriceLab{
    
    UILabel * moneyIconLab = [UILabel labText:@"￥" color:LOGOCOLOR font:12*HWB];
    if (THEMECOLOR==2) {
        moneyIconLab.textColor=[UIColor redColor];
    }
    [self.contentView addSubview:moneyIconLab];
    [moneyIconLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15);
        make.bottom.equalTo(_leftCpBtn.mas_bottom).offset(-5*HWB);
    }];
    
    _endPriceLab = [UILabel labText:@"0.0" color:LOGOCOLOR font:20.0*HWB];
    if (THEMECOLOR==2) {
        _endPriceLab.textColor=[UIColor redColor];
    }
    _endPriceLab.font = [UIFont boldSystemFontOfSize:20.0*HWB];
    [self.contentView addSubview:_endPriceLab];
    [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyIconLab.mas_right).offset(0);
        make.bottom.equalTo(_leftCpBtn.mas_bottom).offset(-3*HWB);
    }];
}

//原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:Black102 font:11*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(5);
            make.bottom.equalTo(_endPriceLab.mas_bottom).offset(-3*HWB);
        }];
    }
    return _oldPriceLab;
}

-(void)takeOrder{
    if (_modeld.goodsGalleryUrls.count>0) {
        NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
        pdic[@"couponMoney"] = _modeld.couponMoney;
        pdic[@"goodsName"] = _modeld.goodsName;
        pdic[@"endPrice"] = _modeld.endPrice;
        pdic[@"goodsId"] = _modeld.goodsId;
        pdic[@"goodsImg"] = _modeld.goodsPic;
        pdic[@"price"] = _modeld.price;
        pdic[@"ifBuy"] = @"1";
        [NetRequest requestTokenURL:url_getInfo_pdd parameter:nil success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                pdic[@"pid"]=REPLACENULL(nwdic[@"pddPid"]);
                pdic[@"clientId"]=REPLACENULL(nwdic[@"clientId"]);
                pdic[@"clientSecret"]=REPLACENULL(nwdic[@"clientSecret"]);
                pdic[@"parameter"]=REPLACENULL(nwdic[@"parameter"]);
                if (REPLACENULL(nwdic[@"pddPid"]).length>0) {
                    NSString * urlScheme = @"pinduoduo://com.xunmeng.pinduoduo/duo_coupon_landing.html?";
                    NSString* urlString = FORMATSTR(@"%@goods_id=%@&pid=%@",urlScheme,_modeld.goodsId,REPLACENULL(nwdic[@"pddPid"]));
                    NSLog(@"%@",urlString);
                    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"pinduoduo://"]]){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                    }else{
                        [NetRequest requestTokenURL:url_share_buy_pdd parameter:pdic success:^(NSDictionary *nwdic) {
                            NSString * takekOrderUrl = REPLACENULL(nwdic[@"url"]);
                            if (takekOrderUrl.length>0) {
                                if ([takekOrderUrl hasPrefix:@"http"]) {
                                    [WebVC pushWebViewUrlOrStr:takekOrderUrl title:@"领券购买" isSHowType:2];
                                }else{
                                    SHOWMSG(@"领券失败!")
                                }
                            }else{
                                SHOWMSG(@"领券失败!")
                            }
                        } failure:^(NSString *failure) {
                            MBHideHUD SHOWMSG(failure)
                        }];
                    }
                }else{
                    SHOWMSG(@"领券失败!")
                }
            }else{
                SHOWMSG(@"领券失败!");
            }
        } failure:^(NSString *failure) {
            SHOWMSG(@"领券失败!")
        }];
    }else{
        [PageJump takeOrder:_modeld];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}


@end


#pragma mark ===>>> 5 原价 券后价 券 销量 在同一行 叮当叮当需要（券较小）

@implementation OneGoodsCell5
    
-(void)setModeld:(GoodsModel *)modeld{
    _modeld = modeld ;
    
    _titleLab.attributedText = _modeld.attributedGoodsName;

    _endPriceLab.text = _modeld.endPrice ;
    _oldPriceLab.attributedText = [self deleteLine:FORMATSTR(@"¥%@",_modeld.price)];
    
    _xlLab.text = FORMATSTR(@"销量%@件",_modeld.sales);
    
    [_youHuiQuan setTitle:FORMATSTR(@"¥%@",_modeld.couponMoney) forState:UIControlStateNormal];
}
    
    //返回一个删除线风格的字符串
-(NSMutableAttributedString*)deleteLine:(NSString*)str{
    NSRange titleRange = {0,str.length};
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    return dlStr;
}
    
    
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self titleLab];
        
        [self creatEndPriceLab]; // 券后价
        
        [self oldPriceLab];
        
        [self youHuiQuan]; // 优惠券
        
        [self xlLab];
        
    }
    return self ;
}
    
    //商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black51 font:13*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.right.offset(-15);
            make.top.offset(6*HWB);
        }];
    }
    return _titleLab;
}
    
    
//券后价
-(void)creatEndPriceLab{
    
    UILabel * moneyIconLab = [UILabel labText:@"￥" color:LOGOCOLOR font:12*HWB];
    [self.contentView addSubview:moneyIconLab];
    [moneyIconLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15); make.top.equalTo(_titleLab.mas_bottom).offset(15*HWB);
    }];
    
    _endPriceLab = [UILabel labText:@"0.0" color:LOGOCOLOR font:20.0*HWB];
    [self.contentView addSubview:_endPriceLab];
    [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyIconLab.mas_right).offset(0);
        make.bottom.equalTo(moneyIconLab.mas_bottom).offset(3*HWB);
    }];
}

// 原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:Black102 font:11*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_endPriceLab.mas_right).offset(5);
            make.bottom.equalTo(_endPriceLab.mas_bottom).offset(-3*HWB);
        }];
    }
    return _oldPriceLab;
}
    
    
-(UIButton *)youHuiQuan{
    if (!_youHuiQuan) {
        _backView = [UIView new];
        _backView.backgroundColor = COLORWHITE ;
        _backView.layer.borderWidth = 1;
        _backView.layer.borderColor = RGBA(250, 81, 57, 1).CGColor;
        _backView.layer.cornerRadius = 4;
        _backView.clipsToBounds = YES;
        [self.contentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_oldPriceLab.mas_right).offset(8*HWB);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
            make.height.offset(18*HWB);
            make.width.offset(60*HWB);
        }];
        
        UILabel * quan = [UILabel labText:@"券" color:COLORWHITE font:11*HWB];

        quan.textAlignment=NSTextAlignmentCenter;
        quan.backgroundColor=RGBA(250, 81, 57, 1);
        
        [_backView addSubview:quan];
        [quan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView.mas_centerY);
            make.width.offset(25*HWB);
            make.height.offset(18*HWB);
            make.left.offset(0);
        }];
        
        _youHuiQuan = [UIButton buttonWithType:UIButtonTypeCustom];
        [_youHuiQuan setTitleColor:RGBA(250, 81, 57, 1) forState:UIControlStateNormal];

        [_youHuiQuan setBackgroundColor:[UIColor whiteColor]];
        _youHuiQuan.titleLabel.font = [UIFont systemFontOfSize:11*HWB];
        [_backView addSubview:_youHuiQuan];
        [_youHuiQuan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(quan.mas_right).offset(0);
            make.centerY.equalTo(_backView.mas_centerY);
            make.width.offset(35*HWB);
            make.height.offset(18*HWB);
        }];
        
        UIButton* takeOrderBtn=[UIButton new];
        [takeOrderBtn addTarget:self action:@selector(takeOrder) forControlEvents:UIControlEventTouchUpInside];
        [_backView addSubview:takeOrderBtn];
        [takeOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.offset(0);
        }];
    }
    return _youHuiQuan;
}
    //销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"销量__万件" color:Black102 font:12*HWB];
        [self.contentView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.right.offset(-10*HWB);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
        }];
    }
    return _xlLab;
}
    
-(void)takeOrder{
    if (_modeld.goodsGalleryUrls.count>0) {
        NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
        pdic[@"couponMoney"] = _modeld.couponMoney;
        pdic[@"goodsName"] = _modeld.goodsName;
        pdic[@"endPrice"] = _modeld.endPrice;
        pdic[@"goodsId"] = _modeld.goodsId;
        pdic[@"goodsImg"] = _modeld.goodsPic;
        pdic[@"price"] = _modeld.price;
        pdic[@"ifBuy"] = @"1";
        [NetRequest requestTokenURL:url_getInfo_pdd parameter:nil success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                pdic[@"pid"]=REPLACENULL(nwdic[@"pddPid"]);
                pdic[@"clientId"]=REPLACENULL(nwdic[@"clientId"]);
                pdic[@"clientSecret"]=REPLACENULL(nwdic[@"clientSecret"]);
                pdic[@"parameter"]=REPLACENULL(nwdic[@"parameter"]);
                if (REPLACENULL(nwdic[@"pddPid"]).length>0) {
                    NSString * urlScheme = @"pinduoduo://com.xunmeng.pinduoduo/duo_coupon_landing.html?";
                    NSString* urlString = FORMATSTR(@"%@goods_id=%@&pid=%@",urlScheme,_modeld.goodsId,REPLACENULL(nwdic[@"pddPid"]));
                    NSLog(@"%@",urlString);
                    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"pinduoduo://"]]){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                    }else{
                        [NetRequest requestTokenURL:url_share_buy_pdd parameter:pdic success:^(NSDictionary *nwdic) {
                            NSString * takekOrderUrl = REPLACENULL(nwdic[@"url"]);
                            if (takekOrderUrl.length>0) {
                                if ([takekOrderUrl hasPrefix:@"http"]) {
                                    [WebVC pushWebViewUrlOrStr:takekOrderUrl title:@"领券购买" isSHowType:2];
                                }else{
                                    SHOWMSG(@"领券失败!")
                                }
                            }else{
                                SHOWMSG(@"领券失败!")
                            }
                        } failure:^(NSString *failure) {
                            MBHideHUD SHOWMSG(failure)
                        }];
                    }
                }else{
                    SHOWMSG(@"领券失败!")
                }
            }else{
                SHOWMSG(@"领券失败!");
            }
        } failure:^(NSString *failure) {
            SHOWMSG(@"领券失败!")
        }];
    }else{
        [PageJump takeOrder:_modeld];
    }
}
    
- (void)awakeFromNib {
    [super awakeFromNib];
}
    
    
@end


#pragma mark ===>>> 6 惠得（券较小）

@implementation OneGoodsCell6

-(void)setModeld:(GoodsModel *)modeld{
    _modeld = modeld ;
    
    _titleLab.attributedText = _modeld.attributedGoodsName;
    
    _endPriceLab.text = _modeld.endPrice ;
    _oldPriceLab.attributedText = [self deleteLine:FORMATSTR(@"原价¥%@",_modeld.price)];
    
    _xlLab.text = FORMATSTR(@"销量%@件",_modeld.sales);
    
    _quanLabel.text=FORMATSTR(@"￥%@",_modeld.couponMoney);
    
    if (StringSize(_quanLabel.text,11*HWB).width-36>0) {
        [[self viewWithTag:333] mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
            make.width.offset(54*HWB+StringSize(_quanLabel.text,11*HWB).width-34);
            make.height.offset(18*HWB);
        }];
    }else{
        
    }

    
}

//返回一个删除线风格的字符串
-(NSMutableAttributedString*)deleteLine:(NSString*)str{
    NSRange titleRange = {0,str.length};
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    return dlStr;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self titleLab];
        
        [self creatEndPriceLab]; // 券后价
        
        [self oldPriceLab];
        
        [self quanLabel]; // 优惠券
        
        [self xlLab];
        
    }
    return self ;
}

//商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black102 font:13*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.right.offset(-15);
            make.top.offset(6*HWB);
        }];
    }
    return _titleLab;
}


//券后价
-(void)creatEndPriceLab{
    
    UILabel * moneyIconLab = [UILabel labText:@"券后￥" color:LOGOCOLOR font:11*HWB];
    [self.contentView addSubview:moneyIconLab];
    [moneyIconLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15); make.top.equalTo(_titleLab.mas_bottom).offset(12*HWB);
    }];
    
    _endPriceLab = [UILabel labText:@"0.0" color:LOGOCOLOR font:17.0*HWB];
    [self.contentView addSubview:_endPriceLab];
    [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyIconLab.mas_right).offset(0);
        make.bottom.equalTo(moneyIconLab.mas_bottom).offset(2*HWB);
    }];
}

//原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:Black102 font:11*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleLab.mas_left);
            make.top.equalTo(_endPriceLab.mas_bottom).offset(10*HWB);
        }];
    }
    return _oldPriceLab;
}


-(UILabel *)quanLabel{
    if (!_quanLabel) {
        UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"quan_6"]];
        imageView.tag=333;
        [self.contentView addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
            make.width.offset(54*HWB);
            make.height.offset(18*HWB);
        }];
        
        UILabel* label=[UILabel new];
        label.text=@"券";
        label.textColor=COLORWHITE;
        label.font=[UIFont systemFontOfSize:11*HWB];
        label.textAlignment=NSTextAlignmentCenter;
        [imageView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(imageView.mas_height);
            make.centerY.equalTo(imageView.mas_centerY);
            make.left.equalTo(imageView.mas_left).offset(5*HWB);
        }];

        UIView* line=[UIView new];
        line.backgroundColor=[[UIColor whiteColor]  colorWithAlphaComponent:0.7];
        [imageView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(4*HWB);
            make.bottom.offset(-4*HWB);
            make.width.offset(1);
            make.left.equalTo(label.mas_right).offset(3*HWB);
        }];
        
        _quanLabel=[UILabel new];
        _quanLabel.textColor=COLORWHITE;
        _quanLabel.font=[UIFont systemFontOfSize:11*HWB];
        _quanLabel.textAlignment=NSTextAlignmentCenter;
        [imageView addSubview:_quanLabel];
        [_quanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(imageView.mas_height);
            make.centerY.equalTo(imageView.mas_centerY);
            make.left.equalTo(line.mas_right);
            make.right.equalTo(imageView.mas_right).offset(-2*HWB);
        }];
    }
    return _quanLabel;
}
//销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"销量__万件" color:Black102 font:11*HWB];
        [self.contentView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
        }];
    }
    return _xlLab;
}

-(void)takeOrder{
    if (_modeld.goodsGalleryUrls.count>0) {
        NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
        pdic[@"couponMoney"] = _modeld.couponMoney;
        pdic[@"goodsName"] = _modeld.goodsName;
        pdic[@"endPrice"] = _modeld.endPrice;
        pdic[@"goodsId"] = _modeld.goodsId;
        pdic[@"goodsImg"] = _modeld.goodsPic;
        pdic[@"price"] = _modeld.price;
        pdic[@"ifBuy"] = @"1";
        [NetRequest requestTokenURL:url_getInfo_pdd parameter:nil success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                pdic[@"pid"]=REPLACENULL(nwdic[@"pddPid"]);
                pdic[@"clientId"]=REPLACENULL(nwdic[@"clientId"]);
                pdic[@"clientSecret"]=REPLACENULL(nwdic[@"clientSecret"]);
                pdic[@"parameter"]=REPLACENULL(nwdic[@"parameter"]);
                if (REPLACENULL(nwdic[@"pddPid"]).length>0) {
                    NSString * urlScheme = @"pinduoduo://com.xunmeng.pinduoduo/duo_coupon_landing.html?";
                    NSString* urlString = FORMATSTR(@"%@goods_id=%@&pid=%@",urlScheme,_modeld.goodsId,REPLACENULL(nwdic[@"pddPid"]));
                    NSLog(@"%@",urlString);
                    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"pinduoduo://"]]){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                    }else{
                        [NetRequest requestTokenURL:url_share_buy_pdd parameter:pdic success:^(NSDictionary *nwdic) {
                            NSString * takekOrderUrl = REPLACENULL(nwdic[@"url"]);
                            if (takekOrderUrl.length>0) {
                                if ([takekOrderUrl hasPrefix:@"http"]) {
                                    [WebVC pushWebViewUrlOrStr:takekOrderUrl title:@"领券购买" isSHowType:2];
                                }else{
                                    SHOWMSG(@"领券失败!")
                                }
                            }else{
                                SHOWMSG(@"领券失败!")
                            }
                        } failure:^(NSString *failure) {
                            MBHideHUD SHOWMSG(failure)
                        }];
                    }
                }else{
                    SHOWMSG(@"领券失败!")
                }
            }else{
                SHOWMSG(@"领券失败!");
            }
        } failure:^(NSString *failure) {
            SHOWMSG(@"领券失败!")
        }];
    }else{
        [PageJump takeOrder:_modeld];
    }
    
    
//    if (_modeld) {
//        [PageJump takeOrder:_modeld];
//    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}


@end


#pragma mark ===>>> 7 京东（券较小）

@implementation OneGoodsCell7

-(void)setModeld:(GoodsModel *)modeld{
    _modeld = modeld ;
    
    _titleLab.attributedText = _modeld.attributedGoodsName;
    
    _endPriceLab.text = _modeld.endPrice ;
    _oldPriceLab.attributedText = [self deleteLine:FORMATSTR(@"原价¥%@",_modeld.price)];
    
    _xlLab.text = FORMATSTR(@"%@ 人已买",_modeld.sales);
    
    _quanLabel.text=FORMATSTR(@"￥%@",_modeld.couponMoney);
        
}

//返回一个删除线风格的字符串
-(NSMutableAttributedString*)deleteLine:(NSString*)str{
    NSRange titleRange = {0,str.length};
    NSMutableAttributedString * dlStr = [[NSMutableAttributedString alloc] initWithString:str];
    [dlStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:titleRange];
    return dlStr;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = COLORWHITE ;
        
        [self titleLab];
        
        [self creatEndPriceLab]; // 券后价
        
        [self oldPriceLab];
        
        [self quanLabel]; // 优惠券
        
        [self xlLab];
        
    }
    return self ;
}

//商品标题
-(UILabel*)titleLab{
    if (!_titleLab ) {
        _titleLab = [UILabel labText:@"商品标题" color:Black102 font:13*HWB];
        _titleLab.backgroundColor = COLORWHITE ;
        _titleLab.numberOfLines = 0 ;
        [self.contentView addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15); make.right.offset(-15);
            make.top.offset(6*HWB);
        }];
    }
    return _titleLab;
}


//券后价
-(void)creatEndPriceLab{
    
    UILabel * moneyIconLab = [UILabel labText:@"券后￥" color:LOGOCOLOR font:11*HWB];
    [self.contentView addSubview:moneyIconLab];
    [moneyIconLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15); make.top.equalTo(_titleLab.mas_bottom).offset(12*HWB);
    }];
    
    _endPriceLab = [UILabel labText:@"0.0" color:LOGOCOLOR font:17.0*HWB];
    [self.contentView addSubview:_endPriceLab];
    [_endPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyIconLab.mas_right).offset(0);
        make.bottom.equalTo(moneyIconLab.mas_bottom).offset(2*HWB);
    }];
}

//原价
-(UILabel*)oldPriceLab{
    if (!_oldPriceLab) {
        _oldPriceLab = [UILabel labText:@"¥0.0" color:Black102 font:11*HWB];
        _oldPriceLab.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_oldPriceLab];
        [_oldPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleLab.mas_left);
            make.top.equalTo(_endPriceLab.mas_bottom).offset(10*HWB);
        }];
    }
    return _oldPriceLab;
}


-(UILabel *)quanLabel{
    if (!_quanLabel) {
        UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"goods_jd_quan_d"]];
        imageView.tag=333;
        [self.contentView addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15*HWB);
            make.centerY.equalTo(_endPriceLab.mas_centerY);
            make.width.offset(54*HWB);
            make.height.offset(18*HWB);
        }];

        UILabel* label=[UILabel new];
        label.text=@"券";
        label.textColor=COLORWHITE;
        label.font=[UIFont systemFontOfSize:11*HWB];
        label.textAlignment=NSTextAlignmentCenter;
        [imageView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(imageView.mas_height);
            make.centerY.equalTo(imageView.mas_centerY);
            make.left.equalTo(imageView.mas_left).offset(5*HWB);
        }];

        UIView* line=[UIView new];
        line.backgroundColor=[[UIColor whiteColor]  colorWithAlphaComponent:0.7];
        [imageView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(4*HWB);
            make.bottom.offset(-4*HWB);
            make.width.offset(1);
            make.left.equalTo(label.mas_right).offset(3*HWB);
        }];
        
        _quanLabel=[UILabel new];
        _quanLabel.textColor=COLORWHITE;
        _quanLabel.font=[UIFont systemFontOfSize:11*HWB];
        _quanLabel.textAlignment=NSTextAlignmentCenter;
        [imageView addSubview:_quanLabel];
        [_quanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(imageView.mas_height);
            make.centerY.equalTo(imageView.mas_centerY);
            make.left.equalTo(line.mas_right);
            make.right.equalTo(imageView.mas_right).offset(-2*HWB);
        }];
    }
    return _quanLabel;
}
//销量
-(UILabel*)xlLab{
    if (!_xlLab) {
        _xlLab = [UILabel labText:@"0 人已买" color:Black102 font:11*HWB];
        [self.contentView addSubview:_xlLab];
        [_xlLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.centerY.equalTo(_oldPriceLab.mas_centerY);
            make.right.offset(-15*HWB);
        }];
    }
    return _xlLab;
}

-(void)takeOrder{
    if (_modeld.goodsGalleryUrls.count>0) {
        NSMutableDictionary * pdic = [NSMutableDictionary dictionary];
        pdic[@"couponMoney"] = _modeld.couponMoney;
        pdic[@"goodsName"] = _modeld.goodsName;
        pdic[@"endPrice"] = _modeld.endPrice;
        pdic[@"goodsId"] = _modeld.goodsId;
        pdic[@"goodsImg"] = _modeld.goodsPic;
        pdic[@"price"] = _modeld.price;
        pdic[@"ifBuy"] = @"1";
        [NetRequest requestTokenURL:url_getInfo_pdd parameter:nil success:^(NSDictionary *nwdic) {
            if ([nwdic[@"result"] isEqualToString:@"OK"]) {
                pdic[@"pid"]=REPLACENULL(nwdic[@"pddPid"]);
                pdic[@"clientId"]=REPLACENULL(nwdic[@"clientId"]);
                pdic[@"clientSecret"]=REPLACENULL(nwdic[@"clientSecret"]);
                pdic[@"parameter"]=REPLACENULL(nwdic[@"parameter"]);
                if (REPLACENULL(nwdic[@"pddPid"]).length>0) {
                    NSString * urlScheme = @"pinduoduo://com.xunmeng.pinduoduo/duo_coupon_landing.html?";
                    NSString* urlString = FORMATSTR(@"%@goods_id=%@&pid=%@",urlScheme,_modeld.goodsId,REPLACENULL(nwdic[@"pddPid"]));
                    NSLog(@"%@",urlString);
                    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"pinduoduo://"]]){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                    }else{
                        [NetRequest requestTokenURL:url_share_buy_pdd parameter:pdic success:^(NSDictionary *nwdic) {
                            NSString * takekOrderUrl = REPLACENULL(nwdic[@"url"]);
                            if (takekOrderUrl.length>0) {
                                if ([takekOrderUrl hasPrefix:@"http"]) {
                                    [WebVC pushWebViewUrlOrStr:takekOrderUrl title:@"领券购买" isSHowType:2];
                                }else{
                                    SHOWMSG(@"领券失败!")
                                }
                            }else{
                                SHOWMSG(@"领券失败!")
                            }
                        } failure:^(NSString *failure) {
                            MBHideHUD SHOWMSG(failure)
                        }];
                    }
                }else{
                    SHOWMSG(@"领券失败!")
                }
            }else{
                SHOWMSG(@"领券失败!");
            }
        } failure:^(NSString *failure) {
            SHOWMSG(@"领券失败!")
        }];
    }else{
        [PageJump takeOrder:_modeld];
    }
    
    
    //    if (_modeld) {
    //        [PageJump takeOrder:_modeld];
    //    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}


@end









#pragma mark ===>>> 显示推荐语view

@implementation RmdStrCltCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        _rmdStrView = [[UIView alloc]init];
        _rmdStrView.backgroundColor = COLORWHITE ;
        [self addSubview:_rmdStrView];
        [_rmdStrView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);  make.right.offset(0);
            make.top.offset(0);  make.bottom.offset(0);
        }];
        
        UIImageView * linesT = [[UIImageView alloc]init];
        linesT.backgroundColor = COLORGROUP ;
        [_rmdStrView addSubview:linesT];
        [linesT mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);  make.height.offset(1);
            make.left.offset(0); make.right.offset(0);
        }];
        
        UIImageView * linesB = [[UIImageView alloc]init];
        linesB.backgroundColor = COLORGROUP ;
        [_rmdStrView addSubview:linesB];
        [linesB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.height.offset(1);
            make.left.offset(0);    make.right.offset(0);
        }];
        
        UILabel * tuiJianCiLab = [UILabel labText:@"推荐词" color:LOGOCOLOR font:11*HWB];
        
        tuiJianCiLab.layer.masksToBounds = YES ;
        tuiJianCiLab.layer.cornerRadius = 3 ;
        tuiJianCiLab.textAlignment = NSTextAlignmentCenter ;
        [tuiJianCiLab boardWidth:1 boardColor:LOGOCOLOR];
        if (THEMECOLOR==2) {
            tuiJianCiLab.textColor=[UIColor redColor];
            [tuiJianCiLab boardWidth:1 boardColor:[UIColor redColor]];
        }
        [_rmdStrView addSubview:tuiJianCiLab];
        [tuiJianCiLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(12);  make.top.offset(15);
            make.width.offset(38*HWB);
            make.height.offset(14*HWB);
        }];
        
        _rmdStrLab = [UILabel labText:@"推荐理由" color:Black102 font:12*HWB];
        _rmdStrLab.numberOfLines = 0 ;
        [_rmdStrView addSubview:_rmdStrLab];
        [_rmdStrLab mas_makeConstraints:^(MASConstraintMaker * make) {
            make.left.offset(15+40*HWB);  make.right.offset(-15);
            make.top.offset(15);
        }];
    }
    return self;
}

@end




#pragma mark ===>>> 商品详情页图片加载

@implementation GoodsDetailImgCell

-(void)setImgUrl:(NSString *)imgUrl{
    _imgUrl = imgUrl;
    [_detailImgV sd_setImageWithURL:[NSURL URLWithString:_imgUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        @try {
            if (image!=nil) {
                float bili = image.size.width/image.size.height;
                NSString * hi = FORMATSTR(@"%.0f",ScreenW/bili);
                
                if (image.size.width>10&&image.size.height>10) {
                    self.blockHi(hi,self.tag);
                }else{
                    self.blockHi(@"0", self.tag);
                }
            }else{
                self.blockHi(@"0",self.tag);
            }
        } @catch (NSException *exception) {
            
        } @finally { }
        
    }];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self =[super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES ;
        [self detailImgV];
    }
    return self ;
}

-(UIImageView*)detailImgV{
    if (!_detailImgV) {
        _detailImgV = [[UIImageView alloc]init];
        _detailImgV.backgroundColor = COLORWHITE ;
        [self.contentView addSubview:_detailImgV];
        [_detailImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0); make.bottom.offset(0);
            make.left.offset(0); make.right.offset(0);
        }];
    }
    return _detailImgV;
}

@end







