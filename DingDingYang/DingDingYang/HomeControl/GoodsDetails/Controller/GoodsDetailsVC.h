//
//  GoodsDetailsVC.h
//  DingDingYang
//
//  Created by ddy on 2017/5/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import <UIKit/UIKit.h>
@class GoodsModel;
@interface GoodsDetailsVC : DDYViewController
// 0:正常商品页面 1:淘宝 2:京东 3:拼多多 4:唯品会 5:多麦
@property(nonatomic,assign) NSInteger pushType;
@property(nonatomic,strong) GoodsModel * dmodel;
@end



/*

//下载一张图片、计算图片宽高比例
[[SDWebImageManager sharedManager]downloadImageWithURL:[NSURL URLWithString:_detailImgAry[0]] options:SDWebImageCacheMemoryOnly progress:^(NSInteger receivedSize, NSInteger expectedSize) {
    
} completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
    if (image!=nil) {
        float bili = image.size.width/image.size.height ;
        _imgHeight = [ROUNDED(FORMATSTR(@"%f",ScreenW/bili), 0) floatValue];
        MBHideHUD
        NSIndexSet * indexS = [[NSIndexSet alloc]initWithIndex:1];
        [_tabView reloadSections:indexS withRowAnimation:UITableViewRowAnimationFade];
        [UIView animateWithDuration:0.26 animations:^{
            _tabView.contentOffset = CGPointMake(0, ScreenW);
        }];
    }else{
        MBHideHUD
        SHOWMSG(@"加载失败!")
    }
}];
*/

