//
//  GoodsDetailsVC.m
//  DingDingYang
//
//  Created by ddy on 2017/5/2.
//  Copyright © 2017年 ddy.All rights reserved.
//

#import "GoodsDetailsVC.h"
#import "OneGoodsCell.h"
#import "HViewOne.h"
#import "GoodsFeedBackVC.h"
#import "GoodsListCltCell1.h"
#import "EmptyCltHFView.h"
#import "GoodsShopCell.h"
#import "ShopModel.h"

#import "CreateShareImageView6.h"
#import "WqeView.h"


@interface GoodsDetailsVC () <UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>

// 顶部View(颜色渐变)
@property(nonatomic,strong) UIView   * topView ;
@property(nonatomic,strong) UIButton * backBtn ;
@property(nonatomic,strong) UILabel  * titLab;

// 底部功能view和按钮
@property(nonatomic,strong) UIView * bottomView,*bottomBtnView;  // 放置分享反馈按钮
@property(nonatomic,strong) UIButton * shareBtn , * buyBtn , * feedBackBtn ; //分享，购买，反馈

//新的商品详情页使用CollectionView实现
@property(nonatomic,strong) UICollectionView * detailCltView;
@property(nonatomic,strong) UICollectionViewFlowLayout * detailLayout ;
@property(nonatomic,strong) NSMutableArray * cnxhGoodsAry ;  // 猜你喜欢的商品数据
@property(nonatomic,strong) NSMutableArray * detailImgAry ;  // 商品主图、轮播图
@property(nonatomic,strong) NSMutableArray * detailImgAry2 ; // 商品详情图片
@property(nonatomic,strong) NSMutableArray * sizeImgAry ;    // 图片高度缓存
@property(nonatomic,assign) BOOL  isopen; //记录商品详情的展开和折叠

@property(nonatomic,strong) UIButton * topBtn ;  // 置顶按钮
@property(nonatomic,copy)   NSString * almmpid ; // 阿里妈妈pid
//@property(nonatomic,assign) float titleRedStrH ; // 动态计算到的标题和推荐语的总高度(第一分区总高度)

@property(nonatomic,assign) NSInteger styleType ; // 商品详情页样式
@property(nonatomic,assign) NSInteger shopTestSection ; // 商品详情页样式
@property(nonatomic,strong) ShopModel* shopModel;
@property(nonatomic,assign) BOOL ifCanClick;

@property(nonatomic,strong) UIButton * changeStyleBtn; // 样式切换按钮

@end

@implementation GoodsDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"商品详情" ;
    self.view.backgroundColor = COLORGROUP ;
    
    _styleType = 3;

    if (PID==10102) { _styleType = 5 ; } // 叮当
    if (PID==10104) { _styleType = 2 ; } // 蜜赚
    if (PID==10110) { _styleType = 6 ; } // 惠得
    
    if (_pushType==4) { _styleType = 6 ; }

    if (_pushType==2) { _styleType = 7 ; }
    
    _shopTestSection = 0;

    _sizeImgAry = [NSMutableArray array];
    _cnxhGoodsAry = [NSMutableArray array];
    _detailImgAry = [NSMutableArray array];
    _detailImgAry2 = [NSMutableArray array];
    
    if (_dmodel.goodsId.length==0&&_dmodel.goodsName.length==0) {
        SHOWMSG(@"推荐商品已过期或已下架!")
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            PopTo(YES)
        });
    } else {
        [self getGoodsPicsAry];
    }
    
    if (THEMECOLOR==2) {
        [_shareBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
        [_buyBtn setTitleColor:Black51 forState:(UIControlStateNormal)];
    }
    
    [self detailCltView];
    [self topView];
    [self bottomView];
    [self topBtn];
    
    /*
    //详情页可能会被多次push，在此修改层级关系，去掉当前详情页之前的其他详情页
    NSMutableArray * mutableVC = [NSMutableArray array];
    NSArray * vcs = self.navigationController.viewControllers ;
    for (NSInteger i=0 ; i< vcs.count-1 ; i++) {
        if (![vcs[i] isKindOfClass:[GoodsDetailsVC class]]) {
            [mutableVC addObject:vcs[i]];
        }
    }
    [mutableVC addObject:self];
    [self.navigationController setViewControllers:(NSArray*)mutableVC animated:YES];
    */
#warning ce_shi
#if DEBUG
    [self changeStyleBtn];
#endif
}

-(UIButton*)changeStyleBtn{
    if (!_changeStyleBtn) {
        _changeStyleBtn = [UIButton titLe:FORMATSTR(@"样式 %ld",(long)_styleType) bgColor:COLORWHITE titColorN:LOGOCOLOR font:13];
        [_changeStyleBtn boardWidth:1 boardColor:LOGOCOLOR corner:15];
        _changeStyleBtn.frame = CGRectMake(ScreenW-66, NAVCBAR_HEIGHT-40, 56, 30);
        ADDTARGETBUTTON(_changeStyleBtn, clickChangeStyle)
        [self.view addSubview:_changeStyleBtn];
    }
    return _changeStyleBtn;
}
-(void)clickChangeStyle{
    if (_styleType>=7) { _styleType = 1; } else { _styleType++; } [_detailCltView reloadData];
    [_changeStyleBtn setTitle:FORMATSTR(@"样式 %ld",(long)_styleType) forState:(UIControlStateNormal)];
}


#pragma mark ===>>> 接口
// 获取商品轮播图
-(void)getGoodsPicsAry{
    
    switch (_pushType) {
            
        case 2:{ // 京东
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            [NetRequest requestType:0 url:url_goods_detail_jd ptdic:@{@"goodsId":_dmodel.goodsId} success:^(id nwdic) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [_dmodel addDetailDic:NULLDIC(@"bean")];
                if (_dmodel.goodsGalleryUrls.count==0) {
                    _dmodel.goodsGalleryUrls = @[_dmodel.goodsPic];
                }
                [_detailImgAry addObjectsFromArray:_dmodel.goodsGalleryUrls] ;
                [_detailCltView reloadData];
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                SHOWMSG(failure)
            }];
        } break;
            
        case 3:{ // 拼多多
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            NSDictionary * pdic = @{@"goodsId":_dmodel.goodsId};
            [NetRequest requestType:0 url:url_goods_details_pdd ptdic:pdic success:^(id nwdic) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [_dmodel addDetailDic:nwdic];
                if (_dmodel.goodsGalleryUrls.count==0) {
                    _dmodel.goodsGalleryUrls = @[_dmodel.goodsPic];
                }
                [_detailImgAry addObjectsFromArray:_dmodel.goodsGalleryUrls] ;
                [_detailCltView reloadData];
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                SHOWMSG(failure)
            }];
        } break;
            
        case 4:{ // 唯品会
            [_detailImgAry addObject:_dmodel.goodsPic];
            [_detailCltView reloadData];
        } break;
            
        case 5:{ // 多麦
            [_detailImgAry addObjectsFromArray:_dmodel.gdImgAry];
            [_detailCltView reloadData];
        } break;
            
        default:{
            
            [NetRequest requestType:0 url:url_goods_pics ptdic:@{@"goodsId":_dmodel.goodsId} success:^(id nwdic) {
                NSArray * ary = NULLARY(nwdic[@"images"]);
                [_detailImgAry addObjectsFromArray:ary];
                if (_detailImgAry.count>0) {
                    [_detailCltView reloadData];
                }else{
                    if ([_dmodel.goodsPic hasPrefix:@"http"]) {
                        [_detailImgAry addObject:_dmodel.goodsPic] ;
                        [_detailCltView reloadData];
                    }
                }
                [_dmodel addDetailDic:nwdic];
                _almmpid = REPLACENULL(nwdic[@"pid"]) ;
                [self huoquDesStr]; // 获取推荐语
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) { SHOWMSG(failure) }];
        } break;
    }

}

// 获取推荐语
-(void)huoquDesStr{
    
    NSDictionary* par=@{@"goodsId":_dmodel.goodsId,@"ifShop":@"1"};
    [NetRequest requestType:0 url:url_goods_rmdText ptdic:par success:^(id nwdic) {
        [_dmodel setValue:REPLACENULL(nwdic[@"des"]) forKey:@"desStr"];
        ShopModel* shop = [[ShopModel alloc]initWithDic:nwdic[@"shop"]];
        _dmodel.shopModel = shop;
        shop.platform = _dmodel.platform;
        if (shop.shopTitle.length>0&&shop.deliveryScore.length>0&&shop.itemScore.length>0&&shop.serviceScore.length>0) {
            _shopModel = shop;
            _shopTestSection = 1;
        }else{
            _shopTestSection=0;
        }
        [_detailCltView reloadData];
        [self caiNiXiHuan];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        
    }];

}

// 获取猜你喜欢
-(void)caiNiXiHuan{
    [NetRequest requestType:0 url:url_goods_list6 ptdic:@{@"key":_dmodel.goodsName} success:^(id nwdic) {
        NSArray * lAry = NULLARY(nwdic[@"list"]);
        [lAry enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
            GoodsModel*model=[[GoodsModel alloc]initWithDic:obj];
            if (![model.goodsId isEqualToString:_dmodel.goodsId]) {
                [_cnxhGoodsAry addObject:model];
            }
        }];
        [_detailCltView reloadData];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        
    }];

}

//获取商品详情
-(void)getGoodsDetailsImgs:(UIButton*)btn{

    if (_isopen==YES) {
        _isopen = NO ;
        [_detailCltView reloadData];
        [_detailCltView setContentOffset:CGPointZero animated:YES];
        return ;
    }
    
    if (_isopen==NO) {
        if (_detailImgAry2.count>0) {
            [self showDetailImg];
        }else{
            MBShow(@"正在获取...")
            [NetRequest requestType:0 url:url_goods_detail2 ptdic:@{@"goodsId":_dmodel.goodsId} success:^(id nwdic) {
                if (REPLACENULL(nwdic[@"images"]).length>0) {
                    if ([nwdic[@"images"] isKindOfClass:[NSArray class]]) {
                        NSArray * aryimgs = NULLARY(nwdic[@"images"]) ;
                        [_detailImgAry2 addObjectsFromArray:aryimgs] ;
                        if (_detailImgAry2.count>0) {
                            [self jiaSuanTuPian];
                        }else{ MBHideHUD SHOWMSG(@"获取到的商品详情为空!") }
                    }
                }else{ MBHideHUD SHOWMSG(@"获取到的商品详情为空!") }
            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
                MBHideHUD  SHOWMSG(failure)
            }];
        }
    }
}

//先下载第一张图片、计算一个默认高度
-(void)jiaSuanTuPian{
    
    [NetRequest downloadImageByUrl:_detailImgAry2[0] success:^(id result) {
        MBHideHUD
        UIImage * image = (UIImage*)result;
        if (image==nil) {
            for (NSInteger i=0 ; i<_detailImgAry2.count ; i++ ) {
                [_sizeImgAry addObject:@"200"];
            }
        }else{
            float bili = image.size.width/image.size.height ;
            NSString * hi = FORMATSTR(@"%.0f",ScreenW/bili) ;
            for (NSInteger i=0 ; i<_detailImgAry2.count ; i++ ) {
                [_sizeImgAry addObject:hi];
            }
        }
        [self showDetailImg];
    } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
        MBHideHUD
        for (NSInteger i=0 ; i<_detailImgAry2.count ; i++ ) {
            [_sizeImgAry addObject:@"200"];
        }
        [self showDetailImg];
    }];

}

-(void)showDetailImg{
    _isopen = YES ;
    @try {
        [_detailCltView reloadData];
        NSIndexPath * indexRow = [NSIndexPath indexPathForRow:0 inSection:1];
        [_detailCltView scrollToItemAtIndexPath:indexRow atScrollPosition:(UICollectionViewScrollPositionCenteredVertically) animated:YES];
    } @catch (NSException *exception) {
        [_detailCltView reloadData];
        [_detailCltView setContentOffset:CGPointMake(0, ScreenW+180) animated:YES];
    } @finally { }
    
}
#pragma mark === >>> 详情使用UICollectionView

-(UICollectionView*)detailCltView{
    if (!_detailCltView) {
        _detailLayout = [[UICollectionViewFlowLayout alloc]init];
        _detailCltView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:_detailLayout];
        _detailCltView.delegate = self ;
        _detailCltView.dataSource = self ;
        _detailCltView.backgroundColor = COLORGROUP ;
        _detailCltView.showsVerticalScrollIndicator = NO ;
        [self.view addSubview:_detailCltView];
        [_detailCltView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(44-NAVCBAR_HEIGHT);
            make.left.offset(0); make.right.offset(0);
            make.bottom.offset(IS_IPHONE_X?-70:-48);
        }];
        
        [_detailCltView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        
        [_detailCltView registerClass:[OneGoodsCell  class] forCellWithReuseIdentifier:@"detail_cell_one"];
        [_detailCltView registerClass:[OneGoodsCell2 class] forCellWithReuseIdentifier:@"detail_cell_one2"];
        [_detailCltView registerClass:[OneGoodsCell3 class] forCellWithReuseIdentifier:@"detail_cell_one3"];
        [_detailCltView registerClass:[OneGoodsCell4 class] forCellWithReuseIdentifier:@"detail_cell_one4"];
        [_detailCltView registerClass:[OneGoodsCell5 class] forCellWithReuseIdentifier:@"detail_cell_one5"];
        [_detailCltView registerClass:[OneGoodsCell6 class] forCellWithReuseIdentifier:@"detail_cell_one6"];
        [_detailCltView registerClass:[OneGoodsCell7 class] forCellWithReuseIdentifier:@"detail_cell_one7"];

        
        [_detailCltView registerClass:[RmdStrCltCell class] forCellWithReuseIdentifier:@"rmd_str_clt_cell"];
        [_detailCltView registerClass:[GoodsShopCell  class] forCellWithReuseIdentifier:@"goods_shop_cell"];
        [_detailCltView registerClass:[GoodsDetailImgCell  class] forCellWithReuseIdentifier:@"detail_img_cell"];
        
        [_detailCltView registerClass:[GoodsListCltCell1   class] forCellWithReuseIdentifier:@"goods_listclt_cell1"];
        [_detailCltView registerClass:[GoodsListCltCell2   class] forCellWithReuseIdentifier:@"goods_listclt_cell2"];
        [_detailCltView registerClass:[GoodsListCltCell3   class] forCellWithReuseIdentifier:@"goods_listclt_cell3"];
        [_detailCltView registerClass:[GoodsListCltCell4   class] forCellWithReuseIdentifier:@"goods_listclt_cell4"];
        
        // 商品轮播图(分区0的区头)
        [_detailCltView registerClass:[BannerHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"banner_head"];
        // 点击查看更多详情
        [_detailCltView registerClass:[HViewOne class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"more_detail_head"];
        // 猜你喜欢、精品推荐区头
        [_detailCltView registerClass:[HViewTwo class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"cnxh_head"];
        // 空的区尾
        [_detailCltView registerClass:[EmptyCltHFView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"empty_foot"];
    
        
        /*
         两个分区
         分区0: (此处的淘宝商品: itme2和3不显示)
             区 头： 放商品轮播图
             itme0: 商品基本信息
             itme1: 商品推荐语(可为空，返回高度0)
             itme2: 店铺信息(可为空，返回高度0)
             区 尾： 不显示
         
         分区1: 商品详情 (拼多多不显示，可返回0)
             区 头： 查看商品详情按钮
             itme:  商品详情图片
         
         */
        
    }
    return _detailCltView ;
}


#pragma mark === >>> UICollectionView 代理

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) { return 3; }
    if (section==1) { return _isopen?_detailImgAry2.count:0; }
    return _cnxhGoodsAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        if (indexPath.item==0) {
            switch (_styleType) {
        
                case 2:{ // 一个大的优惠券显示、占据整个屏幕的宽
                    
                    OneGoodsCell2 * onecell2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one2" forIndexPath:indexPath];
                    onecell2.modeld = _dmodel;  return onecell2 ;
                    
                } break;
                    
                case 3:{ // 券和领券显示在右侧，渐变色的底色
                    
                    OneGoodsCell3 * onecell3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one3" forIndexPath:indexPath];
                    onecell3.modeld = _dmodel;  return onecell3;
                    
                } break;
                    
                case 4:{
                    
                    OneGoodsCell4 * onecell4 = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one4" forIndexPath:indexPath];
                    onecell4.modeld = _dmodel;  return onecell4;
                    
                } break;
                    
                case 5:{
                    
                    OneGoodsCell5 * onecell5 = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one5" forIndexPath:indexPath];
                    onecell5.modeld = _dmodel;  return onecell5;
                    
                } break;
                    
                case 6:{
                    
                    OneGoodsCell6 * onecell = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one6" forIndexPath:indexPath];
                    if (_pushType==4) { onecell.quanLabel.superview.hidden=YES; onecell.xlLab.hidden = YES; }
                    onecell.modeld = _dmodel;  return onecell;
                    
                } break;
                    
                case 7:{
                    
                    OneGoodsCell7 * onecell = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one7" forIndexPath:indexPath];
                    if (_pushType==4) { onecell.quanLabel.superview.hidden=YES; onecell.xlLab.hidden = YES; }
                    onecell.modeld = _dmodel;  return onecell;
                    
                } break;
                    
                default:{ // 第一版默认样式
                    
                    OneGoodsCell * onecell = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_cell_one" forIndexPath:indexPath];
                    onecell.modeld = _dmodel;  return onecell;
                    
                } break;
            }
        }
        if (indexPath.item==1) {
            RmdStrCltCell * rmdcell = [collectionView dequeueReusableCellWithReuseIdentifier:@"rmd_str_clt_cell" forIndexPath:indexPath];
            rmdcell.rmdStrLab.text = _dmodel.desStr ;
            return rmdcell ;
        }
        GoodsShopCell * shopCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_shop_cell" forIndexPath:indexPath];
        shopCell.model = _shopModel;  return shopCell;
        
    }
   
    if (indexPath.section==1) { // 图片详情 只有淘宝有，其它商品没有
        
        GoodsDetailImgCell * detailImgCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"detail_img_cell" forIndexPath:indexPath];
        detailImgCell.tag = indexPath.item;
        if (_detailImgAry2.count>=indexPath.item+1) {
            __weak GoodsDetailsVC * weakSelf = self;
            detailImgCell.blockHi = ^(NSString *hi, NSInteger itemRow) {
                [weakSelf reloadImgItems:itemRow imgHi:hi];
            };
            detailImgCell.imgUrl = _detailImgAry2[indexPath.item];
        }
        return detailImgCell;
    }

    GoodsListCltCell3 * list3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods_listclt_cell3" forIndexPath:indexPath];
    list3.goodsModel = _cnxhGoodsAry[indexPath.row];
    return list3;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==2) {
        [PageJump pushToGoodsDetail:_cnxhGoodsAry[indexPath.row] pushType:0];
    }
}

#pragma mark ===>>> 获取商品详情图片的高(相对于屏幕比例)
-(void)reloadImgItems:(NSInteger)item imgHi:(NSString*)hi{
    @try {
        if (_sizeImgAry.count>=item+1) {
            if (![hi isEqualToString:_sizeImgAry[item]]) {
                NSIndexPath * pathItem = [NSIndexPath indexPathForItem:item inSection:1];
                [_sizeImgAry replaceObjectAtIndex:item withObject:hi];
                [_detailCltView reloadItemsAtIndexPaths:@[pathItem]];
            }
        }
    } @catch (NSException *exception) { } @finally { }
   
}

#pragma mark === >>> item cell 代理设置

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) { // 商品基本信息
        if (indexPath.item==0) {
            float titleHi = _dmodel.titleHi;
            switch (_styleType) {
                case 2:{ return CGSizeMake(ScreenW, 142*HWB+titleHi); } break;
                case 3:{ return CGSizeMake(ScreenW, 80*HWB+titleHi); } break;
                case 4:{ return CGSizeMake(ScreenW, 80*HWB+titleHi); } break;
                case 5:{ return CGSizeMake(ScreenW, 50*HWB+titleHi); } break;
                case 6:{ return CGSizeMake(ScreenW, 70*HWB+titleHi); } break;
                case 7:{ return CGSizeMake(ScreenW, 70*HWB+titleHi); } break;
                default:{
                    CGRect desRect = StringRect(_dmodel.desStr, ScreenW-30, 12*HWB);
                    float hi = titleHi + desRect.size.height ;
                    if (_dmodel.desStr.length==0) { hi = titleHi -5; }
                    return CGSizeMake(ScreenW, 55 + hi ) ;
                } break;
            }
        }
        
        if (indexPath.item==1) { // 推荐语
            if (_styleType!=1&&_dmodel.desStr.length>0) { // 默认以外的样式
                CGRect desRect = StringRect(_dmodel.desStr, ScreenW-30-40*HWB, 12*HWB);
                return CGSizeMake(ScreenW, desRect.size.height+35) ;
            } else { return CGSizeZero; }
        }
        
        if (_pushType==3||_pushType==4) {
            return CGSizeZero;
        }else{
            if (_shopModel) {
                return CGSizeMake(ScreenW, 68*HWB);
            }else{
                return CGSizeZero;
            }
        }

    }

    if (indexPath.section==1) {
        if (_sizeImgAry.count>=indexPath.item+1&&_detailImgAry2.count>=indexPath.item+1) {
            return _isopen?CGSizeMake(ScreenW, [_sizeImgAry[indexPath.row] floatValue]):CGSizeZero;
        }else{ return CGSizeZero; }

        return CGSizeMake(ScreenW, [_sizeImgAry[indexPath.row] floatValue]) ;
    }
    return CGSizeMake(ScreenW/2-2,ScreenW/2 + 70*HWB );
    
}

#pragma mark ===>>> CollectionViewLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==2) { return 5; }
    return 0 ;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (section==2) { return 4 ; }
    return 0 ;
}


#pragma mark === >>> 区头区尾代理

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqual:UICollectionElementKindSectionHeader]) {
        
        if (indexPath.section==0) { // 轮播图
            BannerHeadView * bannerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"banner_head" forIndexPath:indexPath];
            if (_detailImgAry.count>0) { bannerView.urlAry = _detailImgAry ; }
            bannerView.gModel = _dmodel ;
            return bannerView ;
        }
        
        if (indexPath.section==1) { // 查看更多详情
            HViewOne * oneHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"more_detail_head" forIndexPath:indexPath];
            [oneHead.clickBtn addTarget:self action:@selector(getGoodsDetailsImgs:) forControlEvents:UIControlEventTouchUpInside];
            oneHead.isTransform = _isopen ;
            return oneHead ;
        }
        
        if (indexPath.section==2) { // 猜你喜欢
            HViewTwo * cnxhHead = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"cnxh_head" forIndexPath:indexPath];
            return cnxhHead ;
        }
    }
    
    if ([kind isEqual:UICollectionElementKindSectionFooter]) {
        EmptyCltHFView * empty = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"empty_foot" forIndexPath:indexPath];
        return empty ;
    }
    
    return  nil ;
}

// 区头
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section==0) { return CGSizeMake(ScreenW, ScreenW) ; }
    if (section==1) {
        if (_pushType==2||_pushType==3||_pushType==4||_pushType==5) {
            return CGSizeZero;
        }else{
            return CGSizeMake(ScreenW, 40) ;
        }
    }
    
    if (_pushType==0||_pushType==1) {
       return CGSizeMake(ScreenW,30) ;
    }else{ return CGSizeZero; }
    
}

// 区尾
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(ScreenW,5*HWB) ;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"contentOffset"]) {
        float   cofsttabY=_detailCltView.contentOffset.y;
        if (cofsttabY<ScreenW) {
            _topView.backgroundColor = RGBA(255.0,255.0,255.0,cofsttabY/(ScreenW-55));
            _titLab.alpha = cofsttabY/ScreenW;
        }else{
            _topView.backgroundColor = COLORWHITE;
            _titLab.alpha = 1 ;
        }
    }
}

#pragma mark === >>> 创建反馈，分享，领券按钮
-(UIView*)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = COLORWHITE ;
        [self.view addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);  make.height.offset(IS_IPHONE_X?70:48);
            make.left.offset(0);  make.right.offset(0);
        }];
        [self feedBackBtn];
        [self bottomBtnView];
    }
    return _bottomView ;
}

-(UIButton*)feedBackBtn{
    if (!_feedBackBtn) {
        _feedBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _feedBackBtn.backgroundColor = COLORWHITE ;
        [_feedBackBtn setTitle:@"反 馈" forState:UIControlStateNormal];
        _feedBackBtn.titleLabel.font = Font15 ;
        [_feedBackBtn setTitleColor:COLORGRAY forState:UIControlStateNormal];
        [_feedBackBtn setImage:[UIImage imageNamed:@"dgfeedback"] forState:UIControlStateNormal];
        [_feedBackBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 21, 25, 21)];
        [_feedBackBtn setTitleEdgeInsets:UIEdgeInsetsMake(22, -30, 0, 10)];
        ADDTARGETBUTTON(_feedBackBtn, gotoFeedBackVc)
        _feedBackBtn.frame = CGRectMake(0,0, 60, 48);
        [_bottomView addSubview:_feedBackBtn];
        
    }
    return _feedBackBtn ;
}

-(UIView*)bottomBtnView{
    if (!_bottomBtnView) {
        _bottomBtnView = [[UIView alloc]init];
        _bottomBtnView.backgroundColor = COLORWHITE;
        if (PID==10110) { // 惠得，分享购买按钮切圆
           [_bottomBtnView cornerRadius:21];
        }else{
           [_bottomBtnView cornerRadius:IS_IPHONE_X?22:0];
        }
        [_bottomView addSubview:_bottomBtnView];
        [_bottomBtnView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (IS_IPHONE_X||PID==10110) { // 惠得，分享购买按钮切圆
                make.top.offset(3);  make.height.offset(44);
                if (PID==10110) { make.height.offset(42); }
                make.left.offset(60);  make.right.offset(-8);
            }else{
                make.top.offset(0);  make.height.offset(48);
                make.left.offset(60);  make.right.offset(0);
            }
        }];
    }
    [self shareBtn];   [self buyBtn];
    
    if (THEMECOLOR==2) { //主题颜色是浅色
        [_shareBtn setTitleColor:Black51 forState:UIControlStateNormal];
        [_buyBtn setTitleColor:Black51 forState:UIControlStateNormal];
    }
    
    return _bottomBtnView;
}

-(UIButton*)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [UIButton titLe:@"分享商品" bgColor:SHAREBTNCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:14*HWB];
        ADDTARGETBUTTON(_shareBtn, shareGoods)
        [_bottomBtnView addSubview:_shareBtn];
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.offset(0);
            make.right.equalTo(_bottomBtnView.mas_centerX);
        }];
        if (role_state>0) { // 代理或运营商显示分享赚
            [_shareBtn setTitle:FORMATSTR(@"分享赚%@元",_dmodel.commission) forState:UIControlStateNormal];
            if (PID==10110) { // 惠得
               [_shareBtn setTitle:FORMATSTR(@"分享赚￥%@元",_dmodel.commission) forState:UIControlStateNormal];
            }
        }
    }
    
    if (role_state==0&&CANOPENWechat==NO) { _shareBtn.hidden = YES; }

    if (PID==10102) { _shareBtn.backgroundColor = RGBA(250, 81, 57, 1); } // 叮叮当
    return _shareBtn;
}

-(UIButton*)buyBtn{
    if (!_buyBtn) {
        NSString * buyTitle = FORMATSTR(@"领%@元优惠券",_dmodel.couponMoney);
        if (PID==10102) { buyTitle = @"领券购买"; } // 叮当叮当
        if (PID==10110) { buyTitle = FORMATSTR(@"领券省￥%@元",_dmodel.couponMoney); }
        if ([_dmodel.couponMoney floatValue]==0) { buyTitle = @"立即下单"; }
        _buyBtn = [UIButton titLe:buyTitle bgColor:LOGOCOLOR titColorN:COLORWHITE titColorH:COLORWHITE font:14*HWB];
        ADDTARGETBUTTON(_buyBtn, buyGoods)
        [_bottomBtnView addSubview:_buyBtn];
        [_buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (role_state==0&&CANOPENWechat==NO) { make.left.offset(0); }
            else{ make.left.equalTo(_bottomBtnView.mas_centerX).offset(0); }
            make.top.bottom.right.offset(0);
        }];
    }
    return _buyBtn;
}

#pragma mark ===>>>  分享赚钱、购买商品
-(void)shareGoods{
    if (_pushType==5) { // 多麦
        [NetRequest downloadImageByUrl:_dmodel.goodsPic success:^(id result) {
            CreateShareImageView6 * view = [[CreateShareImageView6 alloc]initWithFrame:CGRectMake(-2000, -2000, ScreenW, ScreenW*1.42)];
            view.backgroundColor = COLORWHITE;
            [self.view addSubview:view];
            view.goodsImage = (UIImage*)result;
            view.dmModel = _dmodel;
            UIImage * shareImage = [self convertViewToImage:view];
            [view removeFromSuperview];
            
            if (TARGET_IPHONE_SIMULATOR) { // 模拟器(显示预览)
                [ImgPreviewVC show:TopNavc.topViewController type:0 index:0 imagesAry:@[shareImage]];
            }else{
                [SharePopoverVC shareWPHGoodsModel:_dmodel thubImg:shareImage shareType:3];
            }
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            SHOWMSG(failure)
        }];
        
    }
    
//    else if(_pushType==2){ // 京东
//        [MoreWay showSpView:[UIApplication sharedApplication].keyWindow height:80+NAVCBAR_HEIGHT-64 isJD:YES andSelect:^(NSInteger way) {
//            MBShow(@"正在加载...")
//            [NetRequest requestType:0 url:url_goods_share_jd ptdic:@{@"goodsId":_dmodel.goodsId} success:^(id nwdic) {
//                MBHideHUD
//                NSString * shareText = REPLACENULL(nwdic[@"shareContent"]);
//                if (shareText.length==0) { shareText = SHARECONTENT; }
//                [ClipboardMonitoring pasteboard:REPLACENULL(shareText)];
//                SHOWMSG(@"分享的文本内容已复制，请手动粘贴~")
//                
//                [NetRequest downloadImageByUrl:_dmodel.goodsPic success:^(id result) {
//                    CustomShareModel * model = [[CustomShareModel alloc]init];
//                    model.shareType = 2; model.currentVC = TopNavc.topViewController;
//                    model.imgDataOrAry = result;
//                    model.sharePlatform = way;
//                    [CustomUMShare shareByCustomModel:model result:^(NSInteger result) { }];
//                } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
//                    SHOWMSG(failure)
//                }];
//            } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
//                MBHideHUD  SHOWMSG(failure)
//            }];
//        }];
//    }
    
    else{
        [PageJump shareGoods:_dmodel goodsImgAry:_detailImgAry];
    }
    
}
-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


-(void)buyGoods{
    if (_pushType==2) { // 京东
        MBShow(@"正在加载...")
        [NetRequest requestType:0 url:url_goods_buy_jd ptdic:@{@"goodsId":_dmodel.goodsId} success:^(id nwdic) {
            MBHideHUD
            if (REPLACENULL(nwdic[@"url"]).length>0) {
                [[KeplerApiManager sharedKPService] openKeplerPageWithURL:nwdic[@"url"] sourceController:self jumpType:2 userInfo:nil];
            }else{
                NSString * jdurl = FORMATSTR(@"http://item.jd.com/%@.html",_dmodel.goodsId);
               [[KeplerApiManager sharedKPService] openKeplerPageWithURL:jdurl sourceController:self jumpType:2 userInfo:nil];
            }
        } serverOrNetWorkError:^(NSInteger errorType, NSString *failure) {
            MBHideHUD SHOWMSG(failure)
        }];
    }else{
        [PageJump takeOrder:_dmodel];
    }
}


#pragma mark === >>> 进入反馈界面
-(void)gotoFeedBackVc{
    if (TOKEN.length>0) {
        GoodsFeedBackVC * gfbVC = [[GoodsFeedBackVC alloc]init];
        gfbVC.goodsid = _dmodel.goodsId ;
        PushTo(gfbVC, YES)
    }else{
        
        [LoginVc showLoginVC:self fromType:4 loginBlock:^(NSInteger result) {
            if (result==1) {
                GoodsFeedBackVC * gfbVC = [[GoodsFeedBackVC alloc]init];
                gfbVC.goodsid = _dmodel.goodsId ;
                PushTo(gfbVC, YES)
            }
        }];
        
    }
}

//顶部View
-(UIView*)topView{
    if (!_topView) {
        _topView = [[UIView alloc]init];
        _topView.frame = CGRectMake(0, 0, ScreenW, NAVCBAR_HEIGHT);
        [self.view addSubview:_topView];
        [self.view bringSubviewToFront:_topView];
        
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"dback"] forState:UIControlStateNormal];
        [_backBtn setImageEdgeInsets:UIEdgeInsetsMake(7, 7, 7, 7)];
        ADDTARGETBUTTON(_backBtn, clickBack)
        [_topView addSubview:_backBtn];
        [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(6); make.top.offset(NAVCBAR_HEIGHT-44);
            make.height.offset(44); make.width.offset(44);
        }];
        
        _titLab = [UILabel labText:@"商品详情" color:Black51 font:TITLEFONT];
        _titLab.frame=CGRectMake(ScreenW/2-100, NAVCBAR_HEIGHT-44, 200, 40);
        _titLab.textAlignment=NSTextAlignmentCenter;
        _titLab.alpha = 0 ;
        [_topView addSubview:_titLab];
        
    }
    return _topView;
}

-(void)clickBack{ PopTo(YES) }

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated: animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id) self;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (![[self.navigationController.viewControllers lastObject] isKindOfClass:[HomeVC class]]) {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
}

-(void)dealloc{
    [_detailCltView removeObserver:self forKeyPath:@"contentOffset"];
    _detailCltView.delegate = nil;
}

#pragma mark === >>> 置顶按钮
-(UIButton*)topBtn{
    if (!_topBtn) {
        _topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _topBtn.alpha = 0 ;
        [_topBtn setImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
        [_topBtn setImageEdgeInsets:UIEdgeInsetsMake(8*HWB, 8*HWB, 8*HWB, 8*HWB)];
        _topBtn.backgroundColor = ColorRGBA(255.f, 255.f, 255.f, 0.9);
        [_topBtn addTarget:self action:@selector(clickBackTop) forControlEvents:UIControlEventTouchUpInside];
        [_topBtn boardWidth:2 boardColor:COLORWHITE corner:36*HWB/2];
        [self.view addSubview:_topBtn];
        [_topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_bottomView.mas_top).offset(-10);
            make.right.offset(-10); make.width.offset(36*HWB);
            make.height.offset(36*HWB);
        }];
    }
    return _topBtn ;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (_detailCltView.contentOffset.y>=0) {
        if (_detailCltView.contentOffset.y>ScreenH) {
            _topBtn.alpha = 1 ;
        }else{
            if (_detailCltView.contentOffset.y>ScreenH/2) {
                _topBtn.alpha = _detailCltView.contentOffset.y/ScreenH ;
            }else{
                _topBtn.alpha = 0 ;
            }
        }
    }
}
-(void)clickBackTop{
    [_detailCltView setContentOffset:CGPointZero animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end





