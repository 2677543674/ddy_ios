//
//  ShopModel.h
//  DingDingYang
//
//  Created by ddy on 2018/5/15.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopModel : NSObject

@property(nonatomic,strong)NSString* createTime;
@property(nonatomic,strong)NSString* serviceScore;      //服务态度评分
@property(nonatomic,strong)NSString* shopTitle;         //店铺标题
@property(nonatomic,strong)NSString* itemScore;         //商品描述评分
@property(nonatomic,strong)NSString* picPath;           //头像地址
@property(nonatomic,strong)NSString* nick;              //卖家昵称
@property(nonatomic,strong)NSString* shopBulletin;      //店铺公告
@property(nonatomic,strong)NSString* sellerId;
@property(nonatomic,strong)NSString* shopId;
@property(nonatomic,strong)NSString* shopDes;           //店铺描述
@property(nonatomic,strong)NSString* deliveryScore;     //发货速度评分
@property(nonatomic,strong)NSString* platform;          //来源 淘宝 天猫
-(instancetype)initWithDic:(NSDictionary*)dic;

@end
