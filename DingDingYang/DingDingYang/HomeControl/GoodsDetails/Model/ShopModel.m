//
//  ShopModel.m
//  DingDingYang
//
//  Created by ddy on 2018/5/15.
//  Copyright © 2018年 ddy.All rights reserved.
//

#import "ShopModel.h"

@implementation ShopModel

-(instancetype)initWithDic:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        self.createTime = REPLACENULL(dic[@"createTime"]) ;
        self.serviceScore = REPLACENULL(dic[@"serviceScore"]) ;
        self.picPath =  [REPLACENULL(dic[@"picPath"]) hasPrefix:@"http"]? dic[@"picPath"] : FORMATSTR(@"%@%@",@"http://logo.taobao.com/shop-logo",REPLACENULL(dic[@"picPath"])) ;
        self.shopTitle = REPLACENULL(dic[@"shopTitle"]) ;
        self.itemScore = REPLACENULL(dic[@"itemScore"]) ;
        self.nick = REPLACENULL(dic[@"nick"]) ;
        self.shopBulletin = REPLACENULL(dic[@"shopBulletin"]) ;
        self.sellerId = REPLACENULL(dic[@"sellerId"]) ;
        self.shopId = REPLACENULL(dic[@"shopId"]) ;
        self.shopDes = REPLACENULL(dic[@"shopDes"]) ;
        self.deliveryScore = REPLACENULL(dic[@"deliveryScore"]) ;
        self.platform=@"";
    }
    return self ;
    
}

@end
