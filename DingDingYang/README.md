# DingDingYang

#18666004527
#389389

2019.09.10
1.模块与模块之间的间距都调一下距离，间距小店，间距的问题交给美工设计来做图
2.还有个人中心的今日预估与昨日预估字体调整下，小点，尽量不要让字体都占那么满

2019.09.04
Ios测试问题
1.首页banner的背景色渐变还不够好，广告图出来之后渐变才发生改变，可否同时进行的动作，或者能否加快渐变的速度。
2.编辑模板页，点击恢复模板需要提示“恢复成功，请点击保存按钮”
3.恢复模板 按钮没有请求接口，接口是：
http://apptest.times1688.com/app/getTemplateTxt.api?deviceId=cb2a1004-3a61-3869-a6c7-e747c5d59657/android&__rid=1&sign=b2dad3cb49a13a717b853b1bcb770945&appVersion=2.4.6&time=1567150736506&token=92ca34c1-fc27-4736-b8e4-1da35482cbef&versionNo=2.0&ifAuth=1&partnerId=10107

http://apptest.times1688.com/app/getTemplateTxt.api?partnerId=10107

4.个人中心【最底层】模块展示不全，【其他服务】字眼不需要添加，模块设置交给后台处理展示。
5.个人中心顶部消息模块点击没有反应，跳转的是消息接口。
6.广告模块都需要添加上token，包括闪屏页，弹框，浮动，目前这三个模块已经添加上天猫超市的连接跳转，都提示erro
Ios添加token的方法：网页的，就判断是不是我们平台的链接，是的话，那就再判断链接有没有token，有的不用拼，没的话，就把web链接拼上token

7.猜你喜欢需要放出来，方法是：[self caiNiXiHuan] 全局搜索它，把注释打开就有了



2019.08.27

1.启动页模糊（是否需要提供更大像素的图？）
2.渐变效果还是不理想，继续挑，渐变太突兀的，目前是轮流地往上往下渐变！，不是往左渐变吗？
3.无账号登录是点击无连接模块，提示了暂未开放。不需要做提示，以后自由编排模块
4.编辑模板页 —>点击恢复系统模板按钮出现乱版
5.个人中心淘宝收益与社群icon需要更换
6.点击本月预估按钮需要跳转到消费佣金页面
7.邀请码显示区需要展示出来
8.更换头像后，头像展示位白，没有生效，并且把原来的头像替换了


ios问题
1.首页banner背景色需采用渐变处理，使整个视觉效果更加柔和
2.最后一个模块GIF图依然是静态图，需要修改为动态图
3.编辑模板页面点击恢复系统按钮后，模板排版混乱，且缺少提示，提示为【恢复成功，请点击保存】
4.点击模块苏宁易购，美团，去哪儿网，更多等模块会出现闪退到登录页
5.模块上传一个纯图片，点击后会弹出提示暂未开放，需要取消这个提示，目的是全动态化，便于后期页面设计
6.每个模块都需要加上token，注：首页和个人中心的ban图模块，广告管理的闪屏页，弹框，浮动都需要添加上token
7.个人中心的淘宝消费佣金展示ui图需要添加上
